/***************************************************************************/
/*                                                                         */
/*              D A T A F I L E R E F E R E N C E . C P P                  */
/*                                                                         */
/*     Content : DatafileReference Class                                   */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : September 24th, 2012                                */
/*                                                                         */
/*          This is a template class designed to wrap around data files    */
/*     in order to reference many datafiles into 1 table. The class is a   */
/*     template class that autocast the pointers.                          */
/*                                                                         */
/***************************************************************************/

#include <grpsys.h>
#include <grpstd.h>

/*-------------------------------------------------------------------------*/
/*-                         Constructors                                  -*/
/*-------------------------------------------------------------------------*/

template<class t_datatype> DatafileReference<t_datatype>::DatafileReference ( void )
{
   int i;

   //printf ("Debug: Before Allocate\n");
   //**p_reference = new t_datatype [size];
   //printf ("Debug: after Allocate\n");

   for ( i = 0 ; i < DatafileReference_NB_DATAFILE ; i++)
   {


      p_datf [ i ] = NULL;
      //printf ("Debug: pointer value: %d = %d\n", i, p_reference [ i ] );
   }
}

template<class t_datatype> DatafileReference<t_datatype>::~DatafileReference ( void )
{
   //delete p_reference;
}

    // property methods

/*-------------------------------------------------------------------------*/
/*-                         Property Methods                              -*/
/*-------------------------------------------------------------------------*/

/*template<class t_datatype> int DatafileReference<t_datatype>::size ( void )
{
   return ( p_size );
} */// return the size of the array


/*-------------------------------------------------------------------------*/
/*-                             Methods                                   -*/
/*-------------------------------------------------------------------------*/

template<class t_datatype> void DatafileReference<t_datatype>::add ( DATAFILE *datf, int index )
{
   //int i;
   //int j;

   p_datf [ index ] = datf;



   /*for ( i = refindex, j = datfindex; i < nb_object && datf[j].type != DAT_ID('i','n','f','o') && datf[j].type != DAT_END ; i++, j++ )
   {
      p_reference [ i ] = (t_datatype) datf [ j ] . dat;

      //printf ("type: %d = %d (%s)\n", j, datf[j].type, get_datafile_property(datf+j, DAT_NAME ) );

   }*/

   // test code

   /*for (i=0; datf[i].type != DAT_ID('i','n','f','o') ; i++)
   {
      p_reference [ i ] = (t_datatype) datf [ j ] . dat;
      printf ("type: %d = %d (%s)\n", j, datf[j].type, get_datafile_property(datf+j, DAT_NAME ) );
      //printf ("type: %d = %d (%s)\n", j, datf[j].type, get_datafile_property(datf + j, DAT_ID('T','Y','P','E') ) );

      j++;
   }*/


   //printf ("type: %d = %d \n", j, datf[j].type );

   /*while ( i < refindex + nb_object && datf [ j ].type != DAT_END )
   {
      if ( datf [ j ] . type != DAT_END )
      {
          p_reference [ i ] = (t_datatype) datf [ j ] . dat;
      }
      printf ("type: %d = %d\n", j, datf[j].type);
      if ( datf [ j ] . type == DAT_END )
         printf ("DAT_END = true in %d", j );
      j++;
      i++;
   }*/

/*int i;

   for (i=0; dat[i].type != DAT_END; i++)
   {
      // name string : get_datafile_property(dat+i, DAT_NAME);
      // type: dat[i].type
   }*/


}

/*const int name_type = DAT_ID('N','A','M','E');
   for (i=0; dat[i].type != DAT_END; i++) {
      if (stricmp(get_datafile_property(dat+i, name_type),
                  "my_object") == 0) {
         // found the object at index i
      }
   }
   // not found...
*/

template<class t_datatype> void DatafileReference<t_datatype>::remove ( int index )
{
   //int i;
   //int j;

   //j = datfindex;
   /*for ( i = refindex; i < refindex + nb_object; i++)
   {
      p_reference [ i ] = NULL;
     // j++;
   }*/
   p_datf [ index ] = NULL;
}

template<class t_datatype> bool DatafileReference<t_datatype>::is_loaded ( int index )
{
   if ( p_datf [ index ] == NULL)
      return ( false );
   else
      return ( true );
}

template<class t_datatype> bool DatafileReference<t_datatype>::eof ( int datfid, int objectid )
{
   //if ( p_datf [ datfid] [objectid].type == DAT_END )
   if ( p_datf [ datfid] [objectid].type == DAT_ID('i','n','f','o') )
   {

      return ( true );
   }
   else
      return ( false );

/*// code samples

int i;

   for (i=0; dat[i].type != DAT_END; i++)
   {
      // name string : get_datafile_property(dat+i, DAT_NAME);
      // type: dat[i].type
   }*/




/*const int name_type = DAT_ID('N','A','M','E');
   for (i=0; dat[i].type != DAT_END; i++) {
      if (stricmp(get_datafile_property(dat+i, name_type),
                  "my_object") == 0) {
         // found the object at index i
      }
   }
   // not found...
*/
}

template<class t_datatype> t_datatype DatafileReference<t_datatype>::get ( int datfid, int objectid )
{
   t_datatype retval = NULL;

   if ( p_datf [ datfid ] != NULL )
      retval = (t_datatype) p_datf [ datfid ] [ objectid ] . dat;

   return ( retval );
}

template<class t_datatype> int DatafileReference<t_datatype>::getid ( int datfid, int objectid )
{
   return ( ( datfid << 10) + objectid );
}

/*-------------------------------------------------------------------------*/
/*-                           Operators                                   -*/
/*-------------------------------------------------------------------------*/


template<class t_datatype> t_datatype DatafileReference<t_datatype>::operator[](int index)
{
   int datfid;
   int objid;

   datfid = ( index >> 10 );
   objid = ( index & DatafileReference_OBJECTID_MASK );

   return ( (t_datatype) p_datf [ datfid ] [ objid ] . dat );

   //doc  p_reference [ i ] = (t_datatype) datf [ j ] . dat;
} // read data from the datafile


/*-------------------------------------------------------------------------*/
/*-                           Template Instanciation                      -*/
/*-------------------------------------------------------------------------*/


template class DatafileReference<BITMAP*>;
template class DatafileReference<SAMPLE*>;
template class DatafileReference<MIDI*>;
template class DatafileReference<FONT*>;
