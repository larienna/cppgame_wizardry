
#include <allegro.h>
#include <textoutpatch.h>
#include <stdio.h>
#include <stdarg.h>
//NOTE: only include C headers to avoid loading C++ headers.


//--- Private Global Variables ---

//defines the background color for the wrappers with further method calls
int bg = 0;

// --- Public methods ---

int text_mode_old ( int mode )
{
   bg = mode;
   return 0;
}

void textout_old(BITMAP *bmp, const FONT *f, const char *s, int x, int y, int color)
{
   textout_ex ( bmp, f, s, x, y, color, bg);
}

void textout_centre_old(BITMAP *bmp, const FONT *f, const char *s, int x, int y, int color)
{
   textout_centre_ex(bmp, f, s, x, y, color, bg);
}

void textout_right_old(BITMAP *bmp, const FONT *f, const char *s, int x, int y, int color)
{
   textout_right_ex(bmp, f, s, x, y, color, bg);
}

void textout_justify_old(BITMAP *bmp, const FONT *f, const char *s, int x1, int x2, int y, int diff, int color )
{
   textout_justify_ex(bmp, f, s, x1, x2, y, diff, color, bg);
}

void textprintf_old(BITMAP *bmp, const FONT *f, int x, int y, int color, const char *fmt, ...)
{
   char buffer [ TEXTOUT_BUFFER_MAXSIZE ];
   va_list valist;

   va_start(valist, fmt);
   vsprintf (buffer, fmt, valist );
   textout_ex ( bmp, f, buffer, x, y, color, bg );
   va_end(valist);
}

void textprintf_centre_old(BITMAP *bmp, const FONT *f, int x, int y, int color, const char *fmt, ...)
{
   char buffer [ TEXTOUT_BUFFER_MAXSIZE ];
   va_list valist;

   va_start(valist, fmt);
   vsprintf (buffer, fmt, valist );
   textout_centre_ex ( bmp, f, buffer, x, y, color, bg );
   va_end(valist);
}

void textprintf_right_old(BITMAP *bmp, const FONT *f, int x, int y, int color, const char *fmt, ...)
{
   char buffer [ TEXTOUT_BUFFER_MAXSIZE ];
   va_list valist;

   va_start(valist, fmt);
   vsprintf (buffer, fmt, valist );
   textout_right_ex ( bmp, f, buffer, x, y, color, bg );
   va_end(valist);
}

void textprintf_justify_old(BITMAP *bmp, const FONT *f, int x1, int x2, int y, int diff, int color, const char *fmt, ...)
{
   char buffer [ TEXTOUT_BUFFER_MAXSIZE ];
   va_list valist;

   va_start(valist, fmt);
   vsprintf (buffer, fmt, valist );
   textout_justify_ex ( bmp, f, buffer, x1, x2, y, diff, color, bg);
   va_end(valist);

}





