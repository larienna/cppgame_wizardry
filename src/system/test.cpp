/************************************************************************/
/*                                                                      */
/*                        T E S T . C P P                               */
/*                                                                      */
/*   Content:  test methods                                             */
/*   Programmer: Eric Pietrocupo                                        */
/*   Starting Date: October, 14th, 2017                                 */
/*                                                                      */
/*   List of testing method to be called from the main in order to test */
/*   various features. Also liberate the main from this junk code.      */
/*                                                                      */
/************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>
#include <init.h>


int test_callback ( void )
{
   return 0;
}

void alternate_testing_main (void)
{
   //Menu testmenu ("allo");
   //List testlist ("Select your class", 4 , true);
   //Option opt_test ("Select your option");
   //int init_status;
   //int answer;
   //int answer2;
   //int error;
   //int loctime;
   //int clocktime;
   //int i;
   //int j;
   //int answer;
   //Race tmprace;
   //Game tmpgame;
   //Party tmparty;
   //CClass tmpclass;
   //Character tmpcharacter;
   //Item tmpitem;
   //Monster tmpmonster;
   //char tmpstr [100];
   //char tmpstr2 [100];



   //int value;
//   int i;

   //allegro_init();

   //init_sound();
   /*if ( detect_midi_driver ( MIDI_DIGMID ) > 0)
      printf ("Digimid is available\n");

   if ( detect_midi_driver ( MIDI_OSS ) > 0)
      printf ("Open Sound is available\n");

   if ( detect_midi_driver ( MIDI_ALSA ) > 0)
      printf ("Alsa is available\n");

   set_config_string ( "sound", "patches", "/home/ericp/DataPartition/dev/Wizardry/digmid.dat");

   set_config_string ( "sound", "midi_volume", "255" );
   set_config_string ( "sound", "midi_voices", "-1" );


   //set_config_string ( "sound", "patches", "digmid.dat");

   printf ("- Adding Digital and Midi audio\r\n");
   value = install_sound ( DIGI_AUTODETECT, MIDI_AUTODETECT, NULL);

   if ( value != 0 )
   {
      printf ( "ERROR: %s\n", allegro_error);
   }
   else
   {*/

   //}
   //s_Texture_palette testpal [32];

   //int i;

   //EnemyGroup tmpgroup;
   //List lst_temp ("Select your Enemy Group");

   //SQLactivate_errormsg();

   int init_status = init_game ();
   //s_Texture_palette texpal;

   if ( init_status == INIT_SUCCESS )
   {
      //sound test

      /*draw_sprite ( subbuffer, datref_image[1], 0, 0 );

      copy_buffer();

      rest (1000);

      show_transition_slide_fade ( datref_image[1], Screen_FADE_OUT, Screen_SLIDE_RIGHT, 8);

      show_transition_slide_fade ( datref_image[1], Screen_FADE_IN, Screen_SLIDE_RIGHT, 8);

      draw_sprite ( subbuffer, datref_image[1], 0, 0 );

      copy_buffer();

      rest (1000);*/


     /* set_volume ( i, 255 );
      play_music_track ( 1 );
      for ( i = 55; i <= 255 ; i += 50)
      {

         set_volume ( i, 255 );
         play_sound ( 1027);
         rest (5000);
      }*/


      //rest (3000);








      /*FILE * fp;

      fp = fopen ("anchor.txt", "w+");

      for ( i = 0 ; i < 15 ; i++)
      {
         fprintf (fp, "   { // Tile %d\n", i );

         for ( j = 0 ; j < 9 ; j++)
         {


            FLOOR_OBJECT [i][j] . anchor_x = OBJECT [i][j] . x + ( OBJECT[i][j] .width / 2);
            FLOOR_OBJECT [i][j] . anchor_y = OBJECT [i][j] . y;// + OBJECT[i][j].height;
            FLOOR_OBJECT [i][j] . scale = ( OBJECT [i][j] .width * 100 ) / 256;
            fprintf (fp, "      { %4d, %4d, %4d },\n",
                    FLOOR_OBJECT [i][j] . anchor_x,
                    FLOOR_OBJECT [i][j] . anchor_y,
                    FLOOR_OBJECT [i][j] . scale );

         }
         fprintf (fp, "   },\n");
      }

      fclose(fp);*/

      //DATAFILE *tmpmusic = load_datafile_object ( "/home/ericp/DataPartition/dev/Wizardry/datafile/musicmidi.dat", "W1CAMP_MID");
//      DATAFILE *tmpmusic = load_datafile_object ( "/home/ericp/AllegroSetup/setup.dat", "TEST_MIDI");
      //play_midi ( (MIDI*) tmpmusic -> dat , 0);

      //play_midi ( datref_musicmidi [ 0 ], 0);

      //play_music_track ( 0 );

      //rest (2000);

      //unload_datafile_object (tmpmusic);
   //}
      /*Map testmap;

      testmap.write ( 0, 0, 0 );
      testmap.write ( 1, 1, 0 );
      testmap.write ( 2, 2, 0 );
      testmap.write ( 3, 3, 0 );

      testmap.display( party.position() );*/

      /*for ( i = 0; i < 15; i++ )
      {
         clear (screen);
         draw_sprite ( screen, datref_character [ i ], 0, 0 );
         answer = mainloop_readkeyboard();
         if ( answer == KEY_SPACE)
            make_screen_shot ("loadpng.bmp");
      }*/

      /*BITMAP *testpng = load_png ("01_fighter.png", NULL);

      draw_sprite ( screen, testpng, 0, 0 );
answer = mainloop_readkeyboard();*/

      /*set_volume ( 255, 255);
      set_hardware_volume(255, 255);

      load_midi_patches ();
      play_music_track ( System_MUSIC_w1intro );
      play_sample ( SMP_sound025, 255, 128, 1000, 0 );

      mainloop_readkeyboard();*/

      /*FONT* testfont;
      FONT* testfont2;
      PALETTE tmpal;

      //tmpal = load_palette ("")
      testfont = load_bitmap_font ("BookAntiquaBold24.bmp", tmpal, NULL);

      if ( testfont != NULL)
      {


         if ( font_has_alpha (testfont) == TRUE)
         {
            printf ("Font has alpha\n");

            textout_old ( screen, testfont, "This font has an alpha channel", 20, 20, General_COLOR_TEXT );
         }
         else
         {
            printf ("Font has no alpha\n");

            textout_old ( screen, testfont, "This font has no alpha", 20, 20, General_COLOR_TEXT );
         }

         //copy_buffer();
         //mainloop_readkeyboard();
      }
      else
         printf ("Font could not be loaded\n");

      testfont2 = load_bitmap_font ("BookAntiquaBold24aa.bmp", tmpal, NULL);

      make_trans_font(testfont2);
      set_alpha_blender();

      if ( testfont2 != NULL)
      {


         if ( font_has_alpha (testfont2) == TRUE)
         {
            printf ("Font has alpha\n");

            textout_old ( screen, testfont2, "This font has an alpha channel", 20, 50, General_COLOR_TEXT );
         }
         else
         {
            printf ("Font has no alpha\n");

            textout_old ( screen, testfont2, "This font has not alpha", 20, 50, General_COLOR_TEXT );
         }

         //copy_buffer();
         mainloop_readkeyboard();
      }
      else
         printf ("Font could not be loaded\n");


      destroy_font (testfont);
      destroy_font (testfont2);*/

//-------------------- combat roll testing ----------------------------

/*   int nb_hit_dnd [ 6 ];
   int nb_hit_myd20 [ 6 ];
   int nb_hit_myd20m [ 6 ];
   int i;
   int nbhit;
   int j;
   int bonus;
   int TN = 30;
   int tmpdice;

   // target number 20, level 20 fighter so +20/+15/+10/+5/+0

   for ( i = 0; i <=5 ; i++)
   {
      nb_hit_dnd [ i ] = 0;
      nb_hit_myd20 [ i ] = 0;
      nb_hit_myd20m [ i ] = 0;
   }

   for ( i = 0 ; i < 10000 ; i++ )
   {
      nbhit=0;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 20 ) > TN || tmpdice >= 19 ) nbhit++;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 15 ) > TN || tmpdice >= 19 ) nbhit++;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 10 ) > TN || tmpdice >= 19 ) nbhit++;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 5 ) > TN || tmpdice >= 19 ) nbhit++;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 0 ) > TN || tmpdice >= 19 ) nbhit++;
      nb_hit_dnd [ nbhit ] ++;
   }

   for ( i = 0 ; i < 10000 ; i++ )
   {
      nbhit = 0;
      bonus = 0;
      for ( j = 0 ; j < 5 ; j++)
      {
         tmpdice = dice (20);
         if ( ( tmpdice + 12 + bonus ) > TN || tmpdice >= 19 )
         {

            bonus = 0;
            nbhit++;
         }
         else
            bonus += 2;
      }
      nb_hit_myd20 [ nbhit] ++;

   }

   for ( i = 0 ; i < 10000 ; i++ )
   {
      nbhit = 0;
      bonus = 0;
      for ( j = 0 ; j < 5 ; j++)
      {
         tmpdice = dice ( 20 );
         if ( ( tmpdice + 12 + bonus ) > TN  || tmpdice >= 19 )
         {

            bonus -= 2;
            nbhit++;
         }
         //else
         //   bonus = 0;
      }
      nb_hit_myd20m [ nbhit] ++;

   }

   printf ("Nb hit:     | 0  | 1  | 2  | 3  | 4  | 5  |\n");
   printf ("D&D system: |%4d|%4d|%4d|%4d|%4d|%4d|\n"
           , nb_hit_dnd [ 0 ],nb_hit_dnd [ 1 ],nb_hit_dnd [ 2 ],nb_hit_dnd [ 3 ],nb_hit_dnd [ 4 ],nb_hit_dnd [ 5 ]);
   printf ("My D20:     |%4d|%4d|%4d|%4d|%4d|%4d|\n",
            nb_hit_myd20 [ 0 ],nb_hit_myd20 [ 1 ],nb_hit_myd20 [ 2 ],nb_hit_myd20 [ 3 ],nb_hit_myd20 [ 4 ],nb_hit_myd20 [ 5 ]);
   printf ("My D20 malus|%4d|%4d|%4d|%4d|%4d|%4d|\n",
            nb_hit_myd20m [ 0 ],nb_hit_myd20m [ 1 ],nb_hit_myd20m [ 2 ],nb_hit_myd20m [ 3 ],nb_hit_myd20m [ 4 ],nb_hit_myd20m [ 5 ]);
*/
//--------------- Database reading test code --------------------------


//      Text tmptext;
      //ActiveEffect tmpeffect;
/*      Monster tmpmons;
      int error;

      SQLactivate_errormsg();
      error = SQLopen ( "adventure//DebuggingDemo//database.sqlite");

      if ( error == SQLITE_OK )
      {
         //printf ("I passed open \n");

         //tmpspell.SQLprocedure ( Spell_CB_PRINTF_NAME, "");


         error  = tmpmons.template_SQLprepare ( "");

         List lst_monster ("List of Monsters", 16);
         lst_monster.header ("Name                         HpDice Resistance");

         int i = 0;

         if ( error == SQLITE_OK )
         {
            //printf ("I passed prepare \n");
            error = tmpmons.SQLstep();

            while ( error == SQLITE_ROW)
            {

               //printf ( "%12s: cost:%3d : Hand: %2d : Dmgy %d\n", tmpitem.name(), tmpitem.price(), tmpitem.hand (), tmpitem.dmgy () );
               i++;
               lst_monster.add_itemf ( i, "%-20s %2d %6d %s ", tmpmons.name(), tmpmons.hpdice(),
                                      tmpmons.resistance(), tmpmons.resistance_str() );
               if ( i % 50 == 0)
                  tmpmons.SQLinsert();

               error = tmpmons.SQLstep();
            }
         }

         WinList wlst_effect ( lst_monster, 20, 20 );
         Window::show_all();

      }

      tmpmons.SQLfinalize();
*/


         //tmpspell.SQLfinalize ();

         /*for ( i = 0 ; i < 22; i++)
            tmpeffect.set_int ( i , i);

         tmpeffect.set_str ( 0, "2nd Test Effect");
         tmpeffect.set_str ( 2, "Cond Test");

         tmpeffect.SQLinsert ();*/

         //tmpeffect.template_SQLselect ( 2 );


         //tmpeffect.SQLinsert();
//
         //tmpeffect.print_table_structure ();

         /*error = tmptext.SQLprepare ();



         if ( error == SQLITE_OK )
         {
            printf ("I passed prepare \n");
            error = tmptext.SQLstep();

            if ( error == SQLITE_ROW)
            {
               printf ("I passed step \n");*/
               /*lst_temp.add_itemf (0, "f=%d,a=%d,r=%d,b=%d,t=%d,p=%d,e1=%d,e2=%d,e3=%d,e4=%d,e5=%d,e6=%d,e7=%d,e8=%d",
                                 tmpgroup.floor(),
                                 tmpgroup.area(),
                                 tmpgroup.repeat(),
                                 tmpgroup.bonus(),
                                 tmpgroup.bonus_target(),
                                 tmpgroup.probability(),
                                 tmpgroup.enemy ( 0 ),
                                 tmpgroup.enemy ( 1 ),
                                 tmpgroup.enemy ( 2 ),
                                 tmpgroup.enemy ( 3 ),
                                 tmpgroup.enemy ( 4 ),
                                 tmpgroup.enemy ( 5 ),
                                 tmpgroup.enemy ( 6 ),
                                 tmpgroup.enemy ( 7 )                 );*/

    //           rect ( buffer, 0, 0, 639, 479, General_COLOR_TEXT );
/*
               textout_old ( buffer, FNT_small, tmptext.content(), 0, 0, General_COLOR_TEXT );

               FONT *tmpfnt = FNT_bookantiquabold24;
//FNT_deutschegothicbold48
//FNT_elementarygothicbookhand32
//bookmanoldstyle24
//haettenscheweiler24
//monotypecorsiva24
//bookantiquabold24
               tmptext.format ( 640, tmpfnt  );

               char *tmpstr = tmptext.readline_start();
               int tmpy = 0;// text_height ( tmpfnt);

               while ( tmpstr != NULL)
               {
                  tmpy += text_height ( tmpfnt);
                  textout_justify_old ( buffer, tmpfnt, tmpstr, 0, 640, tmpy, 256,General_COLOR_TEXT);
                  tmpstr = tmptext.readline_next();
               }



               error = tmptext.SQLstep();
            }

            tmptext.SQLfinalize();
         }
      }*/
/*
//      WinList wlst_temp ( lst_temp, 0, 00 );
//      Window::show_all();

      copy_buffer();

      mainloop_readkeyboard();
      mainloop_readkeyboard();*/

//----------------------------------------------------------------

      //printf ("Main: Pass 1\n" );
/*
      ImageList imglst_test ( "Select a Picture", 4, 3,128, 128, true );

     // printf ("Main: Pass 2\n" );
      //for ( i = 2048 ; i < 2248 ; i++ )
      imglst_test.add_datref ( datref_portrait );

      //printf ("Main: Pass 3\n" );

      //answer = imglst_test.show ( 0, 0 );
      WinImageList wimglst_test ( imglst_test, 100, 10 );

      answer = Window::show_all();

      //printf ("Main: Pass 4\n" );

      printf ("Answer was %d\n", answer );
*/
     // for ( i = 0; i < 32; i++ )
       //  build_texture_palette (testpal[i], i);

      /*maze.load_hardcoded();

      party.position ( 0, 0, 0, Party_FACE_NORTH );
      maze.start( true );*/

      /*TexPalette floor;
      BITMAP* floorbmp;


      floor.texcode ( 0, 2048 );
      floor.texposition ( 0, TexPalette_TILING_BOTH + TexPalette_ZOOM_X2, 0, 0 );
      floor.texcode ( 1, 1027 );
      floor.texposition (1, TexPalette_TILING_BOTH + TexPalette_ZOOM_X4, 8, 4 );


      floorbmp = floor.build();


      blit ( floorbmp, screen, 0, 0, 0, 0, floorbmp->w, floorbmp->h );

      while ( mainloop_readkeyboard() != KEY_ENTER );*/


   }

   /*Syslog systemlog;

   for ( i = 0 ; i < 10 ; i++)
      systemlog.writef ( "Message number: %d", i );

   for ( i = 0; i <5; i++ )
      printf ("%d: %s\n", i, systemlog.read(i));

   while ( mainloop_readkeyboard() != KEY_ENTER );
*/

   //Event tmpevent;
   //tmpevent.start();
   /*answer = SQLopen ( "adventure//template//database.sqlite");

   //tmpevent.start();

  SQLerrormsg_active = true;

  if ( answer == SQLITE_OK)
  {
     printf ("before select\n");
     answer = tmpevent.SQLselect ( 1 );

     if ( answer == SQLITE_ROW )
     {
        printf ("query works\n");
        tmpevent.start ();
     }
  }
  else
  {


     printf ("could not open database\n error=%d", answer);
  }*/

  //}
/*   if ( answer == SQLITE_OK)
   {
      List lst_monster ("Select an monster", 8);

      i = 0;

      error = tmpmonster.SQLprepare("");
      printf ("\r\npass 0");
      if ( error == SQLITE_OK )
      {
         printf ("\r\npass 1");
         error = tmpmonster.SQLstep();
         tmpmonster.SQLerrormsg();

         while ( error == SQLITE_ROW )
         {
            printf ("\r\npass 2");


            lst_monster.add_itemf ( 0, "name=%s, level=%d, size=%d, AD=%d clumsy=%d like_front=%d, ",
                                tmpmonster.name(),
                                tmpmonster.level(),
                                tmpmonster.size(),
                                tmpmonster.AD(),
                                tmpmonster.clumsiness(),
                                tmpmonster.like_front () );

*/
            /*printf ( "name=%s, price=%d, type=%d, category=%d, dmgz=%d AD=%d\r\n",
                                tmpitem.name(), tmpitem.price(), tmpitem.type(), tmpitem.category(),
                                tmpitem.attribute(), tmpitem.xyzstat(XYZSTAT_DMG, XYZMOD_ZEQUIP),
                    tmpitem.d20stat(D20STAT_AD));*/

/*


            error =  tmpmonster.SQLstep();
         }
      }

      tmpmonster.SQLfinalize();

      WinList wlst_monster ( lst_monster, 20, 50);

      Window::show_all();


   }

*/
/*   printf ("- closing SQL Database\r\n");
   SQLclose ();
   printf (" after SQL close\n");
*/

}



// This is a procedure used for converting bitmap for datafile processing
// but it is not used by the game once released
void tmpmain_convert_bitmap ( void )
{
   DATAFILE *tmpdatf;
   int init_status = init_game ();
   int i = 0;
   bool filecomplete = false;
   int x = 18;
   int answer;
   BITMAP *tmpcopy = create_bitmap ( 144, 144 );
   BITMAP *tmpstretch = create_bitmap (128, 128);
   PALETTE pal;
   char tmpstr [100] = "";

   if ( init_status == INIT_SUCCESS )
   {
      tmpdatf = load_datafile ("tmpwiz8portrait.dat");

      while ( filecomplete == false)
      {


         clear_to_color ( screen, makecol (0, 0, 0));
         rectfill ( screen, 0, 0, 200, 328, makecol (200, 200, 200));

         draw_sprite ( screen, (BITMAP*) tmpdatf [ i ] . dat, 10, 10 );
         rect ( screen, x+10, 10, x + 154, 154, makecol ( 255, 255, 255));

         answer = mainloop_readkeyboard();

         switch ( answer )
         {
            case KEY_ENTER:

               blit ( (BITMAP*) tmpdatf [ i ] . dat, tmpcopy,
                     x, 0, 0, 0, 144, 144 );
               stretch_sprite ( tmpstretch, tmpcopy, 0, 0, 128, 128 );


               //draw_sprite ( screen, tmpstretch, 10, 200 );

               sprintf ( tmpstr, "output//portrait%03d.bmp", i);
               save_bmp ( tmpstr, tmpstretch, pal);



               //mainloop_readkeyboard();

               i++;
               x=18;

            break;
            case KEY_LEFT:
               if ( x > 0)
                 x--;
            break;
            case KEY_RIGHT:
               if ( x < 35)
               x++;
         }


         if ( tmpdatf [ i ] . type == DAT_ID('i','n','f','o') )
         {
            filecomplete = true;
         }

      }

      unload_datafile (tmpdatf);
   }
}

void recordset_test_main ( void )
{
   //s_SQLrecordset rs;
   //int sqlerror;

   printf ("Test method has been called\n");

   SQLopen ("adventure/DebuggingDemo/database.sqlite" );

   //SQLrs_create ( &rs, SQLdb, "SELECT * FROM monster_category");
   //SQLrs_createf ( &rs, SQLdb, "SELECT %c FROM %s WHERE pk=%d ORDER BY %s"
   //   , '*', "monster_category", 22 , "name" );

  // SQLrs_print ( rs );

  // SQLrs_free( rs );

   if ( SQLpreparef( "SELECT %s,%s FROM %s" , "name", "pictureID", "monster_category" )
        == SQLITE_OK )
   {
      while ( SQLstep() == SQLITE_ROW)
      {
         printf ("%s: %d\n", SQLcolumn_text(0), SQLcolumn_int(1));
      }
   }

   SQLclose (  );
}
