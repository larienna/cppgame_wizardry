/***************************************************************************/
/*                                                                         */
/*               T H E    W I Z A R D R Y    P R O J E C T                 */
/*                                                                         */
/*     Content : Main Programm                                             */
/*     Starting Date : March 14th, 2002                                    */
/*     Programmer : Eric Pietrocupo                                        */
/*     License : GNU General Public License                                */
/*                                                                         */
/*                                                                         */
/*          This game project consist to make a game which is close to     */
/*     the original game called Wizardry. The game will not look           */
/*     exactly as the original, but it will have the same look and feel.   */
/*                                                                         */
/***************************************************************************/

//include groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

#include <init.h>
#include <screen.h>
//#include <setup.h>
#include <wdatproc.h>
#include <test.h>

// Included headers
/*
#include <allegro.h>

//#include <time.h>

//#include <string.h>
#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
#include <init.h>
//#include <menu.h>
//#include <option.h>
#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>

//
//
//
//
//
//
//#include <list.h>
#include <opponent.h>
#include <charactr.h>
#include <ennemy.h>
#include <party.h>

#include <game.h>
#include <city.h>
#include <encountr.h>
#include <maze.h>
#include <editor.h>
//
#include <camp.h>
#include <config.h>
//#include <draw.h>
//#include <dialog.h>
#include <combat.h>
#include <setup.h>
#include <manager.h>

#include <menu.h>
//#include <listwin.h>
#include <window.h>
#include <winmenu.h>
//#include <winlist.h>
//#include <winempty.h>
//#include <wintitle.h>
//#include <windata.h>
//#include <wdatproc.h>
//#include <winmessa.h>
//#include <winquest.h>
//#include <wininput.h>
//
*/


// --- Prototypes ---

void wizardry_main ( int nb_param, char *param[] );

// driver selection

/*   BEGIN_COLOR_DEPTH_LIST
      COLOR_DEPTH_8
      COLOR_DEPTH_15
      COLOR_DEPTH_16
   END_COLOR_DEPTH_LIST*/

// Main programm

int main ( int nb_param, char *param[] )
{

   //recordset_test_main ( );

   wizardry_main ( nb_param, param );

   return ( 0 ); // does not return any success codes
}
END_OF_MAIN()


/** Contains the main method of the wizardry game.
* allow branching between game main and test mains */
void wizardry_main ( int nb_param, char *param[] )
{
   int init_status;
   int title_value = SCREEN_TITLE_START;
   int game_status = System_GAME_STOP;
   //int city_value;
   //int maze_value;
   //int player_value;
   //int account_value;
   //int manager_value;
   //int encount_value;
   //int combat_value;
   //int camp_value;
   //City city; // city engine
   //Camp camp; // camp engine
   //Party party;
   Manager manager;
   //Combat combat; // combat engine
   Editor editor;
   //Encounter encounter;
   short i;
   //int param_code;
   //int music_selected;
   bool continue_game = false;
   int return_from=0;
   //Account tmpacc;
   //Player tmplayer;
//   int partytag;

//   sbrk ( 1400000 );
//   s_Maze_tile test [ 100 ] [ 100 ] [ 10 ];
//   BITMAP *test = create_bitmap ( 640, 480 );


   //param_code = decode_parameter ( nb_param, param );

/*   __dpmi_free_mem_info meminfo;
   __dpmi_get_free_memory_information( &meminfo );

   printf("Largest Block       : %12d Bytes\n", meminfo.largest_available_free_block_in_bytes );
   printf("Max Unlock Pages    : %10d Pages\n", meminfo.maximum_unlocked_page_allocation_in_pages );
   printf("Max Locked Pages    : %10d Pages\n", meminfo.maximum_locked_page_allocation_in_pages );
   printf("Linear Add. Space   : %10d Pages\n", meminfo.linear_address_space_size_in_pages );
   printf("Nb Unlocked Pages   : %10d Pages\n", meminfo.total_number_of_unlocked_pages );
   printf("Nb Free Pages       : %10d Pages\n", meminfo.total_number_of_free_pages );
   printf("Nb Physical Pages   : %10d Pages\n", meminfo.total_number_of_physical_pages );
   printf("Free Lin.add. space : %10d Pages\n", meminfo.free_linear_address_space_in_pages );
   printf("Paging File size    : %10d Pages\n", meminfo.size_of_paging_file_partition_in_pages );

   printf("Physical Memory    : %10d Bytes\n",
      _go32_dpmi_remaining_physical_memory()  );
   printf("Virtual Memory     : %12d Bytes\n",
      _go32_dpmi_remaining_virtual_memory()  );
   printf("sbrk(0)            : %10d ??\n",
      sbrk(0) );


   while ( kbhit() == 0 );*/

 /*  if ( param_code == Init_PARAM_SETUP )
   {
      init_status = init_setup ();
      if ( init_status == INIT_SUCCESS )
         setup_main();
      exit_setup();
   }
   else
     if ( param_code == Init_PARAM_EDITOR )
     {
//        Editor *edit = new Editor;
        init_status = init_editor ();
        if ( init_status == INIT_SUCCESS );
        {
//           editor.load_maz_file ("map000.maz");
           editor.start();
//           editor.save_maz_file ( "map000.maz" );
      //?? to do , more complex logic for maze testing
        }

        exit_editor();
//        delete edit;
     }
   else
   {*/
//      maze = new Maze;


   //SQLactivate_errormsg(); // activate for debugging new DB modifications

      init_status = init_game ();
//      config.verify_if_default();
//      printf ("main:after init_game\n");

      //select random music for the session
      for ( i = 0 ; i < Config_NB_MUSIC_TABLE ; i++)
      {
         Config_rndmusic [ i ] = rnd (3) - 1;
         //printf ("Debug:Main:Musictable i=%d, rnd=%d\n", i, Config_rndmusic [ i ]);
      }
      //music_selected = rnd ( 2 );

 //     config.verify_if_default();
 //     printf ("main:after random music\n");

      if ( init_status == INIT_SUCCESS )
      {
    //     printf ("main:before copyright\n");
 //        config.verify_if_default();
 //        printf ("main:before\n");
         SQLactivate_errormsg();
         show_copyright_screen ();
 //        config.verify_if_default();

 //        printf ("main:after copyright\n");

         while ( title_value != SCREEN_TITLE_END_GAME )
         {

            if ( title_value != SCREEN_TITLE_OPTION )
               play_music_table ( Config_MUSICTAB_INTRO );
//printf ("main:after play music\n");
            /*   if ( music_selected == 1 )
                  play_music_track ( System_MUSIC_w1intro );
               else
                  play_music_track ( System_MUSIC_w3intro );*/

            title_value = show_title_screen ();

 //           config.verify_if_default();
 //     printf ("main:after title screen\n");
            switch ( title_value )
            {
               case SCREEN_TITLE_OPTION :
                  config.start();
               break;

               case SCREEN_TITLE_GALLERY :
                  show_art_gallery ();
               break;

               case SCREEN_TITLE_EDITOR :
                  editor.start();
               break;

               /*case SCREEN_TITLE_SETUP:
                  show_system_setup();
               break;*/


               case SCREEN_TITLE_SOURCES:
                  show_sources();
               break;

               case SCREEN_TITLE_CONTINUE:
                  continue_game = true;
               case SCREEN_TITLE_START :

                  //manager_value = Manager_CONTINUE;
                  game_status = System_GAME_STOP;

                  while ( game_status != System_GAME_EXIT )
                  {
                     if ( continue_game == true )
                     {
                        //printf ("Main: Before continue game\n");
                        game_status = manager.start_last_game();
                        continue_game = false;
                        //printf ("continuing a game\r\n");
                     }
                     else
                        game_status = manager.start();

                    //printf ("Main debug: after manager start\n");

                     //printf ("GameStatus = %d\r\n", game_status);

                     return_from = Party_STATUS_UNDEFINED;

                     //printf ("Debug: branch status = %d\n", game_status);
                     //printf ("Debug: party status = %d\n", party.status());
                     while ( game_status == System_GAME_START )
                     {

                        //printf("main: party status=%d\n", party.status());
                        switch (party.status())
                        {
                           case Party_STATUS_CITY:
                              game_status = city.start();
                              return_from = Party_STATUS_CITY;
                           break;

                           case Party_STATUS_MAZE:
                           //printf ("Main: pass 1\r\n");
                           //printf ("Debug: Before start maze\n");
                              maze.start();
                              return_from = Party_STATUS_MAZE;
                           break;

                           case Party_STATUS_CAMP:
                              if ( return_from == Party_STATUS_CITY)
                                 camp.start( true );
                              else
                                 game_status = camp.start();
                              return_from = Party_STATUS_CAMP;
                           break;

                           case Party_STATUS_ENCOUNTER:
                             // printf ("Debug: Main: Starting Encounter\n");
                              encount.start();
                              return_from = Party_STATUS_ENCOUNTER;
                           break;

                           case Party_STATUS_COMBAT:
                              //printf ("Debug: Main: Starting Combat\n");
                              combat.start();
                              return_from = Party_STATUS_COMBAT;
                           break;

                           case Party_STATUS_ENDGAME:
      //printf ("Main:pass in endgame\n");
                              manager.show_ending();
                              party.status ( Party_STATUS_CITY);
                              return_from = Party_STATUS_ENDGAME;
                           break;
                           default:
                              game_status = System_GAME_STOP;
                              printf ("\r\nError: Party status is invalid, does not know where to send party.");
                              printf ("\r\nForce closing game");

                           break;
                        }

                        if ( game_status == System_GAME_STOP)
                           manager.close_game();
                     }

                  }
               break;
            }
         }
      }
      exit_game ();
}

//------------------ old code ------------------------
                        /*
                        //     party.SQLselect ( tmplayer.selected_party() );

                        city_value = City_CONTINUE;

                        while ( city_value != City_END_GAME )
                        {
                                       //city_value = City_WARP_TO_MAZE; //temp

                           //party.SQLselect ( tmplayer.selected_party() );

                           if ( party.status() == Party_STATUS_CITY )
                           {
                                          //debug ( "before city ");
                              //city.SQLselect ( party.location() );
                              city_value = city.start ();
                                          //debug ( "Exit city if");
                           }
                           //party.SQLselect ( tmplayer.selected_party() );


                                       // warp to another City
                                       if ( party.status() == Party_STATUS_CITY
                                          && city_value == City_WARP_TO_CITY )
                                       {
                                          debug ( "Enter Warp to city if");
                                          party.SQLselect ( tmplayer.selected_party() );
                                          city_value = City_CONTINUE;
                                       }

                                       // warp to maze
                           printf ("debug: partystatus= %d\r\n", party.status());
                           //if ( party.status() == Party_STATUS_MAZE )
                           if (city_value == City_ENTER_MAZE)
                           {
                              printf("debug: enter party status loop\r\n");
//                                          party.status ( Party_STATUS_MAZE );
//                                          party.SQLupdate();
//                                          debug ( "Enter warp to maze if" );
                             // maze.SQLselect ( party.location() );
                              maze_value = Maze_CONTINUE;

                                          //temp
                                          //maze.load();
                              maze.load_hardcoded();



                              s_Party_position tmpos = party.position ();


                              //encount.clear_list();
                                          // temp
                                          //encount.build_list ( maze.ebase(), maze.erange() );
                                          //encount.probability ( maze.eprob() );


                              while ( maze_value != Maze_EXIT )
                              {
                        //         printf ("Debug: before maze start\r\n");
                                 maze_value = maze.start( party.primary_key() );

                                 if ( maze_value == Maze_CAMP )
                                 {
                                    camp_value = camp.start( party.primary_key() );

                                    if ( camp_value == Camp_QUITPARTY )
                                    {
                                       maze_value = Maze_EXIT;
                                       city_value = City_END_GAME;
                                    }
                                 }
                                 if ( maze_value >= Maze_1ST_EXIT &&
                                    maze_value <= Maze_8TH_EXIT )
                                 {
                                    //party.location ( maze.exitwarptag ( maze_value ) );
                                    party.status ( Party_STATUS_CITY );
                                    party.SQLupdate();
                                    city_value = City_ENTER_CITY;
                                    maze_value = Maze_EXIT;
                                 }

                                 if ( maze_value == Maze_COMBAT )
                                 {
                                    encount_value = encount.start( party.primary_key() );

                                    if ( encount_value == Encounter_ENGAGE )
                                    {
                                       combat.add_character_party ( party.primary_key() );
                                       combat.add_ennemy_party ( encount.get_ennemy_party() );

                                       combat_value = combat.start();

                                       if ( combat_value == Combat_DEFEATED )
                                       {
                                          maze_value = Maze_EXIT;
                                          city_value = City_END_GAME;
//                                        tmplayer.disband_party ( tmplayer.selected_partyID() );

                                       }

                                       encount.clear_objects();
                                       encount.clear_selection();
                                    }
                                                //?? combat add parties and call start

                                                // to change = set value according to encounter
                                 }
                              }
                           }
                        }
                        //--------------------------- end old code -------------------------
                        */



