/***************************************************************************/
/*                                                                         */
/*                          S C R E E N . C P P                            */
/*                                                                         */
/*     Content : Game screen Management procedures                         */
/*     Programmer : Eric PIetrocupo                                        */
/*     Stating Date : May 12nd, 2002                                       */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                               Includes                                -*/
/*-------------------------------------------------------------------------*/

//include groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

#include <wdatproc.h>
#include <screen.h>

/*
#include <allegro.h>


#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
//#include <option.h>
#include <screen.h>
//#include <dbdata.h> //??-- to remove start
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h> //?? to remove end



//
//
//
//
//
#include <listwiz.h>

#include <opponent.h>
#include <charactr.h>
#include <ennemy.h>
#include <party.h>

#include <game.h>

//#include <city.h>
#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winempty.h>
#include <wintitle.h>
#include <winmenu.h>
#include <winlist.h>
#include <wdatproc.h>
#include <windata.h>
#include <winmessa.h>
//#include <sinterfc.h>
//#include <siproc.h>
*/

BITMAP* no_picture;

/*-------------------------------------------------------------------------*/
/*-                        Procedures                                     -*/
/*-------------------------------------------------------------------------*/

void show_copyright_screen ( void )
{
   BITMAP *tmpbuf = create_bitmap ( SCREEN_W, SCREEN_H );
   BITMAP *subtmpbuf = create_sub_bitmap ( tmpbuf, System_X_OFFSET, System_Y_OFFSET, 640, 480);

   clear_bitmap ( tmpbuf );

   blit ( subscreen, subbuffer, 0, 0, 0, 0, SCREEN_W, SCREEN_H );

// Show Allegro

   blit ( datref_image [1], subtmpbuf, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest ( Screen_LOGO_WAIT_TIME );
   clear_bitmap ( tmpbuf );

// show company screen

   //textout_centre_old ( tmpbuf, FNT_print,
   //   "Contains copyrighted material from", 320, 0, General_COLOR_TEXT );

   //show_transition_algo7 ( subbuffer, tmpbuf, 10 );
   //rest ( Screen_LOGO_WAIT_TIME );

   blit ( datref_image[5], subtmpbuf, 0, 0, 220, 140, 200, 200 );
   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest ( Screen_LOGO_WAIT_TIME );

   /*blit ( BMP_company001, tmpbuf, 0, 0, 220, 140, 200, 200 );
   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest ( Screen_LOGO_WAIT_TIME );

   blit ( BMP_company002, tmpbuf, 0, 0, 220, 140, 200, 200 );
   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest ( Screen_LOGO_WAIT_TIME );

   blit ( BMP_company003, tmpbuf, 0, 0, 220, 140, 200, 200 );
   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest ( Screen_LOGO_WAIT_TIME );

   blit ( BMP_company004, tmpbuf, 0, 0, 220, 140, 200, 200 );
   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest ( Screen_LOGO_WAIT_TIME );

   blit ( BMP_company005, tmpbuf, 0, 0, 220, 140, 200, 200 );
   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest ( Screen_LOGO_WAIT_TIME );*/



/*   stretch_sprite ( buffer, BMP_company, 0, 0, 640, 480 );
   textout_centre_old ( buffer, FNT_print,
      "This game contains copyrighted material from these companies",
      320, 0, General_COLOR_TEXT );
   copy_buffer ();
   rest (  500 );*/

// show copyright screen
   clear_bitmap ( tmpbuf );

   textout_centre_old ( subtmpbuf, FNT_print, "Wizardry",
      320 , 100, General_COLOR_TEXT );
   textout_centre_old ( subtmpbuf, FNT_print,
      "is a copyright of Andrew Greenberg, Robert Woodhead and",
          320, 116, General_COLOR_TEXT );

   textout_centre_old ( subtmpbuf, FNT_print,
      "Sir-Tech Software.",
         320, 132, General_COLOR_TEXT );

   textout_centre_old ( subtmpbuf, FNT_print,
      "See sources for additional details.",
      320 , 148 , General_COLOR_TEXT );

   /*textout_centre_old ( tmpbuf, FNT_print,
      "Additional images and sound has been extracted from" ,
      320, 180, General_COLOR_TEXT );*/

   /*textout_centre_old ( tmpbuf, FNT_print,
      "the Hexen and Heretic computer game",
      320, 196, General_COLOR_TEXT );*/

   textout_centre_old ( subtmpbuf, FNT_print,
      "Wizardry Legacy",
      320 , 228 , General_COLOR_TEXT );

   textout_centre_old ( subtmpbuf, FNT_print,
      "is a free software, published under the GNU General Public License,",
      320 , 244 , General_COLOR_TEXT );

   textout_centre_old ( subtmpbuf, FNT_print,
      "inspired from the original Wizardry games,",
      320 , 260, General_COLOR_TEXT );

   textout_centre_old ( subtmpbuf, FNT_print,
      "that exist only for the fun of making and playing the game.",
      320 , 276, General_COLOR_TEXT );

   /*textout_centre_old ( tmpbuf, FNT_print,
      "Wizardry Legacy",
      320, 308, General_COLOR_TEXT );

   textout_centre_old ( tmpbuf, FNT_print,
      "exist only for the fun of making and playing the game,",
      320, 324, General_COLOR_TEXT );

   textout_centre_old ( tmpbuf, FNT_print,
      "this is why it should never be used for any kind of profit",
      320, 340, General_COLOR_TEXT );*/

   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest ( Screen_COPYRIGHT_WAIT_TIME );

   clear_bitmap ( tmpbuf );

   show_transition_algo7 ( buffer, tmpbuf, 10 );
   rest( 500 );
   destroy_bitmap ( tmpbuf );


//-------------------------------- Code Testing ---------------------
        /*
   short i;
   Item tmpitem;
   int tmptag;

   unsigned int index = db.search_table_entry ( DBTABLE_ITEM );
   char tmpstr [ 100 ];

   // random value for eachitem types : Weap, shld, armr, accs, exp, other
   int rndval [ 6 ] = { 4, 5, 2, 8, 20, 1 };
   int min [ 6 ] = { 2, 3, 1, 4, 5, 1 };
   int tmptype;
   short tmpvar;

   i = 0;
 //  j = 0;

   // the list of items will regenerate itself at each call.
   // make a sub function to generate
   while ( db.entry_table_tag ( index ) == DBTABLE_ITEM )
   {
      tmpitem.SQLselect ( index );
      tmptag = tmpitem.tag();
      if ( ( tmpitem.type() == Item_TYPE_EXPANDABLE )
         && tmptag.source() != DBSOURCE_SAVEGAME )
      {
//         p_item [ i ] . itemtag = tmpitem.tag();
//         p_item [ i ] . type = tmpitem.type();
         tmptype = tmpitem.type();
//         p_item [ i ] . quantity = 5;
         sprintf ( tmpstr, "%s W:%f S:%d R:%d P:%d", tmpitem.name(), tmpitem.weight(),
             tmpitem.status(), tmpitem.rarity(), tmpitem.price() );
         debug ( tmpstr );
         tmpvar = min [ tmptype ]+rnd( rndval [ tmptype ] );
         i++;
      }
      index++;
   }
      */

   //?? testing Super Interfaces

/*   SICommand ( "This is a test menu", SICMDITEM_TEST, 50, 50 );*/

   //?? testing adventure

//   clear_bitmap ( subscreen );
//   Adventure testadv;

//   testadv.show_intro  ();
//   testadv.show_ending ();

   //?? db tes ---

//   textout_centre_old( subbuffer, FNT_print,
//      "This is a database test", 320, 0, General_COLOR_TEXT );

/*   Ennemy tmpenn;
   unsigned int index = db.search_table_entry ( DBTABLE_MONSTER );
   short i = 0;
   bool accept_ennemy;
   short y = 0;


   short nb_ennemy = 0;

   do
   {
      tmpenn.alDBselect ( Ennemy_DATATYPE_MONSTER, index );
      accept_ennemy = false;

      if ( ( tmpenn.level() >= 1 )
         && ( tmpenn.level() <= 3 ) )
         accept_ennemy = true;

      //?? use mask to block additional monsters

      if ( accept_ennemy == true )
      {
         textprintf_old ( subbuffer, FNT_print, 20, y, General_COLOR_TEXT,
            "- %s | %s | level %2d | size %d |",
         tmpenn.cname(), tmpenn.cgroup_name(), tmpenn.level(), tmpenn.size() );
         i++;
         y+=16;
         nb_ennemy++;
      }
      index++;
   }
   while ( db.entry_table_tag ( index ) == DBTABLE_MONSTER );

   copy_buffer();
   while ( ( readkey()>>8) != KEY_ENTER );*/


}

int show_title_screen ( void )
{
   Menu menu_title ("");
   short answer = 0;
   int tmpvar;
   int i;
   int j;

//printf ("Show_Title_Screen: Before menu Creation\n");
   menu_title.add_item (SCREEN_TITLE_START, "Start Game");
   menu_title.add_item (SCREEN_TITLE_CONTINUE, "Continue Game" );
   menu_title.add_item (SCREEN_TITLE_GALLERY, "Art Gallery" );
   menu_title.add_item (SCREEN_TITLE_OPTION, "Default Options");
   menu_title.add_item (SCREEN_TITLE_EDITOR, "Editor");
   //menu_title.add_item (SCREEN_TITLE_SETUP, "System Setup" );
   menu_title.add_item (SCREEN_TITLE_SOURCES, "Sources" );
   menu_title.add_item (SCREEN_TITLE_END_GAME, "Exit Wizardry" );

//printf ("Show_Title_Screen: Before adding menu to window\n");
   WinMenu wmnu_title ( menu_title, 240, 316, true, true );

   //tmpvar = BMP_DRAGON01 - 1;
   tmpvar = rnd ( 3 ) + 1;

   //blit ( ( BITMAP* ) datfimage [ tmpvar ] . dat, subbuffer, 0, 0, 0, 0, 640, 480 );

//printf ("Show_Title_Screen: Before drawing background\n");
   for ( i = 0 ; i < 640 ; i += 128)
      for ( j = 0 ; j < 480; j += 128)
         draw_sprite ( subbuffer, datref_image [ 1024 ], i, j );

   draw_sprite ( subbuffer, datref_image [ tmpvar ]
      , 95, 15 );
   draw_sprite ( subbuffer, datref_image [6], 160, 80 );
//   draw_border ( 0, 0, 639 , 479, makecol ( 255, 180, 180 ) );
   Window::instruction ( 320, 455 );

//printf ("Show_Title_Screen: Before drawing text\n");
   text_mode_old ( -1 );
   textout_centre_old ( subbuffer, FNT_elgar32,
   "Welcome in the world of Wizardry", 320 , 10 , makecol ( 255, 225, 50 ) );

   textprintf_old ( subbuffer, FNT_elgar32, 10 , 425 , General_COLOR_TEXT,
      "Version %s", VERSION );
   textprintf_old ( subbuffer, FNT_elgar32, 10 , 451 , General_COLOR_TEXT,
      "%s", YEAR );
   text_mode_old ( -1 );

//   copy_buffer ();

//   draw_border_fill ( 234, 344, 396, 436, General_COLOR_BORDER,
//                                                        General_COLOR_FILL );
//   menu_title.show ( 240, 250 );
//printf ("Show_Title_Screen: Before show_all\n");
     answer = Window::show_all();
//printf ("Show_Title_Screen: After show_all\n");
//   answer = menu_title.show ( 240, 350, answer, true );

//   make_screen_shot ( "titlshot.bmp" );


//   copy_buffer ();

//   stop_midi ();

//   copy_buffer ();
//   while ( (readkey() >> 8 ) != KEY_ENTER );


   if ( answer == -1 )
      return ( SCREEN_TITLE_END_GAME );
   else
      return ( answer );

}

void show_loading_screen ( const char* title, short percentage )
{
   //?? note percentage is now 500%
   int height = text_height ( FNT_blackchancery24 );
   short y;

   y = ( height * 3 ) + 12 + 16;
   //?? replace by WinData
//   WinEmpty wemp_border ( 19, 200, 610, y );
//   draw_border_fill ( 19 , 200, 629 , y , General_COLOR_BORDER,
//      General_COLOR_FILL );

   y = 200 + 6 + System_Y_OFFSET;
   textout_centre_old ( subscreen, FNT_blackchancery24, "Entering", 320 , y,
                                                        General_COLOR_TEXT );

   y = y + height;
   textout_centre_old ( subscreen, FNT_ambrosia24, title, 320 , y, General_COLOR_TEXT );

   y = y + height;

   if ( percentage == 0 )
      percentage = 1;

   y = y + 2;
   rectfill ( subscreen, 69, y, 69 + ( percentage /** 5*/ ), y + 10,
                                                 makecol ( 150, 150, 255 ) );
   rect ( subscreen, 69 , y, 569 , y + 10, makecol ( 200, 200, 255 ) );

   y = y + 14;
   textout_centre_old ( subscreen, FNT_freehand16, "Loading", 320 , y, General_COLOR_TEXT );

//   copy_buffer ();
}

void show_art_gallery ( void )
{
   //List lst_monster ("Ennemy List", 20, true );
   //List lst_music ("Music List", 20 );
   //List lst_texset ("Texture Set", 20, true );
   //List lst_masktex ("Masked Textures", 20, true );
   //List lst_object ("Object Image", 20, true );
   //List lst_sound ("Sound List", 20 );

   //List lst_monster2 ("Monster list", 20, true);
   Menu mnu_gallery ("Art Gallery" );
//   WinList* ptr_wlst;
//   WinData<BITMAP> *ptr_wdat;
   BITMAP *ptr_bmp;
   BITMAP *ptr_bmp2;
   short answer1 = 0;
   //short answer2 = 0;
   //short i;
//   string tmpstr;
   //char tmpstr [ 81 ];

   stop_midi ();

   ptr_bmp = create_bitmap ( 256, 256 );
   ptr_bmp2 = create_bitmap ( 128, 128 );

   // list and menu initialisation

   mnu_gallery.add_item (Screen_GALLERY_TEXTURE,  "Textures   ", false );
   mnu_gallery.add_item (Screen_GALLERY_OBJECT,   "Maze Objects   ", false );
   mnu_gallery.add_item (Screen_GALLERY_MUSIC,    "Musics         ", false );
   mnu_gallery.add_item (Screen_GALLERY_SOUND,    "Sounds         ", false );
   mnu_gallery.add_item (Screen_GALLERY_FONT,     "Font           ", false );
   mnu_gallery.add_item (Screen_GALLERY_MONSTER,  "Monsters       ", false );
   mnu_gallery.add_item (Screen_GALLERY_IMAGE,    "Various Images ", false );
   mnu_gallery.add_item (Screen_GALLERY_SKY,      "Sky            ", false );
   mnu_gallery.add_item (Screen_GALLERY_EXIT,     "Exit           ", false );

   //for ( i = 0 ; i < 192  ; i++ )
     // lst_monster.add_item (i, ENNEMY_LIST [ i ].name );

   /*for ( i = 0 ; i < Maze_NB_GAME_TEXSET ; i++ )
   {
      sprintf ( tmpstr, "Texture Set %3d", i );
      lst_texset.add_item (i, tmpstr );
   }

   for ( i = 0 ; i < Maze_NB_GAME_MASKTEX ; i++ )
   {
      sprintf ( tmpstr, "Texture %3d", i );
      lst_masktex.add_item (i, tmpstr );
   }

   for ( i = 0 ; i < Maze_NB_GAME_OBJIMG ; i++ )
   {
      sprintf ( tmpstr, "Object %3d", i );
      lst_object.add_item (i, tmpstr );
   }*/

   /*for ( i = 0 ; i < System_NB_MUSIC ; i++ )
      lst_music.add_item (i, MUSIC_LIST [ i ] . name );

   for ( i = 0 ; i < 37 ; i++ )
   {
      sprintf ( tmpstr, "Sound %3d", i );
      lst_sound.add_item (i, tmpstr );
   }*/

  /* for ( i = 1024 ; i < 1276; i++)
   {
      //need to query database??
      lst_monster2.add_itemf ( i, "Monster %3d", i);
   }*/

   //lst_monster2.add_query ( "name" , "monster", "");


   WinEmpty wemp_background;
//   wemp_background.preshow ();
   WinTitle wttl_title ( "Art Gallery" );
//   wttl_title.preshow ();
   WinMenu wmnu_gallery ( mnu_gallery, 20, 40, true );

   Window::instruction ( 320, 460 );

   while ( answer1 != Screen_GALLERY_EXIT )
   {
      wmnu_gallery.unhide();
      answer1 = Window::show_all();
      wmnu_gallery.hide();

      switch ( answer1 )
      {
         case Screen_GALLERY_TEXTURE:
            show_art_gallery_texture ();
         break;
         case Screen_GALLERY_IMAGE:
            show_art_gallery_image ();
         break;
         case Screen_GALLERY_MONSTER :
            show_art_gallery_monster();
         break;
         case Screen_GALLERY_FONT :
            show_art_gallery_font ();
         break;
         case Screen_GALLERY_SKY:
            show_art_gallery_sky ();
         break;
         case Screen_GALLERY_MUSIC :
            show_art_gallery_music();
         break;
         case Screen_GALLERY_SOUND :
            show_art_gallery_sound();
         break;
         case Screen_GALLERY_OBJECT :
            show_art_gallery_object();
         break;

         /*case Screen_GALLERY_TEXTURE :
            ptr_wdat = new WinData<BITMAP> ( WDatProc_gallery_bitmap,
               *ptr_bmp, WDatProc_POSITION_GALLERY_BITMAP );
            ptr_wlst = new WinList ( lst_texset, 20, 40 );
            answer2 = 0;

            while ( answer2 != -1 )
            {

               clear_bitmap ( ptr_bmp );
               draw_sprite ( ptr_bmp,
                  ( BITMAP* ) datfmaze [ BMP_TEXSET000 + ( answer2 * 4 ) ] . dat
                  , 0, 0 );
               draw_sprite ( ptr_bmp,
                  ( BITMAP* ) datfmaze [ BMP_TEXSET000 + ( answer2 * 4 ) + 1 ] . dat
                  , 128, 0 );
               draw_sprite ( ptr_bmp,
                  ( BITMAP* ) datfmaze [ BMP_TEXSET000 + ( answer2 * 4 ) + 2 ] . dat
                  , 0, 128 );
               draw_sprite ( ptr_bmp,
                  ( BITMAP* ) datfmaze [ BMP_TEXSET000 + ( answer2 * 4 ) + 3 ] . dat
                  , 128, 128 );
               Window::refresh_all();
               answer2 = Window::show_all();
//               make_screen_shot ( "gallshot.bmp");
//               ptr_wdat->parameter (
//                  *(( BITMAP* ) datf [ BMP_MASKTEX000 + answer2 ] . dat ));
            }
            delete ptr_wlst;
            delete ptr_wdat;
         break;*/

         /*case Screen_GALLERY_MASKTEX :
//            ptr_bmp2 = create_bitmap ( 128, 128 );
            clear_bitmap ( ptr_bmp2 );
            draw_sprite ( ptr_bmp2, ( BITMAP* ) datfmaze [ BMP_MASKTEX000 ] . dat, 0, 0 );
            ptr_wdat = new WinData<BITMAP> ( WDatProc_gallery_bitmap, *ptr_bmp2
               // *(( BITMAP* ) datfmaze [ BMP_MASKTEX000 ] . dat )
               , WDatProc_POSITION_GALLERY_BITMAP );
            ptr_wlst = new WinList ( lst_masktex, 20, 40/ );
            answer2 = 0;
            while ( answer2 != -1 )
            {
               answer2 = Window::show_all();
               if ( answer2 != -1 )
               {
                  clear_bitmap ( ptr_bmp2 );
                  draw_sprite ( ptr_bmp2,
                     ( BITMAP* ) datfmaze [ BMP_MASKTEX000 + answer2 ] . dat, 0, 0 );
//               ptr_wdat->parameter (
//                  *(( BITMAP* ) datfmaze [ BMP_MASKTEX000 + answer2 ] . dat ));
//               make_screen_shot ( "gallshot.bmp");
                  ptr_wdat->parameter ( *ptr_bmp2
                  // *(( BITMAP* ) datf [ BMP_MASKTEX000 + answer2 ] . dat ));
                  Window::refresh_all();
                  //printf ("passing through\r\n");
               }
            }
            delete ptr_wlst;
            delete ptr_wdat;
         break;*/



         /*   ptr_wdat = new WinData<BITMAP> ( WDatProc_gallery_bitmap ,
               *(( BITMAP* ) datfennemy [ BMP_ENNEMY000 ] . dat ),
               WDatProc_POSITION_GALLERY_BITMAP );
            ptr_wlst = new WinList ( lst_monster, 20, 40 );
            answer2 = 0;
            while ( answer2 != -1 )
            {
               answer2 = Window::show_all();
//               make_screen_shot ( "gallshot.bmp");
               if ( answer2 != -1 )
               {
                  ptr_wdat->parameter (
                     *(( BITMAP* ) datfennemy [ BMP_ENNEMY000 + answer2 ] . dat ));
                  Window::refresh_all();
               }
            }
            delete ptr_wlst;
            delete ptr_wdat;*/


         /*case Screen_GALLERY_OBJECT :
            ptr_wdat = new WinData<BITMAP> ( WDatProc_gallery_bitmap,
               *(( BITMAP* ) datfmaze [ BMP_MAZEOBJ000 ] . dat ),
               WDatProc_POSITION_GALLERY_BITMAP );
            ptr_wlst = new WinList ( lst_object, 20, 40 );
            answer2 = 0;
            while ( answer2 != -1 )
            {
               answer2 = Window::show_all();
//               make_screen_shot ( "gallshot.bmp");
               if ( answer2 != -1 )
               {
                  ptr_wdat->parameter (
                     *(( BITMAP* ) datfmaze [ BMP_MAZEOBJ000 + answer2 ] . dat ));
                  Window::refresh_all();
               }
            }
            delete ptr_wlst;
            delete ptr_wdat;
         break;*/



/*         case Screen_GALLERY_SOUND :
            ptr_wlst = new WinList ( lst_sound, 20, 40 );
            answer2 = 0;
            while ( answer2 != -1 )
            {
               answer2 = Window::show_all();
               if ( answer2 >= 0 )
                  play_sample ( ( SAMPLE* ) datfaudio [ SMP_SOUND000 + answer2 ] . dat
                     , 255, 128, 1000, 0 );
            }
            delete ptr_wlst;
         break;
*/

            /*ptr_wlst = new WinList ( lst_font, 20, 40 );
            answer2 = 0;
            while ( answer2 != -1 )
            {
               Window::draw_all();
               //to do: Use a WinData window
               textout_centre_old ( subbuffer, ( FONT* ) datffont [ FONT_LIST [ answer2 ] . dataID ] . dat,
                "Prepare your self for", 450, 200, General_COLOR_TEXT );
               textout_centre_old ( subbuffer, ( FONT* ) datffont [ FONT_LIST [ answer2 ] . dataID ] . dat,
                "the ultimate fantasy Game", 450, 232, General_COLOR_TEXT );
               answer2 = ptr_wlst->show();
               if ( answer2 != -1 )
                  Window::refresh_all();
            }
            delete ptr_wlst;*/


        /* case Screen_GALLERY_MONSTER2 :
            //ptr_wdat = new WinData<BITMAP> ( WDatProc_gallery_bitmap ,
            //   *(datref2_monster[0]), WDatProc_POSITION_GALLERY_BITMAP );
            ptr_wdat = new WinData<BITMAP> ( WDatProc_gallery_bitmap ,
               *(datref_monster[0]), WDatProc_POSITION_GALLERY_BITMAP );
            ptr_wlst = new WinList ( lst_monster2, 20, 40 );
            answer2 = 0;
            while ( answer2 != -1 )
            {
               answer2 = Window::show_all();
//               make_screen_shot ( "gallshot.bmp");
               if ( answer2 != -1 )
               {

                     //ptr_wdat->parameter ( *(datref2_monster [ answer2 ]));
                  if ( datref_monster [ answer2 ] != NULL)
                  {*/

                     /*if ( answer2 > 50 )
                     {
                        printf ("%d = %d\n",answer2, datref_monster [ answer2]);
                        //printf ("%d = %d\n",answer2, *(datref_monster [ answer2]));
                     }*/
/*
                     ptr_wdat->parameter ( *(datref_monster [ answer2 ]));
                     //ptr_wdat ->parameter ( *no_picture );
                     Window::refresh_all();
                  }
                  else
                  {
                     //printf ("pass here");
                     ptr_wdat ->parameter ( *no_picture );
                     Window::refresh_all();
                  }

               }
            }
            delete ptr_wlst;
            delete ptr_wdat;
         break;
*/
      }
   }

   destroy_bitmap ( ptr_bmp );
   destroy_bitmap ( ptr_bmp2 );
}

void show_sources ( void )
{

   clear_to_color (subbuffer, makecol (0,0, 0 ) );

   textout_centre_old ( subbuffer, FNT_elgar32,
      "Wizardry Legacy", 320 , 0 , makecol ( 255, 225, 50 ) );

   textout_centre_old ( subbuffer, FNT_print, "Game Sources",
      320 , 32, General_COLOR_TEXT );

   textout_old ( subbuffer, FNT_print,
      "Game Design: Wizardry Original Series from 1-8 + Jananese Wizardry Games",
      0, 48, General_COLOR_TEXT );

   /*textout_old ( subbuffer, FNT_print,
      "Game Design: Dungeons and Dragons 2nd and 3rd edition",
      0, 64, General_COLOR_TEXT );*/

   textout_old ( subbuffer, FNT_print,
      "Textures: Hexen and Heretic Video game",
      0, 64, General_COLOR_TEXT );

   textout_old ( subbuffer, FNT_print,
      "Textures: David Gurrea (www.davegh.com) ",
      0, 80, General_COLOR_TEXT );

    textout_old ( subbuffer, FNT_print,
      "Music: Wizardry 1-3 Series",
      0, 96, General_COLOR_TEXT );

   textout_old ( subbuffer, FNT_print,
      "Artwork: Wizardry 1-3 Series",
      0, 112, General_COLOR_TEXT );

   textout_old ( subbuffer, FNT_print,
      "Artwork: Thalzon ",
      0, 128, General_COLOR_TEXT );

   textout_old ( subbuffer, FNT_small,
      "                        (http://forums.rpgmakerweb.com/index.php?/topic/49-thalzons-battlers-and-faces/)",
      0, 144, General_COLOR_TEXT );

   textout_old ( subbuffer, FNT_print,
      "Code Library: Allegro and SQLite",
      0, 160, General_COLOR_TEXT );

   textout_old ( subbuffer, FNT_print,
      "Code Library: AASTR (Anti Alliased Stretch Blitting) by Michael Bukin",
      0, 176, General_COLOR_TEXT );

   textout_centre_old ( subbuffer, FNT_small,
      "Press any key to return to the title screen", 320 , 460 , General_COLOR_TEXT );

   Window::instruction ( 320, 240, 0);
   copy_buffer();
   mainloop_readkeyboard ();


}

void show_transition_algo7 ( BITMAP *before, BITMAP *after, int delay )
{

   short i;
   short j;
   short tmpj;
   short x;
   short y;
   short xrel;
   short yrel;

   j = 0;
   for ( i = 0 ; i < 16 ; i++ )
   {
      // calculating correct j value;

      if ( j > 15 )
         j = j - 16;

      // interpretation of relative pixel
      tmpj = j;
      xrel = 0;
      yrel = 0;
      while ( tmpj > 3 )
      {
         yrel++;
         tmpj = tmpj - 4;
      }
      xrel = tmpj;

      // draw the pixel
      for ( y = 0 ; y < SCREEN_H ; y = y + 4 )
         for ( x = 0 ; x < SCREEN_W ; x = x + 4 )
            putpixel ( before, x + xrel, y + yrel, getpixel ( after, x + xrel, y+ yrel ) );
      blit ( before, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );

      // incrementing j;
      j = j + 7;
      rest(delay);
   }
}

void show_transition_slide_fade ( BITMAP *bmp, int fade , int direction, int speed )
{

   int slide_progress = 0;
   int slide_max;
   int tmpx;
   int tmpy;
   fixed alpha = 255;
   fixed fade_inc;

   switch ( fade)
   {
      case Screen_FADE_IN :
         alpha = 0;
      break;
      case Screen_FADE_OUT :
         alpha = 255;
      break;
   }


   switch ( direction )
   {
      case Screen_SLIDE_LEFT:
      case Screen_SLIDE_RIGHT:
         slide_max = 640;
         fade_inc = 255 / (640 / speed);
      break;
      default: // used to avoid warning, but makes no sense.
      case Screen_SLIDE_UP:
      case Screen_SLIDE_DOWN:
         slide_max = 480;
         fade_inc = 255 / ( 480 / speed);
      break;
   }

   for ( slide_progress = 0; slide_progress < slide_max; slide_progress += speed)
   {
      tmpx = 0;
      tmpy = 0;
      switch ( direction)
      {
         case Screen_SLIDE_UP:
            if ( fade == Screen_FADE_OUT)
               tmpy = 0 - slide_progress;
            else
               tmpy = 480 - slide_progress;
         break;
         case Screen_SLIDE_RIGHT:
            if ( fade == Screen_FADE_OUT)
               tmpx = 0 + slide_progress;
            else
               tmpx = -640 + slide_progress;
         break;
         case Screen_SLIDE_DOWN:
            if ( fade == Screen_FADE_OUT)
               tmpy = 0 + slide_progress;
            else
               tmpy = -480 + slide_progress;
         break;
         case Screen_SLIDE_LEFT:
            if ( fade == Screen_FADE_OUT)
               tmpx = 0 - slide_progress;
            else
               tmpx = 640 - slide_progress;
         break;

      }

      switch ( fade )
      {
         case Screen_FADE_IN :
            alpha += fade_inc;
         break;
         case Screen_FADE_OUT :
            alpha -= fade_inc;
         break;
      }

      set_trans_blender ( 0, 0, 0, alpha );
      draw_trans_sprite ( subbuffer, bmp, tmpx , tmpy );

      copy_buffer();
      //rest (delay);
      //clear_to_color ( screen, makecol ( 0, 0, 0));


   }


}

/*void show_system_setup ( void )
{
   int answer = 0;

   WinEmpty wemp_background;

   WinTitle wttl_title ( "System Setup");

   WinData<int> wdat_sysinfo ( WDatProc_system_info, 0, WDatProc_POSITION_SYSTEM_INFO );

   // maybe could make a cascade

   Menu mnu_system ("Select what you want to change");

   mnu_system.add_item ( SCREEN_SYSMENU_RESOLUTION , "Graphic Resolution", true);
   mnu_system.add_item ( SCREEN_SYSMENU_MIDI , "Midi driver", true);
   mnu_system.add_item ( SCREEN_SYSMENU_EXIT , "Exit");

   WinMenu wmnu_system ( mnu_system, 20, 50);

   while ( answer != SCREEN_SYSMENU_EXIT )
   {
      wmnu_system.unhide();
      answer = Window::show_all();

      wmnu_system.hide();
*/
      /*switch (answer)
      {
         case SCREEN_SYSMENU_RESOLUTION:
            show_system_setup_resolution();
         break;
         case SCREEN_SYSMENU_MIDI:
            show_system_setup_midi();
         break;
      }*/
/*   }

}


void show_system_setup_resolution ( void )
{
*/
/*
GFX_MODE_LIST *get_gfx_mode_list(int card);
Attempts to create a list of all the supported video modes for a certain graphics
driver, made up from the GFX MODE LIST structure, which has the following
deﬁnition:
typedef struct GFX_MODE_LIST
{
int num_modes;
GFX_MODE *mode;
} GFX_MODE_LIST;
The mode entry points to the actual list of video modes.
typedef struct GFX_MODE

{
int width, height, bpp;
} GFX_MODE;
This list of video modes is terminated with an { 0, 0, 0 } entry.
Note that the card parameter must refer to a real driver. This function fails
if you pass GFX SAFE, GFX AUTODETECT, or any other "magic" driver.
Returns a pointer to a list structure of the type GFX MODE LIST or NULL
if the request could not be satisﬁed.


------------------------------

void destroy_gfx_mode_list(GFX_MODE_LIST *mode_list);
Removes the mode list created by get gfx mode list() from memory. Use this
once you are done with the generated mode list to avoid memory leaks in your
program.

*/

/*}

void show_system_setup_midi ( void )
{
*/
/*

int detect_digi_driver(int driver_id);
Detects whether the speciﬁed digital sound device is available. This function
must be called before install sound().
Returns the maximum number of voices that the driver can provide, or zero if
the hardware is not present.

int detect_midi_driver(int driver_id);
Detects whether the speciﬁed MIDI sound device is available. This function
must be called before install sound().
Returns the maximum number of voices that the driver can provide, or zero if
the hardware is not present.
There are two special-case return values that you should watch out for: if this
function returns -1 it is a note-stealing driver (eg. DIGMID) that shares voices
with the current digital sound driver, and if it returns 0xFFFF it is an external
device like an MPU-401 where there is no way to determine how many voices
are available.

*/

//}

void show_art_gallery_font ( void )
{
   int answer = 0;
   List lst_font ("Font List", 20, true );
   int i;
   BITMAP *bmpfont = create_bitmap (320, 320);

   clear ( bmpfont );

   for ( i = 0 ; i < System_NB_FONT ; i++ )
   {
      lst_font.add_item (i, FONT_LIST [ i ].name );
   }

   WinData<BITMAP> wdat_font ( WDatProc_gallery_font ,
               *bmpfont, WDatProc_POSITION_GALLERY_FONT );

   WinList wlst_font ( lst_font, 20, 40 );

   while ( answer != -1 )
   {
      //Window::draw_all();

      //todo place in side window with Windata & use show all.
      //try drawing in bitmap and refresshing window only. Easier.
      //not sure since creature might be stretched.
      clear (bmpfont);
      textout_centre_old ( bmpfont, datref_font [ answer ] ,
                "Prepare yourself", 160, 0, General_COLOR_TEXT );
      textout_centre_old ( bmpfont, datref_font [ answer ],
                "for the ultimate", 160, 48, General_COLOR_TEXT );
      textout_centre_old ( bmpfont, datref_font [ answer ],
                "fantasy Game.", 160, 96, General_COLOR_TEXT );
      textout_centre_old ( bmpfont, datref_font [ answer ],
                "Wizardry", 160, 144, General_COLOR_TEXT );
      Window::refresh_all();
      //---------------------------------------
      answer = Window::show_all();
   }



}

void show_art_gallery_texture ( void )
{
   //BITMAP *displaybmp = create_bitmap ( 256, 256 );
   int i;
   //int texid = 1;
   int answer = 0;
   //char tmpstr [ 50 ];

   List lst_texset ("Select a texture set", 8 );

   for ( i = 0 ; i < System_NB_TEXTURE_SET; i++)
   {
      lst_texset.add_item ( i, STR_TEXTURE_SET [ i ]);
   }

   WinList wlst_texset ( lst_texset, 20, 40 );
   answer = Window::show_all();


   while ( answer != -1)
   {
      if ( answer < System_NB_TEXTURE_SET )
      {

         ImageList imglst_texture ( STR_TEXTURE_SET [ answer ], 2, 1, 256, 256 );
         imglst_texture.add_datref ( datref_texture, answer );

         if ( imglst_texture.nb_item() > 0 )
         {
            WinImageList wimglst_texture ( imglst_texture, 40, 60);
            Window::show_all();
         }
      }

      answer = Window::show_all();
   }


   //clear_bitmap ( displaybmp );

   // might need to manually add reference for families of textures


   /*for ( i = 1024 ; i < 3071 ; i++ )
   {
      printf ( "Screen: i = %d texid=%d\n", i, texid);
      //// crash as 1290, return to 1 for reason X
      if ( datref_texture [ i ] != NULL)
      {
         sprintf ( tmpstr, "Texture %4d", texid );
         lst_texture.add_item (i, tmpstr );
         texid++;
      }
   }*/
   //draw_sprite ( displaybmp,
   //                  datref_texture [ 1024 ] , 0, 0 );

   //WinData<BITMAP> wdat_texture ( WDatProc_gallery_bitmap, *displaybmp
   //             , WDatProc_POSITION_GALLERY_BITMAP );



   /*while ( answer != -1 )
   {
      answer = Window::show_all();
      //if ( answer != -1 )
      //{
         //clear_bitmap ( displaybmp );
         //draw_sprite ( displaybmp,
         //            datref_texture [ answer ] , 0, 0 );
         Window::refresh_all();
      //}
   }*/


//-------------------------------------------- imported code
  /*ptr_bmp2 = create_bitmap ( 128, 128 );
            clear_bitmap ( ptr_bmp2 );
            draw_sprite ( ptr_bmp2, ( BITMAP* ) datfmaze [ BMP_MASKTEX000 ] . dat, 0, 0 );
            ptr_wdat = new WinData<BITMAP> ( WDatProc_gallery_bitmap, *ptr_bmp2
               // *(( BITMAP* ) datfmaze [ BMP_MASKTEX000 ] . dat )
               , WDatProc_POSITION_GALLERY_BITMAP );
            ptr_wlst = new WinList ( lst_masktex, 20, 40/ );
            answer2 = 0;
            while ( answer2 != -1 )
            {
               answer2 = Window::show_all();
               if ( answer2 != -1 )
               {
                  clear_bitmap ( ptr_bmp2 );
                  draw_sprite ( ptr_bmp2,
                     ( BITMAP* ) datfmaze [ BMP_MASKTEX000 + answer2 ] . dat, 0, 0 );
//               ptr_wdat->parameter (
//                  *(( BITMAP* ) datfmaze [ BMP_MASKTEX000 + answer2 ] . dat ));
//               make_screen_shot ( "gallshot.bmp");
                  ptr_wdat->parameter ( *ptr_bmp2
                  // *(( BITMAP* ) datf [ BMP_MASKTEX000 + answer2 ] . dat ));
                  Window::refresh_all();
                  //printf ("passing through\r\n");
               }
            }
            delete ptr_wlst;
            delete ptr_wdat;*/
}

void show_art_gallery_monster ( void )
{

   //int i;
   //int texid = 1;
   ImageList imglst_monster ("Monsters", 3, 2, 160, 160 );
   //int answer = 0;
   //char tmpstr [ 50 ];

   imglst_monster.add_datref ( datref_monster, 1 );


   WinImageList wimglst_monster ( imglst_monster, 20, 40);

   Window::show_all();

}

void show_art_gallery_music ( void )
{
   List lst_music ("Select a music to play", 20);
   int i;
   int answer2;

   for ( i = 0 ; i < System_NB_MUSIC ; i++ )
      lst_music.add_item (i, MUSIC_LIST [ i ] . name );

   WinList wlst_music ( lst_music, 20, 40 );
   answer2 = 0;
   while ( answer2 != -1 )
   {
      answer2 = Window::show_all();
      if ( answer2 >= 0 )
         play_music_track ( answer2 );
   }


}

void show_art_gallery_sound ( void )
{
   List lst_sound ("Select a sound to play", 20);
   int i;
   int answer;

   //for ( i = 0 ; i < List_MAX_NB_ITEM ; i++ )
   for ( i = 0 ; i < 15 ; i++ )
   {

      lst_sound.add_itemf (i + 1024, "Sound %3d", i );
   }

   WinList wlst_sound ( lst_sound, 20, 40 );
   answer = 0;
   while ( answer != -1 )
   {
      answer = Window::show_all();
      if ( answer >= 0 )
      {
         if ( datref_sound [ answer] != NULL)
         {
            play_sound ( answer );
         }
            // maybe encapsulate play sample function like play music with an helper
      }
   }

   /*WinList wlst_sound ( lst_sound, 20, 40 );
   answer2 = 0;
   while ( answer2 != -1 )
   {
      answer2 = Window::show_all();
      if ( answer2 >= 0 )
         play_music_track ( answer2 );
   }*/






}

void show_art_gallery_object ( void )
{
   //int i;
   //int texid = 1;
   ImageList imglst_mazeobj ("Maze Objects", 3, 2, 200, 200, true );
   //int answer = 0;
   //char tmpstr [ 50 ];

   imglst_mazeobj.add_datref ( datref_object );

   WinImageList wimglst_mazeobj ( imglst_mazeobj, 10, 20);

   Window::show_all();
}

void show_art_gallery_image ( void )
{
   //int i;
   //int texid = 1;
   ImageList imglst_image ("Various Images", 3, 2, 200, 200, true );
   //int answer = 0;
   //char tmpstr [ 50 ];

   imglst_image.add_datref ( datref_image );

   WinImageList wimglst_image ( imglst_image, 10, 20);

   Window::show_all();
}

void show_art_gallery_sky ( void )
{
   //int i;
   //int texid = 1;
   ImageList imglst_sky ("Sky Texture", 2, 2, 256, 200 );
   //int answer = 0;
   //char tmpstr [ 50 ];

   imglst_sky.add_datref ( datref_sky );

   WinImageList wimglst_sky ( imglst_sky, 20, 20);

   Window::show_all();
}

