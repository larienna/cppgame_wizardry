/***************************************************************************/
/*                                                                         */
/*                        G E N E R A L . C P P                            */
/*                                                                         */
/*     Content : Unclassified procedures                                   */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : March 19, 2002                                      */
/*                                                                         */
/***************************************************************************/


// Group Includes
#include <grpsys.h>
#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>

#include <option.h>
#include <config.h>
//#include <allegro.h>


//#include <stdio.h>
//#include <stdlib.h>
//#include <time.h>

//#include <string.h>
//#include <datafile.h>
//#include <datmacro.h>
//#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>

/*-------------------------------------------------------------------------*/
/*-                       General Procedures                              -*/
/*-------------------------------------------------------------------------*/

void textinput ( short x, short y, char *str, int nb_char )

{
   char *input_string;
   int i = 0;
   int tmpkey;
   int tmpcode = KEY_ESC;
   int tmpascii;

   text_mode_old ( 0 );

   input_string = (char*) malloc ( nb_char + 3 );

   strncpy( input_string, "                                                                                                                                ",
      nb_char + 1 );
   input_string [ nb_char + 1 ] = '\0';
   textout_old ( subbuffer, General_FONT_INPUT, input_string, x, y,
      General_COLOR_TEXT );

   strcpy ( input_string, "_ \0" );

   textout_old ( subbuffer, General_FONT_INPUT, input_string, x, y,
      General_COLOR_TEXT );

   vsync();
   blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );

   while ( tmpcode != KEY_ENTER || i == 0)
   {
      tmpkey = readkey();
      tmpcode = tmpkey >> 8;
      tmpascii = tmpkey & 0xff;

      if ( tmpcode != KEY_ENTER )
      {

         if ( tmpcode == KEY_BACKSPACE && i > 0 )
            i--;
         else
            if ( i < nb_char && tmpcode != KEY_BACKSPACE )
            {
               if ( tmpascii != 0/* && tmpcode != KEY_BACKSPACE*/ )
               {
                  input_string [ i ] = tmpascii;
                  i++;
               }
               else
                  clear_keybuf();
            }

         if ( i >= nb_char )
         {
            input_string [ i ] = ' ';
            input_string [ i + 1 ] = '\0';
         }
         else
         {
            input_string [ i ] = '_';
            input_string [ i + 1 ] = ' ';
            input_string [ i + 2 ] = '\0';
         }

      textout_old ( subbuffer, General_FONT_INPUT, input_string, x, y,
         General_COLOR_TEXT );

      vsync();
      blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );

      }
   }

   input_string [ i ] = '\0';

   clear ( buffer );

   strcpy ( str,input_string );
   text_mode_old ( -1 );
   free ( input_string );
//   return ( input_string );
}

/*void save_backup_screen ( void )
{
   blit ( screen, backup, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
}

void load_backup_screen ( void )
{
   blit ( backup, buffer, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
}*/

/*void save_backup_buffer ( void )
{
   blit ( buffer, backup, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
}

void load_backup_buffer ( void )
{
   blit ( backup, buffer, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
}*/

void copy_buffer ( void )
{
   vsync ();
   acquire_screen();
   blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   release_screen();

   //printf ("doing a copy buffer\r\n");

   /*last_buffer = triplebuffer [ bufferid ];
   if ( bufferid == 0)
      bufferid = 1;
   else
      bufferid = 0;
   buffer = triplebuffer [ bufferid ];*/

   clear( buffer );
   //printf ("copu buffer\r\n");
}

void copy_backup_buffer ( void )
{
   vsync ();
   acquire_screen();
   blit ( backup, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   release_screen();
   clear( backup );
}

void copy_buffer_keep ( void )
{
   vsync ();
   acquire_screen();
   blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   release_screen();

}

void copy_backup_buffer_keep ( void )
{
   vsync ();
   acquire_screen();
   blit ( backup, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   release_screen();
}

void blit_mazebuffer ( void )
{
   // this does not blit on the screen yet.
   blit ( mazebuffer, buffer, 0, 0, System_X_OFFSET, System_Y_OFFSET, 640, 480);
   //clear ( mazebuffer );
}

void blit_editorbuffer ( void )
{
   // this does not blit on the screen yet.
   blit ( editorbuffer, buffer, 0, 0, System_X_OFFSET, System_Y_OFFSET, 640, 480);
   //clear ( mazebuffer );
}


void switch_in_callback ( void )
{
   //acquire_screen();
   //blit ( backup, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   //release_screen();
   printf ("switching in\r\n");
   switch_redraw = true;
}

void switch_out_callback ( void )
{
   //printf ("switching out 1\r\n");
   //acquire_screen();
   //blit ( screen, backup, 0, 0 , 0, 0, SCREEN_W, SCREEN_H );
   //release_screen();

   printf ("switching out\r\n");

}


void make_screen_shot ( char *filename )
{

   BITMAP *scrnshot;
   PALETTE pal;

   scrnshot = create_bitmap ( SCREEN_W, SCREEN_H );

   blit ( screen, scrnshot, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   get_palette ( pal );


   save_bitmap (filename, scrnshot, pal );

   destroy_bitmap ( scrnshot );
}


void play_music_track ( int track, bool loop )
{
   if ( loop == true )
      play_midi ( datref_musicmidi [ track ] , 1 );
   else
      play_midi ( datref_musicmidi [ track ] , 0 );
}

void play_music_table ( int table )
{
   //bool random = false;
   int tmpconfig = config.get ( Config_MUSIC_BASE + table);
   int tmprndmusic =  Config_rndmusic [ table ];

   // verification for error
   if ( tmprndmusic < 0)
   {
      tmprndmusic = 0;
      printf ("play_music_table: Trapped error < 0\n");
   }

   if ( tmprndmusic > 2)
   {
      tmprndmusic = 2;
      printf ("play_music_table: Trapped error > 2\n");
   }

   //printf ( "debug: tmpconfg = %d\n",tmpconfig);

   if ( tmpconfig != Config_MUS_RANDOM )
   {
      play_music_track ( Config_MUSIC_TABLE [ table ] [ tmpconfig] );
     // printf ("Debug: play_table: pass here instead\n");
   }
   else
   {
      play_music_track ( Config_MUSIC_TABLE [ table ] [ tmprndmusic  ]);
      //printf ("Debug:play_table: table=%d, rnd=%d\n", table, Config_rndmusic [ table ]);
   }
}

void play_sound ( int soundid )
{
   play_sample ( datref_sound [ soundid ], 255, 128, 1000, 0 );
}

int mainloop_readkeyboard ( void )
{
   while ( keypressed() == false)
   {
      if ( switch_redraw == true)
      {
         switch_redraw = false;
         return ( -1 );
      }
   }

   return ( readkey() >> 8 );
}


void debug ( const char *str, int var1, int var2 )
{
   textprintf_old ( subbuffer, font, 0, 0, General_COLOR_TEXT, str, var1, var2 );
   copy_buffer();
   while ( mainloop_readkeyboard() != KEY_ENTER );
}
/*
unsigned short statdice ( s_Opponent_rollstat rollstat )
{
   unsigned short min = rollstat.min;
   unsigned short max = rollstat.max + rollstat.modifier;
   unsigned short value = 1;

   if ( max < min )
      max = min;

   if ( max > min )
      value = rnd ( max - min );

   return ( min + value + rollstat.bonus );

} */


/*
void flist_proc ( const char *name, int attribute, int param )
{
   strncpy ( filelist [ flistindex ], name, 12 );
   flistindex++;

   textprintf_old ( buffer, font, 10, 10, General_COLOR_TEXT,
      "name : %s | attribute : %d | param : %d",
      name, attribute, param );

   textprintf_old ( buffer, font, 10, 30, General_COLOR_TEXT,
      "%d : %s", flistindex - 1, filelist [ flistindex - 1 ] );

   copy_buffer();
   while ( ( readkey() >> 8 ) != KEY_ENTER );
} */

/*
double exponent ( float x, float y )
{
   double result;


} */

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/


s_OS_Identification OS_NAME [ Allegro_NB_OPERATING_SYSTEM ] =
{
   { OSTYPE_UNKNOWN, "Ms-Dos" },
   { OSTYPE_WIN3,    "Windows 3.1" },
   { OSTYPE_WIN95,   "Windows 95" },
   { OSTYPE_WIN98,   "Windows 98" },
   { OSTYPE_WINME,   "Windows ME" },
   { OSTYPE_WINNT,   "Windows NT" },
   { OSTYPE_WIN2000, "Windows 2000" },
   { OSTYPE_WINXP,   "Windows XP" },
   { OSTYPE_OS2,     "OS/2" },
   { OSTYPE_WARP,    "OS/2 Warp" },
   { OSTYPE_DOSEMU,  "Linux DOSEMU" },
   { OSTYPE_OPENDOS, "Caldera OpenDOS" },
   { OSTYPE_LINUX,   "Linux" },
   { OSTYPE_FREEBSD, "FreeBSD" },
   { OSTYPE_UNIX,    "Unix" },
   { OSTYPE_BEOS,    "BeOS" },
   { OSTYPE_QNX,     "QNX" },
   { OSTYPE_MACOS,   "MacOS" }
};

s_CPU_family CPU_FAMILY_NAME [ Allegro_NB_CPU_FAMILY ] =
{
   { 3, "386" },
   { 4, "486" },
   { 5, "Pentium" },
   { 6, "Pentium Pro" },
   { -1, "Unknown" }
};

bool switch_redraw = false;




