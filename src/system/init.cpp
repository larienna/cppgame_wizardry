/***************************************************************************/
/*                                                                         */
/*                            I N I T . C P P                              */
/*                                                                         */
/*     Content : Initialisation procedures                                 */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : march 19, 2002                                      */
/*                                                                         */
/***************************************************************************/

#include <option.h>

//include groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpengine.h>


#include <init.h>

#ifdef OS_UNIX
#include <unistd.h>
#endif
#ifdef OS_WIN
#include <direct.h>
#endif

/*
#include <allegro.h>

#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
#include <init.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
#include <party.h>
//
//#include <game.h>
//#include <city.h>
#include <maze.h>
//
//#include <camp.h>
#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
*/


/*-------------------------------------------------------------------------*/
/*-                             Procedures                                -*/
/*-------------------------------------------------------------------------*/

/*int decode_parameter ( int nb_param, char *param[] )
{
   if ( nb_param > 1 )
      if ( strcmp ( param [ 1 ] , "-setup" ) == 0 )
         return ( Init_PARAM_SETUP );
      else
         if ( strcmp ( param [ 1 ] , "-editor" ) == 0 )
            return ( Init_PARAM_EDITOR );

   return ( Init_PARAM_NONE );
}*/

/*
int init_setup ( void )
{
   int status = INIT_SUCCESS;
   int value;

   printf ("|--------------------------------------|\r\n");
   printf ("|        Wizardry legacy %s         |\r\n", VERSION );
   printf ("|            Setup Utility             |\r\n" );
   printf ("|--------------------------------------|\r\n\r\n");

   printf ("Initialising %s ", allegro_id);
   set_uformat ( U_ASCII );
   value = allegro_init ();
   if ( value != 0 )
   {
      status = INIT_FAILURE;
      printf ("[ Failure ]\r\n");
   }
   else
      printf ("[ Success ]\r\n");

   if ( status == INIT_SUCCESS )
      printf ("- Running Platform : %s \r\n", ALLEGRO_PLATFORM_STR );

   if ( status == INIT_SUCCESS )
   {
      printf ("Installing Keyboard      ");
      value = init_keyboard ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[ Failure ]\r\n");
      }
      else
         printf ("[ Success ]\r\n");
   }

   if ( status == INIT_SUCCESS )
   {
      printf ("Installing Timer         ");
      value = init_timer ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[ Failure ]\r\n");
      }
      else
         printf ("[ Success ]\r\n");
   }

   if ( status == INIT_SUCCESS )
   {
      printf ("Installing Mouse         ");
      value = install_mouse ();
      if ( value == -1 )
      {
         status = INIT_FAILURE;
         printf ("[ Failure ]\r\n");
      }
      else
         printf ("[ Success ]\r\n");
   }



   if ( status == INIT_SUCCESS )
   {
      printf ("Initialising Graphic Mode");
      set_color_depth ( 8 );
      value = set_gfx_mode(GFX_SAFE, 640, 480, 0, 0);
      if ( value == -1 )
      {
         status = INIT_FAILURE;
         printf ("[ Failure ]\r\n");
      }
      else
         printf ("[ Success ]\r\n");
   }

  printf ("--------------------------------------------------\r\n");

   return ( status );
}*/

int init_game ( void )
{
   int status = INIT_SUCCESS;
   int value;
//   int i;

   printf ("|--------------------------------------|\r\n");
   printf ("|        Wizardry legacy %s           |\r\n", VERSION );
   printf ("|--------------------------------------|\r\n\r\n");

   printf ("Initialising %s \r\n", allegro_id);
   set_uformat ( U_ASCII );
   value = allegro_init ();
   if ( value != 0 )
   {
      status = INIT_FAILURE;
      printf ("[----- Failure -----]\r\n");
   }
   else
   {
      printf ("- Running Platform : %s \r\n", ALLEGRO_PLATFORM_STR );
      printf ("[----- Success -----]\r\n");
   }

   printf ("- Registering PNG Extention\r\n");
   //alpng_init ();


   if ( status == INIT_SUCCESS )
   {
      printf ("- Loading Game Default configuration\r\n");
      config.load();
      //config.verify_if_default();



/*      for ( i = 0 ; i < Allegro_NB_OPERATING_SYSTEM ; i++ )
         if ( os_type == OS_NAME [ i ].ID )
            value = i;
      printf ("- Detected Operating System : %s %d.%d\r\n",
         OS_NAME [ i ].name, os_version, os_revision );

      value = -1;
      for ( i = 0 ; i < Allegro_NB_CPU_FAMILY ; i++ )
         if ( cpu_family == CPU_FAMILY [ i ].ID )
            value = i;
      if ( value == -1 )
         value = Allegro_NB_CPU_FAMILY; // unknown type : last one
      printf ("- Detected CPU : %s\r\n",
         CPU_FAMILY [ i ].name  );*/
   }

   if ( status == INIT_SUCCESS )
   {
      printf ("Installing Keyboard      \r\n");
      value = init_keyboard ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[----- Failure -----]\r\n");
      }
      else
         printf ("[----- Success -----]\r\n");
   }

   if ( status == INIT_SUCCESS )
   {
      printf ("Installing Timer         \r\n");
      value = init_timer ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[----- Failure -----]\r\n");
      }
      else
         printf ("[----- Success -----]\r\n");
   }


   if ( status == INIT_SUCCESS )
   {
      printf ("Installing Sound Driver  \r\n");
      value = init_sound ();
      if ( value != INIT_SUCCESS )
      {
         //status = INIT_FAILURE;
         printf ("[----- Failure -----]\r\n");
         printf ("Continuing game without sound\r\n");
/*         for ( i = 0 ; i < 120; i++ )
            if ( strcmp ( &allegro_error [ i ], "" ) != 0 )
               printf ("- %s\r\n",allegro_error [ i ] );*/
      }
      else
         printf ("[----- Success -----]\r\n");
   }

  /* if ( status == INIT_SUCCESS )
   {
      printf ("Loading SQL Database     ");
      value = init_sqldatabase ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[ Failure ]\r\n");
      }
      else
         printf ("[ Success ]\r\n");
   }*/

   if ( status == INIT_SUCCESS )
   {
      printf ("Initialising other stuff \r\n");
      init_other ();
   }

   if ( status == INIT_SUCCESS )
   {
      printf ("Initialising Graphic Mode\r\n");
      value = init_graphic ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[----- Failure -----]\r\n");
/*         for ( i = 0 ; i < 120; i++ )
            if ( strcmp ( &allegro_error [ i ], "" ) != 0 )
               printf ("- %s\r\n",allegro_error [ i ] );*/
      }
      else
         printf ("[----- Success -----]\r\n");
   }


   if ( status == INIT_SUCCESS )
   {
      printf ("Loading Data             \r\n");
      value = init_data ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[----- Failure -----]\r\n");
      }
      else
         printf ("[----- Success -----]\r\n");
   }

   printf ("--------------------------------------------------\r\n");
   printf ("Debugging Log:\r\n");

   return ( status );
}

/*
int init_editor ( void )
{
   int status = INIT_SUCCESS;
   int value;
//   int i;

   printf ("|--------------------------------------|\r\n");
   printf ("|        Wizardry legacy %s         |\r\n", VERSION );
   printf ("|             Maze Editor              |\r\n" );
   printf ("|--------------------------------------|\r\n\r\n");

   printf ("Initialising %s ", allegro_id);
   set_uformat ( U_ASCII );
   value = allegro_init ();
   if ( value != 0 )
   {
      status = INIT_FAILURE;
      printf ("[ Failure ]\r\n");
   }
   else
      printf ("[ Success ]\r\n");

   if ( status == INIT_SUCCESS )
   {

      printf ("- Running Platform : %s \r\n", ALLEGRO_PLATFORM_STR );
   }

   if ( status == INIT_SUCCESS )
   {
      printf ("Installing Keyboard      ");
      value = init_keyboard ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[ Failure ]\r\n");
      }
      else
         printf ("[ Success ]\r\n");
   }

   if ( status == INIT_SUCCESS )
   {
      printf ("Initialising Graphic Mode");
      value = init_graphic ();
      if ( value != INIT_SUCCESS )
      {
         status = INIT_FAILURE;
         printf ("[ Failure ]\r\n");
      }
      else
         printf ("[ Success ]\r\n");
   }

      printf ("Loading Data             ");

   clear ( screen );

   textout_centre_old ( subscreen, font,
      "Welcome to the Wizardry Maze Editor",
      320, 230, General_COLOR_TEXT );
   textout_centre_old ( subscreen, font,
      "This editor will make .maz file that can be imported into the adventure file",
      320, 250, General_COLOR_TEXT );

   textprintf_centre_old ( subscreen, font, 320, 360, General_COLOR_TEXT,
      "Wizardry Legacy  v%s  %s", VERSION, YEAR );

   rect ( subscreen, 218, 398, 421, 416, General_COLOR_TEXT );

   text_mode_old ( 0 );

   textout_centre_old ( subscreen, font,
      "  Loading Maze Textures  ", 320, 460, General_COLOR_TEXT );
   datfmaze = load_datafile_callback ("datafile/mazetex.dat",
InitProc_datafile_editor_loading);

   textout_centre_old ( subscreen, font,
      "      Loading fonts      ", 320, 460, General_COLOR_TEXT );
   datffont = load_datafile_callback ("datafile/font.dat",
InitProc_datafile_editor_loading);

   textout_centre_old ( subscreen, font,
      "  Loading Editor Images  ", 320, 460, General_COLOR_TEXT );
   datfeditor = load_datafile_callback ("datafile/editor.dat",
InitProc_datafile_editor_loading);

   text_mode_old ( -1 );
   if ( datfmaze == NULL || datfimage == NULL || datfaudio == NULL )
      return ( INIT_FAILURE );


   clear ( buffer );

//   maze.reference_bitmap ();

   config.load();
   config.initialise ();

//   load_database ();

//   return ( INIT_SUCCESS );
  printf ("--------------------------------------------------\r\n");

   return ( status );
}*/


int init_keyboard ( void )
{
   int value;

   printf ("- Adding keyboard\r\n");
   value = install_keyboard ();
   if ( value != 0 )
      return ( INIT_FAILURE );
   else
      return ( INIT_SUCCESS );
}

int init_timer ( void )
{
   int value;

   printf ("- Adding timers\r\n");
   value = install_timer ();
   if ( value != 0 )
      return ( INIT_FAILURE );
   else
      return ( INIT_SUCCESS );
}


int init_sound ( void )
{
   int value;
   //int i;
   char cwd [ 513 ];
   char tmpstr [ 551];
   //char *cwdptr = cwd;

   /*if ( detect_midi_driver ( MIDI_DIGMID ) > 0)
      printf ("Digimid is available\n");

   if ( detect_midi_driver ( MIDI_OSS ) > 0)
      printf ("Midi Open Sound is available\n");

   if ( detect_midi_driver ( MIDI_ALSA ) > 0)
      printf ("Midi Alsa is available\n");*/

   getcwd ( cwd, 513 );

   //printf ( "cwd = %s\n", cwd);

   sprintf ( tmpstr, "%s/datafile/digimid.dat", cwd);

   //printf ( tmpstr);

   set_config_string ( "sound", "patches", tmpstr );

   set_config_string ( "sound", "midi_volume", "-1" );
   set_config_string ( "sound", "midi_voices", "-1" );


   //set_config_string ( "sound", "patches", "digmid.dat");

   printf ("- Adding Digital and Midi audio\r\n");

   if ( config.get (  Config_SYSTEM_MIDI ) == Config_SYS_DIGIMID)
   {
      value = install_sound ( DIGI_AUTODETECT, MIDI_DIGMID, NULL);
      printf ("  . Forcing Digimid Driver\r\n");
   }
   else
      value = install_sound ( DIGI_AUTODETECT, MIDI_AUTODETECT, NULL);

   if ( value != 0 )
   {
      printf ( "ERROR: %s\n", allegro_error);

      return ( INIT_FAILURE );
   }
   else
   {


      return ( INIT_SUCCESS );
   }

   //midi_card = DIGI
   //patches =

}

int init_other ( void )
{

   printf ("- Setting up random seed\r\n");
   #ifndef ALLEGRO_WINDOWS
      srandom ( time(NULL) );
   #else
      srand ( time(NULL) );
   #endif

//   #if ALLEGRO_PLATFORM_STR == ALLEGRO_WINDOWS
//   #if VAR == ALLO
//   #endif

   printf ("- Setting up display switch mode\r\n");
   set_display_switch_mode ( SWITCH_PAUSE );

   set_window_title("Wizardry Legacy");

   return ( INIT_SUCCESS );
}

int init_graphic ( void )
{
   int value;
   //int gfxID = -1;

   printf ("- Adjusting Graphic Parameters\r\n");
   set_color_depth ( 16 );
   set_projection_viewport ( 0 , 0 , 640 , 480 );
   set_trans_blender( 0, 0, 0, 255 );
   text_mode_old ( -1 );

   printf ("- Changing graphic mode\r\n");

   /*printf ("  Testing Optimal Graphic Mode\r\n");
   while ( value != 0 && gfxID < Init_NB_GFXMODE )
   {
      gfxID++;
      printf ( "  ... Testing %dx%d", Init_GFXMODE [ gfxID ] . w,
              Init_GFXMODE [ gfxID ] . h );
      value = set_gfx_mode ( GFX_AUTODETECT,
                            Init_GFXMODE [ gfxID ] . w ,
                            Init_GFXMODE [ gfxID ] . h , 0, 0 );

   }*/

   // use value from config
   //value = set_gfx_mode ( GFX_AUTODETECT, 1024, 576, 0, 0 );

   value = set_gfx_mode ( GFX_AUTODETECT,
            Init_GFXMODE [ config.get (  Config_SYSTEM_RESOLUTION ) ] . w ,
            Init_GFXMODE [ config.get (  Config_SYSTEM_RESOLUTION ) ] . h ,
            0, 0 );

   if ( value != 0 )
      return ( INIT_FAILURE );
   else
   {
      printf ("- Using resolution %dx%d\n", SCREEN_W, SCREEN_H );
      printf ("- Creating Screen buffers\r\n");
      buffer = create_bitmap ( SCREEN_W, SCREEN_H );
      backup = create_bitmap ( SCREEN_W, SCREEN_H );
      mazebuffer = create_bitmap (640, 480);
      editorbuffer = create_bitmap (640, 480);


      //triplebuffer [ 0 ] = create_bitmap ( SCREEN_W, SCREEN_H );
      //triplebuffer [ 1 ] = create_bitmap ( SCREEN_W, SCREEN_H );
      //bufferid = 0;



      if ( buffer == NULL )
         return ( INIT_FAILURE );
      if ( backup == NULL )
         return ( INIT_FAILURE );
      if ( mazebuffer == NULL )
         return ( INIT_FAILURE );
      if ( editorbuffer == NULL )
         return ( INIT_FAILURE );
      /*if ( triplebuffer [ 0 ] == NULL )
         return ( INIT_FAILURE );
      if ( triplebuffer [ 1 ] == NULL )
         return ( INIT_FAILURE );*/


      clear ( buffer );
      clear ( backup );
      clear ( mazebuffer );
      clear ( editorbuffer );


      if ( SCREEN_W > 640 )
      {
         System_X_OFFSET = ((SCREEN_W - 640) / 2);
         System_Y_OFFSET = ((SCREEN_H - 480) / 2);
         //printf ("debug: system_X_offset= %d\r\n", System_X_OFFSET);
         //printf ("debug: system_Y_offset= %d\r\n", System_Y_OFFSET);
      }

      printf ("- Creating sub-screens\r\n");
      subbuffer = create_sub_bitmap ( buffer, System_X_OFFSET, System_Y_OFFSET, 640, 480 );
      subscreen = create_sub_bitmap ( screen, System_X_OFFSET, System_Y_OFFSET, 640, 480 );
      subbackup = create_sub_bitmap ( backup, System_X_OFFSET, System_Y_OFFSET, 640, 480 );;

      printf ("- Hooking switch callbacks\r\n");
      set_display_switch_callback( SWITCH_IN, switch_in_callback);
      set_display_switch_callback( SWITCH_OUT, switch_out_callback);

      printf ("- Initializing Texture Palettes\r\n");
      mazepal.create();
      editorpal.create();
      palsample.create();

      // setting antialliasing mode
      aa_set_trans ( AA_MASKED );


      return ( INIT_SUCCESS );
   }

}

int init_data ( void )
{
   int i;

//   DATAFILE *tmpfnt2;
//   DATAFILE *tmpfnt;

//   tmpfnt2 = load_datafile_object ("wizardry.dat", "FNT_SCRIPT" );
//   tmpfnt = load_datafile_object ("wizardry.dat", "FNT_STYLE" );

//   if ( tmpfnt2 == NULL || tmpfnt == NULL )
//      return ( INIT_FAILURE );


//   textout_centre_old ( buffer, (FONT*) tmpfnt->dat, "Loading", 320, 250,
//                                                   General_COLOR_TEXT );

   clear ( screen );

   printf ("- Loading Datafiles\r\n");

   //rect ( subscreen, 0, 0, 639, 479, General_COLOR_TEXT);

   textout_centre_old ( subscreen, font,
      "Prepare yourself for the ultimate fantasy game!",
      320, 230, General_COLOR_TEXT );

   textprintf_centre_old ( subscreen, font, 320, 360, General_COLOR_TEXT,
      "Wizardry Legacy  v%s  %s", VERSION, YEAR );



   rect ( subscreen, 218, 398, 421, 416, General_COLOR_TEXT );

   text_mode_old ( 0 );

// old datafile loading method

   /*textout_centre_old ( subscreen, font,
      "  Loading Maze Textures  ", 320, 460, General_COLOR_TEXT );
   datfmaze = load_datafile_callback ("datafile/mazetex.dat",
InitProc_datafile_loading);*/

   /*textout_centre_old ( subscreen, font,
      " Loading Ennemy Pictures ", 320, 460, General_COLOR_TEXT );
   datfennemy = load_datafile_callback ("datafile/ennemy.dat",
InitProc_datafile_loading);*/

  /* textout_centre_old ( subscreen, font,
      "  Loading Various Images ", 320, 460, General_COLOR_TEXT );
   datfimage = load_datafile ("datafile/image.dat" );*/

   /*textout_centre_old ( subscreen, font,
      "      Loading Fonts      ", 320, 460, General_COLOR_TEXT );
   datffont = load_datafile ("datafile/font.dat" );*/

  /* textout_centre_old ( subscreen, font,
      "   Loading Audio Files   ", 320, 460, General_COLOR_TEXT );
   datfaudio = load_datafile ("datafile/audio.dat" );*/

   /*textout_centre_old ( subscreen, font,
      "  Loading Editor Images  ", 320, 460, General_COLOR_TEXT );
   datfeditor = load_datafile_callback ("datafile/editor.dat",
InitProc_datafile_editor_loading);*/

/*textout_centre_old ( subscreen, font,
      "  Loading Character Images  ", 320, 460, General_COLOR_TEXT );
   datfcharacter = load_datafile("datafile/character.dat" );
*/

// --- New loading system for new datafile format ---

datafile_total_size = 0;
double progress = 2;
double chunk = 0;
//int eval = 0;

for ( i = 0 ; i < System_NB_DATAFILE ; i++)
{
   if ( strcmp ( datafilelist [ i ] . filename , "") != 0 )
   {
      datafilelist [ i ] . filesize = file_size_ex ( datafilelist [ i ] . filename);
      datafile_total_size += datafilelist [ i ] . filesize;
      //printf ("Sizing %s\n", datafilelist [ i ].filename );
      //printf ("total size is=%d, File Size=%d\n", total_size, datafilelist [ i ] . filesize );
      //printf ( "error number: %d\n", errno );

   }
}


for ( i = 0 ; i < System_NB_DATAFILE ; i++ )
{
   if ( strcmp ( datafilelist [ i ] . filename, "") != 0 )
   {
      textout_centre_old ( subscreen, font,
         datafilelist [ i ].display_text , 320, 460, General_COLOR_TEXT );

      datafilelist [ i ] . datf = load_datafile
         ( datafilelist [ i ] . filename );
      //printf ("Loading %s\n", datafilelist [ i ].filename );
      if ( datafilelist [ i ] . datf == NULL )
      {
         printf ("Error: Could not load: [%d] %s\n", i, datafilelist [ i ] . filename );
         return ( INIT_FAILURE );
      }

      //printf ("total size is=%d", total_size);
      chunk = ( datafilelist [ i ] . filesize * 200 ) ;
      chunk = chunk / datafile_total_size;
      progress += chunk;

      //progress++;
      //eval = ( progress * 200 ) / total_size;
      //if ( eval > drawval )
      //{
         //drawval = eval;

      rectfill ( subscreen, 219, 399, 219 + (int) progress, 415, makecol ( 225, 175, 0 ) );
      //rest (1000);
      //}

      datafilelist [ i ] . loaded = true;



   }

}

// referencing data (new datafile system)


//note: maybe can place reference directly inside datafilelist.
//note: hard to do because of typing of template class. Would need a pointer on a reference class.

//datafile_monster = load_datafile ("datafile/monster.dat");


   no_picture = create_bitmap ( 64, 64 );
   clear_bitmap (no_picture);


   text_mode_old ( -1 );
/*   if ( datfmaze == NULL || datfimage == NULL || datfaudio == NULL || datfeditor == NULL
       || datfennemy == NULL || datffont == NULL)
      return ( INIT_FAILURE );*/

   for ( i = 0 ; i < System_NB_DATAFILE ; i++ )
   {
      if ( strcmp ( datafilelist [ i ] . filename, "") != 0 )
      {
         if ( datafilelist [ i ] . loaded ==  false )
         {
            printf ("Error: Missing loaded datafile: [%d] %s\n", i, datafilelist [ i ] . filename );
            return ( INIT_FAILURE );
         }
      }
   }




   //rest ( 2000 );
   clear ( buffer );

//   textout_centre_old ( buffer, font, "Loading adventure : newadv.wza", 320, 200, General_COLOR_TEXT );

//   copy_buffer();

//   adatf = load_datafile ( "newadv.wza" );

   printf ("- Referencing Datafiles\r\n");
   maze.reference_bitmap ();
   datref_monster.add ( datafilelist [DATF_MONSTER] . datf , 1 );
   datref_monster.add ( datafilelist [DATF_UNIDENTIFIED] . datf , 2 );

   datref_texture.add ( datafilelist [DATF_TEXHH_FLOOR] . datf , 1);
   datref_texture.add ( datafilelist [DATF_TEXHH_WALL] . datf, 2);
   datref_editoricon.add ( datafilelist [DATF_EDITOR_ICON] . datf, 0);
   datref_sky.add (datafilelist [DATF_SKY] . datf, 0 ); // set index 1 if adventure can have sky
   datref_image.add ( datafilelist [DATF_VARIOUSIMAGE] . datf, 0 );
   datref_image.add ( datafilelist [DATF_WINTEX] . datf, 1 );
   datref_portrait.add ( datafilelist [ DATF_PORTRAITW8 ] . datf, 1 );
   datref_font.add ( datafilelist [ DATF_FONT] . datf, 0 );
   datref_character.add ( datafilelist [ DATF_CHARACTER] . datf, 1);
   datref_object.add ( datafilelist [ DATF_OBJECT] . datf, 1);
   datref_object.add ( datafilelist [ DATF_OBJECTHERHEX] . datf, 2);
   datref_musicmidi.add ( datafilelist [ DATF_MUSICMIDI] . datf, 0);
   datref_texture.add ( datafilelist [DATF_TEXDAVEGH] . datf, 4);
   datref_sound.add ( datafilelist [ DATF_SOUNDWIZ] . datf, 1);

   maze.build_sky_bitmap();
   maze.build_smoke_bitmap();

   //datref_texture[Texture_SET_HERHEXEN][Texture_CATEGORY_WALL].add ( datafilelist [DATF_TEXHH_WALL] . datf , 0, 0, 300 );

   /*define Texture_TYPE_UNDEFINED      -1 // mostly indicate that the texture has not been defined, so ignore.
#define Texture_TYPE_WALL           0
#define Texture_TYPE_FLOOR          1 // include ceiling
#define Texture_TYPE_MASKEDWALL     2
#define Texture_TYPE_MASKEDFLOOR    3 // include ceiling
#define Texture_TYPE_GRID           4*/
/*#define Texture_TYPE_OBJECTFLOOR    6
#define Texture_TYPE_OBJECTCEILING  7
#define Texture_TYPE_OBJECTWALL     8*/


// --- texture sets ---
/*#define Texture_SET_UNDEFINED       -1 // determine that the texture has not been defined
#define Texture_SET_ADVENTURE       0 // each adventure can be openned as a texture set
#define Texture_SET_HERHEXEN        1 */


   printf ("- Initializing Default configuration\r\n");
   config.initialise ();

  /* printf ("- Referencing Musics\r\n");
      for ( i = 0 ; i < System_NB_MUSIC ; i++ )
      {
         MUSIC_LIST [ i ] . music = ( MIDI* ) datfaudio [ MID_MUSIC000 + i ] . dat;
      }*/

   //init_database ();

   return ( INIT_SUCCESS );
}

void exit_game ( void )
{
   int i;

   set_gfx_mode ( GFX_TEXT, 80, 25, 0, 0 );

   printf ("--------------------------------------------------\r\n");
   printf ("Shutting down Wizardry\r\n");

   printf ("- Saving Game Configuration\r\n");
   config.save();

//   printf ("- Saving System Database\r\n");
//   db.save_dba_file ( "system.dba", DBSOURCE_SYSTEM );


   printf ("- Unloading Datafiles\r\n");
   //unload_datafile ( datfmaze );
//   unload_datafile ( datfimage );
//   unload_datafile ( datfaudio );
   //unload_datafile ( datfennemy );
   //unload_datafile ( datffont );
   //unload_datafile ( datfeditor );
   //unload_datafile ( datfcharacter );
   //unload_datafile ( adatf );

   for ( i = 0 ; i < System_NB_DATAFILE ; i++ )
   {
      if (  datafilelist [ i ] . loaded == true )
      {
         unload_datafile ( datafilelist [ i ] . datf );
         datafilelist [ i ] . loaded = false;
      }
   }

   //unload_datafile ( datafile_monster );
   printf ("- Destroying Texture Palettes\r\n");
   printf ("  . Maze Palette\r\n");
   mazepal.destroy();
   printf ("  . Editor Palette\r\n");
   editorpal.destroy();
   printf ("  . Palette sample\r\n");
   palsample.destroy();

   printf ("- Unloading Buffers and Sub-screens\r\n");
   destroy_bitmap ( subbuffer );
   destroy_bitmap ( subbackup );
   destroy_bitmap ( subscreen );
      destroy_bitmap ( buffer );
      destroy_bitmap ( backup );
      destroy_bitmap ( mazebuffer );
      destroy_bitmap ( editorbuffer );
      //destroy_bitmap ( triplebuffer [ 0 ] );
      //destroy_bitmap ( triplebuffer [ 1 ] );

   //printf ("- Destroying texture palette\r\n");
   //destroy_texture_palette (texpal); // just in case

   printf ("- Shutting down Allegro\r\n");
   allegro_exit ();
}

/*void exit_setup ( void )
{
   printf ("--------------------------------------------------\r\n");
   set_gfx_mode ( GFX_TEXT, 80, 25, 0, 0 );
   printf ("Shutting down Wizardry Setup\r\n");
   printf ("- Shutting down Allegro\r\n");
   allegro_exit ();
}

void exit_editor ( void )
{
   printf ("--------------------------------------------------\r\n");
   set_gfx_mode ( GFX_TEXT, 80, 25, 0, 0 );
   printf ("Shutting down Wizardry Editor\r\n");
   printf ("- Shutting down Allegro\r\n");
   printf ("- Unloading Datafile\r\n");
//   unload_datafile ( datf );
   allegro_exit ();
}*/


/*int init_database ( void )
{
//   s_Weapon_file* tmpw;
//   s_Armor_file* tmpa;
   int i;


//   DBObject::set_database ( db );



   db.load_dba_file ("datafile//wizardry.dba", DBSOURCE_GAME );

   if ( exists ( "system.dba" ) != 0 )
      db.load_dba_file ( "system.dba", DBSOURCE_SYSTEM );

   db.default_source ( DBSOURCE_SYSTEM );





}*/

/*int init_sqldatabase( void)
{
   int answer;

   //will eventually need to be removed and instead copy and load the save game. Only the default
   // options could be saved in this database.
   answer = SQLopen ( "wizardry.sqlite");//sqlite3_open_v2("testdb.sqlite", &sqldb, SQLITE_OPEN_READWRITE, NULL );

   SQLactivate_errormsg();

   if (answer == SQLITE_OK)
      return INIT_SUCCESS;
   else
   {
      SQLclose ();
      return INIT_FAILURE;

   }
}*/

void InitProc_datafile_loading ( DATAFILE *f )
{
   static double progress = 0;
   static double drawval = 0;
   static double eval = 0;
   //short max = ZZZ_END_OF_AUDIO + ZZZ_END_OF_MAZETEX + ZZZ_END_OF_IMAGE
     // + ZZZ_END_OF_FONT + ZZZ_END_OF_ENNEMY + ZZZ_END_OF_EDITOR + ZZZ_END_OF_CHARACTER;

/*chunk = ( datafilelist [ i ] . filesize * 200 ) ;
      chunk = chunk / datafile_total_size;
      progress += chunk;*/

   // bug: size use the uncompresses size instead of the compressed size.
   // will need to add a data object with the count inside the datafile while
   // using the script to build the datafiles. Not sure I really want that.
   progress += f->size;
   eval = ( progress * 100 ) / datafile_total_size;
   if ( eval > drawval )
   {
      drawval = eval;
      rectfill ( subscreen, 219, 399, 219 + (int) drawval, 415, makecol ( 225, 175, 0 ) );
   }
   printf ( "Max Size= %12f | progress= %12f | eval= %3f%%\n", datafile_total_size, progress, eval );

}

/*void InitProc_datafile_editor_loading ( DATAFILE *f )
{
   static unsigned int progress = 0;
   static unsigned short drawval = 0;
   static unsigned int eval = 0;
   short max = ZZZ_END_OF_MAZETEX + ZZZ_END_OF_FONT + ZZZ_END_OF_EDITOR;

   //?? note: image is loaded because it contains fonts
   progress++;
   eval = ( progress * 200 ) / max;
   if ( eval > drawval )
   {
      drawval = eval;
      rectfill ( subscreen, 219, 399, 219 + drawval, 415, makecol ( 225, 175, 0 ) );
   }


}*/

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

s_Init_gfxmode Init_GFXMODE [ Init_NB_GFXMODE ] =
{
  { 1024, 768 },
  { 1024, 600 },
  { 1024, 576 },
  {  800, 600 },
  {  640, 480 }
};

double datafile_total_size;

/*
Done! extern char allegro_id[];

Done! extern char allegro_error[ALLEGRO_ERROR_SIZE];

Done! extern int os_type;

Done! extern int os_version;
Done! extern int os_revision;

Done! extern int cpu_family;

??? not sure
void allegro_message(char *msg, ...);
   Outputs a message, using a printf() format string. This function must
   only be used when you aren't in graphics mode, eg. before calling
   set_gfx_mode(), or after a set_gfx_mode(GFX_TEXT). On platforms that have
   a text mode console (DOS, Unix and BeOS) it will print the string to the
   console, attempting to work around codepage differences by reducing any
   accented characters to 7 bit ASCII approximations, or under Windows it
   will bring up a GUI message box.

??? not sure
extern int cpu_model;
   Contains the Intel CPU submodel, where applicable. On a 486
   (cpu_family=4), zero or one indicates a DX chip, 2 an SX, 3 a 487 (SX) or
   486 DX, 4 an SL, 5 an SX2, 7 a DX2 write-back enhanced, 8 a DX4 or DX4
   overdrive, 14 a Cyrix, and 15 is unknown. On a Pentium chip
   (cpu_family=5), 1 indicates a Pentium (510\66, 567\66), 2 is a Pentium
   P54C, 3 is a Pentium overdrive processor, 5 is a Pentium overdrive for
   IntelDX4, 14 is a Cyrix, and 15 is unknown.

ALLEGRO_PLATFORM_STR
   text string
*/



