/***************************************************************************/
/*                                                                         */
/*                              S Y S T E M . C P P                        */
/*                             Module Definition                           */
/*                                                                         */
/*     Content : Module System                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : April 26th, 2012                                    */
/*                                                                         */
/*          System procedure and variables not related to allegro.         */
/*                                                                         */
/***************************************************************************/

// Group Includes
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>

#include <stdio.h>

unsigned int hdice ( unsigned int maxvalue )
{
    unsigned int rest;
    unsigned int roll;

    rest = maxvalue - ( maxvalue / 2 );
    roll = rnd ( maxvalue / 2 );

    return ( rest + roll );
}

unsigned int dice ( unsigned int maxvalue )
{
    if ( maxvalue > 0 )
        return ( rnd ( maxvalue ) );
    else
        return ( 0 );
}

unsigned int rngdice ( unsigned int minvalue, unsigned int maxvalue )
{
    unsigned int range;

    range = maxvalue - minvalue;

    return ( rnd ( range ) + minvalue );


}

int roll_xdypz ( int x, int y, int z)
{
    int result = 0;
    int i;

    for ( i = 0; i < x; i++)
    {
        result += dice (y) + z;
    }

    if ( result < 1 )
        result = 1;

    if ( y == 0)
        result = 0;

    return result;

}

int roll_xpypd20 ( int x, int y )
{
    int result = 0;

    result = x + dice(20) + y;

    if ( result < 1 )
        result = 1;

    return result;
}

int bitoint ( unsigned int value )
{
    int i = 0; // safety
    int intvalue = 1;

    if ( value > 0)
    {
        while ( ( value & 0x0001 ) != 1 && i<32)
        {
            value = value>>1;
            intvalue++;
            i++;
            //printf ("intobit: value=%d, i=%d, intvalue=%d masked=%d\r\n", value, i, intvalue,( value & 0x0001) );

        }


    }
    else
        return ( 0 );

    return ( intvalue );
}

unsigned int intobit ( int value )
{
    int i;
    unsigned int bitvalue = 1;

    if ( value > 0 )
    {

        for ( i = 1; i < value; i++)
        {
            bitvalue = bitvalue<<1;
            //printf ("intobit: value=%d, i=%d, bitvalue=%d\r\n", value, i, bitvalue);
        }
    }
    else
        return (0);

    return (bitvalue);
}

int bitoindex ( unsigned int value )
{
    return ( bitoint(value) - 1);
}

unsigned int indextobit ( int value )
{
    return ( intobit( value +1) );
}

void debug_printf ( const char *function, const char *format,  ... )
{
   va_list arguments;
   //int i;
   char tmpstr[200] = "";


   va_start (arguments, format);
   vsprintf (tmpstr,format, arguments);
   va_end (arguments);

   printf ("\r\nDebug - %s: %s", function, tmpstr );
}

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

BITMAP *buffer; // create a double buffer
BITMAP *backup; // create a screen backup
BITMAP *mazebuffer;
BITMAP *editorbuffer;
BITMAP *subbuffer; // sub bitmap of the buffer for the center of the screen
BITMAP *subscreen; // sub bitmap for screen.
BITMAP *subbackup;

//BITMAP *last_buffer; // point on the previously used buffer.
//BITMAP *triplebuffer[2]; // create 2 buffer ans switch from one to another
//int bufferid = 0; // number of the current used buffer.


int SELECT_KEY = KEY_Z;
int CANCEL_KEY = KEY_A;
int DISPLAY_KEY = KEY_ENTER;

//DATAFILE *datfaudio; // Audio datafile
//DATAFILE *datfmaze; // Maze textures datafile
//DATAFILE *datfimage; // Image datafile
//DATAFILE *datfennemy; // Ennemy Images
//DATAFILE *datfeditor; // Editor datafile
//DATAFILE *datffont; // font datafile
//DATAFILE *adatf; // main datafile
//DATAFILE *datfcharacter;

//DATAFILE *datafile_monster; // monster pictures (new format)

DatafileReference<BITMAP*> datref_monster;
DatafileReference<BITMAP*> datref_texture;
DatafileReference<BITMAP*> datref_editoricon;
DatafileReference<BITMAP*> datref_sky;
DatafileReference<BITMAP*> datref_image;
DatafileReference<BITMAP*> datref_portrait;
DatafileReference<FONT*>   datref_font;
DatafileReference<BITMAP*> datref_character;
DatafileReference<MIDI*>   datref_musicmidi;
DatafileReference<BITMAP*> datref_object;
DatafileReference<BITMAP*> datref_unidentified;
DatafileReference<SAMPLE*> datref_sound;
DatafileReference<BITMAP*> datref_storyimg;
//DatafileReference<BITMAP*> datref_floorceiling;

//BITMAP *datref2_monster [ 256 ];



//note: Don't forget to unload them on exit.

//Database db; // database engine

s_music_track MUSIC_LIST [ System_NB_MUSIC ] =
{
    { " 0-[W1-camp]     Music                        " },//w1camp.mid
    { " 1-[W1-city]     Music                        " },//w1city.mid
    { " 2-[W1-fight]    Engaging the Ennemy          " },//w1combat.mid
    { " 3-[W1-credit]   Celebrating the Victory      " },//w1credit.mid
    { " 4-[W1-dungeon]  Mystery in the Darkness      " },//w1dungon.mid
    { " 5-[W1-edgetown] Music                        " },//w1edge.mid
    { " 6-[W1-ending]   The Ascension Ceremony       " },//w1ending.mid
    { " 7-[W1-inn]      Stories around the fireplace " },//w1inn.mid
    { " 8-[W1-intro]    Music                        " },//w1intro.mid
    { " 9-[W1-lvl10]    Nightmare of the 10th Level  " },//w1lvl10.mid
    { "10-[W1-shop]     Music                        " },//w1shop.mid
    { "11-[W1-tavern]   Music                        " },//w1tavern.mid
    { "12-[W1-temple]   Hoping for the Best          " },//w1temple.mid
    { "13-[W1-victory]  Victory is ours !            " },//w1victor.mid
    { "14-[W2-camp]     Music                        " },//w2camp.mid
    { "15-[W2-city]     Music                        " },//w2city.mid
    { "16-[W2-death]    Helas! He has passed away    " },//w2death.mid
    { "17-[W2-dungeon]  Puzzling Exploration         " },//w2dungon.mid
    { "18-[W2-edgetown] Music                        " },//w2edge.mid
    { "19-[W2-ending]   Music                        " },//w2ending.mid
    { "20-[W2-fight]    Dreadfull Fight              " },//w2fight.mid
    { "21-[W2-inn]      Warm Sweet Home              " },//w2inn.mid
    { "22-[W2-intro]    Tales and Legends            " },//w2intro.mid
    { "23-[W2-shop]     It's now on sale !           " },//w2shop.mid
    { "24-[W2-tavern]   Music                        " },//w2tavern.mid
    { "25-[W2-temple]   Music                        " },//w2temple.mid
    { "26-[W3-boss]     L'Kbreth                     " },//w3boss.mid
    { "27-[W3-camp]     Adventurer's in the Maze     " },//w3camp.mid    //camp fields
    { "28-[W3-city]     Legacy of Llylgamyn          " },//w3city.mid    // Legacy of Llylgamyn
    { "29-[W3-death]    The Fallen Heroes            " },//w3death.mid   // all is dead
    { "30-[W3-dungeon]  Dungeon Theme                " },//w3dungon.mid  // DUngeon Theme
    { "31-[W3-edgetown] Edge of Town                 " },//w3edge.mid    // Edge of Town
    { "32-[W3-ending]   Ending Theme                 " },//w3ending.mid  // ending theme
    { "33-[W3-fight]    Unexpected Encounter         " },//w3fight.mid   //Fighting Sights
    { "34-[W3-inn]      Adventurer's Gathering       "},//w3inn.mid     // Rest of Adventurer's inn
    { "35-[W3-intro]    The Legacy Continues         "},//w3intro.mid   // Openning Theme
    { "36-[W3-shop]     Boltac Trading Post corner   "},//w3shop.mid    // Boltac Trading post corner
    { "37-[W3-tavern]   Gilgamesh Tavern             "},//w3tavern.mid  // Gilgamesh Tavern 2ns scene
    { "38-[W3-temple]   Temple of Cant               "},//w3temple.mid  // temple of cant second scene
};

s_font_list FONT_LIST [ System_NB_FONT ]=
{
    /*{ "Ambrosia 24       ", FNT_AMBROSIA24 },
    { "Black Chancery 16 ", FNT_BLACKCHANCERY16 },
    { "Black Chancery 24 ", FNT_BLACKCHANCERY24 },
    { "Elgar 24          ", FNT_ELGAR24 },
    { "Elgar 32          ", FNT_ELGAR32 },
    { "Freehand 16       ", FNT_FREEHAND16 },
    { "Freehand 24       ", FNT_FREEHAND24 },
    { "Helena 24         ", FNT_HELENA24 },
    { "Helvetica 22      ", FNT_HELVETICA22 },
    { "Kaufman 16        ", FNT_KAUFMAN16 },
    { "Kaufman 24        ", FNT_KAUFMAN24 },*/
    { "Print                        "},
    { "Script                       "},
    { "Small                        "},
    { "Book Antiqua Bold 16         "},
    { "Book Antiqua Bold 24         "},
    { "Bookman Old Style 16         "},
    { "Bookman Old Style 24         "},
    { "Deutschegothic Bold 32       "},
    { "Deutschegothic Bold 48       "},
    { "Elementary Gothic Bookhand 24"},
    { "Elementary Gothic Bookhand 32"},
    { "Haettenscheweiler 16         "},
    { "Haettenscheweiler 24         "},
    { "Helena 24                    "},
    { "Helena 32                    "},
    { "Monotype Corsiva 16          "},
    { "Monotype Corsiva 24          "},
    { "Vecker Bold 16               "},
    { "Vecker Bold 24               "},
    { "Verdana Bold 16              "},
    { "Verdana Bold 24              "},
    { "Water Gothic 32              "},
    { "Water Gothic 48              "}
};

int System_X_OFFSET = 0;
int System_Y_OFFSET = 0;
//int System_CASCADE = 20;


//note: duplicate info in "elemental_property"
const char STR_SYS_PROPERTY [][21] =
{
    {"Physical"},
    {"Mental"},
    {"Fire"},
    {"Ice"},
    {"Lightning"},
    {"Poison"},
    {"Undefined"},
    {"Undefined"},

    {"Body Effect"},
    {"Bloodstream Effect"},
    {"Soul Effect"},
    {"Neural Effect"},
    {"Psychic Effect"},
    {"Skin Effect"},
    {"Undefined"},
    {"Undefined"},

    {"Dragon"},
    {"Undead"},
    {"Mythical"},
    {"Divine"},
    {"Dire"},
    {"Magic"},
    {"Were"},
    {"Undefined"},

    {"Warrior"},
    {"Rogue"},
    {"Magic User"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"}


};

int DIE_SIZE [7] = { 2, 4, 6, 8, 10, 12, 20 }; // normally use value from 1 to 5 not 0 to 4.

const char STR_SYS_D20STAT [ D20_NB_STAT ][21] =
{
    {"Active Defense  (AD)"},
    {"Magic Defense   (MD)"},
    {"Physical Saves  (PS)"},
    {"Mental Saves    (MS)"},
    {"MeleeCmbt.Skill(MCS)"},
    {"RangeCmbt.Skill(RCS)"},
    {"Damage Resist.  (DR)"},
    {"Initiative      (IN)"},
};

const char STR_SYS_XYZSTAT [ XYZ_NB_STAT ][21] =
{
    {"Weapon Damage  (DMG)"},
    {"Magic Damage  (MDMG)"},
    {"HP Dice        (HPD)"},
    {"MP Dice        (MPD)"},
};

extern const char STR_TEXTURE_SET [ System_NB_TEXTURE_SET ][ 21 ] =
{
    {"Adventure"},
    {"Hexen/Heretic Floor"},
    {"Hexen/Heretic Wall"},
    {"Basic"},
    {"Dave GH"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},

};

const char STR_SYS_ATTRIBUTE [ 6 ][15] =
{
   {"STRength"},
   {"DEXterity"},
   {"ENDurance"},
   {"INTelligence"},
   {"CUNning"},
   {"WISdom"}
};

const char STR_SYS_ATTRIBUTE_CODE [ 6 ][15] =
{
   {"STR"},
   {"DEX"},
   {"END"},
   {"INT"},
   {"CUN"},
   {"WIS"}
};

s_System_datafile_list_item datafilelist [ System_NB_DATAFILE ] =
{
    // note: display text must be the same with to overlap characters
    { NULL, "datafile//monster.dat", false,      "      Loading monster pictures     ", 0 }, // 0 DATF_MONSTER
    { NULL, "datafile//texhhfloor.dat", false,   "Loading Hexen/Heretic Floor/Ceiling", 0 }, // 1 DATF_TEXHH_FLOOR
    { NULL, "datafile//texhhwall.dat", false,    "Loading Hexen/Heretic Wall Patches ", 0 }, // 2 DATF_TEXHH_WALL
    { NULL, "datafile//editoricon.dat", false,   "      Loading Map editor icons     ", 0 }, // 3 DATF_EDITOR_ICON
    { NULL, "datafile//sky.dat", false,          "        Loading sky Pictures       ", 0 }, // 4
    { NULL, "datafile//variousimage.dat", false, "       Loading Various Image       ", 0 }, // 5
    { NULL, "datafile//windowtex.dat", false,    "      Loading Dialog Textures      ", 0 }, // 6 DATF_WINTEX
    { NULL, "datafile//portraitwiz8.dat", false, "   Loading Wizardry 8 Portraits    ", 0 }, // 7 DATF_PORTRAITW8
    { NULL, "datafile//font.dat", false,         "           Loading Fonts           ", 0 }, // 8 DATF_FONT
    { NULL, "datafile//cclass.dat", false,       "     Loading Class Characters      ", 0 }, // 9 DATF_CHARACTER
    { NULL, "datafile//musicmidi.dat", false,    "        Loading MIDI music         ", 0 }, // 10 DATF_MUSICMIDI
    { NULL, "datafile//object.dat", false,       "   Loading Wizardry Maze objects   ", 0 }, // 11 DATF_OBJECT
    { NULL, "datafile//unidentified.dat", false, "   Loading Unidentified Monsters   ", 0 }, // 12 DATF_UNIDENTIFIED
    { NULL, "datafile//texdavegh.dat", false,    "      Loading DaveGH Textures      ", 0 }, // 13 DATF_TEXDAVEGH
    { NULL, "datafile//objectherhex.dat", false, "Loading Hexen/Heretic Maze objects ", 0 }, // 14 DATF_OBJECTHERHEX
    { NULL, "datafile//soundwiz.dat", false,     "      Loading Wizardry Sounds      ", 0 }, // 15 DATF_SOUNDWIZ
    { NULL, "", false, "", 0 }, // 16
    { NULL, "", false, "", 0 }, // 17
    { NULL, "", false, "", 0 }, // 18
    { NULL, "", false, "", 0 }, // 19
    { NULL, "", false, "", 0 }, // 20
    { NULL, "", false, "", 0 }, // 21
    { NULL, "", false, "", 0 }, // 22
    { NULL, "", false, "", 0 }, // 23
    { NULL, "", false, "", 0 }, // 24
    { NULL, "", false, "", 0 }, // 25
    { NULL, "", false, "", 0 }, // 26
    { NULL, "", false, "", 0 }, // 27
    { NULL, "", false, "", 0 }, // 28
    { NULL, "", false, "", 0 }, // 29
    { NULL, "", false, "", 0 }, // 30
    { NULL, "", false, "", 0 }, // 31
};

s_System_Adventure_subdatafile advdatafilelist [ System_NB_ADVDATAFILE ] =
{
    { NULL, NULL, "IMG_STORY_DAT", false }, // 0 : ADATF_STORYIMG
    { NULL, NULL, "", false }, // 1 :
    { NULL, NULL, "", false }, // 2 :
    { NULL, NULL, "", false }, // 3 :
    { NULL, NULL, "", false }, // 4 :
    { NULL, NULL, "", false }, // 5 :
    { NULL, NULL, "", false }, // 6 :
    { NULL, NULL, "", false }, // 7 :
    { NULL, NULL, "", false }, // 8 :
    { NULL, NULL, "", false }, // 9 :
    { NULL, NULL, "", false }, // 10 :
    { NULL, NULL, "", false }, // 11 :
    { NULL, NULL, "", false }, // 12 :
    { NULL, NULL, "", false }, // 13 :
    { NULL, NULL, "", false }, // 14 :
    { NULL, NULL, "", false }, // 15 :
};

extern const char STR_SPELL_CURE [ 12 ] [ 15 ] =
{
    { "Body" },
    { "Blood Stream" },
    { "Soul" },
    { "Neural" },
    { "Psychic" },
    { "Skin" },
    { "???" },
    { "???" },
    { "???" },
    { "Spell" },
    { "Exped. Spell" },
    { "???" },
};

extern const char STR_AEFFECT_EXPIRATION [ 6 ] [ 15 ] =
{
   {"End of Turn"}, //ActiveEffect_EXPIRATION_ENDOFTURN
   {"Disturbed "}, // ActiveEffect_EXPIRATION_DISTURBED
   {"End of Combat"}, //ActiveEffect_EXPIRATION_ENDOFCOMBAT
   {"Exit or Rest"}, //ActiveEffect_EXPIRATION_EXITREST
   {"Permanent"}, //ActiveEffect_EXPIRATION_PERMANENT
   {"???"}
};

