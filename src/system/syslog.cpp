/******************************************************************************/
/*                                                                            */
/*                              S Y S L O G . C P                             */
/*                              class Definition                              */
/*                                                                            */
/*    class: Syslog                                                           */
/*    Programmer: Eric Pietrocupo                                             */
/*    Starting Date: October 14th, 2012                                       */
/*                                                                            */
/*         This class is used to manage a system log into memory. The log is  */
/*    is not saved to a file and only the 1024 most recent entries are kept   */
/*                                                                            */
/******************************************************************************/

#include <grpsys.h>
#include <grpstd.h>



/******************************************************************************/
/*                        Constructors and destructors                        */
/******************************************************************************/

Syslog::Syslog ( void )
{
   clear();
}

Syslog::~Syslog ( void )
{

}


/******************************************************************************/
/*                                 Methods                                    */
/******************************************************************************/

void Syslog::write ( const char *str )
{

   strncpy ( p_log [ p_index] , str, Syslog_STR_LEN );
   p_index++;
   p_nb_entry++;
   p_marker++;
   if ( p_index >= Syslog_NB_ENTRY)
      p_index = 0;

   if ( p_nb_entry > Syslog_NB_ENTRY)
      p_nb_entry = Syslog_NB_ENTRY;

}

void Syslog::writef ( const char *format, ... )
{
   va_list arguments;
   //int i;
   char tmpstr[Syslog_STR_LEN];


   va_start (arguments, format);
   vsprintf (tmpstr,format, arguments);
   va_end (arguments);

   write(tmpstr);
}

char *Syslog::read ( int offset )
{
   int tmpindex = p_index - offset - 1;
//   char tmpstr[121];

   // checking for loop around
   if ( tmpindex < 0 )
      tmpindex = Syslog_NB_ENTRY + tmpindex;

  // sprintf ( tmpstr, "[%d] %s", tmpindex, p_log [ tmpindex ] );

   return ( p_log [ tmpindex ] );
}

void Syslog::clear ( void )
{
   int i;
   p_index=0;
   p_nb_entry = 0;
   p_marker = -1;

   for ( i = 0 ; i < Syslog_NB_ENTRY ; i++)
      strcpy ( p_log [ i ], "");

}

void Syslog::mark ( void )
{
   p_marker = -1;
}

/******************************************************************************/
/*                              Global Variables                              */
/******************************************************************************/

Syslog system_log;
