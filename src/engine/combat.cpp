/***************************************************************************/
/*                                                                         */
/*                         C O M B A T . C P P                             */
/*                          Class Source Code                              */
/*                                                                         */
/*     Content : Class Combat source code                                  */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : July 2nd, 2002                                      */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h> //<tmpinc>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

#include <wdatproc.h>
#include <time.h>

/*
#include <allegro.h>




//#include <time.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>



//

//
//
//
//#include <list.h>

#include <opponent.h>
#include <charactr.h>
#include <ennemy.h>
#include <party.h>

#include <game.h>

//#include <city.h>
#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
#include <combat.h>
#include <window.h>
#include <windata.h>
#include <winempty.h>
#include <winmenu.h>
#include <wdatproc.h>
#include <wintitle.h>
#include <winmessa.h>
#include <winquest.h>
*/


/*-------------------------------------------------------------------------*/
/*-                      Constructors and Destructors                     -*/
/*-------------------------------------------------------------------------*/

Combat::Combat ( void )
{
   //clear();

   p_pictureID [ 0 ] = 1024;
   p_pictureID [ 1 ] = 1025;
   p_nb_picture = 2;
}

Combat::~Combat ( void )
{
   //clear();
}

/*-------------------------------------------------------------------------*/
/*-                           Property Methods                            -*/
/*-------------------------------------------------------------------------*/

/*int Combat::nb_character ( void )
{
   return ( p_nb_character );
}

int Combat::nb_ennemy ( void )
{
   return ( p_nb_ennemy );
} */

/*-------------------------------------------------------------------------*/
/*-                               Methods                                 -*/
/*-------------------------------------------------------------------------*/

/*void Combat::add_character ( int character )
{
   if ( p_nb_character < 6 )
   {
      p_character [ p_nb_character ] = character;
      p_nb_character++;
   }
} */

//void Combat::add_character_party ( int party )
//{
   /*Party tmparty;
   int i;

   p_char_party = party;
   tmparty.SQLselect ( party );

   i = 0;
   while ( i < tmparty.nb_character() )
   {
      p_character [ i ].SQLselect ( tmparty.FKcharacter ( i ) );
      i++;
   }
   p_nb_character = i;*/
//}
/*
void Combat::add_ennemy ( int ennemy )
{
   if ( p_nb_ennemy < 6 )
   {
      p_ennemy [ p_nb_ennemy ] = ennemy;
      p_nb_ennemy++;
   }
} */

/*void Combat::add_ennemy_party ( int party )
{

   Party tmparty;
   int i;

   p_enmy_party = party;
   tmparty.SQLselect ( party );

   i = 0;
   while ( i < tmparty.nb_character() )
   {
      p_ennemy [ i ].SQLselect ( tmparty.FKcharacter ( i ) );
      i++;
   }
   p_nb_ennemy = i;

}*/

/*void Combat::save_opponents ( void )
{



} */

/*void Combat::reselect_opponents ( void )
{
   int i;
   Party tmparty;
   p_nb_character = 0;
   p_nb_ennemy = 0;

   tmparty.SQLselect ( p_char_party );

   i = 0;
   while ( i < tmparty.nb_character() )
   {
      p_character [ i ].SQLselect ( tmparty.FKcharacter ( i ) );
      i++;
   }
   p_nb_character = i;

   tmparty.SQLselect ( p_enmy_party );

   i = 0;
   while ( i < tmparty.nb_character() )
   {
      p_ennemy [ i ].SQLselect ( tmparty.FKcharacter ( i ) );
      i++;
   }
   p_nb_ennemy = i;

}*/

/*void Combat::clear ( void )
{
  int i;

   p_nb_character = 0;
   p_nb_ennemy = 0;
   p_char_party = 0;
   p_enmy_party = 0;

   for ( i = 0 ; i < p_nb_character ; i++ )
   {
 //     p_character [ i ] . combatstr ("");
//      p_character [ i ].number(0);
//      p_ennemy [ i ].number(0);
   }
}*/

/*void Combat::add_reward ( void )
{
   int i;
   int divider = 0;
   int totalgold = 0;
   int totalexp = 0;
   char tmpstr [ 200 ];

   for ( i = 0 ; i < p_nb_ennemy ; i++ )
   {
      if ( p_ennemy [ i ] . status () != Opponent_STATUS_DISPELLED )
      {
         totalgold += p_ennemy [ i ] . gold();
         totalexp += p_ennemy [ i ] . reward_exp();
      }
   }

   for ( i = 0 ; i < p_nb_character ; i++ )
   {
      if ( p_character [ i ] . body () == Opponent_STATUS_ALIVE )
         divider++;
   }

   if ( divider == 0 )
      divider = 1;

   totalexp = totalexp / divider;
   totalgold = totalgold / divider;

   for ( i = 0 ; i < p_nb_character ; i++ )
   {
      if ( p_character [ i ] . body () == Opponent_STATUS_ALIVE )
      {
         p_character [ i ] . gain_exp ( totalexp );
       //  p_character [ i ] . gain_gold ( totalgold );
      }
   }

   sprintf ( tmpstr ,
      "All characters alive gain \n %d Gold Pieces and %d experience Points",
      totalgold, totalexp );

   //load_backup_screen();
   //maze.draw_maze();
   blit_mazebuffer();
   WinMessage wmsg_reward ( tmpstr );
   Window::show_all();

}*/

void Combat::show_cimetary ( void )
{
   int i = 0;
   short x [ 6 ] = {128, 256, 384, 128, 256, 384 };
   short y [ 6 ] = { 40, 40, 40, 200, 200, 200 };
   Character tmpchar;
   int retval;


   //play_music_track ( System_MUSIC_w3death, false );
   play_music_table ( Config_MUSICTAB_DEATH );

   retval = tmpchar.SQLpreparef ("WHERE location=%d", Character_LOCATION_PARTY );

   if ( retval == SQLITE_OK )
   {
      retval = tmpchar.SQLstep ();

      while ( retval == SQLITE_ROW)
      {
         draw_sprite ( subbuffer, datref_image [ 0 ], x [ i ], y [ i ] );
         textprintf_old ( subbuffer, FNT_print, x [ i ], y [ i ] + 128 , General_COLOR_TEXT,
         "%s", tmpchar . name() );
         i++;
         retval = tmpchar.SQLstep();
      }
   }

   tmpchar.SQLfinalize();


   textout_centre_old ( subbuffer, FNT_elgar32, "Helas, your characters have been defeated!",
      320, 0, General_COLOR_TEXT );

   textout_centre_old ( subbuffer, FNT_print, "Push a button to continue!",
      320, 350, General_COLOR_TEXT );

   textout_centre_old ( subbuffer, FNT_small, "The button I mean is not RESET, it is the Select key.",
      320, 368, General_COLOR_TEXT );

   copy_buffer();

   SQLcommit();

   // note: need to find a way to save the game and return to city. But not sure if it can be done
   // before the player hit the key.
   while ( mainloop_readkeyboard() != SELECT_KEY );
}

/*
void Combat::activesort_characters ( void )
{
   int i;
   int j;
//   int tmptag;
   Character tmpchar;

   for ( i = 0 ; i < p_nb_character - 1 ; i++ )
   {
//      if ( p_character [ i ] != 0 )
//      {
//         tmpchar.SQLselect ( p_character [ i ] );
         if ( p_character [ i ].is_active() == false )
         {
            tmpchar = p_character [ i ];
            p_character [ i ] = p_character [ i + 1 ];
            p_character [ i + 1 ] = tmpchar;
         }
//      }
   }
}

void Combat::activesort_ennemies ( void )
{
   int i;
   int j;
//   int tmptag;
   Ennemy tmpenmy;

   for ( i = 0 ; i < 5 ; i++ )
   {
//      if ( p_ennemy [ i ] != 0 )
//      {
//         tmpenmy.SQLselect ( p_ennemy [ i ] );
         if ( p_ennemy [ i ].is_active() == false )
         {
            tmpenmy = p_ennemy [ i ];
            p_ennemy [ i ] = p_ennemy [ i + 1 ];
            p_ennemy [ i + 1 ] = tmpenmy;
         }
//      }
   }
}*/


/*
void Combat::remove_character ( int index )
{
   int i;

   for ( i = index ; i < p_nb_character ; i++ )
      p_character [ i ] = p_character [ i + 1 ];

   p_nb_character--;
}

void Combat::remove_ennemy ( int index )
{
   int i;

   for ( i = index ; i < p_nb_ennemy ; i++ )
      p_ennemy [ i ] = p_ennemy [ i + 1 ];

   p_nb_ennemy--;

} */

void Combat::start ( void )
{
   int i;
   int j;
   Character tmpchar;
   Monster tmpmons;
   int charlistID [6];
   int nb_character;
   int nb_monsters;
   int error;
   char tmpstr[80];
   int answer;
   bool valid_action = false;
   bool run = false;
   int input_retval = Action_INPUT_RETVAL_NONE;
   //int resolve_retval;
   Action active_action;
   int retval;
   bool endbattle = false;
   bool confirm_action = false;
   char tmpqststr [81];
   // note: add show monster list.

   //WinMessage wmsg_combat ("Combat! but the monster ran away");
   //Window::show_all();

   system_log.write ("----- Starting Battle -----");

   //printf (" Debug: Combat: start: Before load ennemy picture\n");
   load_enemy_picture ();

   blit_mazebuffer();
  // printf (" Debug: Combat: start: Before draw ennemy picture\n");
   draw_enemy();
   Window::draw_party_frame();
   copy_buffer();

   rest (1000);

   play_music_track ( System_MUSIC_w1fight );

   Monster::force_recompile_ranks();
   Character::force_recompile_ranks();

   //while ( mainloop_readkeyboard() != KEY_ENTER );
//printf (" Debug: Combat: start: Before building bars\n");
   WinData<int> wdat_ennemy_bar ( WDatProc_ennemy_bar, 0, WDatProc_POSITION_ENNEMY_BAR, true );
   WinData<int> wdat_combat_log ( WDatProc_combat_log, 0, WDatProc_POSITION_COMBAT_LOG, true );
   WinData<int> wdat_party_bar ( WDatProc_party_bar, party.primary_key(),
         WDatProc_POSITION_PARTY_BAR, true );
   //WinData<int> wdat_char_action ( WDatProc_character_actions, 0, WDatProc_POSITION_CHARACTER_ACTION, true );
      //load_backup_screen ();
   Window::instruction ( 320, 340 );

   wdat_combat_log.hide();
   //WinMessage wmsg_combat ("Combat! but the monster ran away");

   while ( endbattle == false )
   {
     // printf (" Debug: Combat: start: Before identify monsters\n");
      identify_monsters();

      load_enemy_picture ();

      //----- Shoot the dead in the back ----

      //SQLexec ("UPDATE character SET position=999999, rank=0 WHERE body>0");
      //SQLexec ("UPDATE monster SET position=9, rank=0 WHERE body>0");

      //Opponent::compile_ranks();
      Window::refresh_all();

      // --- Query Active Characters ---

      error = tmpchar.SQLprepare ( "WHERE location=2 AND body=0 ORDER BY position" );
      i=0;
      nb_character=0;
//printf ("pass 1-1\r\n");

      if (error == SQLITE_OK )
      {

         error = tmpchar.SQLstep ();

         if (error != SQLITE_ROW)
            tmpchar.SQLerrormsg ();

//printf ("pass 1-2\r\n");
         while (error == SQLITE_ROW)
         {
            charlistID [ i ] = tmpchar.primary_key();

            i++;
            nb_character++;
            error = tmpchar.SQLstep();
//printf ("pass 1-3\r\n");
         }

         error = tmpchar.SQLfinalize ();
         if ( error != SQLITE_OK)
            tmpchar.SQLerrormsg();

      }
      else
         tmpchar.SQLerrormsg();

   // --- Pass through each character commands ---

      confirm_action = false;
      while (confirm_action == false )
      {
         Action tmpaction [ 6 ];

      // Ask for each character action
         i=0;
         while ( i < nb_character && run == false )
         {
//printf ("pass 2-1\r\n");
            tmpchar.SQLselect ( charlistID [i] );
            tmpchar.compile_stat();

            sprintf ( tmpstr, "%s's orders", tmpchar.name() );
            //printf ("%s's order (%d)\n", tmpchar.name(), i );

            List lst_action ( tmpstr, 8 );
            //int tmpcondition = tmpchar.compile_condition();

            for ( j = 0; j < CmdProc_COMMANDLIST_SIZE ; j++ )
            {
//printf ("pass 2-2\r\n");
               if ( tmpchar.check_command ( j , CmdProc_AVAILABLE_COMBAT ) == true )
               {
                  valid_action = false;
                  bool maskedcmd = false;

               // make some check for combat specific conditions not check in character
                  switch ( CmdProc_COMMANDLIST [ j ] . requirement )
                  {
                     case CmdProc_REQUIREMENT_NOTALONE :
                        if (nb_character > 1 )
                           valid_action = true;
                     break;
                     case CmdProc_REQUIREMENT_FIRST :
                        if ( i == 0)
                           valid_action = true;
                     break;
                     case CmdProc_REQUIREMENT_NOTFIRST :
                        if ( i != 0 )
                           valid_action = true;
                     break;
                     default :
                        valid_action = true;
                     break;

                  }

                   if ( CmdProc_COMMANDLIST [ j ] . requirement == CmdProc_REQUIREMENT_SPELL
                   && ( tmpchar.flatstat ( FLAT_CONDITION ) & ActiveEffect_CONDITION_DISSPELL ) > 0)
                     maskedcmd = true;

                  if ( CmdProc_COMMANDLIST [ j ] . requirement == CmdProc_REQUIREMENT_SKILL
                   && ( tmpchar.flatstat ( FLAT_CONDITION ) & ActiveEffect_CONDITION_DISSKILL ) > 0)
                     maskedcmd = true;


                  if ( valid_action == true )
                     lst_action.add_item ( j , CmdProc_COMMANDLIST [ j ] . name, maskedcmd);
//printf ("pass 2-3\r\n");
               }
            }

            answer = 0;


            char conditionstr [101];

            sprintf ( conditionstr, "WHERE target_type=%d AND target_id=%d", Opponent_TYPE_CHARACTER, tmpchar.primary_key() );

            int nb_effect =  SQLcount ("name","effect", conditionstr);

            //printf ("Debug: Combat: start: nb active effect is %d", nb_effect );


            WinData<int> wdat_aeffect ( WDatProc_combat_active_effect, tmpchar.primary_key(), WDatProc_POSITION_COMBAT_ACTIVE_EFFECT, true );

            if ( nb_effect > 0 )
               wdat_aeffect.unhide();
            else
               wdat_aeffect.hide();

            WinList wlst_action (lst_action, 0, 156, false, false, true );
         //WinList wlst_action ( lst_action, 444, 156 );

         //while ( answer2 != -1 && input_retval != Action_INPUT_RETVAL_RETURN )
         //{
            wlst_action.unhide();

            if ( ( tmpchar.flatstat ( FLAT_CONDITION ) & ActiveEffect_CONDITION_DISACTION ) > 0 )
            {
               // skip action if disable action is enabled
               if ( input_retval == Action_INPUT_RETVAL_PREVIOUS )
               {
                  answer = -1;
               }
               else
                  answer = 51;
            }
            else
            {
               blit_mazebuffer();
               draw_enemy();
               //printf ("Debug: Combat: start: pass 3-1\r\n");
               answer = Window::show_all();
               wlst_action.hide();
            }

            input_retval = Action_INPUT_RETVAL_NONE;
            //resolve_retval = Action_RESOLVE_RETVAL_NONE;

            if ( answer != -1 )
            {

               tmpaction [ i ].load_character ( tmpchar );
         //printf ("Combat: before  targetID=%d\n", tmpaction.targetID());
               input_retval = tmpaction [ i ].input ( answer );
               //printf ("cancelled his input(%d)\n", input_retval );
         //printf ("Combat: after  targetID=%d\n", tmpaction.targetID());
            }
            else
               input_retval = Action_INPUT_RETVAL_PREVIOUS;

//printf ("pass 3-2\r\n");
            run = false;
            switch  ( input_retval )
            {
               case Action_INPUT_RETVAL_PREVIOUS:
                  if ( i > 0 )
                     i--;

            // destroy previous characters action;
                  //sprintf ( tmpstr, "DELETE FROM action WHERE actor_type=1 AND actorid=%d;", charlistID [ i ]  );
                  //SQLexec ( tmpstr );
               break;
               case Action_INPUT_RETVAL_RUN:
                  run = true;
               break;
               case Action_INPUT_RETVAL_CANCEL:
                  //printf ("%s cancelled his command (%d)\n", tmpchar.name(),i );
               // do nothing and ask for another command
               break;
               default:
                  //tmpaction.SQLinsert();
                  i++;
               //printf ("Combat: after insert  targetID=%d\n", tmpaction.targetID());
               break;
            }
         }

         if ( run == false)
            sprintf ( tmpqststr, "Do you want to execute these commands?");
         else
            sprintf ( tmpqststr, "Are you sure you want to run?");

         WinQuestion wmsg_confirm ( tmpqststr );

         blit_mazebuffer();
         draw_enemy();

         answer = Window::show_all();

         if ( answer == WinQuestion_ANSWER_YES )
         {
            wmsg_confirm.hide();
            wdat_party_bar.hide();
            wdat_ennemy_bar.hide();
            //wdat_char_action.hide();
            blit_mazebuffer();
            draw_enemy();
            Window::instruction ( 320, 464, 0 );
            Window::draw_all();
            copy_buffer();


            confirm_action = true;
         }
         else
         {
            confirm_action = false;
            run = false;
            i=0;
         }

         if ( confirm_action == true  && run == false )
         {

            int SQLbegin_transaction ( void );

            for ( i = 0; i < nb_character ; i++)
            {
               tmpaction [ i ].SQLinsert();
            }
            int SQLcommit_transaction ( void );

         }

      }

      // --- Add AI actions --- (Pass for each monster and add command)

      retval = tmpmons.SQLpreparef ("WHERE body=%d", Character_BODY_ALIVE );

      if ( retval == SQLITE_OK)
      {
         retval = tmpmons.SQLstep();

         int SQLbegin_transaction ( void );

         while ( retval == SQLITE_ROW )
         {
            //to do: roll for special attack, and if no attack available, it becomes a regular attack
            tmpmons.compile_stat();

            Action tmpaction ( tmpmons );

            int rndselect = dice ( 20 );
            int attackid = 0;

            //---------- Action selection script START ----------

            if ( tmpmons.rank() == Party_FRONT )
            {
               if ( rndselect >= 1 && rndselect <= 8 )
                  attackid = tmpmons.front_attack ( 0 );
               if ( rndselect >= 9 && rndselect <= 14 )
                  attackid = tmpmons.front_attack ( 1 );
               if ( rndselect >= 15 && rndselect <= 18 )
                  attackid = tmpmons.front_attack ( 2 );
               if ( rndselect >= 19 && rndselect <= 20 )
                  attackid = tmpmons.front_attack ( 3 );
            }
            else
            if ( tmpmons.rank() == Party_BACK )
            {
               if ( rndselect >= 1 && rndselect <= 8 )
                  attackid = tmpmons.back_attack ( 0 );
               if ( rndselect >= 9 && rndselect <= 14 )
                  attackid = tmpmons.back_attack ( 1 );
               if ( rndselect >= 15 && rndselect <= 18 )
                  attackid = tmpmons.back_attack ( 2 );
               if ( rndselect >= 19 && rndselect <= 20 )
                  attackid = tmpmons.back_attack ( 3 );
            }


            //---------- Action selection script END ----------

            //printf ("Debug: Combat: Start: Attackid = %d \n", attackid );

            if ( attackid == 0)
               answer = tmpaction.input ( 1 ); // fight action
            else
            {
               tmpaction.value ( attackid );

               answer = tmpaction.input ( 52 ); // special attack and spells for monsters action

               if ( answer == Action_INPUT_RETVAL_CANCEL ) // for example when out of mana or fail to select target
               {
                  answer = tmpaction.input ( 1 );
                  //printf ("Debug: Combat: Start: Attack is cancelled\n");
               }
            }

            if ( answer != Action_INPUT_RETVAL_CANCEL)
               tmpaction.SQLinsert();

            retval = tmpmons.SQLstep();
         }

         int SQLcommit_transaction ( void );
      }
      else
          printf ("Error: Combat: Monsters could not be selected for AI actions\n");

      tmpmons.SQLfinalize();


      // --- Combat Resolution --- (Query all actions and execute them)

      //wlst_action.hide(); out of scope already
         wdat_combat_log.unhide();
         wdat_party_bar.hide();
         wdat_ennemy_bar.hide();

         int logspeed = config.get ( Config_COMBAT_LOG_SPEED);

         if (logspeed == 0)
            Window::instruction ( 320, 464, Window_INSTRUCTION_SELECT );
         else
            Window::instruction ( 320, 464, 0 );

         retval = active_action.SQLprepare ("ORDER BY initiative DESC");

         if ( retval == SQLITE_OK )
         {
            retval = active_action.SQLstep ();

            while ( retval == SQLITE_ROW )
            {
               system_log.mark();
               //printf ("Actor_type:%d, initiative=%d\n", active_action.actor_type(), active_action.initiative() );
               answer = active_action.resolve();

               if ( answer != Action_RESOLVE_RETVAL_CANCEL )
               {
                  Window::refresh_all();
                  blit_mazebuffer();
                  draw_enemy();
                  Window::draw_all();
                  copy_buffer();

            //rest (1000);
                  if ( logspeed == Config_CLS_PAUSE )
                     while (mainloop_readkeyboard() != SELECT_KEY );
                  else
                     rest ( Config_COMBAT_LOG_SPEED_TIME [ logspeed ]);
               }
            //printf ("Before step: %u\n", clock());
               retval = active_action.SQLstep();
            //printf ("After step: %u\n", clock());
            }


         }
         else
            active_action.SQLerrormsg();

         active_action.SQLfinalize();


         system_log.write ("----- End of Combat Round -----");

      // clean all actions for a new round.
         SQLexec ("DELETE FROM action;");
         ActiveEffect::trigger_expiration ( ActiveEffect_EXPIRATION_ENDOFTURN );
         game.clock.add_minute (1);

         wdat_combat_log.hide();
         wdat_party_bar.unhide();
         wdat_ennemy_bar.unhide();
         //wdat_char_action.unhide();
         Window::instruction ( 320, 340 );
         Window::refresh_all();

      // --- Combat Results

      // count nb of characters   and monsters

         nb_character = SQLcount ( "name", "character", "WHERE location=2 AND body=0");
         nb_monsters = SQLcount ( "name", "monster", "WHERE body=0");

         if ( nb_character == 0 ) // Check if Party is destroyed
         {
            show_cimetary();

         //remove characters from the party
            SQLexec ("UPDATE character SET location=1 WHERE location=2;");

            endbattle=true;
            system_log.write ("----- Party Anihilated -----");
            party.status (Party_STATUS_CITY);
         }
         else
            if ( nb_monsters == 0 )
            {
               wdat_combat_log.unhide();
               wdat_party_bar.hide();
               wdat_ennemy_bar.hide();
               Window::instruction ( 320, 464, Window_INSTRUCTION_SELECT );
               give_rewards ();
               wdat_combat_log.hide();

            //printf ("Nb_MOnsters = %d", nb_monsters );

            // send party back to maze since battle is won
               endbattle=true;
               system_log.write ("----- Monsters Defeated -----");
               party.status (Party_STATUS_MAZE);
            }
            else
               if ( run == true )
               {
                  endbattle = true;
                  system_log.write ("----- Party Ran Away -----");
                  party.status (Party_STATUS_MAZE);
                  party.walk_backward();
               }
               // party flee to maze.



   }

   SQLexec ("DELETE FROM monster;");
   SQLexec ("DELETE FROM action;");

   ActiveEffect::trigger_expiration ( ActiveEffect_EXPIRATION_ENDOFCOMBAT );

   //SQLexec ("UPDATE character SET position=999999, rank=0 WHERE body>0");
   //SQLexec ("UPDATE monster SET position=9, rank=0 WHERE body>0");


}

/*-------------------------------------------------------------------------*/
/*-                        Private Methods                                -*/
/*-------------------------------------------------------------------------*/

/*void Combat::draw_combat_screen ( void )
{
   Party &player_party = player.selected_party ();
   short tmpval;
   int height = text_height ( FNT_print );
   int i;

   // ennemy bar ( maybe place in another function if used by encounter
   tmpval = 12 + ( 6 * height );

   WinEmpty wemp_monster_list ( 0, 0, 640, tmpval ); //?? replace with windata
//   draw_border_fill ( 0, 0, 639, tmpval, General_COLOR_BORDER,
//                                                        General_COLOR_FILL );
   tmpval = 6;
   for ( i = 0 ; i < p_nb_ennemy ; i++ )
   {
      textprintf_old ( subbuffer, FNT_print, 30, tmpval, General_COLOR_TEXT,
         "%s", p_ennemy [ i ]->cname_combat() );
      tmpval = tmpval + height;
   }

   // draw party bar
   WinData<Party> wdat_party ( WDatProc_party_bar, player_party,
      WDatProc_POSITION_PARTY_BAR );
//   draw_party_bar( player_party );

   // draw monster display

   WinEmpty wemp_monster_picture ( 0, 112, 396, 244 );
//   draw_border_fill ( 0, 112, 396, 355, General_COLOR_BORDER,
//      General_COLOR_FILL );*/

/*   stretch_sprite ( subbuffer, BMP_monster013, 102, 141, 64, 64 );
   stretch_sprite ( subbuffer, BMP_monster014, 166, 141, 64, 64 );
   stretch_sprite ( subbuffer, BMP_monster015, 230, 141, 64, 64 );

   stretch_sprite ( subbuffer, BMP_monster010, 54, 173, 96, 96 );
   stretch_sprite ( subbuffer, BMP_monster011, 150, 173, 96, 96 );
   stretch_sprite ( subbuffer, BMP_monster012, 246, 173, 96, 96 );*/

/*   draw_sprite ( subbuffer, BMP_ennemy058, 6, 221 );
   if ( p_nb_ennemy >= 2 )
      draw_sprite ( subbuffer, BMP_ennemy059, 134, 221 );
   if ( p_nb_ennemy >= 3 )
      draw_sprite ( subbuffer, BMP_ennemy041, 262, 221 );

   // draw character menu display

//   draw_border_fill ( 400, 112, 639, 355, General_COLOR_BORDER,
//                                                        General_COLOR_FILL );

} */

void Combat::load_enemy_picture ( void )
{
   int i = 0;
   bool picfound = false;
   Monster tmpmonster;
   MonsterCategory tmpcategory;
   int tmppicid [ 10 ];
   int error;

   for ( i = 0; i < 10; i++ )
      tmppicid [ i ] = -1;

   i=0;


   error = tmpmonster.SQLprepare ("WHERE body=0 ORDER BY position");
   //tmpmonster.SQLerrormsg();

   if ( error == SQLITE_OK )
   {
      error = tmpmonster.SQLstep();
      //tmpmonster.SQLerrormsg();

      while ( error == SQLITE_ROW )
      {
         if ( tmpmonster.identified() == true )
            tmppicid [ i ] = tmpmonster.pictureID();
         else
         {
            tmpcategory.SQLselect ( tmpmonster.FKcategory() );
            tmppicid [ i ] = tmpcategory.pictureID();
         }

         //printf ( "Debug: i=%d, %s, pic=%d\n", i, tmpmonster.name(), tmpmonster.pictureID());

         error = tmpmonster.SQLstep();
         i++;
         //tmpmonster.SQLerrormsg();
      }
   }

   tmpmonster.SQLfinalize();

   /*printf ("Monster picID: ");
   for ( i = 0; i < 10; i++ )
      printf ( "%d,", tmppicid [ i ] );
   printf ("\n");*/


   // select different monsters

   i = 1;
   picfound = false;

   p_pictureID [ 0 ] = -1;
   p_pictureID [ 1 ] = -1;

   p_pictureID [ 0 ] = tmppicid [ 0 ];

   while ( i < 10  && picfound == false )
   {
      if ( tmppicid [ i ] != p_pictureID [ 0 ] && tmppicid [ i ] != -1 )
      {
         p_pictureID [ 1 ] = tmppicid [ i ];
         picfound = true;
         //p_nb_picture++;
         //j++;
      }
      i++;
   }

   // count number of pictures

   p_nb_picture = 0;
   for ( i = 0; i < 2 ; i++)
      if ( p_pictureID [ i ] != -1 )
         p_nb_picture++;
}

void Combat::draw_enemy ( void )
{
   // this needs to be resolved quickly since used in loop.

   //int i = 0;
   //int j = 0;
   //int pictureid [ 2 ]; // 2 pictures retained to be drawn

   switch ( p_nb_picture  )
   {
      case 2:
         if ( config.get ( Config_ANTI_ALLIASING ) == Config_YES)
         {
            aa_stretch_sprite ( subbuffer, datref_monster [ p_pictureID[0] ], 0,  80, 400, 400 );
            aa_stretch_sprite ( subbuffer, datref_monster [ p_pictureID[1] ], 240, 80, 400, 400 );
         }
         else
         {
            stretch_sprite ( subbuffer, datref_monster [ p_pictureID[0] ], 0,  80, 400, 400 );
            stretch_sprite ( subbuffer, datref_monster [ p_pictureID[1] ], 240, 80, 400, 400 );
         }
      break;

      case 1:
         if ( config.get ( Config_ANTI_ALLIASING ) == Config_YES)
         {
            aa_stretch_sprite ( subbuffer, datref_monster [ p_pictureID[0] ], 120, 80, 400, 400 );
         }
         else
         {
            stretch_sprite ( subbuffer, datref_monster [ p_pictureID[0] ], 120, 80, 400, 400 );
         }
      break;

      case 0:
      break;
   }


   // todo: select monster from databases

   // Draw selected monsters.

   //stretch_sprite ( subbuffer, datref_monster [ 0 ], 40, 160, 320, 320 );
   //stretch_sprite ( subbuffer, datref_monster [ 0 ], 280, 160, 320, 320 );

   //stretch_sprite ( subbuffer, datref_monster [ 28 ], 40, 120, 360, 360 );
   //stretch_sprite ( subbuffer, datref_monster [ 53 ], 280, 120, 360, 360 );

   // note: is proportions are too problematic, only use 1 monster
   //stretch_sprite ( subbuffer, datref_monster [ 0 ], 0,  80, 400, 400 );
   //stretch_sprite ( subbuffer, datref_monster [ 0 ], 240, 80, 400, 400 );




   //stretch_sprite ( subbuffer, datref_monster [ 53 ], 80,  0, 480, 480 );

   //to do: draw the 2 first monsters or different monsters

   /*{ {   19,    50,   380,   380 },
     {   130,   50,   380,   380 },
     {   241,   50,   380,   380 },
     {   -31,   20,   440,   440 },
     {   100,   20,   440,   440 },
     {   231,   20,   440,   440 },
     {   0,   0,   0,   0 },
     {   0,    0,   0,   0 },
     {   0,   0,   0,   0 }
   },*/

   /*stretch_sprite ( subbuffer,
               (BITMAP*) datfennemy [ BMP_ENNEMY000 + 0 ]. dat,
               130, 50, 380, 380 );*/
   //draw_sprite ( subbuffer, datref_monster [ 1 ], 240, 160 );

   //short xposfront [ 3 ] = { 32, 224, 416 };
   //short xposback [ 3 ] = { 118, 256, 384 };
//   short picture [ 6 ];

   //int nb_picture = 0;
//   Ennemy tmpenn;

/*   for ( i = 0 ; i < p_nb_ennemy ; i++ )
   {
//      tmpenn.SQLselect ( party.character ( i ) );
      if ( p_ennemy [ i ] . status () == Opponent_STATUS_ALIVE )
      {
         picture [ i ] = p_ennemy [ i ].pictureID();
//         nb_picture++;
      }
   }*/
/*
   i = p_nb_ennemy - 1;
   while ( i >= 0 )
   {
      if ( p_ennemy [ i ] . status () == Opponent_STATUS_ALIVE )
      {
         pictureid = p_ennemy [ i ].pictureID();
         if ( i < 3 )
         {
            stretch_sprite ( subbuffer,
               (BITMAP*) datfennemy [ BMP_ENNEMY000 + pictureid ]. dat,
               xposfront [ i  ], 160, 192, 192 );
         }
         if ( i >= 3 )
         {

         }
      }
      i--;
   }*/
}

void Combat::give_rewards ( void )
{
   int expreward = 0;
   int exptotalreward = 0;
   int goldreward = 0;
   int goldtotalreward = 0;
   int golddie = 0;
   Monster tmpmonster;
   Character tmpcharacter;
   int i;
   int retval;
/*   int hitdice [ 6 ] = { 4, 6, 8, 10, 12, 20};
   int size = 0;

   for ( i = 0 ; i < 6 ; i++)
      if ( hitdice [ i ] == tmpmonster.hpdice() )
         size=i;
*/

   //---------------------- Compile rewards --------------------------

   retval = tmpmonster.SQLprepare ("WHERE NOT body >= 10");

   if ( retval == SQLITE_OK)
   {
      retval = tmpmonster.SQLstep ();

      while ( retval == SQLITE_ROW )
      {

         //------------ experience points ------------------

         expreward = Combat_MONSTER_EXP_VALUE [ tmpmonster.size() ];

         // --- Extra Rewards ---

         if ( ( tmpmonster.extra_reward() & Monster_REWARD_EXP_X2 ) > 0 )
            expreward *= 2;

         if ( ( tmpmonster.extra_reward() & Monster_REWARD_EXP_X4 ) > 0 )
            expreward *= 4;


         // --- Monster Bonus EXP ---
         // to do

         // --- Difficulty adjustments ---

         //Config_EXP_REWARD
         switch ( config.get ( Config_EXP_REWARD ) )
         {
            case Config_DIF_LOW:
               expreward *= 2;
            break;
            case Config_DIF_HIGH:
               expreward /= 2;
            break;
         }

         expreward *= tmpmonster.level();

         exptotalreward += expreward;

         //------------ Gold points ------------------------

         golddie = 0;
         if ( ( tmpmonster.extra_reward() & Monster_REWARD_GOLD_D20 ) == Monster_REWARD_GOLD_D20 )
            golddie = 20;
         else
         {
            if ( ( tmpmonster.extra_reward() & Monster_REWARD_GOLD_D6 ) > 0 )
               golddie = 6;

            if ( ( tmpmonster.extra_reward() & Monster_REWARD_GOLD_D12 ) > 0 )
               golddie = 12;
         }

         goldreward = 0;
         if ( golddie > 0 )
         {
            for  (i = 0 ; i < tmpmonster.level() ; i++)
            {
               goldreward += dice ( golddie );
            }
         }

         switch ( config.get ( Config_GOLD_REWARD ) )
         {
            case Config_DIF_LOW:
               goldreward *= 2;
            break;
            case Config_DIF_HIGH:
               goldreward /= 2;
            break;
         }


         //printf ("debug: combat: reward: gold=%d, golddie=%d\n", goldreward, golddie );
         goldtotalreward  += goldreward;

         // ------------------- Gems Reward -----------------------------

         //to do

         // ------------------- Expandable reward -----------------------

         //to do,
         // maybe item rewards only in chest.

         retval = tmpmonster.SQLstep();
      }
   }

   tmpmonster.SQLfinalize ();


   // ------------------ give rewards to characters ------------------------

   retval = tmpcharacter.SQLpreparef ("WHERE location=%d AND body=%d", Character_LOCATION_PARTY,
                                     Character_BODY_ALIVE );

   if ( retval == SQLITE_OK )
   {
      SQLbegin_transaction();
      retval = tmpcharacter.SQLstep();

      while ( retval == SQLITE_ROW )
      {
         tmpcharacter.gain_exp (exptotalreward);
         tmpcharacter.SQLupdate();
         retval = tmpcharacter.SQLstep();
      }

      SQLcommit_transaction();

   }

   tmpcharacter.SQLfinalize();

   party.add_gold (goldtotalreward);


   // ---------------------- Display reward -----------------------

   play_music_track ( System_MUSIC_w1victor, false );

   system_log.mark();
   system_log.writef ("All characters gains %d Experice Points", exptotalreward );

   if ( goldtotalreward > 0 )
      system_log.writef ("The party gains %d Gold Pieces", goldtotalreward );

   Window::refresh_all();
   blit_mazebuffer();

   //draw treasure on floor
   // maybe draw when items found but no gold. Maybe reserve for chest.
   if ( goldtotalreward > 0 )
   {
      if ( config.get ( Config_ANTI_ALLIASING ) == Config_YES)
      {
         aa_stretch_sprite ( subbuffer, datref_object [ Combat_OBJECTID_GOLD ], 208, 255, 225, 225 );
      }
      else
      {
         stretch_sprite ( subbuffer, datref_object [ Combat_OBJECTID_GOLD ], 208, 255, 225, 225 );
      }
   }


   Window::draw_all();
   copy_buffer();

   while (mainloop_readkeyboard() != SELECT_KEY );
}

void Combat::identify_monsters ( void )
{
   Monster tmpmonster;
   int error;

   error = tmpmonster.SQLprepare ("WHERE body=0 ORDER BY position");
   //tmpmonster.SQLerrormsg();

   if ( error == SQLITE_OK )
   {
      error = tmpmonster.SQLstep();
      //tmpmonster.SQLerrormsg();

      while ( error == SQLITE_ROW )
      {
         if ( tmpmonster.identified() == false )
         {
            if ( dice(100) < Combat_IDENTIFY_PROBABILITY )
            {
               tmpmonster.identified ( true );
               //printf ("Combat: Itentify Monster: Before update\n");

               tmpmonster.SQLupdate();
               //void SQLdeactivate_errormsg (void);

            }
         }


         error = tmpmonster.SQLstep();

      }
   }

   tmpmonster.SQLfinalize();

}

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

Combat combat;

const int Combat_MONSTER_EXP_VALUE [ 7 ] =
{
  0, 20, 30, 40, 50, 60, 100
};

/*const float Combat_MONSTER_EXP_BONUS_VALUE [ 7 ] =
{
   0, 1, 1.5, 2, 3, 4, 5
};*/

/*const char STR_CMB_STATUS [] [ 16 ] =
{
   {"is still alive"},
   {"is killed"},
   {"turn to Ashes"},
   {"is DELETED!"},
};*/

/*s_Combat_command COMBAT_COMMAND [ Combat_COMMAND_SIZE ] =
{
//   { "Zap        ", Combat_CMDREQ_TARGET, -1 },
   { "Fight      ", Combat_CMDREQ_TARGET, -1 },
   { "Parry      ", 0, -1 },
   { "Hide       ", 0, 1 },
   { "Use Item   ", 0, -1 },
   { "Cast Spell ", 0, 1 },
   { "Dispel     ", 0, 1 },
   { "Run        ", Combat_CMDREQ_FIRST, -1 },
   { "Back       ", Combat_CMDREQ_NOT_FIRST, -1 },
   { "           ", Combat_CMDREQ_IGNORE, -1 },
   { "           ", Combat_CMDREQ_IGNORE, -1 },
   { "           ", Combat_CMDREQ_IGNORE, -1 },
   { "           ", Combat_CMDREQ_IGNORE, -1 },
};
*/



