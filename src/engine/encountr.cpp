/***************************************************************************/
/*                                                                         */
/*                        E N C O U N T R . C P P                          */
/*                          Class Source Code                              */
/*                                                                         */
/*     Content : Class Encounter                                           */
/*     Programmer : Eric Pietroucpo                                        */
/*     Starting Date : November 1st, 2003                                  */
/*     License : GNU General Public LIcense                                */
/*                                                                         */
/***************************************************************************/


// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

#include <wdatproc.h>

/*
#include <allegro.h>


//#include <time.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>


//
//
//
//
//
//#include <list.h>

#include <cclass.h>
#include <opponent.h>
#include <charactr.h>
#include <ennemy.h>

#include <party.h>

#include <game.h>
//
//#include <city.h>
//#include <maze.h>
#include <encountr.h>
//
//#include <camp.h>
#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winmenu.h>
#include <winempty.h>
#include <wintitle.h>
#include <windata.h>
//#include <wdatproc.h>
#include <winmessa.h>
#include <winquest.h>

*/


/*-------------------------------------------------------------------------*/
/*-                        Constructor and Destructor                     -*/
/*-------------------------------------------------------------------------*/

Encounter::Encounter ( void )
{

   clear_table();
   //clear_selection();
}

Encounter::~Encounter ( void )
{

}


/*-------------------------------------------------------------------------*/
/*-                            Property Methods                           -*/
/*-------------------------------------------------------------------------*/

int Encounter::nb_enemy ( void )
{
   return ( p_nb_enemy );
}

/*void Encounter::probability ( int value )
{
   p_probability = value;
}

int Encounter::probability ( void )
{
   return ( p_probability );
}*/

int Encounter::counter ( void )
{
   return ( p_counter );
}

void Encounter::counter ( int value )
{
    p_counter = value;
}


/*-------------------------------------------------------------------------*/
/*-                               Methods                                 -*/
/*-------------------------------------------------------------------------*/

//void Encounter::build_list ( int base, int range, unsigned short mask )
//{
   /*Ennemy tmpennemy;
   unsigned int index = db.search_table_entry ( DBTABLE_MONSTER );
   short i = 0;
   bool accept_ennemy;
   int min_level = base;
   int max_level = base + range - 1;

   clear_selection();

   p_nb_ennemy = 0;
   short y = 0;
   char tmpstr [ 16 ];

//   textprintf_old ( buffer, font, 0, 0, General_COLOR_TEXT,
//      "Sizeof : %d", sizeof ( dbs_Ennemy_monster ) );
//   copy_buffer ();
//   while ( ( readkey() >> 8 ) != KEY_ENTER );

   do
   {
   */
/*      db.select ( tmpstr, index, 16 );
   textprintf_old ( subbuffer, font, 0, 0, General_COLOR_TEXT,
      "index : %d : string : %s", index, tmpstr );

   copy_buffer ();
   while ( ( readkey() >> 8 ) != KEY_ENTER );*/
/*
      tmpennemy.alDBselect ( Ennemy_DATATYPE_MONSTER,
         sizeof ( dbs_Ennemy_monster ), index );
      accept_ennemy = false;


*/
/*      textprintf_old ( buffer, font, 0, y, General_COLOR_TEXT,
         "%s | %s | lv %d", tmpennemy.cname(), tmpennemy.cgroup_name(),
         tmpennemy.level() );
      y = y + 16;
      copy_buffer();
      while ( ( readkey() >> 8 ) != KEY_ENTER );*/
/*
      if ( ( tmpennemy.level() >= min_level )
         && ( tmpennemy.level() <= max_level ) )
         accept_ennemy = true;

      if ( accept_ennemy == true )
      {
      */
/*         WinData<Opponent> wdat_opponnent_stat ( WDatProc_opponent_stat,
            tmpennemy, WDatProc_POSITION_OPPONENT_STAT );
         Window::show_all();
         copy_buffer();
         while ( ( readkey() >> 8 ) != KEY_ENTER );*/

/*         p_elist [ i ] = tmpennemy.tag();
         i++;
         p_nb_ennemy++;
      }
      index++;
   }
   while ( db.entry_table_tag ( index ) == DBTABLE_MONSTER );
*/



//}

void Encounter::build_table ( int floorID, int areaID )
{

   EnemyGroup tmpgroup;
   int errorsql;
   int i;
   int r;

   clear_table();

   errorsql = tmpgroup.SQLpreparef (
      "WHERE (floor=%d OR floor=99) AND (area=%d OR area=99)",
       floorID, areaID );
   //printf ("Debug:Encounter:Build Table: Floor=%d. area=%d\n", floorID, areaID);

   if ( errorsql == SQLITE_OK)
   {
      errorsql = tmpgroup.SQLstep();

      while ( errorsql == SQLITE_ROW && p_nb_enemy < Encounter_ELIST_SIZE)
      {
         for ( r = 0; r < tmpgroup.repeat()
            && p_nb_enemy < Encounter_ELIST_SIZE; r++)
         {


            for ( i = 0 ; i < EnemyGroup_NB_ENEMY; i++)
            {
               p_etable [ p_nb_enemy] [ i ] . id = tmpgroup.enemy (i);
               p_etable [ p_nb_enemy] [ i ] . prob =
                  EnemyGroup_PROBABILITY [ tmpgroup.probability() ] [ i ];
//printf  ("Debug:Encounter:Build Table: Found Floor=%d, Area=%d\n", tmpgroup.floor(), tmpgroup.area());

               if ( ( tmpgroup.bonus_target() & ( 128 >> i ) ) > 0)
                  p_etable [ p_nb_enemy] [ i ] . bonus = tmpgroup.bonus();
            }
            p_nb_enemy++;
         }

         errorsql = tmpgroup.SQLstep();

      }

      tmpgroup.SQLfinalize();
   }


}

void Encounter::clear_table ( void )
{
   int i;
   int j;
   s_Encounter_enemy emptyenemy;

   emptyenemy.id = 0;
   emptyenemy.prob = 0;
   emptyenemy.bonus = 0;

   for ( i = 0 ; i < Encounter_ELIST_SIZE ; i++ )
      for ( j = 0; j < EnemyGroup_NB_ENEMY; j++)
      {
         p_etable [ i ] [ j ] = emptyenemy;
      }

   p_nb_enemy = 0;

   //p_counter = Encounter_BASE_COUNTER;
   //p_party = 0;
   //p_door = false;


}

/*void Encounter::clear_list ( void )
{
   short i;

   for ( i = 0 ; i < Encounter_ELIST_SIZE ; i++ )
      p_elist [ i ] = 0;

   p_nb_ennemy = 0;

   // Set Probabilities
   switch ( config.get (Config_MONSTER_OCCURENCE) )
   {
      case Config_DIF_LOW: p_probability = 1;
      break;
      case Config_DIF_NORMAL: p_probability = 2;
      break;
      case Config_DIF_HIGH: p_probability = 4;
      break;
   }


}*/

bool Encounter::check_encounter ( s_mazetile tile, bool door )
{
   int side = 0;
   //int dir = 3;
  // bool enc_side [ 4 ] = { false, false, false, false };
   int rndval = 0;
   bool encounter_found = false;
   //char tmpstr [100];
   int sidebonus = 0;
   int probability;
//   short tmprob;
//   string cmpstr;

//?? remove facing but keep increase of probabilities for multiple oppening

//   if ( door == true )
//      tmprob = p_probability * 2;
//   else
//      tmprob = p_probability;

   //printf ("Debug: Encounter: CheckEncounter: Counter=%d, Probability=%d\n", p_counter, probability);

   switch ( config.get (Config_MONSTER_OCCURENCE) )
   {
      case Config_DIF_LOW: probability = 1;
      break;
      default:
      case Config_DIF_NORMAL: probability = 2;
      break;
      case Config_DIF_HIGH: probability = 4;
      break;
   }


   //p_door = door;
   p_counter -= probability;

   //if ( p_nb_ennemy > 0 )
   //{

   sidebonus = 0;
   for ( side = 0 ; side < 4 ; side++ )
   {  if ( get_mazetile_wall( tile, side ) > 0 )
      {  sidebonus++;
      }
   }


   switch ( config.get (Config_MONSTER_OCCURENCE) )
   {
      case Config_DIF_LOW: sidebonus = sidebonus / 4;
      break;
      case Config_DIF_NORMAL: sidebonus = sidebonus / 2;
      break;
   }

   p_counter -= sidebonus;
   //printf ("Encounter check: Counter=%d, Probability=%d, sidebonus=%d\r\n", p_counter, p_probability, sidebonus );

   // Now used fixed counting for each step


//            rndval = rnd( p_decrement );
//            sprintf ( tmpstr, " | dir %d open (%d/%d)", dir, rndval, tmprob );

//            if ( rndval <= tmprob )
//            {
//               enc_side [ side ] = true;
//               encounter_found = true;
//            }

//         else
//            sprintf ( tmpstr, "| dir %d close", dir, rndval, tmprob );


//         if ( dir > 8 )
//            dir = 1;

//      cmpstr += tmpstr;
//      if ( side == 1 )
//         cmpstr += "\n";
//      }
//   WinMessage wmsg_random ( cmpstr );

//   Window::show_all();

     if ( door == true )
     {
        rndval = rnd( 100 );
        if ( rndval >= p_counter )
           encounter_found = true;
     }
     else
     {
        if ( p_counter <= 0 )
           encounter_found = true;
     }

      if ( encounter_found == true )
      {
         p_counter = Encounter_BASE_COUNTER;

         //select_encounter ();

      // setting value like this determine priority ( from front to back )
/*         if ( enc_side [ Encounter_SIDE_BACK ] == true )
            p_side = Encounter_SIDE_BACK;

         if ( enc_side [ Encounter_SIDE_LEFT ] == true )
            p_side = Encounter_SIDE_LEFT;

         if ( enc_side [ Encounter_SIDE_RIGHT ] == true )
            p_side = Encounter_SIDE_RIGHT;

         if ( enc_side [ Encounter_SIDE_LEFT ] == true
            && enc_side [ Encounter_SIDE_RIGHT ] == true )
         {
            // allow even probabilities for left and right
            rndval = rnd(2);
            if ( rndval == 1 )
               p_side = Encounter_SIDE_LEFT;
            else
               p_side = Encounter_SIDE_RIGHT;
         }

         if ( enc_side [ Encounter_SIDE_FRONT ] == true )
            p_side = Encounter_SIDE_FRONT;*/

      }
   //}

//   p_decrement -= p_probability;

//   if ( p_decrement < 4 )
//      p_decrement = 4;

   return ( encounter_found );
}

int Encounter::start ( void )
{
   int errorsql;
   Monster tmpmonster;
   //int i= 0;
   int max = Config_NB_MAX_MONSTER_VALUE [ config.get ( Config_NB_MAX_MONSTER ) ];
   int nb_monster= 0;
   //int monsterindex = 0;
   char tmpmonstername [80] = "";
   //char originalname [ 80 ] = "";
   int nb_activemonster = 0;

   int selected_group = 0;
   //printf ("SQLerrormsg: activated in encounter::start\n");
   //void SQLactivate_errormsg (void);
   if ( p_nb_enemy > 0 )  // else skip encounter since there are no monsters in the table.
   {
      WinTitle wttl_encounter ("Encounter !", 320, 200, true);
      maze.draw_maze();
      Window::show_all();
      wttl_encounter.hide();

  // play_music_track ( System_MUSIC_w3fight, true );
      stop_midi();
      play_sound ( 1026 ); // encounter

      system_log.write ("Encounter!");

// in case it was not previous removed
      SQLexec ( "DELETE FROM monster;");
      SQLexec ("DELETE FROM action;");

   //old selection method
   //retval = tmpmonster.template_SQLprepare ("WHERE level <= 2 ORDER by random()");

      nb_monster = 0;
      nb_activemonster = 0;
      selected_group = dice ( p_nb_enemy ) - 1;

   //SQLactivate_errormsg();
      errorsql = tmpmonster.template_SQLselect ( p_etable [ selected_group ] [ nb_monster ] . id);


   //SQLbegin_transaction();

/*printf ("\nMonster %d id=%d prob=%d bonus=%d\n", nb_monster
        , p_etable [ selected_group ] [ nb_monster ] . id
        , p_etable [ selected_group ] [ nb_monster ] . prob
        , p_etable [ selected_group ] [ nb_monster ] . bonus );*/
//printf ("name=%s", tmpmonster.name());

      while ( nb_monster < max)
      {

         if ( dice (100 ) <= (unsigned int ) p_etable [ selected_group ] [ nb_monster ]. prob )
         {
         //printf ("probabilty passed\n");
            if (errorsql == SQLITE_ROW)
            {
            //printf ("selection passed\n");
            // Select monster, else skip to next monster in case there are other valid monsters
            //sprintf (originalname, "%s", tmpmonster.name() );

            // add letters to differentiate monster
               if ( p_etable [ selected_group ] [ nb_monster ]. bonus > 0)
                  sprintf (tmpmonstername, "%s %c (Boss)", tmpmonster.name(), 'A' + nb_monster );
               else
                  sprintf (tmpmonstername, "%s %c", tmpmonster.name(), 'A' + nb_activemonster );


            //to do add monster bonus and change name (ex: Boss Troll or troll (Boss) )
               tmpmonster.roll_maxHPMP();
               tmpmonster.name (tmpmonstername);

               sprintf (tmpmonstername, "%s %c", tmpmonster.uname(), 'A' + nb_activemonster );
               tmpmonster.uname (tmpmonstername);

               tmpmonster.SQLinsert ();
               //printf ("Encounter: Start: after insertmonster\n");
               nb_activemonster++;
            }
         }
         nb_monster++;
         errorsql = tmpmonster.template_SQLselect ( p_etable [ selected_group ] [ nb_monster ] . id );

/*printf ("Monster %d id=%d prob=%d bonus=%d\n", nb_monster
        , p_etable [ selected_group ] [ nb_monster ] . id
        , p_etable [ selected_group ] [ nb_monster ] . prob
        , p_etable [ selected_group ] [ nb_monster ] . bonus );*/


      }

   //SQLcommit_transaction();

//printf ("Encounter: 1-1\r\n");
   /*if ( retval == SQLITE_OK )
   {
      retval = tmpmonster.SQLstep();

      SQLbegin_transaction();

   //printf ("Encounter: 1-2\r\n");
      while ( retval == SQLITE_ROW && nb_monster < max)
      {
         //tmpmonster.position ( tmpmonster.like_front() );
         //SQLbegin_transaction();

         //tmpmonster.SQLerrormsg();
         //tmpmonster.SQLinsert ();
         //tmpmonster.SQLerrormsg();
         //tmpmonster.SQLinsert ();
         //tmpmonster.SQLerrormsg();
         //SQLcommit_transaction();
         //printf ("Encounter: 1-3\r\n");
         sprintf (originalname, "%s", tmpmonster.name() );
         tmpmonster.SQLinsert ();
         nb_monster++;
         //monsterid = 1;


         while (dice(100) <= 75 && nb_monster > 0 )
         {
            // todo Add letter only if same name than before
            // might be hard to do
            tmpmonster.roll_maxHPMP();
            sprintf (tmpmonstername, "%s %c", originalname, 'A' + monsterid );
            tmpmonster.name (tmpmonstername);
            tmpmonster.SQLinsert ();
            nb_monster--;
            monsterid++;
         }
         //to do: add a 50 or 75% chance to get multiple copies of same monster
         retval = tmpmonster.SQLstep();
      }

      SQLcommit_transaction();

   }
   tmpmonster.SQLfinalize ();
*/
   /*WinData<int> wdat_ennemy_bar ( WDatProc_ennemy_bar, 0, WDatProc_POSITION_ENNEMY_BAR );

   WinMessage wmsg_test ("test");

   Window::show_all();

   int nb_monster = SQLcount ( "name", "monster", "");

   printf ("Encounter: nb monster after insert=%d\r\n", nb_monster);*/


   //while ( mainloop_readkeyboard() != KEY_ENTER );

   //WinMessage wmsg_encounter ("Encounter!");

   //Window::show_all();

   //party.status ( Party_STATUS_COMBAT );
      party.status (Party_STATUS_COMBAT);

      rest(1000);
   }
   else
   {
      /*WinTitle wttl_encounter ("Empty Encounter !", 320, 200, true);
      maze.draw_maze();
      Window::show_all();
      wttl_encounter.hide();*/

      party.status (Party_STATUS_MAZE);
      //rest (1000);
   }
 /* --- old code ---

 Ennemy tmpenn;
   Party tmparty;
   int tmptag;
//   string tmpstr;
   char strmsg [ 241 ];
   int i;
   int j;
   bool engage = false;
   bool nodouble = true;
   bool friendly = false;
   bool docombat = false;
   int answer;
   int rndval;

//   play_music_track ( System_MUSIC_w3fight );
   stop_midi();
   play_sample ( SMP_sound038, 255, 128, 1000, 0 );
   WinTitle wttl_encounter ("Encounter", 320, 200 );

   //load_backup_screen();
   //maze.draw_maze();
   blit_mazebuffer();
   Window::draw_all();
   copy_buffer();

   rest ( 800 );

   wttl_encounter.hide();
   rndval = rnd ( 5 );
   //?? maybe change probability with scouting ability

   if ( rndval == 5 && p_door != true )
   {
      strcpy ( strmsg, "You see ahead a group of\n" );

      for ( i = 0 ; i < p_nb_selected ; i++ )
      {
         nodouble = true;
         for ( j = 0 ; j < i ; j++ )
         {
            if ( p_selection [ i ] == p_selection [ j ] )
               nodouble = false;
         }
         if ( nodouble == true )
         {
         */
        /*    tmpenn.alDBselect ( Ennemy_DATATYPE_MONSTER,
               sizeof ( dbs_Ennemy_monster ), p_selection [ i ] );
            strcat ( strmsg, tmpenn.group_name() );
            strcat ( strmsg, ", " );*/

/*         }
      }
      strcat ( strmsg, "\nDo you want to engage ?" );

      WinQuestion wqst_engage ( strmsg, 320, 200 );

      //load_backup_screen();
      maze.draw_maze();
      answer = Window::show_all();

      if ( answer == 0 )
         engage = true;
   }
   else
      engage = true;

   if ( engage == true )
   {

      //?? check for friendlyness
      //?? Check for surprise

      if ( friendly == true )
      {
         strcpy ( strmsg, "Encounter is friendly" );
         WinMessage wmsg_friendly ( strmsg, 320, 200 );
         wmsg_friendly.preshow();

         //load_backup_screen();
         //maze.draw_maze();
         blit_mazebuffer();
         Window::draw_all();
         copy_buffer();

         rest ( 1000 );
         return ( Encounter_END );
      }
      else
      {
         strcpy ( strmsg, "You engage the Ennemy" );
 //        db.default_source ( DBSOURCE_TEMP );

         for ( i = 0 ; i < p_nb_selected ; i++ )
         {
 */
 /*           tmpenn.alDBselect ( Ennemy_DATATYPE_MONSTER,
               sizeof ( dbs_Ennemy_monster ), p_selection [ i ] );*/
/*         WinData<Opponent> wdat_opponnent_stat ( WDatProc_opponent_stat,
            tmpenn, WDatProc_POSITION_OPPONENT_STAT );
         Window::show_all();
         copy_buffer();
         while ( ( readkey() >> 8 ) != KEY_ENTER );*/
/*
            tmptag = tmpenn.SQLinsert();
            tmparty.add_character ( tmptag );
         }

         tmparty.ennemy_rankprioritysort();
         p_party = tmparty.SQLinsert();
         docombat = true;
         // db.default_source ( DBSOURCE_SAVEGAME );

         WinMessage wmsg_engage ( strmsg, 320, 200 );
         wmsg_engage.preshow();

         //load_backup_screen();
         //maze.draw_maze();
         blit_mazebuffer();
         Window::draw_all();
         copy_buffer();
         rest ( 1000 );
         return ( Encounter_ENGAGE );
      }
   }
*/
   return ( Encounter_IGNORE );
}

/*void Encounter::clear_selection ( void )
{
   int i;

   for ( i = 0 ; i < EnemyGroup_NB_ENEMY ; i++ )
      p_selection [ i ] = 0;
//   p_side = 0;
   p_nb_selected = 0;
   p_counter = Encounter_BASE_COUNTER;
   //p_party = 0;
   p_door = false;

}*/

/*void Encounter::clear_objects ( void )
{*/
  /* Party tmparty;
   Ennemy tmpenn;
   int i;

   tmparty.SQLselect ( p_party );

   for ( i = 0 ; i < tmparty.nb_character() ; i++ )
   {
      tmpenn.SQLselect ( tmparty.FKcharacter ( i ) );
      tmpenn.SQLdelete();
   }

   tmparty.SQLdelete();
*/

/*p_counter = Encounter_BASE_COUNTER;
   //p_party = 0;
   p_door = false;*/
//}

/*int Encounter::get_ennemy_party ( void )
{
   //return ( p_party );
}*/


/*-------------------------------------------------------------------------*/
/*-                            Private Methods                            -*/
/*-------------------------------------------------------------------------*/

/*void Encounter::select_encounter ( void )
{
   short rndval;
   int ngprob = 100;
   bool endselect = false;
   int grpsize;
   int maxgrpsize;
   short ennid;
   int i;
   int index;
   bool selectOK;
   //Ennemy tmpennemy;

// this is some old code
   clear_selection();

   index = 0;
   while ( endselect == false )
   {
      rndval = rnd(100);
      if ( rndval <= ngprob && p_nb_selected < 6 )
      {
         maxgrpsize = 6 - p_nb_selected;
         if ( maxgrpsize > 3 )
            maxgrpsize = 3;

         grpsize = rnd(maxgrpsize);

         selectOK = false;
         while ( selectOK == false )
         {*/
     /*       ennid = rnd ( p_nb_ennemy );
            tmpennemy.alDBselect ( Ennemy_DATATYPE_MONSTER,
               sizeof ( dbs_Ennemy_monster ), p_elist [ ennid - 1 ] );*/
            /*if ( rnd ( 100 ) <= tmpennemy.eprob() )
               selectOK = true;*/
/*         }

         for ( i = 0 ; i < grpsize; i++ )
         {
            p_selection [ index ] = p_elist [ ennid - 1 ];
            index++;
         }

         ngprob = ngprob / 2;
         p_nb_selected = p_nb_selected + grpsize;
      }
      else
         endselect = true;
   }
*/
//   maxgrpsize = 2;//rnd ( 3 );

//   tmpval = 6 / maxgrpsize;

//   if ( tmpval == 1 )
//      nbgrp = 1;
//   else
//      nbgrp = 2;//rnd ( tmpval );

//   index = 0;
/*   for ( j = 0 ; j < 1 ; j++ )
   {
//      if ( maxgrpsize == 1 )
//         grpsize = 1;
//      else
         grpsize = 2;//rnd ( maxgrpsize );

      ennid = rnd ( p_nb_ennemy );
      for ( i = 0 ; i < 2; i++ )
      {
         p_selection [ index ] = p_elist [ ennid ];
         index++;
      }
   }*/

//      p_selection [ 0 ] = p_elist [ rnd ( p_nb_ennemy ) ];
//      p_selection [ 1 ] = p_elist [ rnd ( p_nb_ennemy ) ];
//      p_nb_selected = 2;
//      for ( i = 0 ; i < 2; i++ )
//      {
//         p_selection [ index ] = p_elist [ ennid ];
//         index++;
//      }


   //?? todo : reorder monster according to location priority

//}

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

Encounter encount; // encounter engine

