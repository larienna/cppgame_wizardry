/***************************************************************************/
/*                                                                         */
/*                         E D I T O R . C P P                             */
/*                         MOdule Source Code                              */
/*                                                                         */
/*     Content : Module Editor source code                                 */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : February 22nd, 2004                                 */
/*     License : GNU General Public License                                */
/*                                                                         */
/***************************************************************************/

// Include Groups

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

#include <wdatproc.h>
//#include <editor.h>


/*
//#include <time.h>
#include <allegro.h>


#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>



//
//
//
//
//
//#include <list.h>

#include <opponent.h>
#include <charactr.h>
//#include <monster.h>
#include <party.h>

#include <game.h>

//#include <city.h>
#include <maze.h>
#include <editor.h>
#include <encountr.h>
#include <mazeproc.h>
//
//#include <camp.h>
#include <config.h>
#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winquest.h>
#include <winmessa.h>
#include <wintitle.h>
#include <windata.h>
//#include <wdatproc.h>


//fs_maz_header Editor_header;
//s_Editor_maze_tile Editor_curtile;
*/


/*-------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                         -*/
/*-------------------------------------------------------------------------*/

Editor::Editor ( void )
{
   //short i;
   //short j;
   //short k;
   //s_mazetile tile;

   //p_width = 40;
   //p_depth = 10;
   //p_sky = -1;
   p_rclevel = 0;
   p_startpos.x = 0;
   p_startpos.y = 0;
   p_startpos.z = 0;
   p_startpos.facing = Party_FACE_NORTH;

   p_xcur = 0;
   p_ycur = 0;
   p_zcur = 0;

   p_xscroll = 0;
   p_yscroll = 0;


   //tile.walltex = 0;
   //tile.floortex = 0;
   //tile.objectimg = 0;
   //tile.event = 0;
   //tile.special = 0;
   //tile.texture = 0;
   //tile.solid = 0;

   //tile.objectpic = 0; //* 4 bit Floor Object, 4 bit ceiling object from palette
   //tile.wobjpalette = 0; //* 3 bit wall object + 5 bit palette ID
   //tile.objposition = 0; //* 2 bit wall + 2 bit ceiling + 4 bit floor
   //tile.masked = 0; //* 4 bit Floor Ceiling texture + 4 bit wall texture
   //tile.maskposition = 0; //

   p_tool = 0;
   p_layer = 0;
   p_xmark = 0;
   p_ymark = 0;
   p_selection_active = false;
   p_active_palette = 0;
   p_adventure_selected = false;
   strcpy ( p_adventure_databasefile, "");
   strcpy ( p_adventure_mazefile, "");

   clear_selection();

/*

   k = 0;
   j = 0;
   for ( k = 0 ; k < Maze_MAXDEPTH; k++ )
       for ( j = 0 ; j < Maze_MAXWIDTH; j++ )
       {
         for ( i = 0 ; i < Maze_MAXWIDTH; i++ )
         {
            p_map [ i ] [ j ] [ k ] = tile;
         }
       }*/

//   p_map = create_bitmap ( 640, 480 );


}

Editor::~Editor ( void )
{

}


/*-------------------------------------------------------------------------*/
/*-                            Methods                                    -*/
/*-------------------------------------------------------------------------*/

void Editor::start ( void )
{
   //printf ("Debug: Editor.start: Starting editor\n");
   clear ( buffer );
   //clear ( backup );
   clear ( editorbuffer );
   p_xcur = 0;
   p_ycur = 0;
   p_zcur = 0;
   p_xscroll = 0;
   p_yscroll = 0;
   //unsigned char tmpbyte;
   //unsigned char tmpbyte2;
   bool exit_editor = false;
   int menu_retval;
   int tmpal;

   //maze.load_hardcoded_texture();

   clear_maze();

   mazepalid = ( mazetile [p_zcur][p_ycur][p_xcur].wobjpalette & MAZETILE_PALETTE_MASK );
   //TODO BUG: Cannot load while file not open, maybe force a file open to avoid bug
   mazepal.load ( mazepalid );
   mazepal.build();

   unsigned char tmpkey = 0;

   //s_mazetile cursortile = mazetile [p_zcur][p_ycur][p_xcur];

//   textout_centre_old ( subbackup, font, "This is a test", 320, 200,
//      General_COLOR_TEXT );



   stop_midi();

   while ( exit_editor == false )
   {
      //printf ("Debug: Editor.start: Entering Loop\n");
      Window::instruction ( 216, 452 , Window_INSTRUCTION_EDITOR );

      tmpal = ( mazetile [p_zcur][p_ycur][p_xcur].wobjpalette & MAZETILE_PALETTE_MASK );

      if ( mazepalid != tmpal)
      {
         //printf ("Debug:Editor:Start: Loading palette %d\n", mazepalid );
         mazepalid = tmpal;
         mazepal.load ( mazepalid );
         mazepal.build();
      }

      //read_curtile();
      clear (editorbuffer);
      draw_map ();
      draw_detailed_tile();

      blit_editorbuffer();

      // tmpcode to redump an empty maze
      //maze.save_to_mazefile("adventure//maze.bin");

      if ( p_selection_active == false)
         textout_old ( subbuffer, FNT_small, "[ Arrows ] move [ PgDnUp ] Change Level [ Del ] Delete"
            , 0, 452, General_COLOR_TEXT ); // draw here to allow window instruction to overlap
      else
         textout_old ( subbuffer, FNT_small, "[ Arrows ] move [ Del ] Delete"
            , 0, 452, General_COLOR_TEXT ); // draw here to allow window instruction to overlap
      copy_buffer();

      tmpkey = mainloop_readkeyboard();
      clear_keybuf();

      switch ( tmpkey )
      {
         case KEY_RIGHT :
            if ( p_xcur < Maze_MAXWIDTH - 1 )
            {
               p_xcur++;
               if ( p_xscroll < ( p_xcur - 25 ) &&
                  p_xscroll < 100 - 25 )
                  p_xscroll++;
            }
         break;

         case KEY_LEFT :
            if ( p_xcur > 0 )
            {
               p_xcur--;
               if ( p_xscroll > ( p_xcur )  &&
                  p_xscroll > 0  )
                  p_xscroll--;
            }
         break;

         case KEY_UP :
            if ( p_ycur < Maze_MAXWIDTH - 1 )
            {
               p_ycur++;
               if ( p_yscroll < ( p_ycur - 25 ) &&
                  p_yscroll < 100 - 25 )
                  p_yscroll++;
            }
         break;

         case KEY_DOWN :
            if ( p_ycur > 0 )
            {
               p_ycur--;
               if ( p_yscroll > ( p_ycur )  &&
                  p_yscroll > 0  )
                  p_yscroll--;
            }
         break;

         case KEY_PGUP :
            if ( p_selection_active == false )
               if ( p_zcur > 0 )
                  p_zcur--;
         break;

         case KEY_PGDN :
            if ( p_selection_active == false )
               if ( p_zcur < Maze_MAXDEPTH -1 )
                  p_zcur++;
         break;

         case KEY_DEL :  // solid trigger change key also erase content
            cmd_delete ();
         break;

         case KEY_W :
            if ( p_tool == Editor_TOOL_TILE)
               cmd_wall ( Maze_2BIT_NORTH );
            else
            {
               if ( p_ycur < Maze_MAXWIDTH - 1 )
               {
                  p_ycur++;
                  if ( p_yscroll < ( p_ycur - 25 ) &&
                     p_yscroll < 100 - 25 )
                     p_yscroll++;

                  if (p_tool == Editor_TOOL_CORRIDOR)
                     cmd_corridor ( Maze_2BIT_NORTH );
                  if (p_tool == Editor_TOOL_BRUSH)
                     p_selection [ p_ycur ] [p_xcur] = true;
               }
            }


         break;

         case KEY_D :
            if ( p_tool == Editor_TOOL_TILE)
               cmd_wall ( Maze_2BIT_EAST );
            else
            {
               if ( p_xcur < Maze_MAXWIDTH - 1 )
               {
                  p_xcur++;
                  if ( p_xscroll < ( p_xcur - 25 ) &&
                     p_xscroll < 100 - 25 )
                     p_xscroll++;

                  if (p_tool == Editor_TOOL_CORRIDOR)
                     cmd_corridor ( Maze_2BIT_EAST );
                  if (p_tool == Editor_TOOL_BRUSH)
                     p_selection [ p_ycur ] [p_xcur] = true;
               }
            }
         break;


         case KEY_S :
            if ( p_tool == Editor_TOOL_TILE)
               cmd_wall ( Maze_2BIT_SOUTH );
            else
            {
               if ( p_ycur > 0 )
               {
                  p_ycur--;
                  if ( p_yscroll > ( p_ycur )  &&
                     p_yscroll > 0  )
                     p_yscroll--;

                  if (p_tool == Editor_TOOL_CORRIDOR)
                     cmd_corridor ( Maze_2BIT_SOUTH );
                  if (p_tool == Editor_TOOL_BRUSH)
                     p_selection [ p_ycur ] [p_xcur] = true;
               }
            }
         break;


         case KEY_A :
            if ( p_tool == Editor_TOOL_TILE)
               cmd_wall ( Maze_2BIT_WEST );
            else
            {
               if ( p_xcur > 0 )
               {
                  p_xcur--;
                  if ( p_xscroll > ( p_xcur )  &&
                     p_xscroll > 0  )
                     p_xscroll--;

                  if (p_tool == Editor_TOOL_CORRIDOR)
                     cmd_corridor ( Maze_2BIT_WEST );
                  if (p_tool == Editor_TOOL_BRUSH)
                     p_selection [ p_ycur ] [p_xcur] = true;
               }
            }
         break;

         case KEY_F1 :
            menu_retval = menu_file();
            if ( menu_retval == Editor_MENU_EXIT)
            {

               //printf ("editor.start: should exit editor\n");
               exit_editor = true;
            }
         break;

         case KEY_F2 :
            menu_retval = menu_tool();
         break;

         case KEY_F3 :
            menu_retval = menu_texture();
         break;

         case KEY_F4 :
            menu_retval = menu_view();
         break;

         case KEY_F5 :
            menu_retval = menu_layer();
         break;

         case KEY_F6 :
            menu_retval = menu_error();
         break;

         case KEY_F7 :
            menu_retval = menu_data();
         break;

         case KEY_F8:
            menu_retval = menu_event();
         break;

         // short cut keys

         case KEY_1:
            if ( p_selection_active == false )
               p_tool = Editor_TOOL_TILE;
         break;

         case KEY_2:
            if ( p_selection_active == false )
               p_tool = Editor_TOOL_CORRIDOR;
         break;
         case KEY_3:
            if ( p_selection_active == false )
               p_tool = Editor_TOOL_RECTANGLE;
         break;
         case KEY_4:
            if ( p_selection_active == false )
               p_tool = Editor_TOOL_BRUSH;
         break;

         case KEY_0:
            if ( p_selection_active == false )
               start_test_maze();
         break;

         case KEY_9:
            if ( p_selection_active == false )
               show_full_map();
         break;

         case KEY_OPENBRACE:
            if ( p_tool == Editor_TOOL_TILE )
            {
               tmpal = get_palette( p_zcur, p_ycur, p_xcur);
               if ( tmpal > 0)
               {
                  tmpal--;
                  set_palette ( p_zcur, p_ycur, p_xcur, tmpal );
               }
            }
         break;

         case KEY_CLOSEBRACE:
            if ( p_tool == Editor_TOOL_TILE )
            {
               tmpal = get_palette( p_zcur, p_ycur, p_xcur);
               if ( tmpal < Maze_NB_TEXPALETTE - 1)
               {
                  tmpal++;
                  set_palette ( p_zcur, p_ycur, p_xcur, tmpal );
               }

            }
         break;

         // Selection keys

         case KEY_Z:
            if ( p_selection_active == false)
            {

               if ( p_tool == Editor_TOOL_RECTANGLE )
               {
                  p_xmark = p_xcur;
                  p_ymark = p_ycur;
                  p_selection_active = true;
                  //printf ("Editor.start: Selection should be active\n");
               }
               else if ( p_tool == Editor_TOOL_BRUSH)
               {
                  p_selection [p_ycur][p_xcur] = true;
                  p_selection_active = true;
               }
            }
         break;

         case KEY_X:
            if ( p_selection_active == true )
            {

               if ( p_tool == Editor_TOOL_RECTANGLE )
               {
                  p_xmark = 0;
                  p_ymark = 0;
               }
               /*else if ( p_tool == Editor_TOOL_AREA)
               {
                  // unselect all area

               }*/
               clear_selection();
               p_selection_active = false;
            }
         break;

         case KEY_R:
            if ( p_selection_active == true )
            {
               if ( p_tool == Editor_TOOL_RECTANGLE)
                  rect_to_sel();

               menu_selection();

               if ( p_tool == Editor_TOOL_RECTANGLE )
               {
                  // used to patch up a selection bug
                  p_xmark = 0;
                  p_ymark = 0;
                  clear_selection();
                  p_selection_active = false;
               }



               //p_xmark = 0;
               //p_ymark = 0;
               //p_selection = false;
            }
         break;

         case KEY_MINUS:
            mazetile [ p_zcur][p_ycur][p_xcur].event = 0;
         break;

         case KEY_EQUALS:
            select_event();
         break;

      }
   }

}

void Editor::start_test_maze ( void )
{
   party.position ( p_xcur, p_ycur, p_zcur, Party_FACE_NORTH );
   //maze.light ( Maze_LIGHT_LEVEL_3 );
   //maze.load_hardcoded_texture();

   mazepal.load ( mazetile [p_zcur][p_ycur][p_xcur].wobjpalette & MAZETILE_PALETTE_MASK );
   mazepal.build();

   maze.start( true );
}

int Editor::menu_file ( void )
{
   int answer;
   int answer2;
   Menu mnu_file;
   int retval = Editor_MENU_NONE;
   bool hidden=false;
   int dberror;
   int mazesuccess;

   if ( p_adventure_selected == false )
      hidden = true;

   mnu_file.add_item (0, "Select Adventure");
   mnu_file.add_item (1, "Load Adventure", hidden);
   mnu_file.add_item (2, "Save Adventure", hidden);
   mnu_file.add_item (3, "Exit");

   WinMenu wmnu_file ( mnu_file, 0, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   if (answer == 0)
      select_adventure();
   else if ( answer == 1 )
   {
      //WinQuestion wqst_exit ("Are you sure you want to exit the editor?\nUnsaved data will be lost");
      WinQuestion wqst_load ("Are you sure you want to reload your adventure data?\nAll changes will be lost.");
      blit_editorbuffer();
      answer2 = Window::show_all();
      if ( answer2 == WinQuestion_ANSWER_YES)
      {
         SQLclose();
         mazesuccess = maze.load_from_mazefile (p_adventure_mazefile);
         dberror = SQLopen ( p_adventure_databasefile );

         if ( mazesuccess == false || dberror != SQLITE_OK )
         {
            WinMessage wmsg_errorload ( "Error loading adventure\nResetting Content" );
            blit_editorbuffer();
            Window::show_all();
            clear_maze();

            p_adventure_selected = false;
         }
         else
         {
            p_zcur = 0;
            p_xcur = 0;
            p_ycur = 0;
            p_xscroll = 0;
            p_yscroll = 0;
            mazepalid = ( mazetile [p_zcur][p_ycur][p_xcur].wobjpalette & MAZETILE_PALETTE_MASK );
            mazepal.load ( mazepalid );
            mazepal.build();
            editorpal.load ( 0 );
            editorpal.build();
            palsample.load_build();
         }
      }
   }
   else if ( answer == 2)
   {
         mazesuccess = maze.save_to_mazefile (p_adventure_mazefile);
         //dberror = SQLcommit();

         if ( mazesuccess == false )
         {
             WinMessage wmsg_errorsave ( "Error Saving Maze\nContent still in memory" );
               blit_editorbuffer();
               Window::show_all();
         }
/*         else if ( dberror != SQLITE_OK )
            {
printf ("Editor: Sql error = %d", dberror);
               WinMessage wmsg_errorsave ( "Error Saving Database\nContent still in memory" );
               blit_editorbuffer();
               Window::show_all();
            }*/
   }
   else if ( answer == 3 )
   {
         WinQuestion wqst_exit ("Are you sure you want to exit the editor?\nUnsaved data will be lost");
         blit_editorbuffer();
         answer2 = Window::show_all();
         if ( answer2 == WinQuestion_ANSWER_YES)
         {
            SQLclose();
            clear_maze();
            p_adventure_selected = false;
            retval = Editor_MENU_EXIT;
         }
   }

   return ( retval );
}

int Editor::menu_tool ( void )
{
   int answer;
   //int answer2;
   Menu mnu_file;
   int retval = Editor_MENU_NONE;

   if ( p_tool == Editor_TOOL_TILE )
      mnu_file.add_item (Editor_TOOL_TILE, "* Tile (1)", true);
   else
      mnu_file.add_item (Editor_TOOL_TILE, "  Tile (1)", p_selection_active );

   if ( p_tool == Editor_TOOL_CORRIDOR )
      mnu_file.add_item (Editor_TOOL_CORRIDOR, "* Corridor (2)", true);
   else
      mnu_file.add_item (Editor_TOOL_CORRIDOR, "  Corridor (2)", p_selection_active );

if ( p_tool == Editor_TOOL_RECTANGLE )
      mnu_file.add_item (Editor_TOOL_RECTANGLE, "* Rectangle (3)", true);
   else
      mnu_file.add_item (Editor_TOOL_RECTANGLE, "  Rectangle (3)", p_selection_active);

if ( p_tool == Editor_TOOL_BRUSH )
      mnu_file.add_item (Editor_TOOL_BRUSH, "* Area (4)", true);
   else
      mnu_file.add_item (Editor_TOOL_BRUSH, "  Area (4)", p_selection_active );

   WinMenu wmnu_file ( mnu_file, 72, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   if ( answer != -1 )
      p_tool = answer;

   return ( retval );
}

int Editor::menu_texture ( void )
{
   int answer;
   int answer2 = 0;
   Menu mnu_texture;
   int retval = Editor_MENU_NONE;
   WinTitle wttl_loadpalette ("Loading Palette", 320, 200);
   wttl_loadpalette.hide();
   bool hidden = false;

   if ( p_adventure_selected == false )
      hidden = true;
   else
      hidden = p_selection_active;

   mnu_texture.add_item (0, "Active Palette", hidden);
   mnu_texture.add_item (1, "Edit Palette", hidden);

   WinMenu wmnu_texture ( mnu_texture, 150, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   switch ( answer )
   {
      case 0:
         wmnu_texture.hide();
         answer2 = select_palette();
         if ( answer2 != -1)
         {
            p_active_palette = answer2;
            wttl_loadpalette.unhide();
            blit_editorbuffer();
            Window::draw_all();
            editorpal.load (answer2);
            editorpal.build();

         }

      break;
      case 1:
         while ( answer2 != -1)
         {
            wmnu_texture.hide();
            answer2 = select_active_palette_texture();
            if ( answer2 != -1)
            {
//printf ("Debug:Editor:Start: Before call edit pal=%d, tex=%d\n", p_active_palette, answer2 );
               editorpal.tex [ answer2].edit ( p_active_palette, answer2 );
               editorpal.build();
            }
         }

         palsample.load_build();
      break;
   }

   return ( retval );



}

int Editor::menu_view ( void )
{
   int answer;
   //int answer2;
   Menu mnu_file;
   int retval = Editor_MENU_NONE;

   mnu_file.add_item (0, "Full Map (9)", p_selection_active);
   mnu_file.add_item (1, "Maze View (0)", p_selection_active);

   WinMenu wmnu_file ( mnu_file, 216, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   switch ( answer )
   {
      case 0:
         wmnu_file.hide();
         show_full_map ();
      break;
      case 1:
         wmnu_file.hide();
         start_test_maze();
      break;
   }

   return ( retval );


}

int Editor::menu_layer ( void )
{
   return ( Editor_MENU_NONE );
}

int Editor::menu_error ( void )
{
   return ( Editor_MENU_NONE );
}

int Editor::menu_data ( void )
{
   return ( Editor_MENU_NONE );
}

int Editor::menu_event ( void )
{
   int answer;
   Menu mnu_event;
   //int retval = Editor_MENU_NONE;
   bool hidden = false;

   if ( p_adventure_selected == false )
      hidden = true;

   mnu_event.add_item (0, "Mark Event (+=)", hidden);
   mnu_event.add_item (1, "Unmark Event (-_)", hidden);

   WinMenu wmnu_event ( mnu_event, 400, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   wmnu_event.hide();

   switch ( answer )
   {
      case 0:
         select_event ();
      break;
      case 1:
         mazetile [p_zcur][p_ycur][p_xcur].event = 0;
      break;
   }

   return ( Editor_MENU_NONE);
}

int Editor::menu_selection ( void )
{
   int answer;
   int answer2;
   Menu mnu_file ("Selection Menu");
   int retval = Editor_MENU_NONE;
   int i;
   int tmpal;

   mnu_file.add_item (0, "Make a room");
   mnu_file.add_item (1, "Set a Filling" );
   mnu_file.add_item (2, "Set as Magic Bounce ");
   mnu_file.add_item (3, "Set as Light");
   mnu_file.add_item (4, "Change Palette" );
   mnu_file.add_item (5, "Set masked floor texture", false );
   mnu_file.add_item (6, "Set masked ceiling texture", false );
   mnu_file.add_item (7, "Delete");
   mnu_file.add_item (8, "Remove Special and Fillings");

   mnu_file.add_item (-1, "Do Nothing" );

   WinMenu wmnu_file ( mnu_file, 48, 48 );

   blit_editorbuffer();
   answer = Window::show_all();

   //if ( answer == 0)

   wmnu_file.hide();

   if ( answer == 0)
      sel_build_room ();

   if ( answer == 1)
   {
      Menu mnu_filling ("Select Filling");

      for ( i = 0 ; i < Maze_NB_FILLING ; i++ )
      {
         mnu_filling.add_item ( i, STR_EDT_SPECIAL_FILL [i] );
      }
      WinMenu wmnu_filling ( mnu_filling, 48, 48, true );

      blit_editorbuffer();
      answer2 = Window::show_all();

      sel_filling ( answer2 );
   }

   if ( answer == 2)
      sel_special ( MAZETILE_SPECIAL_MAGIKBOUNCE);

   if ( answer == 3)
      sel_special ( MAZETILE_SPECIAL_LIGHT);

   if ( answer == 4)
   {
      tmpal = select_palette ();

      if ( tmpal != -1 )
         sel_palette ( tmpal );
   }

   if ( answer == 5 || answer == 6)
   {

      List lst_texture ("Select the texture?", 16, true );
      WinData<BITMAP> wdat_texture ( WDatProc_texture_bitmap, *(editorpal.bmp[TexPal_IDX_MFLOOR]),
                                 WDatProc_POSITION_TEXTURE  );

      answer2 = 0;
      for ( i = 0; i < TexPalette_NB_MASKEDFLOOR; i++)
      {
         // need to offset the selected texture because flor ceiling textures goes from 24 to 39
         lst_texture.add_item ( i,STR_TEX_PALETTE [i + TexPal_IDX_MFLOOR]);
      }

      WinList wlst_texture ( lst_texture, 20, 40 );

      while ( lst_texture.selected() == false && answer2 != -1 )
      {
         blit_editorbuffer();
         answer2 = Window::show_all();

         if ( answer2 != -1 )
         {
            wdat_texture.parameter ( *(editorpal.bmp [ answer2 ]) );
            Window::refresh_all();
         }

      }

      if ( lst_texture.selected() == true )
      {
         if ( answer == 5)
            sel_masked ( answer2, MAZETILE_MASKTEXPOS_FLOOR );
         else
            sel_masked ( answer2, MAZETILE_MASKTEXPOS_CEILING );
      }

   }

   if ( answer == 7)
      sel_delete ();

   if ( answer == 8)
      sel_remove_special ();



   return ( retval );
}


void Editor::cmd_wall ( int side ) // use maze 2bit
{
   unsigned char tmpwall = get_mazetile_wall( mazetile [p_zcur][p_ycur][p_xcur], side);

   // Cycle trought the valid wall types
   tmpwall++;
   if ( tmpwall > MAZETILE_SOLIDTYPE_DOOR )
   {   tmpwall = 0;
   }

   set_mazetile_wall ( &mazetile [p_zcur][p_ycur][p_xcur], side, tmpwall );

   if ( ( mazetile [p_zcur][p_ycur][p_xcur].special & MAZETILE_SPECIAL_SOLID ) > 0 )
      mazetile [p_zcur][p_ycur][p_xcur].special = ( mazetile [p_zcur][p_ycur][p_xcur].special & ~MAZETILE_SPECIAL_SOLID );

}

void Editor::cmd_delete ( void )
{
   delete_tile( p_zcur, p_ycur, p_xcur);
}

void Editor::cmd_corridor ( int direction ) // use maze 2bit constants
{
   //int zsrc = p_zcur;
   //int ysrc = p_ycur;
   //int xsrc = p_xcur;
   //unsigned int tmpsolid;
   //int i;
   //int walltype;

   /*switch ( direction )
   {
      case Maze_2BIT_NORTH: ysrc--; break;
      case Maze_2BIT_EAST: xsrc--; break;
      case Maze_2BIT_SOUTH: ysrc++; break;
      case Maze_2BIT_WEST: xsrc++; break;
   }*/

   // remove solid

   mazetile [p_zcur][p_ycur][p_xcur].special =
      ( mazetile [p_zcur][p_ycur][p_xcur].special & ~MAZETILE_SPECIAL_SOLID) ;

   // set_active_palette

   mazetile  [p_zcur][p_ycur][p_xcur].wobjpalette =
      (mazetile  [p_zcur][p_ycur][p_xcur].wobjpalette & ~MAZETILE_PALETTE_MASK)
      + p_active_palette;

   // remove the wall from where you came from

   switch ( direction )
   {
      case Maze_2BIT_NORTH:
         set_mazetile_wall( &mazetile [p_zcur][ p_ycur-1][ p_xcur], direction
            , MAZETILE_SOLIDTYPE_NONE);
      break;
      case Maze_2BIT_SOUTH:
         set_mazetile_wall ( &mazetile [p_zcur][ p_ycur+1][ p_xcur], direction
            , MAZETILE_SOLIDTYPE_NONE);
      break;
      case Maze_2BIT_EAST:
         set_mazetile_wall ( &mazetile [p_zcur][ p_ycur][ p_xcur-1], direction
            , MAZETILE_SOLIDTYPE_NONE);
      break;
      case Maze_2BIT_WEST:
         set_mazetile_wall ( &mazetile [p_zcur][ p_ycur][ p_xcur+1], direction
            , MAZETILE_SOLIDTYPE_NONE);
      break;
   }



   // Set copies of wall from each non-solid area

   patch_surrounding_walls( p_zcur, p_ycur, p_xcur );



   //mazetile [p_zsrc][p_ysrc][p_xsrc].special =
   //   ( mazetile [p_zcur][p_ycur][p_xcur].special & ~MAZETILE_SPECIAL_SOLID) ;


   // side walls in paralell to the direction

   /*switch (direction)
   {
      case Maze_2BIT_NORTH :
      case Maze_2BIT_SOUTH :
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur].solid & ~MAZETILE_SOLID_WEST_MASK );
         //   mazetile [p_zcur][p_ycur][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_WEST_WALL);
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur].solid & ~MAZETILE_SOLID_EAST_MASK );
         //   mazetile [p_zcur][p_ycur][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_EAST_WALL);
         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_WEST);
         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_EAST);

         if ( p_ycur < Maze_MAXWIDTH -1 )
            if ( ( mazetile [ p_zcur][ p_ycur+1][ p_xcur].special & MAZETILE_SPECIAL_SOLID ) == 0 )
               set_wall ( p_zcur, p_ycur+1, p_xcur, Maze_2BIT_SOUTH);
         if ( p_ycur > 0 )
            if ( ( mazetile [ p_zcur][ p_ycur-1][ p_xcur].special & MAZETILE_SPECIAL_SOLID ) == 0 )
               set_wall ( p_zcur, p_ycur-1, p_xcur, Maze_2BIT_NORTH);
      break;

      case Maze_2BIT_EAST :
      case Maze_2BIT_WEST :
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur].solid & ~MAZETILE_SOLID_NORTH_MASK );
         //   mazetile [p_zcur][p_ycur][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_NORTH_WALL);
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur].solid & ~MAZETILE_SOLID_SOUTH_MASK );
         //   mazetile [p_zcur][p_ycur][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_SOUTH_WALL);
         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_NORTH);
         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_SOUTH);

         if ( p_xcur < Maze_MAXWIDTH -1 )
            if ( ( mazetile [ p_zcur][ p_ycur][ p_xcur+1].special & MAZETILE_SPECIAL_SOLID ) == 0 )
               set_wall ( p_zcur, p_ycur, p_xcur+1, Maze_2BIT_WEST);
         if ( p_xcur > 0 )
            if ( ( mazetile [ p_zcur][ p_ycur][ p_xcur-1].special & MAZETILE_SPECIAL_SOLID ) == 0 )
               set_wall ( p_zcur, p_ycur, p_xcur-1, Maze_2BIT_EAST);
      break;
   }*/

   // old code

   /*switch (direction)
   {
      case Maze_2BIT_NORTH :


         if ( is_tile_solid ( p_zcur, p_ycur-1, p_xcur ) == false )
            remove_wall ( p_zcur, p_ycur-1, p_xcur, Maze_2BIT_NORTH ); // remove wall on source if not solid
         else
            set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_SOUTH ); // set wall if source is solid

         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_NORTH ); //Set a wall in front
         if ( is_tile_solid ( p_zcur, p_ycur+1, p_xcur ) == false )
            set_wall ( p_zcur, p_ycur+1, p_xcur, Maze_2BIT_SOUTH ); // set wall on the other side since empty

      break;
      case Maze_2BIT_SOUTH :
        if ( is_tile_solid ( p_zcur, p_ycur+1, p_xcur ) == false )
            remove_wall ( p_zcur, p_ycur+1, p_xcur, Maze_2BIT_SOUTH ); // remove wall on source if not solid
         else
            set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_NORTH ); // set wall if source is solid

         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_SOUTH ); //Set a wall in front
         if ( is_tile_solid ( p_zcur, p_ycur-1, p_xcur ) == false )
            set_wall ( p_zcur, p_ycur-1, p_xcur, Maze_2BIT_NORTH ); // set wall on the other side since empty
      break;
      case Maze_2BIT_EAST :
         if ( is_tile_solid ( p_zcur, p_ycur, p_xcur-1 ) == false )
            remove_wall ( p_zcur, p_ycur, p_xcur-1, Maze_2BIT_EAST ); // remove wall on source if not solid
         else
            set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_WEST ); // set wall if source is solid

         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_EAST ); //Set a wall in front
         if ( is_tile_solid ( p_zcur, p_ycur, p_xcur+1 ) == false )
            set_wall ( p_zcur, p_ycur, p_xcur+1, Maze_2BIT_WEST ); // set wall on the other side since empty
      break;
      case Maze_2BIT_WEST :
         if ( is_tile_solid ( p_zcur, p_ycur, p_xcur+1 ) == false )
            remove_wall ( p_zcur, p_ycur, p_xcur+1, Maze_2BIT_EAST ); // remove wall on source if not solid
         else
            set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_WEST ); // set wall if source is solid

         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_WEST ); //Set a wall in front
         if ( is_tile_solid ( p_zcur, p_ycur, p_xcur-1 ) == false )
            set_wall ( p_zcur, p_ycur, p_xcur-1, Maze_2BIT_EAST ); // set wall on the other side since empty
      break;
   }*/








}

void Editor::patch_surrounding_walls ( int z, int y, int x )
{
   //unsigned int tmpsolid;
   //int i;
   int walltype;

   //TODO: See if can make a loop from 0 to 3
   // test north to set or not walls
   if ( is_mazetile_special( mazetile [z][y+1][x], MAZETILE_SPECIAL_SOLID) == true
       || y == Maze_MAXWIDTH -1)
      set_mazetile_wall( &mazetile [z][y][x], Maze_2BIT_NORTH, MAZETILE_SOLIDTYPE_WALL);
   else
   {
      walltype = get_mazetile_wall( mazetile [z][y+1][x], Maze_2BIT_SOUTH );
      /*( ( mazetile [z][ y+1][x].solid
                  & Editor_POLYGON_INFO [Maze_2BIT_SOUTH] . mask )
                  >> Editor_POLYGON_INFO [Maze_2BIT_SOUTH] . shift );*/
      set_mazetile_wall( &mazetile [z][y][x], Maze_2BIT_NORTH, walltype );
   }

   //south
   if ( is_mazetile_special( mazetile [z][y-1][x], MAZETILE_SPECIAL_SOLID) == true
       || y == 0)
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_SOUTH, MAZETILE_SOLIDTYPE_WALL);
   else
   {
      walltype = get_mazetile_wall( mazetile [z][y-1][x], Maze_2BIT_NORTH );
      /*( ( mazetile [z][ y-1][x].solid
                  & Editor_POLYGON_INFO [Maze_2BIT_NORTH] . mask )
                  >> Editor_POLYGON_INFO [Maze_2BIT_NORTH] . shift );*/
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_SOUTH, walltype );
   }

   //east
   if ( is_mazetile_special( mazetile [z][y][x+1], MAZETILE_SPECIAL_SOLID) == true
       || x == Maze_MAXWIDTH - 1)
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_EAST, MAZETILE_SOLIDTYPE_WALL);
   else
   {
      walltype = get_mazetile_wall ( mazetile [z][y][x+1], Maze_2BIT_WEST);
      /*( ( mazetile [z][ y][x+1].solid
                  & Editor_POLYGON_INFO [Maze_2BIT_WEST] . mask )
                  >> Editor_POLYGON_INFO [Maze_2BIT_WEST] . shift );*/
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_EAST, walltype );

   }

   //west
   if ( is_mazetile_special( mazetile [z][y][x-1], MAZETILE_SPECIAL_SOLID) == true
       || x == 0 )
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_WEST, MAZETILE_SOLIDTYPE_WALL );
   else
   {
      walltype = get_mazetile_wall ( mazetile [z][y][x-1], Maze_2BIT_EAST);
      /*( ( mazetile [z][ y][x-1].solid
                  & Editor_POLYGON_INFO [Maze_2BIT_EAST] . mask )
                  >> Editor_POLYGON_INFO [Maze_2BIT_EAST] . shift );*/
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_WEST, walltype );

   }


}

void Editor::rect_to_sel ( void )
{
   bool xok;
   bool yok;
   int i;
   int j;

   //printf ("xmark=%d, ymark=%d, xcur=%d, ycur=%d\n", p_xmark, p_ymark, p_xcur, p_ycur);

   clear_selection();

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {
         xok = false;
         yok = false;

         if ( p_xmark < p_xcur )
         {
            if ( i >= p_xmark && i <= p_xcur)
               xok = true;
         }
         else
            if ( i >= p_xcur && i <= p_xmark)
               xok = true;

         if ( p_ymark < p_ycur )
         {
            if ( j >= p_ymark && j <= p_ycur)
               yok = true;
         }
         else
            if ( j >= p_ycur && j <= p_ymark)
               yok = true;

         if ( xok == true && yok == true)
         {
            p_selection [j][i] = true;
         }
      }
   }

}

void Editor::clear_selection ( void )
{
   int i;
   int j;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {
         //if ( is_tile_special ( p_zcur, j, i, MAZETILE_SPECIAL_SELECTION ))
         //{
            p_selection [j][i] = false;
         //}

      }
   }
}


void Editor::sel_build_room ( void )
{

   int i;
   int j;
   s_mazetile tmptile = new_mazetile_empty();
   //int k;


   set_mazetile_palette( &tmptile, p_active_palette );

   /*tmptile.solid = 0;
   tmptile.special = 0;
   tmptile.event = 0;
   tmptile.masked = 0;
   tmptile.maskposition = 0;
   tmptile.wobjpalette = p_active_palette;
   tmptile.objectpic = 0;
   tmptile.objposition = 0;*/

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true )
         {
            mazetile [ p_zcur][j][i] = tmptile;
         }
      }
   }

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true )
         {
            patch_surrounding_walls( p_zcur, j, i);
         }
      }
   }

}
void Editor::sel_filling ( int fill )
{
   int i;
   int j;
   unsigned char tmptile;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true && is_mazetile_special ( mazetile [p_zcur][j][i],
            MAZETILE_SPECIAL_SOLID) == false)
         {
            tmptile = (mazetile [ p_zcur][j][i].special & ~MAZETILE_FILLING_MASK);
            mazetile[p_zcur][j][i].special = tmptile + fill;
         }

      }
   }

}
void Editor::sel_special ( unsigned int special )
{
   int i;
   int j;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true && is_mazetile_special( mazetile[p_zcur][j][i],
            MAZETILE_SPECIAL_SOLID) == false)
         {
            set_mazetile_special ( &mazetile [p_zcur][j][i], special);
         }

      }
   }

}

void Editor::sel_remove_special ( void )
{
   int i;
   int j;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true && is_mazetile_special ( mazetile [p_zcur][j][i],
            MAZETILE_SPECIAL_SOLID) == false)
         {
            unset_mazetile_special( &mazetile [p_zcur][j][i], MAZETILE_SPECIAL_ALL );
            set_mazetile_filling( &mazetile [p_zcur][j][i], MAZETILE_FILLING_NONE );
         }

      }
   }

}


void Editor::sel_palette ( int palette )
{
   int i;
   int j;
   int tmpwobj;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true )
         {
            tmpwobj = (mazetile [ p_zcur][j][i].wobjpalette & ~MAZETILE_PALETTE_MASK );
            //extract wobj first
            mazetile [ p_zcur][j][i].wobjpalette = tmpwobj + palette;
         }

      }
   }
}
void Editor::sel_masked ( int texid, unsigned int location  )
{
   int i;
   int j;
   int backup_masked;
   //int backup_position;
 //  int mask;

   //printf ("\nEditor.sel_masked: Texture id = %d, location=%d\n", texid, location );

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true )
         {
    //        if ( location == Maze_TILE_MPOS_FLOOR )
      //         mask =

            backup_masked = (mazetile [p_zcur][j][i].masked & ~MAZETILE_MASKTEX_FLOOR_MASK );
            //printf ("Editor.sel_masked: before %d ", mazetile [p_zcur][j][i].masked);
            mazetile [p_zcur][j][i].maskposition =
               ( mazetile [p_zcur][j][i].maskposition | location );
            //extract wobj first
            mazetile [ p_zcur][j][i].masked =
               backup_masked + ( texid << 4 );
            //printf (": after %d \n", mazetile [p_zcur][j][i].masked);
         }

      }
   }




 //#define Maze_TILE_MPOS_FLOOR    0b10000000 // 128 // 01000000
 //#define Maze_TILE_MPOS_CEILING  0b01000000 // 64  // 10000000


//#define Maze_TILE_MASKED_FLOOR_MASK 0b11110000 //240 // 11110000
//#define Maze_TILE_MASKED_WALL_MASK  0b00001111 //15  // 00001111
//unsigned char masked; //* 4 bit Floor Ceiling texture + 4 bit wall texture
//unsigned char maskposition; //* 2 bit floor ceiling + 4 bit wall position + 2 bit grid tex

}
void Editor::sel_delete ( void )
{
   int i;
   int j;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true)
         {
            delete_tile (p_zcur, j, i );
         }

      }
   }

}

void Editor::delete_tile ( int z, int y, int x )
{
   //unsigned int tmpsolid;


   //printf ("passed through delete tile: z=%d, y=%d, x=%d\n", z, y, x );

   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {

      /*if ( is_mazetile_special ( mazetile [z][y][x], MAZETILE_SPECIAL_SOLID ) == false )
      {
         mazetile [z][y][x].special = MAZETILE_SPECIAL_SOLID;
      }*/

      set_mazetile_empty( &mazetile [z][y][x] );
      set_mazetile_special( &mazetile [z][y][x], MAZETILE_SPECIAL_SOLID);
      /*mazetile [z][y][x].solid = 0;
      mazetile [z][y][x].event = 0;
      mazetile [z][y][x].objectpic = 0;
      mazetile [z][y][x].wobjpalette = 0;
      mazetile [z][y][x].objposition = 0;
      mazetile [z][y][x].masked = 0;
      mazetile [z][y][x].maskposition = 0;*/

   // check for suroundings


   // north
   //if ( y < Maze_MAXWIDTH - 1)
      if ( is_mazetile_special ( mazetile [z][y+1][x], MAZETILE_SPECIAL_SOLID) == false )
      {
         set_mazetile_wall ( &mazetile [z][y+1][x], Maze_2BIT_SOUTH, MAZETILE_SOLIDTYPE_WALL);
         //tmpsolid = ( mazetile [p_zcur][p_ycur+1][p_xcur].solid & ~MAZETILE_SOLID_SOUTH_MASK );
         //mazetile [p_zcur][p_ycur+1][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_SOUTH_WALL);
      }
   // south
   //if ( y > 0)
      if ( is_mazetile_special ( mazetile [z][y-1][x], MAZETILE_SPECIAL_SOLID) == false )
      {
         set_mazetile_wall ( &mazetile [z][y-1][x], Maze_2BIT_NORTH, MAZETILE_SOLIDTYPE_WALL);
         //tmpsolid = ( mazetile [p_zcur][p_ycur-1][p_xcur].solid & ~MAZETILE_SOLID_NORTH_MASK );
         //mazetile [p_zcur][p_ycur-1][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_NORTH_WALL);
      }
   // east
   //if ( x < Maze_MAXWIDTH - 1)
      if ( is_mazetile_special ( mazetile [z][y][x+1], MAZETILE_SPECIAL_SOLID) == false )
      {
         set_mazetile_wall ( &mazetile [z][y][x+1], Maze_2BIT_WEST, MAZETILE_SOLIDTYPE_WALL);
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur+1].solid & ~MAZETILE_SOLID_WEST_MASK );
         //mazetile [p_zcur][p_ycur][p_xcur+1].solid = (tmpsolid | MAZETILE_SOLID_WEST_WALL);
      }
   // west
   //if ( x > 0 )
      if ( is_mazetile_special ( mazetile [z][y][x-1], MAZETILE_SPECIAL_SOLID) == false )
      {
         set_mazetile_wall ( &mazetile [z][y][x-1], Maze_2BIT_EAST, MAZETILE_SOLIDTYPE_WALL);
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur-1].solid & ~MAZETILE_SOLID_EAST_MASK );
         //mazetile [p_zcur][p_ycur][p_xcur-1].solid = (tmpsolid | MAZETILE_SOLID_EAST_WALL);
      }
   }
}

/*void Editor::set_wall ( int z, int y, int x, int direction)
{
   unsigned char tmpsolid;


   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {


   tmpsolid = ( mazetile [z][y][x].solid & ~Editor_POLYGON_INFO[direction].mask );
         mazetile [z][y][x].solid = (tmpsolid | Editor_POLYGON_INFO[direction].wall);
   }
}*/

/*void Editor::set_wall_type ( int z, int y, int x, int side, int type )
{
   unsigned char tmpsolid;

   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {
      tmpsolid = ( mazetile [z][y][x].solid & ~Editor_POLYGON_INFO[side].mask );

      mazetile [z][y][x].solid = (tmpsolid | ( type << Editor_POLYGON_INFO[side].shift) );
   }
}*/

/*void Editor::remove_wall ( int z, int y, int x, int direction)
{
   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {
      mazetile [z][y][x].solid = ( mazetile [z][y][x].solid & ~Editor_POLYGON_INFO[direction].mask );
   }
}*/

/*bool Editor::is_tile_special ( int z, int y, int x, unsigned int special )
{
    if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
    {
      if ( ( mazetile [ z][ y][ x].special & special ) == special )
         return ( true );
      else
         return ( false );
    }
    else
       return ( false );
}*/

/*bool Editor::is_tile_selected ( int z, int y, int x )
{

}*/

/*void Editor::remove_tile_special ( int z, int y, int x, unsigned int special )
{
   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {
      mazetile [z][y][x].special = ( mazetile [z][y][x].special & ~special );
   }
}

void Editor::set_tile_special ( int z, int y, int x, unsigned int special )
{
   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {


      mazetile [z][y][x].special = ( mazetile [z][y][x].special | special );
   }
}*/

void Editor::clear_maze ( void )
{
   int i,j,k;
   s_mazetile tile = new_mazetile_empty();

   set_mazetile_special( &tile, MAZETILE_SPECIAL_SOLID );

   // maybe remove, set only when loading maze
   for ( k = 0 ; k < Maze_MAXDEPTH; k++ )
      for ( j = 0 ; j < Maze_MAXWIDTH; j++ )
         for ( i = 0 ; i < Maze_MAXWIDTH; i++ )
            mazetile [ k ] [ i ] [ j ] = tile;

}

int Editor::get_palette ( int z, int y, int x )
{
   return ( mazetile [z][y][x].wobjpalette & MAZETILE_PALETTE_MASK );
}

void Editor::set_palette ( int z, int y, int x, int palette)
{
   int tmpobj = mazetile [z][y][x].wobjpalette & ~MAZETILE_PALETTE_MASK;

   mazetile [z][y][x].wobjpalette = tmpobj + palette;

}

/*
void Editor::load_maz_file ( char *filename )
{
   int x;
   int y;
   int z;
   int dummy;
   s_mazetile tmptile;
   fs_maz_header mazheader;
   FILE* maz_file;

   //tmptile.walltex = 0;
   //tmptile.floortex = 0;
   //tmptile.objectimg = 0;
   tmptile.event = 0;
   tmptile.special = 0;
   //tmptile.texture = 0;
   tmptile.solid = 0;
   tmptile.objectpic = 0; // * 4 bit Floor Object, 4 bit ceiling object from palette
   tmptile.wobjpalette = 0; // * 3 bit wall object + 5 bit palette ID
   tmptile.objposition = 0; // * 2 bit wall + 2 bit ceiling + 4 bit floor
   tmptile.masked = 0; // * 4 bit Floor Ceiling texture + 4 bit wall texture
   tmptile.maskposition = 0; //

   // clean current maze
   for ( z = 0 ; z < Maze_MAXDEPTH; z++ )
       for ( y = 0 ; y < Maze_MAXWIDTH; y++ )
       {
         for ( x = 0 ; x < Maze_MAXWIDTH; x++ )
         {
            mazetile [ z ] [ y ] [ x ] = tmptile;
         }
       }

   if ( exists ( filename ) != 0 )
   {
      maz_file = fopen ( filename, "rb" );

      if ( maz_file != NULL )
      {
         dummy = fread ( &mazheader, sizeof ( fs_maz_header ), 1, maz_file );

         p_width = mazheader.f_width;
         p_depth = mazheader.f_depth;
         p_sky = mazheader.f_sky;
         p_rclevel = mazheader.f_rclevel;
         p_startpos.x = mazheader.f_startx;
         p_startpos.y = mazheader.f_starty;
         p_startpos.z = mazheader.f_startz;
         p_startpos.facing = mazheader.f_startface;
   //      mazheader.f_reserve;

         for ( z = 0 ; z < p_depth ; z++ )
            for ( y = 0 ; y < p_width ; y++ )
               for ( x = 0 ; x < p_width ; x++ )
               {
                  dummy = fread ( &tmptile, sizeof ( s_mazetile ), 1, maz_file );
                  mazetile [ z ] [ y ] [ x ] = tmptile;
               }
         fclose ( maz_file );
         textout_old ( subbuffer, font, "Passed corectly", 10, 10, General_COLOR_TEXT );
      }
      else
         textout_old ( subbuffer, font, "fopen error", 10, 10, General_COLOR_TEXT );
   }
   else
      textout_old ( subbuffer, font, "exist error", 10, 10, General_COLOR_TEXT );

   copy_buffer();
   while ( mainloop_readkeyboard() != KEY_ENTER );
}

void Editor::save_maz_file ( char *filename )
{
   fs_maz_header mazheader;
   s_mazetile tmptile;
   FILE* maz_file;
   int x;
   int y;
   int z;

   mazheader.f_width = p_width;
   mazheader.f_depth = p_depth;
   mazheader.f_sky = p_sky;
   mazheader.f_rclevel = p_rclevel;
   mazheader.f_startx = p_startpos.x;
   mazheader.f_starty = p_startpos.y;
   mazheader.f_startz = p_startpos.z;
   mazheader.f_startface = p_startpos.facing;
   mazheader.f_reserved = 0;

   maz_file = fopen ( filename, "wb" );

   if ( maz_file != NULL )
   {

      fwrite ( &mazheader, sizeof ( fs_maz_header ), 1, maz_file );

      for ( z = 0 ; z < p_depth ; z++ )
         for ( y = 0 ; y < p_width ; y++ )
            for ( x = 0 ; x < p_width ; x++ )
            {
               tmptile = mazetile [ z ] [ y ] [ x ];
               fwrite ( &tmptile, sizeof ( s_mazetile ), 1, maz_file );
            }
      fclose ( maz_file );
   }

}

*/
/*-------------------------------------------------------------------------*/
/*-                           Private Methods                             -*/
/*-------------------------------------------------------------------------*/

/*void Editor::copy_background ( void )
{
   blit ( backup, buffer, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   clear_bitmap (backup);
}*/

void Editor::draw_map ( void )
{
   short x;
   short y;
   int markx;
   int marky;
   int i;
   int j;
   //int r;
   //short basex;
   short basey;
   //s_mazetile tmptile;
   //unsigned char solid;
   //s_mazetile tile;
   bool draw_selection;
   bool xok = false;
   bool yok = false;
   //int tmpfilling;
   Event tmpevent;
   int error;

   // draw numbers

   //basex = 0;
   basey = 416;
   y = 0;

   for ( i = p_yscroll ; i < p_yscroll + 26 ; i++ )
   {
      textprintf_old ( editorbuffer, FNT_small, 0, basey - y, General_COLOR_TEXT, "%d", i );
      y = y + 16;
   }

   basey = 436;
   x = 18;

   for ( i = p_xscroll ; i < p_xscroll + 26 ; i++ )
   {
      textprintf_old ( editorbuffer, FNT_small, x, basey, General_COLOR_TEXT, "%d", i );
      x = x + 16;
   }

   // draw floor level

   rectfill ( editorbuffer, 0, 436, 16, 451, makecol ( 200, 200, 200 ) );

   /*old code to be replaced with internal number for simplicity
   if ( p_zcur == p_rclevel )
      textprintf_old ( editorbuffer, FNT_small, 0, 436, makecol ( 0, 0, 0 ), "RC" );

   if ( p_zcur < p_rclevel )
      textprintf_old ( editorbuffer, FNT_small, 0, 436, makecol ( 0, 0, 0 ),
         "F%d", ( p_zcur - p_rclevel) );

   if ( p_zcur > p_rclevel )
      textprintf_old ( editorbuffer, FNT_small, 0, 436, makecol ( 0, 0, 0 ),
         "B%d", abs (  p_rclevel - p_zcur  ) );*/

   textprintf_old ( editorbuffer, FNT_small, 0, 436, makecol ( 0, 0, 0 ),
         "%d", p_zcur );

   // draw grid ?? not sure what it does, maybe draw 4 corners here
   //drawing_mode(DRAW_MODE_MASKED_PATTERN, BMP_patgrid, 0, 0 );
   for ( x = 16 ; x < 432; x = x + 16 )
      for ( y = 16 ; y < 432 ; y = y + 16 )
         draw_sprite ( editorbuffer, datref_editoricon [ EDICON_TILE_EMPTY ], x, y );

      //hline ( editorbuffer, 16, y, 432, makecol ( 150, 150, 150 ) );
      //vline ( editorbuffer, x, 16, 432, makecol ( 150, 150, 150 ) );

   //drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0 );

   // ----- main drawing loop -----

   x = 16;
   y = 416;

   for ( j = p_yscroll ; j < p_yscroll + 26 ; j++ )
   {
      for ( i = p_xscroll ; i < p_xscroll + 26 ; i++ )
      {
         // ----- draw special -----
         if ( is_mazetile_special ( mazetile [p_zcur][j][i], MAZETILE_SPECIAL_SOLID) == true )
               draw_sprite ( editorbuffer, datref_editoricon [ EDICON_TILE_SOLID], x, y );

         if ( is_mazetile_special ( mazetile [p_zcur][j][i], MAZETILE_SPECIAL_MAGIKBOUNCE) == true )
            draw_sprite ( editorbuffer, datref_editoricon [EDICON_TILE_MBOUNCE], x, y );


         // ----- Draw Walls -----

         Editor_draw_space_wall( mazetile [ p_zcur ] [ j ] [ i ], y, x );

         //Check each side for a wall, door, or grid


         //if ( ( solid & MAZETILE_SOLID_NORTH_MASK ) == MAZETILE_SOLID_NORTH_WALL )
         //   rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_WALL], x, y, 0<<16 );

         //if ( ( solid & MAZETILE_SOLID_EAST_MASK ) == MAZETILE_SOLID_EAST_WALL )
         //   rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_WALL], x, y, 64<<16 );

         //if ( ( solid & MAZETILE_SOLID_SOUTH_MASK ) == MAZETILE_SOLID_SOUTH_WALL )
         //   rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_WALL], x, y, 128<<16 );

         //if ( ( solid & MAZETILE_SOLID_WEST_MASK ) == MAZETILE_SOLID_WEST_WALL )
         //   rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_WALL], x, y, 192<<16 );

         /*
         if ( ( solid & MAZETILE_SOLID_NORTH_MASK ) == MAZETILE_SOLID_NORTH_DOOR )
            rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_DOOR], x, y, 0<<16 );
         if ( ( solid & MAZETILE_SOLID_EAST_MASK ) == MAZETILE_SOLID_EAST_DOOR )
            rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_DOOR], x, y, 64<<16 );
         if ( ( solid & MAZETILE_SOLID_SOUTH_MASK ) == MAZETILE_SOLID_SOUTH_DOOR )
            rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_DOOR], x, y, 128<<16 );
         if ( ( solid & MAZETILE_SOLID_WEST_MASK ) == MAZETILE_SOLID_WEST_DOOR )
            rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_DOOR], x, y, 192<<16 );

         if ( ( solid & MAZETILE_SOLID_NORTH_MASK ) == MAZETILE_SOLID_NORTH_GRID )
            rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_GRID], x, y, 0<<16 );
         if ( ( solid & MAZETILE_SOLID_EAST_MASK ) == MAZETILE_SOLID_EAST_GRID )
            rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_GRID], x, y, 64<<16 );
         if ( ( solid & MAZETILE_SOLID_SOUTH_MASK ) == MAZETILE_SOLID_SOUTH_GRID )
            rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_GRID], x, y, 128<<16 );
         if ( ( solid & MAZETILE_SOLID_WEST_MASK ) == MAZETILE_SOLID_WEST_GRID )
            rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_GRID], x, y, 192<<16 );
         */

         // draw light

         if ( is_mazetile_special ( mazetile [p_zcur][j][i], MAZETILE_SPECIAL_LIGHT) == true )
         {
            //set_trans_blender(0, 0, 0, 200);
            //drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );
            //rectfill ( editorbuffer, x, y, x+15, y+15, makecol ( 255, 255, 255) );
            draw_sprite ( editorbuffer, datref_editoricon [EDICON_TILE_LIGHT], x, y );
            //set_trans_blender(0, 0, 0, 255);
         }

         // Draw Event Icon

         if ( mazetile [p_zcur][j][i].event > 0 )
         {
            error = tmpevent.SQLselect ( mazetile [p_zcur][j][i].event );

            if ( error == SQLITE_ROW )
            {
               if ( tmpevent.pass_event() > 0 )
                  draw_sprite ( editorbuffer,
                     datref_editoricon [ EDICON_EVENT_PASSFAIL [ tmpevent.pass_event() ] ], x, y );
               else
                  draw_sprite ( editorbuffer,
                     datref_editoricon [ EDICON_EVENT_LOCK [ tmpevent.lock_event() ] ], x, y );

            }
            else
               draw_sprite ( editorbuffer,
                  datref_editoricon [ EDICON_EVENT_PASSFAIL [ 0 ] ], x, y );

         }

         // draw filling
         Editor_draw_space_filling( mazetile [p_zcur][j][i], y, x );

         /*tmpfilling = mazetile [p_zcur][j][i].special & MAZETILE_FILLING_MASK;
         if ( tmpfilling > 0 )
         {
            set_trans_blender(0, 0, 0, 100);
            drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );

            if (Maze_SWALL_INFO[ tmpfilling].drawmode == DRAW_MODE_TRANS)
            {
               rectfill ( editorbuffer, x, y, x+15, y+15,
                      makecol ( Maze_SWALL_INFO [ tmpfilling ].red,
                         Maze_SWALL_INFO [ tmpfilling ].green,
                         Maze_SWALL_INFO [ tmpfilling ].blue ) );

            }
            else
            {
               if ( tmpfilling == MAZETILE_FILLING_DARKNESS )
                  draw_trans_sprite ( editorbuffer, datref_editoricon [EDICON_TILE_LIGHT], x, y );
            }

            drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
            set_trans_blender(0, 0, 0, 255);
         }*/

         // draw selection if active

         draw_selection = false;
         xok = false;
         yok = false;


         if ( p_selection_active == true)
         {
            switch ( p_tool)
            {

               case Editor_TOOL_RECTANGLE :
                  if ( p_xmark < p_xcur )
                  {
                     if ( i >= p_xmark && i <= p_xcur)
                        xok = true;
                  }
                  else
                     if ( i >= p_xcur && i <= p_xmark)
                        xok = true;

                  if ( p_ymark < p_ycur )
                  {
                     if ( j >= p_ymark && j <= p_ycur)
                        yok = true;
                  }
                  else
                     if ( j >= p_ycur && j <= p_ymark)
                        yok = true;

                  if ( xok == true && yok == true)
                     draw_selection = true;

               break;

            }
         }

         if ( draw_selection == true || p_selection [j][i] == true )
         {
            // display tile  EDICON_MARKER_PAINT

            set_trans_blender(0, 0, 0, 100);
            drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );
            draw_trans_sprite ( editorbuffer, datref_editoricon [EDICON_MARKER_PAINT], x, y );
            set_trans_blender(0, 0, 0, 255);
            drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
         }

         x = x + 16;
      }
      x = 16;
      y = y - 16;
   }

   // draw start position

   if  ( p_startpos.z == p_zcur )
   {
      if ( p_startpos.x >= p_xscroll && p_startpos.x <= ( p_xscroll + 26 ) )
         if ( p_startpos.y >= p_yscroll && p_startpos.y <= ( p_yscroll + 26 ) )
         {
            x = ( ( p_startpos.x - p_xscroll ) * 16 ) + 16;
            y = 416 - ( ( p_startpos.y - p_yscroll ) * 16 );
            draw_sprite ( editorbuffer, datref_editoricon [ EDICON_SPECIAL_START ], x, y );
         }
   }

   // draw walls ( use tiles instead ) code moved

   /*x = 16;
   y = 416;

   for ( j = p_yscroll ; j < p_yscroll + 26 ; j++ )
   {
      for ( i = p_xscroll ; i < p_xscroll + 26 ; i++ )
      {
//         if ( i < p_width && j < p_width )
//         {

  //       }
         x = x + 16;

      }
      x = 16;
      y = y - 16;
   }*/

   // instruction

   switch ( p_tool)
   {
      case Editor_TOOL_TILE:
         textout_old ( editorbuffer, FNT_small, "TOOL: TILE: [ASDW] change wall solidity [Bracket]Change Palette"
            , 0, 466, General_COLOR_TEXT );
      break;
      case Editor_TOOL_CORRIDOR:
         textout_old ( editorbuffer, FNT_small, "TOOL: CORRIDOR: [ASDW] Dig in the indicated direction"
            , 0, 466, General_COLOR_TEXT );
      break;
      case Editor_TOOL_RECTANGLE:
         textout_old ( editorbuffer, FNT_small, "TOOL: RECTANGLE: [Z] Start/End Rectangle [X] Clear Selection -> [R] Edit Selected"
            , 0, 466, General_COLOR_TEXT );
      break;
      case Editor_TOOL_BRUSH:
         textout_old ( editorbuffer, FNT_small, "TOOL: BRUSH: [Z] Start/End Selection [ASDW] Expand Selection [X] Clear Selection -> [R] Edit Selected"
            , 0, 466, General_COLOR_TEXT );
      break;
   }

   textout_old ( editorbuffer, FNT_small, "F1-FILE     F2-TOOL     F3-TEXTURE  F4-VIEW     F5-LAYER    F6-ERROR    F7-DATA     F8-EVENT"
            , 0, 0, General_COLOR_TEXT );

/*public: void menu_file ( void );
   public: void menu_tool ( void );
   public: void menu_texture ( void );
   public: void menu_view ( void );
   public: void menu_layer ( void );
   public: void menu_error ( void );
   public: void menu_data ( void );
*/

   // draw cursor

   x = ( ( p_xcur - p_xscroll ) * 16 ) + 16;
   y = 416 - ( ( p_ycur - p_yscroll ) * 16 );
   markx = ( ( p_xmark - p_xscroll ) * 16 ) + 16;
   marky = 416 - ( ( p_ymark - p_yscroll ) * 16 );

   switch ( p_tool )
   {
      case Editor_TOOL_TILE:
         draw_sprite ( editorbuffer, datref_editoricon [ EDICON_CURSOR_SQUARE], x, y );
      break;
      case Editor_TOOL_CORRIDOR:
         draw_sprite ( editorbuffer, datref_editoricon [ EDICON_CURSOR_ARROW], x, y );
      break;
      case Editor_TOOL_RECTANGLE:
         draw_sprite ( editorbuffer, datref_editoricon [ EDICON_CURSOR_ANGLE], x, y );
         if ( p_selection_active == true)
            draw_sprite ( editorbuffer, datref_editoricon [ EDICON_MARKER_FLAG], markx, marky );
      break;
      case Editor_TOOL_BRUSH:
         draw_sprite ( editorbuffer, datref_editoricon [ EDICON_CURSOR_BRUSH], x, y );
      break;
   }


   // todo, change cursor according to mode



}

//TODO refactor this method and the way the preview is drawn on screen considering rotations
//will eventually be handled.
void Editor::draw_detailed_tile ( void )
{
   int i;
   unsigned char mask = 0;
   short x = 0;
   short y = 0;
   s_mazetile tile = mazetile [p_zcur][p_ycur][p_xcur];
   int texID = 0;
   int tmpsolid;
   int polytype;
   int tmpolytype;
   int tmpfill;

   //TODO: add texture masking on walls and floors
   //

   // 6 polygons to be drawn and the information stored in 2 different structures
   // Editor_POLYGON_INFO and Editor_room
   set_trans_blender ( 0, 0, 0, EDITOR_TRANSWALL_BLEND );

   for ( i = 0; i < 6; i++)
   {
      texID = -1; // skip drawing if remains at -1, there is no wall there.
      //printf ("step 1\n");
      if ( Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_WALL
         || Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL)
      {
         //printf ("step 2\n");
         tmpsolid = ( tile.solid & Editor_POLYGON_INFO[i].mask );


         if ( tmpsolid > 0)
         {
          //  printf ("step 4\n");
            tmpsolid = tmpsolid >> Editor_POLYGON_INFO[i].shift;

            // set polytype transparent for the front wall that will hide the rest of the room
            polytype = Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL
               ? POLYTYPE_ATEX_TRANS : POLYTYPE_ATEX;

        //    printf ("step 5\n");
            switch (tmpsolid )
            {

               case MAZETILE_SOLID_WEST_WALL:
                  texID = TexPal_IDX_WALL;
               break;
               case MAZETILE_SOLID_WEST_GRID:
                  texID = TexPal_IDX_GRID;
                  texID += ( mazetile [p_zcur][p_ycur][p_xcur].maskposition
                     & MAZETILE_MASKPOS_GRID_MASK );
                  polytype = Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL
                     ? POLYTYPE_ATEX_MASK_TRANS : POLYTYPE_ATEX_MASK;
               break;
               case MAZETILE_SOLID_WEST_DOOR:
                  texID = TexPal_IDX_DOOR;
                  polytype = Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL
                     ? POLYTYPE_ATEX_MASK_TRANS : POLYTYPE_ATEX_MASK;
                  tmpolytype = Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL
                     ? POLYTYPE_ATEX_TRANS : POLYTYPE_ATEX;
                  // Draw wall first, then the door
                  if ( Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL )
                  {
   //                  set_trans_blender ( 0, 0, 0, EDITOR_TRANSWALL_BLEND );
                  }
                  quad3d ( editorbuffer, tmpolytype,
                     mazepal.bmp [TexPal_IDX_WALL],
                     &Editor_room [i][ 0 ],
                     &Editor_room [i][ 1 ],
                     &Editor_room [i][ 2 ],
                     &Editor_room [i][ 3 ] );
               break;

            }


         }
      }
      else // it's a floor or ceiling
      {
         polytype = POLYTYPE_ATEX;
         texID =  Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_FLOOR
            ? TexPal_IDX_FLOOR : TexPal_IDX_CEILING;

      }
      //printf ("step 6\n");
      if ( texID != -1)
      {
         //printf ("Drawing poly=%d, type=%d, texID=%d\n", i, polytype, texID );

         if ( Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL )
         {
            //set_trans_blender ( 0, 0, 0, EDITOR_TRANSWALL_BLEND );
         }

         quad3d ( editorbuffer, polytype,
                 mazepal.bmp [texID],
                 &Editor_room [i][ 0 ],
                 &Editor_room [i][ 1 ],
                 &Editor_room [i][ 2 ],
                 &Editor_room [i][ 3 ] );

         //restore Opacity.
         //set_trans_blender ( 0, 0, 0, 255 );

         //printf ("step 7\n");
         //printf ("Drawn polygon %d\n", i);
      }

   }
   set_trans_blender ( 0, 0, 0, 255 );

   /*quad3d ( editorbuffer, POLYTYPE_ATEX,
              mazepal.bmp [TexPal_IDX_CEILING],
              &Editor_room [4][ 0 ],
              &Editor_room [4][ 1 ],
              &Editor_room [4][ 2 ],
              &Editor_room [4][ 3 ] );

   quad3d ( editorbuffer, POLYTYPE_ATEX,
              mazepal.bmp [TexPal_IDX_FLOOR],
              &Editor_room [5][ 0 ],
              &Editor_room [5][ 1 ],
              &Editor_room [5][ 2 ],
              &Editor_room [5][ 3 ] );*/

   textprintf_old ( editorbuffer, FNT_small, 440, 188, General_COLOR_TEXT,
      "Palette" );

   textprintf_old ( editorbuffer, FNT_small, 440, 204, General_COLOR_TEXT,
      "ID (%2d)", ( tile.wobjpalette & MAZETILE_PALETTE_MASK) );


   // todo: display textures instead. Not sure since need to prebuild them each time
   // else need palete management change in the editor
   textprintf_old ( editorbuffer, FNT_small, 440, 276, General_COLOR_TEXT,
      "Active Pal(%2d):Wall-Floor-Ceiling", p_active_palette );

// note: to change with active palette textures
   stretch_sprite ( editorbuffer, editorpal.bmp[TexPal_IDX_WALL], 440, 292, 66, 66 );
   //stretch_sprite ( editorbuffer, texpal.tex[TexPal_IDX_WALL], 490, 292, 50, 50 );
   //stretch_sprite ( editorbuffer, texpal.tex[TexPal_IDX_DOOR], 490, 292, 50, 50 );
   stretch_sprite ( editorbuffer, editorpal.bmp[TexPal_IDX_FLOOR], 504, 292, 66, 66 );
   stretch_sprite ( editorbuffer, editorpal.bmp[TexPal_IDX_CEILING], 570, 292, 66, 66 );

   if ( tile.event > 0 )
   {
      Event tmpevent;
      int error;

      error = tmpevent.SQLselect ( tile.event);

      if ( error == SQLITE_ROW)
      {
         textprintf_old ( editorbuffer, FNT_small, 440, 396, General_COLOR_TEXT,
            "EVENT %d: WHEN %s OCCURS", tmpevent.primary_key(), STR_EVE_TRIGGER [ tmpevent.trigger()] );
         textprintf_old ( editorbuffer, FNT_small, 440, 412, General_COLOR_TEXT,
            "IF %s PASS", STR_EVE_LOCK [ tmpevent.lock_event()] );
         textprintf_old ( editorbuffer, FNT_small, 440, 428, General_COLOR_TEXT,
            "THEN %s", STR_EVE_PASSFAIL [ tmpevent.pass_event()] );
         textprintf_old ( editorbuffer, FNT_small, 440, 444, General_COLOR_TEXT,
            "ELSE %s", STR_EVE_PASSFAIL [ tmpevent.fail_event()] );
      }
      else
         textprintf_old ( editorbuffer, FNT_small, 440, 396, General_COLOR_TEXT,
         "Could not read event from the Database" );
   }
   else
      textprintf_old ( editorbuffer, FNT_small, 440, 396, General_COLOR_TEXT,
         "There is no event on this tile" );


   /*// display textures instead in small
   textprintf_old ( editorbuffer, FNT_small, 440, 396, General_COLOR_TEXT,
      "Wall Masked tex ID :  %d", p_curtile.walltex );
   textprintf_old ( editorbuffer, FNT_small, 440, 412, General_COLOR_TEXT,
      "Floor Masked tex ID : %d", p_curtile.floortex );
   textprintf_old ( editorbuffer, FNT_small, 440, 428, General_COLOR_TEXT,
      "Object image ID :     %d", p_curtile.objectimg );*/

   y = 380;
   x = 440;
   mask = MAZETILE_SPECIAL_MAGIKBOUNCE;
   for ( i = 0 ; i < 4; i++)
   {
      if ( ( tile.special & mask ) > 0 )
      {
         textout_old (editorbuffer, FNT_small, STR_EDT_SPECIAL_TECH [i], x, y,
         General_COLOR_TEXT );
         x += 42;
      }
      mask = mask << 1;
   }

   tmpfill = tile.special & MAZETILE_FILLING_MASK;
   if ( tmpfill > 0 )
   {
      textout_old (editorbuffer, FNT_small, STR_EDT_SPECIAL_FILL [tmpfill], x, y,
         General_COLOR_TEXT );
   }


}

void Editor::show_full_map ( void )
{
   int x;
   int y;
   int i; // pixel coordinates
   int j; // pixel coordinates
   int palette;
   //bool solid = true;
   int key;
   int exit = false;
   int tmpcolor;

   while (exit == false)
   {
      clear (editorbuffer);
      drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
      rect ( editorbuffer , 15, 15, 416, 416, makecol (255, 255, 255));
      i=16;
      j=412;
      textprintf_old ( editorbuffer, FNT_small, 0, 417, makecol ( 255, 255, 255 ),
         "Level=%d | Active Palette = %d", p_zcur, p_active_palette );

      for ( x = 0 ; x < Maze_MAXWIDTH; x++ )
      {
         for ( y = 0 ; y < Maze_MAXWIDTH ; y++ )
         {
            palette = get_palette( p_zcur, y, x);

       /*     if ( ( mazetile [p_zcur][p_ycur][p_xcur].special & MAZETILE_SPECIAL_SOLID ) > 0 )
               solid = true;
            else
               solid = false;*/

            if ( is_mazetile_special ( mazetile [p_zcur][y][x], MAZETILE_SPECIAL_SOLID ) == false)
            {
               //printf ("Editor: Draw something at %d, %d\n", i, j );
               if ( Editor_texpalette_color [palette].texture != -1 )
               {
                  drawing_mode ( DRAW_MODE_MASKED_PATTERN, datref_editoricon [ Editor_texpalette_color [palette].texture ], 0, 0);
               }
               else
               {
                  drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
               }
               tmpcolor = Editor_texpalette_color [palette].color;
               if ( palette == p_active_palette )
                  tmpcolor = 255;
               //printf ("Makecol=%d, tablecol=%d", makecol (255, 255, 255), Editor_texpalette_color [31].color );
               rectfill ( editorbuffer, i, j, i+3, j+3, makecol (tmpcolor, tmpcolor, tmpcolor)  );
            }

            if ( mazetile [p_zcur][y][x].event > 0)
              rectfill ( editorbuffer, i+1, j+1, i+2, j+2, makecol (128, 128, 255) );

            j = j - 4;
         }

         i = i + 4;
         j = 412;
      }

      textprintf_old ( editorbuffer, FNT_small, 0, 460, General_COLOR_TEXT,
         "[ESC]Exit the mode [Bracers]Change active palette [PgUp/Dn]Change Floor"  );

      blit_editorbuffer();



      copy_buffer();

      //tmpcode
//      make_screen_shot( "FullMap.bmp");

      key = mainloop_readkeyboard();
      clear_keybuf();

      switch ( key )
      {
         case KEY_ESC:
            exit = true;
         break;
         case KEY_PGUP:
            if ( p_zcur > 0)
               p_zcur--;
         break;
         case KEY_PGDN:
            if ( p_zcur < Maze_MAXDEPTH -1)
               p_zcur++;
         break;
         case KEY_OPENBRACE:
            if ( p_active_palette > 0 )
               p_active_palette--;
         break;
         case KEY_CLOSEBRACE:
            if ( p_active_palette < Maze_NB_TEXPALETTE -1 )
               p_active_palette++;
         break;
      }
   }

}


void Editor::select_adventure ( void )
{

   List lst_file ("Select adventure directory to edit ( adventure// )", 20 );
   //char tmpstr [ Manager_TMP_STR_LEN ];
   int i;
   int answer;
   //short answer2;
   int retval;
   char filelist [ Manager_MAX_NB_FILE ] [ Manager_FILENAME_LEN ];
   int flistindex;
//   string tmpfilename;
   //char strfname [ 25 ];
   al_ffblk info;
   //char tmpsavename [Manager_FILENAME_LEN];
   //int savenumber = 0;
   int fileID;
   //struct tm *timeptr;
   //time_t loctime;
   //char tmpstr_database [ Manager_TMP_STR_LEN + 20];
   //char tmpstr_maze [ Manager_TMP_STR_LEN + 20];
   int error;
   int success;

   retval = al_findfirst("adventure//*", &info, FA_ALL /*33*/ );

   flistindex = 0;
   if ( retval == 0 )
   {
      if ((info.attrib & FA_DIREC) == FA_DIREC && info.name[0] != '.')
      {
         strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
         flistindex++;
      }
      while ( al_findnext ( &info ) == 0 && flistindex < Manager_MAX_NB_FILE)
      {
         if ((info.attrib & FA_DIREC) == FA_DIREC && info.name[0] != '.')
         {
            strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
            flistindex++;
         }
      }
      al_findclose( &info);
   }

   if ( flistindex > 0)
   {
      for ( i = 0 ; i < flistindex ; i++ )
      {
         lst_file.add_item ( i, filelist [ i ] );
      }

      WinList wlst_file ( lst_file, 20, 40 );
      answer = Window::show_all();
      wlst_file.hide();

      if ( answer != -1 )
      {

         fileID = answer;
         sprintf (p_adventure_databasefile, "adventure//%s//database.sqlite", filelist [ fileID ] );
         sprintf (p_adventure_mazefile, "adventure//%s//maze.bin", filelist [ fileID ] );

         clear_maze();
         success = maze.load_from_mazefile ( p_adventure_mazefile);

         if ( success == true)
         {
            SQLclose();
            error = SQLopen (p_adventure_databasefile);

            if ( error == SQLITE_OK)
            {

               p_zcur = 0;
               p_xcur = 0;
               p_ycur = 0;
               p_xscroll = 0;
               p_yscroll = 0;
               mazepalid = ( mazetile [p_zcur][p_ycur][p_xcur].wobjpalette & MAZETILE_PALETTE_MASK );
               mazepal.load ( mazepalid );
               mazepal.build();
               editorpal.load ( 0 );
               editorpal.build();
               palsample.load_build();


               p_adventure_selected = true;
            }
            else
            {
               WinMessage wmsg_error ( "Error loading database.sqlite\n" );
               Window::show_all();
               clear_maze();
               p_adventure_selected = false;
            }

         }
         else
            {
               WinMessage wmsg_error ( "Error loading maze.bin" );
               Window::show_all();
            }
         /*Window::draw_all();
         textprintf_centre_old ( subbuffer, FNT_print, 320, 200, General_COLOR_TEXT,
            "Reading %s", filelist [ fileID ] );
         textout_centre_old ( subbuffer, FNT_print, "Please Wait", 320, 220, General_COLOR_TEXT );
         copy_buffer();*/

         //tmpobj = load_datafile ( filelist [ answer ] );
         // start loading DATAFILE
         /*sprintf ( strfname,"adventur//%s", filelist [ answer ] );
         tmpobj = load_datafile_object( strfname , "DBA_ADATABASE" );*/

         //fake dbloading
         //retval = SQLopen ("wizardry.sqlite");



      }

   }

}

void Editor::select_event ( void )
{
   //??to do: select event from the list and set mazetile[p_zcur][y][x] to event ID
   // do not select event > 255. 0 = no event.
   // events are absolute, there is not a different list for each palete
   // but even can also be used in diffent place. Ex a stair up can be used for all start up events.
   int error;
   int answer;
   List lst_event ("Select the event you want ot trigger?", 20 );
   Event tmpevent;
   int showlist = true;

   error = tmpevent.SQLprepare ("WHERE pk >= 1 AND pk <= 255");

   if ( error == SQLITE_OK)
   {
      error = tmpevent.SQLstep();

      while ( error == SQLITE_ROW )
      {
         lst_event.add_itemf (tmpevent.primary_key(), "%3d-WHEN %s IF %s PASS, THEN %s ELSE %s",
            tmpevent.primary_key(),
            STR_EVE_TRIGGER [ tmpevent.trigger()],
            STR_EVE_LOCK [ tmpevent.lock_event() ],
            STR_EVE_PASSFAIL [ tmpevent.pass_event() ],
            STR_EVE_PASSFAIL [ tmpevent.fail_event() ] );

         //printf ("debug: select event: passed in loop\n");

         error = tmpevent.SQLstep();
      }

      tmpevent.SQLfinalize();
   }
   else
   {
      WinMessage wmsg_error ("There was an error querying the event list.\nMaybe there are no events in the database.\nMake sure their PK is between 1 and 255");
      Window::show_all();
      showlist = false;
   }

   if ( showlist == true)
   {

      WinList wlst_event ( lst_event, 0, 16 );
      blit_editorbuffer();
      answer = Window::show_all();

      if ( answer != -1)
      {
         mazetile [p_zcur][p_ycur][p_xcur].event = answer;
      }
   }

   /* retval = tmpitem.SQLpreparef( "WHERE loctype=%d A2ND lockey=%d AND type=%d", Item_LOCATION_CHARACTER, primary_key(), Item_TYPE_WEAPON );
//printf ("pass 4-1\r\n");
         if ( retval == SQLITE_OK)
         {
            retval = tmpitem.SQLstep();

            while ( retval == SQLITE_ROW)
            {
               if ( tmpitem.range() > max_range)
                  max_range = tmpitem.range();

               retval = tmpitem.SQLstep();
            }*/

}

int  Editor::select_palette ( void )
{
   int i;
   int selpalette = 0;
   //bool selected = false;
   //int answer;
   List lst_texture ("Select the palette?", 20, true );
   WinData<BITMAP> wdat_texture ( WDatProc_texture_bitmap, *(palsample.bmp[0]),
                                 WDatProc_POSITION_TEXTURE  );

   for ( i = 0; i < TexSample_NB_TEXPALETTE; i++)
   {
      lst_texture.add_itemf ( i,"Palette %d", i);
   }

   WinList wlst_texture ( lst_texture, 20, 40 );

   while ( lst_texture.selected() == false && selpalette != -1 )
   {
      blit_editorbuffer();
      selpalette = Window::show_all();

      if ( selpalette != -1 )
      {
         wdat_texture.parameter ( *(palsample.bmp [ selpalette ]) );
         Window::refresh_all();
      }

   }

   return (selpalette);
}

 int Editor::select_active_palette_texture ( void )
 {
       int i;
   int seltexture = -1;
   //bool selected = false;
   //int answer;
   List lst_texture ("Select the texture?", 20, true );
   WinData<BITMAP> wdat_texture ( WDatProc_texture_bitmap, *(editorpal.bmp[0]),
                                 WDatProc_POSITION_TEXTURE  );

   /*printf ("Active Palette = %d\n", p_active_palette);
   // test code
   if ( p_active_palette == 2 )
   {
      editorpal.load (2);

      draw_sprite (buffer, editorpal.bmp [0], 0, 0 );
      copy_buffer();
   }*/

   for ( i = 0; i < TexPalette_NB_TEXTURE; i++)
   {
      lst_texture.add_item ( i,STR_TEX_PALETTE [i]);
   }

   WinList wlst_texture ( lst_texture, 20, 40 );

   do // to avoid a warning, I changed a while as a do while to allow seltexture of value -1
      // to enter the block.
   {
      blit_editorbuffer();
      seltexture = Window::show_all();

      if ( seltexture != -1 )
      {
         wdat_texture.parameter ( *(editorpal.bmp [ seltexture ]) );
         Window::refresh_all();
      }

   }
   while ( lst_texture.selected() == false && seltexture != -1 );

   return (seltexture);
 }


/*void Editor::read_curtile ( void )
{
   s_mazetile mtile = mazetile [p_zcur][p_ycur][p_xcur];

   p_curtile.solid [0] = (mtile.solid & MAZETILE_SOLID_NORTH_MASK) >> 6;
   p_curtile.solid [1] = (mtile.solid & MAZETILE_SOLID_EAST_MASK) >> 4;
   p_curtile.solid [2] = (mtile.solid & MAZETILE_SOLID_SOUTH_MASK) >> 2;
   p_curtile.solid [3] = (mtile.solid & MAZETILE_SOLID_WEST_MASK);

   p_curtile.special_tech = (mtile.special & MAZETILE_SPECIAL_TECH_MASK);
   p_curtile.special_fill = (mtile.special & MAZETILE_FILLING_MASK);
   p_curtile.masktex = (mtile.special & ( Maze_WALL_MTEX_MASK + Maze_FLOOR_MTEX_MASK));
//   p_curtile.object_position = (mtile.texture & Maze_OBJECT_MASK) >> 6;
   //p_curtile.tilesetid = (mtile.texture & Maze_TEXSET_MASK);
   //p_curtile.walltex = mtile.walltex;
   //p_curtile.floortex = mtile.floortex;
   //p_curtile.objectimg = mtile.objectimg;
   p_curtile.event = mtile.event;


}*/

/*void Editor::write_curtile ( void )
{
   s_mazetile mtile;

   mtile.solid = (p_curtile.solid [0] << 6) + (p_curtile.solid [1] << 4)
      + (p_curtile.solid [2] << 2) + p_curtile.solid [3];
   mtile.special = p_curtile.special_tech + p_curtile.special_fill;
   //mtile.texture = p_curtile.masktex + ( p_curtile.object_position << 6 )
   //   + p_curtile.tilesetid;
   //mtile.walltex = p_curtile.walltex;
   //mtile.floortex = p_curtile.floortex;
   //mtile.objectimg = p_curtile.objectimg;
   mtile.event = p_curtile.event;

   mazetile [p_zcur][p_ycur][p_xcur] = mtile;
}*/


/*
   notes on work to do that should normaly be written on paper.

the grid icon must be thiner
Command : Add various view type : Special, fill, texture, masktex, item
   Or detailed view ( larger square with texture, item in it.
Command : Add one way wall door finder, or simple viewer
Launch demo view ( check with maze, drop some operationality and no combat)

*/

/*-----------------------------------------------------------------------------*/
/*-                                 Procedures                                -*/
/*-----------------------------------------------------------------------------*/

void Editor_draw_space_wall ( s_mazetile tile, int y, int x )
{  int r;

   for ( r = 0 ; r <= Maze_2BIT_WEST ; r++ )
   {  if ( is_mazetile_wall_oftype( tile, r, MAZETILE_SOLIDTYPE_WALL))
      {  rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_WALL],
            x, y, (r*64)<<16 );
      }
      if ( is_mazetile_wall_oftype( tile, r, MAZETILE_SOLIDTYPE_DOOR))
      {  rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_DOOR],
            x, y, (r*64)<<16 );
      }
      if ( is_mazetile_wall_oftype( tile, r, MAZETILE_SOLIDTYPE_GRID))
      {  rotate_sprite ( editorbuffer, datref_editoricon [EDICON_WALL_GRID],
            x, y, (r*64)<<16 );
      }
   }
}
/*void Editor_draw_space_special ( s_mazetile tile, int y, int x )
{
}*/
void Editor_draw_space_filling ( s_mazetile tile, int y, int x )
{
   int tmpfilling = get_mazetile_filling( tile );
   if ( tmpfilling > 0 )
   {
      set_trans_blender(0, 0, 0, 100);
      drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );

      if (Maze_SWALL_INFO[ tmpfilling].drawmode == DRAW_MODE_TRANS)
      {
         rectfill ( editorbuffer, x, y, x+15, y+15,
                makecol ( Maze_SWALL_INFO [ tmpfilling ].red,
                   Maze_SWALL_INFO [ tmpfilling ].green,
                   Maze_SWALL_INFO [ tmpfilling ].blue ) );

      }
      else
      {
         if ( tmpfilling == MAZETILE_FILLING_DARKNESS )
            draw_trans_sprite ( editorbuffer, datref_editoricon [EDICON_TILE_LIGHT], x, y );
      }

      drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
      set_trans_blender(0, 0, 0, 255);
   }
}


/*-----------------------------------------------------------------------------*/
/*-                          Global Variables                                 -*/
/*-----------------------------------------------------------------------------*/


const char STR_EDT_SPECIAL_TECH [][16] =
{
   {"Bounce"},
   {"Solid"},
   {"Light"},
   {""}
};

const char STR_EDT_SPECIAL_FILL [][16] =
{
   {"None"},
   {"Water"},
   {"Fizzle"},
   {"Fog"},
   {"Poison Gas"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"Darkness"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"}
};


/*const char STR_EDT_TOOL [][10] =
{
   { "Tile" },
   { "Corridor" },
   { "Room" },
   { "Area" }
   //{ "Area" }
}*/

const s_Editor_polygon_info Editor_POLYGON_INFO [6] =
{
   { EDITOR_POLYGON_WALL, 6, MAZETILE_SOLID_NORTH_MASK, MAZETILE_SOLID_NORTH_WALL },
   { EDITOR_POLYGON_WALL, 4, MAZETILE_SOLID_EAST_MASK, MAZETILE_SOLID_EAST_WALL },
   { EDITOR_POLYGON_WALL, 0, MAZETILE_SOLID_WEST_MASK, MAZETILE_SOLID_WEST_WALL },
   { EDITOR_POLYGON_CEILING, 0, 0, 0 },
   { EDITOR_POLYGON_FLOOR, 0, 0, 0 },
   { EDITOR_POLYGON_TRANSWALL, 2, MAZETILE_SOLID_SOUTH_MASK, MAZETILE_SOLID_SOUTH_WALL }

};

const s_Editor_texpalette_color Editor_texpalette_color [32] =
{
  { 80, -1 }, // texture palette 0
  { 80, 25 },
  { 88, -1 },
  { 88, 25 },
  { 96, -1 },
  { 96, 25 },
  { 104, -1 },
  { 104, 25 },
  { 112, -1 },
  { 112, 25 },
  { 120, -1 },
  { 120, 25 },
  { 128, -1 },
  { 128, 25 },
  { 136, -1 },
  { 136, 25 },

  { 144, -1 }, // texture palette 16
  { 144, 25 },
  { 152, -1 },
  { 152, 25 },
  { 160, -1 },
  { 160, 25 },
  { 168, -1 },
  { 168, 25 },
  { 176, -1 },
  { 176, 25 },
  { 184, -1 },
  { 184, 25 },
  { 192, -1 },
  { 192, 25 },
  { 200, -1 },
  { 200, 25 }

};


/*typedef struct V3D                  // a 3d point (fixed point version)
{
   fixed x, y, z;                   // position
   fixed u, v;                      // texture map coordinates
   int c;                           // color
} V3D;
*/

V3D Editor_room [ 6 ] [ 4 ] =
{

   { // center wall
      {490<<16, 145<<16, 1, 0,           TEXSIZE<<16, 255 },
      {490<<16, 59<<16,  1, 0,           0,           255 },
      {590<<16, 59<<16,  1, TEXSIZE<<16, 0,           255 },
      {590<<16, 145<<16, 1, TEXSIZE<<16, TEXSIZE<<16, 255 }
   },
   { // right wall
      {590<<16, 145<<16, 1, 0,           TEXSIZE<<16, 255 },
      {590<<16, 59<<16,  1, 0,           0,           255 },
      {640<<16, 16<<16,  0, TEXSIZE<<16, 0,           255 },
      {640<<16, 188<<16, 0, TEXSIZE<<16, TEXSIZE<<16, 255 }
   },
   { // left wall
      {440<<16, 188<<16, 0, 0,           TEXSIZE<<16, 255},
      {440<<16, 16<<16,  0, 0,           0,           255},
      {490<<16, 59<<16,  1, TEXSIZE<<16, 0,           255},
      {490<<16, 145<<16, 1, TEXSIZE<<16, TEXSIZE<<16, 255}
   },
   { // ceiling
      {490<<16, 59<<16, 1, 0,           TEXSIZE<<16, 255 },
      {440<<16, 16<<16,  0, 0,           0,          255 },
      {640<<16, 16<<16,  0, TEXSIZE<<16, 0,          255 },
      {590<<16, 59<<16, 1, TEXSIZE<<16, TEXSIZE<<16, 255 }
   },
   { //floor
      {440<<16, 188<<16, 0, 0,           TEXSIZE<<16, 255 },
      {490<<16, 145<<16,  1, 0,           0,          255 },
      {590<<16, 145<<16,  1, TEXSIZE<<16, 0,          255 },
      {640<<16, 188<<16, 0, TEXSIZE<<16, TEXSIZE<<16, 255 }
   },
   { // translucent wall (last in the list ot make sure it's drawn last)
      {440<<16, 188<<16, 0, 0,           TEXSIZE<<16, 255 },
      {440<<16, 16<<16,  0, 0,           0,           255 },
      {640<<16, 16<<16,  0, TEXSIZE<<16, 0,           255 },
      {640<<16, 188<<16, 0, TEXSIZE<<16, TEXSIZE<<16, 255 }
   },

};

/*{ { 228<<16,  320<<16, 3, 0,       TEXSIZE<<16, 924 },
      { 228<<16,  160<<16, 3, 0,       0,       924 },
      { 262<<16,  190<<16, 4, TEXSIZE<<16, 0,       858 },
      { 262<<16,  290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 858 } },*/

/*#define Maze_2BIT_NORTH   0
#define Maze_2BIT_EAST    1
#define Maze_2BIT_SOUTH   2
#define Maze_2BIT_WEST    3*/
