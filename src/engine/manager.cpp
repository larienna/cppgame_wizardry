/***************************************************************************/
/*                                                                         */
/*                          M A N A G E R . C P P                          */
/*                           Class source Code                             */
/*                                                                         */
/*     Content : Class Manager                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : July 24th, 2003                                     */
/*     License : GNU General Public License                                */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

#include <time.h>
#include <screen.h>

// NOTE: used for the mkdir and _mkdir methods. Could not find an allegro
// equivalent for directory creation which is platfrom independent.
#if defined (ALLEGRO_WINDOWS)
   #include <direct.h>
#else
   #include <sys/stat.h>
#endif


/*
//#include <time.h>
#include <allegro.h>
#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>








#include <listwiz.h>

#include <opponent.h>
#include <charactr.h>
//#include <monster.h>
#include <party.h>
#include <manager.h>


#include <game.h>
#include <city.h>
#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winmenu.h>
#include <winlist.h>
#include <winempty.h>
#include <wintitle.h>
#include <windata.h>
//#include <wdatproc.h>
#include <winmessa.h>
#include <winquest.h>
#include <wininput.h>
*/



/*-------------------------------------------------------------------------*/
/*-                     Constructor and Destructor                        -*/
/*-------------------------------------------------------------------------*/

Manager::Manager ( void )
{
   //p_account_logged.number ( 0 );

   strcpy ( p_savegamename, "");
}

Manager::~Manager ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                            Property Methods                           -*/
/*-------------------------------------------------------------------------*/

/*int Manager::account_logged ( void )
{
   return ( p_account_logged );
}*/

/*int Manager::nb_game ( void )
{

//   Game tmpgame;
//   unsigned int index =
   int nb_game = 0;
   unsigned int i = db.search_table_entry ( DBTABLE_GAME );
//   unsigned int nb_entry = db.nb_entry ();

   while ( db.entry_table_tag ( i ) == DBTABLE_GAME )
//   for ( i = db.search_table_entry ( DBTABLE_GAME ) ; i < nb_entry ; i++ )
   {
//      if ( db.entry_table_tag ( i ) == DBTABLE_GAME )
      nb_game++;
      i++;
   }

   return ( nb_game );
}*/
/*
int Manager::nb_account ( void )
{
//   Game tmpgame;
   unsigned int index = db.search_table_entry ( DBTABLE_ACCOUNT );
   int nb_account = 0;

   while ( db.entry_table_tag ( index ) == DBTABLE_ACCOUNT )
   {
      nb_account++;
      index++;
   }

   return ( nb_account );

}

int Manager::nb_adventure ( void )
{
   unsigned int index = db.search_table_entry ( DBTABLE_ADVENTURE );
   int nb_adventure = 0;

   while ( db.entry_table_tag ( index ) == DBTABLE_ADVENTURE )
   {
      nb_adventure++;
      index++;
   }

   return ( nb_adventure );

}

int Manager::nb_race ( void )
{
   unsigned int index = db.search_table_entry ( DBTABLE_RACE );
   int nb_race = 0;

   while ( db.entry_table_tag ( index ) == DBTABLE_RACE )
   {
      if ( db.entry_source_tag ( index ) == DBSOURCE_SYSTEM )
         nb_race++;
      index++;
   }

   return ( nb_race );

}

int Manager::nb_class ( void )
{
   unsigned int index = db.search_table_entry ( DBTABLE_CCLASS );
   int nb_class = 0;

   while ( db.entry_table_tag ( index ) == DBTABLE_CCLASS )
   {
      if ( db.entry_source_tag ( index ) == DBSOURCE_SYSTEM )
         nb_class++;
      index++;
   }

   return ( nb_class );

}*/


/*-------------------------------------------------------------------------*/
/*-                              Methods                                  -*/
/*-------------------------------------------------------------------------*/

int Manager::start ( void )
{

   int retvalue = System_GAME_STOP;
   short answer;
   //short answer2;

//   Window::cascade_start();

   Menu mnu_command ("Game Management");
   mnu_command.add_item (Manager_MENU_CREATE_GAME, "Create Game");
   mnu_command.add_item (Manager_MENU_LOAD_GAME, "Load Game");
   mnu_command.add_item (Manager_MENU_INSPECT_GAME, "Inspect Game", true);
   mnu_command.add_item (Manager_MENU_DELETE_GAME, "Delete Game");
   mnu_command.add_item (Manager_MENU_EXIT, "Exit");


   //db.default_source ( DBSOURCE_SYSTEM );
   play_music_track ( System_MUSIC_w3edge );

   WinEmpty wemp_background;
   WinTitle wttl_title ( "Wizardry Game Management");

   while ( retvalue == System_GAME_STOP )
   {
      //printf("Debug: Manager Start - pass 100\r\n");
      Window::instruction ( 320, 460 );

      WinMenu wmnu_command ( mnu_command, 20, 40 );
      //printf("Debug: Manager Start - pass 101\r\n");
      answer = Window::show_all();

      wmnu_command.hide();
      switch ( answer )
      {
         case Manager_MENU_CREATE_GAME:
            show_create_game();
        //    printf("Debug: Manager Start - pass 102\r\n");
         break;
         case Manager_MENU_LOAD_GAME:
            retvalue = show_load_game();
         break;
         case Manager_MENU_INSPECT_GAME:
            show_inspect_game();
         break;
         case Manager_MENU_DELETE_GAME:
            show_delete_game();
         break;
         case Manager_MENU_EXIT :
            retvalue = System_GAME_EXIT;
         break;
      }
 //     printf("Debug: Manager Start - pass 103\r\n");
   }

//   Window::cascade_stop();
//printf("Debug: Manager Start - pass 104\r\n");
   return ( retvalue );
}

void Manager::close_game ( void)
{

   char tmpstr [Manager_TMP_STR_LEN];
   struct tm *timeptr;
   time_t loctime;
   int i;

   WinEmpty wemp_background;

   WinTitle wttl_wait ("Saving Game", 320, 200);
   Window::show_all();
   wttl_wait.hide();

   /*Window::draw_all();
   textprintf_centre_old ( subbuffer, FNT_print, 320, 200, General_COLOR_TEXT,
      "Saving Game" );
   textout_centre_old ( subbuffer, FNT_print, "Please Wait", 320, 220, General_COLOR_TEXT );
   copy_buffer();*/


   loctime = time(NULL);
   game.lastgame (loctime);
   game.encounter_count (encount.counter());
   timeptr = localtime(&loctime);

   sprintf (tmpstr, "%d/%d/%d %d:%d", timeptr->tm_mday, timeptr->tm_mon +1,
                           timeptr->tm_year + 1900, timeptr->tm_hour, timeptr->tm_min );
   game.date ( tmpstr);
   Window::hide_party_frame();



   game.SQLupdate();
   party.SQLupdate();

   //?? save option config
   config.SQLsave();
   handmap.save();

   SQLcommit();
   SQLclose();

   config.load();
   config.initialise();



   //--- unloading sub datafiles ---
   //printf ("Manager:Close Game: Before unloading subdatafiles\n");
   for ( i = 0 ; i < System_NB_ADVDATAFILE ; i++ )
   {
      //printf ("i=%d\n", i);
      if (  advdatafilelist [ i ] . loaded == true )
      {
         unload_datafile_object ( advdatafilelist [ i ] . subobj );
         advdatafilelist [ i ] . loaded = false;
      }
   }


   system_log.write ("Game Closed");
   //printf ("Manager:Close Game: end of function\n");

}

int Manager::start_game ( void )
{
   int retval = System_GAME_STOP;
   char tmpstr [ Manager_TMP_STR_LEN ];
   int error = 0;
   int answer;
   int i;
   bool loadmaze = false;
   bool errorloadingsubdat = false;
   //DATAFILE* tmpdatobj;

//printf ("Manager: Start Game: entring function\n");

   sprintf (tmpstr, "savegame//%s", p_savegamename);
   error = SQLopen (tmpstr);
   //printf ("Open String = %s\r\n", tmpstr);

   if ( error == SQLITE_OK)
   {
         Window::draw_all();


         WinTitle wttl_wait ("Loading Game", 320, 200);
         Window::show_all();
         wttl_wait.hide();

         /*textprintf_centre_old ( subbuffer, FNT_print, 320, 200, General_COLOR_TEXT,
            "Loading Game" );
         textout_centre_old ( subbuffer, FNT_print, "Please Wait", 320, 220, General_COLOR_TEXT );*/
         //copy_buffer();

//         printf ("Manager: Start Last Game: before game loading\n");


         //printf ("debug: Manager::start_game: before config.save()\n");
         config.save();
         //printf ("debug: Manager::start_game: before config.SQLload()\n");
         config.SQLload();
         //printf ("debug: Manager::start_game: before config.initialise()\n");
         config.initialise();
         //printf ("debug: Manager::start_game: before game.SQLselect()\n");
         game.SQLselect(1);
         //printf ("debug: Manager::start_game: before party.SQLselect()\n");
         party.SQLselect(1);
//         printf ("party status=%d\n", party.status());
         //retval = Manager_PLAY;

         city.name (game.cityname());
         //maze.load_hardcoded(); //this was for testing purpose
         //printf ("debug: Manager::stat_game: before encount.clear_table\n");
         encount.clear_table ();
         encount.counter ( game.encounter_count() );
         //printf ("debug: Manager::stat_game: before force_recompile_rank()\n");
         Character::force_recompile_ranks();
         //printf ("debug: Manager::stat_game: before handmap.load()\n");
         handmap.load();

//         printf ("Manager: Start Last Game: after game loading\n");

         sprintf (tmpstr, "adventure//%s", game.adventurefile());
//         printf ("filename: %s", tmpstr);

//         printf ("debug: Before load maze\n");
         loadmaze = maze.load_from_adventure ( game.adventurefile() );
//         printf ("debug: after load maze\n");

         if ( loadmaze == true )
         {
            // loading adventure subdatafile

            for ( i = 0 ; i < System_NB_ADVDATAFILE ; i++ )
            {
               if ( strcmp ( advdatafilelist [ i ] . objectname, "") != 0 )
               {
                  advdatafilelist [ i ] . subobj = load_datafile_object
                     ( tmpstr, advdatafilelist [ i ] . objectname );

                  advdatafilelist [ i ] . datf =
                     (DATAFILE*) advdatafilelist [ i ] . subobj->dat;

                  if (advdatafilelist [ i ] . datf == NULL )
                  {
                     printf ("Error: Could not load: [%d] %s\n", i, advdatafilelist [ i ] . objectname );
                     errorloadingsubdat = true;
                  }

                  advdatafilelist [ i ] . loaded = true;
               }
            }

            if ( errorloadingsubdat == false )
            {
                system_log.clear();
                system_log.write ("Starting Game");

                // reference subdatafile

                datref_storyimg.add ( advdatafilelist [ ADATF_STORYIMG] . datf, 0 );



                retval = System_GAME_START;

                if ( game.newgame() == Game_NEW_TRUE )
                {
                    //temporarily disabled until new adventure file is used
                    WinQuestion wqst_intro ("Do you want to see the introduction story?");
                    answer = Window::show_all();

                    if ( answer == WinQuestion_ANSWER_YES)
                        show_intro();

                    //printf ("After Intro pass\n");
                    //show_ending(); // temp code
                  game.newgame( Game_NEW_FALSE );
//                  printf ( "Start Game: After newgame\n");
                  game.SQLupdate();
                  //printf ( "Start Game: After SQLupdate\n");

               }

               Window::show_party_frame();
            }
            else
            {
               WinMessage wmsg_error ( "Error loading adventure content" );
               Window::show_all();
               retval = System_GAME_STOP;

            }
        }
        else
        {
           WinMessage wmsg_error ( "Error loading maze content" );
           Window::show_all();
           retval = System_GAME_STOP;
        }
   }
   else
   {
      WinMessage wmsg_error ( "Error loading the Savegame" );
      Window::show_all();
      retval = System_GAME_STOP;
   }

//   printf ("Manager: Start Last Game: exiting function\n");
//   printf ( "start_game retval = %d\r\n", retval);
//   printf ( "Start Game: Before exit proc\n");
   return ( retval );
}


int Manager::start_last_game (void)
{

   //int result = -1;
   int error;
   int retval = System_GAME_EXIT;
   char tmpstr [Manager_TMP_STR_LEN];
   int file_retval;
   char filelist [ Manager_MAX_NB_FILE ] [ Manager_FILENAME_LEN ];
   int flistindex;
   al_ffblk info;
   //char tmpsavename [Manager_FILENAME_LEN];
   int tmptime = 0;
   int i;
   Game tmpgame;

//printf ("Manager: Start Last Game: entring function\n");

   // to replace
   //result = get_savegamename( "load");

   //printf ("Load result: %d\r\n", result);
   WinEmpty wemp_background;

   file_retval = al_findfirst("savegame//*.sqlite", &info, FA_ALL/*33*/ );

   //seach for all sqlite files
   flistindex = 0;
   if ( file_retval == 0 )
   {
      strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
      flistindex++;
      while ( al_findnext ( &info ) == 0 && flistindex < Manager_MAX_NB_FILE)
      {
         strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
         flistindex++;
      }
      al_findclose( &info);
   }

   if ( flistindex > 0)
   {
      //printf ("pass1\r\n");
      for ( i = 0 ; i < flistindex ; i++ )
      {
         sprintf (tmpstr, "savegame//%s", filelist [ i ]);
         error = SQLopen ( tmpstr);

         if ( error == SQLITE_OK )
         {

            error = tmpgame.SQLselect (1); //test in SQLselect for crash place

            if ( error == SQLITE_ROW )
            {
               if ( tmpgame.lastgame() > 0 && tmpgame.lastgame() > tmptime )
               {
                  strncpy ( p_savegamename, filelist [ i ], Manager_FILENAME_LEN);
                  tmptime = tmpgame.lastgame();
               }
            }
         }

         SQLclose();
      }

   }

   if ( tmptime > 0 )
   {
      retval = start_game();
      //printf ("after start game: Return Value= %d\r\n", retval);
      if ( retval == System_GAME_STOP)
         retval = System_GAME_EXIT;
   }
   else
   {
      WinMessage wmsg_error ( "There is no game to continue" );
      Window::show_all();
      retval = System_GAME_EXIT;
   }
//   printf ("start last game retval= %d\r\n", retval);
   return ( retval );
}



void Manager::show_create_game ( void )
{


   List lst_file ("Select adventure file to open ( adventure// )", 20 );
   //Adventure tmpadv;
   //unsigned int advindex;
   //DATAFILE *tmpobj = NULL;
   //Database tmpdb;
   char tmpstr [ Manager_TMP_STR_LEN ];
   int i;
   int answer;
   //short answer2;
   int retval;
   char filelist [ Manager_MAX_NB_FILE ] [ Manager_FILENAME_LEN ];
   int flistindex;
//   string tmpfilename;
   //char strfname [ 25 ];
   al_ffblk info;
   char tmpsavename [Manager_FILENAME_LEN];
   int savenumber = 0;
   int fileID;
   struct tm *timeptr;
   time_t loctime;

   retval = al_findfirst("adventure//*.wla", &info, FA_ALL/*33*/ );

   //Window::instruction ( 320, 460);

   //seach for all wza files
   flistindex = 0;
   if ( retval == 0 )
   {
      strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
      flistindex++;
      while ( al_findnext ( &info ) == 0 && flistindex < Manager_MAX_NB_FILE)
      {
         strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
         flistindex++;
      }
      al_findclose( &info);
   }

   if ( flistindex > 0)
   {
      for ( i = 0 ; i < flistindex ; i++ )
      {
         lst_file.add_item ( i, filelist [ i ] );
      }

      WinList wlst_file ( lst_file, 20, 40 );
      answer = Window::show_all();
      wlst_file.hide();

      if ( answer != -1 )
      {
         fileID = answer;
         /*Window::draw_all();
         textprintf_centre_old ( subbuffer, FNT_print, 320, 200, General_COLOR_TEXT,
            "Reading %s", filelist [ fileID ] );
         textout_centre_old ( subbuffer, FNT_print, "Please Wait", 320, 220, General_COLOR_TEXT );
         copy_buffer();*/

         //tmpobj = load_datafile ( filelist [ answer ] );
         // start loading DATAFILE
         /*sprintf ( strfname,"adventur//%s", filelist [ answer ] );
         tmpobj = load_datafile_object( strfname , "DBA_ADATABASE" );*/

         //fake dbloading
         //retval = SQLopen ("wizardry.sqlite");

         sprintf (tmpstr, "adventure//%s", filelist [ fileID ] );

         retval = SQLopentemp_from_dataobj ( tmpstr, "DATABASE_SQLITE" );


         //printf ("Debug: Filename %s\r\n", tmpstr);

         //if ( exists (tmpstr) != 0)
         //   printf ("Debug: File should exist\r\n");

         //SQLactivate_errormsg();

         if ( retval == SQLITE_OK )
         {

            retval = game.SQLselect (1);
            //printf("pass 1\r\n");

            if ( retval == SQLITE_ROW )
            {
               //printf("pass 2\r\n");
               sprintf ( tmpstr, "Do you want play this adventure?\nName:%s\nVersion %s\nSaga:%s\nEpisode:%d",
               game.name(), game.version(), game.saganame(), game.episode() );

               WinQuestion wqst_confirm ( tmpstr );
               answer = Window::show_all();
               wqst_confirm.hide();

               if ( answer == WinQuestion_ANSWER_YES )
               {
//printf("Debug: Create Game: pass 1\r\n");
                  // Check if savegame folder exists first, else create it
                  if ( file_exists("savegame", FA_DIREC, NULL) == FALSE )
                  {
                      //NOTE: mkdir methods are not portable on windows
                      #if defined(ALLEGRO_WINDOWS)
                         _mkdir("./savegame");
                      #else
                         mkdir("./savegame", 0700);
                      #endif
                  }

                  do
                  {
                     savenumber++;
                     sprintf (tmpsavename, "savegame//save%03d.sqlite", savenumber );

                  }
                  while ( exists (tmpsavename) != 0);

                  //WinEmpty wemp_difficulty;
                  //Window::

//printf("Debug: Create Game: pass 2\r\n");

                  config.save();
                  Window::instruction ( 320, 460, Window_INSTRUCTION_MENU +
                     Window_INSTRUCTION_SWITCH + Window_INSTRUCTION_CANCEL );
                  config.show_config_part( Config_CATEGORY_DIFFICULTY );

                  system_log.write ("Creating Game");

                  WinTitle wttl_wait ("Creating Game", 320, 200);
                  Window::show_all();
                  wttl_wait.hide();
//printf("Debug: Create Game: pass 3\r\n");
                  /*Window::draw_all();
                  textprintf_centre_old ( subbuffer, FNT_print, 320, 200, General_COLOR_TEXT,
                     "Creating Game" );
                  textout_centre_old ( subbuffer, FNT_print, "Please Wait", 320, 220, General_COLOR_TEXT );
                  copy_buffer();*/

                  config.SQLsave();
//printf("Debug: Create Game: pass 3.1\r\n");

//printf("Debug: Create Game: pass 3.2\r\n");

                  game.adventurefile ( filelist [ fileID ]);
//printf("Debug: Create Game: pass 4\r\n");
                  loctime = time(NULL);
                  timeptr = localtime(&loctime);

                  sprintf (tmpstr, "%d/%d/%d %d:%d", timeptr->tm_mday, timeptr->tm_mon +1,
                           timeptr->tm_year + 1900, timeptr->tm_hour, timeptr->tm_min );
                  game.date ( tmpstr);

//printf("Debug: Create Game: pass 5\r\n");
                  game.SQLupdate();


                  switch (config.get (Config_STARTING_GOLD))
                  {
                     case Config_DIF_LOW:
                        party.gold (2000);
                     break;
                     case Config_DIF_NORMAL:
                        party.gold (1000);
                     break;
                     case Config_DIF_HIGH:
                        party.gold (500);
                     break;
                  }
                  //party.destroy_all_spell();
                  party.reset_position();
                  party.status (Party_STATUS_CITY);
                  party.encounter (0);

                  party.SQLinsert(1);
                  //printf("Debug: Create Game: pass 5\r\n");
                  city.generate_newgame_item ();

                  //printf("Debug: Create Game: pass 6\r\n");
                  SQLcommit();
                  SQLbackup (tmpsavename);

                  SQLclosetemp();
                  config.load();
                  //printf("Debug: Create Game: pass 7\r\n");
               }
            }
            else
            {
               WinMessage wmsg_error ( "Error reading the adventure information." );
               Window::show_all();
               SQLerrormsg();
            }



         }
         else
         {
            WinMessage wmsg_error ( "Could not open the adventure database." );
            Window::show_all();
            SQLerrormsg();

         }

         SQLclosetemp ();

      }
   }
   else
   {
         //no files found error message
      WinMessage wmsg_error ( "There are no adventure files available\nmake sure the .dat files are located in the 'adventure' folder" );
      Window::show_all();

   }
//printf("Debug: Create Game: pass 8\r\n");
}



void Manager::show_inspect_game ( void )
{
   int result = -1;

   result = get_savegamename( "inspect");

   if ( result != -1 )
   {

   }


}

void Manager::show_delete_game ( void )
{

   int result = -1;
   char tmpstr [Manager_TMP_STR_LEN];
   int answer;

   result = get_savegamename( "delete");
//printf ("pass1\r\n");
   if ( result != -1 )
   {
      sprintf (tmpstr, "savegame//%s", p_savegamename);

//printf ("pass2\r\n");
      WinQuestion wqst_confirm("Are you sure you want to delete this game?");
      answer = Window::show_all();
//printf ("pass3\r\n");
      if ( answer == WinQuestion_ANSWER_YES )
      {
  //       printf ("pass1\r\n");
         system_log.write ("Deleting Game");
         result = remove ( tmpstr);

         if ( result != 0)
         {
            WinMessage wmsg_error ( "Error Deleting the game" );
            Window::show_all();
            system_log.write ("Error Deleting Game");
         }
      }
   }

}

int Manager::show_load_game ( void )
{
   // code to be added.

   int result = -1;
   //int error;
   int retval = System_GAME_STOP;
   //char tmpstr [Manager_TMP_STR_LEN];

   result = get_savegamename( "load");

   //printf ("Load result: %d\r\n", result);

   if ( result != -1 )
   {
      /*sprintf (tmpstr, "savegame//%s", p_savegamename);
      error = SQLopen (tmpstr);

      if ( error == SQLITE_OK)
      {*/
         system_log.write ("Loading Game");
         retval = start_game();
         //printf ("Load Game retval = %d\r\n", retval);

         /* // moved into start_game()
//printf ("Opening Game\r\n");
         Window::draw_all();
         textprintf_centre_old ( subbuffer, FNT_print, 320, 200, General_COLOR_TEXT,
            "Loading Game" );
         textout_centre_old ( subbuffer, FNT_print, "Please Wait", 320, 220, General_COLOR_TEXT );
         copy_buffer();

         config.save();
         config.SQLload();
         config.initialise();
         game.SQLselect(1);
         party.SQLselect(1);
         retval = Manager_PLAY;
         city.name (game.cityname());
         maze.load_hardcoded();

         sprintf (tmpstr, "adventure//%s", game.adventurefile());
         //printf ("filename: %s", tmpstr);
         adatf = load_datafile ( tmpstr );

         if ( adatf != NULL )
         {


            maze.reference_adv_bitmap();

            //maze.load();
            //??need to load events from database. Not ready yet

            if ( game.newgame() == Game_NEW_TRUE )
            {
               show_intro();
               //show_ending(); // temp code
               game.newgame( Game_NEW_FALSE );
               game.SQLupdate();
            }
         }
         else
         {
            WinMessage wmsg_error ( "Error loading adventure content" );
            Window::show_all();
            retval = System_GAME_STOP;

         }

      }*/


   }

   return retval;



}

int Manager::get_savegamename ( const char *purpose )
{
   char tmpstr [ Manager_TMP_STR_LEN ];
   char itemstr [ Manager_TMP_STR_LEN ];
   int i;
   int answer = -1;
   int retval;
   char filelist [ Manager_MAX_NB_FILE ] [ Manager_FILENAME_LEN ];
   int flistindex;
   al_ffblk info;
   int error;
   int error2;
   Game tmpgame;
   Party tmparty;
   char partystatus [ 16 ];
   //char tmpsavename [Manager_FILENAME_LEN];
   //int savenumber = 0;
   //int fileID;

   sprintf (tmpstr, "Select a game to %s", purpose);

   List lst_file ( tmpstr, 20 );

   retval = al_findfirst("savegame//*.sqlite", &info, FA_ALL/*33*/ );

   //seach for all sqlite files
   flistindex = 0;
   if ( retval == 0 )
   {
      strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
      flistindex++;
      while ( al_findnext ( &info ) == 0 && flistindex < Manager_MAX_NB_FILE)
      {
         strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
         flistindex++;
         //printf ("%d: %s\r\n", flistindex, info.name);
      }
      al_findclose( &info);
   }


   if ( flistindex > 0)
   {
      //printf ("pass1\r\n");
      for ( i = 0 ; i < flistindex ; i++ )
      {
         sprintf (itemstr, "savegame//%s", filelist [ i ]);
         error = SQLopen ( itemstr);

         if ( error == SQLITE_OK )
         {
  //          printf ("pass2\r\n");
            error = tmpgame.SQLselect (1); //test in SQLselect for crash place
            /*tmpgame.SQLprepare("");
            error = tmpgame.SQLstep();
            tmpgame.SQLfinalize();*/
//printf ("pass2.25\r\n");
            /*tmparty.SQLprepare("");
            error2 = tmparty.SQLstep();
            tmparty.SQLfinalize();*/
            //debug_printf( __func__, "Before SQLselect\n");
            error2 = tmparty.SQLselect (1);
//printf ("pass2.5\r\n");
            if ( error == SQLITE_ROW && error2 == SQLITE_ROW )
            {
//printf ("pass3\r\n");
               if ( tmpgame.newgame() == false)
                  sprintf ( partystatus, "Party in %s", STR_PAR_STATUS [ tmparty.status()]);
               else
                  sprintf ( partystatus, "New Game");

               lst_file.add_itemf ( i, "%s - %15s [%s](%s)", tmpgame.name(),
                                   partystatus, tmpgame.date(),
                                   filelist [ i ] );
                 //lst_file.add_item ( i, itemstr);
                 //printf ("The string added is: %s", itemstr);
            }
         }

         SQLclose();
      }

//      printf ("nb of item is: %d\r\n", lst_file.nb_item() );

      if (lst_file.nb_item() > 0 )
      {
//printf ("pass4\r\n");

         /*WinEmpty wemp_test ();
         Window::draw_all();
         lst_file.show (20,40);*/

         WinList wlst_file ( lst_file, 20, 40 );
         answer = Window::show_all();
         wlst_file.hide();

//         printf ("After read result: %d\r\n", answer);
         if (answer != -1)
         {
  //          printf ("pass5\r\n");
            strncpy ( p_savegamename, filelist [ answer ], Manager_FILENAME_LEN );
         }
      }
      else
      {
         WinMessage wmsg_error ( "There are no valid savegame files\nfiles could be corrupted." );
         Window::show_all();
      }
   }
   else
   {
         //no files found error message
      sprintf (tmpstr, "There are no game to %s", purpose);
      WinMessage wmsg_error ( tmpstr );
      Window::show_all();

   }

   return answer;
}

void Manager::show_intro ( void )
{
   short i;
   //char textline [ 101 ];
   char *textlineptr;
   //char *textptr;
   BITMAP *textbmp = create_bitmap ( 380, 520 );
   bool changeimg = false;
   bool nomoretext = false;
   short j;
   //int imgcount = 0;
   short l;
   short tmpl;
   short x;
   short y;
   short xrel;
   short yrel;

   Text txtparagraph [ Manager_INTEND_NB_PARAGRAPH ];
   Text tmptxt;
   int nb_paragraph = 0;
   int errorsql;
   //int paragraphid = 0;
   int currentpar = 0;
   bool paragraphend = false;

   errorsql = tmptxt.SQLprepare ( "WHERE rank BETWEEN 0 AND 15 ORDER BY rank" );

   nb_paragraph = 0;
   if ( errorsql == SQLITE_OK)
   {
      errorsql = tmptxt.SQLstep ();

      while ( errorsql == SQLITE_ROW )
      {
         txtparagraph [ nb_paragraph ] = tmptxt;
         txtparagraph [ nb_paragraph ].format ( 380, FNT_bookantiquabold24 );

         errorsql = tmptxt.SQLstep();
         nb_paragraph++;
      }


      tmptxt.SQLfinalize ();
   }


   system_log.write ("Showing Introduction Story");

   BITMAP *tmpbuffer1 = create_bitmap ( SCREEN_W, SCREEN_H );
   BITMAP *tmpbuffer2 = create_bitmap ( SCREEN_W, SCREEN_H );

   BITMAP *buf1 = create_sub_bitmap ( tmpbuffer1, System_X_OFFSET, System_Y_OFFSET, 640, 480);
   BITMAP *buf2 = create_sub_bitmap ( tmpbuffer2, System_X_OFFSET, System_Y_OFFSET, 640, 480);

   play_music_track ( System_MUSIC_w2intro );

   clear_to_color ( buf1, makecol ( 0, 0, 0 ) );
   clear_to_color ( buf2, makecol ( 0, 0, 0 ) );

   // buffer 1
   textout_centre_old ( buf1, FNT_watergothic48 , "W i z a r d r y    L e g a c y",
      320, 200, makecol ( 255, 225, 50 ) );

//   textout_centre_old ( buf1, FNT_kaufman24, p_authorname, 320, 300
//      , General_COLOR_TEXT );

   textout_centre_old ( buf1, FNT_monotypecorsiva24, "Presents", 320, 330
      , General_COLOR_TEXT );

   // buffer 2

   textout_centre_old ( buf2, FNT_bookmanoldstyle24, game.name(), 320, 100,
      General_COLOR_TEXT );

   textout_centre_old ( buf2, FNT_bookmanoldstyle24, game.saganame(), 320, 200,
      General_COLOR_TEXT );

   if ( game.episode() != 0 )
      textprintf_centre_old ( buf2, FNT_bookmanoldstyle24, 320, 240, General_COLOR_TEXT,
         "Episode %d", game.episode() );

   textprintf_centre_old ( buf2, FNT_bookmanoldstyle24, 320, 320, General_COLOR_TEXT, "Created by %s,  Version %s",
      game.author(), game.version() );

   //testing code


   //testcode that does not work
   /*blit ( screen, datref_storyimg [ 0 ] , 0, 0, 0, 0, 240, 360 );
   blit ( screen, datref_storyimg [ 1 ] , 0, 0, 240, 0, 240, 360 );
   blit ( screen, datref_storyimg [ 2 ] , 0, 0, 480, 0, 240, 360 );

   rest (5000);*/

   vsync();
   blit ( tmpbuffer1, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );

   rest ( Manager_INTRO_SCROLL_PAUSE );

   for ( i = 1 ; i <= 160 ; i++ )
   {
      blit ( tmpbuffer1, buffer, 0, 0, 0 , 0 - ( i * 3 ), SCREEN_W, SCREEN_H );
      blit ( tmpbuffer2, buffer, 0, 0, 0, 480 - ( i * 3 ), SCREEN_W, SCREEN_H );
      vsync();
      blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
      rest ( 1 );
   }

   rest ( Manager_INTRO_SCROLL_PAUSE );

   for ( i = 1 ; i <= 160 ; i++ )
   {

      blit ( tmpbuffer2, buffer, 0, 0, 0, 0 - ( i * 3 ), SCREEN_W, SCREEN_H );
      vsync();
      blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
      rest ( 1 );
   }

   clear_bitmap ( screen );
   clear_bitmap ( buffer );
   clear_bitmap ( tmpbuffer1 );
   clear_bitmap ( tmpbuffer2 );
//   short linex [ 20 ];
//   char linechar [ 20 ] [ 31 ];

   clear_bitmap ( textbmp );

   for ( j = 0 ; j < 4 ; j++ )
   {
      switch ( j )
      {
         case 0 :
            textout_centre_old ( textbmp, FNT_elementarygothicbookhand32, game.saganame(), 180, 480, General_COLOR_TEXT );
         break;
         case 1 :
         if ( game.episode() != 0 )
            textprintf_centre_old ( textbmp, FNT_elementarygothicbookhand32, 180, 480, General_COLOR_TEXT,
            "           Episode %d         ", game.episode() );
         break;
         case 2 :
            textout_centre_old ( textbmp, FNT_elementarygothicbookhand32, game.name(), 180, 480, General_COLOR_TEXT );
         break;
         /*case 3 :
            textprintf_centre_old ( textbmp, FNT_elementarygothicbookhand24, 180, 480, General_COLOR_TEXT, "Created by %s,   Version %s",
                               game.author(), game.version() );
   //      textout_old ( textbmp, FNT_helena24, "    -    1    -    2    -    3    -    4    -    5"
//            , 0, 480, General_COLOR_TEXT );
         break;*/
      }

      for ( i = 0 ; i < 24 ; i++ )
      {
         blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 520 );
         vsync ();
         blit ( textbmp, subscreen, 0, 0, 0, 0, 380, 480 );
         rest(Manager_INTRO_SCROLL_REST);
      }
   }

   //blit ( buf1, datref_storyimg [ txtparagraph [ currentpar ] . pictureid() ], 0, 0, 0, 0, 240, 360 );

   //blit ( buf1, subscreen, 0, 0, 0, 0, 240, 360 );

   // need text in adventure
   //textptr = TXT_astory;

   changeimg = false;
   paragraphend = true;
   //nomoretext=true;
   while ( nomoretext == false )
   {
      if ( paragraphend == true )
      {
         textlineptr = txtparagraph [ currentpar ].readline_start ();
         paragraphend = false;
         blit ( datref_storyimg [ txtparagraph [ currentpar ] . pictureid() ], buf2, 0, 0, 0, 0, 240, 360 );
         changeimg = true;
      }
      else
      {
         textlineptr = txtparagraph [ currentpar ].readline_next ();
      }

      if ( textlineptr == NULL )
      {
         paragraphend = true;
         currentpar++;
         if ( currentpar >= nb_paragraph)
         {


            nomoretext = true;
         }

      }

      // copy text
      /*i = 0;
      while ( *textptr != '\n' && i < 40 )
      {
         if ( *textptr != '\n' && *textptr != '\r' )
         {
            textline [ i ] = *textptr;
            i++;
         }
         textptr++;

      }
      textline [ i ] = '\0';
      textptr++;*/

      // check for code
      /*if ( strcmp ( textline, "<NEXTIMAGE>" ) == 0 )
      {
         imgcount++;

         //need pictures from adventure
         //blit ( (BITMAP*) adatf [ BMP_ASTORY000 + imgcount ] . dat, buf2,
         //   0, 0, 0, 0, 240, 360 );

         changeimg = true;
//         if ( imgcount == 1 )
//            make_screen_shot ("storshot.bmp");//@@
      }*/

      /*if ( strcmp ( textline, "<ENDSTORY>" ) == 0 )
         nomoretext = true;*/


      if ( nomoretext == false )
      {
         l = 0;


         if ( /*changeimg != true &&*/ textlineptr != NULL)
         {


            textout_justify_old ( textbmp, FNT_bookantiquabold24 , textlineptr, 0, 380, 480, 152, General_COLOR_TEXT );
            //vline ( textbmp, 380, 480, 520, General_COLOR_TEXT );
           // textout_old ( textbmp, FNT_bookantiquabold24, textlineptr, 0, 480, General_COLOR_TEXT );
         }
//
         for ( i = 0 ; i < 24 ; i++ )
         {

            blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 520 );
            if ( changeimg == true )
            {
               // image blitting

               if ( i < 16 )
               {

 //                 for ( k = 0 ; k < 16 ; k++ )
//                  {
                  // calculating correct l value;

                  if ( l > 15 )
                     l = l - 16;

                  // interpretation of relative pixel
                  tmpl = l;
                  xrel = 0;
                  yrel = 0;
                  while ( tmpl > 3 )
                  {
                     yrel++;
                     tmpl = tmpl - 4;
                  }
                  xrel = tmpl;

                  // draw the pixel
                  for ( y = 0 ; y < 360 ; y = y + 4 )
                     for ( x = 0 ; x < 240 ; x = x + 4 )
                        putpixel ( buf1, x + xrel, y + yrel, getpixel ( buf2, x + xrel, y+ yrel ) );

                  // incrementing l;
                  l = l + 7;
               }
            }
            vsync ();
            blit ( textbmp, subscreen, 0, 0, 0, 0, 380, 480 );
            if ( changeimg == true && i < 16 )
               blit ( buf1, subscreen, 0, 0, 400, 60, 240, 360 );
            rest(Manager_INTRO_SCROLL_REST);
         }
      }
      changeimg = false;
   }

   // scrool rest

   for ( i = 0 ; i < 480 ; i++ )
   {
      blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 520 );
      vsync ();
      blit ( textbmp, subscreen, 0, 0, 0, 0, 380, 480 );
      rest ( Manager_INTRO_SCROLL_REST );
   }

   //clear_bitmap ( screen );
   //clear_bitmap ( buffer );
//   printf ("Show Intro: Before clearing process\n");
   clear_bitmap ( backup );

   destroy_bitmap ( textbmp );
   destroy_bitmap ( buf1 );
   destroy_bitmap ( buf2 );
   destroy_bitmap ( tmpbuffer1 );
   destroy_bitmap ( tmpbuffer2 );

   blit ( screen, buffer, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   show_transition_algo7 ( buffer, backup, 10 );

   clear_bitmap( buffer );

   //rest ( 1000 );
   stop_midi();
   rest ( 1000 );
   //printf ("Show Intro: After clearing process\n");
}

void Manager::show_ending ( void )
{
   short i;
   char textline [ 41 ];
   char tmpstr [60] = "";
   char *textptr;
   char *textlineptr;
   BITMAP *textbmp = create_bitmap ( 380, 510 );
   bool changeimg = false;
   bool nomoretext = false;
   short j;
   //int imgcount = 0;
   short l;
   short tmpl;
   short x;
   short y;
   short xrel;
   short yrel;

   Text txtparagraph [ Manager_INTEND_NB_PARAGRAPH ];
   Text tmptxt;
   int nb_paragraph = 0;
   int errorsql;
   //int paragraphid = 0;
   int currentpar = 0;
   bool paragraphend = false;

   errorsql = tmptxt.SQLprepare ( "WHERE rank BETWEEN 15 AND 31 ORDER BY rank" );

   nb_paragraph = 0;
   if ( errorsql == SQLITE_OK)
   {
      errorsql = tmptxt.SQLstep ();

      while ( errorsql == SQLITE_ROW )
      {
         txtparagraph [ nb_paragraph ] = tmptxt;
         txtparagraph [ nb_paragraph ].format ( 380, FNT_bookantiquabold24 );

         errorsql = tmptxt.SQLstep();
         nb_paragraph++;
      }


      tmptxt.SQLfinalize ();
   }

   system_log.write ("Showing Ending Story and Credits");

   BITMAP *tmpbuffer1 = create_bitmap ( SCREEN_W, SCREEN_H );
   BITMAP *tmpbuffer2 = create_bitmap ( SCREEN_W, SCREEN_H );

   BITMAP *buf1 = create_sub_bitmap ( tmpbuffer1, System_X_OFFSET, System_Y_OFFSET, 640, 480);
   BITMAP *buf2 = create_sub_bitmap ( tmpbuffer2, System_X_OFFSET, System_Y_OFFSET, 640, 480);
   clear_bitmap ( textbmp );

   play_music_track ( System_MUSIC_w1ending );

// End Story

   clear_bitmap ( screen );

   //need picture in adventure
   //blit ( BMP_aendstory000, buf1, 0, 0, 0, 0, 240, 360 );

   //blit ( buf1, subscreen, 0, 0, 380, 60, 240, 360 );

   //need text from adventure
   //textptr = TXT_aendstory;
   changeimg = false;
   paragraphend = true;
   while ( nomoretext == false )
   {
      if ( paragraphend == true )
      {
         //printf ("Show_ending: currentpar = %d\n", currentpar );
         textlineptr = txtparagraph [ currentpar ].readline_start ();
         paragraphend = false;
         blit ( datref_storyimg [ txtparagraph [ currentpar ] . pictureid() ], buf2, 0, 0, 0, 0, 240, 360 );
         changeimg = true;
      }
      else
      {
         textlineptr = txtparagraph [ currentpar ].readline_next ();
      }

      if ( textlineptr == NULL )
      {
         paragraphend = true;
         currentpar++;
         if ( currentpar >= nb_paragraph)
         {


            nomoretext = true;
         }

      }
      // copy text
      /*i = 0;
      while ( *textptr != '\n' && i < 40 )
      {
         if ( *textptr != '\n' && *textptr != '\r' )
         {
            textline [ i ] = *textptr;
            i++;
         }
         textptr++;

      }
      textline [ i ] = '\0';
      textptr++;*/

      // check for code
      /*if ( strcmp ( textline, "<NEXTIMAGE>" ) == 0 )
      {
         imgcount++;
         //need picture from adventure
         //blit ( (BITMAP*) adatf [ BMP_ASTORY000 + imgcount ] . dat, buf2,
         //   0, 0, 0, 0, 240, 360 );

         changeimg = true;
      }
      if ( strcmp ( textline, "<ENDSTORY>" ) == 0 )
         nomoretext = true;*/

      if ( nomoretext == false )
      {
         l = 0;
         if ( /*changeimg != true &&*/ textlineptr != NULL)
         {


            textout_justify_old ( textbmp, FNT_bookantiquabold24 , textlineptr, 0, 380, 480, 152, General_COLOR_TEXT );
         }
         //if ( changeimg != true )
//            textout_old ( textbmp, FNT_helena24, textline, 0, 480, General_COLOR_TEXT );
         //   textout_old ( textbmp, FNT_blackchancery24, textline, 0, 480, General_COLOR_TEXT );

         for ( i = 0 ; i < 24 ; i++ )
         {

            blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 510 );
            if ( changeimg == true )
            {
               // image blitting

               if ( i < 16 )
               {

 //                 for ( k = 0 ; k < 16 ; k++ )
//                  {
                  // calculating correct l value;

                  if ( l > 15 )
                     l = l - 16;

                  // interpretation of relative pixel
                  tmpl = l;
                  xrel = 0;
                  yrel = 0;
                  while ( tmpl > 3 )
                  {
                     yrel++;
                     tmpl = tmpl - 4;
                  }
                  xrel = tmpl;

                  // draw the pixel
                  for ( y = 0 ; y < 360 ; y = y + 4 )
                     for ( x = 0 ; x < 240 ; x = x + 4 )
                        putpixel ( buf1, x + xrel, y + yrel, getpixel ( buf2, x + xrel, y+ yrel ) );

                  // incrementing l;
                  l = l + 7;
               }
            }
            vsync ();
            blit ( textbmp, subscreen, 0, 0, 0, 0, 380, 480 );
            if ( changeimg == true && i < 16 )
               blit ( buf1, subscreen, 0, 0, 400, 60, 240, 360 );
            rest(Manager_ENDING_SCROLL_REST);
         }
      }
      changeimg = false;
   }

   // scrool rest

   for ( i = 0 ; i < 480 ; i++ )
   {
      blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 510 );
      vsync ();
      blit ( textbmp, subscreen, 0, 0, 0, 0, 380, 480 );
      rest ( Manager_ENDING_SCROLL_REST );
   }

   //clear_bitmap ( screen );
   clear_bitmap ( backup );

   //show_transition_algo7 ( screen, buffer, 10 );

   blit ( screen, buffer, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   show_transition_algo7 ( buffer, backup, 10 );

   stop_midi ();
   rest ( 1000 );


// Congratulation

   sprintf ( tmpstr, "adventure//%s", game.adventurefile() );
   DATAFILE *creditsdat = load_datafile_object ( tmpstr, "CREDITS_TEXT");

   play_music_table ( Config_MUSICTAB_CREDITS );

   //play_music_track ( System_MUSIC_w2ending);

   textout_centre_old ( subbuffer, FNT_helena24, "C o n g r a t u l a t i o n s",
      320, 100, makecol ( 255, 225, 50 ) );

   textout_centre_old ( subbuffer, FNT_ambrosia24, "You have succesfully completed",
      320, 200, makecol ( 255, 225, 50 ) );

   textout_centre_old ( subbuffer, FNT_elgar32, game.name() ,
      320, 300, makecol ( 255, 225, 50 ) );

   textout_centre_old ( subbuffer, FNT_elgar32, game.saganame(),
      320, 350, makecol ( 255, 225, 50 ) );

   if ( game.episode() != 0 )
      textprintf_centre_old ( subbuffer, FNT_elgar32, 320, 400,
         makecol ( 255, 225, 50 ), "Episode %d", game.episode() );

   vsync();
   blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );

   rest (Manager_ENDING_SCROLL_PAUSE);

   for ( i = 0 ; i < 160 ; i++ )
   {
      blit ( buffer, buffer, 0, 3, 0, 0, SCREEN_W, SCREEN_H - 3 );
      vsync ();
      blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
      rest(1);
   }

// Credits

   for ( j = 0 ; j < 2 ; j++ )
   {
      switch ( j )
      {
         case 0 :
         textout_centre_old ( textbmp, FNT_ambrosia24, "Wizardry Legacy Credits",
            180, 480, General_COLOR_TEXT );
         break;
         case 1 :
         break;
      }

      for ( i = 0 ; i < 24 ; i++ )
      {
         blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 510 );
         vsync ();
         blit ( textbmp, subscreen, 0, 0, 130, 0, 380, 480 );
         rest(Manager_CREDIT_SCROLL_REST);
      }
   }


   textptr = wizcredits;
   nomoretext = false;
   while ( nomoretext == false )
   {
      // copy text
      i = 0;
      while ( *textptr != '\n' && i < 40 )
      {
         if ( *textptr != '\n' && *textptr != '\r' )
         {
            textline [ i ] = *textptr;
            i++;
         }
         textptr++;

      }
      textline [ i ] = '\0';
      textptr++;

      // check for code
      if ( strcmp ( textline, "<ENDCREDITS>" ) == 0 )
         nomoretext = true;

      if ( nomoretext == false )
      {
         textout_justify_old ( textbmp, FNT_helvetica22, textline, 0, 380, 480, 152, General_COLOR_TEXT );
         for ( i = 0 ; i < 24 ; i++ )
         {

            blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 510 );
            vsync ();
            blit ( textbmp, subscreen, 0, 0, 130, 0, 380, 480 );
            rest(Manager_CREDIT_SCROLL_REST);
         }
      }
   }

   //credits from adventure.

   textptr = (char*) creditsdat -> dat;
   if ( textptr != NULL)
   {

      for ( j = 0 ; j < 3 ; j++ )
      {
          switch ( j )
         {
            case 0 :
            textout_centre_old ( textbmp, FNT_helena24, game.name(), 180, 480, General_COLOR_TEXT );
            break;
            case 1 :
            textout_centre_old ( textbmp, FNT_helena24, "Credits", 180, 480, General_COLOR_TEXT );
            break;
            case 2 :
            break;
          }

         for ( i = 0 ; i < 24 ; i++ )
         {
            blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 510 );
            vsync ();
            blit ( textbmp, subscreen, 0, 0, 130, 0, 380, 480 );
            rest(Manager_CREDIT_SCROLL_REST);
         }
      }

      nomoretext = false;
      while ( nomoretext == false )
      {
         // copy text
         i = 0;
         while ( *textptr != '\n' && i < 40 )
         {
            if ( *textptr != '\n' && *textptr != '\r' )
            {
               textline [ i ] = *textptr;
               i++;
            }
            textptr++;

         }
         textline [ i ] = '\0';
         textptr++;

         // check for code
         if ( strcmp ( textline, "<ENDCREDITS>" ) == 0 )
            nomoretext = true;

         if ( nomoretext == false )
         {
            textout_justify_old ( textbmp, FNT_helvetica22, textline, 0, 380, 480, 152, General_COLOR_TEXT );
            for ( i = 0 ; i < 24 ; i++ )
            {

               blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 510 );
               vsync ();
               blit ( textbmp, subscreen, 0, 0, 130, 0, 380, 480 );
               rest(Manager_CREDIT_SCROLL_REST);
            }
         }
      }
   }

   // scrool rest

   for ( i = 0 ; i < 48 ; i++ )
   {
       blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 510 );
       vsync ();
       blit ( textbmp, subscreen, 0, 0, 130, 0, 380, 480 );
       rest(Manager_CREDIT_SCROLL_REST);
   }


   textout_old ( textbmp, FNT_elgar32, "And please worship Vorpal Bunnies" , 0, 480, General_COLOR_TEXT );
   for ( i = 0 ; i < 510 ; i++ )
   {
      blit ( textbmp, textbmp, 0, 1, 0, 0, 380, 510 );
      vsync ();
      blit ( textbmp, subscreen, 0, 0, 130, 0, 380, 480 );
      rest ( Manager_CREDIT_SCROLL_REST );
   }

   clear_bitmap ( screen );
   clear_bitmap ( buffer );

   rest ( 2000 );

   BITMAP *thend = create_bitmap ( 320, 50 );

   clear_to_color ( thend, makecol ( 255, 0, 255 ) );

   textout_centre_old ( thend, FNT_helenabold32, "T h e    E n d", 160, 10,
      makecol ( 250, 150, 150 ) );

   for ( i = 3 ; i < 256 ; i = i + 4 )
   {
      set_trans_blender ( 0, 0, 0, i );
      draw_trans_sprite ( subbuffer, thend, 160, 215 );
      copy_buffer();
   }

   rest ( Manager_ENDING_THEND_PAUSE );

   for ( i = 255 ; i > 0 ; i = i - 4 )
   {
      set_trans_blender ( 0, 0, 0, i );
      draw_trans_sprite ( subbuffer, thend, 160, 215 );
      copy_buffer();
   }

   rest ( 1000 );

   destroy_bitmap ( buf1 );
   destroy_bitmap ( buf2 );
   destroy_bitmap ( textbmp );
   destroy_bitmap ( tmpbuffer1 );
   destroy_bitmap ( tmpbuffer2 );
   destroy_bitmap ( thend );
   unload_datafile_object ( creditsdat );

}


/*
void Manager::show_create_account ( void )
{
   Account tmpacc;
   Account tmpacc2;
   unsigned int index;
   char tmpstr [ 16 ];
//   char strentry [ 81 ];
   char tmpstr2 [ 16 ];
   bool doublelogin = false;

   WinData<Account> wdat_account ( WDatProc_new_account, tmpacc,
      WDatProc_POSITION_NEW_ACCOUNT );

   WinInput winp_loginame ("Enter the Login Name of your account", 10, 20, 40 );
   do
   {
      doublelogin = false;

      Window::show_all();
      winp_loginame.get_string ( tmpstr );

      index = db.search_table_entry ( DBTABLE_ACCOUNT );

      while ( db.entry_table_tag ( index ) == DBTABLE_ACCOUNT )
      {
         tmpacc2.SQLselect ( index );
         if ( strcmp ( tmpstr, tmpacc2.loginame() ) == 0 )
            doublelogin = true;
         index++;
      }
      if ( doublelogin == true )
      {
         WinMessage msg_error ("This loginame already exist, type another one" );
         Window::show_all();
      }
   }
   while ( doublelogin == true );

   tmpacc.loginame ( tmpstr );
   winp_loginame.hide();
   wdat_account.refresh();

   WinInput winp_name ("Enter Your first name", 15, 20, 40 );
   Window::show_all();
   winp_name.get_string ( tmpstr );
   tmpacc.name ( tmpstr );
   winp_name.hide();
   wdat_account.refresh();

   WinInput winp_family ("Enter Your family name", 15, 20, 40 );
   Window::show_all();
   winp_family.get_string ( tmpstr );
   tmpacc.family ( tmpstr );
   winp_family.hide();
   wdat_account.refresh();

   WinInput winp_nickname ("Enter your Nick Name", 15, 20, 40 );
   Window::show_all();
   winp_nickname.get_string ( tmpstr );
   tmpacc.nickname ( tmpstr );
   winp_nickname.hide();
   wdat_account.refresh();

   do
   {
      WinInput winp_password ("Enter the Password", 10, 20, 40 );
      Window::show_all();
      winp_password.get_string ( tmpstr );
//      winp_password.hide();

      WinInput winp_confirm ("Confirm the Password", 10, 20, 100 );
      Window::show_all();
      winp_confirm.get_string ( tmpstr2 );
//      winp_password.hide();

      if ( strcmp ( tmpstr, tmpstr2 ) != 0 )
      {
         WinMessage wmsg_error ("Your password does not match, try again");
         Window::show_all();
      }

   }
   while ( strcmp ( tmpstr, tmpstr2 ) != 0 );

   tmpacc.password ( tmpstr );

   tmpacc.SQLinsert ();


}

void Manager::show_inspect_account ( void )
{
   List lst_account ("Select an account to INSPECT", 22);
   Account tmpacc;
   unsigned int index = db.search_table_entry ( DBTABLE_ACCOUNT );
//   string tmpstr;
   char strentry [ 81 ];
   short answer = 0;

   while ( db.entry_table_tag ( index ) == DBTABLE_ACCOUNT )
   {
      tmpacc.SQLselect ( index );
      strmake_account_list ( tmpacc, strentry );
      lst_account.add_item ( strentry );
      index++;
   }

   WinList wlst_account ( lst_account, 20, 40 );
   while ( answer != -1 )
   {
      Window::instruction ( 320, 460 );
      answer = Window::show_all();
   }

}

void Manager::show_delete_account ( void )
{

//   string tmpstr;
   char strentry [ 81 ];
   char tmpstr2 [ 101 ];
   short answer = 0;
   short answer2 = 0;
   char password [ 11 ];
//   bool selected = false;
   int copytag [ Manager_NB_MAX_ACCOUNT ];
   short j;
   Account tmpacc;

   while ( answer != -1 && nb_account() > 0 )
   {
      List lst_account ("Select an account to DELETE", 22);
      unsigned int index = db.search_table_entry ( DBTABLE_ACCOUNT );
      j = 0;
      Window::instruction ( 320, 460 );
      while ( db.entry_table_tag ( index ) == DBTABLE_ACCOUNT )
      {
         tmpacc.SQLselect ( index );
         strmake_account_list ( tmpacc, strentry );
         lst_account.add_item ( strentry );
         copytag [ j ] = tmpacc.tag();
         index++;
         j++;
      }

      WinList wlst_account ( lst_account, 20, 40 );
      answer = Window::show_all();
      if ( answer != -1 )
      {
         tmpacc.SQLselect ( copytag [ answer ] );
         sprintf ( tmpstr2, "Are you sure you want to delete the [%s] account?\nIt will remove all your players from all games",
            tmpacc.loginame() );
         WinQuestion wmsg_confirm ( tmpstr2 );
         answer2 = Window::show_all();
         wmsg_confirm.hide();
         if ( answer2 == 0 )
         {
            WinInput winp_password ("Enter your Account's password?", 10, 200, 100 );
            Window::show_all();
            winp_password.hide();
            winp_password.get_string ( password );

            if ( tmpacc.verify_password( password ) == true )
            {
               tmpacc.DBremove();
            }
            else
            {
               WinMessage wmsg_error ("Password Incorrect!");
               Window::show_all();
            }
         }
      }
   }
}

void Manager::show_create_race ( void )
{
   Race tmprace;
   int answer;

   tmprace.edit();

   WinQuestion wqst_confirm ("Do you want to save this new race?");
   answer = Window::show_all();

   if ( answer == WinQuestion_ANSWER_YES )
   {
      db.default_source ( DBSOURCE_SYSTEM );
      tmprace.SQLinsert();
   }
}

void Manager::show_inspect_race ( void )
{
   List lst_race ("Select an Race to INSPECT", 22);
   Race tmprace;
   unsigned int index = db.search_table_entry ( DBTABLE_RACE );
//   string tmpstr;
   char strentry [ 81 ];
   short answer = 0;
   int tmptag;

   while ( db.entry_table_tag( index ) == DBTABLE_RACE )
   {
      tmptag = db.get_tag ( index );
      if ( tmptag.source() == DBSOURCE_SYSTEM )
      {
         tmprace.SQLselect ( index );
         strmake_race_list ( tmprace, strentry );
         lst_race.add_item ( strentry );
      }
      index++;
   }

   WinList wlst_race ( lst_race, 20, 40 );
   while ( answer != -1 )
   {
      Window::instruction ( 320, 460 );
      answer = Window::show_all();
   }

}

void Manager::show_delete_race ( void )
{
   List lst_race ("Select an Race to DELETE", 22);
   Race tmprace;
   unsigned int index = db.search_table_entry ( DBTABLE_RACE );
//   string tmpstr;
   char strentry [ 81 ];
   short answer = 0;
   short answer2 = 0;
   int tmptag;
   unsigned int copyindex [ Manager_NB_MAX_RACE ];
   int j = 0;
   char tmpstr2 [ 101 ];

   while ( db.entry_table_tag( index ) == DBTABLE_RACE )
   {
      tmptag = db.get_tag ( index );
      if ( tmptag.source() == DBSOURCE_SYSTEM )
      {
         tmprace.SQLselect ( index );
         strmake_race_list ( tmprace, strentry );
         lst_race.add_item ( strentry );
         copyindex [ j ] = index;
         j++;
      }
      index++;
   }

   WinList wlst_race ( lst_race, 20, 40 );
   if ( answer != -1 )
   {
      Window::instruction ( 320, 460 );
      answer = Window::show_all();

      if ( answer >= 0 )
      {
         tmprace.SQLselect ( copyindex [ answer ] );
         sprintf ( tmpstr2, "Are you sure you want to delete the %s", tmprace.plural() );
         WinQuestion wqst_confirm ( tmpstr2 );

         answer2 = Window::show_all();
         wqst_confirm.hide();

         if ( answer2 == WinQuestion_ANSWER_YES )
         {
            tmprace.DBremove();
         }
      }
   }
}

void Manager::show_import_race ( void )
{

}

void Manager::show_export_race ( void )
{

}

void Manager::show_create_class ( void )
{

}

void Manager::show_inspect_class ( void )
{

}

void Manager::show_delete_class ( void )
{

}

void Manager::show_import_class ( void )
{

}

void Manager::show_export_class ( void )
{

}
*/

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

//Manager manager;


char wizcredits [ 1501 ] =
{
"Game Redesigner                         \n          Eric Pietrocupo \nProgrammer                              \n          Eric Pietrocupo \nGraphics Ripper                         \n          Eric Pietrocupo \nImage Manipulation                      \n          Eric Pietrocupo \nMusic Instrumentaliser                  \n          Eric Pietrocupo \nSound Ripper                            \n          Eric Pietrocupo \nGame Tester                             \n          Eric Pietrocupo \nWeb Master                              \n          Eric Pietrocupo \nSpecial Thanks                          \n          Andrew Greenberg \n          Robert Woodhead  \n          Sir-Tech Software   \n\n\n<ENDCREDITS>"
};


