
#include <stdbool.h>
#include <mazetile.h>


s_mazetile new_mazetile_empty ( void )
{  s_mazetile tile;
   set_mazetile_empty( &tile );

   return tile;
}

void set_mazetile_empty ( s_mazetile *tile )
{  tile->event = 0;
   tile->special = 0;
   tile->solid = 0;
   tile->objectpic = 0;
   tile->wobjpalette = 0;
   tile->objposition = 0;
   tile->masked = 0;
   tile->maskposition = 0;
}

//-------------------------- Byte 1 ----------------------------

void set_mazetile_wall ( s_mazetile *tile, int side, int type )
{  int i;
   unsigned char mask = MAZETILE_SOLID_WEST_MASK;
   //shift the type left a number of times according to the direction
   for ( i = 3 ; i > side; i--)
   {  type = type << 2;
      mask = mask << 2;
   }

   //Clear out the area to place the new value
   tile->solid = tile->solid & ~mask;

   //assign the new value
   tile->solid = tile->solid | type;

}

bool is_mazetile_wall_oftype ( s_mazetile tile, int side, int type )
{  return ( get_mazetile_wall ( tile, side ) == type );
}


unsigned char get_mazetile_wall ( s_mazetile tile, int side )
{  int i;
   //shift the solid right a number of times according to the direction
   for ( i = 3 ; i > side; i--)
   {  tile.solid = tile.solid >> 2;
   }
   return ( tile.solid & MAZETILE_SOLID_WEST_MASK );
}

//-------------------------- Byte 2 ----------------------------

//-------------------------- Byte 3 ----------------------------

void set_mazetile_special ( s_mazetile *tile, int flag )
{  tile->special = tile->special | flag ;
}

void unset_mazetile_special ( s_mazetile *tile, int flag )
{  tile->special = tile->special & ~flag;
}

bool is_mazetile_special ( s_mazetile tile, int flag )
{  return ( (tile.special & flag) == flag );
}

void set_mazetile_filling ( s_mazetile *tile, int filling )
{  unsigned char special_bits = tile->special & MAZETILE_SPECIAL_MASK;
   tile->special = special_bits + filling;
}

int get_mazetile_filling ( s_mazetile tile )
{  return (tile.special & MAZETILE_FILLING_MASK );
}


//-------------------------- Byte 4 ----------------------------

//-------------------------- Byte 5 ----------------------------

void set_mazetile_palette ( s_mazetile *tile, int paletteid )
{  //Zero the palette value first, then add the new palette number
   tile->wobjpalette = tile->wobjpalette & MAZETILE_WALLOBJIMG_MASK;
   tile->wobjpalette = tile->wobjpalette + paletteid;
}

//-------------------------- Byte 6 ----------------------------

//-------------------------- Byte 7 ----------------------------

//-------------------------- Byte 8 ----------------------------
