/***************************************************************************/
/*                                                                         */
/*                             M A Z E . C P P                             */
/*                                                                         */
/*     Content : Class Maze                                                */
/*     Programmer : Eric PIetrocupo                                        */
/*     Starting Date : March 18th, 2002                                    */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

//#include <screen.h>
//#include <draw.h>
#include <wdatproc.h>
//#include <mazeproc.h>
/*
//#include <time.h>
#include <allegro.h>
#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>


//
//
//
//
//
//#include <list.h>

#include <opponent.h>
#include <charactr.h>
//#include <monster.h>
#include <party.h>

#include <game.h>

//#include <city.h>
#include <maze.h>
#include <editor.h>
#include <encountr.h>
#include <mazeproc.h>
//
//#include <camp.h>
#include <config.h>
#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winquest.h>
#include <winmessa.h>
#include <wintitle.h>
#include <windata.h>
#include <wdatproc.h>
*/


/*-------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                         -*/
/*-------------------------------------------------------------------------*/

Maze::Maze ( void /* maze data ( not file ) */ )
{
   s_mazetile tile;
   //s_Maze_event event;
   short i;
   short j;
   short k;

   //p_dblength = sizeof ( dbs_Maze_data );
   //p_dbtableID = DBTABLE_MAZE;


   p_polytype = POLYTYPE_PTEX_LIT;
   p_mask_polytype = POLYTYPE_PTEX_LIT;
   p_display = Maze_DISPLAY_LAST_ITEM - 1;
   p_transparency = false;
   p_adjust_color = false;

   p_xpos = 0;
   p_ypos = 0;
   p_mapID = 0;



/*   for ( i = 0 ; i < Maze_NB_WARP ; i++ )
      p_warp [ i ]. number ( 0 );*/

   /*for ( i = 0 ; i < Maze_NB_SAVEVALUE ; i++ )
      p_savevalue [ i ] = 0;*/

   /*s_Maze_alteration altempty;

   altempty.xpos = 0;
   altempty.ypos = 0;
   altempty.zpos = 0;
   altempty.savevalueID = 0;
   altempty.change = 0;
   altempty.info.solid = 0;
   //altempty.info.walltex = 0;
   //altempty.info.floortex = 0;
   //altempty.info.objectimg = 0;
   altempty.info.event = 0;
   altempty.info.special = 0;
   //altempty.info.texture = 0;
   altempty.info.objectpic = 0;
   altempty.info.wobjpalette = 0;
   altempty.info.objposition = 0;
   altempty.info.masked = 0;
   altempty.info.maskposition = 0;*/

   /*for ( i = 0 ; i < Maze_NB_ALTERATION ; i++ )
      p_alteration [ i ] = altempty;*/


   p_sky = -1;
   //p_sky = 1; // test code
   strcpy ( p_name, "" );
   strcpy ( p_shortname, "");

   p_rclevel = 0;

   p_music = System_MUSIC_w2dungon;
   p_eprob = Encounter_PROBABILITY_NORMAL;
   p_ebase = 1;
   p_erange = 2;
   p_einc = 2;



   // emptying all the data struct of the maze
   tile = new_mazetile_empty();

   set_mazetile_special( &tile, MAZETILE_SPECIAL_SOLID );

   // maybe remove, set only when loading maze
   for ( k = 0 ; k < Maze_MAXDEPTH; k++ )
      for ( j = 0 ; j < Maze_MAXWIDTH; j++ )
         for ( i = 0 ; i < Maze_MAXWIDTH; i++ )
            mazetile [ k ] [ i ] [ j ] = tile;

   light ( Maze_LIGHT_LEVEL_2 );

}

Maze::~Maze ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                          Property Methods                             -*/
/*-------------------------------------------------------------------------*/

/*string& Maze::name ( void )
{
   return ( p_name );
} */

const char* Maze::name ( void )
{
   return ( p_name );
}

/*string& Maze::shortname ( void )
{
   return ( p_shortname );
} */

const char* Maze::shortname ( void )
{
   return ( p_shortname );
}

short Maze::xpos ( void )
{
   return ( p_xpos );
}

short Maze::ypos ( void )
{
   return ( p_ypos );
}

int Maze::type ( void )
{
   return ( p_type );
}

int Maze::mapID ( void )
{
   return ( p_mapID );
}
/*
int Maze::warp ( int index )
{
   return ( p_warp [ index ] );
}

unsigned int Maze::target ( int index )
{
   return ( p_target [ index ] );
} */

int Maze::eprob ( void )
{
   return ( p_eprob );
}

int Maze::ebase ( void )
{
   return ( p_ebase );
}
int Maze::erange ( void )
{
   return ( p_erange );
}
int Maze::einc ( void )
{
   return ( p_einc );
}


/*int Maze::exitwarptag ( int exitval )
{
   int index;

   index = exitval - Maze_BASE_EXIT;

   return ( p_warp [ index ] . tag );
}*/

void Maze::polytype ( short type )
{
   p_polytype = type;
}

short Maze::polytype ( void )
{
   return ( p_polytype );
}

void Maze::mask_polytype ( short type )
{
   p_mask_polytype = type;
}

short Maze::mask_polytype ( void )
{
   return ( p_mask_polytype );
}

unsigned char Maze::display ( void )
{
   return ( p_display );
}

void Maze::display ( unsigned char value )
{
   p_display = value;
}

void Maze::transparency ( bool value )
{
   p_transparency = value;
}

bool Maze::transparency ( void )
{
   return ( p_transparency );
}

void Maze::adjust_color ( bool value )
{
   p_adjust_color = value;
}

bool Maze::adjust_color ( void )
{
   return ( p_adjust_color );
}

/*s_Party_position Maze::start_position ( void )
{
   return ( p_startpos );
}*/

/*-------------------------------------------------------------------------*/
/*-                            Methods                                    -*/
/*-------------------------------------------------------------------------*/

void Maze::start ( bool demo )
{
   bool maze_interrupt = false;
   int tmp_key = KEY_ESC;
//   unsigned char tmp_solid;
   s_mazetile tmptile;
   bool ouch = false;
   bool draw = true;
   bool eventodo = false;
   s_Party_position tpos;

   bool encountval = false;
   bool checkencounter = false;
   bool dooropened = false;
   WinTitle wttl_ouch ( "Ouch!", 320, 208, true );
   WinTitle wttl_solid ( "Solid!", 320, 240, true );
   Event tmpevent;
   int errorsql;
   bool movein_trigger = true;
   bool face_trigger = true;
   bool check_door_event = false;

//debug_printf( __func__, "Enter Maze Start"); // party bug

   wttl_ouch.hide();
   wttl_solid.hide();

//printf ("Maze debug: start of function\n");
   // temporary

//test code
/*   draw_maze();
   copy_buffer();
   do
            {
               tmp_key = readkey () >> 8;
            }
         while ( tmp_key != KEY_ENTER );*/

//printf ("Debug: after init\r\n");

   //party.SQLselect ( party );

   if ( demo == false)
      WinData<int> wdat_corner_info ( WDatProc_character_gold, party.primary_key(),
                                  WDatProc_POSITION_PARTY_GOLD, true );

   WinData<int> wdat_party_bar ( WDatProc_party_bar, party.primary_key(),
      WDatProc_POSITION_PARTY_BAR, true );

  // debug_printf( __func__, "After windata initialisation");

   //printf ("IS IT HERE! MAZE\n");
//printf ("Debug: after partybar\r\n");

//   int tmplight = Maze_LIGHT_LEVEL_3;
//   light ( tmplight );

// variable used for screenshot

   char shotstr [ 18 ];/*= { "mazshot1.bmp", "mazshot2.bmp", "mazshot3.bmp",
      "mazshot4.bmp", "mazshot5.bmp", "mazshot6.bmp", "mazshot7.bmp", "mazshot8.bmp" };*/
   int shotID = 0;

   if ( demo == false )
      play_music_track ( p_music );

   draw = true;

//printf ("Maze debug: before maze interupt\n");
//printf ("Maze: pass 1\r\n");
   while ( maze_interrupt == false )
   {
//      printf ("Debug: enter main loop\r\n");
//printf ("Maze: pass 2\r\n");
      tpos = party.position ();
      tmptile = mazetile [ tpos.z ] [ tpos.y ] [ tpos.x ] ;

      if ( draw == true )
      {
//         printf ("Maze: pass 3\r\n");

         //if ( demo == true )
           // light (Maze_LIGHT_LEVEL_3 );

//printf ("Debug: before draw maze\r\n");
         Window::refresh_all();
         draw_maze ( demo );
         //rest(1000);
         //debug ( "debug 1");
         //printf ("1\r\n");

         // hide or unhide icons and party bar in the maze
         if ( ( p_display & Maze_DISPLAY_PARTY ) > 0 && demo == false)
            wdat_party_bar.unhide();
         else
            wdat_party_bar.hide();
  /*       if ( ( p_display & Maze_DISPLAY_SPELL ) > 0 && demo == false)
            draw_party_spell ( party );*/
         if ( ( p_display & Maze_DISPLAY_SPECIAL ) > 0 )
            draw_maze_icons ( tmptile );

         if ( demo == false)
            Window::instruction ( 320, 0, Window_INSTRUCTION_MOVE
               + Window_INSTRUCTION_CAMP /*+ Window_INSTRUCTION_SELECT*/ );
         else
            Window::instruction ( 320, 0, Window_INSTRUCTION_MOVE + Window_INSTRUCTION_EXIT );

         if ( ouch == true )
         {
            wttl_ouch.unhide();
//            play_sample ( SMP_sound025, 255, 128, 1000, 0 );
            play_sound ( 1024 ); // Beep
         }

         if ( (tmptile.special & MAZETILE_SPECIAL_SOLID ) == MAZETILE_SPECIAL_SOLID )
            wttl_solid.unhide();
         else
            wttl_solid.hide();

         Window::draw_all();
         //rest(1000);
         //printf ("2\r\n");
         //debug ( "debug 2");
         wttl_ouch.hide();
//         textprintf_old ( mazebuffer, font, 0, 0, General_COLOR_TEXT, "Counter : %d", encount.counter() );
         copy_buffer ();
         //rest(1000);
         //printf ("3\r\n");
         //debug ( "debug 3");
         draw = false;
         /*do
            {
               tmp_key = readkey () >> 8;
            }
         while ( tmp_key != KEY_ENTER );*/
//printf ("Maze debug: after maze drawing\n");
      }

      handmap.write ( tpos );
//uncomment later if necessary
//      Opponent::set_maze_special
//            ( p_data.tile [ tpos.y ] [ tpos.x ] [ tpos.z ] . special );

//printf ("Maze: pass 4\r\n");
      // ---------- check for event ----------
      if ( tmptile.event != MAZETILE_NO_EVENT )
      {

         errorsql = tmpevent.SQLselect ( tmptile.event );
         if ( errorsql == SQLITE_ROW )
         {
            //movein events
            if ( movein_trigger == true )
            {
               if ( tmpevent.trigger() == Event_TRIGGER_MOVEIN  )
               {
                  //if (tmpevent.primary_key() == 15 )
                    // printf ("Debug: Maze: Will start elevator event\n");
                  draw = true;
                  maze_interrupt = tmpevent.start( demo );
                  movein_trigger = false;
               }
            }


            // test for facing events
            if ( face_trigger == true )
            {
               eventodo = false;

//printf ("Maze: Event: left right key detected\n");

               switch ( tmpevent.trigger() )
               {
                  case Event_TRIGGER_FACE_NORTH :
//printf ("Maze: Event: trigger face north detected detected\n");
                     if ( tpos.facing == Party_FACE_NORTH )
                     {
//printf ("Maze: Event: event todo activated\n");

                        eventodo = true;
                     }
                  break;

                  case Event_TRIGGER_FACE_EAST :
                     if ( tpos.facing == Party_FACE_EAST )
                        eventodo = true;
                  break;

                  case Event_TRIGGER_FACE_SOUTH :
                     if ( tpos.facing == Party_FACE_SOUTH )
                        eventodo = true;
                  break;

                  case Event_TRIGGER_FACE_WEST :
                     if ( tpos.facing == Party_FACE_WEST )
                        eventodo = true;
                  break;
               }

               if ( eventodo == true )
               {
//printf ("Maze: Event: Facing event started\n");
                  maze_interrupt = tmpevent.start ( demo );
                  face_trigger = false;
                  draw = true;
               }
            }

            switch ( tmpevent.trigger() )
            {
               case Event_TRIGGER_OPENDOOR_NORTH :
               case Event_TRIGGER_OPENDOOR_EAST :
               case Event_TRIGGER_OPENDOOR_SOUTH :
               case Event_TRIGGER_OPENDOOR_WEST :
               //printf ("Door Trigger detected\n");
                  check_door_event = true;
               break;
            }

         }




         tmp_key = 0;
      }



      // remove keyup from keyboard buffer if hit a wall
      if ( draw == false )
      {
         if ( ouch == true )
            do
            {
               tmp_key = mainloop_readkeyboard();

               if ( tmp_key == -1)
                  draw = true;

            }
            while ( tmp_key == KEY_UP );
         else
         {


            // read next key
            tmp_key = mainloop_readkeyboard();

            if ( tmp_key == -1)
               draw = true;

         }
         clear_keybuf ();
      }
      ouch = false;

      if( tmp_key == KEY_UP )
      {
         if ( get_mazetile_wall( tmptile, party.facing() ) != MAZETILE_SOLIDTYPE_NONE )
            ouch = true;
         else
         {
            party.walk_forward ();
            //game.clock.add_minute ( 1 );
//            party.autoability();
            checkencounter = true;
            movein_trigger = true;
            check_door_event = false;
            //printf ("Debug:maze:I set check door event to false\n");
         }
         draw = true;
      }

      if ( tmp_key == KEY_LEFT )
      {
         party.turn_left();
         face_trigger = true;
         draw = true;
      }

      if ( tmp_key == KEY_RIGHT )
      {
         party.turn_right();
         face_trigger = true;
         draw = true;
      }

/*      if ( tmp_key == DISPLAY_KEY  )
      {
         change_display ();
         draw = true;
      }*/

//printf ("Before Select KEY\n");
      if ( tmp_key == SELECT_KEY )
      {
//printf ("Inside Select KEY\n");
         if ( get_mazetile_wall( tmptile, party.facing() ) == MAZETILE_SOLIDTYPE_DOOR )
         {
//printf ("Inside test front door\n");
            //printf ("Maze: Event: Before check for door\n");
            // test for door open events
            //printf ("There is a front door\n");
            if ( check_door_event == true /*&&
                test_front_door ( tmp_solid ) == true*/)
            {
//printf ("Inside check_door_event\n");
               eventodo = false;
               //printf ("I am checking for door Event\n");

//printf ("Maze: Event: Select Key detected\n");
               switch ( tmpevent.trigger() )
               {
                  case Event_TRIGGER_OPENDOOR_NORTH :
                     if ( tpos.facing == Party_FACE_NORTH )
                        eventodo = true;
                  break;

                  case Event_TRIGGER_OPENDOOR_EAST :
                     if ( tpos.facing == Party_FACE_EAST )
                        eventodo = true;
                  break;

                  case Event_TRIGGER_OPENDOOR_SOUTH :
                     if ( tpos.facing == Party_FACE_SOUTH )
                        eventodo = true;
                  break;

                  case Event_TRIGGER_OPENDOOR_WEST :
//printf ("Maze: Event: DEtected event on west\n");
                     if ( tpos.facing == Party_FACE_WEST )
                     {

//printf ("Maze: Event: party facing is valid\n");
                        eventodo = true;
                     }
                  break;
               }

               if ( eventodo == true )
               {
                  //printf ("debug:maze: I am executing the event\n");
                  maze_interrupt = tmpevent.start ( demo );
                  //eventrigger = false;
                  draw = true;
                  check_door_event = false;
               }
            }
            else
            {
               party.walk_forward ();
//               play_sample ( SMP_sound024, 255, 128, 1000, 0 );
               play_sound ( 1025 ); // door
               checkencounter = true;
               dooropened = true;
               draw = true;
               tmp_key = KEY_UP;
            }
         }
      }

      if ( tmp_key == CANCEL_KEY )
      {
         if ( demo == false )
         {
            maze_interrupt = true;
            party.status (Party_STATUS_CAMP);
         }
      }

      if ( tmp_key == KEY_ESC )
      {
         if ( demo == true )
         {
            maze_interrupt = true;
         }
      }


      if ( tmp_key == KEY_SPACE )
     {
/*         tmplight--;
         if ( tmplight < 0 )
            tmplight = 3;
         light ( tmplight );*/
          sprintf ( shotstr, "mazeshot%03d.bmp", shotID);
         make_screen_shot ( shotstr );
         shotID++;
      }
//printf ("Maze debug: after key reading\n");


//printf ("Maze: pass 5\r\n");

//---------- check for encounter ----------

      if ( checkencounter == true && demo == false)
      {
      // need to do some maintenance
         tpos = party.position (); //note: not sure if need to reread the position
         tmptile = mazetile [ tpos.z ] [ tpos.y ] [ tpos.x ];

         encountval = encount.check_encounter ( tmptile, dooropened );
         if ( encountval == true && encount.nb_enemy() > 0 )
         {
            //printf ("Maze: Start: Maze Interrupted for encounter\n");

            maze_interrupt = true;
            party.status ( Party_STATUS_ENCOUNTER);
         }
      }
      checkencounter = false;
      dooropened = false;

   }

   //draw_maze();
   //copy_buffer();
   //save_backup_screen();
   //clear ( buffer );
   if ( demo == false)
   {

      //debug_printf(__func__, "Before party update");
      party.SQLupdate();
      //debug_printf(__func__, "After party update");

   }
//   stop_midi ();
   //return ( maze_interrupt );

}

s_Party_position Maze::show_entrance ( void )
{
   List lst_entrance ("Which entrance do you want to use?", 5 );
   int nb_entrance;
   s_Party_position tmpos;
   int errorsql;
   Entrance tmpenter;
   int answer;

   tmpos.x = 0;
   tmpos.y = 0;
   tmpos.z = 0;
   tmpos.facing = 0;

   nb_entrance = SQLcount ("name", "entrance", "WHERE unlocked=1");

   if ( nb_entrance > 1)
   {
      lst_entrance.add_query("name","entrance","WHERE unlocked=1");

      WinList wlst_entrance ( lst_entrance, 20, 50, true);
      answer = Window::show_all();

      errorsql = tmpenter.SQLselect ( answer );

      if ( errorsql == SQLITE_ROW )
      {
         tmpos.z = tmpenter.z();
         tmpos.y = tmpenter.y();
         tmpos.x = tmpenter.x();
         tmpos.facing = tmpenter.face();
      }
      else
      {
         WinMessage wmsg_error("ERROR: I cannot select the entrance you have chosen\n using 0, 0, 0, Face North");
         Window::show_all();
      }
   }
   else
      if ( nb_entrance == 1)
      {
         //SQLactivate_errormsg();
         errorsql = tmpenter.SQLprepare ("WHERE unlocked=1");

         if (errorsql == SQLITE_OK)
         {
            errorsql = tmpenter.SQLstep ();

            if ( errorsql == SQLITE_ROW)
            {
               tmpos.z = tmpenter.z();
               tmpos.y = tmpenter.y();
               tmpos.x = tmpenter.x();
               tmpos.facing = tmpenter.face();
            }
            else
            {
               WinMessage wmsg_error("ERROR: I count 1 valid entrance, but cannot select it\n using 0, 0, 0, Face North");
               Window::show_all();
            }

            tmpenter.SQLfinalize();
         }
         else
         {
            WinMessage wmsg_error("ERROR: I count 1 valid entrance, but cannot prepare it\n using 0, 0, 0, Face North");
            Window::show_all();
         }

      }
      else
         if ( nb_entrance == 0)
         {
            WinMessage wmsg_error("ADVENTURE ERROR: No entrance found\n using 0, 0, 0, Face North");
            Window::show_all();
         }

    return ( tmpos );
}


void Maze::light ( int value )
{
   switch ( value )
   {
      case Maze_LIGHT_LEVEL_3 :
//         p_wall_vision = Maze_WALL_VISION_3SPACE;
//         p_floor_vision = Maze_FLOOR_VISION_3SPACE;
         p_darkness = 0;
         p_light = value;
      break;

      case Maze_LIGHT_LEVEL_2 :
//         p_wall_vision = Maze_WALL_VISION_2SPACE;
//         p_floor_vision = Maze_FLOOR_VISION_2SPACE;
         p_darkness = 70;
         p_light = value;
      break;

      case Maze_LIGHT_LEVEL_1 :
//         p_wall_vision = Maze_WALL_VISION_1SPACE;
//         p_floor_vision = Maze_FLOOR_VISION_1SPACE;
         p_darkness = 140;
         p_light = value;
      break;

      case Maze_LIGHT_LEVEL_0 :
//         p_wall_vision = Maze_WALL_VISION_0SPACE;
//         p_floor_vision = Maze_FLOOR_VISION_0SPACE;
         p_darkness = 255;
         p_light = value;
      break;
   }
}

void Maze::reference_bitmap ( void )
{
   //unsigned char i;
   //unsigned char j;
   // will be replaced by new reference bitmap system


   // referencing masked textures
   /*for ( i = 0 ; i < Maze_NB_GAME_MASKTEX ; i++ )
      p_masktex [ i ] = ( BITMAP* ) datfmaze [ BMP_MASKTEX000 + i ] . dat;

   for ( i = 0 ; i < Maze_NB_ADV_MASKTEX ; i++ )
      p_masktex [ Maze_NB_GAME_MASKTEX + i ] = BMP_masktexdum;

   // referencing Object Images
   for ( i = 0 ; i < Maze_NB_GAME_OBJIMG ; i++ )
      p_objimg [ i ] = ( BITMAP* ) datfmaze [ BMP_MAZEOBJ000 + i ] . dat;

   for ( i = 0 ; i < Maze_NB_ADV_OBJIMG ; i++ )
      p_objimg [ Maze_NB_GAME_OBJIMG + i ] = BMP_mazeobjdum;

   // referencing Texture set
   for ( i = 0 ; i < Maze_NB_GAME_TEXSET ; i++ )
      for ( j = 0 ; j < 4 ; j++ )
         p_texset [ i ] [ j ] =
            (BITMAP*) datfmaze [ BMP_TEXSET000 + ( i * 4 ) + j ].dat;

   for ( i = 0 ; i < Maze_NB_ADV_TEXSET ; i++ )
      for ( j = 0 ; j < 4 ; j++ )
         p_texset [ Maze_NB_GAME_TEXSET + i ] [ j ] = BMP_texsetdum;
*/
/*   clear_bitmap ( mazebuffer );
   textout_old ( mazebuffer, font, "Passed Here", 10, 10, General_COLOR_TEXT );
   copy_buffer();
   while ( ( readkey() >> 8 ) != KEY_ENTER );*/
}

void Maze::reference_adv_bitmap ( void )
{
   //unsigned char i;
   //unsigned char j;

   // referencing masked textures
/*   for ( i = 0 ; i < Maze_NB_ADV_MASKTEX ; i++ )
      p_masktex [ i + Maze_NB_GAME_MASKTEX ]
         = ( BITMAP* ) adatf [ BMP_AMASKTEX000 + i ] . dat;

   // referencing Object Images
   for ( i = 0 ; i < Maze_NB_ADV_OBJIMG ; i++ )
      p_objimg [ i + Maze_NB_GAME_OBJIMG ]
         = ( BITMAP* ) adatf [ BMP_AMAZEOBJ000 + i ] . dat;

   // referencing Texture set
   for ( i = 0 ; i < Maze_NB_ADV_TEXSET ; i++ )
      for ( j = 0 ; j < 4 ; j++ )
         p_texset [ i + Maze_NB_GAME_TEXSET ] [ j ] =
            (BITMAP*) adatf [ BMP_ATEXSET000 + ( i * 4 ) + j ] . dat;*/
/*   clear_bitmap ( mazebuffer );
   textout_old ( mazebuffer, font, "Passed there", 10, 10, General_COLOR_TEXT );
   copy_buffer();
   while ( ( readkey() >> 8 ) != KEY_ENTER );*/

}

/*void Maze::reference_target ( int citytaglist [ 16 ], int mazetaglist [ 16 ] )
{
   int i;
   int tmptag;

//   for ( i = 0 ; i < Maze_NB_WARP ; i++ )
//      p_warp [ i ] . tag . number ( 0 );

   for ( i = 0 ; i < Maze_NB_WARP ; i++ )
   {
      if ( p_warp[ i ].target  != - 1 )
      {
         if ( p_warp[ i ].target  < 16  )
         {
            p_warp [ i ].tag = citytaglist [ p_warp [ i ] .target ];
         }
         else
            p_warp [ i ].tag = mazetaglist [ p_warp [ i ] . target - 16 ];
      }
   }
}*/

//void Maze::load ( void )
//{
   /*unsigned char* subptr = static_cast<unsigned char*>( adatf [ MAZ_AMAP ] . dat);
   fs_maz_header header;
   s_mazetile tile;
   int z;
   int y;
   int x;
   short progress;
   short counter;
   short maxcount;

   clear_bitmap(screen);*/

/*   textprintf_old ( mazebuffer, font, 0, 0, General_COLOR_TEXT,
      "Map ID : %d", p_mapID );
   textprintf_old ( mazebuffer, font, 0, 16, General_COLOR_TEXT,
      "Name : %s [ %s ]", p_name, p_shortname );

   copy_buffer();
   while ( ( readkey()>> 8 ) != KEY_ENTER );*/

   /*if ( subptr != NULL )
   {
      show_loading_screen ( p_name, 1 );
      memcpy ( &header, subptr, sizeof ( fs_maz_header ) );
      subptr = subptr + ( sizeof ( fs_maz_header ) );

      p_width = header.f_width;
      p_depth = header.f_depth;
      p_sky = header.f_sky;
      p_rclevel = header.f_rclevel;
      p_startpos.x = header.f_startx;
      p_startpos.y = header.f_starty;
      p_startpos.z = header.f_startz;
      p_startpos.facing = header.f_startface;

      progress = 0;
      counter = 0;
      maxcount = ( p_width * p_width * p_depth ) / 500;

      for ( z = 0 ; z < p_depth ; z++ )
         for ( y = 0 ; y < p_width ; y++ )
            for ( x = 0 ; x < p_width ; x++ )
            {
               memcpy ( &tile, subptr, sizeof ( s_mazetile ) );
               subptr = subptr + ( sizeof ( s_mazetile ) );

               mazetile [ z ] [ y ] [ x ] = tile;

               counter++;
               if ( counter >= maxcount )
               {
                  counter = 0;
                  progress++;
               }
               rectfill ( subscreen, 70, 256 + 1, 70 + ( progress ), 256 + 9,
                                                 makecol ( 150, 150, 255 ) );

//               show_loading_screen ( maze.name(), progress );
            }
      clear_bitmap ( screen );
*/
      // loading event

/*      Database tmpdb;
      unsigned int index;
      dbs_Maze_event tmpevent;
      short i = 0;

      tmpdb.load_dba_from_dat ( DBA_aevent, DBSOURCE_TEMP );

      index = tmpdb.search_table_entry ( p_mapID + 1 );

      clear_bitmap ( mazebuffer );
      while ( tmpdb.entry_table_tag ( index ) == ( p_mapID + 1 ) )
      {
         tmpdb.select ( &tmpevent, index, sizeof ( tmpevent ) );

         p_event [ i ].type = tmpevent.type;
         p_event [ i ].trigger = tmpevent.trigger;
         p_event [ i ].var [ 0 ] = tmpevent.var [ 0 ];
         p_event [ i ].var [ 1 ] = tmpevent.var [ 1 ];
         p_event [ i ].var [ 2 ] = tmpevent.var [ 2 ];
         p_event [ i ].var [ 3 ] = tmpevent.var [ 3 ];
         p_event [ i ].savevalID = tmpevent.savevalID;
         p_event [ i ].text = tmpevent.text;
         strcpy ( p_event [ i ].title, tmpevent.title );
         strcpy ( p_event [ i ].message, tmpevent.message );
         index++;
         i++;
      }
*/
/*      copy_buffer();
      while ( ( readkey() >> 8 ) != KEY_ENTER );*/



   //}
//}

/*void Maze::load_hardcoded (void)
{
   s_mazetile tile;
  // s_Maze_event event;


   // temporary hardcoded maze

   strcpy ( p_name ,"Wizardry Demo");
   strcpy ( p_shortname , "Wiz Demo");

   load_hardcoded_texture();

   tile.solid = WWEST + WSOUTH ;
   //tile.floortex = 38;
   //tile.objectimg = 63;
   tile.event = 1; // maze exit
   //tile.texture = 0 + Maze_FLOOR_MTEX_UP +  Maze_OBJECT_CENTER;
   tile.special = 0;
   tile.objectpic = 0;
   tile.wobjpalette = 0;
   tile.objposition = 0;
   tile.masked = 0;
   tile.maskposition = 0;
   mazetile [ 0 ] [ 0 ] [ 0 ] = tile;
   //tile.texture = 0;
   tile.event = 0;
   //tile.floortex = 0;
   //tile.objectimg = 0;


   tile.solid = WSOUTH;
   tile.event = 14;
   mazetile [ 0 ] [ 0 ] [ 1 ] = tile;
   tile.event = 0;

   tile.solid = WSOUTH + WNORTH + WEAST;
   mazetile [ 0 ] [ 0 ] [ 2 ] = tile;

   tile.solid = WWEST + WEAST;
   tile.event = 4;
   mazetile [ 0 ] [ 1 ] [ 0 ] = tile;
   tile.event = 0;

   tile.solid = WWEST + WEAST + WNORTH;
   //tile.floortex = 5;
   //tile.objectimg = 5;
   //tile.walltex = 21;
   //tile.texture = 0 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_4CORNER + Maze_WALL_MTEX_NORTH;

   tile.masked = 0 + 2;
   tile.maskposition = 0 + MAZETILE_MASKTEXPOS_CEILING + MAZETILE_MASKTEXPOS_FLOOR + MAZETILE_MASKTEXPOS_WEST
      + MAZETILE_MASKTEXPOS_EAST;

   mazetile [ 0 ] [ 1 ] [ 1 ] = tile;
   //tile.floortex = 0;
   //tile.objectimg = 0;
   //tile.texture = 0;
   tile.masked = 0;
   tile.maskposition = 0;


   tile.solid = WWEST + WEAST;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.walltex = 3;
   //tile.floortex = 34;
   //tile.texture = 0 + Maze_FLOOR_MTEX_DOWN + Maze_WALL_MTEX_WEST
   //   + Maze_WALL_MTEX_EAST;
   mazetile [ 0 ] [ 2 ] [ 0 ] = tile;
   tile.special = 0;

   tile.solid = WWEST + WEAST + DNORTH;
   //tile.objectimg = 0;
   //tile.texture = 0 + Maze_OBJECT_TWINN;
   tile.event = 5;
   mazetile [ 0 ] [ 3 ] [ 0 ] = tile;
   //tile.texture = 2;
   tile.event = 0;

   tile.solid = WWEST + WEAST + DSOUTH;
   tile.event = 6;
   mazetile [ 0 ] [ 4 ] [ 0 ] = tile;
   tile.event = 0;

   tile.solid = WWEST + WNORTH;
   //tile.walltex = 19;
   //tile.texture = 2 + Maze_WALL_MTEX_WEST;
   tile.event = 7;
   mazetile [ 0 ] [ 5 ] [ 0 ] = tile;
   tile.event = 0;
   //tile.walltex = 0;
   //tile.texture = 2;

   tile.solid = WNORTH + WSOUTH;
   //tile.floortex = 35;
   //tile.texture = 2 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_CENTER;
   //tile.objectimg = 54;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   mazetile [ 0 ] [ 5 ] [ 1 ] = tile;
   tile.special = 0;
   //tile.floortex = 0;
   //tile.texture = 2;


   tile.solid = WNORTH + WSOUTH;
   mazetile [ 0 ] [ 5 ] [ 2 ] = tile;

   tile.solid = WNORTH + WSOUTH + WWEST;
   tile.event = 8;
   mazetile [ 0 ] [ 5 ] [ 3 ] = tile;
   tile.event = 0;

   tile.solid = WNORTH + WSOUTH;
   mazetile [ 0 ] [ 5 ] [ 4 ] = tile;

   tile.solid = WSOUTH + WEAST;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.objectimg = 6;
   //tile.floortex = 34;
   //tile.texture = 2 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_TWINN;
   mazetile [ 0 ] [ 5 ] [ 5 ] = tile;
   tile.special = 0;
   //tile.floortex = 0;
   //tile.texture = 2;

   tile.solid = WWEST + WEAST;
   tile.event = 9;
   mazetile [ 0 ] [ 6 ] [ 5 ] = tile;
   tile.event = 0;

      // rotator room
   tile.solid = WWEST + WSOUTH;
   //tile.texture = 3;
   mazetile [ 0 ] [ 7 ] [ 4 ] = tile;

   tile.solid = WSOUTH;
   mazetile [ 0 ] [ 7 ] [ 5 ] = tile;


   tile.solid = WEAST + WSOUTH;
   mazetile [ 0 ] [ 7 ] [ 6 ] = tile;

   tile.solid = 0;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.floortex = 37;
   //tile.objectimg = 8;
   //tile.texture = 3 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_CENTER;
   mazetile [ 0 ] [ 8 ] [ 4 ] = tile;
   tile.special = 0;
   //tile.floortex = 0;
   //tile.texture = 3;

   tile.solid = 0;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.objectimg = 5;
   tile.event = 3;
   //tile.texture = 3 + Maze_OBJECT_WEST;
   mazetile [ 0 ] [ 8 ] [ 5 ] = tile;
   tile.special = 0;
   //tile.objectimg = 0;
   //tile.texture = 3;
   tile.event = 0;

   tile.solid = WEAST;
   mazetile [ 0 ] [ 8 ] [ 6 ] = tile;

   tile.solid = WWEST + WNORTH;
   mazetile [ 0 ] [ 9 ] [ 4 ] = tile;

   tile.solid = GNORTH;
   //tile.walltex = 42;
   mazetile [ 0 ] [ 9 ] [ 5 ] = tile;
   //tile.walltex = 0;

   tile.solid = WEAST + WNORTH;
   mazetile [ 0 ] [ 9 ] [ 6 ] = tile;

   tile.solid = DNORTH + WWEST + WEAST + GSOUTH;
   //tile.walltex = 42;
   mazetile [ 0 ] [ 10 ] [ 5 ] = tile;
   //tile.walltex = 0;

      // end rotator room

   //tile.texture = 5;
   tile.solid = WSOUTH + WNORTH;
   mazetile [ 0 ] [ 8 ] [ 3 ] = tile;

   tile.solid = WSOUTH + WNORTH;
   mazetile [ 0 ] [ 8 ] [ 2 ] = tile;

   tile.solid = WSOUTH + WWEST;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.floortex = 34;
   //tile.walltex = 3;
   //tile.texture = 5 + Maze_FLOOR_MTEX_DOWN + Maze_WALL_MTEX_SOUTH
   //   + Maze_WALL_MTEX_WEST;
   mazetile [ 0 ] [ 8 ] [ 1 ] = tile;
   tile.special = 0;
   //tile.floortex = 0;
   //tile.texture = 5;

   tile.solid = WWEST + WEAST;
   tile.event = 10;
   mazetile [ 0 ] [ 9 ] [ 1 ] = tile;
   tile.event = 0;

      // poison gas room

   //tile.texture = 9;
   tile.solid = WWEST + WSOUTH;
   tile.special = MAZETILE_FILLING_POISON_GAS;
   mazetile [ 0 ] [ 10 ] [ 0 ]  = tile;

   tile.solid = WSOUTH;
   mazetile [ 0 ] [ 10 ] [ 1 ]  = tile;

   tile.solid = WSOUTH + WEAST;
   mazetile [ 0 ] [ 10 ] [ 2 ]  = tile;

   tile.solid = WWEST;
   mazetile [ 0 ] [ 11 ] [ 0 ]  = tile;

   tile.solid = 0;
   mazetile [ 0 ] [ 11 ] [ 1 ]  = tile;

   tile.solid = WEAST;
   mazetile [ 0 ] [ 11 ] [ 2 ]  = tile;

   tile.solid = WWEST + WNORTH;
   mazetile [ 0 ] [ 12 ] [ 0 ]  = tile;

   tile.solid = 0;
   tile.special = MAZETILE_FILLING_POISON_GAS + MAZETILE_SPECIAL_LIGHT;
   //tile.floortex = 34;
   //tile.objectimg = 54;
   //tile.texture = 9 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_CENTER;
   mazetile [ 0 ] [ 12 ] [ 1 ]  = tile;
   tile.special = MAZETILE_FILLING_POISON_GAS;
   //tile.floortex = 0;
   //tile.texture = 9;

   tile.solid = WNORTH + WEAST;
   mazetile [ 0 ] [ 12 ] [ 2 ]  = tile;
   tile.special = 0;
      // end of poison gas room

   //tile.texture = 7;
   tile.solid = WWEST + WEAST;
   mazetile [ 0 ] [ 13 ] [ 1 ]  = tile;

   tile.solid = WWEST + WNORTH;
   mazetile [ 0 ] [ 14 ] [ 1 ]  = tile;

   tile.solid = WNORTH + WSOUTH;
   mazetile [ 0 ] [ 14 ] [ 2 ]  = tile;

   tile.solid = WNORTH + WSOUTH;
   tile.event = 11;
   mazetile [ 0 ] [ 14 ] [ 3 ]  = tile;
   tile.event = 0;

      // dark corridor
   tile.solid = WNORTH + WSOUTH;
   tile.special = MAZETILE_FILLING_DARKNESS;
   mazetile [ 0 ] [ 14 ] [ 4 ]  = tile;

   tile.solid = WNORTH + WSOUTH;
   mazetile [ 0 ] [ 14 ] [ 5 ]  = tile;

   tile.solid = WNORTH + WSOUTH;
   mazetile [ 0 ] [ 14 ] [ 6 ]  = tile;
   tile.special = 0;
      // end of dark corridor

   tile.solid = WNORTH + WSOUTH;
   //tile.texture = 1;
   mazetile [ 0 ] [ 14 ] [ 7 ]  = tile;

   tile.solid = WNORTH + WEAST;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.floortex = 36;
   //tile.objectimg = 53;
   //tile.texture = 1 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_4CORNER;
   mazetile [ 0 ] [ 14 ] [ 8 ]  = tile;
   tile.special = 0;
   //tile.floortex = 0;
   //tile.texture = 1;


   tile.solid = WWEST + WEAST;
   tile.event = 12;
   mazetile [ 0 ] [ 13 ] [ 8 ]  = tile;
   tile.event = 0;

      // special xyz room

   tile.solid = WWEST + WNORTH;
   //tile.floortex = 24;
   //tile.walltex = 17;
   //tile.texture = 1 +  Maze_FLOOR_MTEX_DOWN + Maze_WALL_MTEX_WEST;
   mazetile [ 0 ] [ 12 ] [ 7 ]  = tile;
   //tile.texture = 1 + Maze_FLOOR_MTEX_DOWN;

   tile.solid = WNORTH;
   mazetile [ 0 ] [ 12 ] [ 8 ]  = tile;

   tile.solid = WEAST + WNORTH;
   //tile.walltex = 33;
   //tile.texture = 1 +  Maze_FLOOR_MTEX_DOWN + Maze_WALL_MTEX_NORTH;
   mazetile [ 0 ] [ 12 ] [ 9 ]  = tile;
   //tile.floortex = 0;
   //tile.texture = 1;

   tile.solid = DWEST;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.floortex = 37;
   //tile.objectimg = 52;
   //tile.texture = 1 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_TWINW;
   mazetile [ 0 ] [ 11 ] [ 7 ]  = tile;
   tile.special = 0;
   //tile.floortex = 0;
   //tile.texture = 1;


   tile.solid = 0;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.floortex = 10;
   //tile.objectimg = 10;
   //tile.texture = 1 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_CENTER;
   mazetile [ 0 ] [ 11 ] [ 8 ]  = tile;
   tile.special = 0;
   //tile.floortex = 0;
   //tile.objectimg = 0;
   //tile.texture = 1;


   tile.solid = WEAST;
   //tile.floortex = 24;
   //tile.walltex = 17;
   //tile.texture = 1 +  Maze_FLOOR_MTEX_DOWN + Maze_WALL_MTEX_EAST;
   mazetile [ 0 ] [ 11 ] [ 9 ]  = tile;
   //tile.texture = 1 +  Maze_FLOOR_MTEX_DOWN;

   tile.solid = WWEST + WSOUTH;
   //tile.walltex = 33;
   //tile.texture = 1 +  Maze_FLOOR_MTEX_DOWN + Maze_WALL_MTEX_WEST;
   mazetile [ 0 ] [ 10 ] [ 7 ]  = tile;

   tile.solid = WSOUTH;
   //tile.walltex = 17;
   //tile.texture = 1 +  Maze_FLOOR_MTEX_DOWN + Maze_WALL_MTEX_SOUTH;
   mazetile [ 0 ] [ 10 ] [ 8 ]  = tile;
   //tile.texture = 1 +  Maze_FLOOR_MTEX_DOWN;

   tile.solid = WEAST + WSOUTH;
   //tile.walltex = 33;
   //tile.texture = 1 +  Maze_FLOOR_MTEX_DOWN + Maze_WALL_MTEX_SOUTH
   //   + Maze_WALL_MTEX_EAST;

   mazetile [ 0 ] [ 10 ] [ 9 ]  = tile;
   //tile.floortex = 0;
   //tile.texture = 1;

      // end of special xyz room

   //tile.texture = 8;
   tile.solid = WSOUTH + WNORTH + DEAST;
   mazetile [ 0 ] [ 11 ] [ 6 ]  = tile;

   tile.solid = WSOUTH + WNORTH;
   tile.event = 13;
   mazetile [ 0 ] [ 11 ] [ 5 ]  = tile;
   tile.event = 0;

   tile.solid = WSOUTH + WWEST + WNORTH;
   //tile.floortex = 1;
   tile.special = MAZETILE_SPECIAL_LIGHT;
   //tile.objectimg = 45;
   tile.event = 2; // teleporter
   //tile.walltex = 2;
   //tile.texture = 8 + Maze_FLOOR_MTEX_DOWN + Maze_OBJECT_CENTER +
   //   Maze_WALL_MTEX_NORTH + Maze_WALL_MTEX_SOUTH + Maze_WALL_MTEX_WEST;
   mazetile [ 0 ] [ 11 ] [ 4 ]  = tile;
   //tile.floortex = 0;
   //tile.objectimg = 0;
   //tile.texture = 8;
   //tile.walltex = 0;

   // hardcoded events

   event.type = Maze_EVENT_EXIT;
   strcpy(event.title, "Ladder Up");
   strcpy ( event.message, "There is a ladder going up\nDo You want to use it ?");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK + Maze_EVENT_TRIGICON_UP;
   p_event [ 1 ] = event;

   event.type = Maze_EVENT_TELEPORTER;
   //event.title = "";
   //event.message = "";
   event.text = 0;
   event.var [ Maze_EVTPORT_XPOS ] = 2;
   event.var [ Maze_EVTPORT_YPOS ] = 0;
   event.var [ Maze_EVTPORT_ZPOS ] = 0;
   event.var [ Maze_EVTPORT_FACE ] = Maze_FACE_WEST;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK + Maze_EVENT_TRIGICON_DOWN;
   p_event [ 2 ] = event;

   event.type = Maze_EVENT_ROTATOR;
   //event.title = "";
   //event.message = "";
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK + Maze_EVENT_TRIGICON_DOWN;
   p_event [ 3 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy(event.title, "Divine Message 1");
   strcpy ( event.message , "Welcome to the Wizardry legacy demo maze\nWalk foward and follow instructions $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 4 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 2");
   strcpy ( event.message, "This is a door. You can open doors\nby pressing the SELECT button $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 5 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 3");
   strcpy ( event.message, "Move foward and turn left and you will be able\nto read the message on the left wall $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 6 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title , "Wall Inscription");
   strcpy ( event.message , "The floor arrows will guide you through this maze $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_FACEWEST;
   p_event [ 7 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 4");
   strcpy ( event.message, "If you turn around, you will realise that you\nhave crossed a one way wall. Watch out!\ncontinue and follow the arrows. $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 8 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 5");
   strcpy ( event.message, "In the middle of the room, there is a rotator.\nThe chain will help you to know which way you have turned. $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 9 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 6");
   strcpy ( event.message, "The room ahead is filled with poison gas.\nWalk through the room. $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 10 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 7");
   strcpy ( event.message, "The corridor ahead is filled with magikal darkness\nJust move constantly foward and you'll reach \n the end of the corridor $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 11 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 9");
   strcpy ( event.message, "There is currently nothing interesting in the room.\ncontinue to the right. $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 12 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 10");
   strcpy ( event.message, "If you walk foward, you will enter a\nteleportation device that will send you\nelsewhere in the maze. $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 13 ] = event;

   event.type = Maze_EVENT_MESSAGE;
   strcpy ( event.title, "Divine Message 11");
   strcpy ( event.message, "The demo is over. Walk foward to exit the maze $ allo");
   event.text = 0;
   event.var [ 0 ] = 0;
   event.var [ 1 ] = 0;
   event.var [ 2 ] = 0;
   event.var [ 3 ] = 0;
   event.savevalID = 0;
   event.trigger = Maze_EVENT_TRIGGER_WALK;
   p_event [ 14 ] = event;


}*/

void Maze::load_hardcoded_texture (void)
{
   //Texture tmptex;

 // preload hardcoded palette

   // floor

   /*mazepal.tex [ TexPal_IDX_FLOOR ].texcode ( 0, 1026 );
   mazepal.tex [ TexPal_IDX_FLOOR ].texposition ( 0, Texture_TILING_BOTH + Texture_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_FLOOR] = tmptex.build();

   mazepal.tex [ TexPal_IDX_CEILING ].texcode ( 0, 1100 );
   mazepal.tex [ TexPal_IDX_CEILING ].texposition ( 0, Texture_TILING_BOTH + Texture_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_CEILING] = tmptex.build();

   mazepal.tex [ TexPal_IDX_MFLOOR ].texcode ( 0, 1083 );
   mazepal.tex [ TexPal_IDX_MFLOOR ].texposition ( 0, Texture_ZOOM_X4, 0, 0 );
   //texpal.tex [ TexPal_IDX_MFLOOR] = tmptex.build();

   mazepal.tex [ TexPal_IDX_MFLOOR + 1].texcode ( 0, 1094 );
   mazepal.tex [ TexPal_IDX_MFLOOR + 1].texposition ( 0, Texture_TILING_BOTH + Texture_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_MFLOOR + 1] = tmptex.build();

   mazepal.tex [ TexPal_IDX_MFLOOR + 2].texcode ( 0, 1128 );
   mazepal.tex [ TexPal_IDX_MFLOOR + 2].texposition ( 0, Texture_TILING_BOTH, 0, 0 );
   //texpal.tex [ TexPal_IDX_MFLOOR + 2] = tmptex.build();

   // wall

   mazepal.tex [ TexPal_IDX_DOOR ].texcode ( 0, 2246 );
   mazepal.tex [ TexPal_IDX_DOOR ].texposition ( 0, Texture_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_DOOR] = tmptex.build( true );

   mazepal.tex [ TexPal_IDX_GRID ].texcode ( 0, 2234 );
   mazepal.tex [ TexPal_IDX_GRID ].texposition ( 0, Texture_TILING_BOTH + Texture_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_GRID] = tmptex.build();

   mazepal.tex [ TexPal_IDX_WALL ].texcode ( 0, 2072 );
   mazepal.tex [ TexPal_IDX_WALL ].texposition ( 0, Texture_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_WALL ] = tmptex.build();
   //texpal.tex [ TexPal_IDX_WALL + 1 ] = tmptex.build();
   //texpal.tex [ TexPal_IDX_WALL + 2 ] = tmptex.build();
   //texpal.tex [ TexPal_IDX_WALL + 3 ] = tmptex.build();

   //tmptex.texcode ( 0, 2117 );
   //tmptex.texposition ( 0, TexPalette_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_WALL + 1 ] = tmptex.build();

   mazepal.tex [ TexPal_IDX_MWALL ].texcode ( 0, 2198 );
   mazepal.tex [ TexPal_IDX_MWALL ].texposition ( 0, Texture_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_MWALL ] = tmptex.build();

   mazepal.tex [ TexPal_IDX_MWALL + 1 ].texcode ( 0, 2195 );
   mazepal.tex [ TexPal_IDX_MWALL + 1 ].texposition ( 0, Texture_ZOOM_X2, 0, 0 );
   //texpal.tex [ TexPal_IDX_MWALL + 1 ] = tmptex.build();

   mazepal.tex [ TexPal_IDX_MWALL + 2 ].texcode ( 0, 2223 );
   mazepal.tex [ TexPal_IDX_MWALL + 2 ].texposition ( 0, Texture_ZOOM_X2, 4, 0 );
   //texpal.tex [ TexPal_IDX_MWALL + 2 ] = tmptex.build();

   mazepal.build();
*/
   // temporary palette display

   /*draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_MWALL ], 0, 0 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_MWALL + 2 ], 256, 0 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_MWALL + 1 ], 512, 0 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_MFLOOR  ], 0, 256 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_MFLOOR + 1 ], 256, 256 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_MFLOOR + 2 ], 512, 256 );
*/

/*texpal.tex [ TexPal_IDX_WALL ], 0, 0 );
   draw_sprite ( screen, texpal.tex [ TexPal_IDX_FLOOR ], 256, 0 );
   draw_sprite ( screen, texpal.tex [ TexPal_IDX_CEILING ], 512, 0 );
   draw_sprite ( screen, texpal.tex [ TexPal_IDX_DOOR ], 0, 256 );
   draw_sprite ( screen, texpal.tex [ TexPal_IDX_GRID ]*/

  /* while ( mainloop_readkeyboard() != KEY_ENTER );

   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_WALL ], 0, 0 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_FLOOR  ], 256, 0 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_CEILING  ], 512, 0 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_DOOR  ], 0, 256 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_GRID  ], 256, 256 );
   draw_sprite ( screen, mazepal.bmp [ TexPal_IDX_MFLOOR + 2 ], 512, 256 );

   while ( mainloop_readkeyboard() != KEY_ENTER );
*/
   /*#define TexPal_IDX_WALL       0  // 0-3
#define TexPal_IDX_GRID       4  // 4-7
#define TexPal_IDX_MWALL      8  // 8-23
#define TexPAl_IDX_MFLOOR     24 // 24-39
#define TexPal_IDX_FLOOR      40
#define TexPal_IDX_CEILING    41
#define TexPal_IDX_DOOR       42*/


}

bool Maze::save_to_mazefile ( const char* filename )
{
   FILE *savefile;
   s_Editor_maze_header header;
   int x;
   int y;
   int z;
   bool retval = false;

   // setup header, this is only there for compatibility. Not used in the game.
   header.version = 1;
   header.width = Maze_MAXWIDTH;
   header.depth = Maze_MAXDEPTH;
   header.reserved = 0;

   savefile = fopen (filename, "wb");

   if (savefile != NULL)
   {
      fwrite ( &header, sizeof (s_Editor_maze_header), 1, savefile);

      for ( z = 0; z < Maze_MAXDEPTH; z++ )
         for ( y = 0; y < Maze_MAXWIDTH; y++)
            for ( x = 0; x < Maze_MAXWIDTH; x++)
               fwrite ( &mazetile [z][y][x], sizeof (s_mazetile), 1, savefile );

      retval = true;
      fclose ( savefile );
   }


   return (retval);
}

bool Maze::load_from_mazefile ( const char* filename )
{

   FILE *loadfile;
   s_Editor_maze_header header;
   int x;
   int y;
   int z;
   bool retval = false;
   //int dummy;

   loadfile = fopen (filename, "rb");

   if (loadfile != NULL)
   {
      fread ( &header, sizeof (s_Editor_maze_header), 1, loadfile);

      for ( z = 0; z < Maze_MAXDEPTH; z++ )
         for ( y = 0; y < Maze_MAXWIDTH; y++)
            for ( x = 0; x < Maze_MAXWIDTH; x++)
               fread ( &mazetile [z][y][x], sizeof (s_mazetile), 1, loadfile );

      retval = true;
      fclose ( loadfile );
   }

   return (retval);
}

bool Maze::load_from_adventure ( const char* adventurefile )
{
   s_Editor_maze_header header;
   DATAFILE *tmpobj;
   unsigned char *subptr;
   s_mazetile tile;
   int z;
   int y;
   int x;
   bool retval = false;
   char tmpstr [128];
   //FILE *fileptr;

   sprintf (tmpstr, "adventure//%s", adventurefile);
   tmpobj = load_datafile_object ( tmpstr, "MAZE_BIN");

   if ( tmpobj != NULL )
   {
       subptr = static_cast<unsigned char*>(tmpobj -> dat); // convert as generic pointer
      // read the header part, values are not considered so far.
      memcpy ( &header, subptr, sizeof ( s_Editor_maze_header ) );
      subptr = subptr + ( sizeof ( s_Editor_maze_header ) );
         // increment pointer to the maze data.

      for ( z = 0 ; z < Maze_MAXDEPTH ; z++ )
         for ( y = 0 ; y < Maze_MAXWIDTH ; y++ )
            for ( x = 0 ; x < Maze_MAXWIDTH ; x++ )
            {
               memcpy ( &tile, subptr, sizeof ( s_mazetile ) );
               subptr = subptr + ( sizeof ( s_mazetile ) );

               mazetile [ z ] [ y ] [ x ] = tile;
            }


   load_hardcoded_texture();
      retval = true;
      unload_datafile_object (tmpobj);
   }


 /*unsigned char* subptr = static_cast<unsigned char*>( adatf [ MAZ_AMAP ] . dat);
   fs_maz_header header;
   s_mazetile tile;
   int z;
   int y;
   int x;
   short progress;
   short counter;
   short maxcount;

   clear_bitmap(screen);*/

/*   textprintf_old ( mazebuffer, font, 0, 0, General_COLOR_TEXT,
      "Map ID : %d", p_mapID );
   textprintf_old ( mazebuffer, font, 0, 16, General_COLOR_TEXT,
      "Name : %s [ %s ]", p_name, p_shortname );

   copy_buffer();
   while ( ( readkey()>> 8 ) != KEY_ENTER );*/

   /*if ( subptr != NULL )
   {
      show_loading_screen ( p_name, 1 );
      memcpy ( &header, subptr, sizeof ( fs_maz_header ) );
      subptr = subptr + ( sizeof ( fs_maz_header ) );

      p_width = header.f_width;
      p_depth = header.f_depth;
      p_sky = header.f_sky;
      p_rclevel = header.f_rclevel;
      p_startpos.x = header.f_startx;
      p_startpos.y = header.f_starty;
      p_startpos.z = header.f_startz;
      p_startpos.facing = header.f_startface;

      progress = 0;
      counter = 0;
      maxcount = ( p_width * p_width * p_depth ) / 500;

      for ( z = 0 ; z < p_depth ; z++ )
         for ( y = 0 ; y < p_width ; y++ )
            for ( x = 0 ; x < p_width ; x++ )
            {
               memcpy ( &tile, subptr, sizeof ( s_mazetile ) );
               subptr = subptr + ( sizeof ( s_mazetile ) );

               mazetile [ z ] [ y ] [ x ] = tile;

               counter++;
               if ( counter >= maxcount )
               {
                  counter = 0;
                  progress++;
               }
               rectfill ( subscreen, 70, 256 + 1, 70 + ( progress ), 256 + 9,
                                                 makecol ( 150, 150, 255 ) );

//               show_loading_screen ( maze.name(), progress );
            }
      clear_bitmap ( screen );
*/
      // loading event

/*      Database tmpdb;
      unsigned int index;
      dbs_Maze_event tmpevent;
      short i = 0;

      tmpdb.load_dba_from_dat ( DBA_aevent, DBSOURCE_TEMP );

      index = tmpdb.search_table_entry ( p_mapID + 1 );

      clear_bitmap ( mazebuffer );
      while ( tmpdb.entry_table_tag ( index ) == ( p_mapID + 1 ) )
      {
         tmpdb.select ( &tmpevent, index, sizeof ( tmpevent ) );

         p_event [ i ].type = tmpevent.type;
         p_event [ i ].trigger = tmpevent.trigger;
         p_event [ i ].var [ 0 ] = tmpevent.var [ 0 ];
         p_event [ i ].var [ 1 ] = tmpevent.var [ 1 ];
         p_event [ i ].var [ 2 ] = tmpevent.var [ 2 ];
         p_event [ i ].var [ 3 ] = tmpevent.var [ 3 ];
         p_event [ i ].savevalID = tmpevent.savevalID;
         p_event [ i ].text = tmpevent.text;
         strcpy ( p_event [ i ].title, tmpevent.title );
         strcpy ( p_event [ i ].message, tmpevent.message );
         index++;
         i++;
      }
*/
/*      copy_buffer();
      while ( ( readkey() >> 8 ) != KEY_ENTER );*/



   //}

   return (retval);
}

/*-------------------------------------------------------------------------*/
/*-                         Private Methods                               -*/
/*-------------------------------------------------------------------------*/

//---------------------------- Maze Drawing ---------------------------------

void Maze::draw_wall ( s_Maze_wall_info &wallinfo, BITMAP *texture, bool flipodd )
{

   int i;
   V3D tmpv [ 4 ];
   //fixed texu;
   //fixed texv;
   int tmpval;
   //bool flip = false;
   int tmpx;
   int tmpy;
   //int tmpcolor;
   //int swpcolor = 1<<31;


   /*if ( wallinfo.flipok == true )
   {
      if ( flipodd == true )
         flip = true;
   }*/

   for ( i = 0 ; i < 4 ; i++ )
   {
      tmpv [ i ] = wallinfo.vertex [ i ];

      tmpx = LIGHT_DISTANCE [ wallinfo.ltile ] . tablex;
      tmpy = LIGHT_DISTANCE [ wallinfo.ltile ] . tabley;
      tmpv [ i ].c = p_light_table [ tmpy ] [ tmpx ]
         [ wallinfo.lvtex [ i ] ];
      //tmpv [ i ].x += System_X_OFFSET<<16;
      //tmpv [ i ].y += System_Y_OFFSET<<16;

      //printf ("tmpval=%d\n", tmpv[i].c);

      if ( p_adjust_color == true )
      {
         if ( p_polytype == POLYTYPE_FLAT )
         {
            if ( tmpv [ i ] . c != 0 )
               tmpval = makecol ( 150, 150, 150); //( tmpv [ i ] . c  / 4 );// + 96;
            else
               tmpval = 0;
            //tmpcolor = makecol ( tmpval, tmpval, tmpval );
            //if ( tmpcolor > swpcolor )
            //   swpcolor = tmpcolor;
            tmpv [ i ] . c = tmpval;

         }
         else
         {
            tmpval = ( tmpv [ i ] . c );

            if ( tmpval < 0 )
               tmpval = 0;
            tmpv [ i ] . c = makecol24 ( tmpval, tmpval, tmpval );
         }
      }
   }

   // color adjustment : color decay uniformisation
   /*if ( p_adjust_color == true && p_polytype == POLYTYPE_FLAT)
   {
      for ( i = 0 ; i < 4 ; i++ )
         tmpv [ i ] . c = swpcolor;
   }*/


   /*if ( flip == true )
   {
      // switch texture cordonates for vertex 0 and 3
      texu = tmpv [ 0 ] . u;
      texv = tmpv [ 0 ] . v;
      tmpv [ 0 ] . u = tmpv [ 3 ] . u;
      tmpv [ 0 ] . v = tmpv [ 3 ] . v;
      tmpv [ 3 ] . u = texu;
      tmpv [ 3 ] . v = texv;

      // switch texture cordonates for vertex 1 and 2
      texu = tmpv [ 1 ] . u;
      texv = tmpv [ 1 ] . v;
      tmpv [ 1 ] . u = tmpv [ 2 ] . u;
      tmpv [ 1 ] . v = tmpv [ 2 ] . v;
      tmpv [ 2 ] . u = texu;
      tmpv [ 2 ] . v = texv;
   }*/

   //if ( tmpv[0].c != 0 || tmpv[1].c != 0 || tmpv[2].c != 0 || tmpv[3].c != 0 )
   //{


      quad3d ( mazebuffer, p_polytype, texture, &tmpv[ 0 ],
                           &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );
      if ( p_adjust_color == true )
      {
         line ( mazebuffer, tmpv[0].x>>16, tmpv[0].y>>16, tmpv[1].x>>16, tmpv[1].y>>16, makecol(0,0,0));
         line ( mazebuffer, tmpv[1].x>>16, tmpv[1].y>>16, tmpv[2].x>>16, tmpv[2].y>>16, makecol(0,0,0));
         line ( mazebuffer, tmpv[2].x>>16, tmpv[2].y>>16, tmpv[3].x>>16, tmpv[3].y>>16, makecol(0,0,0));
         line ( mazebuffer, tmpv[3].x>>16, tmpv[3].y>>16, tmpv[0].x>>16, tmpv[0].y>>16, makecol(0,0,0));
      }
   //}

}

void Maze::draw_masked_wall ( s_Maze_wall_info &wallinfo, BITMAP *texture )
{
   int i;
   V3D tmpv [ 4 ];
   int tmpx;
   int tmpy;


   for ( i = 0 ; i < 4 ; i++ )
   {
      tmpv [ i ] = wallinfo.vertex [ i ];
      tmpx = LIGHT_DISTANCE [ wallinfo.ltile ] . tablex;
      tmpy = LIGHT_DISTANCE [ wallinfo.ltile ] . tabley;
      tmpv [ i ].c = p_light_table [ tmpy ] [ tmpx ]
         [ wallinfo.lvtex [ i ] ];
      //tmpv [ i ].x += System_X_OFFSET<<16;
      //tmpv [ i ].y += System_Y_OFFSET<<16;

   }

   if ( tmpv[0].c != 0 || tmpv[1].c != 0 || tmpv[2].c != 0 || tmpv[3].c != 0 )
      quad3d ( mazebuffer, p_mask_polytype, texture
         , &tmpv[ 0 ], &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );
}

void Maze::draw_thickness ( s_Maze_wall_info &wallinfo, BITMAP *texture )
{
   int i;
   V3D tmpv [ 4 ];
   int tmpx;
   int tmpy;
   int tmpval;

   for ( i = 0 ; i < 4 ; i++ )
   {
      tmpv [ i ] = wallinfo.vertex [ i ];
      tmpx = LIGHT_DISTANCE [ wallinfo.ltile ] . tablex;
      tmpy = LIGHT_DISTANCE [ wallinfo.ltile ] . tabley;
      tmpv [ i ].c = p_light_table [ tmpy ] [ tmpx ]
         [ wallinfo.lvtex [ i ] ];
      if ( p_adjust_color == true )
      {

         if ( p_polytype == POLYTYPE_FLAT)
         {
            if ( tmpv [ i ] . c != 0 )
               tmpval = makecol (150, 150, 150);// ( tmpv [ i ] . c  / 4 );// + 24;//96;
            else
               tmpval = 0;

            tmpv [ i ] . c = tmpval;
         }
         else
         {

            // make color close to the shading of the wall
            //get the wall shading parameter
            tmpval = ( tmpv [ i ] . c );
            if ( tmpval < 0 )
               tmpval = 0;
            tmpv [ i ] . c = makecol24 ( tmpval, tmpval, tmpval );
         }
      }
      //tmpv [ i ].x += System_X_OFFSET<<16;
      //tmpv [ i ].y += System_Y_OFFSET<<16;

   }

   /*// color adjustment : color decay uniformisation
   if ( p_adjust_color == true &&  p_polytype == POLYTYPE_FLAT)
   {
      for ( i = 0 ; i < 4 ; i++ )
         tmpv [ i ] . c = makecol ( 255, 255, 255 );
   }*/

   if ( tmpv[0].c != 0 || tmpv[1].c != 0 || tmpv[2].c != 0 || tmpv[3].c != 0)
      quad3d ( mazebuffer, p_polytype, texture
         , &tmpv[ 0 ], &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );
}


void Maze::draw_transparent_wall ( s_Maze_wall_info &wallinfo, unsigned char type, int wallID, bool heavy )
{
   int i;
   V3D tmpv [ 4 ];
//   int type;
   int tmpcolor;
   int tmpx;
   int tmpy;
   short tmpalpha;
   float divider = 1;
   float alphamul = 1;
   s_Party_position tpos;

   tpos = party.position();

//   type = p_special_wall [ wallID ];

   if ( ( p_transparency == true
      /*&& Maze_SWALL_INFO [ type ] . drawmode == DRAW_MODE_TRANS*/
      && ( wallinfo.vertex[0].c != 0 || wallinfo.vertex[1].c != 0
         || wallinfo.vertex[2].c != 0|| wallinfo.vertex[3].c != 0))
      || Maze_SWALL_INFO [ type ].drawmode == DRAW_MODE_SOLID  )
   {


//?? revise , not sure of the right values

      //use area information to trigger
      //if ( p_sky == -1 || tpos.z != 0 )
      //{
/*//                          Center > -----------+
//                            Left > --------+  |   +--------- < Right
//                        Far left > -----+  |  |   |  +----- < Far Right
//                       Away left > --+  |  |  |   |  |  +-- < Away Right
//                                     |  |  |  |   |  |  |
// s_Maze_wall id       Front 3 ->     -- -- -- -- -- -- --
//                      Side  3 ->       |  |  |  |  |  |
//                      Front 2 ->        -- -- -- -- --
//                      Side  2 ->          |  |  |  |
//                      Front 1 ->           -- -- --
//                      Side  1 ->             |  |

#define Maze_WALL_AWAY_LEFT_FRONT_3       0
#define Maze_WALL_AWAY_RIGHT_FRONT_3      1
#define Maze_WALL_FAR_LEFT_FRONT_3        2
#define Maze_WALL_FAR_RIGHT_FRONT_3       3
#define Maze_WALL_LEFT_FRONT_3            4
#define Maze_WALL_RIGHT_FRONT_3           5
#define Maze_WALL_CENTER_FRONT_3          6

#define Maze_WALL_AWAY_LEFT_SIDE_3        7
#define Maze_WALL_AWAY_RIGHT_SIDE_3       8
#define Maze_WALL_FAR_LEFT_SIDE_3         9
#define Maze_WALL_FAR_RIGHT_SIDE_3        10
#define Maze_WALL_LEFT_SIDE_3             11
#define Maze_WALL_RIGHT_SIDE_3            12

#define Maze_WALL_FAR_LEFT_FRONT_2        13
#define Maze_WALL_FAR_RIGHT_FRONT_2       14
#define Maze_WALL_LEFT_FRONT_2            15
#define Maze_WALL_RIGHT_FRONT_2           16
#define Maze_WALL_CENTER_FRONT_2          17

#define Maze_WALL_FAR_LEFT_SIDE_2         18
#define Maze_WALL_FAR_RIGHT_SIDE_2        19
#define Maze_WALL_LEFT_SIDE_2             20
#define Maze_WALL_RIGHT_SIDE_2            21

#define Maze_WALL_LEFT_FRONT_1            22
#define Maze_WALL_RIGHT_FRONT_1           23
#define Maze_WALL_CENTER_FRONT_1          24

#define Maze_WALL_LEFT_SIDE_1             25
#define Maze_WALL_RIGHT_SIDE_1            26

note: there is a bug with wall 22.
Maybe draw 1 line of wall at a time to see if right
easier to see with heavy applied.
temporary color set to red: Test to see if color change move toward blue.

*/

      if ( config.get ( Config_SHADING ) == Config_SHD_YES )
      {
         if ( wallID <= 6 ) // 5-6
            {
               divider = 3;
               alphamul = 2;
            }
         else
            if ( wallID <= 12 )
            {
               divider = 2;
               alphamul = 1.5;
            }
            else
                if ( wallID <= 17 ) // 16-17
                {
                   divider = 2;
                   alphamul = 1.5;
                }
                else
                   if ( wallID <= 21 )
                   {
                      divider = 1.5;
                      alphamul = 1.25;
                   }
                   else
                      if ( wallID <= 24 ) // 23-24
                      {
                         divider = 1.5;
                         alphamul = 1.25;
                      }
                      else
                      {
                         divider = 1;
                         alphamul = 1;
                      }
      }

      //alphamul = 1;

      if ( heavy == true )
      {
         //divider = divider * 2;
         alphamul = alphamul /2;
      }

      tmpcolor = makecol (
         static_cast<int>( Maze_SWALL_INFO [ type ].red / divider),
         static_cast<int>( Maze_SWALL_INFO [ type ].green / divider),
         static_cast<int>( Maze_SWALL_INFO [ type ].blue / divider ) );

//tmp debugginb code
//tmpcolor  = makecol ( 255, 0, 0);

      for ( i = 0 ; i < 4 ; i++ )
      {
         tmpv [ i ] = wallinfo.vertex [ i ];
         tmpx = LIGHT_DISTANCE [ wallinfo.ltile ] . tablex;
         tmpy = LIGHT_DISTANCE [ wallinfo.ltile ] . tabley;

         if ( p_light_table [ tmpy ] [ tmpx ] [ wallinfo.lvtex [ i ] ] == 0)
            tmpv [ i ].c = 0;
         else
            tmpv [ i ].c = tmpcolor; // this gets overwrited. Test
         //tmpv [ i ].x += System_X_OFFSET<<16;
         //tmpv [ i ].y += System_Y_OFFSET<<16;
/*
   for ( i = 0 ; i < 4 ; i++ )
   {
      tmpv [ i ] = wallinfo.vertex [ i ];



      //tmpv [ i ].x += System_X_OFFSET<<16;
      //tmpv [ i ].y += System_Y_OFFSET<<16;

      //printf ("tmpval=%d\n", tmpv[i].c);


   }*/
      }

      tmpalpha = static_cast<short>
         ( Maze_SWALL_INFO [ type ] . alpha * alphamul );
      if ( tmpalpha > 255 )
         tmpalpha = 255;

      drawing_mode ( Maze_SWALL_INFO [ type ] . drawmode , NULL, 0, 0 );
      set_trans_blender ( 0, 0, 0, tmpalpha );

//      quad3d ( mazebuffer, POLYTYPE_GRGB, NULL
//         , &tmpv[ 0 ], &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );


      quad3d ( mazebuffer, POLYTYPE_FLAT, NULL
         , &tmpv[ 0 ], &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );

      drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
      set_trans_blender ( 0, 0, 0, 255 );
   }
   else
   {
      if (type == 9) // darkness
      {


         for ( i = 0 ; i < 4 ; i++ )
         {
            tmpv [ i ] = wallinfo.vertex [ i ];
            tmpv [ i ].c =  makecol ( 0, 0, 0) ;
            //tmpv [ i ].x += System_X_OFFSET<<16;
            //tmpv [ i ].y += System_Y_OFFSET<<16;

         }

         quad3d ( mazebuffer, POLYTYPE_FLAT, NULL
            , &tmpv[ 0 ], &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );
      }

   }

}


void Maze::draw_floor ( s_Maze_floor_info &floorinfo, BITMAP *texture, int nb_rotation )
{
   int i;
   int j;
   V3D tmpv [ 4 ];
   fixed texu;
   fixed texv;
   int tmpval;
   int tmpx;
   int tmpy;
  // int tmpcolor;
   //int swpcolor = makecol ( 255, 255, 255 );
   //int swpcolor = makecol ( 125, 125, 125 );
//printf ("Debug: draw floor: before light\n");
   for ( i = 0 ; i < 4 ; i++ )
   {
      tmpv [ i ] = floorinfo.vertex [ i ];

      tmpx = LIGHT_DISTANCE [ floorinfo.ltile ] . tablex;
      tmpy = LIGHT_DISTANCE [ floorinfo.ltile ] . tabley;
      tmpv [ i ].c = p_light_table [ tmpy ] [ tmpx ] [ i ];
      //tmpv [ i ].x += System_X_OFFSET<<16;
      //tmpv [ i ].y += System_Y_OFFSET<<16;

      if ( p_adjust_color == true )
      {
         if ( p_polytype == POLYTYPE_FLAT )
         {
            if ( tmpv [ i ] . c != 0 )
               tmpval = makecol (50, 50, 50);// ( tmpv [ i ] . c  / 4 );// + 24;//96;
            else
               tmpval = 0;
            /*tmpcolor = makecol ( tmpval, tmpval, tmpval );
            if ( tmpcolor < swpcolor )
               swpcolor = tmpcolor;
            tmpv [ i ] . c = swpcolor;*/
            // note : color will be saved at the end ??? what!

            tmpv [ i ] . c = tmpval;
         }
         else
         {
            tmpval = ( tmpv [ i ] . c ) - 64;//32;
            if ( tmpval < 0 )
               tmpval = 0;
            tmpv [ i ] . c = makecol24 ( tmpval, tmpval, tmpval );
         }
      }
   }

   // color adjustment : color decay uniformisation
   /*if ( p_adjust_color == true && p_polytype == POLYTYPE_FLAT)
   {
      for ( i = 0 ; i < 4 ; i++ )
         tmpv [ i ] . c = swpcolor;
   }*/
//printf ("Debug: draw Maze: before texture rotation\n");
   // texture rotation
   for ( j = nb_rotation ; j > 0 ; j-- )
   {
      texu = tmpv [ 0 ] . u;
      texv = tmpv [ 0 ] . v;
      for ( i = 0 ; i < 3 ; i++ )
      {
         tmpv [ i ] . u = tmpv [ i + 1 ] . u;
         tmpv [ i ] . v = tmpv [ i + 1 ] . v;
      }
      tmpv [ 3 ] . u = texu;
      tmpv [ 3 ] . v = texv;
   }

//printf ("Debug: draw Maze: before draw quad\n");
   if ( mazebuffer == NULL ) printf ("Debug: draw Maze: mazebuffer = null\n");
   if ( texture == NULL) printf ("Debug: draw Maze: texture = null\n");

   if ( tmpv[0].c != 0 || tmpv[1].c != 0 || tmpv[2].c != 0 || tmpv[3].c != 0 )
      quad3d ( mazebuffer, p_polytype, texture
         , &tmpv[ 0 ], &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );
//printf ("Debug: draw Maze: after draw quad\n");
}

void Maze::draw_masked_floor ( s_Maze_floor_info &floorinfo, BITMAP *texture, int nb_rotation )
{
   int i;
   int j;
   V3D tmpv [ 4 ];
   int tmpx;
   int tmpy;
   fixed texu;
   fixed texv;


   for ( i = 0 ; i < 4 ; i++ )
   {
      tmpv [ i ] = floorinfo.vertex [ i ];
      tmpx = LIGHT_DISTANCE [ floorinfo.ltile ] . tablex;
      tmpy = LIGHT_DISTANCE [ floorinfo.ltile ] . tabley;
      tmpv [ i ].c = p_light_table [ tmpy ] [ tmpx ] [ i ];
      //tmpx = System_X_OFFSET;
      //tmpx = tmpx<<16'
      //tmpv [ i ].x += System_X_OFFSET<<16;
      //tmpv [ i ].y += System_Y_OFFSET<<16;
   }

   // texture rotation
   for ( j = nb_rotation ; j > 0 ; j-- )
   {
      texu = tmpv [ 0 ] . u;
      texv = tmpv [ 0 ] . v;
      for ( i = 0 ; i < 3 ; i++ )
      {
         tmpv [ i ] . u = tmpv [ i + 1 ] . u;
         tmpv [ i ] . v = tmpv [ i + 1 ] . v;
      }
      tmpv [ 3 ] . u = texu;
      tmpv [ 3 ] . v = texv;
   }


   if ( tmpv[0].c != 0 || tmpv[1].c != 0 || tmpv[2].c != 0 || tmpv[3].c != 0 )
      quad3d ( mazebuffer, p_mask_polytype, texture,
         &tmpv[ 0 ],  &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );
}

void Maze::draw_screen ( void )
{
   int i;
   V3D tmpv [ 4 ];
   int tmpcolor;
   int type;
   s_Party_position tpos = party.position ();
   int offset = 0;

//printf ("Maze: Draw Screen: Pass1\n");

   type = mazetile [ tpos.z ] [ tpos.y ] [ tpos.x ]  . special &
      MAZETILE_FILLING_MASK ;
//printf ("Maze: Draw Screen: Pass2\n");
   /*if ( ( mazetile [ tpos.z ] [ tpos.y ] [ tpos.x ]  . special &
      Maze_SPECIAL_DARKNESS ) > 0 )
   {
      //rectfill ( mazebuffer, System_X_OFFSET + 0, System_Y_OFFSET + 0, System_X_OFFSET + 639,
      //          System_Y_OFFSET + 479, makecol (0, 0, 0) );
      //rectfill ( mazebuffer, 0, 0, 639, 479, makecol (0, 0, 0) );

      type = 9; // darkness

   }*/

//printf ("Maze: Draw Screen: Pass3\n");

   if ( type > 0 )
   {
      if ( p_transparency == true || Maze_SWALL_INFO [ type ].drawmode == DRAW_MODE_SOLID)
      {

         if ( Maze_SWALL_INFO [ type ]. smoke == true)
         {

            for ( i = 0; i < tpos.facing ; i++ )
               offset += 1;

            //if ( ( offset % 2)  == 0)
            set_trans_blender(0, 0, 0, 32);
            drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );
            draw_trans_sprite ( mazebuffer, p_smokebitmap, 0, 0 );
            /*draw_trans_sprite ( mazebuffer, datref_sky [ Maze_TEX_SMOKE ], 0, 0 );
            draw_trans_sprite ( mazebuffer, datref_sky [ Maze_TEX_SMOKE ], 0, 256 );
            draw_trans_sprite ( mazebuffer, datref_sky [ Maze_TEX_SMOKE ], 200, 0 );
            draw_trans_sprite ( mazebuffer, datref_sky [ Maze_TEX_SMOKE ], 200, 256 );*/
            set_trans_blender(0, 0, 0, 255);
            drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );

            /*blit ( p_smokebitmap, mazebuffer, offset, 0, 0, 0, 768-offset, 200);
            blit ( p_smokebitmap, mazebuffer, 0, 0, 768-offset, 0, offset, 200);
            blit ( p_smokebitmap, mazebuffer, offset, 0, 0, 200, 768-offset, 200);
            blit ( p_smokebitmap, mazebuffer, 0, 0, 768-offset, 200, offset, 200);
            blit ( p_smokebitmap, mazebuffer, offset, 0, 0, 400, 768-offset, 200);
            blit ( p_smokebitmap, mazebuffer, 0, 0, 768-offset, 400, offset, 200);*/
         }

         tmpcolor = makecol ( Maze_SWALL_INFO [ type ].red,
                        Maze_SWALL_INFO [ type ].green,
                        Maze_SWALL_INFO [ type ].blue );
//printf ("Maze: Draw Screen: Pass4\n");
         for ( i = 0 ; i < 4 ; i++ )
         {
            tmpv [ i ] = SCREEN [ i ];
            tmpv [ i ].c = tmpcolor;
            //tmpv [ i ].x += System_X_OFFSET<<16;
            //tmpv [ i ].y += System_Y_OFFSET<<16;
         }

         drawing_mode ( Maze_SWALL_INFO [ type ] . drawmode , NULL, 0, 0 );
         set_trans_blender ( 0, 0, 0, Maze_SWALL_INFO [ type ] . alpha );

         quad3d ( mazebuffer, POLYTYPE_FLAT, NULL,
            &tmpv[ 0 ], &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );

         drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
         set_trans_blender ( 0, 0, 0, 255 );
//printf ("Maze: Draw Screen: Pass5\n");
      }
   }
}

/*void Maze::draw_object ( s_Maze_object_info &objinfo, BITMAP *image, unsigned char light )
{
   BITMAP *tmpbuf;

   //?? must test if shading is set

   if ( light != 0 )
   {
      if ( p_polytype == POLYTYPE_FLAT || p_polytype == POLYTYPE_PTEX
         || p_polytype == POLYTYPE_ATEX )
      {
         //stretch_sprite ( mazebuffer, image, System_X_OFFSET + objinfo.x, System_Y_OFFSET + objinfo.y, objinfo.width,
         //   objinfo.height );
         if ( config.get ( Config_ANTI_ALLIASING ) == Config_YES)
         {
           aa_stretch_sprite ( mazebuffer, image, objinfo.x, objinfo.y, objinfo.width,
            objinfo.height );
         }
         else
         {
            stretch_sprite ( mazebuffer, image, objinfo.x, objinfo.y, objinfo.width,
            objinfo.height );
         }

      }

      else
      {
         tmpbuf = create_bitmap ( 128, 128 );
         clear_to_color ( tmpbuf, makecol ( 255, 0, 255 ) );
         draw_lit_sprite ( tmpbuf, image, 0, 0, ( 255 - light ) );
         //stretch_sprite ( mazebuffer, tmpbuf, System_X_OFFSET + objinfo.x, System_Y_OFFSET + objinfo.y, objinfo.width,
         //   objinfo.height );

         stretch_sprite ( mazebuffer, tmpbuf, objinfo.x, objinfo.y, objinfo.width,
            objinfo.height );
         destroy_bitmap ( tmpbuf );
      }
   }
}*/

void Maze::draw_floor_object ( int tile, int objectID, int picID )
{
   s_Maze_object_anchor objinfo = FLOOR_OBJECT [ tile ] [ objectID ];
   BITMAP *image = datref_object [ picID ];
   int tmpwidth = (objinfo.scale * image -> w  ) / 100;
   int tmpheight = (objinfo.scale * image -> h  ) / 100;
   int tmpx = objinfo.anchor_x - ( tmpwidth / 2);
   int tmpy = objinfo.anchor_y - tmpheight;
   int light = p_light_table [ LIGHT_DISTANCE [ tile ].tabley ]
                        [ LIGHT_DISTANCE [ tile ].tablex ] [ 4 ];

   draw_anchored_object ( image, tmpx, tmpy, tmpwidth, tmpheight, light );

}

void Maze::draw_ceiling_object ( int tile, int objectID, int picID )
{
   s_Maze_object_anchor objinfo = CEILING_OBJECT [ tile ] [ objectID ];
   BITMAP *image = datref_object [ picID ];
   int tmpwidth = (objinfo.scale * image -> w  ) / 100;
   int tmpheight = (objinfo.scale * image -> h  ) / 100;
   int tmpx = objinfo.anchor_x - ( tmpwidth / 2);
   int tmpy = objinfo.anchor_y;
   int light = p_light_table [ LIGHT_DISTANCE [ tile ].tabley ]
                        [ LIGHT_DISTANCE [ tile ].tablex ] [ 4 ];


   draw_anchored_object ( image, tmpx, tmpy, tmpwidth, tmpheight, light );

}

void Maze::draw_wall_object ( int tile, int objectID, int picID)
{

}

void Maze::draw_anchored_object ( BITMAP *image, int x, int y, int w, int h, int light )
{
   BITMAP *tmpbuf;

   //?? must test if shading is set
   //(I think there is currently 1 light level for the whole tile.
   // Not sure if light is adjusted according to various filling.

   //printf ("Maze draw floor object: Pass in function\n");
   if ( image != NULL )
   {
      //printf ("image is not null\n");
      if ( light != 0 )
      {
         if ( p_polytype == POLYTYPE_FLAT || p_polytype == POLYTYPE_PTEX
            || p_polytype == POLYTYPE_ATEX )
         {
            if ( config.get ( Config_ANTI_ALLIASING ) == Config_YES)
            {
               aa_stretch_sprite ( mazebuffer, image, x , y , w, h );
            }
            else
            {


               stretch_sprite ( mazebuffer, image, x , y , w, h );
            }
         }
         else
         {
            tmpbuf = create_bitmap ( image -> w, image -> h );
            clear_to_color ( tmpbuf, makecol ( 255, 0, 255 ) );
            draw_lit_sprite ( tmpbuf, image, 0, 0, ( 255 - light ) );

            if ( config.get ( Config_ANTI_ALLIASING ) == Config_YES)
            {
               aa_stretch_sprite ( mazebuffer, tmpbuf, x , y , w, h );
            }
            else
               stretch_sprite ( mazebuffer, tmpbuf, x , y , w, h );

            destroy_bitmap ( tmpbuf );
         }
      }
   }
}

void Maze::draw_sky ( void )
{
   s_Party_position tpos;

   tpos = party.position();
   int offset = 0;
   int i;

   if ( p_sky != -1 )
   {
      for ( i = 0; i < tpos.facing ; i++ )
         offset += 192;

      blit ( p_skybitmap, mazebuffer, offset, 0, 0, 0, 768-offset, 200);
      blit ( p_skybitmap, mazebuffer, 0, 0, 768-offset, 0, offset, 200);

      //for ( i = 0; i < 640; i += 16 )
      //   draw_sprite ( mazebuffer, datref_variousimage [ Maze_IMG_HORIZONDITHER], i, 184 );


      //drawing_mode ( DRAW_MODE_MASKED_PATTERN , datref_variousimage [ Maze_IMG_HORIZONDITHER], 0, 0 );
      //rectfill ( mazebuffer, 0, 200, 640, 480, makecol (100, 100, 100));
      //drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
   }


   /*if ( p_sky != -1 && tpos.z == 0 )
   {
      if ( tpos.facing == Party_FACE_NORTH ||
         tpos.facing == Party_FACE_SOUTH )
      {
         stretch_sprite ( mazebuffer, datref_sky [ p_sky ],
            0, -80, 320, 290 );
         stretch_sprite ( mazebuffer, datref_sky [ p_sky ],
            320, -80, 320, 290 );
      }
      else
      {
         stretch_sprite ( mazebuffer, datref_sky [ p_sky ],
            -160, -80, 320, 290 );
         stretch_sprite ( mazebuffer, datref_sky [ p_sky ],
            160, -80, 320, 290 );
          stretch_sprite ( mazebuffer, datref_sky [ p_sky ],
            480, -80, 320, 290 );

      }


   }*/
}

void Maze::build_sky_bitmap ( void )
{

   if ( p_skybitmap == NULL )
      p_skybitmap = create_bitmap ( 768, 200 );

   if ( p_sky != -1 )
   {


      draw_sprite ( p_skybitmap, datref_sky [ p_sky ], 0, 0 );
      draw_sprite ( p_skybitmap, datref_sky [ p_sky ], 256, 0 );
      draw_sprite ( p_skybitmap, datref_sky [ p_sky ], 512, 0 );
   }
   // seconds layer (currently hardcoded)

   /*draw_sprite ( p_skybitmap, datref_sky [ 8 ], 0, 0 );
   draw_sprite ( p_skybitmap, datref_sky [ 8 ], 256, 0 );
   draw_sprite ( p_skybitmap, datref_sky [ 8 ], 512, 0 );*/

   // add dithering picture
   // ?? todo
}

void Maze::build_smoke_bitmap ( void )
{
   static BITMAP* tmpsmoke;

   if ( p_smokebitmap == NULL )
   {
      p_smokebitmap = create_bitmap ( 640, 480 );
      tmpsmoke = create_bitmap (256, 200);
      clear_to_color ( tmpsmoke, makecol (255, 0, 255));
   }

   draw_sprite_v_flip(tmpsmoke, datref_sky [ Maze_TEX_SMOKE ], 0, 0);

   if ( config.get ( Config_ANTI_ALLIASING ) == Config_YES)
   {
      aa_stretch_sprite ( p_smokebitmap, datref_sky [ Maze_TEX_SMOKE ], 0, 0, 320, 240 );
      aa_stretch_sprite ( p_smokebitmap, datref_sky [ Maze_TEX_SMOKE ], 320, 0, 320, 240 );
      aa_stretch_sprite ( p_smokebitmap, tmpsmoke, 0, 240, 320, 240 );
      aa_stretch_sprite ( p_smokebitmap, tmpsmoke, 320, 240, 320, 240 );
   }
   else
   {
      stretch_sprite ( p_smokebitmap, datref_sky [ Maze_TEX_SMOKE ], 0, 0, 320, 240 );
      stretch_sprite ( p_smokebitmap, datref_sky [ Maze_TEX_SMOKE ], 320, 0, 320, 240 );
      stretch_sprite ( p_smokebitmap, tmpsmoke, 0, 240, 320, 240 );
      stretch_sprite ( p_smokebitmap, tmpsmoke, 320, 240, 320, 240 );
   }
}

void Maze::check_palette ( void )
{
   s_Party_position tpos;
   int tmpal;

   tpos = party.position ();

   tmpal = ( mazetile [tpos.z][tpos.y][tpos.x].wobjpalette & MAZETILE_PALETTE_MASK );

   //printf ("Maze:Check Palette:mazepalid=%d, tmpal=%d\n", mazepalid, tmpal );

   if ( mazepalid != tmpal)
   {
      //printf ("Debug:Maze:Check Palette Loading palette %d\n", tmpal );
      mazepalid = tmpal;
      mazepal.load ( mazepalid );
      mazepal.build();
      encount.build_table ( tpos.z, mazepalid );
   }


}


void Maze::draw_maze ( bool demo )
{
   s_Party_position tpos;
   short tmpx;
   short tmpy;
   s_mazetile tmptile;
   int i;
   int j;
   int fog;
   unsigned char tmpmask;
   unsigned short tmpbitset;
   unsigned char tmpspec;
   short tmpval;
   bool tmpodd; // result of moduloed oddeven
   int oddeven; // accumulate and then modulo to determin if odd drawing
   bool tmpheavy= false; // draw thickier transparancy when inside it.
   bool drawmwall;
   int walltex_index;
   //int walltex_value;
   //int tmpobjpic;

//printf ("Debug: draw maze: after init\r\n");

   // adjust lighting according to filling.
         tpos = party.position ();
         tmptile = mazetile [ tpos.z ] [ tpos.y ] [ tpos.x ] ;
         // set light level
         if ( ( tmptile.special & MAZETILE_FILLING_MASK ) == MAZETILE_FILLING_FOG )
            fog = 1;
         else
            fog = 0;

         if ( ( tmptile.special & MAZETILE_FILLING_MASK ) != MAZETILE_FILLING_DARKNESS )
         {
            // uncomment when active effect will be available
            /*if ( party.spell_duration ( Party_SPELL_LIGHT ) > 0 )
               light ( Maze_LIGHT_LEVEL_3 - fog );
            else*/
               if ( demo == false)
                  light ( Maze_LIGHT_LEVEL_2 - fog );
               else
                  light ( Maze_LIGHT_LEVEL_3 - fog );

         }
         else
         {
            light ( Maze_LIGHT_LEVEL_0 );
         }

   check_palette();

   clear ( mazebuffer );

   tpos = party.position ();

   evaluate_light ();

   draw_sky ();

//printf ("Debug: draw maze: after drawsky\r\n");

   if ( (mazetile [ tpos.z ] [ tpos.y ] [ tpos.x ] .special &
      MAZETILE_FILLING_MASK ) > 0 )
      tmpheavy = true;

   // loop that draws floors, then walls then objects
   for ( i = 0 ; i < 15 ; i++ )
   {
      //printf ("Maze: Draw_maze(): Pass 1");
      tmpx = tpos.x + Maze_DRAW_INFO.test [tpos.facing].offset [ i ].xoff;
      tmpy = tpos.y + Maze_DRAW_INFO.test [tpos.facing].offset [ i ].yoff;

//printf ("Debug: draw maze: drawloop tile %d\r\n", i);
      if ( tmpx >=0 && tmpx < Maze_MAXWIDTH && tmpy >=0 && tmpy < Maze_MAXWIDTH )
      {
         tmptile = mazetile [ tpos.z ] [ tmpy ] [ tmpx ];

         tmpodd = false;
         oddeven = 0;
          // set odd even values to be used by drawwall for texture flipping
         if ( tmpx % 2 > 0 )
            oddeven++;
         if ( tmpy % 2 > 0 )
            oddeven++;
         if ( tpos.facing == Maze_FACE_EAST || tpos.facing == Maze_FACE_WEST )
            oddeven++;

         if ( oddeven % 2 > 0 )
            tmpodd = true;
         else
            tmpodd = false;
//printf ("Debug: draw Maze: before floor and ceiling\n");
         // drawing floor and ceiling
         if ( config.get ( Config_FLOOR_CEILING ) == Config_FAC_YES )
         {
             //printf ("Debug: draw Maze: before draw floor\n");
            draw_floor ( FLOOR [ i ], mazepal.bmp [ TexPal_IDX_FLOOR ],
               //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 1 ],
               Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );
               //printf ("Debug: draw Maze: after draw floor\n");

            if ( p_sky == -1 || tpos.z != 0 )
            {
               //printf ("Debug: draw Maze: before draw ceiling\n");
               draw_floor ( CEILING [ i ], mazepal.bmp [ TexPal_IDX_CEILING ],
                  //p_texset [ tmptile.texture & Maze_TEXSET_MASK] [ 3 ],
                  Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );
               //printf ("Debug: draw Maze: after draw ceiling\n");
            }
         }
//printf ("Debug: draw Maze: before special floor\n");
         // drawing special floor and ceiling
         if ( ( tmptile.maskposition & MAZETILE_MASKPOS_FLOOR_MASK ) > 0 )
         {
            //if ( ( tmptile.texture & Maze_FLOOR_MTEX_DOWN ) > 0 )
            if ( ( tmptile.maskposition & MAZETILE_MASKTEXPOS_FLOOR ) > 0 )
               draw_masked_floor ( FLOOR [ i ] ,
                     mazepal.bmp [ TexPal_IDX_MFLOOR
                     + ( ( tmptile.masked & MAZETILE_MASKPOS_FLOOR_MASK ) >> 4 ) ],
                  //p_masktex [ tmptile.floortex ],
                  Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );

            //if ( ( tmptile.texture & Maze_FLOOR_MTEX_UP ) > 0 )
            if ( ( tmptile.maskposition & MAZETILE_MASKTEXPOS_CEILING ) > 0 )
               draw_masked_floor ( CEILING [ i ] ,
                  mazepal.bmp [ TexPal_IDX_MFLOOR
                     + ( ( tmptile.masked & MAZETILE_MASKPOS_FLOOR_MASK ) >> 4 ) ],
                  //p_masktex [ tmptile.floortex ],
                  Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );
         }

//printf ("Debug: draw Maze: before wall and door\n");
         // walls and doors drawing
         for ( j = 0 ; j < 3 ; j++ )
         {
            //printf ("Maze: Draw_maze(): Pass 2");
            // draw wall
            tmpmask = Maze_DRAW_INFO.test [tpos.facing].wall [j];
            if ( ( tmptile.solid & tmpmask ) == tmpmask &&
               Maze_DRAW_INFO.tested_wallID [ i ] [ j ] != -1 )
            {
               // test for which wall texture to use to apply dual, quad and octo textures
               // removed, too problematic, also conflict with door
               // There is overlap problems and there is a bug where Y is repeated all the time
               //walltex_value = 0;
               walltex_index = 0;
               /*if ( tmpmask == WNORTH || tmpmask == WSOUTH)
                  walltex_value = tmpx;
               else if ( tmpmask == WWEST || tmpmask == WEAST)
                  walltex_value = tmpy;



               if ( tmpx % 2 == 0 )
                     walltex_index = 1;
               if ( tmpx % 4 == 0 )
                     walltex_index = 2;
               if ( tmpx % 8 == 0 )
                     walltex_index = 3;*/

               draw_wall ( WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ],
                  //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 0 ],
                  mazepal.bmp [ TexPal_IDX_WALL + walltex_index ],
                  tmpodd );
               // drawing wall thickness
               if ( WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ].thickwallID
                  != -1 )
                  draw_thickness ( THICKWALL [
                     WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ].thickwallID ],
                        mazepal.bmp [ TexPal_IDX_WALL ] );
                        //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 0 ] );
            }

//printf ("Debug: draw Maze: before door\n");
            // draw door
            tmpmask = Maze_DRAW_INFO.test [tpos.facing].door [j];
            if ( ( tmptile.solid & tmpmask ) == tmpmask &&
               Maze_DRAW_INFO.tested_wallID [ i ] [ j ] != -1 )
            {
               draw_masked_wall (
                  WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ],
                  mazepal.bmp [ TexPal_IDX_DOOR ] );
                  //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 2 ] );
            }
            // draw grid
            else
            {
               tmpmask = Maze_DRAW_INFO.test [tpos.facing].grid [j];
               if ( ( tmptile.solid & tmpmask ) == tmpmask &&
                  Maze_DRAW_INFO.tested_wallID [ i ] [ j ] != -1 )
               {
                  draw_masked_wall (
                     WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ],
                     mazepal.bmp [ TexPal_IDX_GRID + ( tmptile.maskposition & MAZETILE_MASKPOS_GRID_MASK ) ] );
                  //p_masktex [ tmptile.walltex ] );
               }

            }

//printf ("Debug: draw Maze: before masked wall\n");
            // draw masked wall
            tmpbitset = ( ( tmptile.maskposition & MAZETILE_MASKPOS_WALL_MASK )  >> 2 );
            if ( tmpbitset > 0 )
            {
               // convert from Maze_WALL_MTEX to Maze_4bit
               //tmpbitset = ( ( tmptile.texture & Maze_WALL_MTEX_MASK ) >> 10 );


               drawmwall = false;

               switch ( Maze_DRAW_INFO.test [tpos.facing].wall [j] )
               {
                  case WNORTH :
                     if ( ( tmpbitset & Maze_4BIT_NORTH ) > 0 )
                        drawmwall = true;
                  break;
                  case WEAST :
                     if ( ( tmpbitset & Maze_4BIT_EAST ) > 0 )
                        drawmwall = true;
                  break;
                  case WSOUTH :
                     if ( ( tmpbitset & Maze_4BIT_SOUTH ) > 0 )
                        drawmwall = true;
                  break;
                  case WWEST :
                     if ( ( tmpbitset & Maze_4BIT_WEST ) > 0 )
                        drawmwall = true;
                  break;
               }
               if ( drawmwall == true
                  && ( Maze_DRAW_INFO.tested_wallID [ i ] [ j ] != -1 ) )
                  draw_masked_wall (
                     WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ],
                     mazepal.bmp [ TexPal_IDX_MWALL
                     + ( tmptile.masked & MAZETILE_MASKPOS_WALL_MASK ) ] );
                     //p_masktex [ tmptile.walltex ] );
            }
         }

         // place objects in the maze
         //tmpval = ( tmptile.objposition & Maze_OBJECT_MASK ) >> 6;

         // object display testing code
         //for ( j = 0 ; j < 9 ; j++ )
         //{
            //draw_floor_object ( i, 4, 1024 );

            //draw_ceiling_object ( i, 4, 1024 );

         //}

         tmpval = 0; // new comparation to make since 3 type of objects
         if ( tmpval > 0 )
         {
            tmpbitset = Maze_DRAW_INFO.test [ tpos.facing ].object [ tmpval ];
            //tmpobjpic = tmptile.objectpic; // need to adapt according to wall ceiling, floor

            for ( j = 0 ; j < 9 ; j++ )
            {
               /*if ( j == 4)
                  draw_floor_object ( FLOOR_OBJECT [ i ] [ j ], 1024,
                     p_light_table [ LIGHT_DISTANCE [ i ].tabley ]
                        [ LIGHT_DISTANCE [ i ].tablex ] [ 4 ] );*/
               // replace with new object drawing functions
               /*if ( tmpbitset & ( 256 >> j ) )
                  draw_object ( OBJECT [ i ] [ j ],
                     p_objimg [ tmpobjpic ],
                     p_light_table [ LIGHT_DISTANCE [ i ].tabley ]
                        [ LIGHT_DISTANCE [ i ].tablex ] [ 4 ] );*/
            }
         }

         // drawing transparent wall
         tmpspec = tmptile.special & MAZETILE_FILLING_MASK;
         if ( tmpspec > 0 )
            for ( j = 0 ; j < 2 ; j++ )
            {
               if ( Maze_DRAW_INFO.trans_wallID [ i ] [ j ] != -1 )
                  draw_transparent_wall (
                     WALL [ Maze_DRAW_INFO.trans_wallID [ i ] [ j ] ],
                     tmpspec, Maze_DRAW_INFO.trans_wallID [ i ] [ j ], tmpheavy );
            }

         if ( ( tmptile.special & MAZETILE_FILLING_MASK )  == MAZETILE_FILLING_DARKNESS)
            for ( j = 0 ; j < 2 ; j++ )
            {
               if ( Maze_DRAW_INFO.trans_wallID [ i ] [ j ] != -1 )
                  draw_transparent_wall (
                     WALL [ Maze_DRAW_INFO.trans_wallID [ i ] [ j ] ],
                     8 , Maze_DRAW_INFO.trans_wallID [ i ] [ j ]
                     , false );
            }

      }
   }

   // debuging display not to keep
   //textprintf_old ( mazebuffer, System_FONT_NORMAL, 0, 0, General_COLOR_TEXT,
   //            "Encounter Counter: %d", encount.counter() );


  // printf ("before draw screen\n");
   draw_screen ();
   //printf ("after draw screen\n");

  // textprintf_old ( mazebuffer, FNT_print, 0,0, General_COLOR_TEXT, "Palette: %d", mazepalid);

   blit_mazebuffer();
}

//---------------------------------------------------------------------------

/*bool Maze::execute_event ( void )
{*/

   //----- old code -----
   //printf ("Maze event start\r\n");
   //save_backup_screen ();
   //blit_mazebuffer();

   // verify the event ID and call the right function according to event type
   /*if ( tmptile.event != 0 )
   {
      procID = p_event [ tmptile.event ] . type;
      if ( MazeProc_EVENT [ procID  ] . proc != NULL )
         retval = MazeProc_EVENT [ procID ] . proc
            ( p_event [ tmptile.event ] );
   }*/

   //printf ("maze event end\r\n");

  // return ( retval );
//}

void Maze::evaluate_light ( void )
{
   s_Party_position tpos;
   int x;
   int y;
   int tblx;
   int tbly;
   int i;
   int j;
   int k;
   int tmpval;
   s_mazetile tmptile;
   int tmplight = p_light;

   tpos = party.position ();

   // tmpcode
   //tmplight = Maze_LIGHT_LEVEL_3;

//light ( Maze_LIGHT_LEVEL_3 );

   /*if ( p_sky != -1 && tpos.z == 0 ) //bug: not dependent on level 0, but palette
   {
      tmplight = Maze_LIGHT_LEVEL_3;
   }*/

   for ( y = 0 ; y < 5 ; y++ )
      for ( x = 0 ; x < 9 ; x++ )
         for ( i = 0 ; i < 5 ; i++ )
            p_light_table [ y ] [ x ] [ i ] = 0;

   // copy basic light diffusion from party

   /*to do: Use maze area information to setup sky
   if ( p_sky != -1 && tpos.z == 0 )
   {
      for ( i = 0 ; i < 15 ; i++ )
      {
         x = LIGHT_DISTANCE [ i ] .tablex;
         y = LIGHT_DISTANCE [ i ] .tabley;

         for ( j = 0 ; j < 5 ; j++ )
         {
//            tmpval = LIGHT_DISTANCE [ i ] . vtex [ j ] - p_darkness;
//            if ( tmpval < 0 )
//               p_light_table [ y ] [ x ] [ j ] = 0;
//            else
               p_light_table [ y ] [ x ] [ j ] = 255;
         }
      }

   }
   else*/
   {
      for ( i = 0 ; i < 15 ; i++ )
      {
         x = LIGHT_DISTANCE [ i ] .tablex;
         y = LIGHT_DISTANCE [ i ] .tabley;

         for ( j = 0 ; j < 5 ; j++ )
         {
            tmpval = LIGHT_DISTANCE [ i ] . vtex [ j ] - p_darkness;
            if ( tmpval < 0 )
               p_light_table [ y ] [ x ] [ j ] = 0;
            else
               p_light_table [ y ] [ x ] [ j ] = tmpval;
         }
      }
   }

   // patch light diffussion for non shaded drawing
//   if ( p_polytype == POLYTYPE_FLAT || p_polytype == POLYTYPE_PTEX
//         || p_polytype == POLYTYPE_ATEX )
//   {
      if ( p_sky == -1 || tpos.z != 0 )
      {
         for ( i = 0 ; i < 15 ; i++ )
         {
            if ( LIGHT_VISION [ tmplight ] [ i ] == false )
            {
               x = LIGHT_DISTANCE [ i ] .tablex;
               y = LIGHT_DISTANCE [ i ] .tabley;

               for ( j = 0 ; j < 5 ; j++ )
                  p_light_table [ y ] [ x ] [ j ] = 0;
            }
         }
      }
//   }

   for ( i = 0 ; i < 15 ; i++ )
   {
      x = tpos.x + Maze_DRAW_INFO . test [ tpos.facing ] . offset [ i ] . xoff;
      y = tpos.y + Maze_DRAW_INFO . test [ tpos.facing ] . offset [ i ] . yoff;
      tblx = LIGHT_DISTANCE [ i ] .tablex;
      tbly = LIGHT_DISTANCE [ i ] .tabley;
      tmptile =  mazetile [ tpos.z ] [ y ] [ x ] ;

      // implement light
      if ( ( tmptile.special & MAZETILE_SPECIAL_LIGHT ) > 0 )
      {
         //  on lighted tile
         for ( k = 0 ; k < 5 ; k++ )
         {
            // old light scheme
            //if ( p_light_table [ tbly ] [ tblx ] [ k ] < 175 )
             //  p_light_table [ tbly ] [ tblx ] [ k ] = 175;

            // incremental light scheme

            p_light_table [ tbly ] [ tblx ] [ k ] += 100;
            if ( p_light_table [ tbly ] [ tblx ] [ k ] > 255 )
               p_light_table [ tbly ] [ tblx ] [ k ] = 255;
         }
      }
      // ?? light propagation on standby

      // implement darkness
      if ( ( tmptile.special & MAZETILE_FILLING_MASK ) == MAZETILE_FILLING_DARKNESS )
      {
         //  on darkened tile
         for ( k = 0 ; k < 5 ; k++ )
         {
            p_light_table [ tbly ] [ tblx ] [ k ] = 0;
         }
      }
      //?? darkness propagation on standby
   }



}

/*void Maze::change_display ( void )
{
   p_display++;
   if ( p_display == Maze_DISPLAY_LAST_ITEM )
      p_display = 0;
} */

/*bool Maze::test_front_wall ( unsigned char solid )
{
   unsigned char mask = MAZETILE_SOLID_NORTH_MASK;

   switch ( party.facing() )
   {
      case Maze_FACE_NORTH :
         mask = MAZETILE_SOLID_NORTH_MASK;
      break;

      case Maze_FACE_EAST :
         mask = MAZETILE_SOLID_EAST_MASK;
      break;

      case Maze_FACE_SOUTH :
         mask = MAZETILE_SOLID_SOUTH_MASK;
      break;

      case Maze_FACE_WEST :
         mask = MAZETILE_SOLID_WEST_MASK;
      break;
   }

  if ( ( solid & mask ) > 0 )
     return ( true );
  else
     return ( false );

}

bool Maze::test_front_door ( unsigned char solid )
{
   unsigned char mask = MAZETILE_SOLID_NORTH_DOOR;

   switch ( party.facing() )
   {
      case Maze_FACE_NORTH :
         mask = MAZETILE_SOLID_NORTH_DOOR;
      break;

      case Maze_FACE_EAST :
         mask = MAZETILE_SOLID_EAST_DOOR;
      break;

      case Maze_FACE_SOUTH :
         mask = MAZETILE_SOLID_SOUTH_DOOR;
      break;

      case Maze_FACE_WEST :
         mask = MAZETILE_SOLID_WEST_DOOR;
      break;
   }

  if ( ( solid & mask ) == mask )
     return ( true );
  else
     return ( false );
}*/

/*-------------------------------------------------------------------------*/
/*-                          Virtual Methods                              -*/
/*-------------------------------------------------------------------------*/
/*
void Maze::objdat_to_strdat ( void *dataptr )
{
   dbs_Maze_data &tmpdat = *(static_cast<dbs_Maze_data*> ( dataptr ));
   int i;
   short j;

   strncpy ( tmpdat.name, p_name, 31 );
   strncpy ( tmpdat.shortname, p_shortname, 11 );
   tmpdat.type = p_type;
   tmpdat.xpos = p_xpos;
   tmpdat.ypos = p_ypos;
   tmpdat.mapID = p_mapID;
   tmpdat.music = p_music;
   tmpdat.eprob = p_eprob;
   tmpdat.ebase = p_ebase;
   tmpdat.erange = p_erange;
   tmpdat.einc = p_einc;

   for ( i = 0 ; i < Maze_NB_WARP ; i++ )
   {
      tmpdat.warp [ i ].tag = p_warp [ i ].tag;
      tmpdat.warp [ i ].target = p_warp [ i ].target;
   }

   for ( j = 0 ; j < Maze_NB_SAVEVALUE ; j++ )
      tmpdat.savevalue [ j ] = p_savevalue [ j ];
}

void Maze::strdat_to_objdat ( void *dataptr )
{
   dbs_Maze_data &tmpdat = *(static_cast<dbs_Maze_data*> ( dataptr ));
   int i;
   short j;

   strcpy ( p_name, tmpdat.name );
   strcpy ( p_shortname, tmpdat.shortname );
   p_type = tmpdat.type;
   p_xpos = tmpdat.xpos;
   p_ypos = tmpdat.ypos;
   p_mapID = tmpdat.mapID;
   p_music = tmpdat.music;
   p_eprob = tmpdat.eprob;
   p_ebase = tmpdat.ebase;
   p_erange = tmpdat.erange;
   p_einc = tmpdat.einc;

   for ( i = 0 ; i < Maze_NB_WARP ; i++ )
   {
      p_warp [ i ].tag =  tmpdat.warp [ i ].tag ;
      p_warp [ i ].target = tmpdat.warp [ i ].target;
   }

   for ( j = 0 ; j < Maze_NB_SAVEVALUE ; j++ )
      p_savevalue [ j ] = tmpdat.savevalue [ j ];
}

void Maze::child_DBremove ( void )
{
// this function is empty
}

void Maze::alternate_strdat_to_objdat ( int datatypeID, void *dataptr )
{
   //note : only 1 alternate loading so datatype is not considered
   dbs_ADV_Maze_data &tmpdat = *(static_cast<dbs_ADV_Maze_data*> ( dataptr ));
   int i;
   short j;

   strcpy ( p_name, tmpdat.name );
   strcpy ( p_shortname, tmpdat.shortname );
   p_type = tmpdat.type;
   p_xpos = tmpdat.xpos;
   p_ypos = tmpdat.ypos;
   p_mapID = tmpdat.mapID;
   p_music = tmpdat.music;
   p_eprob = tmpdat.eprob;
   p_ebase = tmpdat.ebase;
   p_erange = tmpdat.erange;
   p_einc = tmpdat.einc;

   for ( i = 0 ; i < Maze_NB_WARP ; i++ )
   {
      p_warp [ i ].tag = 0 ;
      p_warp [ i ].target = tmpdat.warp [ i ];
   }

   for ( j = 0 ; j < Maze_NB_SAVEVALUE ; j++ )
      p_savevalue [ j ] = 0;

}*/


/*-------------------------------------------------------------------------*/
/*-                      Non-Class Procedures                             -*/
/*-------------------------------------------------------------------------*/

void Maze::draw_maze_icons ( s_mazetile tmptile )
{
   s_Party_position tpos;
   tpos = party.position ();
   //short y = 0;

/*   switch ( tmptile.special & Maze_FILLING_MASK )
   {
      case Maze_FILLING_FIZZLE :
         draw_sprite( mazebuffer, BMP_icn_fizzle , 608, y );
         y = y + 32;
      break;
      case Maze_FILLING_POISON_GAS :
         draw_sprite( mazebuffer, BMP_icn_poison_gas , 608, y );
         y = y + 32;
      break;
      case Maze_FILLING_FOG :
         draw_sprite( mazebuffer, BMP_icn_fog , 608, y );
         y = y + 32;
      break;
      case Maze_FILLING_WATER :
         draw_sprite( mazebuffer, BMP_icn_water , 608, y );
         y = y + 32;
      break;
      case Maze_FILLING_DARKNESS:
         draw_sprite( mazebuffer, BMP_icn_darkness , 608, y );
         y = y + 32;
      break;
   }
*/
   // not sure since hidden to user somehow.
   // add icon for light
   // add icon for magic bounce
   // add icon for solid

   if ( tmptile.event > 0 )
   {
      /*switch ( p_event [ tmptile.event ] . trigger
         & Maze_EVENT_TRIGICON_MASK )
      {
          case Maze_EVENT_TRIGICON_DOWN :
             draw_sprite( mazebuffer, BMP_icn_sdown , 608, y );
             y = y + 32;
          break;
          case Maze_EVENT_TRIGICON_UP :
             draw_sprite( mazebuffer, BMP_icn_sup , 608, y );
             y = y + 32;
          break;
          case Maze_EVENT_TRIGICON_BOTH :
             draw_sprite( mazebuffer, BMP_icn_supdown , 608, y );
             y = y + 32;
          break;
          case Maze_EVENT_TRIGICON_NORTH :
          break;
          case Maze_EVENT_TRIGICON_EAST :
          break;
          case Maze_EVENT_TRIGICON_SOUTH :
          break;
          case Maze_EVENT_TRIGICON_WEST :
          break;
          case Maze_EVENT_TRIGICON_DOBJECT :
             draw_sprite( mazebuffer, BMP_icn_sobj , 608, y );
             y = y + 32;
          break;
      }*/
   }

/*--- this is the old maze code ---



  //?? note use event information for determining icon, not texture position

  if ( ( tmptile.space & Maze_FLOOR_MTEX_BOTH ) == Maze_FLOOR_MTEX_BOTH )
  {
     draw_sprite( mazebuffer, BMP_icn_supdown , 608, y );
     y = y + 32;
  }
  else
     if ( ( tmptile.space & Maze_FLOOR_MTEX_UP ) == Maze_FLOOR_MTEX_UP )
     {
        draw_sprite( mazebuffer, BMP_icn_sup , 608, y );
        y = y + 32;
     }
     else
        if ( ( tmptile.space & Maze_FLOOR_MTEX_DOWN ) == Maze_FLOOR_MTEX_DOWN )
        {
           draw_sprite( mazebuffer, BMP_icn_sdown , 608, y );
           y = y + 32;
        }
   */

}

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

Maze maze; // create 3d maze object

//          coordonates      Z                 Y                X
s_mazetile mazetile [ Maze_MAXDEPTH ] [ Maze_MAXWIDTH ] [ Maze_MAXWIDTH ];

// Images reference ( no need to save )
//BITMAP *p_texset [ Maze_NB_TEXTURESET ] [ 4 ];
//BITMAP *p_masktex [ Maze_NB_MASKTEX ];
//BITMAP *p_objimg [ Maze_NB_OBJECTIMG ];

s_Maze_wall_info WALL [ 27 ] =
{ // use value<<16 to assign to a fixed type
   { // #define Maze_WALL_AWAY_LEFT_FRONT_3       0
      { { 0,        290<<16, 4,  192<<16, TEXSIZE<<16, 793 },
      { 0,        190<<16, 4,  192<<16, 0,       793 },
      { 30<<16,   190<<16, 4, TEXSIZE<<16, 0,       805 },
      { 30<<16,   290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 805 } },
      false,
      -1,
      0,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_AWAY_RIGHT_FRONT_3      1
      { { 610<<16,  290<<16, 4, 0,      TEXSIZE<<16, 805 },
      { 610<<16,  190<<16, 4, 0,      0,       805 },
      { 640<<16,  190<<16, 4, 64<<16, 0,       793 },
      { 640<<16,  290<<16, 4, 64<<16, TEXSIZE<<16, 793 } },
      false,
      -1,
      1,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_FAR_LEFT_FRONT_3        2
      { { 30<<16,   290<<16, 4, 0,       TEXSIZE<<16, 805 },
      { 30<<16,   190<<16, 4, 0,       0,       805 },
      { 146<<16,  190<<16, 4, TEXSIZE<<16, 0,       838 },
      { 146<<16,  290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 838 } },
      true,
      -1,
      2,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_FAR_RIGHT_FRONT_3       3
      {{  494<<16,  290<<16, 4, 0,       TEXSIZE<<16, 838 },
      { 494<<16,  190<<16, 4, 0,       0,       838 },
      { 610<<16,  190<<16, 4, TEXSIZE<<16, 0,       805 },
      { 610<<16,  290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 805 } },
      true,
      -1,
      3,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_LEFT_FRONT_3            4
      {{  146<<16,  290<<16, 4, 0,       TEXSIZE<<16, 838 },
      { 146<<16,  190<<16, 4, 0,       0,       838 },
      { 262<<16,  190<<16, 4, TEXSIZE<<16, 0,       858 },
      { 262<<16,  290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 858 } },
      true,
      -1,
      4,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_RIGHT_FRONT_3           5
      {{  378<<16,  290<<16, 4, 0,       TEXSIZE<<16, 858 },
      { 378<<16,  190<<16, 4, 0,       0,       858 },
      { 494<<16,  190<<16, 4, TEXSIZE<<16, 0,       838 },
      { 494<<16,  290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 838 } },
      true,
      -1,
      5,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_CENTER_FRONT_3          6
      {{  262<<16,  290<<16, 4, 0,       TEXSIZE<<16, 858 },
      { 262<<16,  190<<16, 4, 0,       0,       858 },
      { 378<<16,  190<<16, 4, TEXSIZE<<16, 0,       858 },
      { 378<<16,  290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 858 } },
      true,
      -1,
      6,
      { 1 , 1 , 2 , 2 },
   },
   //////////////////////////////////////////////////////
   { //#define Maze_WALL_AWAY_LEFT_SIDE_3        7
      { { -140<<16, 320<<16, 3, 0,       TEXSIZE<<16, 850 },
      { -140<<16, 160<<16, 3, 0,       0,       850 },
      { 30<<16,   190<<16, 4, TEXSIZE<<16, 0,       805 },
      { 30<<16,   290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 805 } },
      true,
      -1,
      2,
      { 0 , 0 , 1 , 1 },
   },
   { // #define Maze_WALL_AWAY_RIGHT_SIDE_3       8
      { { 610<<16,  290<<16, 4, 0,       TEXSIZE<<16, 805 },
      { 610<<16,  190<<16, 4, 0,       0,       805 },
      { 780<<16,  160<<16, 3, TEXSIZE<<16, 0,       850 },
      { 780<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 850 } },
      true,
      -1,
      3,
      { 2 , 2 , 3 , 3 },
   },
   { // #define Maze_WALL_FAR_LEFT_SIDE_3         9
      { { 44<<16,   320<<16, 3, 0,       TEXSIZE<<16, 898 },
      { 44<<16,   160<<16, 3, 0,       0,       898 },
      { 146<<16,  190<<16, 4, TEXSIZE<<16, 0,       838 },
      { 146<<16,  290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 838 } },
      true,
      0,
      4,
      { 0 , 0 , 1 , 1 },
   },
   { // #define Maze_WALL_FAR_RIGHT_SIDE_3        10
      { { 494<<16,  290<<16, 4, 0,       TEXSIZE<<16, 838 },
      { 494<<16,  190<<16, 4, 0,       0,       838 },
      { 596<<16,  160<<16, 3, TEXSIZE<<16, 0,       898 },
      { 596<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 898 } },
      true,
      1,
      5,
      { 2 , 2 , 3 , 3 },
   },
   { // #define Maze_WALL_LEFT_SIDE_3             11
      { { 228<<16,  320<<16, 3, 0,       TEXSIZE<<16, 924 },
      { 228<<16,  160<<16, 3, 0,       0,       924 },
      { 262<<16,  190<<16, 4, TEXSIZE<<16, 0,       858 },
      { 262<<16,  290<<16, 4, TEXSIZE<<16, TEXSIZE<<16, 858 } },
      true,
      2,
      6,
      { 0 , 0 , 1 , 1 },
   },
   { // #define Maze_WALL_RIGHT_SIDE_3            12
      { { 378<<16,  290<<16, 4, 0,       TEXSIZE<<16, 858 },
      { 378<<16,  190<<16, 4, 0,       0,       858 },
      { 412<<16,  160<<16, 3, TEXSIZE<<16, 0,       924 },
      { 412<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 924 } },
      true,
      3,
      6,
      { 2 , 2 , 3 , 3 },
   },
   /////////////////////////////////////////////////////
   { // #define Maze_WALL_FAR_LEFT_FRONT_2        13
      { { 0,        320<<16, 3, 204<<16, TEXSIZE<<16, 889 },
      { 0,        160<<16, 3, 204<<16, 0,       889 },
      { 44<<16,   160<<16, 3, TEXSIZE<<16, 0,       898 },
      { 44<<16,   320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 898 } },
      false,
      6,
      7,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_FAR_RIGHT_FRONT_2       14
      { { 596<<16,  320<<16, 3, 0,       TEXSIZE<<16, 898 },
      { 596<<16,  160<<16, 3, 0,       0,       898 },
      { 640<<16,  160<<16, 3, 52<<16,  0,       889 },
      { 640<<16,  320<<16, 3, 52<<16,  TEXSIZE<<16, 889 } },
      false,
      7,
      8,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_LEFT_FRONT_2            15
      { { 44<<16,   320<<16, 3, 0,       TEXSIZE<<16, 898 },
      { 44<<16,   160<<16, 3, 0,       0,       898 },
      { 228<<16,  160<<16, 3, TEXSIZE<<16, 0,       924 },
      { 228<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 924 } },
      true,
      8,
      9,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_RIGHT_FRONT_2           16
      { { 412<<16,  320<<16, 3, 0,       TEXSIZE<<16, 924 },
      { 412<<16,  160<<16, 3, 0,       0,       924 },
      { 596<<16,  160<<16, 3, TEXSIZE<<16, 0,       898 },
      { 596<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 898 } },
      true,
      9,
      10,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_CENTER_FRONT_2          17
      { { 228<<16,  320<<16, 3, 0,       TEXSIZE<<16, 924 },
      { 228<<16,  160<<16, 3, 0,       0,       924 },
      { 412<<16,  160<<16, 3, TEXSIZE<<16, 0,       924 },
      { 412<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 924 } },
      true,
      -1,
      11,
      { 1 , 1 , 2 , 2 },
   },
   ////////////////////////////////////////////////////
   { // #define Maze_WALL_FAR_LEFT_SIDE_2         18
      { { -232<<16, 400<<16, 2, 0,       TEXSIZE<<16, 944 },
      { -232<<16, 80<<16,  2, 0,       0,       944 },
      { 44<<16,   160<<16, 3, TEXSIZE<<16, 0,       898 },
      { 44<<16,   320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 898 } },
      true,
      -1,
      9,
      { 0 , 0 , 1 , 1 },
   },
   { // #define Maze_WALL_FAR_RIGHT_SIDE_2        19
      { { 596<<16,  320<<16, 3, 0,       TEXSIZE<<16, 898 },
      { 596<<16,  160<<16, 3, 0,       0,       898 },
      { 872<<16,  80<<16,  2, TEXSIZE<<16, 0,       944 },
      { 872<<16,  400<<16, 2, TEXSIZE<<16, TEXSIZE<<16, 944 } },
      true,
      -1,
      10,
      { 2 , 2 , 3 , 3 },
   },
   { // #define Maze_WALL_LEFT_SIDE_2             20
      { { 136<<16,  400<<16, 2, 0,       TEXSIZE<<16, 990 },
      { 136<<16,  80<<16,  2, 0,       0,       990 },
      { 228<<16,  160<<16, 3, TEXSIZE<<16, 0,       924 },
      { 228<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 924 } },
      true,
      4,
      11,
      { 0 , 0 , 1 , 1 },
   },
   { // #define Maze_WALL_RIGHT_SIDE_2            21
      { { 412<<16,  320<<16, 3, 0,       TEXSIZE<<16, 924 },
      { 412<<16,  160<<16, 3, 0,       0,       924 },
      { 504<<16,  80<<16,  2, TEXSIZE<<16, 0,       990 },
      { 504<<16,  400<<16, 2, TEXSIZE<<16, TEXSIZE<<16, 990 } },
      true,
      5,
      11,
      { 2 , 2 , 3 , 3 },
   },
   ///////////////////////////////////////////////////
   { // #define Maze_WALL_LEFT_FRONT_1            22
      { { 0,        400<<16, 2, 158<<16, TEXSIZE<<16, 974 },
      { 0,        80<<16,  2, 158<<16, 0,       974 },
      { 136<<16,  80<<16,  2, TEXSIZE<<16, 0,       990 },
      { 136<<16,  400<<16, 2, TEXSIZE<<16, TEXSIZE<<16, 990 } },
      false,
      10,
      12,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_RIGHT_FRONT_1           23
      { { 504<<16,  400<<16, 2, 0,       TEXSIZE<<16, 990 },
      { 504<<16,  80<<16,  2, 0,       0,       990 },
      { 640<<16,  80<<16,  2, 98<<16,  0,       974 },
      { 640<<16,  400<<16, 2, 98<<16,  TEXSIZE<<16, 974 } },
      false,
      11,
      13,
      { 1 , 1 , 2 , 2 },
   },
   { // #define Maze_WALL_CENTER_FRONT_1          24
      { { 136<<16,  400<<16, 2, 0,       TEXSIZE<<16, 990 },
      { 136<<16,  80<<16,  2, 0,       0,       990 },
      { 504<<16,  80<<16,  2, TEXSIZE<<16, 0,       990 },
      { 504<<16,  400<<16, 2, TEXSIZE<<16, TEXSIZE<<16, 990 } },
      true,
      -1,
      14,
      { 1 , 1 , 2 , 2 },
   },
   ///////////////////////////////////////////////////
   { // #define Maze_WALL_LEFT_SIDE_1             25
      { { 0,        520<<16, 1, 64<<16,  TEXSIZE<<16, 1023 },
      { 0,        -40<<16, 1, 64<<16,  0,       1023 },
      { 136<<16,  80<<16,  2, TEXSIZE<<16, 0,       990 /*990*/},
      { 136<<16,  400<<16, 2, TEXSIZE<<16, TEXSIZE<<16, 990 /*990*/} },
      false,
      -1,
      14,
      { 0 , 0 , 1 , 1 },
   },
   { // #define Maze_WALL_RIGHT_SIDE_1            26
      { { 504<<16,  400<<16, 2, 0,       TEXSIZE<<16, 990 /*990*/},
      { 504<<16,  80<<16,  2, 0,       0,       990   /*990*/},
      { 640<<16,  -40<<16, 1,  192<<16, 0,       1023 },
      { 640<<16,  520<<16, 1,  192<<16, TEXSIZE<<16, 1023 } },
      false,
      -1,
      14,
      { 2 , 2 , 3 , 3 },
   },
};

s_Maze_wall_info THICKWALL [ Maze_NB_THICKWALL ] =
/*typedef struct s_Maze_wall_info
{
   V3D vertex [ 4 ];
   bool flipok; // indicates if this wall can be flipped
   int thickwallID; // thickwall id to use with wall, -1 = not thick wall
   int ltile; // ID of the tile refered to in the table
   int lvtex [ 4 ]; // indicates which light vertex to use in the tile
}s_Maze_wall_info;*/
{
   { // thickness of Maze_WALL_FAR_LEFT_SIDE_3         0
      {{ 36<<16,  320<<16, 3, 240<<16, TEXSIZE<<16, 898 },
      { 36<<16,  160<<16, 3, 240<<16, 0,       898 },
      { 44<<16,  160<<16, 3, TEXSIZE<<16, 0,       898 },
      { 44<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 898 }},
      false,
      -1,
      4,
      { 0 , 0 , 0 , 0 },
   },
   { // thickness of Maze_WALL_FAR_RIGHT_SIDE_3        1
      {{ 596<<16,  320<<16, 3, 0,       TEXSIZE<<16, 898 },
      { 596<<16,  160<<16, 3, 0,       0,       898 },
      { 604<<16,  160<<16, 3, 16<<16,   0,       898 },
      { 604<<16,  320<<16, 3, 16<<16,   TEXSIZE<<16, 898 }},
      false,
      -1,
      5,
      { 3 , 3 , 3 , 3 },
   },
   { // thickness of Maze_WALL_LEFT_SIDE_3             2
      {{ 220<<16,  320<<16, 3, 240<<16, TEXSIZE<<16, 924 },
      { 220<<16,  160<<16, 3, 240<<16, 0,       924 },
      { 228<<16,  160<<16, 3, TEXSIZE<<16, 0,       924 },
      { 228<<16,  320<<16, 3, TEXSIZE<<16, TEXSIZE<<16, 924 }},
      false,
      -1,
      6,
      { 0 , 0 , 0 , 0 },
   },
   { // thickness of Maze_WALL_RIGHT_SIDE_3            3
      {{ 412<<16,  320<<16, 3, 0,       TEXSIZE<<16, 924 },
      { 412<<16,  160<<16, 3, 0,       0,       924 },
      { 420<<16,  160<<16, 3, 16<<16,   0,       924 },
      { 420<<16,  320<<16, 3, 16<<16,   TEXSIZE<<16, 924 }},
      false,
      -1,
      6,
      { 3 , 3 , 3 , 3 },
   },
   { // thickness of Maze_WALL_LEFT_SIDE_2             4
      {{ 120<<16,  400<<16, 2, 240<<16, TEXSIZE<<16, 990 },
      { 120<<16,  80<<16,  2, 240<<16, 0,       990 },
      { 136<<16,  80<<16,  2, TEXSIZE<<16, 0,       990 },
      { 136<<16,  400<<16, 2, TEXSIZE<<16, TEXSIZE<<16, 990 }},
      false,
      -1,
      11,
      { 0 , 0 , 0 , 0 },
   },
   { // thickness of Maze_WALL_RIGHT_SIDE_2            5
      {{ 504<<16,  400<<16, 2, 0,       TEXSIZE<<16, 990 },
      { 504<<16,  80<<16,  2, 0,       0,       990 },
      { 520<<16,  80<<16,  2, 16<<16,   0,       990 },
      { 520<<16,  400<<16, 2, 16<<16,   TEXSIZE<<16, 990 }},
      false,
      -1,
      11,
      { 3 , 3 , 3 , 3 },
   },
   { // thickness of Maze_WALL_FAR_LEFT_FRONT_2            6
      {{ 44<<16,  320<<16, 2, 0,       TEXSIZE<<16, 898 },
      { 44<<16,  160<<16,  2, 0,       0,       898 },
      { 48<<16,  162<<16,  2, 16<<16,   0,       898 },
      { 48<<16,  318<<16, 2, 16<<16,   TEXSIZE<<16, 898 }},
      false,
      -1,
      4,
      { 3 , 3 , 3 , 3 },
   },
   { // thickness of Maze_WALL_FAR_RIGHT_FRONT_2            7
      {{ 592<<16,  318<<16, 2, 240<<16,       TEXSIZE<<16, 898 },
      { 592<<16,  162<<16,  2, 240<<16,       0,       898 },
      { 596<<16,  160<<16,  2, TEXSIZE<<16,   0,       898 },
      { 596<<16,  320<<16, 2, TEXSIZE<<16,   TEXSIZE<<16, 898 }},
      false,
      -1,
      5,
      { 3 , 3 , 3 , 3 },
   },
   { // thickness of Maze_WALL_LEFT_FRONT_2            8
      {{ 228<<16,  320<<16, 2, 0,       TEXSIZE<<16, 924 },
      { 228<<16,  160<<16,  2, 0,       0,       924 },
      { 232<<16,  162<<16,  2, 16<<16,   0,       924 },
      { 232<<16,  318<<16, 2, 16<<16,   TEXSIZE<<16, 924 }},
      false,
      -1,
      6,
      { 3 , 3 , 3 , 3 },
   },
   { // thickness of Maze_WALL_RIGHT_FRONT_2            9
      {{ 408<<16,  318<<16, 2, 240<<16,       TEXSIZE<<16, 924 },
      { 408<<16,  162<<16,  2, 240<<16,       0,       924 },
      { 412<<16,  160<<16,  2, TEXSIZE<<16,   0,       924 },
      { 412<<16,  320<<16, 2, TEXSIZE<<16,   TEXSIZE<<16, 924 }},
      false,
      -1,
      6,
      { 3 , 3 , 3 , 3 },
   },
   { // thickness of Maze_WALL_LEFT_FRONT_1            10
      {{ 136<<16,  400<<16, 2, 0,       TEXSIZE<<16, 990 },
      { 136<<16,  80<<16,  2, 0,       0,       990 },
      { 142<<16,  84<<16,  2, 16<<16,   0,       990 },
      { 142<<16,  396<<16, 2, 16<<16,   TEXSIZE<<16, 990 }},
      false,
      -1,
      11,
      { 3 , 3 , 3 , 3 },
   },
   { // thickness of Maze_WALL_RIGHT_FRONT_1            11
      {{ 498<<16,  396<<16, 2, 240<<16,       TEXSIZE<<16, 990 },
      { 498<<16,  84<<16,  2, 240<<16,       0,       990 },
      { 504<<16,  80<<16,  2, TEXSIZE<<16,   0,       990 },
      { 504<<16,  400<<16, 2, TEXSIZE<<16,   TEXSIZE<<16, 990 }},
      false,
      -1,
      11,
      { 3 , 3 , 3 , 3 },
   },


};

V3D SCREEN [ 4 ] =
{
   { 0<<16,   480<<16,  2, 0,       TEXSIZE<<16, 1023 },
   { 0<<16,     0<<16,  2, 0,       0,       1023 },
   { 640<<16,   0<<16,  2, TEXSIZE<<16, 0,       1023 },
   { 640<<16, 480<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 1023 },
};

s_Maze_floor_info FLOOR [ 15 ] =
{
   {// Maze_FLOOR_AWAY_LEFT_3             0
      { { -324<<16, 320<<16,  3, 0,       TEXSIZE<<16, 846 },
      {  -86<<16, 290<<16,  3, 0,       0,       772 },
      {   30<<16, 290<<16,  3, TEXSIZE<<16, 0,       805 },
      { -140<<16, 320<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 872 } },
      0,
   },

   {// Maze_FLOOR_AWAY_RIGHT_3            1
      { { 780<<16, 320<<16,  3, 0,       TEXSIZE<<16, 872 },
      { 610<<16, 290<<16,  3, 0,       0,       805 },
      { 726<<16, 290<<16,  3, TEXSIZE<<16, 0,       772 },
      { 964<<16, 320<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 846 } },
      1,
   },

   {// Maze_FLOOR_FAR_LEFT_3              2
      { { -140<<16, 320<<16,  3, 0,       TEXSIZE<<16, 872 },
      {   30<<16, 290<<16,  3, 0,       0,       805 },
      {  146<<16, 290<<16,  3, TEXSIZE<<16, 0,       838 },
      {   44<<16, 320<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 898 } },
      2,
   },

   {// Maze_FLOOR_FAR_RIGHT_3             3
      { { 596<<16, 320<<16,  3, 0,       TEXSIZE<<16, 898 },
      { 494<<16, 290<<16,  3, 0,       0,       838 },
      { 610<<16, 290<<16,  3, TEXSIZE<<16, 0,       805 },
      { 780<<16, 320<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 872 } },
      3,
   },

   {// Maze_FLOOR_LEFT_3                  4
      { {  44<<16, 320<<16,  3, 0,       TEXSIZE<<16, 898 },
      { 146<<16, 290<<16,  3, 0,       0,       838 },
      { 262<<16, 290<<16,  3, TEXSIZE<<16, 0,       858 },
      { 228<<16, 320<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 924 } },
      4,
   },

   {// Maze_FLOOR_RIGHT_3                 5
      { { 412<<16, 320<<16,  3, 0,       TEXSIZE<<16, 924 },
      { 378<<16, 290<<16,  3, 0,       0,       858 },
      { 494<<16, 290<<16,  3, TEXSIZE<<16, 0,       838 },
      { 596<<16, 320<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 898 } },
      5,
   },

   {// Maze_FLOOR_CENTER_3                6
      { { 228<<16, 320<<16,  3, 0,       TEXSIZE<<16, 924 },
      { 262<<16, 290<<16,  3, 0,       0,       858 },
      { 378<<16, 290<<16,  3, TEXSIZE<<16, 0,       858 },
      { 412<<16, 320<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 924 } },
      6,
   },

   {// Maze_FLOOR_FAR_LEFT_2              7
      { { -600<<16, 400<<16,  2, 0,       TEXSIZE<<16, 926 },
      { -140<<16, 320<<16,  2, 0,       0,       872 },
      {   44<<16, 320<<16,  2, TEXSIZE<<16, 0,       898 },
      { -232<<16, 400<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 958 } },
      7,
   },

   {// Maze_FLOOR_FAR_RIGHT_2             8
      { {  872<<16, 400<<16,  2, 0,       TEXSIZE<<16, 958 },
      {  596<<16, 320<<16,  2, 0,       0,       898 },
      {  780<<16, 320<<16,  2, TEXSIZE<<16, 0,       872 },
      { 1240<<16, 400<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 926 } },
      8,
   },

   {// Maze_FLOOR_LEFT_2                  9
      { { -232<<16, 400<<16,  2, 0,       TEXSIZE<<16, 958 },
      {  44<<16,  320<<16,  2, 0,       0,       898 },
      {  228<<16, 320<<16,  2, TEXSIZE<<16, 0,       924 },
      {  136<<16, 400<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 990 } },
      9,
   },

   {// Maze_FLOOR_RIGHT_2                10
      { { 504<<16, 400<<16,  2, 0,       TEXSIZE<<16, 990 },
      { 412<<16, 320<<16,  2, 0,       0,       924 },
      { 596<<16, 320<<16,  2, TEXSIZE<<16, 0,       898 },
      { 872<<16, 400<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 958 } },
      10,
   },

   {// Maze_FLOOR_CENTER_2               11
      { { 136<<16, 400<<16,  2, 0,       TEXSIZE<<16, 990 },
      { 228<<16, 320<<16,  2, 0,       0,       924 },
      { 412<<16, 320<<16,  2, TEXSIZE<<16, 0,       924 },
      { 504<<16, 400<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 990 } },
      11,
   },

   {// Maze_FLOOR_LEFT_1                 12
      { {  -680<<16, 520<<16,  1, 0,       TEXSIZE<<16, 985 },
      {  -232<<16, 400<<16,  1, 0,       0,       958 },
      {  136<<16,  400<<16,  1, TEXSIZE<<16, 0,       990 },
      {  -10<<16,  520<<16,  1, TEXSIZE<<16, TEXSIZE<<16, 1023 } },
      12,
   },

   {// Maze_FLOOR_RIGHT_1                13
      { {  650<<16, 520<<16,  1, 0,       TEXSIZE<<16, 1023 },
      {  504<<16, 400<<16,  1, 0,       0,       990 },
      {  872<<16, 400<<16,  1, TEXSIZE<<16, 0,       958 },
      { 1320<<16, 520<<16,  1, TEXSIZE<<16, TEXSIZE<<16, 985 } },
      13,
   },

   {// Maze_FLOOR_CENTER_1               14
      { {  -10<<16, 520<<16,  1, 0,       TEXSIZE<<16, 1023 },
      {  136<<16, 400<<16,  1, 0,       0,       990 },
      {  504<<16, 400<<16,  1, TEXSIZE<<16, 0,       990 },
      {  650<<16, 520<<16,  1, TEXSIZE<<16, TEXSIZE<<16, 1023 } },
      14,
   }
};

s_Maze_floor_info CEILING [ 15 ] =
{
   {// Maze_CEILING_AWAY_LEFT_3             0
      { { -324<<16, 160<<16,  3, 0,       TEXSIZE<<16, 846 },
      {  -86<<16, 190<<16,  3, 0,       0,       772 },
      {   30<<16, 190<<16,  3, TEXSIZE<<16, 0,       805 },
      { -140<<16, 160<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 872 } },
      0,
   },

   {// Maze_CEILING_AWAY_RIGHT_3            1
      { { 780<<16, 160<<16,  3, 0,       TEXSIZE<<16, 872 },
      { 610<<16, 190<<16,  3, 0,       0,       805 },
      { 726<<16, 190<<16,  3, TEXSIZE<<16, 0,       772 },
      { 964<<16, 160<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 846 } },
      1,
   },

   {// Maze_CEILING_FAR_LEFT_3              2
      { { -140<<16, 160<<16,  3, 0,       TEXSIZE<<16, 872 },
      {   30<<16, 190<<16,  3, 0,       0,       805 },
      {  146<<16, 190<<16,  3, TEXSIZE<<16, 0,       838 },
      {   44<<16, 160<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 898 } },
      2,
   },

   {// Maze_CEILING_FAR_RIGHT_3             3
      { { 596<<16, 160<<16,  3, 0,       TEXSIZE<<16, 898 },
      { 494<<16, 190<<16,  3, 0,       0,       838 },
      { 610<<16, 190<<16,  3, TEXSIZE<<16, 0,       805 },
      { 780<<16, 160<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 872 } },
      3,
   },

   {// Maze_CEILING_LEFT_3                  4
      { {  44<<16, 160<<16,  3, 0,       TEXSIZE<<16, 898 },
      { 146<<16, 190<<16,  3, 0,       0,       838 },
      { 262<<16, 190<<16,  3, TEXSIZE<<16, 0,       858 },
      { 228<<16, 160<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 924 } },
      4,
   },

   {// Maze_CEILING_RIGHT_3                 5
      { { 412<<16, 160<<16,  3, 0,       TEXSIZE<<16, 924 },
      { 378<<16, 190<<16,  3, 0,       0,       858 },
      { 494<<16, 190<<16,  3, TEXSIZE<<16, 0,       838 },
      { 596<<16, 160<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 898 } },
      5,
   },

   {// Maze_CEILING_CENTER_3                6
      { { 228<<16, 160<<16,  3, 0,       TEXSIZE<<16, 924 },
      { 262<<16, 190<<16,  3, 0,       0,       858 },
      { 378<<16, 190<<16,  3, TEXSIZE<<16, 0,       858 },
      { 412<<16, 160<<16,  3, TEXSIZE<<16, TEXSIZE<<16, 924 } },
      6,
   },

   {// Maze_CEILING_FAR_LEFT_2              7
      { { -600<<16,  80<<16,  2, 0,       TEXSIZE<<16, 926 },
      { -140<<16, 160<<16,  2, 0,       0,       872 },
      {   44<<16, 160<<16,  2, TEXSIZE<<16, 0,       898 },
      { -232<<16,  80<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 958 } },
      7,
   },

   {// Maze_CEILING_FAR_RIGHT_2             8
      { {  872<<16,  80<<16,  2, 0,       TEXSIZE<<16, 958 },
      {  596<<16, 160<<16,  2, 0,       0,       898 },
      {  780<<16, 160<<16,  2, TEXSIZE<<16, 0,       872 },
      { 1240<<16,  80<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 926 } },
      8,
   },

   {// Maze_CEILING_LEFT_2                  9
      { { -232<<16,  80<<16,  2, 0,       TEXSIZE<<16, 958 },
      {  44<<16,  160<<16,  2, 0,       0,       898 },
      {  228<<16, 160<<16,  2, TEXSIZE<<16, 0,       924 },
      {  136<<16,  80<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 990 } },
      9,
   },

   {// Maze_CEILING_RIGHT_2                10
      { { 504<<16,  80<<16,  2, 0,       TEXSIZE<<16, 990 },
      { 412<<16, 160<<16,  2, 0,       0,       924 },
      { 596<<16, 160<<16,  2, TEXSIZE<<16, 0,       898 },
      { 872<<16,  80<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 958 } },
      10,
   },

   {// Maze_CEILING_CENTER_2               11
      { { 136<<16,  80<<16,  2, 0,       TEXSIZE<<16, 990 },
      { 228<<16, 160<<16,  2, 0,       0,       924 },
      { 412<<16, 160<<16,  2, TEXSIZE<<16, 0,       924 },
      { 504<<16,  80<<16,  2, TEXSIZE<<16, TEXSIZE<<16, 990 } },
      11,
   },

   {// Maze_CEILING_LEFT_1                 12
      { {  -680<<16, -40<<16,  1, 0,       TEXSIZE<<16, 985 },
      {  -232<<16,  80<<16,  1, 0,       0,       958 },
      {  136<<16,   80<<16,  1, TEXSIZE<<16, 0,       990 },
      {  -10<<16,  -40<<16,  1, TEXSIZE<<16, TEXSIZE<<16, 1023 } },
      12,
   },

   {// Maze_CEILING_RIGHT_1                13
      { {  650<<16, -40<<16,  1, 0,       TEXSIZE<<16, 1023 },
      {  504<<16,  80<<16,  1, 0,       0,       990 },
      {  872<<16,  80<<16,  1, TEXSIZE<<16, 0,       958 },
      { 1320<<16, -40<<16,  1, TEXSIZE<<16, TEXSIZE<<16, 985 } },
      13,
   },

   {// Maze_CEILING_CENTER_1               14
      { {  -10<<16, -40<<16,  1, 0,       TEXSIZE<<16, 1023 },
      {  136<<16,  80<<16,  1, 0,       0,       990 },
      {  504<<16,  80<<16,  1, TEXSIZE<<16, 0,       990 },
      {  650<<16, -40<<16,  1, TEXSIZE<<16, TEXSIZE<<16, 1023 } },
      14,
   }
};


s_Maze_object_info OBJECT [ 15 ] [ 9 ] =
{
   // posx, posy, width, height
   // view tile 0
   { {   -169,   182,   115,   115 },
     {   -139,   182,   115,   115 },
     {   -103,   182,   115,   115 },
     {   -232,  175,   130,   130 },
     {   -195,  175,   130,   130 },
     {   -158,  175,   130,   130 },
     {   -295,  167,   145,   145 },
     {   -254,  167,   145,   145 },
     {   -213,  167,   145,   145 }
   },
   // view tile 1
   { {   628,   182,   115,   115 },
     {   671,   182,   115,   115 },
     {   694,   182,   115,   115 },
     {   668,   175,   130,   130 },
     {   705,   175,   130,   130 },
     {   742,   175,   130,   130 },
     {   707,   167,   145,   145 },
     {   748,   167,   145,   145 },
     {   789,   167,   145,   145 }
   },
   // view tile 2
   { {   -37,   182,   115,   115 },
     {    -4,   182,   115,   115 },
     {    29,   182,   115,   115 },
     {   -82,   175,   130,   130 },
     {   -45,   175,   130,   130 },
     {    -8,   175,   130,   130 },
     {  -128,   167,   145,   145 },
     {   -92,   167,   145,   145 },
     {   -46,   167,   145,   145 }
   },
   // view tile 3
   { {   495,   182,   115,   115 },
     {   528,   182,   115,   115 },
     {   561,   182,   115,   115 },
     {   518,   175,   130,   130 },
     {   555,   175,   130,   130 },
     {   592,   175,   130,   130 },
     {   540,   167,   145,   145 },
     {   581,   167,   145,   145 },
     {   622,   167,   145,   145 }
   },
   // view tile 4
   { {   96,   182,   115,   115 },
     {   129,   182,   115,   115 },
     {   162,   182,   115,   115 },
     {    68,   175,   130,   130 },
     {   105,   175,   130,   130 },
     {   142,   175,   130,   130 },
     {   39,    167,   145,   145 },
     {   80,    167,   145,   145 },
     {   121,   167,   145,   145 }
   },
   // view tile 5
   { {   362,   182,   115,   115 },
     {   395,   182,   115,   115 },
     {   428,   182,   115,   115 },
     {   365,   175,   130,   130 },
     {   405,   175,   130,   130 },
     {   442,   175,   130,   130 },
     {   373,   167,   145,   145 },
     {   414,   167,   145,   145 },
     {   455,   167,   145,   145 }
   },
   // view tile 6
   { {   229,   182,   115,   115 },
     {   262,   182,   115,   115 },
     {   295,   182,   115,   115 },
     {   218,   175,   130,   130 },
     {   255,   175,   130,   130 },
     {   292,   175,   130,   130 },
     {   206,   167,   145,   145 },
     {   247,   167,   145,   145 },
     {   288,   167,   145,   145 }
   },
   // view tile 7
   { {   -297,  140,   200,   200 },
     {   -239,  140,   200,   200 },
     {   -182,  140,   200,   200 },
     {   -421,  120,   240,   240 },
     {   -352,  120,   240,   240 },
     {   -283,  120,   240,   240 },
     {   -544,  100,   280,   280 },
     {   -463,  100,   280,   280 },
     {   -383,  100,   280,   280 }
   },
   // view tile 8
   { {   622,   140,   200,   200 },
     {   679,   140,   200,   200 },
     {   737,   140,   200,   200 },
     {   683,   120,   240,   240 },
     {   752,   120,   240,   240 },
     {   821,   120,   240,   240 },
     {   741,   100,   280,   280 },
     {   821,   100,   280,   280 },
     {   902,   100,   280,   280 }
   },
   // view tile 9
   { {   -67,   140,   200,   200 },
     {   -10,   140,   200,   200 },
     {    48,   140,   200,   200 },
     {   -145,  120,   240,   240 },
     {   -76,   120,   240,   240 },
     {    -7,   120,   240,   240 },
     {   -222,  100,   280,   280 },
     {   -139,  100,   280,   280 },
     {   -61,   100,   280,   280 }
   },
   // view tile 10
   { {   392,   140,   200,   200 },
     {   449,   140,   200,   200 },
     {   507,   140,   200,   200 },
     {   407,   120,   240,   240 },
     {   476,   120,   240,   240 },
     {   545,   120,   240,   240 },
     {   420,   100,   280,   280 },
     {   500,   100,   280,   280 },
     {   580,   100,   280,   280 }
   },
   // view tile 11
   { {   163,   140,   200,   200 },
     {   220,   140,   200,   200 },
     {   277,   140,   200,   200 },
     {   131,   120,   240,   240 },
     {   200,   120,   240,   240 },
     {   269,   120,   240,   240 },
     {   100,   100,   280,   280 },
     {   180,   100,   280,   280 },
     {   260,   100,   280,   280 }
   },
   // view tile 12
   { {   -425,   50,   380,   380 },
     {   -314,   50,   380,   380 },
     {   -203,   50,   380,   380 },
     {   -555,   20,   440,   440 },
     {   -424,   20,   440,   440 },
     {   -293,   20,   440,   440 },
     {   0,   0,   0,   0 },
     {   0,   0,   0,   0 },
     {   0,   0,   0,   0 }
   },
   // view tile 13
   { {   463,   50,   380,   380 },
     {   574,   50,   380,   380 },
     {   685,   50,   380,   380 },
     {   493,   20,   440,   440 },
     {   624,   20,   440,   440 },
     {   755,   20,   440,   440 },
     {   0,   0,   0,   0 },
     {   0,   0,   0,   0 },
     {   0,   0,   0,   0 }
   },
   // view tile 14
   { {   19,    50,   380,   380 },
     {   130,   50,   380,   380 },
     {   241,   50,   380,   380 },
     {   -31,   20,   440,   440 },
     {   100,   20,   440,   440 },
     {   231,   20,   440,   440 },
     {   0,   0,   0,   0 },
     {   0,    0,   0,   0 },
     {   0,   0,   0,   0 }
   },
};

s_Maze_object_anchor FLOOR_OBJECT [ 15 ] [ 9 ] =
{
   { // Tile 0
      { -112,  297,   44 },
      {  -82,  297,   44 },
      {  -46,  297,   44 },
      { -167,  305,   50 },
      { -130,  305,   50 },
      {  -93,  305,   50 },
      { -223,  312,   56 },
      { -182,  312,   56 },
      { -141,  312,   56 },
   },
   { // Tile 1
      {  685,  297,   44 },
      {  728,  297,   44 },
      {  751,  297,   44 },
      {  733,  305,   50 },
      {  770,  305,   50 },
      {  807,  305,   50 },
      {  779,  312,   56 },
      {  820,  312,   56 },
      {  861,  312,   56 },
   },
   { // Tile 2
      {   20,  297,   44 },
      {   53,  297,   44 },
      {   86,  297,   44 },
      {  -17,  305,   50 },
      {   20,  305,   50 },
      {   57,  305,   50 },
      {  -56,  312,   56 },
      {  -20,  312,   56 },
      {   26,  312,   56 },
   },
   { // Tile 3
      {  552,  297,   44 },
      {  585,  297,   44 },
      {  618,  297,   44 },
      {  583,  305,   50 },
      {  620,  305,   50 },
      {  657,  305,   50 },
      {  612,  312,   56 },
      {  653,  312,   56 },
      {  694,  312,   56 },
   },
   { // Tile 4
      {  153,  297,   44 },
      {  186,  297,   44 },
      {  219,  297,   44 },
      {  133,  305,   50 },
      {  170,  305,   50 },
      {  207,  305,   50 },
      {  111,  312,   56 },
      {  152,  312,   56 },
      {  193,  312,   56 },
   },
   { // Tile 5
      {  419,  297,   44 },
      {  452,  297,   44 },
      {  485,  297,   44 },
      {  430,  305,   50 },
      {  470,  305,   50 },
      {  507,  305,   50 },
      {  445,  312,   56 },
      {  486,  312,   56 },
      {  527,  312,   56 },
   },
   { // Tile 6
      {  286,  297,   44 },
      {  319,  297,   44 },
      {  352,  297,   44 },
      {  283,  305,   50 },
      {  320,  305,   50 },
      {  357,  305,   50 },
      {  278,  312,   56 },
      {  319,  312,   56 },
      {  360,  312,   56 },
   },
   { // Tile 7
      { -197,  340,   78 },
      { -139,  340,   78 },
      {  -82,  340,   78 },
      { -301,  360,   93 },
      { -232,  360,   93 },
      { -163,  360,   93 },
      { -404,  380,  109 },
      { -323,  380,  109 },
      { -243,  380,  109 },
   },
   { // Tile 8
      {  722,  340,   78 },
      {  779,  340,   78 },
      {  837,  340,   78 },
      {  803,  360,   93 },
      {  872,  360,   93 },
      {  941,  360,   93 },
      {  881,  380,  109 },
      {  961,  380,  109 },
      { 1042,  380,  109 },
   },
   { // Tile 9
      {   33,  340,   78 },
      {   90,  340,   78 },
      {  148,  340,   78 },
      {  -25,  360,   93 },
      {   44,  360,   93 },
      {  113,  360,   93 },
      {  -82,  380,  109 },
      {    1,  380,  109 },
      {   79,  380,  109 },
   },
   { // Tile 10
      {  492,  340,   78 },
      {  549,  340,   78 },
      {  607,  340,   78 },
      {  527,  360,   93 },
      {  596,  360,   93 },
      {  665,  360,   93 },
      {  560,  380,  109 },
      {  640,  380,  109 },
      {  720,  380,  109 },
   },
   { // Tile 11
      {  263,  340,   78 },
      {  320,  340,   78 },
      {  377,  340,   78 },
      {  251,  360,   93 },
      {  320,  360,   93 },
      {  389,  360,   93 },
      {  240,  380,  109 },
      {  320,  380,  109 },
      {  400,  380,  109 },
   },
   { // Tile 12
      { -235,  430,  148 },
      { -124,  430,  148 },
      {  -13,  430,  148 },
      { -335,  460,  171 },
      { -204,  460,  171 },
      {  -73,  460,  171 },
      {    0,    0,    0 },
      {    0,    0,    0 },
      {    0,    0,    0 },
   },
   { // Tile 13
      {  653,  430,  148 },
      {  764,  430,  148 },
      {  875,  430,  148 },
      {  713,  460,  171 },
      {  844,  460,  171 },
      {  975,  460,  171 },
      {    0,    0,    0 },
      {    0,    0,    0 },
      {    0,    0,    0 },
   },
   { // Tile 14
      {  209,  430,  148 },
      {  320,  430,  148 },
      {  431,  430,  148 },
      {  189,  460,  171 },
      {  320,  460,  171 },
      {  451,  460,  171 },
      {    0,    0,    0 },
      {    0,    0,    0 },
      {    0,    0,    0 },
   },
}; // Anchor points for floor objects


s_Maze_object_anchor CEILING_OBJECT [ 15 ] [ 9 ] =
{
   { // Tile 0
      { -112,  182,   44 },
      {  -82,  182,   44 },
      {  -46,  182,   44 },
      { -167,  175,   50 },
      { -130,  175,   50 },
      {  -93,  175,   50 },
      { -223,  167,   56 },
      { -182,  167,   56 },
      { -141,  167,   56 },
   },
   { // Tile 1
      {  685,  182,   44 },
      {  728,  182,   44 },
      {  751,  182,   44 },
      {  733,  175,   50 },
      {  770,  175,   50 },
      {  807,  175,   50 },
      {  779,  167,   56 },
      {  820,  167,   56 },
      {  861,  167,   56 },
   },
   { // Tile 2
      {   20,  182,   44 },
      {   53,  182,   44 },
      {   86,  182,   44 },
      {  -17,  175,   50 },
      {   20,  175,   50 },
      {   57,  175,   50 },
      {  -56,  167,   56 },
      {  -20,  167,   56 },
      {   26,  167,   56 },
   },
   { // Tile 3
      {  552,  182,   44 },
      {  585,  182,   44 },
      {  618,  182,   44 },
      {  583,  175,   50 },
      {  620,  175,   50 },
      {  657,  175,   50 },
      {  612,  167,   56 },
      {  653,  167,   56 },
      {  694,  167,   56 },
   },
   { // Tile 4
      {  153,  182,   44 },
      {  186,  182,   44 },
      {  219,  182,   44 },
      {  133,  175,   50 },
      {  170,  175,   50 },
      {  207,  175,   50 },
      {  111,  167,   56 },
      {  152,  167,   56 },
      {  193,  167,   56 },
   },
   { // Tile 5
      {  419,  182,   44 },
      {  452,  182,   44 },
      {  485,  182,   44 },
      {  430,  175,   50 },
      {  470,  175,   50 },
      {  507,  175,   50 },
      {  445,  167,   56 },
      {  486,  167,   56 },
      {  527,  167,   56 },
   },
   { // Tile 6
      {  286,  182,   44 },
      {  319,  182,   44 },
      {  352,  182,   44 },
      {  283,  175,   50 },
      {  320,  175,   50 },
      {  357,  175,   50 },
      {  278,  167,   56 },
      {  319,  167,   56 },
      {  360,  167,   56 },
   },
   { // Tile 7
      { -197,  140,   78 },
      { -139,  140,   78 },
      {  -82,  140,   78 },
      { -301,  120,   93 },
      { -232,  120,   93 },
      { -163,  120,   93 },
      { -404,  100,  109 },
      { -323,  100,  109 },
      { -243,  100,  109 },
   },
   { // Tile 8
      {  722,  140,   78 },
      {  779,  140,   78 },
      {  837,  140,   78 },
      {  803,  120,   93 },
      {  872,  120,   93 },
      {  941,  120,   93 },
      {  881,  100,  109 },
      {  961,  100,  109 },
      { 1042,  100,  109 },
   },
   { // Tile 9
      {   33,  140,   78 },
      {   90,  140,   78 },
      {  148,  140,   78 },
      {  -25,  120,   93 },
      {   44,  120,   93 },
      {  113,  120,   93 },
      {  -82,  100,  109 },
      {    1,  100,  109 },
      {   79,  100,  109 },
   },
   { // Tile 10
      {  492,  140,   78 },
      {  549,  140,   78 },
      {  607,  140,   78 },
      {  527,  120,   93 },
      {  596,  120,   93 },
      {  665,  120,   93 },
      {  560,  100,  109 },
      {  640,  100,  109 },
      {  720,  100,  109 },
   },
   { // Tile 11
      {  263,  140,   78 },
      {  320,  140,   78 },
      {  377,  140,   78 },
      {  251,  120,   93 },
      {  320,  120,   93 },
      {  389,  120,   93 },
      {  240,  100,  109 },
      {  320,  100,  109 },
      {  400,  100,  109 },
   },
   { // Tile 12
      { -235,   50,  148 },
      { -124,   50,  148 },
      {  -13,   50,  148 },
      { -335,   20,  171 },
      { -204,   20,  171 },
      {  -73,   20,  171 },
      {    0,    0,    0 },
      {    0,    0,    0 },
      {    0,    0,    0 },
   },
   { // Tile 13
      {  653,   50,  148 },
      {  764,   50,  148 },
      {  875,   50,  148 },
      {  713,   20,  171 },
      {  844,   20,  171 },
      {  975,   20,  171 },
      {    0,    0,    0 },
      {    0,    0,    0 },
      {    0,    0,    0 },
   },
   { // Tile 14
      {  209,   50,  148 },
      {  320,   50,  148 },
      {  431,   50,  148 },
      {  189,   20,  171 },
      {  320,   20,  171 },
      {  451,   20,  171 },
      {    0,    0,    0 },
      {    0,    0,    0 },
      {    0,    0,    0 },
   },
}; // Anchoir points for ceiling objects


const s_Maze_light_info LIGHT_DISTANCE [ 15 ] =
{
   {  // tile 0
      { 0, 0, 0, 48, 0 },
//      { 255, 255, 255, 255 },
      1,1,
   },
   {  // tile 1
      { 48, 0, 0, 0, 0 },
//      { 255, 255, 255, 255 },
      7,1,
   },
   {  // tile 2
      { 48, 0, 36, 100, 48 },
//      { 255, 255, 255, 255 },
      2,1,
   },
   {  // tile 3
      { 100, 36, 0, 48, 48 },
//      { 255, 255, 255, 255 },
      6,1,
   },
   {  // tile 4
      { 100, 36, 59, 135, 84 },
//      { 255, 255, 255, 255 },
      3,1,
   },
   {  // tile 5
      { 135, 59, 36, 100, 84 },
//      { 255, 255, 255, 255 },
      5,1,
   },
   {  // tile 6
      { 135, 59, 59, 135, 100 },
//      { 255, 255, 255, 255 },
      4,1,
   },
   {  // tile 7
      { 84, 48, 100, 153, 100 },
//      { 255, 255, 255, 255 },
      2,2,
   },
   {  // tile 8
      { 153, 100, 48, 84, 100 },
//      { 255, 255, 255, 255 },
      6,2,
   },
   {  // tile 9
      { 153, 100, 135, 209, 153 },
//      { 255, 255, 255, 255 },
      3,2,
   },
   {  // tile 10
      { 209, 135, 100, 153, 153 },
//      { 255, 255, 255, 255 },
      5,2,
   },
   {  // tile 11
      { 209, 135, 135, 209, 176 },
//      { 255, 255, 255, 255 },
      4,2,
   },
   {  // tile 12
      { 176, 153, 209, 255, 209 },
//      { 255, 255, 255, 255 },
      3,3,
   },
   {  // tile 13
      { 255, 209, 153, 176, 209 },
//      { 255, 255, 255, 255 },
      5,3,
   },
   {  // tile 14
      { 255, 209, 209, 255, 255 },
//      { 255, 255, 255, 255 },
      4,3,
   }
};

// light vision adjustment for nonshaded vision
const bool LIGHT_VISION [ 4 ] [ 15 ] =
{
      { false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false },
      { false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, true },
      { false, false, false, false, false,
        false, false, false, false, true,
        true, true, true, true, true },
      { false, false, false, false, true,
        true, true, true,  true, true,
        true, true, true, true, true },
};


s_Maze_swall_info Maze_SWALL_INFO [ Maze_NB_FILLING ] =
{
   // color, alpha, drawmode
   { 0, 0, 0, 0, 0 },//Maze_SWALL_NONE
   { 0, 100, 150 , 150, DRAW_MODE_TRANS, true },   // Maze_SWALL_WATER
   { 150, 200, 0 , 100, DRAW_MODE_TRANS, false },  // Maze_SWALL_FIZZLE
   { 100, 100, 100 , 150, DRAW_MODE_TRANS, true },// Maze_SWALL_FOG
   { 30, 200, 0 , 100, DRAW_MODE_TRANS, true },   // Maze_SWALL_POISON_GAS
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0 , 255, DRAW_MODE_SOLID, false },      // Maze_SWALL_DARKNESS
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0, 0, 0 },                                   // not defined
   { 0, 0, 0, 0, 0 },                                   // not defined
};


const s_Maze_draw_info Maze_DRAW_INFO =
/* //--- X 4
      int wall [ 3 ]; // wall 4 bit encoding, identify wall to test
      int door [ 3 ]; // door 4 bit encoding, identify door to test
      unsigned short object [ 16 ]; // contains object location 9 bit encoding
      s_Maze_draw_info offset [ 15 ]; // contains drawinf information for each facing
      //--- X 15
         int xoff;
         int yoff;
         int ftex_rotation; // the number of times the floor texture must be rotated
      //---
   //---
   int tested_wallID [ 15 ] [ 3 ]; // the ID# of the corresponding wall if testing is true*/


{
//--- Maze_test
   {  //--- face NORTH
      {
         { WNORTH, WWEST, WEAST }, { DNORTH, DWEST, DEAST },
         { GNORTH, GWEST, GEAST },
         { Maze_9BIT_OBJ_NONE,
           Maze_9BIT_OBJ_CENTER,
           Maze_9BIT_OBJ_NORTH,
           Maze_9BIT_OBJ_EAST,
           Maze_9BIT_OBJ_SOUTH,
           Maze_9BIT_OBJ_WEST,
           Maze_9BIT_OBJ_TWINN,
           Maze_9BIT_OBJ_TWINE,
           Maze_9BIT_OBJ_TWINS,
           Maze_9BIT_OBJ_TWINW,
           Maze_9BIT_OBJ_4CORNER,
           Maze_9BIT_OBJ_4SIDE,
           Maze_9BIT_OBJ_SQUARE,
           Maze_9BIT_OBJ_LOC13,
           Maze_9BIT_OBJ_LOC14,
           Maze_9BIT_OBJ_LOC15 },
         { { -3, 2 },
           { 3,  2 },
           { -2, 2 },
           { 2,  2 },
           { -1, 2 },
           { 1,  2 },
           { 0,  2 },
           { -2, 1 },
           { 2,  1 },
           { -1, 1 },
           { 1,  1 },
           { 0,  1 },
           { -1, 0 },
           { 1,  0 },
           { 0,  0 } },
           0,
      },
      //--- face EAST
      {
         { WEAST, WNORTH, WSOUTH }, { DEAST, DNORTH, DSOUTH },
         { GEAST, GNORTH, GSOUTH },
         { Maze_9BIT_OBJ_NONE,
           Maze_9BIT_OBJ_CENTER,
           Maze_9BIT_OBJ_WEST,
           Maze_9BIT_OBJ_NORTH,
           Maze_9BIT_OBJ_EAST,
           Maze_9BIT_OBJ_SOUTH,
           Maze_9BIT_OBJ_TWINW,
           Maze_9BIT_OBJ_TWINN,
           Maze_9BIT_OBJ_TWINE,
           Maze_9BIT_OBJ_TWINS,
           Maze_9BIT_OBJ_4CORNER,
           Maze_9BIT_OBJ_4SIDE,
           Maze_9BIT_OBJ_SQUARE,
           Maze_9BIT_OBJ_LOC13,
           Maze_9BIT_OBJ_LOC14,
           Maze_9BIT_OBJ_LOC15 },
         { { 2,  3 },
           { 2, -3 },
           { 2,  2 },
           { 2, -2 },
           { 2,  1 },
           { 2, -1 },
           { 2,  0 },
           { 1,  2 },
           { 1, -2 },
           { 1,  1 },
           { 1, -1 },
           { 1,  0 },
           { 0,  1 },
           { 0, -1 },
           { 0,  0 } },
           1,
      },
      //--- face SOUTH
      {

         { WSOUTH, WEAST, WWEST }, { DSOUTH, DEAST, DWEST },
         { GSOUTH, GEAST, GWEST },
         { Maze_9BIT_OBJ_NONE,
           Maze_9BIT_OBJ_CENTER,
           Maze_9BIT_OBJ_SOUTH,
           Maze_9BIT_OBJ_WEST,
           Maze_9BIT_OBJ_NORTH,
           Maze_9BIT_OBJ_EAST,
           Maze_9BIT_OBJ_TWINS,
           Maze_9BIT_OBJ_TWINW,
           Maze_9BIT_OBJ_TWINN,
           Maze_9BIT_OBJ_TWINE,
           Maze_9BIT_OBJ_4CORNER,
           Maze_9BIT_OBJ_4SIDE,
           Maze_9BIT_OBJ_SQUARE,
           Maze_9BIT_OBJ_LOC13,
           Maze_9BIT_OBJ_LOC14,
           Maze_9BIT_OBJ_LOC15 },
         { {  3, -2 },
           { -3, -2 },
           {  2, -2 },
           { -2, -2 },
           {  1, -2 },
           { -1, -2 },
           {  0, -2 },
           {  2, -1 },
           { -2, -1 },
           { 1,  -1 },
           { -1, -1 },
           {  0, -1 },
           {  1,  0 },
           { -1,  0 },
           {  0,  0 } },
           2,
      },
      //--- face WEST
      {

         { WWEST, WSOUTH, WNORTH }, { DWEST, DSOUTH, DNORTH },
         { GWEST, GSOUTH, GNORTH },
         { Maze_9BIT_OBJ_NONE,
           Maze_9BIT_OBJ_CENTER,
           Maze_9BIT_OBJ_EAST,
           Maze_9BIT_OBJ_SOUTH,
           Maze_9BIT_OBJ_WEST,
           Maze_9BIT_OBJ_NORTH,
           Maze_9BIT_OBJ_TWINE,
           Maze_9BIT_OBJ_TWINS,
           Maze_9BIT_OBJ_TWINW,
           Maze_9BIT_OBJ_TWINN,
           Maze_9BIT_OBJ_4CORNER,
           Maze_9BIT_OBJ_4SIDE,
           Maze_9BIT_OBJ_SQUARE,
           Maze_9BIT_OBJ_LOC13,
           Maze_9BIT_OBJ_LOC14,
           Maze_9BIT_OBJ_LOC15 },
         { { -2, -3 },
           { -2,  3 },
           { -2, -2 },
           { -2,  2 },
           { -2, -1 },
           { -2,  1 },
           { -2,  0 },
           { -1, -2 },
           { -1,  2 },
           { -1, -1 },
           { -1,  1 },
           { -1,  0 },
           {  0, -1 },
           {  0,  1 },
           {  0,  0 } },
           3,
      },
   },
   //--- tested_wallID
   { { 0,  -1, -1 },
     { 1,  -1, -1 },
     { 2,  7,  -1 },
     { 3,  -1, 8  },
     { 4,  9,  -1 },
     { 5,  -1, 10 },
     { 6,  11, 12 },
     { 13, -1, -1 },
     { 14, -1, -1 },
     { 15, 18, -1 },
     { 16, -1, 19 },
     { 17, 20, 21 },
     { 22, -1, -1 },
     { 23, -1, -1 },
     { 24, 25, 26 } },
   //--- trans_wallID
   { { 7,  -1 },
     { 8,  -1 },
     { 9,  13 },
     { 10, 14 },
     { 11, 15 },
     { 12, 16 },
     { 17, -1 },
     { 18, -1 },
     { 19, -1 },
     { 20, 22 },
     { 21, 23 },
     { 24, -1 },
     { 25, -1 },
     { 26, -1 },
     { -1, -1 } }

};

