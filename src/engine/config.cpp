/***************************************************************************/
/*                                                                         */
/*                           C O N F I G . C P P                           */
/*                            Class source code                            */
/*                                                                         */
/*     Content : Class Config source code                                  */
/*     Programmer : Eric PIetrocupo                                        */
/*     Starting Date : May 20th, 2002                                      */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

//#include <config.h>


/*
//#include <time.h>
#include <allegro.h>


//#include <datafile.h>
//#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>


//#include <list.h>
#include <opponent.h>
#include <charactr.h>
//#include <monster.h>
#include <party.h>

//#include <game.h>
//#include <city.h>
#include <maze.h>
//
//#include <camp.h>
#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winempty.h>
#include <wintitle.h>
*/


/*-------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                         -*/
/*-------------------------------------------------------------------------*/

Config::Config ( void )
{
   reset ();

}

Config::~Config ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                           Property Methods                            -*/
/*-------------------------------------------------------------------------*/

void Config::set ( int configID, int value )
{
   p_value [ configID ] = value;
}

int Config::get ( int configID )
{
   return ( p_value [ configID ] );
}

/*-------------------------------------------------------------------------*/
/*-                              Methods                                  -*/
/*-------------------------------------------------------------------------*/

void Config::start ( bool in_game )
{
   int i;
   //int j;
   //int index;
   int answer;
   bool readonly [Config_NB_CATEGORY];

   for ( i = 0; i < Config_NB_CATEGORY; i++)
   {
      readonly[i] = false;
   }

   if ( in_game == true)
   {
      readonly [Config_CATEGORY_DIFFICULTY] = true;
      readonly [Config_CATEGORY_SYSTEM] = true;
   }


   Menu mnu_config ("Game Configuration");

   mnu_config.add_item ( Config_MENU_APPEARANCE, "Appearance");
   mnu_config.add_item ( Config_MENU_INTERFACE, "Interface");
   mnu_config.add_item ( Config_MENU_DIFFICULTY, "Difficulty");
   mnu_config.add_item ( Config_MENU_SYSTEM, "System" );
   mnu_config.add_item ( Config_MENU_MUSIC, "Music Track" );
   //mnu_config.add_item ( Config_MENU_UNDEFINED1, "Undefined", true );
   //mnu_config.add_item ( Config_MENU_UNDEFINED2, "Undefined", true );
   //mnu_config.add_item ( Config_MENU_UNDEFINED3, "Undefined", true );

   mnu_config.add_item ( Config_MENU_EXIT, "Exit");

   WinEmpty wemp_background;
   WinTitle wttl_title ("Game Options");

   Window::instruction ( 320, 460, Window_INSTRUCTION_MENU +
      Window_INSTRUCTION_SWITCH + Window_INSTRUCTION_CANCEL );

   WinMenu wmnu_config ( mnu_config, 20, 40);

   answer = Window::show_all();

//   draw_border ( 0 , 0 , 640, 480 , General_COLOR_BORDER );

   while (answer != -1)
   {

      wmnu_config.hide();
      Window::draw_all();

      show_config_part( answer, readonly [ answer ] );

      wmnu_config.unhide();

      Window::refresh_all();
      answer = Window::show_all();

   }



   //printf ("Option 0 = %d - pass 2\r\n", p_value [ 0 ]);

   initialise ();
   //printf ("Option 0 = %d - pass 3\r\n", p_value [ 0 ]);
}




void Config::reset ( void )
{
   /*p_value [ Config_TEXTURE ] = Config_TEX_YES;
   p_value [ Config_RENDERING ] = Config_REN_PTEX;
   p_value [ Config_SHADING ] =  Config_SHD_YES;
   p_value [ Config_TRANSPARENCY ] = Config_TRS_YES;
   p_value [ Config_FLOOR_CEILING ] = Config_FAC_YES;
   p_value [ Config_KEY_DISPLAY ] = Config_KEY_YES;
   p_value [ Config_HELP_DISPLAY ] = Config_HLP_YES;
   p_value [ Config_MUSIC_VOLUME ] = Config_MUS_MEDIUM;
   p_value [ Config_SOUND_VOLUME ] = Config_SND_MEDIUM;
   p_value [ Config_BASIC_KEY ] = Config_BKY_AZ;
   p_value [ Config_DISPLAY_PARTY ] = Config_PAR_YES;
   p_value [ Config_DISPLAY_SPELL ] = Config_SPL_YES;
   p_value [ Config_DISPLAY_SPECIAL ] = Config_SPC_YES;
   p_value [ Config_WINDOW_BORDER ] = Config_WBD_THICK;
   p_value [ Config_ANIMATION ] = Config_ANI_YES;
   p_value [ Config_WINDOW_BACKGROUND ] = Config_WBG_TEXTURE;
   p_value [ Config_WINDOW_TEXTURE ] = Config_WTX_GRANITE;
   p_value [ Config_WINDOW_COLOR ] = Config_WCO_BLACK;*/

   int i;

   //printf ("Config::reset: Resetting config");

   for (i = 0; i < Config_NB_MAX_CONFIG ; i++)
   {

      p_value [ i ] = config_entry [ i ] . default_value;

   }
}

void Config::initialise ( void )
{
   configure_volume ();
   configure_key ();
   configure_maze ();
   configure_window();
}

void Config::load ( void )
{
   FILE *Config_file;
   s_Config_file filebuffer;
   int i;
   //int dummy;


   if ( exists ("wizardry.cfg") != 0 )
   {
//printf ("Config::load: File exists, open and load\n");
      Config_file = fopen ("wizardry.cfg","rb");

      if ( Config_file != NULL )
      {
         fread ( &filebuffer, sizeof ( s_Config_file ), 1, Config_file );

         for ( i = 0 ; i < Config_NB_MAX_CONFIG ; i++ )
            p_value [ i ] = filebuffer.f_value [ i ];

      // refresh data ofoption engine
//      for ( i = 0 ; i < Config_NB_CONFIG ; i++ )
//         opt_config.selected ( i , p_value [ i ] );

      }

      fclose ( Config_file );
   }
}

void Config::save ( void )
{
   FILE *Config_file;
   s_Config_file filebuffer;
   int i;

   Config_file = fopen ("wizardry.cfg","wb");

   if ( Config_file != NULL)
   {
//printf ("Congif::save: valid file handle, saving\n");

      for ( i = 0 ; i < Config_NB_MAX_CONFIG ; i++ )
         filebuffer.f_value [ i ] = p_value [ i ];

      fwrite ( &filebuffer, sizeof ( s_Config_file ), 1, Config_file );

   }

   fclose ( Config_file );
}

void Config::SQLload ( void )
{
   //int i;
   int error;
   int tmppk;
   int tmpvalue;

   error = SQLprepare ("SELECT * FROM option;");

   if ( error == SQLITE_OK )
   {
      error = SQLstep();

      while ( error == SQLITE_ROW)
      {
         tmppk = SQLcolumn_int ( 0 );
         tmpvalue = SQLcolumn_int ( 1 );
         p_value [ tmppk ] = tmpvalue;
         //printf ("Row %d = %d\r\n");
         error = SQLstep();
      }
   }
   else
      printf ("Error Loading config\r\n");

   SQLfinalize ();

}

void Config::SQLsave ( void )
{
   int i;
   char querystr [ ( 13 * Config_NB_MAX_CONFIG) + 28 ];
   char tmpstr [20];
   int error;

   SQLbegin_transaction();
   error = SQLexec ("DELETE FROM option;");

   if ( error == SQLITE_OK )
   {

      //printf ( "pass 1\r\n");
      sprintf ( querystr, "INSERT INTO option VALUES ");
      //printf ( "pass 2\r\n");
      for ( i = 0; i < Config_NB_MAX_CONFIG - 1 ; i++)
      {
         sprintf ( tmpstr, " ( %d, %d ),", i, p_value [ i ] );
        // printf ( "pass 3\r\n");
         strcat ( querystr, tmpstr );
         //printf ( "pass 4\r\n");

      }
      i = Config_NB_MAX_CONFIG - 1;
      //printf ( "pass 5\r\n");
      sprintf ( tmpstr, " ( %d, %d );", i, p_value [ i ] );
      //printf ( "pass 6\r\n");
      strcat ( querystr, tmpstr );

      //printf ("Query: %s\r\n", querystr);

      error = SQLexec ( querystr );
      //printf ( "pass here\r\n");

      if ( error != SQLITE_OK)
         SQLerrormsg();

   }

   SQLcommit_transaction();
   //printf ("debug: saving done\r\n");
}


void Config::show_config_part ( int section, int readonly  )
{

   Option opt_config ( STR_CONFIG_PART_TITLE [ section ] );
    s_Option_result tmp_result;

   int start = 0;
   int i;
   int j;
   int nb_item = 0;

      //for (i = 0; i < Config_NB_CATEGORY ; i++)
   //{
   if ( section < Config_NB_CATEGORY)
   {

      start = Config_NB_PER_CATEGORY * section;

      for (j = start; j < start + Config_NB_PER_CATEGORY ; j++)
      {
         if ( config_entry [ j ] . active == true)
         {

            opt_config . add_item ( j, config_entry [ j ].text, config_entry [ j ].elem [ 0 ],
               config_entry[ j ].elem [ 1 ], config_entry[ j ].elem [ 2 ], config_entry[ j ].elem [ 3 ],
               config_entry[ j ].elem [ 4 ], p_value [ j ] );
         }
         //printf ("Option [%d,%d] %d = %d\r\n", i, j, index, p_value [index ] );
         //index++;
      }

      opt_config . quit ("Exit");
   }
   //printf ("Option 0 = %d - pass 1\r\n", p_value [ 0 ]);

   Window::draw_all();
   opt_config . show (10, 40, readonly);


//printf ("Option 0 = %d - pass 4\r\n", p_value [ 0 ]);
   reset_config_part( section);
//   printf ("Option 0 = %d - pass 5\r\n", p_value [ 0 ]);

   tmp_result.key = 0;
   tmp_result.selected = 0;
   nb_item = opt_config.nb_item();

// ---------------------- save configuration in class ------------------------

      for (i = 0; i < nb_item ; i++)
      {
         // bug, if option is not valid, no key has been input, so it takes 0,
         //need to find a way that I am out of bounf, use nb item of list to set J limit.
         tmp_result = opt_config . result ( i );
         //printf("\r\nloaded: i=%d j=%d sel=%d key=%d", i, j, tmp_result.selected, tmp_result.key );
         p_value [ tmp_result.key ] = tmp_result.selected;
 //        printf ("Option [%d,%d] %d = %d -> %d\r\n", i, j, tmp_result.key, tmp_result.selected, p_value [ tmp_result.key ]);
 //        printf ("Option 0 = %d - pass inside\r\n", p_value [ 0 ]);

      }



   initialise ();
}

int Config::verify_if_default ( void )
{
   int valid = 1;
   int i;

   for ( i = 0 ; i < Config_NB_MAX_CONFIG; i++)
   {
      if ( p_value [ i ] != config_entry [ i ] . default_value )
         valid = 0;
   }

   if ( valid == 1)
      printf ("Config::Verify_if_default:Configuration is Default SUCCESS\n");
   else
      printf ("Config::Verify_if_default:Configuration not Default FAILED\n");

   return ( valid );
}

/*-------------------------------------------------------------------------*/
/*-                           Private Methods                             -*/
/*-------------------------------------------------------------------------*/


void Config::reset_config_part ( int section )
{

   int i;
   int start;

   if ( section < Config_NB_CATEGORY)
   {
      start = Config_NB_PER_CATEGORY * section;
      for (i = start; i < start + Config_NB_PER_CATEGORY ; i++)
      {

         p_value [ i ] = config_entry [ i ] . default_value;

      }
   }



}

void Config::configure_volume ( void )
{
   short svalue = 0;
   short mvalue = 0;

   if ( p_value [ Config_MUSIC_VOLUME ] > 0 )
      mvalue = 55 + p_value [ Config_MUSIC_VOLUME ] * 50;
   else
      mvalue = 0;

   if ( p_value [ Config_SOUND_VOLUME ] > 0 )
      svalue = 55 + p_value [ Config_SOUND_VOLUME ] * 50;
   else
      svalue = 0;

   set_volume ( svalue, mvalue );

}

void Config::configure_key ( void )
{

   switch ( p_value [ Config_BASIC_KEY ] )
   {
      case Config_BKY_AZ :
         CANCEL_KEY = KEY_A;
         SELECT_KEY = KEY_Z;
      break;

      case Config_BKY_QW :
         CANCEL_KEY = KEY_Q;
         SELECT_KEY = KEY_W;
      break;

      case Config_BKY_ZX :
         CANCEL_KEY = KEY_Z;
         SELECT_KEY = KEY_X;
      break;
   }

}

void Config::configure_maze ( void )
{
   unsigned char tmpval;

   switch ( p_value [ Config_TEXTURE ] )
   {
      case Config_TEX_YES :
         switch ( p_value [ Config_RENDERING ] )
         {
            case Config_REN_PTEX :
               switch ( p_value [ Config_SHADING ] )
               {
                  case Config_SHD_YES :
                     maze.polytype ( POLYTYPE_PTEX_LIT );
                     maze.mask_polytype ( POLYTYPE_PTEX_MASK_LIT );
                     maze.adjust_color ( false );
                  break;

                  case Config_SHD_NO :
                     maze.polytype ( POLYTYPE_PTEX );
                     maze.mask_polytype ( POLYTYPE_PTEX_MASK );
                     maze.adjust_color ( false );
                  break;
               }
            break;

            case Config_REN_ATEX :
               switch ( p_value [ Config_SHADING ] )
               {
                  case Config_SHD_YES :
                     maze.polytype ( POLYTYPE_ATEX_LIT );
                     maze.mask_polytype ( POLYTYPE_ATEX_MASK_LIT );
                     maze.adjust_color ( false );
                  break;

                  case Config_SHD_NO :
                     maze.polytype ( POLYTYPE_ATEX );
                     maze.mask_polytype ( POLYTYPE_ATEX_MASK );
                     maze.adjust_color ( false );
                  break;
               }
            break;
         }
      break;

      case Config_TEX_NO :
         switch ( p_value [ Config_SHADING ] )
         {
            case Config_SHD_YES :
               maze.polytype ( POLYTYPE_GRGB );
               maze.mask_polytype ( POLYTYPE_PTEX_MASK_LIT );
               maze.adjust_color ( true );
            break;

            case Config_SHD_NO :
               maze.polytype ( POLYTYPE_FLAT );
               maze.mask_polytype ( POLYTYPE_PTEX_MASK );
               maze.adjust_color ( true );
            break;
        }
      break;
   }

   switch ( p_value [ Config_TRANSPARENCY ] )
   {
      case Config_TRS_YES :
         maze.transparency ( true );
      break;

      case Config_TRS_NO :
         maze.transparency ( false );
      break;
   }

   // set display
   tmpval = 0;

   if ( p_value [ Config_DISPLAY_PARTY ] == Config_PAR_YES )
      tmpval = tmpval + Maze_DISPLAY_PARTY;

   if ( p_value [ Config_DISPLAY_SPELL ] == Config_SPL_YES )
      tmpval = tmpval + Maze_DISPLAY_SPELL;

   if ( p_value [ Config_DISPLAY_SPECIAL ] == Config_SPC_YES )
      tmpval = tmpval + Maze_DISPLAY_SPECIAL;

   maze.display ( tmpval );
   mazepal.build ();

}


void Config::configure_window ( void )
{
   Window::border_type ( p_value [ Config_WINDOW_BORDER ] );
   Window::background_color  ( p_value [ Config_WINDOW_COLOR ] );
   Window::background_texture ( p_value [ Config_WINDOW_TEXTURE ] );
   Window::background_mode ( p_value [ Config_WINDOW_BACKGROUND ] );
   Window::translucency_level ( Config_TRANSLUCENCY_LEVEL [ p_value [ Config_WINDOW_TRANSLUCENCY ] ] );

   if ( p_value [ Config_MENU_CASCADE] == Config_YES)
      Window::enable_cascade();
   else
      Window::disable_cascade();

   // add color setting
}

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

Config config; // configiration class and engine


/*typedef struct s_Config_entry
{
   bool active;
   char text [ Option_TEXT_SIZE ];
   char elem [ 5 ] [ Option_ELEM_SIZE ];
   int category; // used to group options together.
   int default_value; // default value at creation

}s_Config_entry;*/

s_Config_entry config_entry [ Config_NB_MAX_CONFIG ] =
{
   // Performance

   // Config_TEXTURE
   { true, "Textured Polygon    ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_APPEARANCE, Config_TEX_YES},
   // Config_RENDERING
   { true, "Polygon Rendering   ", "Perspec.", " Affine ", "", "", "", Config_CATEGORY_APPEARANCE, Config_REN_PTEX},
   // Config_SHADING
   { true, "Polygon Shading     ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_APPEARANCE, Config_SHD_YES},
   // Config_TRANSPARENCY
   { true, "Transparent Tint    ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_APPEARANCE, Config_TRS_YES},
   // Config_FLOOR_CEILING
   { true, "Floor & Ceiling     ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_APPEARANCE, Config_FAC_YES},
   // Config_ANIMATION
   { true, "Animations          ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_APPEARANCE, Config_ANI_YES},
   // Config_WINDOW_BORDER
   { true, "Window Border Type  ", " Thick  ", "  Thin  ", " Double ", "  Flat  ", "  None  ", Config_CATEGORY_APPEARANCE, Config_WBD_THICK},
   // Config_WINDOW_BACKGROUND
   { true, "Window Background   ", " Opaque ", "Translc.", "Textured", "", "", Config_CATEGORY_APPEARANCE, Config_WBG_TEXTURE},
   // Config_WINDOW_TEXTURE
   { true, "Window Texture      ", " Marble ", " Earth  ", " Forest ", " Slate  ", "  Rock  ", Config_CATEGORY_APPEARANCE, Config_WTX_GRANITE},
   // Config_WINDOW_COLOR
   { true, "Window Color        ", " Black  ", " Green  ", "  Blue  ", " Brown  ", " Ocean  ", Config_CATEGORY_APPEARANCE, Config_WCO_BLACK},
   // Config_WINDOW_TRANSLUCENCY
   { true, "Window Translucency ", "25%", "40%", "55%", "70%", "85%", Config_CATEGORY_APPEARANCE, Config_WTL_MED},
   // Config_ANTI-ALLIASSING
   { true, "Anti-Aliassing      ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_APPEARANCE, Config_YES },
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_APPEARANCE, 0},


   // Interface
   // Config_KEY_DISPLAY
   { true, "Display Key Usage   ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_INTERFACE, Config_KEY_YES},
   // Config_HELP_DISPLAY
   { true, "Display Instant Help", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_INTERFACE, Config_HLP_YES},
   // Config_MUSIC_VOLUME
   { true, "Music Volume        ", "  Mute  ", "  Low   ", " Medium ", "  High  ", " MAX  ", Config_CATEGORY_INTERFACE, Config_MUS_MEDIUM},
   // Config_SOUND_VOLUME
   { true, "Sound Volume        ", "  Mute  ", "  Low   ", " Medium ", "  High  ", " MAX  ", Config_CATEGORY_INTERFACE, Config_SND_MEDIUM},
   // Config_BASIC_KEY
   { true, "Cancel/Select Key   ", " A / Z  ", " Q / W  ", " Z / X  ", "", "", Config_CATEGORY_INTERFACE, Config_BKY_AZ},
   // Config_DISPLAY_PARTY
   { true, "Display Party Bar   ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_INTERFACE, Config_PAR_YES},
   // Config_DISPLAY_SPELL
   { true, "Display Spell Icons ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_INTERFACE, Config_SPL_YES},
   // Config_DISPLAY_SPECIAL
   { true, "Display Maze Icons  ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_INTERFACE, Config_SPC_YES},
   // Config_MENU_CASCADE
   { true, "Menu in Cascade     ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_COMBAT_LOG_SPEED
   { true, "Combat Log Speed    ", " Pause  ", "0.5 Sec.", " 1 sec. ", " 2 sec. ", " 3 sec. ", Config_CATEGORY_INTERFACE, Config_CLS_PAUSE},
   // Config_DISPLAY_PARTY_FRAME
   { true, "Display Party Frame ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_INTERFACE, Config_PAR_NO},
   // Config_DETAIL_COMBAT_ROLL
   { true, "Detail Combat Rolls ", "  Yes   ", "   No   ", "", "", "", Config_CATEGORY_INTERFACE, Config_NO },
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_INTERFACE, 0},

   // Difficulty
   // Config_MONSTER_OCCURENCE
   { true, "Encounter Occurence    ", "  Half  ", " Normal ", " Double ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_NB_MAX_MONSTER
   { true, "Max number of ennemies ", "   4    ", "   6    ", "   8    ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_CHARACTER_POINTS
   { true, "Character Points       ", "   36   ", "   24   ", "   12   ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_STARTING_LEVEL
   { true, "Starting Character Lvl.", "   5    ", "   3    ", "   1    ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_HEALTH_STATUS
   { true, "Health Status Effect   ", "Disabled", "Enabled ", "", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_ENABLED},
   // Config_STARTING_GOLD
   { true, "Starting Gold          ", "2000 Gp ", "1000 Gp ", " 500 Gp ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_SOUL_RECOVERY
   { true, "Soul Recovery          ", "Doubled ", " Normal ", "  Half  ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_MONSTER_HPMP
   { true, "Monster HP and MP      ", "Average ", " Random ", "Maximum ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_ITEM_DROP
   { true, "Nb of Item Dropped(N/A)", "Doubled ", " Normal ", "  Half  ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_EXP_REWARD
   { true, "Experience Reward      ", "Doubled ", " Normal ", "  Half  ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_GOLD_REWARD
   { true, "Gold Reward            ", "Doubled ", " Normal ", "  Half  ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_MARKET_RESTOCK
   { true, "Market Item Restock    ", "Doubled ", " Normal ", "  Half  ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_CHEST_TRAP
   { true, "Chest Traps       (N/A)", "Disabled", "Enabled ", "", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_ENABLED},
   // Config_MAZE_TRAP
   { true, "Maze Traps        (N/A)", "Disabled", "Enabled ", "", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_ENABLED},
   // Config_MAZE_AREA
   { true, "Maze Area Effects (N/A)", "Disabled", "Enabled ", "", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_ENABLED},
   // Config_CURSED_ITEM
   { true, "Cursed Items           ", "Disabled", "Enabled ", "", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_ENABLED},
   // Config_SPELL_NAME
   { true, "Arcane Spell Names     ", "Disabled", "Enabled ", "", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_ENABLED},
   // Config_MARKET_RARITY
   { true, "Market Item Rarity     ", "  High  ", "Average ", "  Low   ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_LEVELUP_ATTRIBUTE
   { true, "Level Up Attribute     ", "1/2 lvl ", "1/4 lvl ", "1/6 lvl ", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL},
   // Config_WEAKRESIST_STRENGTH
   { true, "Weakn./Resist. Strength", "  Weak  ", " Strong ", "", "", "", Config_CATEGORY_DIFFICULTY, Config_DIF_NORMAL },
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_DIFFICULTY, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_DIFFICULTY, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_DIFFICULTY, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_DIFFICULTY, 0},


   // System
   // Config_SYSTEM_RESOLUTION
   { true, "Graphic Resolution     ", "1024x768", "1024x600", "1024x576", "800x600 ", "640x480", Config_CATEGORY_SYSTEM, 4},
   // Config_SYSTEM_MIDI
   { true, "MIDI Music Engine      ", "  Auto  ", " Digimid", "", "", "", Config_CATEGORY_SYSTEM, Config_SYS_AUTODETECT },
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_SYSTEM, 0},


   // Config_MUSIC_INTRO
   { true, "Introduction           ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_MUSIC_CAMP
   { true, "Camp                   ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_MUSIC_CITY
   { true, "City                   ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_MUSIC_INN
   { true, "Adventurer's Inn       ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_MUSIC_SHOP
   { true, "Marketplace            ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_MUSIC_TAVERN
   { true, "Tavern                 ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_MUSIC_TEMPLE
   { true, "Temple                 ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_MUSIC_TRAINING
   { true, "Training Grounds       ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_
   { true, "Death Screen           ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_
   { true, "Endgame Credits        ", "  Wiz 1 ", "  Wiz 2 ", "  Wiz 3 ", " Random ", "", Config_CATEGORY_MUSIC, 3},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_MUSIC, 0},

   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED1, 0},

   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED2, 0},

   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},
   // Config_
   { false, "", "", "", "", "", "", Config_CATEGORY_UNDEFINED3, 0},

};

const char STR_CONFIG_PART_TITLE [ Config_NB_CATEGORY] [51] =
{
   "Maze and windows appearance",
   "User interface customization",
   "Game difficulty setup",
   "System Configuration (Requires Game Restart)",
   "Music Track Selection",
   "Undefined",
   "Undefined",
   "Undefined"
};

const int Config_COMBAT_LOG_SPEED_TIME [ 5 ] =
{
   0, 500, 1000, 2000, 3000
};

const int Config_NB_MAX_MONSTER_VALUE [ 5 ] =
{
   4, 6, 8, 2, 2 // value 4 and 5 are not used
};

const int Config_TRANSLUCENCY_LEVEL [ 5 ] =
{
   63, 102, 140, 178, 216
};

const int Config_MUSIC_TABLE [ Config_NB_MUSIC_TABLE ] [ 3 ] =
{

   //#define Config_MUSIC_INTRO             96
   { 8, 22, 35},
   //#define Config_MUSIC_CAMP              97
   { 0, 14, 27},
   //#define Config_MUSIC_CITY              98
   { 1, 15, 28},
   //#define Config_MUSIC_INN               99
   { 7, 21, 34},
   //#define Config_MUSIC_SHOP              100
   { 10, 23, 36},
   //#define Config_MUSIC_TAVERN            101
   { 11, 24, 37},
   //#define Config_MUSIC_TEMPLE            102
   { 12, 25, 38},
   //#define Config_MUSIC_TRAINING          103
   { 5, 18, 31},
   //#define Config_MUSIC_DEATH             104
   { 6, 16, 29},
   //#define Config_MUSIC_TRAINING          105
   { 3, 19, 32}


};

int Config_rndmusic [ Config_NB_MUSIC_TABLE ];
