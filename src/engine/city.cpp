/***************************************************************************/
/*                                                                         */
/*                           C I T Y . C P P                               */
/*                                                                         */
/*     Content : Class City                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : March 27th, 2002                                    */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h> //<tmpinc>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>

#include <grpinterface.h>
#include <grpengine.h>

#include <wdatproc.h>
/*
#include <allegro.h>



//#include <time.h>


#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
//#include <option.h>
#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>








#include <listwiz.h>

#include <opponent.h>
#include <charactr.h>
//#include <monster.h>
#include <party.h>

#include <game.h>


#include <city.h>
#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winempty.h>
#include <wintitle.h>
#include <winmenu.h>
#include <windata.h>
#include <wdatproc.h>
#include <winlist.h>
#include <winmessa.h>

*/



/*-------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                         -*/
/*-------------------------------------------------------------------------*/

City::City ( void )
{
   short i;
   short j;

/* to initialise
  private: string p_title; // title of the city
   private: string p_shortname;
   private: int p_nb_place; // number of places in the city
   private: short p_xpos; // xposition on the map
   private: short p_ypos; // yposition on the map
   private: s_City_place p_place [ City_NB_MAX_LOCATION ]; // list of places which can be visited
   private: s_City_warp p_warp [ City_NB_MAX_WARP ]; // city warping information
*/

   strcpy ( p_name, "City long name" );
 //  strcpy ( p_shortname, "City name" );
   p_nb_place = 0;

   /*p_xpos = 0;
   p_ypos = 0;
   p_type = City_TYPE_VILLAGE;

   for ( i = 0 ; i < City_NB_MAX_LOCATION ; i++ )
   {
      strcpy ( p_place [ i ].name, "Location" );
      p_place [ i ].music_track = 1;
      for ( j = 0 ; j < City_NB_MAX_SERVICE ; j++ )
         p_place [ i ].service [ j ] = -1;
   }

   for ( i = 0 ; i < City_NB_MAX_WARP ; i++ )
   {
      p_warp [ i ].type = 0;
//      p_warp.distance = 0;
      p_warp [ i ].target = 0;
      p_warp [ i ].tag.number ( 0 );
   }

   for ( i = 0 ; i < City_NB_MAX_ITEM ; i++ )
   {
      p_item [ i ].itemtag.number ( 0 );
      p_item [ i ].type = -1;
      p_item [ i ].quantity = 0;
   }

   p_dblength = sizeof ( dbs_City );
   p_dbtableID = DBTABLE_CITY;
*/

   strcpy (p_name, "Allo");

   for ( i = 0 ; i < City_NB_MAX_LOCATION ; i++ )
      for ( j = 0 ; j < City_NB_MAX_SERVICE ; j++ )
         p_place [ i ] . service [ j ] = -1;

   // note: adding tavern for mercenaries and quest
   // unless place in inn and keep wizard covenant separate


   strcpy (p_place[ 0 ].name, "Adventurer's Inn");
   p_place[ 0 ].service [ 0 ] = City_SERVICE_REST;
   p_place[ 0 ].service [ 1 ] = City_SERVICE_PARTY;
//   p_place[ 0 ].service [ 1 ] = City_SERVICE_RETURN;
   //p_place[ 0 ].music_track = System_MUSIC_w1inn;
   p_place[ 0 ].music_table = Config_MUSICTAB_INN;

   //p_place[ 1 ].name = "Adventurer's Guild" ;
   //p_place[ 1 ].service [ 0 ] = City_SERVICE_HIRE;
//   p_place[ 1 ].service [ 1 ] = City_SERVICE_RETURN;
   //p_place[ 1 ].music_track = System_MUSIC_w3inn;

   strcpy (p_place[ 1 ].name, "Marketplace");
   p_place[ 1 ].service [ 0 ] = City_SERVICE_BUY;
   p_place[ 1 ].service [ 1 ] = City_SERVICE_SELL;
   p_place[ 1 ].service [ 2 ] = City_SERVICE_FORGE;
   //p_place[ 1 ].service [ 3 ] = City_SERVICE_KEY;
//   p_place[ 1 ].service [ 3 ] = City_SERVICE_RETURN;
   //p_place[ 1 ].music_track = System_MUSIC_w2shop;
   p_place[ 1 ].music_table = Config_MUSICTAB_SHOP;

   strcpy (p_place[ 2 ].name,"Temple");
   p_place[ 2 ].service [ 0 ] = City_SERVICE_HEAL;
   p_place[ 2 ].service [ 1 ] = City_SERVICE_REVIVE;
   //p_place[ 2 ].service [ 2 ] = City_SERVICE_KNOWLEDGE;
//   p_place[ 2 ].service [ 3 ] = City_SERVICE_RETURN;
   //p_place[ 2 ].music_track = System_MUSIC_w1temple;
   p_place[ 2 ].music_table = Config_MUSICTAB_TEMPLE;
   //note: maybe move to temple and use same building scheme than wiz.
   strcpy (p_place[ 3 ].name, "Wizard's Covenant");
   p_place[ 3 ].service [ 0 ] = City_SERVICE_ENCHANT;
   p_place[ 3 ].service [ 1 ] = City_SERVICE_IDENTIFY;
   p_place[ 3 ].service [ 2 ] = City_SERVICE_UNCURSE;
//   p_place[ 3 ].service [ 3 ] = City_SERVICE_RETURN;
   //p_place[ 3 ].music_track = System_MUSIC_w3shop;
   p_place[ 3 ].music_table = Config_MUSICTAB_TAVERN;

//   p_place[ 5 ].name = "Edge of Town";
//   p_place[ 5 ].service [ 0 ] = City_SERVICE_LANDWARP;
//   p_place[ 5 ].service [ 1 ] = City_SERVICE_SEAWARP;
//   p_place[ 5 ].service [ 2 ] = City_SERVICE_AIRWARP;
//   p_place[ 5 ].service [ 3 ] = City_SERVICE_MAGIKWARP;
//   p_place[ 5 ].service [ 4 ] = City_SERVICE_EXPLORE;
//   p_place[ 5 ].service [ 5 ] = City_SERVICE_RETURN;
//   p_place[ 5 ].music_track = System_MUSIC_w1city;

//?? maybe set by default, still allows variation according to adventure
   strcpy (p_place[ 4 ].name, "Training Grounds");

   p_place[ 4 ].service [ 0 ] = City_SERVICE_CHARACTERS;
   p_place[ 4 ].service [ 1 ] = City_SERVICE_LEVELUP;
//   p_place[ 4 ].service [ 3 ] = City_SERVICE_RETURN;
   //p_place[ 4 ].music_track = System_MUSIC_w2edge;
   p_place[ 4 ].music_table = Config_MUSICTAB_TRAINING;

   p_nb_place = 5;

/*
   for ( i = 0 ; i < City_NB_MAX_WARP ; i++ )
   {
      p_warp [ i ] . type = City_WARPTYPE_LAND;
      p_warp [ i ] . distance = 0;
      p_warp [ i ] . citytag . number ( 0 );
   }

   p_xpos = 100;
   p_ypos = 100;
*/
   for ( i = 0 ; i < City_NB_MAX_ITEM ; i++ )
   {
      p_item [ i ] . itemtag = 0;
      p_item [ i ] . type = -1;
      p_item [ i ] . quantity = 0;
      //p_item [ i ] . modifier = 0;
   }

}

City::~City ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                         Property Methods                              -*/
/*-------------------------------------------------------------------------*/

/*void City::name ( string &str )
{
   p_name = str;
}

string& City::name ( void )
{
   return ( p_name );
} */

void City::name ( const char* str )
{
   strncpy ( p_name, str, 31 );
}

const char* City::name ( void )
{
   return ( p_name );
}

int City::nb_place ( void )
{
   return ( p_nb_place );
}

/*short City::xpos ( void )
{
   return ( p_xpos );
}

short City::ypos ( void )
{
   return ( p_ypos );
}

int City::type ( void )
{
   return ( p_type );
}*/

/*void City::party ( Party &party )
{
   party = party;
} */

/*-------------------------------------------------------------------------*/
/*-                             Methods                                   -*/
/*-------------------------------------------------------------------------*/

int City::start ( void )
{
   //short tmpval;
   //short tmpval2;
   int retval = System_GAME_START;
   bool exit_city = false;
   short i;
   char tmpstr [ 101];

   //party.SQLselect ( party );

   int answer = 0;
   int answer2 = 0;

//debug_printf( __func__, "Enter City");

   Window::instruction ( 320, 340 );
   WinEmpty wem_border;
//   wem_border.preshow();

   WinTitle wtl_name ( p_name );

   Window::cascade_start();

   WinTitle wttl_wait1 ( "Please Wait", 320, 200);
   Window::show_all();
   wttl_wait1.hide();
   //rest(2000);

   //printf ("debug: City::start: begin method\n");

   while ( game.clock.day() > game.lastcityday())
   {
      generate_item();
      game.increment_lastcityday();
   }


   Menu p_menu_place ( "Places to go" );



   i = 0;
   while ( i < p_nb_place )
   {
      p_menu_place.add_item ( i, p_place [ i ].name );
      i++;
   }

   p_menu_place.add_item ( City_MENU_CAMP, "Party Camp" );
   p_menu_place.add_item ( City_MENU_MAZE, "Enter Maze" );
   p_menu_place.add_item ( City_MENU_QUIT, "Quit Game" );

//   tmpval = p_nb_place + 1;
//   tmpval = ( tmpval * 16 ) + 56;

   // menu

   // show graphic


   WinData<int> wdat_gold ( WDatProc_character_gold, 0,
      WDatProc_POSITION_PARTY_GOLD );

   WinData<Party> wdat_party_bar ( WDatProc_party_bar, 0,
      WDatProc_POSITION_PARTY_BAR );

//debug_printf( __func__, "Enter Win Data Initialisation");
//   wtl_name.preshow();

   // does not work as expected
   //List lst_party_bar;
   //InterfaceBuild_Party_Bar (lst_party_bar);
   //WinList wlst_party_bar ( lst_party_bar, 0, 355);



   while ( exit_city == false )
   {
      WinMenu wmnu_place ( p_menu_place, 20, 50, true );

      ActiveEffect::trigger_expiration ( ActiveEffect_EXPIRATION_EXITREST );


      play_music_table ( Config_MUSICTAB_CITY );

      //play_music_track ( System_MUSIC_w1city );

      //wmnu_place.unhide();

      Window::instruction ( 320, 340 );

      answer = Window::show_all ();
      //debug_printf( __func__, "After first show all");

      if ( answer >=0 && answer < p_nb_place )
      {

         Menu mnu_submenu ( p_place [ answer ] . name );

         i = 0;
         while ( p_place [ answer ] . service [ i ] != -1
            && i < City_NB_MAX_SERVICE )
         {
            mnu_submenu.add_item ( i,
               SERVICE_NAME [ p_place [ answer ] . service [ i ] ] );
            i++;
         }

         mnu_submenu.add_item ( i,  "Exit" );


         WinMenu wmnu_service ( mnu_submenu, 20 , 50 );
         answer2 = 0;
         //play_music_track ( p_place [ answer ] . music_track );
         play_music_table(  p_place [ answer ] . music_table );
         while ( answer2 != -1 )
         {
            //printf ("Debug: City start: pass 1\r\n");
            wdat_party_bar.refresh();
            Window::instruction ( 320, 340 );
            wmnu_place.hide();
            wmnu_service.unhide();

            answer2 = Window::show_all();
            //printf ("Debug: City start: pass 2\r\n");
            wmnu_service.hide();

            if ( answer2 == i )
               answer2 = -1;

            if ( answer2 != -1 )
            {


               switch ( p_place [ answer ] . service [ answer2 ] )
               {
                  case City_SERVICE_REST :
                     show_service_rest ();

                  break;
                  case City_SERVICE_LEVELUP :
                     wdat_party_bar.hide();
                     wdat_gold.hide();
                     //wmnu_service.hide();
                     show_service_levelup();
                     wdat_party_bar.unhide();
                     wdat_gold.unhide();
                     //wmnu_service.unhide();
                  break;
                  case City_SERVICE_HOME :
                  break;
                  case City_SERVICE_HIRE :
                  break;
                  case City_SERVICE_BUY :
                     show_service_buy ();
                  break;
                  case City_SERVICE_SELL :
                     show_service_sell ();
                  break;
                  case City_SERVICE_FORGE :
                  break;
                  case City_SERVICE_ENCHANT :
                  break;
                  case City_SERVICE_IDENTIFY :
                     show_service_identify();
                  break;
                  case City_SERVICE_HEAL :
                     show_service_heal();
                  break;
                  case City_SERVICE_REVIVE :
                     show_service_revive();
                  break;
                  case City_SERVICE_KNOWLEDGE :
                  break;
                  case City_SERVICE_KEY :
                  break;
                  case City_SERVICE_UNCURSE :
                  break;
                  case City_SERVICE_CHARACTERS:
                     wdat_party_bar.hide();
                     wdat_gold.hide();
                     show_service_characters();
                     wdat_party_bar.unhide();
                     wdat_gold.unhide();
                  break;
                  case City_SERVICE_PARTY:
                     show_service_party ();
                  break;
               }
//printf ("Debug: City start: pass 3\r\n");
               while ( game.clock.day() > game.lastcityday())
               {
                  generate_item();
                  game.increment_lastcityday();
               }
//printf ("Debug: City start: pass 4\r\n");
            }
         }

      }
      else
      {
         switch ( answer )
         {
            case City_MENU_QUIT:
               retval = System_GAME_STOP;
               exit_city = true;
            break;
            case City_MENU_MAZE:
               //debug_printf( __func__, "Before check if functionnal");
               if ( party.is_functional() == true)
               {
                  //debug_printf( __func__, "Enter Maze menu");
                  play_music_track ( System_MUSIC_w3edge );
                  party.status (Party_STATUS_MAZE);
                  party.position (maze.show_entrance());

                  Window::cascade_pause();

                  wmnu_place.hide();
                  wtl_name.hide();

                  //printf ("--- Start new method ---\r\n");
                  sprintf ( tmpstr, "Entering %s", game.mazename());
                  WinTitle wttl_wait (tmpstr, 320, 200);
                  Window::show_all();
                  wttl_wait.hide();
                  rest (2000);

                  Window::cascade_continue();

                  /*printf ("--- Start old method ---\r\n");
                  Window::draw_all();
                  textprintf_centre_old ( subbuffer, FNT_print, 320, 200, General_COLOR_TEXT, "Entering %s", game.mazename());
                  copy_buffer();
                  rest (2000);
                  printf ("--- end method ---\r\n");*/
               }
               else
               {
                  WinMessage wmsg_error ( "Party is not functionnal to enter maze\nEither it's empty or all characters are dead");
                  Window::show_all();
               }
               exit_city = true;
            break;
            case City_MENU_CAMP:
               //wmnu_place.hide();
               //Window::cascade_stop();
               //camp.start ( true );
               //Window::cascade_start();

               sprintf ( tmpstr, "WHERE location=2 and fkparty=%d", party.primary_key());

               if ( SQLcount ( "name", "character", tmpstr ) > 0 )
               {
                  party.status (Party_STATUS_CAMP);
                  exit_city = true;
               }
               else
               {
                  WinMessage wmsg_error ( "There is no characters in your party");
                  Window::show_all();
               }
            break;
         }



      }
   }



   party.SQLupdate();

   Window::cascade_stop();
//   debug_printf( __func__, "Exiting City");
   return ( retval );
}

/*void City::generate ( dbs_ADV_City_data dat )
{
 //?? to do convert adventure struct to properties
} */

/*void City::reference_target ( int citytaglist [ 16 ], int mazetaglist [ 16 ] )
{
   int i;
   int tmptag;

   for ( i = 0 ; i < City_NB_MAX_WARP ; i++ )
      p_warp [ i ] . tag . number ( 0 );

   for ( i = 0 ; i < City_NB_MAX_WARP ; i++ )
   {
      if ( p_warp [ i ] . target != - 1 )
      {
         if ( p_warp [ i ] .target < 16  )
         {
            p_warp [ i ].tag = citytaglist [ p_warp [ i ].target ];
         }
         else
            p_warp [ i ].tag = mazetaglist [ p_warp [ i ].target - 16 ];
      }
   }

}*/


/*-------------------------------------------------------------------------*/
/*-                      Private methods                                  -*/
/*-------------------------------------------------------------------------*/

void City::generate_item ()
{
   int nb_item;
   unsigned int base_prob = 0;
   int i;
   //int j;
   //int rarity;
   int error;
   //char querystr[101];
   Item tmpitem;
   int tmprarity;


      // get number of items to generate
      switch ( config.get(Config_MARKET_RESTOCK))
      {
         case Config_DIF_LOW: nb_item = 32;
         break;
         default:
         case Config_DIF_NORMAL: nb_item = 24;
         break;
         case Config_DIF_HIGH: nb_item = 16;
         break;
      }

      //change probability level for calculating rarity of item
      switch ( config.get(Config_MARKET_RARITY))
      {
         case Config_DIF_LOW: base_prob = 75;
         break;
         case Config_DIF_NORMAL: base_prob = 65;
         break;
         case Config_DIF_HIGH: base_prob = 50;
         break;
      }
      //base_prob=70;
      //printf ( "Probability = %d\r\n", base_prob);

      // remove items from the shop

      // oderby and limit not supported
      /*sprintf ( querystr, "DELETE FROM item WHERE loctype=%d ORDER BY random() LIMIT %d;", Item_LOCATION_CITY, nb_item / 2 );
      SQLexec( querystr );
      SQLerrormsg();*/

      SQLbegin_transaction();

      //printf ("Debug: City generate Item: pass 1\r\n");

      error = tmpitem.SQLpreparef("WHERE loctype=%d ORDER BY random() LIMIT %d", Item_LOCATION_CITY, nb_item );
      //printf ("\r\npass 0");
      if ( error == SQLITE_OK )
      {
         //printf ("\r\npass 1");
         error = tmpitem.SQLstep();

         while ( error == SQLITE_ROW )
         {
            tmpitem.SQLdelete();
            error = tmpitem.SQLstep();
         }
      }
      tmpitem.SQLfinalize();

      SQLcommit_transaction();

      //printf ("Debug: City generate Item: pass 2\r\n");

      // create item from template one at a time

      SQLbegin_transaction();

      for ( i = 0 ; i < nb_item; i++)
      {
         error = tmpitem.template_SQLprepare("ORDER BY random()");
         //printf ("\r\npass 0");
         if ( error == SQLITE_OK )
         {
            //printf ("\r\npass 1");
            error = tmpitem.SQLstep();

            if ( error == SQLITE_ROW )
            {
               //caculate rarity of the item
               tmprarity=0;
               while ( dice(100) < base_prob && tmprarity <12) /* max rarity is +3 instead +5*/
                  tmprarity++;
               //printf ("rarity = %d\r\n", tmprarity);

               tmpitem.rarity ( tmprarity );
               tmpitem.generate_ability ();
               tmpitem.identified ( true );
               tmpitem.location (Item_LOCATION_CITY, -1);

               tmpitem.SQLinsert ();
        //       printf ("Debug: City generate Item: pass 4\r\n");
            }
         }
         tmpitem.SQLfinalize();
      }

      SQLcommit_transaction();

      system_log.writef ("City: Generated new items " );
      //printf ("Debug: City generate Item: pass 5\r\n");
}




void City::generate_newgame_item ( void )
{
   int nb_item = 1;
   unsigned int base_prob = 0;
   int i;
   int j;
   //int rarity;
   int error;
   //char querystr[101];
   Item tmpitem;
   int tmprarity;
   int tmpnb_item;

   //printf("Debug: Generate New Game item: pass 1\r\n");
   switch ( config.get(Config_MARKET_RESTOCK))
      {
         case Config_DIF_LOW: nb_item = 32;
         break;
         case Config_DIF_NORMAL: nb_item = 24;
         break;
         case Config_DIF_HIGH: nb_item = 16;
         break;
      }

      //change probability level for calculating rarity of item
      switch ( config.get(Config_MARKET_RARITY))
      {
         case Config_DIF_LOW: base_prob = 75;
         break;
         case Config_DIF_NORMAL: base_prob = 65;
         break;
         case Config_DIF_HIGH: base_prob = 50;
         break;
      }
      //base_prob=70;
      //printf ( "Probability = %d\r\n", base_prob);




   // create item from template one at a time for each category

   //printf("Debug: Generate New Game item: pass 2\r\n");
   SQLbegin_transaction();

   for ( j = 1 ; j <= 5 ; j++ ) /// loop for item types
   {
      //printf("Debug: Generate New Game item: pass 3-%d\r\n", j);

      switch ( j )
      {
         case Item_TYPE_WEAPON: tmpnb_item = nb_item; break;
         case Item_TYPE_SHIELD: tmpnb_item = nb_item / 2; break;
         case Item_TYPE_ARMOR: tmpnb_item = nb_item / 2; break;
         case Item_TYPE_ACCESSORY: tmpnb_item = nb_item / 2; break;
         case Item_TYPE_EXPANDABLE: tmpnb_item = nb_item; break;
      }

      for ( i = 0 ; i < tmpnb_item; i++)
      {
         //printf("Debug: Generate New Game item: pass 4-%d\r\n", i);
         error = tmpitem.template_SQLpreparef("WHERE type=%d ORDER BY random()", j);
         //printf ("\r\npass 0");
         if ( error == SQLITE_OK )
         {
            //printf ("\r\npass 1");
            error = tmpitem.SQLstep();

            if ( error == SQLITE_ROW )
            {
               //caculate rarity of the item
               tmprarity=0;
               while ( dice(100) < base_prob && tmprarity <20)
                  tmprarity++;
               //printf ("rarity = %d\r\n", tmprarity);

               tmpitem.rarity ( tmprarity );
               tmpitem.generate_ability ();
               tmpitem.identified ( true );
               tmpitem.location (Item_LOCATION_CITY, -1);
               tmpitem.SQLinsert ();
            }
         }
         tmpitem.SQLfinalize();
      }

   }

   //printf("Debug: Generate New Game item: pass 5\r\n");
   SQLcommit_transaction();

   system_log.write ("City: Generated Items for the New Game" );
   //printf("Debug: Generate New Game item: pass 6\r\n");
}



void City::show_service_rest ( void )
{
   //int nb_character = party.nb_character();
   int nb_character = 0;//SQLcount ("*", "character", ""); //??tmpo
   //int character_keys [ Party_NB_MAX_CHAR ];
   Menu mnu_inn ( "Where and how much time do you want to stay ?" );
   //Menu mnu_days ( "How many days ?" );
   int answer = 0;
   //int answer2;
   int i;
   char tmpstr [ 101 ];
   //bool have_money;
   //int price [ 2 ];
   //int day_multi [ 4 ];
   int nb_days;
   int cost;
   int nextcost;
   bool full_recover = false;
   bool fineroom = false;

   Character selectchar;
   Character chartable [ 6 ];
   CClass tmpclass;
   CClass classtable [ 6 ];
   int tmpcharid [ Party_NB_MAX_CHAR ];
   int error;

   error = selectchar.SQLprepare ("WHERE location=2");

   if ( error == SQLITE_OK )
   {
      error = selectchar.SQLstep();
      //printf ("pass 1\r\n");

      while (error == SQLITE_ROW )
      {

         tmpcharid [ nb_character ] = selectchar.primary_key();
          //printf ( "Debug:City Rest: Character ID = %d, position = %d\n", tmpcharid [ nb_character ], selectchar.position() );
         nb_character++;
//printf ("pass 2\r\n");
         error = selectchar.SQLstep();
      }

   }
   selectchar.SQLfinalize ();

   /*day_multi [ 0 ] = 1;
   day_multi [ 1 ] = 3;
   day_multi [ 2 ] = 7;
   day_multi [ 3 ] = 14;*/

   mnu_inn.add_itemf ( 1, "Regular - 1 day  for %d ( 1 Gp / Person )", 1 * nb_character );
   mnu_inn.add_itemf ( 3, "Regular - 3 days for %d ( 3 Gp / Person )", 3 * nb_character );
   mnu_inn.add_itemf ( 5, "Regular - 5 days for %d ( 5 Gp / Person )", 5 * nb_character );
   mnu_inn.add_item ( 0, "Regular - Until Rested   ( Pay until restored )" );
   mnu_inn.add_itemf ( 11, "Fine - 1 days for %d ( 3 Gp / Person )", 1 * 3 * nb_character );
   mnu_inn.add_itemf ( 13, "Fine - 3 days for %d ( 9 Gp / Person )", 3 * 3 * nb_character );
   mnu_inn.add_itemf ( 15, "Fine - 5 days for %d ( 15 Gp / Person )", 5 * 3 * nb_character );
   mnu_inn.add_item ( 10, "Fine - Until Rested   ( Pay until restored )" );
   mnu_inn.add_item ( -1, "Exit");


   if ( nb_character >= 1)
   {
      WinMenu wmnu_inn ( mnu_inn, 20 , 50 );

      if ( answer != -1 )
      {
         Window::refresh_all();
         answer = Window::show_all();

         if ( answer >= 10 )
         {
            fineroom = true;
            answer = answer - 10;
         }

         if ( answer > 0)
         {
            // --- rest a fixed amount of days ---

            nb_days = answer;
            cost = nb_days * nb_character;

            if ( fineroom == true)
               cost *= 3;

            //printf ("nb days: %d\r\n", nb_days);
            //printf ("cost: %d\r\n", cost);

            SQLbegin_transaction ();
            if ( party.gold() >= cost )
            {
               WinTitle wttl_wait ("Please Wait", 320, 200);
               Window::show_all();
               wttl_wait.hide();

               //SQLbegin_transaction();
               for ( i = 0 ; i < nb_character ; i++ )
               {
              //    printf ("Pass inside\r\n");
                  selectchar.SQLselect ( tmpcharid [ i ]);
                  //printf ( "Debug:City Rest: Character ID = %d, position = %d\n", tmpcharid [ i ], selectchar.position() );
                  //debugging, uncomment in real game
                  tmpclass.SQLselect ( selectchar.FKcclass() );
                  selectchar.rest ( nb_days, tmpclass.HPdice(), tmpclass.MPdice(), fineroom );
                  selectchar.SQLupdate();
                  //printf ( "Debug:City Rest: Character ID = %d, position = %d\n", tmpcharid [ i ], selectchar.position() );
               }
               //SQLcommit_transaction();



               party.remove_gold ( cost );
               game.clock.add_day ( nb_days );
               sprintf ( tmpstr, "It cost you a total of %d gold", cost);
               WinMessage wmsg_cost ( tmpstr );

               while ( game.clock.day() > game.lastcityday())
               {
                  generate_item();
                  game.increment_lastcityday();
               }
               Window::refresh_all();
               Window::show_all();

            }
            else
            {
               sprintf ( tmpstr, "Sorry, you cannot afford the room.\nThe cost is %d", cost);
               WinMessage wmsg_error ( tmpstr );
               Window::show_all();
            }

            SQLcommit_transaction ();
         }

         if ( answer == 0 )
         {
         // rest until restored
            cost = 0;
            nextcost = nb_character;
            int nb_days = 0;

            if ( fineroom == true)
               { nextcost *= 3; }

            full_recover = false;

            WinTitle wttl_wait ("Please Wait", 320, 200);
            Window::show_all();
            wttl_wait.hide();

            /*full_recover = true;
               for ( i = 0; i < nb_character; i++)
               {
                  selectchar.SQLselect ( tmpcharid [ i ]);

                  if ( ( selectchar.current_HP() < selectchar.max_HP()
                     || selectchar.current_MP() < selectchar.max_MP() ) && selectchar.body () == Character_BODY_ALIVE )
                  {
                     full_recover = false;
                  }
               }*/

            //int debugloop = 0;

            SQLbegin_transaction();

            for ( i = 0 ; i < nb_character; i++)
            {
               chartable [ i ].SQLselect ( tmpcharid [ i ]);
               classtable [ i ].SQLselect ( chartable [ i ]  . FKcclass() );
            }

            while ( full_recover == false && nextcost <= party.gold()
                   /*&& debugloop < 5*/ )
            {
               /*if ( full_recover == true )
                  printf ("Debug: City Rest: Full recover = True\n");
               else
                  printf ("Debug: City Rest: Full recover = false\n");*/

               full_recover = true;

               for ( i = 0; i < nb_character; i++)
               {
                  if ( ( chartable [ i ].current_HP() < chartable [ i ].max_HP()
                      || chartable [ i ].current_MP() < chartable [ i ].max_MP() )
                      && chartable [ i ].body () == Character_BODY_ALIVE )
                  chartable [ i ].rest ( 1 , classtable [ i ] . HPdice(), classtable [i].MPdice(), fineroom );
                  //printf ("cost=%d character=%s HP=%d MP=%d\n", nextcost,
                  //        selectchar.name(), selectchar.current_HP(), selectchar.current_MP() );

                  if ( ( chartable [ i ].current_HP() < chartable [ i ].max_HP()
                      || chartable [ i ].current_MP() < chartable [ i ].max_MP() )
                      && chartable [ i ].body () == Character_BODY_ALIVE )
                   {
                      full_recover = false;
                   }


               }
               nb_days++;

               //debugloop++;


               if ( fineroom == true)
                  nextcost += nb_character * 3;
               else
                  nextcost += nb_character;



            }

            for ( i = 0 ; i < nb_character; i++)
               chartable [ i ].SQLupdate();


            SQLcommit_transaction();

            game.clock.add_day( nb_days );

            if ( fineroom == true)
                  cost = nb_character * nb_days * 3;
            else
                  cost = nb_character * nb_days;


            if ( cost > 0 )
            {
               //if ( fineroom == true)
                 // cost *= 3;

               party.remove_gold ( cost );
               //for ( i = 0 ; i < nb_character ; i++ )
               //   tmpcharacter [ i ].SQLupdate();
            }

            sprintf ( tmpstr, "It cost you a total of %d gold", cost);
            WinMessage wmsg_cost ( tmpstr );

            while ( game.clock.day() > game.lastcityday())
            {
               generate_item();
               game.increment_lastcityday();
            }
            Window::refresh_all();
            Window::show_all();

         }
      }
   }
   else
   {
      WinMessage wmsg_error ( "Sorry, you need characters in your party." );
      Window::show_all();
   }

   Window::refresh_all();
   //debug_printf( __func__, "Before party SQLupdate");
   party.SQLupdate();
   //debug_printf( __func__, "After party SQLupdate");
   //printf ("City-rest: I exit here\r\n");
}

void City::show_service_levelup ( void )
{
   Character tmpchar;
   Character currentchar;
   int retval;
   int levelraised = false;
   int bonuscharacterpoints = 0;
   char tmpstr [80];

   Window::cascade_pause();

   retval = tmpchar.SQLpreparef ( "WHERE location=%d AND body=%d", Character_LOCATION_PARTY
                           , Character_BODY_ALIVE);
   //tmpoldchar.SQLerrormsg();

   //WinEmpty wemp_background();
   WinData<int> wdat_combat_log ( WDatProc_combat_log, 0, WDatProc_POSITION_COMBAT_LOG );

   if ( retval == SQLITE_OK )
   {
      retval = tmpchar.SQLstep();

      SQLbegin_transaction();

      while ( retval == SQLITE_ROW )
      {
         levelraised = false;
         bonuscharacterpoints = 0;
         currentchar.SQLselect  ( tmpchar.primary_key());
         system_log.mark();
         //printf ("Debug: Level up: pass 1");

         while ( currentchar.check_levelup() == true )
         {
            currentchar.raise_level();
            levelraised = true;
            //printf ("Debug: Level up: pass 2");

            if ( currentchar.level() % Character_LEVELUP_ATTRIBUTE_DIVIDER
                [ config.get(Config_LEVELUP_ATTRIBUTE) ] == 0)
               bonuscharacterpoints++;
            //printf ("Debug: City: Service Levelup: %s gained a level (%d) \n", currentchar.name(), currentchar.level() );
         }

         Window::instruction ( 320, 80, Window_INSTRUCTION_SELECT );

         if (levelraised == true)
         {
            //display windows
//            play_sample ( SMP_sound036, 255, 128, 1000, 0 );
            play_sound ( 1029 ); // level up

            system_log.writef ( "%s raised from level %d to %d", tmpchar.name(),
                               tmpchar.level(), currentchar.level() );

            Window::refresh_all();
            WinData<Character> wdat_old ( WDatProc_character_levelup,
               tmpchar, WDatProc_POSITION_LEVELUP_OLD );
         //tmpchar.raise_level();
            WinData<Character> wdat_new ( WDatProc_character_levelup,
               currentchar, WDatProc_POSITION_LEVELUP_NEW );
         //WinMessage wmsg_levelup ( strmsg );

            Window::show_all();
            copy_buffer();

            while (mainloop_readkeyboard() != SELECT_KEY );
         //tmpchar.SQLupdate();

            // ----- Give bonus character points ---

            if ( bonuscharacterpoints > 0)
            {
               Race tmprace;
               tmprace.SQLselect ( currentchar.FKrace() );
               sprintf ( tmpstr, "%s has gained character points", currentchar.name() );

               Distributor dst_attribute;

               dst_attribute.title ( tmpstr );
               dst_attribute.header ( "Attributes" );
               dst_attribute.total ( "Character Points" );

               dst_attribute.add_item ("STRength", currentchar.attribute(0), tmprace.attribute(0) + 10 );
               dst_attribute.add_item ("DEXterity", currentchar.attribute(1), tmprace.attribute(1) + 10 );
               dst_attribute.add_item ("ENDurence", currentchar.attribute(2), tmprace.attribute(2) + 10 );
               dst_attribute.add_item ("INTelligence", currentchar.attribute(3), tmprace.attribute(3) + 10 );
               dst_attribute.add_item ("CUNning", currentchar.attribute(4), tmprace.attribute(4) + 10 );
               dst_attribute.add_item ("WILpower", currentchar.attribute(5), tmprace.attribute(5) + 10 );

               dst_attribute.total_value ( bonuscharacterpoints );

               WinDistributor wdst_attribute ( dst_attribute, 10, 100 );
               Window::show_all();

               currentchar.attribute( Character_STR, dst_attribute.item_value(Character_STR) );
               currentchar.attribute( Character_DEX, dst_attribute.item_value(Character_DEX) );
               currentchar.attribute( Character_END, dst_attribute.item_value(Character_END) );
               currentchar.attribute( Character_INT, dst_attribute.item_value(Character_INT) );
               currentchar.attribute( Character_CUN, dst_attribute.item_value(Character_CUN) );
               currentchar.attribute( Character_WIL, dst_attribute.item_value(Character_WIL) );


            }

         //   printf ("Debug: City: Service Levelup: before SQLupdate\n");
            currentchar.SQLupdate();
        //    printf ("Debug: City: Service Levelup: after SQLupdate\n");
            //printf ("Debug: Level up: pass 3");
         }
         else
         {
            if ( tmpchar.level() < Character_MAX_LEVEL )
               system_log.writef ( "%s need %d to raise to level %d", tmpchar.name(),
                               ( tmpchar.next_level_exp() - tmpchar.exp() ),tmpchar.level() +1 );
            else
               system_log.writef ( "%s has reached the max level", tmpchar.name() );

            Window::show_all();
            copy_buffer();

            while (mainloop_readkeyboard() != SELECT_KEY );
         }

         retval = tmpchar.SQLstep();
      }

      //printf ("Debug: City: Service Levelup: before SQLcommit transaction\n");
      SQLcommit_transaction();
      //printf ("Debug: City: Service Levelup: after SQLcommit transaction\n");
   }

   if ( SQLcount ("name", "character", "WHERE location=2 AND body=0" ) == 0)
   {
      WinMessage wmsg_error ("There is no character alive in your party");
      Window::show_all();
   }

   //printf ("Debug: City: Service Levelup: before SQLfinalize\n");
   tmpchar.SQLfinalize();
   //printf ("Debug: City: Service Levelup: after SQLfinalize\n");

   Window::cascade_continue();

   //WinMessage wmsg_complete ("Level up verification complete");

   //Window::show_all();



}

void City::show_service_buy ( void )
{

   Menu mnu_equipment ("What do you need ?");
   int answer = 0;
   int answer2 = 0;
   Item tmpitem;
   int error = 0;
   //char itemstr [11];


   mnu_equipment.add_item (Item_TYPE_WEAPON, "Weapons");
   mnu_equipment.add_item (Item_TYPE_SHIELD, "Shield");
   mnu_equipment.add_item (Item_TYPE_ARMOR, "Armor" );
   mnu_equipment.add_item (Item_TYPE_ACCESSORY, "Accessories" );
   mnu_equipment.add_item (Item_TYPE_EXPANDABLE, "Expandables" );
   mnu_equipment.add_item (-1, "Exit" );

   while ( answer != -1 )
   {

      WinMenu wmnu_equipment ( mnu_equipment, 20, 50 );
      answer = Window::show_all();
      wmnu_equipment.hide();

      if ( answer != -1)
      {
         Window::cascade_pause();
         Window::instruction ( 320, 282 );
         List lst_item ("Select and item to buy", 11, true);
         lst_item.header ("Name                                               Price");

         error = tmpitem.SQLpreparef ( "WHERE type=%d AND loctype=%d ORDER BY profiency, name", answer, Item_LOCATION_CITY );
         //tmpitem.SQLerrormsg();

         if ( error == SQLITE_OK )
         {
            //printf ("Debug: City- buy item: pass1\r\n");
            error = tmpitem.SQLstep();

            while ( error == SQLITE_ROW)
            {
               /*switch ( answer )
               {
                  case Item_TYPE_WEAPON:
                     strcpy ( itemstr, STR_ITM_CAT_WEAPON[ bitoindex(tmpitem.category()) ]);
                  break;
                  case Item_TYPE_SHIELD:
                     strcpy ( itemstr, STR_ITM_CAT_SHIELD[ bitoindex(tmpitem.category()) ]);
                  break;
                  case Item_TYPE_ARMOR:
                     strcpy ( itemstr, STR_ITM_CAT_ARMOR[ bitoindex(tmpitem.category()) ]);
                  break;
                  case Item_TYPE_ARMOR:
                     strcpy ( itemstr, STR_ITM_CAT_ARMOR[tmpitem.category]);
                  break;
                  default:
                     strcpy ( itemstr, "test");
                  break;
               }*/

               //printf ("Debug: City- buy item: pass2\r\n");
               lst_item.add_itemf ( tmpitem.primary_key(), "%-40s %5d Gp", tmpitem.name(), tmpitem.price());

               error = tmpitem.SQLstep();

            }
         }
         tmpitem.SQLfinalize();

         if ( lst_item.nb_item() > 0 )
         {
            WinData<int> wdat_equip ( WDatProc_character_equip, lst_item.answer(0), WDatProc_POSITION_CHARACTER_EQUIP );
            WinData<int> wdat_item ( WDatProc_inspect_item, lst_item.answer(0), WDatProc_POSITION_INSPECT_ITEM );

            WinList wlst_item ( lst_item, 20, 50 );
            answer2 = 0;

            while ( answer2 != -1  && lst_item.nb_item() > 0)
            {

               while ( lst_item.selected() == false)
               {

                  Window::refresh_all();
                  answer2 = Window::show_all();
                  wdat_equip.key ( lst_item.answer());
                  wdat_item.key ( lst_item.answer());
                  //printf("Answer2=%d\r\n", answer2);
               }

               if ( answer2 != -1 )
               {
                  tmpitem.SQLselect ( answer2 );

                  if ( party.gold() >= tmpitem.price())
                  {
                     tmpitem.location (Item_LOCATION_PARTY, party.primary_key());
                     tmpitem.SQLupdate();
                     party.remove_gold ( tmpitem.price());
                     lst_item.remove_selected_item();
                     lst_item.selected ( false );
                     wdat_equip.key ( lst_item.answer());
                     wdat_item.key ( lst_item.answer());
                     Window::refresh_all();

                     system_log.writef ("City: Bought %s for %d ", tmpitem.name(), tmpitem.price() );
                  }
                  else
                  {
                     WinMessage wmsg_expensive ("You do not have enough gold to buy this item");
                     Window::show_all();
                     lst_item.selected ( false );
                  }
               }
            }
         }
         else
         {
            WinMessage wmsg_notavailable ("There is no items of this type available for sale");
            Window::show_all();
         }
      }
      Window::cascade_continue();
      Window::instruction ( 320, 340 );

   }


}

void City::show_service_sell ( void )
{

   int answer = 0;
   int error;
   //char itemstr[11];
   List lst_item ( "Which item do you want to sell?", 14);
   lst_item.header ("Name                                               Price");
   Item tmpitem;

   Window::cascade_pause();

   //error = tmpitem.SQLpreparef ( "WHERE type=%d AND loctype=%d AND lockey=%d ORDER BY type, category", answer, Item_LOCATION_PARTY, party.primary_key() );
   error = tmpitem.SQLpreparef ( "WHERE loctype=%d AND lockey=%d ORDER BY type, profiency",  Item_LOCATION_PARTY, party.primary_key() );
   //error = tmpitem.SQLpreparef ( "WHERE loctype=%d ORDER BY type, category",  Item_LOCATION_PARTY );
   //error = tmpitem.SQLprepare ( "ORDER BY type, category" );
   //tmpitem.SQLerrormsg();

   if ( error == SQLITE_OK )
   {
      //printf ("Debug: City- sell item: pass1\r\n");
      error = tmpitem.SQLstep();

      while ( error == SQLITE_ROW)
      {
         /*switch ( answer )
         {
            case Item_TYPE_WEAPON:
               strcpy ( itemstr, STR_ITM_CAT_WEAPON[tmpitem.category]);
            break;
            case Item_TYPE_SHIELD:
               strcpy ( itemstr, STR_ITM_CAT_SHIELD[tmpitem.category]);
            break;
            case Item_TYPE_ARMOR:
               strcpy ( itemstr, STR_ITM_CAT_ARMOR[tmpitem.category]);
            break;
            default:
               strcpy ( itemstr, "test");
            break;
         }*/
         //printf ("Debug: City- sell item: pass2\r\n");
         if ( tmpitem.identified() == true )
            lst_item.add_itemf ( tmpitem.primary_key(), "%-40s %5d Gp", tmpitem.name(), tmpitem.price() / 2 );
         //lst_item.add_itemf ( tmpitem.primary_key(), "%d:%-20s %d:%d", tmpitem.primary_key(), tmpitem.name(), tmpitem.location_type(), tmpitem.location_key() );
         error = tmpitem.SQLstep();

      }
   }
   tmpitem.SQLfinalize();

   if ( lst_item.nb_item() > 0 )
   {
      //WinData<int> wdat_equip ( WDatProc_character_equip, lst_item.answer(0), WDatProc_POSITION_CHARACTER_EQUIP );
      //WinData<int> wdat_item ( WDatProc_inspect_item, lst_item.answer(0), WDatProc_POSITION_INSPECT_ITEM );

      WinList wlst_item ( lst_item, 20, 50 );
      answer = 0;

      while ( answer != -1  && lst_item.nb_item() > 0)
      {
         answer = Window::show_all();

         if ( answer != -1 )
         {
            tmpitem.SQLselect ( answer );
            tmpitem.location (Item_LOCATION_CITY, -1);
            party.add_gold ( tmpitem.price() / 2);
            tmpitem.SQLupdate();

            system_log.writef ("City: Sold %s for %d", tmpitem.name(), tmpitem.price() / 2 );

            lst_item.remove_selected_item();
            Window::refresh_all();
         }
      }
   }
   else
   {
      WinMessage wmsg_noinventory ("Your party inventory is empty");
      Window::show_all();
   }

   Window::cascade_continue();

}

void City::show_service_identify ( void )
{
   //SQLexec ("UPDATE character SET hp=1, mp=1 WHERE location=2");
}

void City::show_service_heal ( void )
{
   //printf ("Debug: City: Heal: Before active effect declaration\n");
   ActiveEffect tmpeffect;
   //printf ("Debug: City: Heal: After active effect declaration\n");
   Character tmpchar;
   int error;
   //int cost;
   int cost_table [6] =
      { 100, //ActiveEffect_CATEGORY_BODY
        200, //ActiveEffect_CATEGORY_BLOOD         2
        300, //ActiveEffect_CATEGORY_SOUL          3
        150, //ActiveEffect_CATEGORY_NEURAL        4
        250, //ActiveEffect_CATEGORY_PSYCHIC       5
        50   //ActiveEffect_CATEGORY_SKIN          6
      };

   //SQLactivate_errormsg();
   //printf ("Debug: City: Heal: Before SQL prepare\n");
   error = tmpeffect.SQLpreparef ( "WHERE target_type=%d ORDER BY target_id", Opponent_TYPE_CHARACTER );

   List lst_effect ("Which effect do you want to cure?", 8);
   lst_effect.header ("Character              Cost   Effect");

   if ( error == SQLITE_OK)
   {
       error = tmpeffect.SQLstep ();

       while ( error == SQLITE_ROW )
       {
          //printf ("Debug: City: Heal: Before select character\n");
          tmpchar.SQLselect ( tmpeffect.target_id());

          lst_effect.add_itemf ( tmpeffect.primary_key(), "%-15s  %3d  %-32s"
                                ,tmpchar.name()
                                ,cost_table [ tmpeffect.category() - 1]
                                ,tmpeffect.name() );
          error = tmpeffect.SQLstep ();
       }

       tmpeffect.SQLfinalize();
   }


   if ( lst_effect.nb_item() > 0 )
   {
      //printf ("Debug: City: Heal: Inside list\n");
      lst_effect.add_item ( -1, "Exit");

      WinList wlst_effect ( lst_effect, 20, 50 );

      int answer = Window::show_all();

      //printf ("Debug: City: Heal: After Display List\n");

      if ( answer != -1 )
      {
         tmpeffect.SQLselect ( answer );

         if ( cost_table [ tmpeffect.category() ] <= party.gold() )
         {

            party.remove_gold ( cost_table [ tmpeffect.category()] );

            tmpeffect.SQLdelete();

            char tmpstr [101];

            tmpchar.SQLselect ( tmpeffect.target_id() );

            sprintf ( tmpstr, "Murmur - Chant - Pray - Invoke ^ %s is cured", tmpchar.name() );

            play_sound ( 1028 );

            //wmnu_character.hide();
            WinMessage wmsg_error (tmpstr);
            Window::show_all();
            //Character::force_recompile_ranks();
         }
         else
         {
            WinMessage wmsg_error ("You do not have enough gold");
            Window::show_all();

         }

      }

   }
   else
   {
      WinMessage wmsg_error ("No characters needs to be cured");
      Window::show_all();
   }

}

void City::show_service_revive ( void )
{

   Character tmpchar;
   int error;
   int cost;

   error =  tmpchar.SQLpreparef ("WHERE body > %d AND body < %d", Character_BODY_ALIVE, Character_BODY_DELETED);

   Menu mnu_character ("Which Character needs my help?");
   mnu_character.header ("Name                  Level    Body             Cost");

   if ( error == SQLITE_OK )
   {
      error = tmpchar.SQLstep();

      //SQLbegin_transaction();

      while ( error == SQLITE_ROW )
      {
         cost = tmpchar.level() * tmpchar.body() * 25;

         mnu_character.add_itemf ( tmpchar.primary_key(), "%-15s  %2d  [ %-10s ] %d Gp",
                                  tmpchar.name(), tmpchar.level(), STR_CHR_BODY [ tmpchar.body() ], cost );


         error = tmpchar.SQLstep();
      }

      //SQLcommit_transaction();
   }
   tmpchar.SQLfinalize();

   if ( mnu_character.nb_item () > 0 )
   {
      mnu_character.add_item ( -1, "Exit");

      WinMenu wmnu_character ( mnu_character, 20, 50 );

      int answer = Window::show_all();

      if ( answer != -1 )
      {
         tmpchar.SQLselect ( answer );

         cost = tmpchar.level() * tmpchar.body() * 25;

         if ( cost <= party.gold() )
         {

            party.remove_gold ( cost );
            tmpchar.revive();

            if ( tmpchar.current_HP() <= 0 && tmpchar.body() == Character_BODY_ALIVE)
               tmpchar.recover_HP ( 1 );

            tmpchar.SQLupdate();

            char tmpstr [101];

            sprintf ( tmpstr, "Murmur - Chant - Pray - Invoke ^ %s is %s", tmpchar.name(), STR_CHR_BODY [ tmpchar.body() ] );

            play_sound ( 1028 );

            //wmnu_character.hide();
            WinMessage wmsg_error (tmpstr);
            Window::show_all();
            Character::force_recompile_ranks();
         }
         else
         {
            WinMessage wmsg_error ("You do not have enough gold");
            Window::show_all();

         }

      }

   }
   else
   {
      WinMessage wmsg_error ("No characters needs to be revived");
      Window::show_all();
   }

   //Opponent::compile_ranks();

}

/*int City::show_edge_of_town ( void )
{

   BITMAP* tmpbmp;
   unsigned int index;
   int i;
   City tmpcity;
   Maze tmpmaze;
   List lst_target ("Select Destination", 10 );
//   string tmpstr;
   char strmenu [ 51 ];
   int answer;
   int color = 0;
   int retval;
   int citylist [ 16 ];
   unsigned int index;
   int i;


   index = db.search_table_entry ( DBTABLE_CITY );
   i = 0;
   while ( db.entry_table_tag ( index ) == DBTABLE_CITY )
   {
      city
   }


   tmpbmp = create_bitmap ( 400, 400 );

   blit ( BMP_aworldmap, tmpbmp, 0, 0, 0, 0, 400, 400 );

   textout_centre_old ( tmpbmp, FNT_small, p_shortname,
      p_xpos + 12, p_ypos - 16, makecol ( 225, 225, 225 ) );

   i = 0;
   while ( p_warp [ i ] . type != -1 )
   {
      if ( p_warp [ i ] . target < 16 )
      {
         tmpcity.SQLselect ( p_warp [ i ].tag );

         switch ( p_warp [ i ] . type )
         {
            case City_WARPTYPE_LAND :
               color = makecol ( 100, 225, 100 );
            break;
            case City_WARPTYPE_SEA :
               color = makecol ( 100, 100, 225 );
            break;
            case City_WARPTYPE_AIR :
               color = makecol ( 225, 225, 225 );
            break;
            case City_WARPTYPE_MAGIC :
               color = makecol ( 225, 100, 100 );
            break;
         }
         sprintf ( strmenu,"%s %s", STR_CTY_WARPACTION [ p_warp [ i ] . type ],
            tmpcity.p_shortname );

         lst_target.add_item ( strmenu );
         line ( tmpbmp, p_xpos + 8, p_ypos + 8,
            tmpcity.xpos() + 8, tmpcity.ypos() + 8, color );
         textout_centre_old ( tmpbmp, FNT_small, tmpcity.p_shortname,
         tmpcity.xpos() + 12, tmpcity.ypos() - 16, makecol ( 225, 225, 225 ) );

      }
      else
      {
         tmpmaze.SQLselect ( p_warp [ i ] . tag );
         sprintf ( strmenu, "Explore %s",tmpmaze.shortname() );
         lst_target.add_item ( strmenu );
         line ( tmpbmp, p_xpos + 8, p_ypos + 8,
            tmpmaze .xpos() + 8, tmpmaze.ypos() + 8, General_COLOR_TEXT );
         textout_centre_old ( tmpbmp, FNT_small, tmpmaze.shortname(),
         tmpmaze.xpos() + 12, tmpmaze.ypos() - 16, makecol ( 225, 225, 225 ) );

         switch ( tmpmaze.type() )
         {
            case Maze_TYPE_CAVE :
               draw_sprite ( tmpbmp, BMP_icn_cave, tmpmaze.xpos(), tmpmaze.ypos() );
            break;
            case Maze_TYPE_KEEP :
               draw_sprite ( tmpbmp, BMP_icn_keep, tmpmaze.xpos(), tmpmaze.ypos() );
            break;
            case Maze_TYPE_RUIN :
               draw_sprite ( tmpbmp, BMP_icn_ruin, tmpmaze.xpos(), tmpmaze.ypos() );
            break;
            case Maze_TYPE_TEMPLE :
               draw_sprite ( tmpbmp, BMP_icn_temple, tmpmaze.xpos(), tmpmaze.ypos() );
            break;
            case Maze_TYPE_TOWER :
               draw_sprite ( tmpbmp, BMP_icn_tower, tmpmaze.xpos(), tmpmaze.ypos() );
            break;
         }
      }
      i++;
   }

   index = db.search_table_entry ( DBTABLE_CITY );
   while ( db.entry_table_tag ( index ) == DBTABLE_CITY )
   {
      tmpcity.SQLselect ( index );
      switch ( tmpcity.type() )
      {
         case City_TYPE_VILLAGE :
            draw_sprite ( tmpbmp, BMP_icn_village, tmpcity.xpos(), tmpcity.ypos() );
         break;
         case City_TYPE_CITY :
            draw_sprite ( tmpbmp, BMP_icn_city, tmpcity.xpos(), tmpcity.ypos() );
         break;
         case City_TYPE_CASTLE :
            draw_sprite ( tmpbmp, BMP_icn_castle, tmpcity.xpos(), tmpcity.ypos() );
         break;
      }
      index++;
   }


   lst_target.add_item ("Cancel");



   WinData<BITMAP> wdat_worldmap ( WDatProc_worldmap, *tmpbmp,
      WDatProc_POSITION_WORLDMAP );

   WinList wlst_target ( lst_target, 420, 30 );
   Window::instruction ( 320, 460 );

   answer = Window::show_all();
//   make_screen_shot ( "mapshot.bmp");//@@
   retval = City_CONTINUE;

   if ( answer != -1 && answer != i )
   {
      if ( p_warp [ answer ] . target >= 16 )
      {
         party.reset_position();
         party.location ( p_warp [ answer ] . tag );
         party.status ( Party_STATUS_MAZE );
//         party.SQLupdate();
         retval = City_WARP_TO_MAZE; // indicate the party has just warped
         // select maze
      }
      else
      {
         party.location ( p_warp [ answer ] . tag );
         party.status ( Party_STATUS_CITY );
//         party.SQLupdate();
         retval = City_WARP_TO_CITY;
      }
   }

   destroy_bitmap ( tmpbmp );


}*/

void City::show_service_characters ( void )
{
   Menu player_menu;
   Game tmpgame;
   int answer = 0;
   int answer2 = 0;
   int confirm = 0;
   //int ret_value;
   //int tmpval;
   int nb_character = 0;
   Character tmpcharacter;
   CClass tmpclass;
   Race tmprace;
   int error;



   player_menu.title ("Character Management");

   player_menu.add_item ( City_CREATE, "Create Character" );
   player_menu.add_item ( City_INSPECT, "Inspect Character" );
   player_menu.add_item ( City_DELETE, "Delete Character" );
   player_menu.add_item ( City_RENAME_CHARACTER, "Rename Character");
   player_menu.add_item ( -1, "Exit" );

   //printf ("pass 1\r\n");

   //play_music_track ( System_MUSIC_w2edge );
   //ret_value = Player_CONTINUE;

   //WinEmpty wemp_background;
   //WinTitle wttl_player ("Player's Domain");



   while ( answer != -1 )
   {
      List lst_character ("", 12 );
      player_menu.unmask_all_item ();
      lst_character.header ("Name                 Race Class Level Body          Location");

//      nb_character = SQLcount ( "name", "character", "" );

      //if ( nb_character == Player_NB_MAX_CHARACTER )
      //   player_menu.mask_item ( Player_CREATE_CHARACTER );

      //fill up the character menu
      //printf ("pass 2\r\n");
      nb_character = 0;
      error = tmpcharacter.SQLprepare ();

      if ( error == SQLITE_OK )
      {
         //printf ("pass 2.1\r\n");
         error = tmpcharacter.SQLstep();

         while ( error == SQLITE_ROW)
         {
            //printf ("pass 2.2\r\n");
            tmpclass.SQLselect ( tmpcharacter.FKcclass() );
            tmprace.SQLselect ( tmpcharacter.FKrace() );
            //printf ("name=%s", tmpcharacter.name());
            lst_character.add_itemf ( tmpcharacter.primary_key(), "%-15s %3s %3s %3d %-10s %-10s", tmpcharacter.name(),
                                     tmprace.initial(), tmpclass.initial(), tmpcharacter.level(),
                                     STR_CHR_BODY [tmpcharacter.body()], STR_CHR_LOCATION [tmpcharacter.location()]);
            error = tmpcharacter.SQLstep();
            nb_character++;
         }
      }

      tmpcharacter.SQLfinalize();

      //printf ("pass 3\r\n");

      if ( nb_character == 0 )
      {
         //printf ("pass 4a\r\n");
         player_menu.mask_answer ( City_INSPECT );
         player_menu.mask_answer ( City_DELETE );
         player_menu.mask_answer ( City_RENAME_CHARACTER );
         //printf ("pass 4b\r\n");
      }

      Window::instruction ( 320, 460 );

      WinMenu wmnu_player ( player_menu, 20 , 50 );
      answer = Window::show_all();

      wmnu_player.hide();
      //printf ("pass 5\r\n");
      WinList wlst_character (lst_character, 20 , 50 );
      wlst_character.hide();

      //printf ("pass 6\r\n");
      switch ( answer )
      {

         case City_CREATE :
            show_subservice_create_character ();
         break;

         case City_INSPECT :
            answer2 = 0;
            lst_character.title ("Select a character to INSPECT");
            while ( answer2 != -1)
            {
               wlst_character.unhide();
               answer2 = Window::show_all();
               //wlst_character.hide();
               if (answer2 >= 0 )
               {
                  tmpcharacter.SQLselect ( answer2 );

               // try to convert using primary_key. No need to select
                  WinData<Character> wdat_character ( WDatProc_character,
                     tmpcharacter, WDatProc_POSITION_CHARACTER  );
                  Window::instruction ( 550, 460, Window_INSTRUCTION_CANCEL );
                  Window::draw_all();

                  copy_buffer();


                  while (mainloop_readkeyboard() != CANCEL_KEY );
               }
            }
         break;

         case City_DELETE :
               answer2 = 0;
               lst_character.title ("Select a character to DELETE");

               while ( answer2  != -1 && lst_character.nb_item() > 0 )
               {
                  wlst_character.unhide();
                  answer2 = Window::show_all();

                  if ( answer2 != -1 )
                  {
                     tmpcharacter.SQLselect ( answer2 );
               // try to convert using primary_key. No need to select
                     WinData<Character> wdat_character ( WDatProc_character,
                        tmpcharacter, WDatProc_POSITION_CHARACTER );
               //wlst_character.hide();
                     WinQuestion wqst_confirm ( "Are you sure you want to delete this character?", 320, 360 );
                     confirm = Window::show_all();

                     if ( confirm == WinQuestion_ANSWER_YES )
                     {
                        tmpcharacter.SQLdelete();
                        lst_character.remove_selected_item();
                     //Window::refresh_all(); //not sure why
                     }
                  }

               }

         break;

         case City_RENAME_CHARACTER :
               answer2 = 0;
               lst_character.title ("Select a character to RENAME");

               while ( answer2  != -1)
               {
                  wlst_character.unhide();
                  answer2 = Window::show_all();

                  if ( answer2 != -1 )
                  {

                     tmpcharacter.SQLselect ( answer2 );
               // try to convert using primary_key. No need to select
                  //WinData<Character> wdat_character ( WDatProc_character,
                     //tmpcharacter, WDatProc_POSITION_CHARACTER );
               //wlst_character.hide();
                     wlst_character.hide();
                     WinInput winp_name("Enter the new name of your character", Character_NAME_STRLEN -1, 10, 40);

                     Window::show_all();

                     tmpcharacter.name ( winp_name.get_string() );

                     tmpcharacter.SQLupdate();

                     tmpclass.SQLselect ( tmpcharacter.FKcclass() );
                     tmprace.SQLselect ( tmpcharacter.FKrace() );
                     //printf ("Rename cursor value: %d\r\n", lst_character.cursor());
                     lst_character.update_selected_itemf (tmpcharacter.primary_key(), "%-15s %3s %3s %3d %-10s %-10s", tmpcharacter.name(),
                                     tmprace.initial(), tmpclass.initial(), tmpcharacter.level(),
                                     STR_CHR_BODY [tmpcharacter.body()], STR_CHR_LOCATION [tmpcharacter.location()] );
                  }
               }
         break;
      }
   }

   //return ( ret_value );

}

void City::show_service_party ( void )
{

   Menu mnu_party;
   Game tmpgame;
   int answer = 0;
   int answer2 = 0;
   //int confirm = 0;
   //int ret_value;
   //int tmpval;
   //int nb_character = 0;
   Character tmpcharacter;
   CClass tmpclass;
   Race tmprace;
   int error;
   int nb_char_in_party = 0;
   int nb_char_in_reserve = 0;

   mnu_party.title ("Party Management");

   mnu_party.add_item ( City_PARTYMENU_ADD, "Add Character" );
   mnu_party.add_item ( City_PARTYMENU_REMOVE, "Remove Character" );
   mnu_party.add_item ( City_PARTYMENU_REMOVEALL, "Remove all Character" );
   mnu_party.add_item ( -1, "Exit" );


   while ( answer != -1 )
   {
      mnu_party.unmask_all_item ();

      nb_char_in_party = SQLcount ( "name", "character", "WHERE location=2" );
      nb_char_in_reserve = SQLcount ( "name", "character", "WHERE location=1" );

//      nb_character = SQLcount ( "name", "character", "" );
      //printf ("nb character = %d\r\n", nb_character);

      if ( nb_char_in_reserve == 0 )
      {
         //printf ("pass 4a\r\n")
         mnu_party.mask_answer ( City_PARTYMENU_ADD );
         //player_menu.mask_answer ( City_DELETE );
         //player_menu.mask_answer ( City_RENAME_CHARACTER );
         //printf ("pass 4b\r\n");
      }

      if ( nb_char_in_party == 0 )
      {
         mnu_party.mask_answer ( City_PARTYMENU_REMOVEALL );
         mnu_party.mask_answer ( City_PARTYMENU_REMOVE );
      }

      //Window::instruction ( 320, 460 );

      WinMenu wmnu_party ( mnu_party, 20 , 50 );
      Window::refresh_all();
      answer = Window::show_all();

      wmnu_party.hide();
      //printf ("pass 5\r\n");

      if ( answer == City_PARTYMENU_ADD )
      {
         List lst_character ("Select a character to ADD", 10 );

         lst_character.header ("Name                 Race Class Level Body          Location");

         error = tmpcharacter.SQLprepare ("WHERE location=1");

         if ( error == SQLITE_OK )
         {

            error = tmpcharacter.SQLstep();

            while ( error == SQLITE_ROW)
            {
               tmpclass.SQLselect ( tmpcharacter.FKcclass() );
               tmprace.SQLselect ( tmpcharacter.FKrace() );
               lst_character.add_itemf ( tmpcharacter.primary_key(), "%-15s %3s %3s %3d %-10s %-10s", tmpcharacter.name(),
                                     tmprace.initial(), tmpclass.initial(), tmpcharacter.level(),
                                     STR_CHR_BODY [tmpcharacter.body()], STR_CHR_LOCATION [tmpcharacter.location()]);
               error = tmpcharacter.SQLstep();
            }
         }

         tmpcharacter.SQLfinalize();

         WinList wlst_character (lst_character, 20, 50);

         answer2 = 0;


         while ( answer2  != -1 && lst_character.nb_item() > 0
                      && SQLcount ( "name", "character", "WHERE location=2" ) < 6)
         {
            //Opponent::compile_ranks();
            Window::refresh_all();
            answer2 = Window::show_all();

            if ( answer2 != -1 )
            {
               tmpcharacter.SQLselect ( answer2 );
               tmpcharacter.location ( Character_LOCATION_PARTY );
               tmpcharacter.position ( clock() );
               tmpcharacter.rank ( Party_NONE );
               tmpcharacter.FKparty ( party.primary_key() );
               tmpcharacter.SQLupdate();
               lst_character.remove_selected_item();
            }
         }

      }
      else if ( answer == City_PARTYMENU_REMOVE )
      {
         List lst_remove ("Select a character to REMOVE", 6);
         WinList wlst_remove ( lst_remove, 20, 50);
         wlst_remove.hide();

         answer2 = 0;

         lst_remove.add_query ( "name", "character", "WHERE location=2");

         while ( answer2  != -1 && lst_remove.nb_item() > 0)
         {
            wlst_remove.unhide();
            //Opponent::compile_ranks();
            Window::refresh_all();
            answer2 = Window::show_all();

            if ( answer2 != -1 )
            {
               tmpcharacter.SQLselect ( answer2 );
               tmpcharacter.location ( Character_LOCATION_RESERVE );
               tmpcharacter.position ( 0 );
               tmpcharacter.FKparty ( -1 );
               tmpcharacter.SQLupdate();
               lst_remove.remove_selected_item();
            }
         }
      }
      else if ( answer == City_PARTYMENU_REMOVEALL )
         SQLexec ("UPDATE character SET location=1, position=0, FKparty=-1 WHERE location=2");


   }

   //return ( ret_value );
   Character::force_recompile_ranks();
   //Opponent::compile_ranks();
}


void City::show_subservice_create_character ( void )
{
   Character tmpchar;
   int answer = 0;
   int i = 0;
   int nb_level = 1;
   int nb_point = 1;
   int bonuspoint = 1;

   Window::instruction ( 320, 284 );
   Window::cascade_pause();

   WinData<Character> wdat_newchar ( WDatProc_new_character,
      tmpchar, WDatProc_POSITION_NEW_CHARACTER );

   //---------- name entry ----------
   //Window::instruction ( 200, 460, Window_INSTRUCTION_INPUT );
   WinInput winp_name("Enter the name of your character", Character_NAME_STRLEN -1, 10, 50);

   Window::show_all();

   tmpchar.name ( winp_name.get_string() );
   winp_name.hide();



   //---------- race----------
   //Window::instruction ( 200, 460, Window_INSTRUCTION_SELECT +
     // Window_INSTRUCTION_MENU ); //uncomment if required
   List lst_race ("Select the race of your character", 8, true);

   lst_race.add_query ("name", "race", "" );

   WinData<Character> wdat_race ( WDatProc_race, lst_race.answer(0), WDatProc_POSITION_RACE );

   WinList wlst_race ( lst_race, 10, 50, true );

   while ( lst_race.selected() == false)
   {
      Window::refresh_all();
      answer = Window::show_all();
      wdat_race.key ( lst_race.answer());
   }

   tmpchar.FKrace (answer);
   tmpchar.init_race_attribute();
   wlst_race.hide();
   wdat_race.hide();

   //---------- Class ----------
   List lst_class ("Select the class of your character", 12, true);

   lst_class.add_query ("name", "class", "");

   WinData<Character> wdat_class ( WDatProc_cclass, lst_class.answer(0), WDatProc_POSITION_CCLASS );

   WinList wlst_class ( lst_class, 10, 50, true );

   while ( lst_class.selected() == false)
   {
      Window::refresh_all();
      answer = Window::show_all();
      wdat_class.key ( lst_class.answer());

   }

   tmpchar.FKcclass (answer);

   wlst_class.hide();
   wdat_class.hide();

   //---------- Portrait ----------
   ImageList imglst_portrait ("Select your character portrait", 4, 3);

   imglst_portrait.add_datref ( datref_portrait, 1 );

   WinImageList wimglst_portrait ( imglst_portrait, 20, 20 );

   answer = -1;

   while ( answer == -1)
   {
      answer = Window::show_all();
   }

   tmpchar.portraitid ( answer );

   wimglst_portrait.hide();

   //---------- Attribute ----------

   Distributor dst_attribute;

   dst_attribute.title ("Distribute your character points");
   dst_attribute.header ( "Attributes" );
   dst_attribute.total ( "Character Points" );
   //dst_attribute.nb_digit (4);

   dst_attribute.add_item ("STRength", tmpchar.attribute(0), tmpchar.attribute(0) + 10 );
   dst_attribute.add_item ("DEXterity", tmpchar.attribute(1), tmpchar.attribute(1) + 10 );
   dst_attribute.add_item ("ENDurence", tmpchar.attribute(2), tmpchar.attribute(2) + 10 );
   dst_attribute.add_item ("INTelligence", tmpchar.attribute(3), tmpchar.attribute(3) + 10 );
   dst_attribute.add_item ("CUNning", tmpchar.attribute(4), tmpchar.attribute(4) + 10 );
   dst_attribute.add_item ("WILpower", tmpchar.attribute(5), tmpchar.attribute(5) + 10 );

   switch (config.get ( Config_CHARACTER_POINTS ))
   {
      case Config_DIF_LOW:
         nb_point = 36;
      break;
      case Config_DIF_NORMAL:
         nb_point = 24;
      break;
      case Config_DIF_HIGH:
         nb_point = 12;
      break;
   }

   switch (config.get ( Config_STARTING_LEVEL ))
   {
      case Config_DIF_LOW:
         nb_level = 5;
      break;
      case Config_DIF_NORMAL:
         nb_level = 3;
      break;
      case Config_DIF_HIGH:
         nb_level = 1;
      break;
   }

   bonuspoint =  nb_level / Character_LEVELUP_ATTRIBUTE_DIVIDER
                [ config.get(Config_LEVELUP_ATTRIBUTE) ];

   //printf ("Level=%d, divider=%d, bonuspoints=%d\n", nb_level, Character_LEVELUP_ATTRIBUTE_DIVIDER
   //             [ config.get(Config_LEVELUP_ATTRIBUTE) ], bonuspoint);
   nb_point += bonuspoint;

   dst_attribute.total_value ( nb_point );

   WinData<Character> wdat_attribute ( WDatProc_attribute_info, 0, WDatProc_POSITION_ATTRIBUTE_INFO );

   WinDistributor wdst_attribute ( dst_attribute, 10, 50 );

   Window::refresh_all();
   Window::show_all();

   wdst_attribute.hide();
   wdat_attribute.hide();

   tmpchar.attribute( Character_STR, dst_attribute.item_value(Character_STR) );
   tmpchar.attribute( Character_DEX, dst_attribute.item_value(Character_DEX) );
   tmpchar.attribute( Character_END, dst_attribute.item_value(Character_END) );
   tmpchar.attribute( Character_INT, dst_attribute.item_value(Character_INT) );
   tmpchar.attribute( Character_CUN, dst_attribute.item_value(Character_CUN) );
   tmpchar.attribute( Character_WIL, dst_attribute.item_value(Character_WIL) );

   //---------- ----------

   //---------- ----------

   //---------- other ----------

   /*switch (config.get ( Config_STARTING_LEVEL ))
   {
      case Config_DIF_LOW:
         nb_level = 5;
      break;
      case Config_DIF_NORMAL:
         nb_level = 3;
      break;
      case Config_DIF_HIGH:
         nb_level = 1;
      break;
   }*/

   for ( i = 0; i < nb_level; i++)
      tmpchar.raise_level();

   tmpchar.finalize_newchar_stats();



   WinQuestion wqst_confirmation
      ( "Character creation complete.\nDo you want to keep this character ?", 200, 50 );

   Window::refresh_all();
   answer = Window::show_all();

   if ( answer == WinQuestion_ANSWER_YES)
   {
      tmpchar.SQLinsert();
      system_log.writef ("City: New character %s has been created", tmpchar.name() );
   }

   Window::cascade_continue();

}

/*
void City::objdat_to_strdat ( void *dataptr )
{
   dbs_City &tmpdat = *(static_cast<dbs_City*> ( dataptr ));
   int i;
   int j;

   strncpy ( tmpdat.name, p_name, 31 );
   strncpy ( tmpdat.shortname, p_shortname, 11 );
   tmpdat.nb_place = p_nb_place;
   tmpdat.xpos = p_xpos;
   tmpdat.ypos = p_ypos;
   tmpdat.type = p_type;

   for ( i = 0 ; i < City_NB_MAX_LOCATION ; i++ )
   {
      strncpy ( tmpdat.place [ i ].name, p_place [ i ].name, 21 );
      tmpdat.place [ i ].music_track = p_place [ i ].music_track;

      for ( j = 0 ; j < City_NB_MAX_SERVICE ; j++ )
         tmpdat.place [ i ].service [ j ] = p_place [ i ].service [ j ];
   }

   for ( i = 0 ; i < City_NB_MAX_WARP ; i++ )
   {
      tmpdat.warp [ i ].type = p_warp [ i ].type;
      tmpdat.warp [ i ].target = p_warp [ i ].target;
      tmpdat.warp [ i ].tag = p_warp [ i ].tag;
   }
}

void City::strdat_to_objdat ( void *dataptr )
{
   dbs_City &tmpdat = *(static_cast<dbs_City*> ( dataptr ));
   int i;
   int j;

   strcpy ( p_name, tmpdat.name );
   strcpy ( p_shortname, tmpdat.shortname );
   p_nb_place = tmpdat.nb_place;
   p_xpos = tmpdat.xpos;
   p_ypos = tmpdat.ypos;
   p_type = tmpdat.type;

   for ( i = 0 ; i < City_NB_MAX_LOCATION ; i++ )
   {
      strcpy ( p_place [ i ].name, tmpdat.place [ i ].name );
      p_place [ i ].music_track = tmpdat.place [ i ].music_track;

      for ( j = 0 ; j < City_NB_MAX_SERVICE ; j++ )
         p_place [ i ].service [ j ] = tmpdat.place [ i ].service [ j ];
   }

   for ( i = 0 ; i < City_NB_MAX_WARP ; i++ )
   {
      p_warp [ i ].type = tmpdat.warp [ i ].type;
      p_warp [ i ].target = tmpdat.warp [ i ].target;
      p_warp [ i ].tag = tmpdat.warp [ i ].tag;
   }

}

void City::child_DBremove ( void )
{
// this function does nothing
}

void City::alternate_strdat_to_objdat ( int datatypeID, void *dataptr )
{
   //note : only 1 alternate loading so datatype is not considered

   dbs_ADV_City_data &tmpdat = *(static_cast<dbs_ADV_City_data*> ( dataptr ));
   int i;
   int j;
   int placeindex [ City_NB_MAX_LOCATION ];
   int tmpindex;

   strcpy ( p_name, tmpdat.name );
   strcpy ( p_shortname, tmpdat.shortname );
   p_nb_place = tmpdat.nb_place;
   p_xpos = tmpdat.xpos;
   p_ypos = tmpdat.ypos;
   p_type = tmpdat.type;

   for ( i = 0 ; i < City_NB_MAX_LOCATION ; i++ )
   {
      strcpy ( p_place [ i ].name, tmpdat.place [ i ].name );
      p_place [ i ].music_track = tmpdat.place [ i ].music_track;
   }

   for ( i = 0 ; i < City_NB_MAX_WARP ; i++ )
   {
      p_warp [ i ].type = tmpdat.warp [ i ].type;
      p_warp [ i ].target = tmpdat.warp [ i ].target;
//      p_warp [ i ].tag = tmpdat.warp [ i ].tag;
   }

   for ( i = 0 ; i < City_NB_MAX_LOCATION ; i++ )
      placeindex [ i ] = 0;

   for ( i = 0 ; i < City_NB_MAX_LOCATION ; i++ )
      for ( j = 0 ; j < City_NB_MAX_SERVICE ; j++ )
         p_place [ i ].service [ j ] = -1;

   for ( i = 0 ; i < City_NB_SERVICE ; i++ )
   {
      tmpindex = tmpdat.service_location [ i ];
      if ( tmpindex != -1 )
      {
         p_place [ tmpindex ].service [ placeindex [ tmpindex ] ] = i;
         placeindex [ tmpindex ]++;
      }
   }

}*/

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

City city;

const char STR_CTY_BUYTYPE [] [ 81 ] =
{
   {"Here are the Weapons I have"},
   {"Here are the Shields I have"},
   {"Here are the Armors I have"},
   {"Here are the Accessories I have"},
   {"Here are the Expandables I have"},
};

/*const char STR_CTY_WARPACTION [] [ 11 ] =
{
   {"Ride to"},
   {"Sail to"},
   {"Fly to"},
   {"Warp to"},
   {"Explore"},
};*/

const char SERVICE_NAME [ City_NB_SERVICE ] [ 26 ] =
{
   { "Rest                     " }, // City_SERVICE_REST
   { "Level Check              " }, // City_SERVICE_LEVELUP
   { "Home             [Closed]" }, // City_SERVICE_HOME
   { "Hire Mercenary   [Closed]" }, // City_SERVICE_HIRE
   { "Quit Game                " }, // City_SERVICE_QUIT
   { "Buy                      " }, // City_SERVICE_BUY
   { "Sell                     " }, // City_SERVICE_SELL
   { "Forge Item       [Closed]" }, // City_SERVICE_FORGE
   { "Enchant Item     [Closed]" }, // City_SERVICE_ENCHANT
   { "Identify Item    [Closed]" }, // City_SERVICE_IDENTIFY
   { "Cure Character           " }, // City_SERVICE_HEAL
   { "Revive Character         " }, // City_SERVICE_REVIVE
   { "Find Knowledge   [Closed]" }, // City_SERVICE_KNOWLEDGE
   { "Locksmith        [Closed]" }, // City_SERVICE_KEY
   { "Uncurse          [Closed]" }, // City_SERVICE_UNCURSE
   { "Manage Characters        " }, // City_SERVICE_CHARACTER
   { "Manage Party             " }  // CIty_SERVICE_PARTY
};




