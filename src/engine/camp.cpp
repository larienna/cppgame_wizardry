/***************************************************************************/
/*                                                                         */
/*                             C A M P . C P P                             */
/*                            Class Source Code                            */
/*                                                                         */
/*     Content : Class Camp                                                */
/*     Programmer : Eric PIetrocupo                                        */
/*     Stating Date : May 10th, 2002                                       */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h> //<tmpinc>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

//#include <draw.h> //<tmpinc>
#include <wdatproc.h>




/*
//#include <time.h>
#include <allegro.h> // try to remove
#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h> // to remove
//#include <init.h>
#include <menu.h>
#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>

*/

//


//
//
//#include <list.h>
/*
#include <opponent.h>
#include <charactr.h>
//#include <monster.h>
#include <party.h>

#include <game.h>

//#include <city.h>
//#include <maze.h>
//
#include <camp.h>
#include <config.h>
#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winmenu.h>
#include <winempty.h>
#include <wintitle.h>
#include <windata.h>
#include <wdatproc.h>
#include <winmessa.h>
//#include <winquest.h>
*/

/*-------------------------------------------------------------------------*/
/*-                     Constructors & Destructors                        -*/
/*-------------------------------------------------------------------------*/

Camp::Camp ( void )
{
   p_return_to_city = false;
}

Camp::~Camp ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                                 Method                                -*/
/*-------------------------------------------------------------------------*/

int Camp::start ( bool return_to_city )
{
   bool exit_camp = false;
   int answer = 0;
   Menu p_mnu_camp; // main menu of the camp screen
   int retval = System_GAME_START;

   //printf ("Camp: Start: enter function\n");

   p_return_to_city = return_to_city;

//   party.SQLselect ( party );

//   save_backup_screen ();

//printf ("camp debug: start of camp\n");

   Window::cascade_start();

   WinEmpty wemp_background;
   wemp_background.hide();

   //printf ("p_return_to_city = %d\r\n", p_return_to_city);

   if ( p_return_to_city == true )
   {
      //printf ("pass here\r\n");

      wemp_background.unhide();

   }


   WinTitle wttl_title ("Party Camp", WinTitle_X, WinTitle_Y, ~p_return_to_city);
//   p_mnu_camp.title ("Camp");

   WinData<int> wdat_corner_info ( WDatProc_character_gold, party.primary_key(),
                                  WDatProc_POSITION_PARTY_GOLD, ~p_return_to_city );


   p_mnu_camp.add_item (Camp_MENU_CHARACTER, "Character");
   p_mnu_camp.add_item (Camp_MENU_INVENTORY, "Inventory" );
   p_mnu_camp.add_item (Camp_MENU_REORDER, "Reorder" );
   p_mnu_camp.add_item (Camp_MENU_FORMATION, "Formation" );

   if ( p_return_to_city == false)
      p_mnu_camp.add_item (Camp_MENU_SEARCH, "Search" );


   p_mnu_camp.add_item (Camp_MENU_DIARY, "Diary", true );
   p_mnu_camp.add_item (Camp_MENU_OPTION, "Options");
   p_mnu_camp.add_item (Camp_MENU_SYSTEMLOG, "System Log" );

   if ( p_return_to_city == false)
      p_mnu_camp.add_item (Camp_MENU_MAP, "Map" );

   if ( p_return_to_city == false)
      p_mnu_camp.add_item (Camp_MENU_QUIT, "Quit Party");

   p_mnu_camp.add_item (Camp_MENU_CANCEL, "Return");

         //---------------------------------------------


   /*
#define Event_TRIGGER_SEARCH_ROOM         7
#define Event_TRIGGER_SEARCH_NORTH        8
#define Event_TRIGGER_SEARCH_EAST         9
#define Event_TRIGGER_SEARCH_SOUTH        10
#define Event_TRIGGER_SEARCH_WEST         11
#define Event_TRIGGER_USE_ITEM            12
*/

         //-----------------------------------------------


   WinMenu mnu_camp ( p_mnu_camp, 20, 50, false, false, true );
//   Window::instruction ( 320, 250 );

   play_music_table ( Config_MUSICTAB_CAMP );
   //play_music_track ( System_MUSIC_w3camp );
//   p_mnu_camp.unmask_all_item ();



   while ( exit_camp == false )
   {
//printf ("camp debug: main loop\n");
      //load_backup_screen ();
      if ( p_return_to_city == false )
         maze.draw_maze();
//printf ("camp debug: after draw maze\n");
//      draw_party_spell ( party );
//      draw_border_fill ( 194 , 24, 446, 236, General_COLOR_BORDER,
//         General_COLOR_FILL );
      Window::instruction ( 320, 340, Window_INSTRUCTION_MENU +
         Window_INSTRUCTION_SELECT + Window_INSTRUCTION_CANCEL );
      answer = Window::show_all();
//      answer = p_mnu_camp.show ( 200 , 30, answer );

      mnu_camp.hide();
      switch ( answer )
      {

         case Camp_MENU_CHARACTER :
            if ( show_character () == Action_RESOLVE_RETVAL_EXIT)
            {

               //printf ("Debug: Camp: start: I have triggered resolve_retval_exit\n");
               exit_camp = true; // party status have been set by action
               //party.status ( Party_STATUS_CITY);
            }
         break;

         case Camp_MENU_SEARCH :
            Window::cascade_pause();
            if ( show_search () == true ) //event has been found
            {
               exit_camp = true;
               party.status (Party_STATUS_MAZE);
            }
            Window::cascade_continue();
         break;
/*
         case Camp_MENU_SPELL :
         break;

         case Camp_MENU_MECHANICS :
         break;

         case Camp_MENU_HEAL :
         break;

         case Camp_MENU_USE_ITEM :
         break;
*/
         case Camp_MENU_INVENTORY:
            show_inventory();
         break;

         case Camp_MENU_FORMATION :
            show_formation();
         break;

         case Camp_MENU_REORDER :
            show_reorder();
         break;

         case Camp_MENU_DIARY :
            show_diary();
         break;

         case Camp_MENU_OPTION :
            Window::cascade_pause();
            config.start( true );
            Window::cascade_continue();
         break;

         case Camp_MENU_SYSTEMLOG :
            Window::cascade_pause();
            show_system_log();
            Window::cascade_continue();
         break;

         case Camp_MENU_MAP:
            handmap.display( party.position());
         break;

         case Camp_MENU_QUIT :
            exit_camp = true;
            retval = System_GAME_STOP;

            //manager.close_game();
         break;

         case Camp_MENU_CANCEL :
            exit_camp = true;
            if ( p_return_to_city == false )
               party.status (Party_STATUS_MAZE);
            else
               party.status (Party_STATUS_CITY);
            //retval = Camp_BACKTOMAZE;
         break;

         /*                     retval = City_END_GAME;
                     answer2 = -1;
*/
      }
      mnu_camp.unhide();
   }

   Window::cascade_stop();

   party.SQLupdate();
   return ( retval );
}

/*-------------------------------------------------------------------------*/
/*-                         Private Methods                               -*/
/*-------------------------------------------------------------------------*/

int Camp::show_character ( void )
{

   Menu mnu_list ("Select a Character");
   //int retval = Action_RESOLVE_RETVAL_NONE;
   //Menu mnu_action;

   int i;
   int answer;
   int answer2;
   //int answer3;
   //bool exit_list = false;
   //bool exit_charmenu = false;
   //short ystart;
   //int nb_character = party.nb_character();
   //int nb_character = 0; //??

   //int errval;
//   string tmpstr;
   //char strmsg [ 241 ];
   Character tmpchar;
   int input_retval = Action_INPUT_RETVAL_NONE;
   int resolve_retval = Action_RESOLVE_RETVAL_NONE;

   /*mnu_action.add_item (Camp_CHARMENU_EQUIP, "Equip Item" );
   //mnu_action.add_item (Camp_CHARMENU_UNEQUIP, "Unequip Item" );
   //mnu_action.add_item (Camp_CHARMENU_INSPECT, "Inspect Item" );
   //mnu_action.add_item (Camp_CHARMENU_DROP, "Drop Item" );
//   mnu_character.add_item ("Fighting Style");
   mnu_action.add_item (Camp_CHARMENU_READ, "Spell Book", true );
   mnu_action.add_item (Camp_CHARMENU_CAST, "Cast Spell", true );
//   mnu_character.add_item ("Combat Stats" );
   mnu_action.add_item (Camp_CHARMENU_CANCEL, "Return" );*/

   //ystart = 355 + 6 + 14;

 WinData<int> wdat_party_bar ( WDatProc_party_bar, party.primary_key(),
         WDatProc_POSITION_PARTY_BAR, true );
      //load_backup_screen ();
   Window::instruction ( 320, 340 );



//   debug_printf( __func__, "Before");
   mnu_list.add_query ( "name", "character", "WHERE location=2 ORDER BY position" );
//   debug_printf( __func__, "After");

   answer = 0;
   while ( answer != -1 )
   {
      //printf ("Camp: Show_character: Pass 1\n");
      WinMenu wmnu_list ( mnu_list, 20, 50);

//      draw_party_spell ( party );

      //printf ("Camp: Show_character: Pass 2\n");
      if ( p_return_to_city == false)
         blit_mazebuffer();
      answer = Window::show_all();


      if ( answer != -1 )
      {
         List lst_action ( "", 6 );
         bool maskedcmd = false;
         //printf ("Camp: Show_character: Pass 3\n");
         // might need to pause cascasing.
         Window::cascade_pause();

         tmpchar.SQLselect ( answer );
         WinData<Character> wdat_character
               ( WDatProc_character, tmpchar, WDatProc_POSITION_CHARACTER );

         //lst_action.add_command_list ( tmpchar, CmdProc_AVAILABLE_CAMP );
         //int nb_cmd_added = 0;

         int tmpcondition = tmpchar.compile_condition();

         for ( i = 0; i < CmdProc_COMMANDLIST_SIZE ; i++ )
         {
            if ( tmpchar.check_command ( i , CmdProc_AVAILABLE_CAMP ) == true )
            {
               if ( tmpchar.body() != Character_BODY_ALIVE && CmdProc_COMMANDLIST [ i ] . usablebydead == false )
                  maskedcmd = true;
               else
                  maskedcmd = false;

               if ( ( tmpcondition & ActiveEffect_CONDITION_DISACTION ) > 0 )
                  maskedcmd = true;

              /* if ( ( tmpcondition & ActiveEffect_CONDITION_RNDACTION ) > 0 )
                  maskcmd = true;

                if ( ( tmpcondition & ActiveEffect_CONDITION_RNDFRIEND ) > 0 )
                  maskcmd = true;

               if ( ( tmpcondition & ActiveEffect_CONDITION_RNDINSANE ) > 0 )
                  maskcmd = true;
*/
               if ( CmdProc_COMMANDLIST [ i ] . requirement == CmdProc_REQUIREMENT_SPELL
                   && ( tmpcondition & ActiveEffect_CONDITION_DISSPELL ) > 0)
                   maskedcmd = true;

               if ( CmdProc_COMMANDLIST [ i ] . requirement == CmdProc_REQUIREMENT_SKILL
                   && ( tmpcondition & ActiveEffect_CONDITION_DISSKILL ) > 0)
                   maskedcmd = true;

               if ( i == 18 ) // active affect command usable at all time, not a real command
                  maskedcmd = false;

               //if ( i == 63 ) // check of rthe "return" command
               //   maskedcmd = false; // the return command is always shown

               lst_action.add_item ( i , CmdProc_COMMANDLIST [ i ] . name, maskedcmd );
               //nb_cmd_added++;
            }
         }

         answer2 = 0;
         input_retval = Action_INPUT_RETVAL_NONE;
         resolve_retval = Action_RESOLVE_RETVAL_NONE;


         WinList wlst_action ( lst_action, 444, 350 );

         //printf ( "debug: camp: Before command loop\n");
         while ( answer2 != -1 && input_retval != Action_INPUT_RETVAL_RETURN && resolve_retval != Action_RESOLVE_RETVAL_EXIT)
         {
            //printf ("Camp: Show_character: Pass 4\n");
            //mnu_character.unmask_all_item();

            wlst_action.unhide();

            Window::instruction ( 320, 460 );

            answer2 = Window::show_all();
            wlst_action.hide();
            Action tmpaction ( tmpchar );

            //printf ( "debug: camp: before action input. Action ID = %d\n", answer2 );
            input_retval = tmpaction.input ( answer2 );

            if ( input_retval != Action_INPUT_RETVAL_CANCEL )
            {
               //printf ( "debug: camp: before resolve\n");
               resolve_retval = tmpaction.resolve ();

            }

            tmpchar.refresh();

            wdat_character.refresh();



         }


         if ( resolve_retval == Action_RESOLVE_RETVAL_EXIT )
            answer = -1;

         wdat_party_bar.refresh();


         //printf ("Camp: Show_character: Pass 10\n");
         Window::cascade_continue();

      }
   }

   return resolve_retval;
}

/*void Camp::show_equip_item ( int character_key )
{



}*/

/*int Camp::show_select_inventory ( Character &tmpchar )
{
   int answer = 0;
   int i;
   int nb_item = 0;
//   Character tmpcharacter;
//   Item *tmpitem;
//   Character tmpcharacter;

//   tmpcharacter.SQLselect ( party.character ( characterID ) );

   Menu mnu_inventory;

   //nb_item = tmpchar.nb_inventory();

   if ( nb_item > 0 )
   {
      for ( i = 0 ; i < nb_item ; i++ )
         mnu_inventory.add_item (i, "");

      Window::instruction ( 320, 460, Window_INSTRUCTION_MENU +
         Window_INSTRUCTION_SELECT + Window_INSTRUCTION_CANCEL );

      Window::refresh_all();
      Window::draw_all();
//      draw_character ( tmpcharacter );


      answer = mnu_inventory.show ( 218, 58 );
   }
   else
      return ( -1 );

   return ( answer );
}

int Camp::show_select_equipment ( Character &tmpchar )
{
   int answer = 0;
   int i;
//   Item *tmpitem;
//   Character tmpcharacter;
//   bool masklist [ 8 ];
//   Character tmpcharacter;

//   tmpcharacter.SQLselect ( party.character ( characterID ) );

   Menu mnu_equipment;

//   for ( i = 0 ; i < 8 ; i++ )
//      masklist [ i ] = false;

//   if ( tmpchar.weaponA () == 0 ) masklist [ 0 ] = true;
//   if ( tmpchar.weaponB () == 0 ) masklist [ 1 ] = true;
//   if ( tmpchar.shield () == 0 ) masklist [ 2 ] = true;
//   if ( tmpchar.armor () == 0 ) masklist [ 3 ] = true;
//   if ( tmpchar.head () == 0 ) masklist [ 4 ] = true;
//   if ( tmpchar.hand () == 0 ) masklist [ 5 ] = true;
//   if ( tmpchar.feet () == 0 ) masklist [ 6 ] = true;
//   if ( tmpchar.other () == 0 ) masklist [ 7 ] = true;

   for ( i = 0 ; i < 7 ; i++ )
      mnu_equipment.add_item (i, "" );

   Window::instruction ( 320, 460, Window_INSTRUCTION_MENU +
      Window_INSTRUCTION_SELECT + Window_INSTRUCTION_CANCEL );

   Window::refresh_all();
   Window::draw_all();
//   draw_character ( tmpcharacter );

   answer = mnu_equipment.show ( 218, 250 );

   return ( answer );

}*/

/*void Camp::show_inspect_item ( Character &tmpchar, int inventoryID )
{
   Item tmpitem;

   tmpitem.SQLselect ( tmpchar.inventory ( inventoryID ) );

   WinData<Item> wdat_item ( WDatProc_inspect_item,
      tmpitem, WDatProc_POSITION_INSPECT_ITEM );

   Window::instruction ( 320, 380, Window_INSTRUCTION_CANCEL );
   Window::show_all();
   copy_buffer();
   while ((readkey() >> 8) != CANCEL_KEY );
}
*/
/*
void Camp::show_use_item ( Character &tmpchar, int inventoryID )
{
   Item tmpitem;
   string tmpstr;
   s_Item_ability tmpablt;
   unsigned int value;

   tmpitem.SQLselect ( tmpchar.inventory ( inventoryID ) );
   tmpablt = tmpitem.ability();

   if ( tmpablt.type != Item_ABILITY_NONE )
   {
      value = tmpchar.use_ability ( inventoryID );
      //?? add other interpretation : maybe common with item abilityS()
      tmpstr = ABILITY_INFO [ tmpablt.type ] . name;
      tmpstr += value;
   }
   else
      tmpstr ="No Effect !";

   WinMessage wmsg_effect ( tmpstr );

   Window::instruction ( 320, 380, Window_INSTRUCTION_CANCEL );
   Window::show_all();
   copy_buffer();
   while ((readkey() >> 8) != CANCEL_KEY );

}
  */
  /*
void Camp::show_fighting_style ( Character &tmpchar )
{
   Weapon tmpw1;
   Weapon tmpw2;
   Shield tmps;
   Menu mnu_wpnA ( "Select Fighting style A" );
   Menu mnu_wpnB ( "Select Fighting style B" );
   unsigned char fstyleval [ 4 ];
   string tmpstr;
   int listidx;
   int answer;
   int i;

   // weapon A style
   for ( i = 0 ; i < 4 ; i++ )
      fstyleval [ i ] = 0;

   listidx = 0;
   if ( tmpchar.weaponA() != 0 )
   {
      tmpw1.SQLselect ( tmpchar.weaponA() );

      fstyleval [ listidx ] = Character_FSTYLE_WEAPONA;
      mnu_wpnA.add_item ( tmpw1.name() );
      listidx++;

      if ( tmpw1.nb_hand() == Weapon_ONEHANDED )
      {
         if ( tmpchar.shield() != 0 )
         {
            tmps.SQLselect ( tmpchar.shield() );
            tmpstr = tmpw1.name() + " & ";
            tmpstr += tmps.name();
            fstyleval [ listidx ] = Character_FSTYLE_WEAPONA_SHIELD;
            mnu_wpnA.add_item ( tmpstr );
            listidx++;
         }

         if ( tmpchar.weaponB () != 0 )
         {
            tmpw2.SQLselect ( tmpchar.weaponB () );

            if ( tmpw2.nb_hand () == Weapon_ONEHANDED )
            {
               tmpstr = tmpw1.name() + " & ";
               tmpstr += tmpw2.name();
               fstyleval [ listidx ] = Character_FSTYLE_WEAPONA_WEAPONB;
               mnu_wpnA.add_item ( tmpstr );
               listidx++;
            }
         }
      }
   }
   else
   {
      if ( tmpchar.shield() != 0 )
      {
         tmps.SQLselect ( tmpchar.shield() );

         tmpstr += tmps.name();
         fstyleval [ listidx ] = Character_FSTYLE_SHIELD;
         mnu_wpnA.add_item ( tmps.name() );
         listidx++;
      }
   }

   if ( mnu_wpnA.nb_item () > 0 )
   {
      WinMenu wmnu_wpnA ( mnu_wpnA, 100, 200, true );
      answer = Window::show_all();
      tmpchar.fstyle ( 0 , fstyleval [ answer ] );
   }
   else
      tmpchar.fstyle ( 0 , Character_FSTYLE_NONE );


   // weapon B style
   for ( i = 0 ; i < 4 ; i++ )
      fstyleval [ i ] = 0;

   listidx = 0;
   if ( tmpchar.weaponB() != 0 )
   {
      tmpw1.SQLselect ( tmpchar.weaponB() );

      fstyleval [ listidx ] = Character_FSTYLE_WEAPONB;
      mnu_wpnB.add_item ( tmpw1.name() );
      listidx++;

      if ( tmpw1.nb_hand() == Weapon_ONEHANDED )
      {
         if ( tmpchar.shield() != 0 )
         {
            tmps.SQLselect ( tmpchar.shield() );
            tmpstr = tmpw1.name() + " & ";
            tmpstr += tmps.name();
            fstyleval [ listidx ] = Character_FSTYLE_WEAPONB_SHIELD;
            mnu_wpnB.add_item ( tmpstr );
            listidx++;
         }

         if ( tmpchar.weaponA () != 0 )
         {
            tmpw2.SQLselect ( tmpchar.weaponA () );

            if ( tmpw2.nb_hand () == Weapon_ONEHANDED )
            {
               tmpstr = tmpw1.name() + " & ";
               tmpstr += tmpw2.name();
               fstyleval [ listidx ] = Character_FSTYLE_WEAPONA_WEAPONB;
               mnu_wpnB.add_item ( tmpstr );
               listidx++;
            }
         }
      }
   }
   else
   {
      if ( tmpchar.shield() != 0 )
      {
         tmps.SQLselect ( tmpchar.shield() );

         tmpstr += tmps.name();
         fstyleval [ listidx ] = Character_FSTYLE_SHIELD;
         mnu_wpnB.add_item ( tmps.name() );
         listidx++;
      }
   }

   if ( mnu_wpnB.nb_item () > 0 )
   {
      WinMenu wmnu_wpnB ( mnu_wpnB, 100, 200, true );
      answer = Window::show_all();
      tmpchar.fstyle ( 1 , fstyleval [ answer ] );
   }
   else
      tmpchar.fstyle ( 1 , Character_FSTYLE_NONE );

}   */

void Camp::show_read_spell ( Character &tmpchar )
{

}

/*void Camp::show_combat_stat ( Character &tmpchar )
{
   WinData<Character> wdat_character ( WDatProc_character_stat,
                      tmpchar, WDatProc_POSITION_CHARACTER );

   Window::instruction ( 550, 460, Window_INSTRUCTION_CANCEL );
   Window::draw_all();
   copy_buffer();
   while (mainloop_readkeyboard() != CANCEL_KEY );

}*/

void Camp::show_inventory ( void )
{

   Item tmpitem;
   int answer2;
   int answer3;
   int error;

   Window::cascade_pause();

   Window::instruction (320, 282);


   List lst_item ( "Inventory List", 11, true);
   lst_item.header ("Name");


   error = tmpitem.SQLpreparef ( "WHERE loctype=%d AND lockey=%d ORDER BY type, profiency",  Item_LOCATION_PARTY, party.primary_key() );

   if ( error == SQLITE_OK )
   {
      error = tmpitem.SQLstep();

      while ( error == SQLITE_ROW)
      {
         lst_item.add_itemf ( tmpitem.primary_key(), "%-40s", tmpitem.vname());
         error = tmpitem.SQLstep();
      }
   }

   tmpitem.SQLfinalize();


   if ( lst_item.nb_item() > 0 )
   {
      WinData<int> wdat_equip ( WDatProc_character_equip, lst_item.answer(0), WDatProc_POSITION_CHARACTER_EQUIP );
      WinData<int> wdat_item ( WDatProc_inspect_item, lst_item.answer(0), WDatProc_POSITION_INSPECT_ITEM );

      WinList wlst_item ( lst_item, 20, 50 );
      answer2 = 0;

      while ( answer2 != -1  && lst_item.nb_item() > 0)
      {

         while ( lst_item.selected() == false)
         {

            Window::refresh_all();
            if ( p_return_to_city == false)
               blit_mazebuffer();

            answer2 = Window::show_all();
            wdat_equip.key ( lst_item.answer());
            wdat_item.key ( lst_item.answer());
            //printf("Answer2=%d\r\n", answer2);
         }

         if ( answer2 != -1 )
         {
            // do some stuff, probably drop items or other item stuff
            Menu mnu_inventory ("What do you want to do with this item");

            mnu_inventory.add_item ( Camp_INVMENU_USE, "Use", true);
            mnu_inventory.add_item ( Camp_INVMENU_DROP, "Drop" );
            mnu_inventory.add_item ( Camp_INVMENU_CANCEL, "Cancel");

            WinMenu wmnu_inventory ( mnu_inventory, 40, 70 );
            if ( p_return_to_city == false)
               blit_mazebuffer();
            answer3 = Window::show_all();

            switch ( answer3)
            {
               case Camp_INVMENU_DROP:
                  tmpitem.SQLselect ( lst_item.answer() );
                  tmpitem.SQLdelete ();
                  lst_item.remove_selected_item();
               // need to disable item in the list. See market
               break;
               case Camp_INVMENU_USE:
               break;
            }

            lst_item.selected( false);
         }

      }
   }
   else
   {
      WinMessage wmsg_notavailable ("Your inventory is empty");
      blit_mazebuffer();
      Window::show_all();
   }

   Window::instruction ( 320, 340);

   Window::cascade_continue();


 /* // Code from city

 int answer = 0;
   int error;
   char itemstr[11];

   Item tmpitem;






 */

}

void Camp::show_reorder ( void )
{
   char tmpstr [ 101 ];
   int answer2 = 0;
   Character tmpcharacter;



   Window::cascade_pause();

   sprintf ( tmpstr, "UPDATE character SET location=3 WHERE location=2 AND fkparty=%d", party.primary_key());
   SQLexec( tmpstr);
   //SQLerrormsg();

   List lst_character ("Select the new order of the characters?", 6 );


   sprintf ( tmpstr, "WHERE location=3 AND fkparty=%d", party.primary_key());
   lst_character.add_query ("name", "character", tmpstr );

   WinData<int> wdat_party_bar ( WDatProc_party_bar, party.primary_key(),
         WDatProc_POSITION_PARTY_BAR );

   WinList wlst_character ( lst_character, 20, 50, true);

   while ( lst_character.nb_item() > 0 )
   {
      //Opponent::compile_ranks();
      Window::refresh_all();
      if ( p_return_to_city == false)
         blit_mazebuffer();
      answer2 = Window::show_all();


      tmpcharacter.SQLselect ( answer2 );
      tmpcharacter.location ( Character_LOCATION_PARTY );
      //if ( tmpcharacter.body() == Character_BODY_ALIVE)
      tmpcharacter.position ( clock() );
      tmpcharacter.rank ( Party_NONE );
      //tmpcharacter.FKparty ( party.primary_key() );
      tmpcharacter.SQLupdate();
      lst_character.remove_selected_item();

   }

   wlst_character.hide();
   system_log.write ("Camp: Party Reordered");

   Character::force_recompile_ranks();

   Window::refresh_all();
   if ( p_return_to_city == false)
      blit_mazebuffer();
   Window::draw_all();
   copy_buffer();
   rest (1000);

   Window::cascade_continue();

   //SQLexec ("UPDATE character SET position=999999, rank=0 WHERE body>0");

   //Opponent::compile_ranks();

}

void Camp::show_formation ( void )
{
   int answer = Party_FORMATION_OFFENSIVE;

   List lst_formation ("Select a formation for your party", 3, true);

   lst_formation.add_item ( Party_FORMATION_OFFENSIVE, "Offensive (4:2)" );
   lst_formation.add_item ( Party_FORMATION_BALANCED,  "Balanced  (3:3)" );
   lst_formation.add_item ( Party_FORMATION_DEFENSIVE, "Defensive (2:4)" );

   WinData<int> wdat_party_bar ( WDatProc_party_bar, party.primary_key(),
         WDatProc_POSITION_PARTY_BAR );

   WinList wlst_fomation ( lst_formation, 20, 50, true );

   while ( lst_formation.selected() == false)
   {
      party.formation ( answer );
      Character::force_recompile_ranks();
      //Opponent::compile_ranks();
      Window::refresh_all();
      if ( p_return_to_city == false)
         blit_mazebuffer();
      answer = Window::show_all();

   }

   party.SQLupdate();

   system_log.writef ("Camp: Party Formation changed" );

   Character::force_recompile_ranks();

   //Opponent::compile_ranks();
}

void Camp::show_diary ( void )
{

}

void Camp::show_system_log ( void )
{
   List lst_syslog ( "System Log", 24);
   int i;

   //adding test data
   /*system_log.write ("allo 1");
   system_log.write ("allo 2");
   system_log.write ("allo 3");
   system_log.write ("allo 4");
   system_log.write ("allo 5");
   system_log.write ("allo 6");
   system_log.write ("allo 7");
   system_log.write ("allo 8");
   system_log.write ("allo 9");
   system_log.write ("allo 10");*/

   for ( i = 0; i < system_log.nb_entry() ; i++ )
      lst_syslog.add_item ( i, system_log.read ( i ) );

// ?? maybe relocate instructions

   Window::instruction ( 320, 40, Window_INSTRUCTION_MENU +
         Window_INSTRUCTION_CANCEL );
   WinList wlst_system_log ( lst_syslog, 0, 60 );
   if ( p_return_to_city == false)
         blit_mazebuffer();
   Window::show_all();

}

bool Camp::show_search ( void )
{
   bool searchfailed = false;
   s_Party_position tmpos;
   Event tmpevent;
   int errorsql;
   int eventID;
   bool eventodo = false;

   WinTitle wttl_searching ("Searching ...", 320, 240, true);
   blit_mazebuffer();
   Window::show_all();


   rest (1000);

   tmpos = party.position();
   eventID = mazetile [tmpos.z][tmpos.y][tmpos.x].event;

   if ( eventID != 0)
   {
      errorsql = tmpevent.SQLselect (eventID);

      if ( errorsql == SQLITE_ROW)
      {

         switch ( tmpevent.trigger() )
         {
            case Event_TRIGGER_SEARCH_NORTH :
//printf ("Maze: Event: trigger face north detected detected\n");
               if ( tmpos.facing == Party_FACE_NORTH )
               {
//printf ("Maze: Event: event todo activated\n");
                  eventodo = true;
               }
            break;

            case Event_TRIGGER_SEARCH_EAST :
               if ( tmpos.facing == Party_FACE_EAST )
                  eventodo = true;
            break;

            case Event_TRIGGER_SEARCH_SOUTH :
               if ( tmpos.facing == Party_FACE_SOUTH )
                  eventodo = true;
            break;

            case Event_TRIGGER_SEARCH_WEST :
               if ( tmpos.facing == Party_FACE_WEST )
                  eventodo = true;
            break;
            case Event_TRIGGER_SEARCH_ROOM :
               eventodo = true;
            break;
         }

         if ( eventodo == true )
         {
//printf ("Maze: Event: Facing event started\n");
            tmpevent.start ();
         }
         else
            searchfailed = true;

      }
      else
      {
         WinMessage wmsg_failure ("ADVENTURE ERROR: I cannot find the event in the database.",
                               WinMessage_X, WinMessage_Y, true);
         blit_mazebuffer();
         Window::show_all();
      }

   }
   else
      searchfailed = true;

   wttl_searching.hide();

   if (searchfailed == true)
   {
      WinMessage wmsg_failure ("You searched the front wall and the content of the room\nUnfortunately, you could not find anything interesting.",
                               WinMessage_X, WinMessage_Y, true);
      blit_mazebuffer();
      Window::show_all();
   }
   else
      return (true); // exit camp

   return (false);
}

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

Camp camp;

