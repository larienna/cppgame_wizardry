/***************************************************************************/
/*                                                                         */
/*                             C M D P R O C . H                           */
/*                             Procedures                                  */
/*                                                                         */
/*     Content : Command Procedures                                        */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : September 30th, 2012                                */
/*                                                                         */
/*          This file contains the procedures required to resolve the      */
/*     character and monster commands. THere is a procedure for the        */
/*     assignment and the resolution.                                      */
/*                                                                         */
/***************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

#include <wdatproc.h>


/* Reminder
// --- target type

#define Action_TARGET_TYPE_ONEENEMY   0 // target_ID = table monster ID
#define Action_TARGET_TYPE_GROUPENEMY 1 // target_ID = 1 = front, 2 = back
#define Action_TARGET_TYPE_ALLENEMY   2 // target_ID is ignored
#define Action_TARGET_TYPE_ONEFRIEND  3 // target_ID = PK of character
#define Action_TARGET_TYPE_GROUPFRIEND 4 // target_ID = 1 = front, 2 = back
#define Action_TARGET_TYPE_ALLFRIEND  5 // target_ID is ignored.
#define Action_TARGET_TYPE_EVERYBODY  6 // target_ID is ignored.
// note: some actions will assume certain types because there are no other possibilities.

// --- actor type ---

#define Action_ACTOR_TYPE_UNKNOWN   0 // has no effect when resolved.
#define Action_ACTOR_TYPE_CHARACTER 1
#define Action_ACTOR_TYPE_ENNEMY    2

// --- input return value ---

#define Action_INPUT_RETVAL_NONE     0
#define Action_INPUT_RETVAL_PREVIOUS 1
#define Action_INPUT_RETVAL_RETURN   2
#define Action_INPUT_RETVAL_CANCEL   3
#define Action_INPUT_RETVAL_RUN      4

#define Action_RESOLVE_RETVAL_NONE   0
*/

/*-------------------------------------------------------------------------*/
/*-                            Prototypes                                 -*/
/*-------------------------------------------------------------------------*/

int CmdProc_input_zap ( Action &action )
{
   int answer;
   int retval = Action_INPUT_RETVAL_NONE;
   /*Character tmpchar;
   int tmpcondition;

   tmpchar.SQLselect ( action.actorID() );

   tmpcondition = tmpchar.compile_condition();

   if ( ( tmpcondition & ActiveEffect_CONDITION_DISTARGET ) == 0 )
   {*/

      Menu mnu_monster ("Choose a target");

      mnu_monster.add_query ("name","monster","WHERE body=0 ORDER BY position");

      WinMenu wmnu_monster ( mnu_monster, 0, 156, false, false, true );

      blit_mazebuffer();
      combat.draw_enemy();
      answer = Window::show_all();

      if ( answer != -1)
      {
         action.targetID ( answer );
         action.target_type ( Action_TARGET_ONEENEMY );
      //action.SQLupdate();
      //printf ("Cmdproc: input targetID=%d\n", answer);
      }
      else
      {

         retval =  Action_INPUT_RETVAL_CANCEL;

      }
   //}
   return ( retval );
}
int CmdProc_AIinput_zap ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
int CmdProc_resolve_zap ( Action &action, Opponent *actor, Opponent *target  )
{
   //int retval;
   //static char tmpactorname [ 40 ];
   //static char tmptargetname [ 40 ];


   if (target->body() == Character_BODY_ALIVE )
   {

      //float tmpfirst = clock();
      //float tmpnow;
      //actor.displayname ( &tmpactorname );
      //actor.displayname ( &tmptargetname );

      system_log.writef ("%s Zap %s", actor->displayname(), target->displayname() );
         //tmpnow = clock();
         //tmpnow = ( tmpnow - tmpfirst )  / CLOCKS_PER_SEC;
         //printf ("Resolve Syslog Time: %f\n", tmpnow);

      target->body (Character_BODY_DEAD);
         //tmpnow = clock();
         //tmpnow = ( tmpnow - tmpfirst )  / CLOCKS_PER_SEC;
         //printf ("Resolve Set body Time: %f\n", tmpnow);

      system_log.writef ("%s is destroyed", target->displayname() );
         //tmpnow = clock();
         //tmpnow = ( tmpnow - tmpfirst )  / CLOCKS_PER_SEC;
         //printf ("Resolve Syslog2 Time: %f\n", tmpnow);

      target->SQLupdate();
         //tmpnow = clock();
         //tmpnow = ( tmpnow - tmpfirst )  / CLOCKS_PER_SEC;
         //printf ("Resolve Update Time: %f\n", tmpnow);


   }
   else
   {
      system_log.writef ("%s's target is out", actor->displayname() );
   }


   return ( Action_RESOLVE_RETVAL_NONE );
}

int CmdProc_input_fight ( Action &action )
{
   Character tmpchar;
   Item tmpitem;
   Monster tmpmonster;
   //int nb_monster = 0;
   int max_range = 0;
   int max_rank = 0;
   //char tmpstr [ 100 ];
   int answer;
   int retval;
   int inputretval = Action_INPUT_RETVAL_NONE;

   tmpchar.SQLselect ( action.actorID() );


   /*int tmpcondition;

   tmpcondition = tmpchar.compile_condition();
*/
// get attack range of the character.
   retval = tmpitem.SQLpreparef( "WHERE loctype=%d AND lockey=%d AND type=%d", Item_LOCATION_CHARACTER, action.actorID(), Item_TYPE_WEAPON );

   if ( retval == SQLITE_OK)
   {
      retval = tmpitem.SQLstep();

      while ( retval == SQLITE_ROW)
      {
         if ( tmpitem.range() > max_range)
            max_range = tmpitem.range();

         retval = tmpitem.SQLstep();
      }
   }
   else
      tmpitem.SQLerrormsg();

   tmpitem.SQLfinalize();

   // ----- calculate maximum range -----

   switch ( tmpchar.rank())
   {
      case Party_FRONT:
         max_rank = max_range;
      break;
      case Party_BACK:
         max_rank = -1 + max_range;
      break;
   }

   // ----- Select targettable monster -----


   Menu mnu_monster ("Select a target in range");

   retval = tmpmonster.SQLpreparef ("WHERE rank <= %d AND body=0 ORDER BY position", max_rank );

   if ( retval == SQLITE_OK)
   {
      retval = tmpmonster.SQLstep();

      while ( retval == SQLITE_ROW)
      {
         mnu_monster.add_item ( tmpmonster.primary_key(), tmpmonster.displayname() );
         retval = tmpmonster.SQLstep();
      }
   }
   else
      tmpmonster.SQLerrormsg();

   tmpmonster.SQLfinalize();

   //need to do manually if want fakename
   //mnu_monster.add_query ( "name", "monster", tmpstr );

   WinMenu wmnu_monster ( mnu_monster, 0, 156, false, false, true );

   blit_mazebuffer();
   combat.draw_enemy();
   answer = Window::show_all();

   if ( answer != -1)
   {
      action.targetID ( answer );
      action.target_type ( Action_TARGET_ONEENEMY );
   }
   else
      inputretval = Action_INPUT_RETVAL_CANCEL;

   return ( inputretval );

}
int CmdProc_AIinput_fight ( Action &action )
{
   Monster tmpmonster;
   Character tmpchar;
   int max_range = 0;
   int max_rank = 0;
   int retval;
   int return_value = Action_INPUT_RETVAL_NONE;

   tmpmonster.SQLselect ( action.actorID());
   max_range = tmpmonster.range();

   switch ( tmpmonster.rank())
   {
      case Party_FRONT:
         max_rank = max_range;
      break;
      case Party_BACK:
         max_rank = -1 + max_range;
      break;
   }

   retval = tmpchar.SQLpreparef ( "WHERE rank <= %d AND body=0 AND location=%d ORDER BY random()", max_rank, Character_LOCATION_PARTY);
   //tmpchar.SQLerrormsg();

   if ( retval == SQLITE_OK )
   {
      retval = tmpchar.SQLstep();
      //printf ("tmpchar: %d\n", tmpchar.name());

      if ( retval == SQLITE_ROW )
      {
         action.targetID ( tmpchar.primary_key() );
         action.target_type ( Action_TARGET_ONECHARACTER );
      }
      else
         return_value = Action_INPUT_RETVAL_CANCEL;
   }
   else
   {
      return_value = Action_INPUT_RETVAL_CANCEL;

      printf ("CmdProc: AIinput_fight: Error: Target character could not be selected\n");
   }
   tmpchar.SQLfinalize ();

   return ( return_value );
}

int CmdProc_resolve_fight ( Action &action, Opponent *actor, Opponent *target  )
{
   int nb_attack = 0;
   int i;
   //char tmpstr;
   int attackdamage = 0;
   int totaldamage = 0;
   int finalattackdamage = 0;
   int attackroll = 0;
   int dr_roll = 0;
   int nb_attack_hit;
   int multihitbonus = 0;
   bool attacksuccessful = false;
   int resolve_value = Action_RESOLVE_RETVAL_NONE;

   system_log.writef ("%s attacks %s", actor->displayname(), target->displayname() );

   if (target->body() == Character_BODY_ALIVE )
   {

//      actor->compile_stat();
//      target->compile_stat();

      // ----- calculate number of attacks ----

      if ( actor->flatstat( FLAT_ATTDIV ) > 0 )
         nb_attack = actor->level() / actor->flatstat( FLAT_ATTDIV ) + 1;

      //nb_attack++; // Minimum of 1 attack per round.


      int targetresistance = target ->resistance ();
      int targetweakness = target->weakness();

      nb_attack_hit = 0;
      for ( i = 0; i < nb_attack; i++)
      {
         attackdamage = 0;
         attackroll = 0;
         dr_roll = 0;

         // ----- Make combat rolls for each attack -----

         attacksuccessful = true;

         int tmpcondition_target = target->compile_condition();
         int tmpcondition_actor = actor->compile_condition();

        // if ( ( tmpcondition_target & ActiveEffect_CONDITION_FAILDODGE) == 0
         //   || ( tmpcondition_actor & ActiveEffect_CONDITION_FAILATTACK ) > 0)
         //{

            if ( (actor->flatstat ( FLAT_DEFENSE ) & DEFENSE_ACTIVE_DEFENSE) > 0
               ||  (actor->flatstat ( FLAT_DEFENSE ) & DEFENSE_HALF_ACTIVE_DEFENSE) > 0)
            {
               int ad = target->d20stat ( D20STAT_AD );
               attackroll = dice ( 20 );

               if ( ( actor->flatstat ( FLAT_DEFENSE ) & DEFENSE_HALF_ACTIVE_DEFENSE) > 0 )
                  ad /= 2;

               if ( ( actor->flatstat( FLAT_CS) + attackroll + multihitbonus ) <= ad + 10 )
                  attacksuccessful = false;

               if ( ( tmpcondition_target & ActiveEffect_CONDITION_FAILDODGE) > 0 )
                  attacksuccessful = true;

               if ( attackroll <=2 ) //automatic Failure.
                  attacksuccessful = false;

               if ( ( tmpcondition_actor & ActiveEffect_CONDITION_FAILATTACK ) > 0 )
                  attacksuccessful = false;

               if ( attackroll >= 19 ) // roll of 19+ automatically hit, so always 10% chance to hit.
                  attacksuccessful = true;


               if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
               {
                  if ( attacksuccessful == true )
                     system_log.writef ("Attackroll: Dice=%d + CS %d + MH %d <> AD %d + 10 -> SUCCESS"
                                  , attackroll, actor->flatstat( FLAT_CS), multihitbonus, ad );
                  else
                     system_log.writef ("Attackroll: Dice=%d + CS %d + MH %d <> AD %d + 10 -> FAIL",
                                  attackroll, actor->flatstat( FLAT_CS), multihitbonus, target->d20stat ( D20STAT_AD) );

               }
            }

         //else
           // attacksuccessful = false;

         if ( (actor->flatstat ( FLAT_DEFENSE ) & DEFENSE_MAGIC_DEFENSE) > 0
            ||  (actor->flatstat ( FLAT_DEFENSE ) & DEFENSE_HALF_MAGIC_DEFENSE) > 0)
         {
            int md = target->d20stat ( D20STAT_MD );
            int md_roll = dice ( 20 );

            if ( ( actor->flatstat ( FLAT_DEFENSE ) & DEFENSE_HALF_MAGIC_DEFENSE) > 0 )
               md /= 2;

            if ( md_roll <  md )
               attacksuccessful = false;

         }


         if ( attacksuccessful == true )
         {

            nb_attack_hit++;

            // ----- Calculate damage of attack -----

            attackdamage = roll_xdypz ( 1 , actor->xyzstat ( XYZSTAT_DMG, XYZMOD_Y), actor->xyzstat ( XYZSTAT_DMG ) );

            if ( attackroll == 20 ) // critical hit
               attackdamage *= 2;

            // ----- Apply Damage resistance -----

            int dr = 0;
            dr_roll = dice (20);

            finalattackdamage = attackdamage;

            // check reduce damage condition
            int tmpcondition = actor->compile_condition();

            if ( ( tmpcondition & ActiveEffect_CONDITION_REDDAMAGE) > 0 )
               totaldamage /= 4;

            if ( totaldamage <= 0)
               totaldamage = 1;

            // check resistance / 8 damage


            if ( ( actor->flatstat ( FLAT_ATTACKELEMENT ) & targetresistance ) > 0 )
            {
               // note: resistance will counter a weakness.

               switch ( config.get ( Config_WEAKRESIST_STRENGTH) )
               {
                  case Config_DIF_LOW: totaldamage /= 4; break;
                  case Config_DIF_NORMAL: totaldamage /= 8; break;
                  default: totaldamage /= 4;
               }

               if ( totaldamage <= 0)
                  totaldamage = 1;

               //sprintf  ( tmpstr, "%s RESISTANCE(dmg /8) -> %d", tmpstr, totaldamage);
            }
            else
            {

               // check weakness *4 DMG

               if ( ( actor->flatstat ( FLAT_ATTACKELEMENT ) & targetweakness ) > 0)
               {
                  switch ( config.get ( Config_WEAKRESIST_STRENGTH) )
                  {
                  case Config_DIF_LOW: totaldamage *= 2; break;
                  case Config_DIF_NORMAL: totaldamage *= 4; break;
                  default: totaldamage *= 4;
                  }


                  //sprintf  ( tmpstr, "%s WEAKNESS(dmg x4) -> %d", tmpstr, totaldamage);
               }
            }


            if ( (actor->flatstat ( FLAT_DEFENSE ) & DEFENSE_DAMAGE_RESISTANCE) > 0
               ||  (actor->flatstat ( FLAT_DEFENSE ) & DEFENSE_HALF_DAMAGE_RESISTANCE) > 0)
            {
               dr = target->d20stat (D20STAT_DR);

               if ( ( target->d20stat (D20STAT_DR) & DEFENSE_HALF_DAMAGE_RESISTANCE) > 0 )
                  dr /= 2;

               if ( dr_roll <= dr || dr_roll <=2 )
                  finalattackdamage /= 2;

               if ( dr_roll == 1 ) // critical success
                  finalattackdamage /= 2;

               if ( attackdamage == 0)
                  finalattackdamage = 1;

            }

            if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
               system_log.writef ("Atk Dmg: 1 d%d + %d = %d, Dmg Resist.: DR=%d, roll=%d -> %d Dmg"
                                 , actor->xyzstat ( XYZSTAT_DMG, XYZMOD_Y)
                                 , actor->xyzstat ( XYZSTAT_DMG )
                                 , attackdamage
                                 , dr
                                 , dr_roll
                                 , finalattackdamage );

            // ----- Add up the damage of successfull attacks -----

            totaldamage += finalattackdamage;

            // multi hit penalty increased if attack successful
            // disabled since too strong
            multihitbonus = 0;


         }
         else
            multihitbonus += actor->flatstat ( FLAT_MULTIHIT );


      }

      if ( nb_attack_hit > 0 )
      {
         target->lose_HP ( totaldamage );
         target->SQLupdate();

         system_log.writef ( "%s made %d attacks, hit %d times for %d damage", actor->displayname(), nb_attack, nb_attack_hit, totaldamage );

         if ( target->body () != Character_BODY_ALIVE )
            system_log.writef ( "%s is killed", target->displayname() );

         ActiveEffect::trigger_disturb_expiration( target );
      }
      else
         system_log.writef ( "%s made %d attacks that missed it's target.", actor->displayname(), nb_attack );

   }
   else
   {
      //system_log.writef ("%s's target is out", actor->name() );
      resolve_value = Action_RESOLVE_RETVAL_CANCEL;
   }

   return ( resolve_value );

}
/*int CmdProc_input_parry ( Action &action )
{

}
int CmdProc_AIinput_parry ( Action &action )
{

}*/
int CmdProc_resolve_parry ( Action &action, Opponent *actor, Opponent *target  )
{
   ActiveEffect::add_effect( 200, actor );
   system_log.writef ("%s is in defense", actor->displayname() );

   return Action_RESOLVE_RETVAL_NONE;
}
int CmdProc_input_protect ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
int CmdProc_AIinput_protect ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
int CmdProc_resolve_protect ( Action &action, Opponent *actor, Opponent *target  )
{
   system_log.writef ("%s Protect (not Implemented)", actor->displayname() );

   return Action_RESOLVE_RETVAL_NONE;
}
int CmdProc_input_run ( Action &action )
{
   return ( Action_INPUT_RETVAL_RUN );
}
int CmdProc_AIinput_run ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
int CmdProc_resolve_run ( Action &action, Opponent *actor, Opponent *target  )
{
   // do nothing
   return Action_RESOLVE_RETVAL_NONE;
}
int CmdProc_input_arcane ( Action &action )
{
   //actor->compile_stat();

   return CmdProc_input_generic_spell ( action, CClass_MAGICSCHOOL_ALLARCANE );

}
int CmdProc_AIinput_arcane ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
int CmdProc_resolve_spell ( Action &action, Opponent *actor, Opponent *target  )
{

   // will call a generic spell casting function

   //printf ("Resolve spell: actor name = %s\n", actor->name() );



   //if ( ( action.action_type() & Action_TARGET_MASK_QTY) == Action_TARGET_QTY_ONE)
   //   target->compile_stat();

//   actor->compile_stat();

   int tmpcondition = actor->compile_condition();

   if ( (tmpcondition & ActiveEffect_CONDITION_DISSPELL ) > 0 )
      return Action_RESOLVE_RETVAL_CANCEL;




   Spell tmpspell;

   tmpspell.SQLselect ( action.value() );

   return tmpspell.cast ( action, actor, target );



   //system_log.writef ("%s Cast Arcane Spells (not Implemented)", actor->displayname() );

   return Action_RESOLVE_RETVAL_NONE;
}
int CmdProc_input_divine ( Action &action )
{
   //actor->compile_stat();

   return CmdProc_input_generic_spell ( action, CClass_MAGICSCHOOL_ALLDIVINE );
}

int CmdProc_AIinput_divine ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
/*int CmdProc_resolve_divine ( Action &action, Opponent *actor, Opponent *target  )
{
   system_log.writef ("%s Cast Divine Spells (not Implemented)", actor->displayname() );
}*/
int CmdProc_input_alchemy ( Action &action )
{
   //actor->compile_stat();

   return CmdProc_input_generic_spell ( action, CClass_MAGICSCHOOL_ALLALCHEMY );
}
int CmdProc_AIinput_alchemy ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
/*int CmdProc_resolve_alchemy ( Action &action, Opponent *actor, Opponent *target  )
{
   system_log.writef ("%s Cast Alchemy Spells (not Implemented)", actor->displayname() );
}*/
int CmdProc_input_psionic ( Action &action )
{
   //actor->compile_stat();

   return CmdProc_input_generic_spell ( action, CClass_MAGICSCHOOL_ALLPSIONIC );
}
int CmdProc_AIinput_psionic ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
/*int CmdProc_resolve_psionic ( Action &action, Opponent *actor, Opponent *target  )
{
   system_log.writef ("%s Cast Psionic Spells (not Implemented)", actor->displayname() );
}*/
int CmdProc_input_use_item ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
int CmdProc_AIinput_use_item ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}
int CmdProc_resolve_use_item ( Action &action, Opponent *actor, Opponent *target  )
{
   system_log.writef ("%s use item (not Implemented)", actor->displayname() );

   return Action_RESOLVE_RETVAL_NONE;
}
/*int CmdProc_input_call_help ( Action &action )
{

}
int CmdProc_AIinput_call_help ( Action &action )
{

}*/
int CmdProc_resolve_call_help ( Action &action, Opponent *actor, Opponent *target  )
{
   system_log.writef ("%s Call for help (not Implemented)", actor->displayname() );

   return Action_RESOLVE_RETVAL_NONE;
}
/*int CmdProc_input_equip ( Action &action )
{

}
int CmdProc_AIinput_equip ( Action &action )
{

}*/
int CmdProc_resolve_equip ( Action &action, Opponent *actor, Opponent *target  )
{

   int i;
   bool asked_for_item = false;
   int error;
   int answer;
   char tmpstr [100];
   Character tmpchar;
   Item tmpitem;
   int nb_hand = 0;
   bool cursed [ 9 ]; // mark which item type is cursed.
   int innerloop;
   int j;

   if ( action.actor_type() == Action_ACTOR_TYPE_CHARACTER )
   // ennemies cannot equip stuff
   // not implemented in action since equip is the only action that does that for now.
   {

      for ( i = 0; i < 8; i++)
         cursed [ i ]  = false;


      tmpchar.SQLselect ( action.actorID() );

      if ( config.get ( Config_CURSED_ITEM ) == Config_DIF_ENABLED)
         sprintf ( tmpstr, "UPDATE item SET loctype=%d, lockey=%d WHERE loctype=%d AND lockey=%d AND NOT cursed=1", Item_LOCATION_PARTY, party.primary_key(), Item_LOCATION_CHARACTER, tmpchar.primary_key() );
      else // also remove cursed items
         sprintf ( tmpstr, "UPDATE item SET loctype=%d, lockey=%d WHERE loctype=%d AND lockey=%d", Item_LOCATION_PARTY, party.primary_key(), Item_LOCATION_CHARACTER, tmpchar.primary_key() );

      SQLexec ( tmpstr );

      error = tmpitem.SQLpreparef ( "WHERE loctype=%d AND lockey=%d AND cursed=1", Item_LOCATION_CHARACTER, tmpchar.primary_key() );

      if ( error == SQLITE_OK )
      {
         error = tmpitem.SQLstep();

         while ( error == SQLITE_ROW)
         {
            cursed [ tmpitem.type()] = true;
            nb_hand += tmpitem.hand();
            error = tmpitem.SQLstep();

         }
      }

      tmpitem.SQLfinalize();

   /*SQLerrormsg();
   printf ( "PartyPK=%d, characterPK=%d\r\n", party.primary_key(), character_key);


   // tmpcode to fix databse bug
   sprintf ( tmpstr, "UPDATE item SET loctype=2, lockey=1 WHERE lockey=0" );

   SQLexec ( tmpstr );
SQLerrormsg();*/

      for ( i = 1 ; i <= Item_TYPE_EQUIPABLE; i++ )
      {

         if ( i == Item_TYPE_EXPANDABLE )
            innerloop = 4;
         else
            innerloop = 1;

         for ( j = 0; j < innerloop; j++)
         {
            sprintf ( tmpstr, "Select a %s to equip", STR_ITM_TYPE [ i ] );

            List lst_item ( tmpstr, 11, true);
            lst_item.header ("Name");

            if ( cursed [ i ] == false)
            {
               SQLerrormsg_activate();
               error = tmpitem.SQLpreparef ( "WHERE type=%d AND loctype=%d AND lockey=%d ORDER BY profiency",  i, Item_LOCATION_PARTY, party.primary_key() );
               //tmpitem.SQLerrormsg();
               SQLerrormsg_deactivate();

               if ( error == SQLITE_OK )
               {
            //printf("pass 1\r\n");
                  error = tmpitem.SQLstep();

                  while ( error == SQLITE_ROW)
                  {
                     if ( tmpchar.is_equipable ( tmpitem.primary_key() ) == true
                        && ( tmpitem.hand() + nb_hand <=2 ) )
                        lst_item.add_itemf ( tmpitem.primary_key(), "%-40s", tmpitem.vname());
            //printf ("character=%s, item=%s, is equipable=%d\r\n", tmpchar.name(), tmpitem.name(), tmpchar.is_equipable ( tmpitem.primary_key()) );
                     error = tmpitem.SQLstep();
                  }
               }
               tmpitem.SQLfinalize();

               if ( lst_item.nb_item() > 0 )
               {
                  lst_item.add_item ( -1, "None");
                  asked_for_item = true;

                  WinData<int> wdat_item ( WDatProc_inspect_item, lst_item.answer(0), WDatProc_POSITION_INSPECT_ITEM );

                  WinList wlst_item ( lst_item, 20, 50 );
                  answer = 0;

                  while ( lst_item.selected() == false)
                  {
                     if ( lst_item.answer() == -1)
                        wdat_item.hide();
                     else
                     {
                        wdat_item.key ( lst_item.answer());
                        wdat_item.unhide();
                     }
                     Window::refresh_all();
                     answer = Window::show_all();

                  //printf("Answer2=%d\r\n", answer2);
                  }

                  if ( answer != -1 )
                  {
                     //printf ( "before: loctype=%d, lockey=%d\r\n", tmpitem.location_type(), tmpitem.location_key());
                     tmpitem.SQLselect ( answer );

                     tmpitem.location ( Item_LOCATION_CHARACTER, tmpchar.primary_key()  );
                     nb_hand += tmpitem.hand();
                     tmpitem.SQLupdate();


                     if ( tmpitem.cursed() == true )
                     {
                        system_log.writef ("Camp: %s equiped %s ( CURSED )", tmpchar.name(), tmpitem.name() );

                        WinMessage wmsg_cursed ("C U R S E D");
                        Window::show_all();
                     }

                     system_log.writef ("Camp: %s equiped %s", tmpchar.name(), tmpitem.name() );


                     //tmpitem.SQLselect( tmpitem.primary_key());
                     //printf ( "before: loctype=%d, lockey=%d\r\n", tmpitem.location_type(), tmpitem.location_key());

                     lst_item.selected( false);
                  }
                  else
                     innerloop = 0;

               }
            }
            else
            {
               sprintf ( tmpstr, "%s is cursed and cannot be replaced", STR_ITM_TYPE [ i ] );
               WinMessage wmsg_error ( tmpstr );
               Window::show_all();
            }

         }


      }

      if ( asked_for_item == false)
      {
         WinMessage wmsg_notavailable ("There is no items your character can equip.");
         Window::show_all();
      }

   }

   return Action_RESOLVE_RETVAL_NONE;
}
/*int CmdProc_input_read ( Action &action )
{

}
int CmdProc_AIinput_read ( Action &action )
{

}*/
/*int CmdProc_resolve_read ( Action &action, Opponent *actor, Opponent *target )
{



}*/

int CmdProc_resolve_read_arcane ( Action &action, Opponent *actor, Opponent *target  )
{
   if ( action.actor_type() == Action_ACTOR_TYPE_CHARACTER )
   {
//      actor->compile_stat();

      CmdProc_resolve_generic_read( action, actor , CClass_MAGICSCHOOL_ALLARCANE );
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_resolve_read_divine ( Action &action, Opponent *actor, Opponent *target  )
{
   if ( action.actor_type() == Action_ACTOR_TYPE_CHARACTER )
   {
//      actor->compile_stat();

      CmdProc_resolve_generic_read( action, actor , CClass_MAGICSCHOOL_ALLDIVINE );
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_resolve_read_alchemy ( Action &action, Opponent *actor, Opponent *target  )
{
   if ( action.actor_type() == Action_ACTOR_TYPE_CHARACTER )
   {

//      actor->compile_stat();
      CmdProc_resolve_generic_read( action, actor , CClass_MAGICSCHOOL_ALLALCHEMY );
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_resolve_read_psionic ( Action &action, Opponent *actor, Opponent *target  )
{
   if ( action.actor_type() == Action_ACTOR_TYPE_CHARACTER )
   {

//      actor->compile_stat();
      CmdProc_resolve_generic_read( action, actor , CClass_MAGICSCHOOL_ALLPSIONIC );
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_resolve_read_other ( Action &action, Opponent *actor, Opponent *target  )
{
   if ( action.actor_type() == Action_ACTOR_TYPE_CHARACTER )
   {

//      actor->compile_stat();
      CmdProc_resolve_generic_read( action, actor, CClass_MAGICSCHOOL_OTHER );
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_do_nothing ( Action &action)
{
   return Action_INPUT_RETVAL_NONE;
}

int CmdProc_resolve_nothing ( Action &action, Opponent *actor, Opponent *target )
{
   system_log.writef ("%s does nothing", actor->displayname() );

   return Action_RESOLVE_RETVAL_NONE;
}


int CmdProc_input_previous ( Action &action )
{
   return ( Action_INPUT_RETVAL_PREVIOUS );
}
/*int CmdProc_AIinput_previous ( Action &action )
{

}
int CmdProc_resolve_previous ( Action &action )
{

}*/

int CmdProc_input_return ( Action &action )
{
   return ( Action_INPUT_RETVAL_RETURN );
}
/*int CmdProc_AIinput_return ( Action &action )
{

}
int CmdProc_resolve_return ( Action &action )
{

}*/

void CmdProc_resolve_generic_read ( Action &action, Opponent *actor, int school )
{
   char tmpstr [201];
   // ennemies cannot read spells
/*      char schoolstr [121];
      char levelstr [21] = "level";
      bool highlevel = false;
      int schoolcursor = 1;
      bool firstschool = true;
      int charschool = 0;

      Character tmpchar;
      tmpchar.SQLselect ( action.actorID() );
      CClass tmpclass;
      tmpclass.SQLselect ( tmpchar.FKcclass() );

      charschool = (tmpclass.magic_school () & school );
*/


      List lst_spells ( "Spell list ", 25, true);

      CmdProc_build_spell_list (action, school, tmpstr );
      //Character tmpchar;
      int answer;

 //     char tmpstr [50];
      char spellname [10] = "word";

      if ( config.get ( Config_SPELL_NAME ) == Config_DIF_DISABLED )
         strcpy ( spellname, "name");
/*
      for ( schoolcursor = 1; schoolcursor < CClass_MAGICSCHOOL_HIGHALL ; schoolcursor = schoolcursor<< 1)
      {
         //printf ( "debug: cmdproc: readspell %d, charschool=%d\n", schoolcursor, charschool );

         if ( (charschool & schoolcursor) > 0 )
         {
            if ( firstschool == true )
            {


               sprintf ( schoolstr, "school=%d ", schoolcursor );
               firstschool = false;
            }
            else
               sprintf ( schoolstr, "%s OR school=%d ", schoolstr, schoolcursor );

            if ( schoolcursor > CClass_MAGICSCHOOL_ONLYLOW)
               highlevel = true;
         }
      }

      if ( highlevel == true )
         sprintf ( levelstr, "highlevel");


      //SQLactivate_errormsg();
      sprintf ( tmpstr, "WHERE ( %s) AND %s <= %d ORDER BY effect_id, highlevel",
               schoolstr, levelstr, actor->level() );


      //SQLactivate_errormsg();
      printf ( "%s\n", tmpstr );
      */
      lst_spells.add_query( spellname ,"spell", tmpstr );

      if ( lst_spells.nb_item() > 0 )
      {
         WinData<int> wdat_spell ( WDatProc_spell_info, lst_spells.answer(0), WDatProc_POSITION_SPELL_INFO );
         WinList wlst_spells ( lst_spells, 16, 16);

         while ( answer != -1 )
         {
            //while ( lst_spells.selected() == false)
            //{
               Window::refresh_all();
               answer = Window::show_all();
               wdat_spell.key ( lst_spells.answer());
               // to add spell inspection window. Needs wdat.
            //}

         }

      }
      else
      {
         WinMessage wmsg_notavailable ("You don't know any spells of that school yet.");
         Window::show_all();
      }

//SQLdeactivate_errormsg();
   //return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_input_generic_spell ( Action &action, int school )
{

   int answer = -1;
   int retval = Action_INPUT_RETVAL_NONE;
   List lst_spell ("Choose a spell to cast", 8);
   char tmpstr [201] = ""; // Temporary query string

   /*Character tmpchar;
   int tmpcondition;

   tmpchar.SQLselect ( action.actorID() );

   tmpcondition = tmpchar.compile_condition();*/
   /* // fork according to if in battle or in camp
   #define Spell_CASTABLE_CAMP         1
#define Spell_CASTABLE_COMBAT       2
#define Spell_CASTABLE_CAMP_COMBAT  3*/

   //SQLactivate_errormsg();
   // select spell castable in combat and camp-combat
   CmdProc_build_spell_list ( action, school, tmpstr, true );

   if ( config.get ( Config_SPELL_NAME) == Config_DIF_ENABLED )
   {

      lst_spell.header ( "Word           Cost");
      lst_spell.add_query_multi ( "word, mp_cost", "spell", tmpstr, "%-10s (%2s MP)", 2);
   }
   else
   {
      lst_spell.header ("Word            Name                           Cost ");
      lst_spell.add_query_multi ( "word, name, mp_cost", "spell", tmpstr, "%-10s [%-20s] (%2s MP)", 3);
   }
   //SQLdeactivate_errormsg();
//   lst_spell.add_query ( "word", "spell", "" ); // test code

   if ( lst_spell.nb_item() > 0 )
   {
      bool translucent= false;

      if ( party.status() == Party_STATUS_COMBAT )
         translucent = true;

      WinList wlst_spell ( lst_spell, 0, 156, false, false, translucent );

      blit_mazebuffer();
      combat.draw_enemy();
      answer = Window::show_all();
   }
      else
      {
         WinMessage wmsg_notavailable ("There is no spells you can cast here.");
         Window::show_all();
      }

/*#define Action_TARGET_ONEENEMY   5 // target_ID = table monster ID
#define Action_TARGET_GROUPENEMY 6 // target_ID = 1 = front, 2 = back
#define Action_TARGET_ALLENEMY   7 // target_ID is ignored
#define Action_TARGET_ONECHARACTER  9 // target_ID = PK of character
#define Action_TARGET_GROUPCHARACTER 10 // target_ID = 1 = front, 2 = back
#define Action_TARGET_ALLCHARACTER  11 // target_ID is ignored.
#define Action_TARGET_EVERYBODY  15 // target_ID is ignored.
// note: some actions will assume certain types because there are no other possibilities.
#define Action_TARGET_SHORT_ONEENEMY      0x0015
#define Action_TARGET_SHORT_GROUPENEMY      0x0016*/

/*#define Action_TARGET_MASK_QTY          0x0003
#define Action_TARGET_MASK_FRIEND       0x0008
#define Action_TARGET_MASK_ENEMY        0x0004
#define Action_TARGET_MASK_SHORTRANGE   0x0010 */



   if ( answer != -1)
   {
      Spell tmpspell;
      //printf ("debug: cmdproc:input_arcane : spellid= %d", answer );
      tmpspell.SQLselect ( answer );
      Character tmpchar;
      tmpchar.SQLselect ( action.actorID() );
      //int target;

      if ( tmpspell.MP_cost () <= tmpchar.current_MP() )
      {
         action.target_type ( tmpspell.target () );
         action.value ( tmpspell.primary_key() );

         List lst_target ("Select your Target", 10);

         //action.target_type ( tmpspell.target() );

         int target_qty = (tmpspell.target() & Action_TARGET_MASK_QTY );

         // ----- calculate maximum range for short range spell-----

         int max_rank = 0;

         if ( (tmpspell.target() & Action_TARGET_SHORT_RANGE ) > 0)
         {
            switch ( tmpchar.rank())
            {
               case Party_FRONT:
                  max_rank = 2;
               break;
               case Party_BACK:
                  max_rank = 1;
               break;
            }
         }
         else
            max_rank = 2;

         //printf ("debug: cmdproc:input_arcane: target=%d, qty=%d", tmpspell.target(), target_qty );

         // else if required to instanciate menu/list inside the code, switch does not allow this.
         if ( target_qty == Action_TARGET_QTY_NONE )
         {

            // exception for targetting self
            if ( tmpspell.target() == Action_TARGET_SELF_RANGE )
            {
               action.target_type (Action_TARGET_ONECHARACTER );
               action.targetID ( action.actorID());
            }


            //do nothing
            //printf ("Debug:cmdproc:input_arcane: pass in none\n");
         }
         else if ( target_qty == Action_TARGET_QTY_ALL)
            {
               // do nothing
               //printf ("Debug:cmdproc:input_arcane: pass in all\n");
            }
            else
               if ( target_qty == Action_TARGET_QTY_GROUP)
               {
                  //printf ("Debug:cmdproc:input_arcane: pass in group\n");
                  Menu mnu_target_group ("Select a group to target?");

                  mnu_target_group.add_item (1, "Front Row");
                  if ( max_rank == 1 && ( ( tmpspell.target() & Action_TARGET_MASK_ENEMY) > 0 ) )
                     mnu_target_group.add_item (2, "Back Row", true);
                  else
                     mnu_target_group.add_item (2, "Back Row");
                  //mnu_target_group.add_item (-1, "Cancel");

                     WinData<int> wdat_party_bar ( WDatProc_party_bar, party.primary_key(),
                        WDatProc_POSITION_PARTY_BAR, true );

                     wdat_party_bar.hide();

                     //printf ("Debug:cmdproc:input generic_spell: before party bar check\n");
                     if ( party.status () == Party_STATUS_CAMP
                         && (tmpspell.target() & Action_TARGET_MASK_CHARACTER) > 0 )
                     {
                        wdat_party_bar.unhide();
                        Window::instruction ( 320, 340 );
                     }

                  WinMenu wmnu_target_group ( mnu_target_group, 0, 156, false, false, true );

                  blit_mazebuffer();
                  combat.draw_enemy();
                  int answer2 = Window::show_all();

                  if ( answer2 != -1)
                     action.targetID ( answer2 );
                  else
                     retval = Action_INPUT_RETVAL_CANCEL;

               }
               else
                  if (target_qty == Action_TARGET_QTY_ONE)
                  {

                     //printf ("Debug:cmdproc:input_arcane: pass in one\n");
                     List lst_target ("Select a target? ", 8);

                     if ( (tmpspell.target() & Action_TARGET_MASK_ENEMY) > 0 )
                     {


                        Monster tmpmonster;

                     // ennemies first
                     //SQLerrormsg_activate();
                        int error = tmpmonster.SQLpreparef ("WHERE body=0 AND rank<=%d ORDER BY position", max_rank );

                        if ( error == SQLITE_OK)
                        {
                           error = tmpmonster.SQLstep();

                           while ( error == SQLITE_ROW)
                           {
                              lst_target.add_item ( tmpmonster.primary_key(), tmpmonster.displayname() );
                              error = tmpmonster.SQLstep();
                           }
                        }
                        else
                           tmpmonster.SQLerrormsg();

                        tmpmonster.SQLfinalize();

                     }

                     //SQLerrormsg_deactivate();

                     // friends second

                     if ( (tmpspell.target() & Action_TARGET_MASK_CHARACTER) > 0 )
                     {
//                        debug_printf( __func__, "Before");
                        lst_target.add_query ( "name", "character", "WHERE location=2 ORDER BY position");
 //                       debug_printf( __func__, "After");
                     }

                     //problem, if include both target type, targetID will not point on the right table.

                     WinData<int> wdat_party_bar ( WDatProc_party_bar, party.primary_key(),
                        WDatProc_POSITION_PARTY_BAR, true );

                     wdat_party_bar.hide();

                     //printf ("Debug:cmdproc:input generic_spell: before party bar check\n");
                     if ( party.status () == Party_STATUS_CAMP
                         && (tmpspell.target() & Action_TARGET_MASK_CHARACTER) > 0 )
                         {


                           wdat_party_bar.unhide();
                           Window::instruction ( 320, 340 );
                         }

                     WinList wlst_target ( lst_target, 0, 156, false, false, true );

                     blit_mazebuffer();
                     combat.draw_enemy();
                     int answer2 = Window::show_all();

                     if ( answer2 != -1)
                        action.targetID ( answer2 );
                     else
                        retval = Action_INPUT_RETVAL_CANCEL;
                  }

      }

      else
      {
         WinMessage wmsg_error ("You do not have enough mana to cast this spell");
         Window::show_all ();
         retval = Action_INPUT_RETVAL_CANCEL;
      }

   }
   else
   {
      retval =  Action_INPUT_RETVAL_CANCEL;
   }

   return ( retval );
   /*




   +private: int p_target_type; // target ennemy
   +private: int p_targetID; //
   +private: int p_value; // value used differently according to the command type*/

   /*
      action.targetID ( answer );
      action.target_type ( Action_TARGET_ONEENEMY );
      //action.SQLupdate();
      //printf ("Cmdproc: input targetID=%d\n", answer);
   */
}

void CmdProc_build_spell_list ( Action &action, int school, char *querystr, bool party_restriction  )
{

      char schoolstr [121];
      char levelstr [21] = "level";
      bool highlevel = false;
      int schoolcursor = 1;
      bool firstschool = true;
      int charschool = 0;
      //int answer;
      //char spellname [10] = "word";
      char castable [35] = "";


      if ( party_restriction == true )
      {


      // need to consider that read does not restrict to castable spells
      // need an extra parameter.
         switch ( party.status ())
         {
            case Party_STATUS_CAMP:
               strcpy ( castable, " AND (castable=1 OR castable=3) ");
            break;
            case Party_STATUS_COMBAT:
               strcpy ( castable, " AND (castable=2 OR castable=3) ");
            break;

         }
      }

      Character tmpchar;
      tmpchar.SQLselect ( action.actorID() );
      CClass tmpclass;
      tmpclass.SQLselect ( tmpchar.FKcclass() );

      charschool = (tmpclass.magic_school () & school );

//      List lst_spells ( "Spell list ", 25, true);
      //Character tmpchar;


      /*if ( config.get ( Config_SPELL_NAME ) == Config_DIF_DISABLED )
         strcpy ( spellname, "name");*/

      for ( schoolcursor = 1; schoolcursor < CClass_MAGICSCHOOL_HIGHALL ; schoolcursor = schoolcursor<< 1)
      {
         //printf ( "debug: cmdproc: readspell %d, charschool=%d\n", schoolcursor, charschool );

         if ( (charschool & schoolcursor) > 0 )
         {
            if ( firstschool == true )
            {


               sprintf ( schoolstr, "school=%d ", schoolcursor );
               firstschool = false;
            }
            else
               sprintf ( schoolstr, "%s OR school=%d ", schoolstr, schoolcursor );

            if ( schoolcursor > CClass_MAGICSCHOOL_ONLYLOW)
               highlevel = true;
         }
      }

      if ( highlevel == true )
         sprintf ( levelstr, "highlevel");


      //SQLactivate_errormsg();
      sprintf ( querystr, "WHERE ( %s) %s AND %s <= %d ORDER BY effect_id, pk",
               schoolstr, castable, levelstr, tmpchar.level() );

      //printf ( querystr );

}

int CmdProc_resolve_insane_imitation ( Action &action, Opponent *actor, Opponent *target )
{
   system_log.writef ("%s is making an imitation of W E R D N A", actor->displayname() );

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_resolve_insane_sing ( Action &action, Opponent *actor, Opponent *target )
{
   system_log.writef ("%s is signing \"Tales of the Vorpal Bunnies\" ", actor->displayname() );

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_resolve_disturb ( Action &action, Opponent *actor, Opponent *target )
{
   system_log.writef ("%s seems disturbed and do nothing.", actor->displayname() );
   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_input_slap ( Action &action )
{
   Character tmpchar;
   Character targetchar;
   //Item tmpitem;
   //Monster tmpmonster;
   //int nb_monster = 0;
   //int max_range = 0;
   //int max_rank = 0;
   //char tmpstr [ 100 ];
   int answer;
   int retval;
   int inputretval = Action_INPUT_RETVAL_NONE;

   tmpchar.SQLselect ( action.actorID() );

   // ----- Select targettable monster -----


   Menu mnu_character ("Select a character to slap");

   retval = targetchar.SQLpreparef ("WHERE location=%d ORDER BY position", Character_LOCATION_PARTY );

   if ( retval == SQLITE_OK)
   {
      retval = targetchar.SQLstep();

      while ( retval == SQLITE_ROW)
      {
         if ( targetchar.primary_key() != tmpchar.primary_key())
            mnu_character.add_item ( targetchar.primary_key(), targetchar.displayname() );
         retval = targetchar.SQLstep();
      }
   }
   else
      targetchar.SQLerrormsg();

   targetchar.SQLfinalize();

   //need to do manually if want fakename
   //mnu_monster.add_query ( "name", "monster", tmpstr );

   WinMenu wmnu_character ( mnu_character, 0, 156, false, false, true );

   blit_mazebuffer();
   combat.draw_enemy();
   answer = Window::show_all();

   if ( answer != -1)
   {
      action.targetID ( answer );
      action.target_type ( Action_TARGET_ONECHARACTER );
   }
   else
      inputretval = Action_INPUT_RETVAL_CANCEL;

   return ( inputretval );
}

int CmdProc_AIinput_slap ( Action &action )
{
   return Action_INPUT_RETVAL_NONE;
}

int CmdProc_resolve_slap ( Action &action, Opponent *actor, Opponent *target  )
{
   system_log.writef ("%s slap %s.", actor->displayname(), target->displayname() );

   ActiveEffect::trigger_disturb_expiration( target );

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_resolve_active_effect ( Action &action, Opponent *actor, Opponent *target )
{
   //printf ( "debug: cmdproc: resolve active effect\n");
  /*

   Window::instruction ( 550, 460, Window_INSTRUCTION_CANCEL );
   Window::draw_all();

   copy_buffer();*/
   //char tmpstr [101];

   int error;
   WinEmpty wempty (0, 0, 640, 480 );
   List lst_effect ("", 17, true );
   lst_effect.header ("Effect Name               Time  Expiration         Category           Condition                    ");

   Window::instruction ( 320, 306 );
   ActiveEffect tmpeffect;

   error = tmpeffect.SQLpreparef ( "WHERE target_type=%d AND target_id=%d", Opponent_TYPE_CHARACTER, actor->primary_key() );

   if ( error == SQLITE_OK )
   {
      error = tmpeffect.SQLstep();

      while ( error == SQLITE_ROW )
      {
         lst_effect.add_itemf ( tmpeffect.primary_key(), "%-16s (%4d) %-13s %-12s % s", tmpeffect.name (), tmpeffect.time(),
                     STR_AEFFECT_EXPIRATION [ tmpeffect.expiration()-1 ], STR_SPELL_CURE [ tmpeffect.category()-1 ]
                        ,tmpeffect.condition_str() );

         error = tmpeffect.SQLstep();
      }

      tmpeffect.SQLfinalize();
   }

   int answer = 0;

   if ( lst_effect.nb_item() > 0)
   {
      WinData<int> wdat_aeffect ( WDatProc_active_effect_detail, lst_effect.answer(0) , WDatProc_POSITION_ACTIVE_EFFECT_DETAIL );

      WinList wlst_effect ( lst_effect, 0, 0 );

      while ( answer != -1)
      {

         Window::refresh_all();
         answer = Window::show_all();
         wdat_aeffect.key ( lst_effect.answer());

      }

   }
   else
   {
      WinMessage wmsg_noeffect ("That character has no active effects?");
      Window::show_all();
   }

   //while (mainloop_readkeyboard() != CANCEL_KEY );

   return Action_RESOLVE_RETVAL_NONE;
}

int CmdProc_AIinput_special_attack_spell ( Action &action )
{
   Spell tmpattack;
   int error;

   error = tmpattack.SQLselect ( action.value() );
//   printf ("Debug: CmdProc: Special Attack: --- Pass 1 ---\n");
   if ( error == SQLITE_ROW )
   {
      Monster tmpmons;
      tmpmons.SQLselect ( action.actorID() );
      //printf ("debug: cmdproc:AIinput_special_attack_spell : action value= %d\n", action.value() );
//printf ("Debug: CmdProc: Special Attack: pass 2\n");
      if ( tmpattack.MP_cost () <= tmpmons.current_MP() )
      {
         //int tmptarget = action.target_type();
//printf ("Debug: CmdProc: Special Attack: pass 2\n");
         if ( ( tmpattack.target () & Action_TARGET_MASK_ENEMY) > 0)
            action.target_type ( ( tmpattack.target () & ~Action_TARGET_MASK_ENEMY) | Action_TARGET_MASK_CHARACTER );
         else if ( ( tmpattack.target () & Action_TARGET_MASK_CHARACTER) > 0)
            action.target_type ( ( tmpattack.target () & ~Action_TARGET_MASK_CHARACTER) | Action_TARGET_MASK_ENEMY );
         else
            action.target_type ( tmpattack.target());

         action.value ( tmpattack.primary_key() );

         //if ( tmptarget == Action_TARGET_SELF_RANGE)
         //   printf ("Debug: CmdProc: Special Attack: Before=%d, after=%d\n", tmpattack.target(), action.target_type() );


         int target_qty = (action.target_type() & Action_TARGET_MASK_QTY );

         // ----- calculate maximum range for short range spell-----

         int max_rank = 0;
         int tmprange = (tmpattack.target() & Action_TARGET_MASK_RANGE );


         switch ( tmprange  )
         {
            case Action_TARGET_MELEE_RANGE:
               if ( tmpmons.rank() == Party_FRONT )
                  max_rank = 1;
               else
                  max_rank = 0;
            break;
            case Action_TARGET_SHORT_RANGE:
               switch ( tmpmons.rank())
               {
                  case Party_FRONT: max_rank = 2; break;
                  case Party_BACK: max_rank = 1; break;
               }
            break;
            case Action_TARGET_SELF_RANGE:
               max_rank = -1;
            break;
            default:
               max_rank = 2;
            break;
         }

         if ( max_rank == 0 )
         {

            //printf ("Debug: cmdproc: special attack: cancel by max_rank==0\n");
            return Action_INPUT_RETVAL_CANCEL; // cancel action since no selectable target in that range
         }
         //printf ("debug: cmdproc:Aiinput_special_attack_spell: target=%d, qty=%d", tmpattack.target(), target_qty );

         Monster target_friend;
         Character target_enemy;
         int error;
         bool failed = false;



         switch ( target_qty )
         {
            case Action_TARGET_QTY_NONE:
               // exception for targetting self
               //printf ("Debug: CmdProc: Special attack spell: Passed in qty none\n");
               if ( tmpattack.target() == Action_TARGET_SELF_RANGE )
               {
                  //printf ("Debug: CmdProc: Special attack spell: Passed in qty none\n");
                  action.target_type (Action_TARGET_SELF_RANGE );
                  action.targetID ( action.actorID());
               }

            break;
            case Action_TARGET_QTY_ALL:
            break;
            case Action_TARGET_QTY_GROUP:
               action.targetID ( dice ( max_rank ));
               // could see if there are monsters/characters in front or back row, to make sure does not target void
            break;
            case Action_TARGET_QTY_ONE: // note: monster and character are inverted since friends are monsters
               if ( (tmpattack.target() & Action_TARGET_MASK_CHARACTER) > 0 )
               {
                  // can currently target dead and yourself what ever the spell.
                  error = target_friend.SQLpreparef ("WHERE NOT pk=%d AND body=0 AND location=%d ORDER BY random()",
                                                     action.actorID(), Character_LOCATION_PARTY );

                  if ( error == SQLITE_OK)
                  {
                     error = target_friend.SQLstep();

                     if ( error == SQLITE_ROW)
                        action.targetID ( target_friend.primary_key());
                     else
                        failed = true;
                  }
                  else
                     failed = true;
               }
               else if ( (tmpattack.target() & Action_TARGET_MASK_ENEMY) > 0 )
               {
                  error = target_enemy.SQLpreparef ("WHERE body=0 ORDER BY random()");

                  if ( error == SQLITE_OK)
                  {
                     error = target_enemy.SQLstep();

                     if ( error == SQLITE_ROW)
                        action.targetID ( target_enemy.primary_key () );
                     else
                        failed = true;
                  }
                  else
                     failed = true;

               }
            break;
            default:
               printf ("Debug: cmdproc: Special Attack: Error: Target_qty=%d", target_qty );
               failed = true;
            break;
         }

         if ( failed == true )
         {

            //printf ("Debug: cmdproc: special attack: cancel by failed == true\n");
            return Action_INPUT_RETVAL_CANCEL;
         }
         else
            return Action_INPUT_RETVAL_NONE;

      }
      else
      {

         //printf ("Debug: cmdproc: special attack: cancel by not enough mann\n");
         return ( Action_INPUT_RETVAL_CANCEL ); // cannot execute this command. Not enough mana
      }
   }
   else
   {

      //printf ("Debug: cmdproc: special attack: cancel by cannot find action\n");
      return ( Action_INPUT_RETVAL_CANCEL ); // cannot execute this command. Attack does not exists
   }

   return Action_INPUT_RETVAL_NONE;
}

/*-------------------------------------------------------------------------*/
/*-                     Global Variables                                  -*/
/*-------------------------------------------------------------------------*/

const s_CmdProc_command CmdProc_COMMANDLIST [ CmdProc_COMMANDLIST_SIZE ] =
{
   //name, usablebydead, Initiative, requirement, value, monster action, available, input, AIinput, resolve
   // 0-15
   { "Zap "                ,false , 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_input_zap, CmdProc_AIinput_zap, CmdProc_resolve_zap  },
   { "Fight "              ,false, 0, CmdProc_REQUIREMENT_REACH, 0,  CmdProc_AVAILABLE_COMBAT, CmdProc_input_fight, CmdProc_AIinput_fight, CmdProc_resolve_fight  },
   { "Defend "             ,false, 8, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_COMBAT, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_parry  },
   { "Protect "            ,false, 4, CmdProc_REQUIREMENT_NOTALONE,  0, CmdProc_AVAILABLE_NEVER, CmdProc_input_protect, CmdProc_AIinput_protect, CmdProc_resolve_protect  }, // disabled, maybe become a class skill
   { "Run "                ,false,-8, CmdProc_REQUIREMENT_FIRST, 0,  CmdProc_AVAILABLE_COMBAT, CmdProc_input_run, CmdProc_AIinput_run, CmdProc_resolve_run  },
   { "Arcane Spell "       ,false, 0, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_ARCANE,  CmdProc_AVAILABLE_BOTH, CmdProc_input_arcane, CmdProc_AIinput_arcane, CmdProc_resolve_spell  },
   { "Divine Spell "       ,false,-8, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_DIVINE,  CmdProc_AVAILABLE_BOTH, CmdProc_input_divine, CmdProc_AIinput_divine, CmdProc_resolve_spell  },
   { "Alchemy Spell "      ,false,-4, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_ALCHEMY,  CmdProc_AVAILABLE_BOTH, CmdProc_input_alchemy, CmdProc_AIinput_alchemy, CmdProc_resolve_spell  },
   { "Psionic Spell "      ,false, 4, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_PSIONIC,  CmdProc_AVAILABLE_BOTH, CmdProc_input_psionic, CmdProc_AIinput_psionic, CmdProc_resolve_spell  },
   { "Use Item "           ,false,-4, CmdProc_REQUIREMENT_ITEM, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_input_use_item, CmdProc_AIinput_use_item, CmdProc_resolve_use_item  },
   { "Call for Help "      ,false, 0, CmdProc_REQUIREMENT_NOTFULL,  0, CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_call_help  },
   { "Equip "              ,true ,-4, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_CAMP, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_equip  },
   { "Read Arcane "        ,true , 0, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_ARCANE,  CmdProc_AVAILABLE_CAMP, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_read_arcane  },
   { "Read Divine "        ,true , 0, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_DIVINE,  CmdProc_AVAILABLE_CAMP, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_read_divine },
   { "Read Alchemy "       ,true , 0, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_ALCHEMY,  CmdProc_AVAILABLE_CAMP, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_read_alchemy },
   { "Read Psionic "       ,true , 0, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_PSIONIC,  CmdProc_AVAILABLE_CAMP, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_read_psionic },
   // 16 - 31
   { "Read Other "         ,true , 0, CmdProc_REQUIREMENT_SPELL, CClass_MAGICSCHOOL_OTHER,  CmdProc_AVAILABLE_CAMP, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_read_other },
   { "Slap "               ,false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_COMBAT, CmdProc_input_slap, CmdProc_AIinput_slap, CmdProc_resolve_slap },
   { "Active Effects "     ,true , 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_CAMP, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_active_effect },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   // 32-47
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0, CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   // 48 -63
   { "Insane Imitation    ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_insane_imitation },
   { "Insane Sing         ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_insane_sing },
   { "Disturbed           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_disturb },
   { "Do Nothing          ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "Special Attack/Spell",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_AIinput_special_attack_spell, CmdProc_resolve_spell },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "?Unknown?           ",false, 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_NEVER, CmdProc_do_nothing, CmdProc_do_nothing, CmdProc_resolve_nothing },
   { "Previous "           ,true , 0, CmdProc_REQUIREMENT_NOTFIRST,  0, CmdProc_AVAILABLE_COMBAT, CmdProc_input_previous, CmdProc_do_nothing, CmdProc_resolve_nothing  },
   { "Return "             ,true , 0, CmdProc_REQUIREMENT_NONE, 0,  CmdProc_AVAILABLE_CAMP, CmdProc_input_return, CmdProc_do_nothing, CmdProc_resolve_nothing  }

};
