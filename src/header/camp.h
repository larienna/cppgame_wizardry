/***************************************************************************/
/*                                                                         */
/*                              C A M P . H                                */
/*                            Class definition                             */
/*                                                                         */
/*     Content : Class Camp                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Stating Date : May 10th, 2002                                       */
/*                                                                         */
/*          The camp class is a engine for managing the camp menu, display */
/*     character stats and executer their commands, etc.                   */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                      Constant Class Parameter                         -*/
/*-------------------------------------------------------------------------*/

#define Camp_FONT_CHARACTER   FNT_print
#define Camp_FONT_INSTRUCTION FNT_small

/*-------------------------------------------------------------------------*/
/*-                             Constants                                 -*/
/*-------------------------------------------------------------------------*/

// camp return value

//#define Camp_BACKTOMAZE    0
//#define Camp_QUITPARTY     1

// camp menu selection

#define Camp_MENU_CHARACTER    0
#define Camp_MENU_INVENTORY    1
#define Camp_MENU_REORDER      2
#define Camp_MENU_FORMATION    3
#define Camp_MENU_SEARCH       4
#define Camp_MENU_DIARY        5
#define Camp_MENU_OPTION       6
#define Camp_MENU_SYSTEMLOG    7
#define Camp_MENU_MAP          8
#define Camp_MENU_QUIT         9
#define Camp_MENU_CANCEL       -1

/*#define Camp_MENU_SPELL        2
#define Camp_MENU_MECHANICS    3
#define Camp_MENU_HEAL         4*/
//#define Camp_MENU_USE_ITEM     5
//#define Camp_MENU_TRADE_ITEM   6

// character Menu selection

/*#define Camp_CHARMENU_EQUIP   0
//#define Camp_CHARMENU_UNEQUIP 1
//#define Camp_CHARMENU_INSPECT 2
//#define Camp_CHARMENU_DROP    3
#define Camp_CHARMENU_READ    1
#define Camp_CHARMENU_CAST    2
//#define Camp_CHARMENU_STAT    5
#define Camp_CHARMENU_CANCEL  -1*/

#define Camp_INVMENU_USE      0
#define Camp_INVMENU_DROP     1
#define Camp_INVMENU_CANCEL   -1

/*-------------------------------------------------------------------------*/
/*-                        Class Definition                               -*/
/*-------------------------------------------------------------------------*/

class Camp
{
   /*--- Properties ---*/

   private: bool p_return_to_city; // do not display any maze

   //private: Party p_party;

   /*--- Constructors & Destructors ---*/

   public: Camp ( void );
   public: ~Camp ( void );

   /*--- Property Methods ---*/


   /*--- Methods ---*/

   public: int start ( bool return_to_city = false );

   /*--- Private Methods ---*/

   private: int  show_character ( void ); // return value of command, in case force exit
   //private: void show_equip_item ( int character_key );
   //private: int show_select_inventory ( Character &tmpchar );
   //private: int show_select_equipment ( Character &tmpchar );
   //private: void show_inspect_item ( Character &tmpchar, int inventoryID );
//   private: void show_use_item ( Character &tmpchar, int inventoryID );
//   private: void show_fighting_style ( Character &tmpchar );
   private: void show_read_spell ( Character &tmpchar );

   //private: void show_combat_stat ( Character &tmpchar );

   private: void show_inventory ( void );
   private: void show_reorder ( void );
   private: void show_formation ( void );
   private: void show_diary ( void );
   private: void show_system_log ( void );
   private: bool show_search ( void );

};

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

extern Camp camp;

