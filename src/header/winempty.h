/***************************************************************************/
/*                                                                         */
/*                         W I N E M P T Y . H                             */
/*                         Class Definition                                */
/*                                                                         */
/*     Engine : Window Engine                                              */
/*     Content : Class WinEmpty                                            */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 4th, 2002                                  */
/*                                                                         */
/*          Derived class from Window which allows to create empty windows.*/
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                          Class Definition                             -*/
/*-------------------------------------------------------------------------*/

class WinEmpty : public Window
{

   // --- Constructors and destructors ---

   public: WinEmpty ( short x1 = 0, short y1 = 0, short x2 = 639, short y2 = 479, bool translucent = false  );
//   public: ~WinEmpty ( void );

   //--- Virtual Methods ---

   public: virtual void preshow ( void );
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

};

