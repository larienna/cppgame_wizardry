/***************************************************************************/
/*                                                                         */
/*                         O P P O N E N T . H                             */
/*                           Class Definition                              */
/*                                                                         */
/*     Content : Class Opponent                                            */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : June 9, 2002                                        */
/*                                                                         */
/*          This class is used to store the necessary information of each  */
/*     opponent who participate in a combat. Opponent objects are created  */
/*     from Characters and Monster which are converted. Which mean that    */
/*     character's and monsters class will no be used for combat           */
/*     operations. When comabt is finished, data is saved in the character */
/*     classes if they were generated from characters.                     */
/*                                                                         */
/*     this class need to be redifined according to combat needs           */
/*     Maybe it will be a temporary class used for battles only. The data  */
/*     will be generated or duplicated from the monster or characters.     */
/*                                                                         */
/***************************************************************************/

#ifndef OPPONENT_H_INCLUDED
#define OPPONENT_H_INCLUDED




/*-------------------------------------------------------------------------*/
/*-                     Constant class Parameter                          -*/
/*-------------------------------------------------------------------------*/

/*#define Opponent_NB_MAX_HIT_LOCATION 10
#define Opponent_NB_MAX_ATTACK_STAT  5
#define Opponent_NB_HEALTH           16
#define Opponent_NB_ELEMENT          6
#define Opponent_NB_MAX_SABILITY     3
#define Opponent_NB_SIZE             5
#define Opponent_NB_ALIGMENT         6*/

/*-------------------------------------------------------------------------*/
/*-                         Constants                                     -*/
/*-------------------------------------------------------------------------*/

// hit location for humanoids

// Use Armor_LOCATION Instead
/*
#define Opponent_LOCATION_TORSO  0
#define Opponent_LOCATION_LLEG   1
#define Opponent_LOCATION_RLEG   2
#define Opponent_LOCATION_LARM   3
#define Opponent_LOCATION_RARM   4
#define Opponent_LOCATION_HEAD   5
*/

// Opponent Type

#define Opponent_TYPE_CHARACTER  1
#define Opponent_TYPE_ENNEMY     2

// Attribute identification


//#define Character_LUCK           6

/*#define Opponent_STRENGTH       0
#define Opponent_DEXTERITY      1
#define Opponent_ENDURANCE      2
#define Opponent_INTELLIGENCE   3
#define Opponent_CUNNING        4
#define Opponent_WILLPOWER      5
#define Opponent_LUCK           6

#define Opponent_ALIGMENT_KIND         0 // life         kn
#define Opponent_ALIGMENT_PEACEFULL    1 // water        pf
#define Opponent_ALIGMENT_LIVELY       2 // Air          lv
#define Opponent_ALIGMENT_SELLFISH     3 // Death        sf
#define Opponent_ALIGMENT_AGGRESSIVE   4 // Fire         ag
#define Opponent_ALIGMENT_STOICAL      5 // Earth        st

// Element identification

#define Opponent_ELEMENT_LIFE      0
#define Opponent_ELEMENT_WATER     1
#define Opponent_ELEMENT_AIR       2
#define Opponent_ELEMENT_DEATH     3
#define Opponent_ELEMENT_FIRE      4
#define Opponent_ELEMENT_EARTH     5

#define Opponent_ELEMENTBIT_LIFE      1
#define Opponent_ELEMENTBIT_WATER     2
#define Opponent_ELEMENTBIT_AIR       4
#define Opponent_ELEMENTBIT_DEATH     8
#define Opponent_ELEMENTBIT_FIRE      16
#define Opponent_ELEMENTBIT_EARTH     32
*/
// Elemental Strength


// maze special resistance ID

//?? note sure how to implemement these, either race ability
/*
#define Opponent_SABILITY_AMPHIBIOUS         1
#define Opponent_SABILITY_NATURALMANA        2
#define Opponent_SABILITY_INFRAVISION        4
#define Opponent_SABILITY_POISON_BREATHING   8
#define Opponent_SABILITY_DARKVISION         16
#define Opponent_SABILITY_LEVITATE           32
#define Opponent_SABILITY_WEAPON_IMMUNITY    64
#define Opponent_SABILITY_MAGIC_IMMUNITY     128
#define Opponent_SABILITY_NATURAL_ARMOR      256
#define Opponent_SABILITY_HEAVY_ARMOR        512
#define Opponent_SABILITY_REGENERATION       1024
*/

// health identification value
// note : 8 first health are physical health status while
//        8 last health status are mental/magic status

/*#define Opponent_PHYSICAL_HEALTH_LIMIT   128 // define where the physcal
   // health status stop in the list in case the proportion is not 8/8

//?? idea : last physical health : Lycanthropy
//?? idea : Mental health : Confused

#define Opponent_HEALTH_ASLEEP     1       // cannot move, recover easy
#define Opponent_HEALTH_PARALYSED  2       // cannot move, recover hard
#define Opponent_HEALTH_PETRIFIED  4       // cannot move, no recover
#define Opponent_HEALTH_RESERVED   8       //
#define Opponent_HEALTH_POISONED   16      // lose HP gradualy
#define Opponent_HEALTH_BLINDED    32      // combat penalty
#define Opponent_HEALTH_CRIPPLED   64      // special injury, final penalty to combat
#define Opponent_HEALTH_RESERVED3  128
#define Opponent_HEALTH_AFFRAID    256     // combat penalty
#define Opponent_HEALTH_CURSED     512     // Luck rolls are allways failed
#define Opponent_HEALTH_SEALED     1024    // cannot cast spells
#define Opponent_HEALTH_DOOMED     2048    // Probability to die instantly
#define Opponent_HEALTH_FADING     4096    // lose soul continuously
#define Opponent_HEALTH_RESERVED5  8192
#define Opponent_HEALTH_RESERVED6  16384
#define Opponent_HEALTH_RESERVED7  32768

#define Opponent_HEALTHID_SLEEP      1       // cannot move, recover easy
#define Opponent_HEALTHID_PARALYZE   2       // cannot move, recover hard
#define Opponent_HEALTHID_PETRIFY    3       // cannot move, no recover
#define Opponent_HEALTHID_RESERVED   4       //
#define Opponent_HEALTHID_POISON     5       // LOSE HP
#define Opponent_HEALTHID_BLIND      6       // combat penalty
#define Opponent_HEALTHID_CRIPPLE    7
#define Opponent_HEALTHID_RESERVED3  8
#define Opponent_HEALTHID_FEAR       9       // combat penalty
#define Opponent_HEALTHID_CURSE      10      // Luck rolls are allways failed
#define Opponent_HEALTHID_SEAL       11      // cannot cast spells
#define Opponent_HEALTHID_DOOM       12      // Probability to die instantly
#define Opponent_HEALTHID_FADE       13    // lose soul continuously
#define Opponent_HEALTHID_RESERVED5  14
#define Opponent_HEALTHID_RESERVED6  15
#define Opponent_HEALTHID_RESERVED7  16

// character status values

#define Opponent_STATUS_ALIVE      0
#define Opponent_STATUS_DEAD       1
#define Opponent_STATUS_ASHES      2
#define Opponent_STATUS_DELETED    3
#define Opponent_STATUS_DISPELLED  4

#define Opponent_SIZE_TINY      0
#define Opponent_SIZE_SHORT     1
#define Opponent_SIZE_NORMAL    2
#define Opponent_SIZE_LARGE     3
#define Opponent_SIZE_HUGE      4

*/

/* copy of stats constant for reference
// stats constants

#define D20_NB_STAT        8
#define D20_NB_MODIFIER    3
#define XYZ_NB_STAT        2
#define XYZ_NB_MODIFIER    5

#define D20STAT_AD      0
#define D20STAT_MD      1
#define D20STAT_PS      2
#define D20STAT_MS      3
#define D20STAT_PCS     4
#define D20STAT_MCS     5
#define D20STAT_DR      6
#define D20STAT_INIT    7

#define D20MOD_ATTRIBUTE   0
#define D20MOD_EQUIP       1
#define D20MOD_AEFFECT     2

#define XYZSTAT_DMG     0
#define XYZSTAT_MDMG    1

#define XYZMOD_X           0
#define XYZMOD_Y           1
#define XYZMOD_ZATTRIBUTE  2
#define XYZMOD_ZEQUIP      3
#define XYZMOD_ZAEFFECT    4
*/


/*-------------------------------------------------------------------------*/
/*-                          Type Definition                              -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Opponent_stat
{
   int d20 [ D20_NB_STAT ] [ D20_NB_MODIFIER + 1 ]; //add +1 for the total column
   int xyz [ XYZ_NB_STAT ] [ XYZ_NB_MODIFIER + 1 ]; // add +1 for the total column
   int flat [ FLAT_NB_STAT ]; // basic flat stats
}s_Opponent_stat;

/*typedef struct s_Opponent_aligment
{
   int law;
   int good;
}s_Opponent_aligment;*/

/*typedef struct s_Opponent_healthC
{
   char letter;
   int color;
}s_Opponent_healthC;

typedef struct s_Opponent_hstr
{
   s_Opponent_healthC character [ 16 ];
}s_Opponent_hstr;*/

/*
typedef struct s_Opponent_health_info
{
   unsigned short mask; // mask to apllay to check presence
   char adjective[ 17 ]; // string used for display
   char name [ 17 ]; // string used for display
   short red;  // color that must be used for display
   short green;
   short blue;
   int attribute; // attribute lost when raising this resistance
   int TN; // base TN + level modifier + difficulty modifier
}s_Opponent_health_info;

typedef struct s_Opponent_aligment_info
{
   char name [ 11 ]; // full name
   char initial [ 3 ]; // 2 letter code
}s_Opponent_aligment_info;
*/

/*typedef struct s_Opponent_rollstat
{
   int min;
   int max;
   int modifier; // modify max but this stat can change compared to others
   int bonus;
//   short other;
}s_Opponent_rollstat;*/


/*typedef struct s_Opponent_stat
{
   unsigned short elm_resist;//  ;
   unsigned short hlt_resist;//  ;
   unsigned short elm_effect;//  ;
   unsigned short hlt_effect;//  ;
   unsigned short magikproperty;//  ;
   int multihitmod;//  ;
   int hitbonus;//  ;
   int encmod;//  ;
   int dmg_x;//  ;
   int dmg_y;//  ;
   int dmg_z;//  ;
   int dmg_type;//  ;
   int mdmg_x;//  ;
   int mdmg_y;//  ;
   int mdmg_z;//  ;
   int range;//  ;
//   int hitroll  ;
   int nb_max_attack;//  ;
   int PD;//  ;
   int AD;//  ;
   int DR;//  ;
   int MDR;//  ;
   int MPD;//  ;
   int MAD;//  ;
   int PSAVE;//  ;
   int MSAVE;//  ;
   int init;//  ;
}  s_Opponent_stat;

typedef struct dbs_Opponent
{
   char name [ 16 ];//  ;
   int type;//  ;
   int size;//  ;
   short max_HP;//  ;
   short current_HP;//  ;
   short max_MP;//  ;
   short current_MP;//  ;
   int aligment;//  ;
   int attribute [ 7 ];//  ;
   int level;//  ;
   int soul;//  ;
   unsigned short health;//  ;
   int status;//  ;
   unsigned short elm_weakness;//  ;
   short reward_exp;//  ;
   short gold;//  ;
   unsigned char pictureID;//  ;
}  dbs_Opponent;
*/

/*-------------------------------------------------------------------------*/
/*-                        Global Variables                               -*/
/*-------------------------------------------------------------------------*/

//extern V3D ELEMENT_DIAGRAM [ Opponent_NB_ELEMENT ] [ Opponent_NB_ELEM_LEVEL ];

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class Opponent : public SQLobject
{

   /*--- Properties ---*/
   // this class will contain no properties anymore, only virtual function to the child class data.
   // most properties are in the derived class. Only the compiled stats are here.
   // Properties in this class are non persistant and needs to be recalculated with compile stat

   protected: s_Opponent_stat p_stat;
   //protected: int p_tmpCS; // temporary copy, will select the right combat skill modifiers for the character weapon
   //protected:
   //protected: int p_rank; // determines if character is in front or in the back.

   /*--- Constructor and Destructors ---*/

   public: Opponent ( void );
   public: virtual ~Opponent ( void );


   // ----- Property methods ----

   //public: int virtual CS ( void ) = 0;

   /*--- Methods --- */


   protected: void clear_stat ( void );
   protected: void sum_stat (void);

   public: void revive ( void );

   /*--- Virtual Methods to redefine ---*/


   public: char virtual *name ( void ) = 0;
   //public: void virtual name ( const char *str ) = 0;
   public: int virtual current_HP ( void ) = 0;
   public: int virtual max_HP ( void ) = 0;
   public: int virtual current_MP ( void ) = 0;
   public: int virtual max_MP ( void ) = 0;
   //public: unsigned int virtual health ( void ) = 0;
   public: int virtual body ( void ) = 0;
   public: int virtual location ( void ) = 0;
   //public: int virtual position ( void ) = 0;
   //public: bool virtual health ( unsigned int value ) = 0;
   //public: unsigned int virtual health ( void ) = 0;
   public: void virtual body ( int value ) = 0;
   public: int virtual soul ( void ) = 0;
   public: void virtual location ( int value ) = 0;
   //public: void virtual position ( int value ) = 0;

   public: void virtual recover_HP ( int value ) = 0;
   public: void virtual recover_MP ( int value ) = 0;
   public: void virtual recover_soul ( int value ) = 0;
   public: void adjust_HP ( int value) { value < 0 ? lose_HP (ABS (value)) : recover_HP (value); };
   public: void adjust_MP ( int value) { value < 0 ? lose_MP (ABS (value)) : recover_MP (value); };
   public: void virtual set_HP ( int value ) = 0;
   public: void virtual lose_HP ( int value ) = 0;
   public: void virtual lose_MP ( int value ) = 0;
   public: void virtual lose_soul ( int value ) = 0;
   public: void virtual set_MP ( int value ) = 0;

   public: int virtual resistance ( void ) = 0;
   public: int virtual weakness ( void ) = 0;

   public: int virtual rank ( void ) = 0;
   public: void virtual rank ( int value ) = 0;

   public: char virtual* displayname ( void ) = 0;

   public: int virtual level ( void) = 0;
   public: int virtual type ( void ) = 0;
   public: int virtual size ( void ) = 0;
   public: int virtual power ( void ) = 0;



   //--- Static Function ---

   //public: static void compile_ranks ( void );

   /*--- Property Method ---*/



   public: int d20stat ( int statID ); /// return the total of the stat
   public: int d20stat ( int statID, int modifier ); // return a specific modifier
   public: int xyzstat ( int statID ); /// return the total of the stat
   public: int xyzstat ( int statID, int modifier ); // return a specific modifier
   public: int flatstat ( int statID ) { return ( p_stat.flat [ statID] ); } // return single value stats
   public: void virtual compile_stat ( void ) = 0;
   public: int  compile_condition ( void );


   //--------------------------------------- OLD CODE -----------------------------------

  // protected: char p_name [ 16 ]; // basic name of the opponent
   //protected: int p_aligment; // struct containing the aligment
   //protected: int p_size; // size of the opponent
   //protected: short p_max_HP; // maximum Hit Point of the character
   //protected: short p_current_HP; // current Hit Point value
   //protected: short p_max_MP; // maxium spell point of the character
   //protected: short p_current_MP; // curent spell point of the character
   //protected: int p_attribute [ 7 ]; // Opponent_attribute
   //protected: int p_level; // experience level of the character
   //protected: int p_soul; // current soul level of the character
   //protected: unsigned short p_health; // health status of the character
   //protected: int p_status; // status of the character
   //protected: unsigned short p_elm_weakness; // double dmg for these elements
   //protected: short p_reward_exp; // exp received for destroying ennemy
   //protected: short p_gold; // gold pieces hold by the opponent
   //protected: unsigned char p_pictureID; // Ennemy/Skin image ID number corresponding to the monster
   //protected: int p_type; // opponent type
   //protected: s_Opponent_stat p_stat; // compiled stats

   //private: static unsigned char p_maze_special; // contains maze special unsigned char



   /*--- Property Methods ---*/

   /*public: const char* name ( void );
   public: void name ( const char *str );
   public: void attribute ( int attribID, int value );
   public: int attribute ( int attribID );
   public: void strength ( int value );
   public: int strength ( void );
   public: int STRmodifier ( void );
   public: void dexterity ( int value );
   public: int dexterity ( void );
   public: int DEXmodifier ( void );
   public: void endurance ( int value );
   public: int endurance ( void );
   public: int ENDmodifier ( void );
   public: void intelligence ( int value );
   public: int intelligence ( void );
   public: int INTmodifier ( void );
   public: void cunning ( int value );
   public: int cunning ( void );
   public: int CUNmodifier ( void );
   public: void willpower ( int value );
   public: int willpower ( void );
   public: int WILmodifier ( void );
   public: void luck ( int value );
   public: int luck ( void );
   public: int LUCKmodifier ( void );
   public: int level ( void );
   public: int size ( void );
   public: void aligment ( int value );
   public: int aligment ( void );
   public: short max_HP ( void );
   public: short current_HP ( void );
   public: short max_MP ( void );
   public: short current_MP ( void );
   public: int soul ( void );
   public: bool health ( unsigned short healthID );
   public: unsigned short health ( void );
   public: void status ( int statusID );
   public: int status ( void );
   public: short reward_exp ( void );
   public: void reward_exp ( short value );
   public: short gold ( void );
   public: void gold ( short value );
   public: unsigned char pictureID ( void );
   public: void pictureID ( unsigned char value );
//   public: void type ( int typeID );
   public: int type ( void );



   public: const char* aligmentC ( void ); // 3 character
   public: const char* aligmentS ( void );
   public: const char* statusS ( void ); // 9 character
   public: s_Opponent_hstr healthS ( void );

   public: s_Opponent_stat stat ( void );
*/
   /*--- Methods ---*/

  /* public: void recover_HP ( short value );
   public: void recover_MP ( short value );
   public: void recover_soul ( int value );
   public: void lose_HP ( short value );
   public: void lose_MP ( short value );
   public: void lose_soul ( int value );
   public: void add_health ( unsigned short healthID );
   public: void remove_health ( unsigned short healthID );
   public: void gain_gold ( short value );
   public: void lose_gold ( short value );
   public: void save_vs ( unsigned short healthID, int level );

   public: void eval_stat_all ( void ); // call all the eval stat function
   public: void eval_stat_clear ( void );
   public: void eval_stat_base ( void );
   public: void eval_stat_wound ( void );
   public: void eval_stat_health ( void );
   public: void eval_stat_maze ( void );
   public: void eval_stat_enc_modifier ( void );
   public: void eval_stat_limit ( void );
//   public: void eval_stat_spell ( void );
   public: int initiative ( void );
   public: bool is_active ( void );

   public: void combat_action ( int action_type, int value,
                   char *result, Opponent &target);
*/
   /*--- Private Methods ---*/

/*   private: void cmbact_zap ( int value, char *result, Opponent &target  );
   private: void cmbact_fight ( int value, char *result, Opponent &target );
   private: void cmbact_parry ( int value, char *result );
   private: void cmbact_use_item ( int value, char *result );*/
//   private: void cmbact_move ( int value, char *result );

/*#define Combat_COMMAND_HIDE               3
#d#define Combat_COMMAND_CAST_SPELL         7
#define Combat_COMMAND_DISPEL             8
#define Combat_COMMAND_WEILD_MAGIC        9
#define Combat_COMMAND_PREVIOUS           10*/

   /*--- Virtual Methods ---*/

   //public: virtual void eval_stat_other ( void ) = 0;

//   public: virtual string& name_combat ( void ) =0;
   //public: virtual void name_combat ( char *str ) = 0;

//   public: l void save_vs ( unsigned short healthID, int modifier )= 0;


//   public: virtual void objdat_to_strdat ( void *dataptr );
//   public: virtual void strdat_to_objdat ( void *dataptr );
//   public: virtual void child_DBremove ( void );

};

/*-------------------------------------------------------------------------*/
/*-                   Global Variable Definition                          -*/
/*-------------------------------------------------------------------------*/

//extern const char STR_OPP_SIZE [][7];

//extern const char STR_OPP_DEFENSE [] [ 11 ];
//extern s_Opponent_aligment_info ALIGMENT_INFO [ Opponent_NB_ALIGMENT ];
//extern s_Opponent_limit_stat_info LIMIT_STAT [ Opponent_NB_STAT ];
//extern s_Opponent_life_template LIFE_TEMPLATE [ Opponent_NB_LIFE_TEMPLATE ];

#endif
