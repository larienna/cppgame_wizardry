/*************************************************************************/
/*                                                                       */
/*                              S P E L L . H                            */
/*                             Class Definition                          */
/*                                                                       */
/*     Content: Spell Class                                              */
/*     Programmer: Eric Pietrocupo                                       */
/*     Creation Date: October 31st, 2012                                 */
/*                                                                       */
/*          This class is used to manage spells castable by characters   */
/*     and monsters.                                                     */
/*                                                                       */
/*************************************************************************/

#ifndef SPELL_H_INCLUDED
#define SPELL_H_INCLUDED

#define Spell_WORD_STRLEN 13
#define Spell_NAME_STRLEN 21
#define Spell_ELEMENT_STRLEN 100
#define Spell_PROPERTY_STRLEN 49
#define Spell_DESCRIPTION_STRLEN 129


// ------------------------- Constants -------------------------------

#define Spell_CASTABLE_CAMP         1
#define Spell_CASTABLE_COMBAT       2
#define Spell_CASTABLE_CAMP_COMBAT  3
#define Spell_CASTABLE_DOOR         4
#define Spell_CASTABLE_CHEST        8
#define Spell_CASTABLE_DOOR_CHEST   12
#define Spell_CASTABLE_NPC          16
#define Spell_CASTABLE_MAZE         32

#define Spell_MONSTER_NO_USE        0
#define Spell_MONSTER_CAN_USE       1



//Note: try to use spell effect as sorting order
#define Spell_EFFECT_ATTACK_DICE          10
#define Spell_EFFECT_ATTACK_FIXED         11
#define Spell_EFFECT_ATTACK_TYPE_DICE     12
#define Spell_EFFECT_ATTACK_TYPE_FIXED    13
#define Spell_EFFECT_ATTACK_DRAIN_DICE    18
#define Spell_EFFECT_ATTACK_DRAIN_FIXED   19
#define Spell_EFFECT_HEAL_DICE            20
#define Spell_EFFECT_HEAL_FIXED           21
#define Spell_EFFECT_BUFF                 30
#define Spell_EFFECT_BUFF_EXPEDITION      35
#define Spell_EFFECT_DEBUFF               40
#define Spell_EFFECT_CONDITION            50
#define Spell_EFFECT_CONDITION_BODY       51
#define Spell_EFFECT_CONDITION_RANDOM     52
#define Spell_EFFECT_CURE                 60
#define Spell_EFFECT_CURE_BODY            61
#define Spell_EFFECT_UTILITY              70
#define Spell_EFFECT_OTHER                80
#define Spell_EFFECT_DESTROY_ALL_EFFECT   82
// --- Monster Special Attacks defined as spells ---
#define Spell_EFFECT_SA_ATTRIBUTE_LOST    90  // permanent is too strong. Would need a temporary to really use it.
#define Spell_EFFECT_SA_BREATH            91
//#define Spell_EFFECT_SA_GRAB              92 // includes constrict, swallow : use added_effect
#define Spell_EFFECT_SA_SOUL_DRAIN        92
#define Spell_EFFECT_SA_PHYSICAL          93
#define Spell_EFFECT_SA_CALL_HELP         94
#define Spell_EFFECT_SA_DRAIN_DICE        95
#define Spell_EFFECT_SA_DRAIN_FIXED        96



#define Spell_CB_PRINTF_NAME     1

/*#define Spell_PROPERTY_MAGIC_DEFENSE      0x0001 // apply magic defense (default)
#define Spell_PROPERTY_PHYSICAL_SAVE      0x0002 // apply phisical save
#define Spell_PROPERTY_MENTAL_SAVE        0x0004 // apply mental save
#define Spell_PROPERTY_DAMAGE_RESISTANCE  0x0008 // apply damage resistance (default)
#define Spell_PROPERTY_ACTIVE_DEFENSE     0x0010 // apply active defense to avoid effect
#define Spell_PROPERTY_HALF_VALUE         0x0100 // divide value above by 2*/


// ------------------------- Class Definition ------------------------------


class Spell : public EnhancedSQLobject
{
   // --- Properties ---

   public: const static int NB_FIELD = 16;
   private: static s_SQLfield p_SQLfield [ NB_FIELD ]; // NEW list the definition of all the fields in order
   private: s_SQLdata p_SQLdata [ NB_FIELD ]; // actual data which can be integer or string pointer

   // --- character strings ---

   private: char p_word [ Spell_WORD_STRLEN ];
   private: char p_name [ Spell_NAME_STRLEN ];
   private: char p_element [ Spell_ELEMENT_STRLEN ];
   private: char p_property [ Spell_PROPERTY_STRLEN ];
   private: char p_description [ Spell_DESCRIPTION_STRLEN ];

   /*
   private: int p_progress; // spell progress required to know the spell
   private: int p_mp_cost; // Mana Required to cast the spell
   private: int p_target; // Nb of people affected by the spell
   private: unsigned int p_castable; // indicate situation when it can be cast
   private: bool p_monster; // true if can be cast by monsters
   private: int p_proc_id; // Procedure to use to resolve spell (probably a procedure set since need AI proc too)
   private: int p_param [Spell_NB_PARAM]; // 1st param of the procedure
   private: unsigned int p_school; // Indicate to which school of magic it belongs to. 1 spell requires all school.
   private: int p_initiative; // Initiative modifier to add to the action.*/



   // --- Constructor & destructor ---

   public: Spell ( void );
   public: virtual ~Spell ( void );

   // --- Property Methods ---

   public: char *word ( void ) { return ( p_word ); }
   public: char *name ( void ) { return ( p_name ); }
   public: char *element_str ( void ) { return ( p_element ); }
   public: char *property_str ( void ) { return ( p_property ); }
   public: char *description ( void ) { return ( p_description ); }


   public: int level ( void ) { return ( get_int ( 2 ) ); }
   public: int MP_cost ( void ) { return ( get_int (3) ); }
   public: int target ( void ) { return ( get_int ( 4 ) ); }
   public: int castable ( void ) { return ( get_int (5) ); }
   public: int item ( void ) { return ( get_int (6)  ); }
   public: int school ( void ) { return ( get_int (7) ); }
   public: int element ( void ) { return ( get_int (8) ); }
   //public: int buff_group ( void ) { return ( get_int (9) ); }
   public: int defense ( void ) { return ( get_int (9) ); }
   public: int effect_id ( void ) { return ( get_int (11) ); }
   public: int param1 ( void ) { return ( get_int (12) ); }

   public: int param2 ( void ) { return ( get_int (13) ); }
   public: int param3 ( void ) { return ( get_int (14) ); }


   // --- methods ---

   public: int cast ( Action &action, Opponent *actor, Opponent *target );

   public: void cast_on_target ( Action &action, Opponent *actor, Opponent *target );
   public: int adjust_apply_damage ( Opponent *actor, Opponent *target, int totaldamage );
   //public: void apply_condition ( Opponent *target, int conditionID );

   public: int cast_attack_dice ( Opponent *actor, Opponent *target  );
   public: int cast_attack_fixed ( Opponent *actor, Opponent *target  );
   //public: int cast_attack_type_dice ( Opponent *actor, Opponent *target );
   //public: int cast_attack_type_fixed ( Opponent *actor, Opponent *target );
   public: void cast_drain_heal ( Opponent *actor, int HP );
   public: void cast_heal_dice ( Opponent *actor, Opponent *target);
   public: void cast_heal_fixed ( Opponent *actor, Opponent *target );
   //public: void cast_condition ( Opponent *actor, Opponent *target );
   public: void cast_condition_body ( Opponent *actor, Opponent *target );
   public: void cast_condition_random ( Opponent *actor, Opponent *target );
   public: void cast_cure ( Opponent *actor, Opponent *target );
   public: void cast_cure_body ( Opponent *actor, Opponent *target );
   public: void cast_active_effect ( Opponent *actor, Opponent *target );
   public: void cast_negative_active_effect ( Opponent *actor, Opponent *target );
   public: void cast_not_implemented ( Opponent *actor, Opponent *target );

   public: void special_attack_attribute_lost ( Opponent *actor, Opponent *target );
   public: int special_attack_breath ( Opponent *actor, Opponent *target );
   public: int special_attack_soul_drain ( Opponent *actor, Opponent *target );
   public: int special_attack_physical ( Opponent *actor, Opponent *target );
   public: void special_attack_call_help ( Opponent *actor, Opponent *target );


   public: int cast_utility ( void );

   public: int cast_utility_DUMAPIC ( void );
   public: int cast_utility_MALKAMA ( void );
   public: int cast_utility_MALOR ( void );

   public: int cast_utility_CALFO ( void );
   public: int cast_utility_ZILFE ( void );
   public: int cast_utility_LOKTOFEIT ( void );

   public: int cast_utility_DESTO ( void );
   public: int cast_utility_CALNOVA ( void );
   public: int cast_utility_CALDU ( void );

   public: int cast_utility_ELEMOS ( void );
   public: int cast_utility_NOBAIS ( void );
   public: int cast_utility_KATU ( void );

 //  public: void cast_not_implemented ( Opponent *actor, Opponent *target );

/*

#define Spell_EFFECT_BUFF                 30
#define Spell_EFFECT_BUFF_EXPEDITION      35
#define Spell_EFFECT_DEBUFF               40
#define Spell_EFFECT_CONDITION            50
#define Spell_EFFECT_CONDITION_RANDOM     52
#define Spell_EFFECT_UTILITY              70
#define Spell_EFFECT_OTHER                80


  */

   // --- callback procedures ---

   public: void callback_handler ( int index ); // returns SQL error

   public: void cb_printf_name ( void );


   // virtual method from SQL enchanced object
   protected: void virtual template_load_completion ( void );

};

/*----------------------------------------------------------------------*/
/*-                       Hash Table Definition                        -*/
/*----------------------------------------------------------------------*/


extern const char STR_SPELL_CURE [ 12 ] [ 15 ];



#endif // SPELL_H_INCLUDED
