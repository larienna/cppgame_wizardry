/***************************************************************************/
/*                                                                         */
/*                         W I N I M G L I S T . H                         */
/*                         Class Definitnon                                */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinImageList                                        */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting date : May 25th, 2013                                      */
/*                                                                         */
/*          Class who inherint from window to manage a window with a list  */
/*                                                                         */
/***************************************************************************/



#ifndef WINIMGLIST_H_INCLUDED
#define WINIMGLIST_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*-                       class definition                                -*/
/*-------------------------------------------------------------------------*/

class WinImageList : public Window
{
   // --- Property Methods ---

   private: ImageList *p_list; // listshow in the window
   //private: bool p_no_cancel; // no cancel command enabled in menu
   private: bool p_reset_cursor; // reset cursor at start for each show
   //private: short p_cursor; // last cursor position

   // --- Constructor & Destructor ---

   public: WinImageList ( ImageList &lst, short x_pos, short y_pos,
              /*bool no_cancel = false*/ bool reset_cursor = false, bool translucent = false //,
              /*bool sensible = false*/ );
//   public: ~WinList ( void );

   // --- Virtual Methods ---

   public: virtual void preshow ( void ); // this function does nothing
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

};



#endif // WINIMGLIST_H_INCLUDED
