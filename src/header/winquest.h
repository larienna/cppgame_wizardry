/***************************************************************************/
/*                                                                         */
/*                        W I N Q U E S T . H                              */
/*                         Class Definition                                */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinQuestion                                         */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 23rd, 2002                                 */
/*                                                                         */
/*          This class inherit from window to draw a simple question       */
/*     message on the screen.                                              */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                         Class Parameter                               -*/
/*-------------------------------------------------------------------------*/

#define WinQuestion_FONT   FNT_print
#define WinQuestion_COLOR  General_COLOR_TEXT

/*-------------------------------------------------------------------------*/
/*-                          Constants                                    -*/
/*-------------------------------------------------------------------------*/

#define WinQuestion_ANSWER_YES   0
#define WinQuestion_ANSWER_NO    1

#define WinQuestion_X   320
#define WinQuestion_Y   100

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class WinQuestion : public Window
{
   //--- Properties ---

   private: char p_question [ 241 ];
   private: short p_center_x;

   //--- Constructor & Destructor ---

//   public: WinQuestion ( string &question, short center_x = 320, short y_pos = 100 );
   public: WinQuestion ( const char *question, short center_x = WinQuestion_X, short y_pos = WinQuestion_Y, bool translucent = false  );

//   public: ~WinQuestion ( void );

   //--- Virtual Methods ---

   public: virtual void preshow ( void );
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

   //--- private method ---

   private: void construct ( const char *message, short center_x, short y_pos );
   private: int line_count ( void );
   private: int max_line_size ( void );

};
