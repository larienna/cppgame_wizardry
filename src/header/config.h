/***************************************************************************/
/*                                                                         */
/*                             C O N F I G . H                             */
/*                            Class definition                             */
/*                                                                         */
/*     Content : Class Config                                              */
/*     Programmer : Eric Pietrocupo                                        */
/*     Stating date : May 20th, 2002                                       */
/*                                                                         */
/*          This class contains the information, configuration procedures  */
/*     and the config screen engine to configure the parameters of the     */
/*     game.                                                               */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                              Class Parameter                          -*/
/*-------------------------------------------------------------------------*/

#define Config_NB_PER_CATEGORY   24
#define Config_NB_CATEGORY       8
#define Config_NB_MAX_CONFIG     Config_NB_PER_CATEGORY * Config_NB_CATEGORY
#define Config_NB_MUSIC_TABLE    10
//#define Config_NB_CONFIG      18

/*-------------------------------------------------------------------------*/
/*-                          Constants                                    -*/
/*-------------------------------------------------------------------------*/

//----------------- configuration identification values----------------------

#define Config_CATEGORY_APPEARANCE     0
#define Config_CATEGORY_INTERFACE      1
#define Config_CATEGORY_DIFFICULTY     2
#define Config_CATEGORY_SYSTEM         3
#define Config_CATEGORY_MUSIC          4
#define Config_CATEGORY_UNDEFINED1     5
#define Config_CATEGORY_UNDEFINED2     6
#define Config_CATEGORY_UNDEFINED3     7

#define Config_MENU_APPEARANCE         0
#define Config_MENU_INTERFACE          1
#define Config_MENU_DIFFICULTY         2
#define Config_MENU_SYSTEM             3
#define Config_MENU_MUSIC              4
#define Config_MENU_UNDEFINED1         5
#define Config_MENU_UNDEFINED2         6
#define Config_MENU_UNDEFINED3         7
#define Config_MENU_EXIT               -1

// Appearance (0-23)
#define Config_TEXTURE                0
#define Config_RENDERING              1
#define Config_SHADING                2
#define Config_TRANSPARENCY           3
#define Config_FLOOR_CEILING          4
#define Config_ANIMATION              5
#define Config_WINDOW_BORDER          6
#define Config_WINDOW_BACKGROUND      7
#define Config_WINDOW_TEXTURE         8
#define Config_WINDOW_COLOR           9
#define Config_WINDOW_TRANSLUCENCY    10
#define Config_ANTI_ALLIASING        11

// Interface (24-47)
#define Config_KEY_DISPLAY            24
#define Config_HELP_DISPLAY           25
#define Config_MUSIC_VOLUME           26
#define Config_SOUND_VOLUME           27
#define Config_BASIC_KEY              28
#define Config_DISPLAY_PARTY          29
#define Config_DISPLAY_SPELL          30
#define Config_DISPLAY_SPECIAL        31
#define Config_MENU_CASCADE           32
#define Config_COMBAT_LOG_SPEED       33
#define Config_DISPLAY_PARTY_FRAME    34
#define Config_DETAIL_COMBAT_ROLL     35

// Difficulty (48-71)

#define Config_MONSTER_OCCURENCE       48
#define Config_NB_MAX_MONSTER          49
#define Config_CHARACTER_POINTS        50
#define Config_STARTING_LEVEL          51
#define Config_HEALTH_STATUS           52 // Done but not tested
#define Config_STARTING_GOLD           53
#define Config_SOUL_RECOVERY           54
#define Config_MONSTER_HPMP            55
#define Config_ITEM_DROP               56 // not implemented. I think all item drops are not implemented. (need chest)
#define Config_EXP_REWARD              57
#define Config_GOLD_REWARD             58 // Done, not tested
#define Config_MARKET_RESTOCK          59
#define Config_CHEST_TRAP              60 // not implemented, chest not implementd
#define Config_MAZE_TRAP               61 // not implemented, not trap event yet.
#define Config_MAZE_AREA               62 // not implemented, no maze active effect
#define Config_CURSED_ITEM             63
#define Config_SPELL_NAME              64
#define Config_MARKET_RARITY           65
#define Config_LEVELUP_ATTRIBUTE       66
#define Config_WEAKRESIST_STRENGTH     67


// System (72-95)

#define Config_SYSTEM_RESOLUTION       72
#define Config_SYSTEM_MIDI             73

// Music (96-119)

#define Config_MUSIC_BASE              96
#define Config_MUSIC_INTRO             96
#define Config_MUSIC_CAMP              97
#define Config_MUSIC_CITY              98
#define Config_MUSIC_INN               99
#define Config_MUSIC_SHOP              100
#define Config_MUSIC_TAVERN            101
#define Config_MUSIC_TEMPLE            102
#define Config_MUSIC_TRAINING          103
#define Config_MUSIC_DEATH             104
#define Config_MUSIC_CREDITS           105

#define Config_MUSICTAB_INTRO             0
#define Config_MUSICTAB_CAMP              1
#define Config_MUSICTAB_CITY              2
#define Config_MUSICTAB_INN               3
#define Config_MUSICTAB_SHOP              4
#define Config_MUSICTAB_TAVERN            5
#define Config_MUSICTAB_TEMPLE            6
#define Config_MUSICTAB_TRAINING          7
#define Config_MUSICTAB_DEATH             8
#define Config_MUSICTAB_CREDITS           9

//------------------- configuaration values -------------------------
// Generic

#define Config_YES         0
#define Config_NO          1

// appearance
#define Config_TEX_YES     0
#define Config_TEX_NO      1

#define Config_REN_PTEX    0
#define Config_REN_ATEX    1

#define Config_SHD_YES     0
#define Config_SHD_NO      1

#define Config_TRS_YES     0
#define Config_TRS_NO      1

#define Config_FAC_YES     0
#define Config_FAC_NO      1

#define Config_ANI_YES      0
#define Config_ANI_NO       1

#define Config_WBD_THICK    0
#define Config_WBD_THIN     1
#define Config_WBD_DOUBLE   2
#define Config_WBD_FLAT     3
#define Config_WBD_NONE     4

#define Config_WBG_COLOR    0 // solid Color
#define Config_WBG_TRANS    1
#define Config_WBG_TEXTURE  2

#define Config_WTX_GRANITE  0
#define Config_WTX_FOREST   1
#define Config_WTX_WATER    2
#define Config_WTX_PURPLE   3
#define Config_WTX_EARTH    4

#define Config_WCO_BLACK    0
#define Config_WCO_GREEN    1
#define Config_WCO_BLUE     2
#define Config_WCO_PURPLE   3
#define Config_WCO_BROWN    4

#define Config_WTL_VLOW     0
#define Config_WTL_LOW      1
#define Config_WTL_MED      2
#define Config_WTL_HIGH     3
#define Config_WTL_VHIGH    4




// interface

#define Config_KEY_YES     0
#define Config_KEY_NO      1

#define Config_HLP_YES     0
#define Config_HLP_NO      1

#define Config_MUS_MUTE     0
#define Config_MUS_LOW      1
#define Config_MUS_MEDIUM   2
#define Config_MUS_HIGH     3
#define Config_MUS_MAX      4

#define Config_SND_MUTE     0
#define Config_SND_LOW      1
#define Config_SND_MEDIUM   2
#define Config_SND_HIGH     3
#define Config_SND_MAX      4

#define Config_BKY_AZ       0
#define Config_BKY_QW       1
#define Config_BKY_ZX       2

#define Config_PAR_YES      0
#define Config_PAR_NO       1

#define Config_SPL_YES      0
#define Config_SPL_NO       1

#define Config_SPC_YES      0
#define Config_SPC_NO       1

#define Config_CLS_PAUSE    0
#define Config_CLS_HALF     1
#define Config_CLS_1SECONDS 2
#define Config_CLS_2SECONDS 3
#define Config_CLS_4SECONDS 4


// Difficulty

#define Config_DIF_LOW     0
#define Config_DIF_NORMAL  1
#define Config_DIF_HIGH    2

#define Config_DIF_DISABLED   0
#define Config_DIF_ENABLED    1

// system

#define Config_SYS_AUTODETECT    0
#define Config_SYS_DIGIMID       1

// Music

// 0-2 = music track from wiz 1, 2, or 3
#define Config_MUS_RANDOM     3

/*-------------------------------------------------------------------------*/
/*-                    Structures and type definition                     -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Config_file
{
   int f_value [ Config_NB_MAX_CONFIG ];
}s_Config_file;

typedef struct s_Config_entry
{
   bool active;
   char text [ Option_TEXT_SIZE ];
   char elem [ 5 ] [ Option_ELEM_SIZE ];
   int category; // used to group options together.
   int default_value; // default value at creation

}s_Config_entry;

/*-------------------------------------------------------------------------*/
/*-                           Class Definition                            -*/
/*-------------------------------------------------------------------------*/

class Config
{
   /*--- Properties ---*/

   private: int p_value [ Config_NB_MAX_CONFIG ]; // contains the configuration values

   /*--- Constructor & Destructors ---*/

   public: Config ( void );
   public: ~Config ( void );

   /*--- Property methods ---*/

   public: void set ( int configID, int value );
   public: int get ( int configID );

   /*--- Methods ---*/

   public: void start ( bool in_game = false );
   public: void reset ( void );
   public: void initialise ( void );

   public: void load ( void );
   public: void save ( void );

   public: void SQLload ( void );
   public: void SQLsave ( void );

   public: void show_config_part ( int section, int readonly = false );

   public: int verify_if_default ( void );

   /*--- Private Methods ---*/

   private: void reset_config_part ( int section);

   private: void configure_volume ( void );
   private: void configure_key ( void );
   private: void configure_maze ( void );
   private: void configure_window ( void );

};



/*-------------------------------------------------------------------------*/
/*-                      Global Varaible Definition                       -*/
/*-------------------------------------------------------------------------*/

extern Config config;
extern s_Config_entry config_entry [ Config_NB_MAX_CONFIG];
extern const char STR_CONFIG_PART_TITLE [ Config_NB_CATEGORY][51];
extern const int Config_COMBAT_LOG_SPEED_TIME [ 5 ];
extern const int Config_NB_MAX_MONSTER_VALUE [ 5 ];
extern const int Config_TRANSLUCENCY_LEVEL [ 5 ];
extern const int Config_MUSIC_TABLE [ Config_NB_MUSIC_TABLE ] [ 3 ];
extern int Config_rndmusic [ Config_NB_MUSIC_TABLE ];
