/***************************************************************************/
/*                                                                         */
/*                               M A Z E . H                               */
/*                                                                         */
/*     Content : Class Maze                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : March 17th , 2002                                   */
/*                                                                         */
/*          This file contains the classe definition of the maze engine.   */
/*     This class will read the maze map data, draw the polygon, read      */
/*     the user input and react to the special events                      */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                          Constant Parameter                           -*/
/*-------------------------------------------------------------------------*/

#define Maze_MAXWIDTH             100
#define Maze_MAXDEPTH             20
#define Maze_NB_SAVEVALUE         256
#define Maze_NB_ALTERATION        256
//#define Maze_NB_WARP           8
//#define Maze_NB_EVENT         256 // runs on a unsigned char
#define Maze_NB_FILLING        16
#define Maze_NB_THICKWALL      12
#define Maze_NB_TEXPALETTE    32

// old code, might bot be valid
#define Maze_NB_MASKTEX       256 // runs on a unsigned char
#define Maze_NB_OBJECTIMG     256 // runs on a unsigned char
#define Maze_NB_TEXTURESET     64 // runs on a unsigned char
#define Maze_NB_GAME_MASKTEX   192
#define Maze_NB_ADV_MASKTEX    64
#define Maze_NB_GAME_TEXSET    48
#define Maze_NB_ADV_TEXSET     16
#define Maze_NB_GAME_OBJIMG    192
#define Maze_NB_ADV_OBJIMG     64


/*-------------------------------------------------------------------------*/
/*-                             Constant                                  -*/
/*-------------------------------------------------------------------------*/

// Alternate loading

#define Maze_DATATYPE_ADVENTURE  1

//                          Center > -----------+
//                            Left > --------+  |   +--------- < Right
//                        Far left > -----+  |  |   |  +----- < Far Right
//                       Away left > --+  |  |  |   |  |  +-- < Away Right
//                                     |  |  |  |   |  |  |
// s_Maze_wall id       Front 3 ->     -- -- -- -- -- -- --
//                      Side  3 ->       |  |  |  |  |  |
//                      Front 2 ->        -- -- -- -- --
//                      Side  2 ->          |  |  |  |
//                      Front 1 ->           -- -- --
//                      Side  1 ->             |  |

#define Maze_WALL_AWAY_LEFT_FRONT_3       0
#define Maze_WALL_AWAY_RIGHT_FRONT_3      1
#define Maze_WALL_FAR_LEFT_FRONT_3        2
#define Maze_WALL_FAR_RIGHT_FRONT_3       3
#define Maze_WALL_LEFT_FRONT_3            4
#define Maze_WALL_RIGHT_FRONT_3           5
#define Maze_WALL_CENTER_FRONT_3          6

#define Maze_WALL_AWAY_LEFT_SIDE_3        7
#define Maze_WALL_AWAY_RIGHT_SIDE_3       8
#define Maze_WALL_FAR_LEFT_SIDE_3         9
#define Maze_WALL_FAR_RIGHT_SIDE_3        10
#define Maze_WALL_LEFT_SIDE_3             11
#define Maze_WALL_RIGHT_SIDE_3            12

#define Maze_WALL_FAR_LEFT_FRONT_2        13
#define Maze_WALL_FAR_RIGHT_FRONT_2       14
#define Maze_WALL_LEFT_FRONT_2            15
#define Maze_WALL_RIGHT_FRONT_2           16
#define Maze_WALL_CENTER_FRONT_2          17

#define Maze_WALL_FAR_LEFT_SIDE_2         18
#define Maze_WALL_FAR_RIGHT_SIDE_2        19
#define Maze_WALL_LEFT_SIDE_2             20
#define Maze_WALL_RIGHT_SIDE_2            21

#define Maze_WALL_LEFT_FRONT_1            22
#define Maze_WALL_RIGHT_FRONT_1           23
#define Maze_WALL_CENTER_FRONT_1          24

#define Maze_WALL_LEFT_SIDE_1             25
#define Maze_WALL_RIGHT_SIDE_1            26

// floor constant

#define Maze_FLOOR_AWAY_LEFT_3             0
#define Maze_FLOOR_AWAY_RIGHT_3            1
#define Maze_FLOOR_FAR_LEFT_3              2
#define Maze_FLOOR_FAR_RIGHT_3             3
#define Maze_FLOOR_LEFT_3                  4
#define Maze_FLOOR_RIGHT_3                 5
#define Maze_FLOOR_CENTER_3                6

#define Maze_FLOOR_FAR_LEFT_2              7
#define Maze_FLOOR_FAR_RIGHT_2             8
#define Maze_FLOOR_LEFT_2                  9
#define Maze_FLOOR_RIGHT_2                10
#define Maze_FLOOR_CENTER_2               11

#define Maze_FLOOR_LEFT_1                 12
#define Maze_FLOOR_RIGHT_1                13
#define Maze_FLOOR_CENTER_1               14

// ceiling constants

#define Maze_CEILING_AWAY_LEFT_3             0
#define Maze_CEILING_AWAY_RIGHT_3            1
#define Maze_CEILING_FAR_LEFT_3              2
#define Maze_CEILING_FAR_RIGHT_3             3
#define Maze_CEILING_LEFT_3                  4
#define Maze_CEILING_RIGHT_3                 5
#define Maze_CEILING_CENTER_3                6

#define Maze_CEILING_FAR_LEFT_2              7
#define Maze_CEILING_FAR_RIGHT_2             8
#define Maze_CEILING_LEFT_2                  9
#define Maze_CEILING_RIGHT_2                10
#define Maze_CEILING_CENTER_2               11

#define Maze_CEILING_LEFT_1                 12
#define Maze_CEILING_RIGHT_1                13
#define Maze_CEILING_CENTER_1               14

// Maze.start interrupt return value

/*#define Maze_CONTINUE   0
#define Maze_EXIT       1 // value used to exit main loop
#define Maze_CAMP       2
#define Maze_COMBAT     3
#define Maze_ENDGAME    4 // not sure : maybe use Maze_EXIT
#define Maze_QUITPARTY  5*/

//?? use a function to get warp value
/*#define Maze_1ST_EXIT   92 // warp to another map
#define Maze_2ND_EXIT   93 // warp to another map
#define Maze_3RD_EXIT   94 // warp to another map
#define Maze_4TH_EXIT   95 // warp to another map
#define Maze_5TH_EXIT   96 // warp to another map
#define Maze_6TH_EXIT   97 // warp to another map
#define Maze_7TH_EXIT   98 // warp to another map
#define Maze_8TH_EXIT   99 // warp to another map
#define Maze_OTHER      100*/

#define Maze_BASE_EXIT 92 // add exitvalue to this number for real maze exit

// Maze.facing return value

#define Maze_FACE_NORTH 0
#define Maze_FACE_EAST  1
#define Maze_FACE_SOUTH 2
#define Maze_FACE_WEST  3

// special event type


/*
#define Maze_EVENT_NONE                0 //
#define Maze_EVENT_EXIT                1 // exitID
#define Maze_EVENT_UP                  2 //
#define Maze_EVENT_DOWN                3 //
#define Maze_EVENT_ELEVATOR            4 // min z, max z,
#define Maze_EVENT_CHUTE               5 // nb level
#define Maze_EVENT_DAMAGE_TRAP         6 // evadeness, dmg, unresist, nb_target
#define Maze_EVENT_EFFECT_TRAP         7 // evadeness, effect, modifier, nb target
#define Maze_EVENT_ROTATOR             8 //
#define Maze_EVENT_TELEPORTER          9 // target x, y, z, facing, no fx
#define Maze_EVENT_SPECIAL_COMBAT     10 // special combat ID
#define Maze_EVENT_NPC                11 // npc ID
#define Maze_EVENT_LOCK_DOOR           12 // need item to open
#define Maze_EVENT_SECRET_DOOR         13 // hidden passage
#define Maze_EVENT_MESSAGE             14 // draw a message
#define Maze_EVENT_CHEST               15 // treasure chest to open
*/
// event triggering types
/*
#define Maze_EVENT_TRIGGER_MASK        15 // 00001111

#define Maze_EVENT_TRIGGER_WALK        0  // 00000000
#define Maze_EVENT_TRIGGER_FACENORTH   1  // 00000001
#define Maze_EVENT_TRIGGER_FACEEAST    2  // 00000010
#define Maze_EVENT_TRIGGER_FACESOUTH   3  // 00000011
#define Maze_EVENT_TRIGGER_FACEWEST    4  // 00000100
#define Maze_EVENT_TRIGGER_SEARCH      5  // 00000101
#define Maze_EVENT_TRIGGER_SEARCHNORTH 6  // 00000110
#define Maze_EVENT_TRIGGER_SEARCHEAST  7  // 00000111
#define Maze_EVENT_TRIGGER_SEARCHSOUTH 8  // 00001000
#define Maze_EVENT_TRIGGER_SEARCHWEST  9  // 00001001

#define Maze_EVENT_TRIGICON_MASK       240 // 11110000

#define Maze_EVENT_TRIGICON_NONE       0   // 00000000
#define Maze_EVENT_TRIGICON_DOWN       16  // 00010000
#define Maze_EVENT_TRIGICON_UP         32  // 00100000
#define Maze_EVENT_TRIGICON_BOTH       48  // 00110000
#define Maze_EVENT_TRIGICON_NORTH      64  // 01000000
#define Maze_EVENT_TRIGICON_EAST       80  // 01010000
#define Maze_EVENT_TRIGICON_SOUTH      96  // 01100000
#define Maze_EVENT_TRIGICON_WEST       112 // 01110000
#define Maze_EVENT_TRIGICON_DOBJECT    128 // 10000000
*/
// texture placing for masked floor and ceiling MAze_tile.texture

/*#define Maze_FLOOR_MTEX_MASK           49152   // 1100000000000000b

#define Maze_FLOOR_MTEX_UP             16384   // 0100000000000000b
#define Maze_FLOOR_MTEX_DOWN           32768   // 1000000000000000b
#define Maze_FLOOR_MTEX_BOTH           49152   // 1100000000000000b*/

// texture placing for masked wall Maze_tile.texture

/*#define Maze_WALL_MTEX_MASK            15360 // 0011110000000000b

#define Maze_WALL_MTEX_NORTH            1024 // 0000010000000000b
#define Maze_WALL_MTEX_EAST             2048 // 0000100000000000b
#define Maze_WALL_MTEX_SOUTH            4096 // 0001000000000000b
#define Maze_WALL_MTEX_WEST             8192 // 0010000000000000b*/


// transcription object position value as bitwise for an easier drawing
#define Maze_9BIT_OBJ_NONE    0     // 000000000b
#define Maze_9BIT_OBJ_CENTER  16    // 000010000b
#define Maze_9BIT_OBJ_NORTH   128   // 010000000b
#define Maze_9BIT_OBJ_EAST    8     // 000001000b
#define Maze_9BIT_OBJ_SOUTH   2     // 000000010b
#define Maze_9BIT_OBJ_WEST    32    // 000100000b
#define Maze_9BIT_OBJ_TWINN   320   // 101000000b
#define Maze_9BIT_OBJ_TWINE   65    // 001000001b
#define Maze_9BIT_OBJ_TWINS   5     // 000000101b
#define Maze_9BIT_OBJ_TWINW   260   // 100000100b
#define Maze_9BIT_OBJ_4CORNER 325   // 101000101b
#define Maze_9BIT_OBJ_4SIDE   170   // 010101010b
#define Maze_9BIT_OBJ_SQUARE  495   // 111101111b
#define Maze_9BIT_OBJ_LOC13   0     // 000000000b
#define Maze_9BIT_OBJ_LOC14   0     // 000000000b
#define Maze_9BIT_OBJ_LOC15   0     // 000000000b

//mask to be used for texture set

//#define Maze_TEXSET_MASK      63       // 0000000000111111b


// event exit parameter : exit ID in value 0
#define Maze_EVEXIT_ID     0 // used as event.var [ index ]

#define Maze_EXIT_VALUE_1   0
#define Maze_EXIT_VALUE_2   1
#define Maze_EXIT_VALUE_3   2
#define Maze_EXIT_VALUE_4   3
#define Maze_EXIT_VALUE_5   4
#define Maze_EXIT_VALUE_6   5
#define Maze_EXIT_VALUE_7   6
#define Maze_EXIT_VALUE_8   7

// 4bit encoding, side location

#define Maze_4BIT_NORTH   1 // 00000001b
#define Maze_4BIT_EAST    2 // 00000010b
#define Maze_4BIT_SOUTH   4 // 00000100b
#define Maze_4BIT_WEST    8 // 00001000b

// 2bit encoding, side location

#define Maze_2BIT_NORTH   0
#define Maze_2BIT_EAST    1
#define Maze_2BIT_SOUTH   2
#define Maze_2BIT_WEST    3


// Light level setting     maze.light
// to be redifined
#define Maze_LIGHT_LEVEL_0    0
#define Maze_LIGHT_LEVEL_1    1
#define Maze_LIGHT_LEVEL_2    2
#define Maze_LIGHT_LEVEL_3    3

// Display switch
#define Maze_DISPLAY_LAST_ITEM   8

#define Maze_DISPLAY_PARTY       1
#define Maze_DISPLAY_SPELL       2
#define Maze_DISPLAY_SPECIAL     4 // maze icons

// Alteration changes to make

#define Maze_ALTERATE_SOLID      1
#define Maze_ALTERATE_WALLTEX    2
#define Maze_ALTERATE_FLOORTEX   4
#define Maze_ALTERATE_OBJECTIMG  8
#define Maze_ALTERATE_EVENT      16
#define Maze_ALTERATE_SPECIAL    32
#define Maze_ALTERATE_TEXTURE    64

// Maze type

/*#define Maze_TYPE_CAVE     1
#define Maze_TYPE_KEEP     2
#define Maze_TYPE_RUIN     3
#define Maze_TYPE_TEMPLE   4
#define Maze_TYPE_TOWER    5*/


//--- Prefixed texture

#define Maze_TEX_SMOKE  6     // maybe use 1024 + 6
#define Maze_IMG_SKYDITHER       0
#define Maze_IMG_HORIZONDITHER   1

/*-------------------------------------------------------------------------*/
/*-                           Type Definition                             -*/
/*-------------------------------------------------------------------------*/


/*
typedef struct s_Maze_event
{
   unsigned char type; // type of event ot be triggered
   unsigned char trigger; // type of triggering type of event and icon type
   char title [ 31 ]; // title string to draw on the top of the screen
   char message [ 201 ]; // message to use by the event
   unsigned char text; // additional text to use for the event
   short var [ 4 ]; // information relative to special space type
   unsigned char savevalID; // save value to use for memorising maze alterations
}s_Maze_event;

typedef struct dbs_Maze_event
{
   unsigned char type;//  ; // type of event ot be triggered
   unsigned char trigger;//  ; // type of triggering type of event and icon type
   short var [ 4 ];//  ; // information relative to special space type
   unsigned char savevalID;//  ; // save value to use for memorising maze alterations
   unsigned char text;//  ; // additional text to use for the event
   char title [ 31 ];//  ; // title string to draw on the top of the screen
   char message [ 201 ];//  ; // message to use by the event
}  dbs_Maze_event;
*/

/*typedef struct s_Maze_alteration
{
   short xpos;
   short ypos;
   short zpos;
   unsigned char savevalueID; // save value to read for enabling
   unsigned char change;
   s_mazetile info; // info on how to make change
}s_Maze_alteration;*/

// giga structure that contains all the maze's data.
/*typedef struct s_Maze_data
{
   string title; // name of the maze
   string shortname;
   short xpos;
   short ypos;
   int mapID;
   int warp [ Maze_NB_WARP ];
   s_mazetile tile [ Maze_WIDTH ] [ Maze_WIDTH ] [ Maze_DEPTH ];
   s_Maze_event event [ Maze_NB_EVENT ] [ Maze_DEPTH ];
   BITMAP *texset [ Maze_NB_TEXTURESET ] [ 4 ];
   BITMAP *masktex [ Maze_NB_MASKTEX ];
   BITMAP *objimg [ Maze_NB_OBJECTIMG ];
}s*_Maze_data;*/

typedef struct s_Maze_object_info
{
   short x;       //old variables
   short y;       //old variables
   short width;   //old variables
   short height;  //old variables
}s_Maze_object_info;

typedef struct s_Maze_object_anchor
{
   int anchor_x;  // center X position to anchor
   int anchor_y;  // center Y position to anchor
   int scale;   // scaling proportion, premultiplied by 100 (so 150 = 1.5)
   // note: the anchor point change if it's a ground, ceiling or wall object
   // ceiling ( y = top), floor ( y= bottom), wall (y = center), x always = center.
}s_Maze_object_anchor;

typedef struct s_Maze_wall_info
{
   V3D vertex [ 4 ];
   bool flipok; // indicates if this wall can be flipped
   int thickwallID; // thickwall id to use with wall, -1 = not thick wall
   int ltile; // ID of the tile refered to in the table
   int lvtex [ 4 ]; // indicates which light vertex to use in the tile
}s_Maze_wall_info;

typedef struct s_Maze_floor_info
{
   V3D vertex [ 4 ];
   int ltile; // ID of the tile refered to in the table
   // light vertex are always 0, 1, 2, 3
}s_Maze_floor_info;

typedef struct s_Maze_light_info
{
   unsigned char vtex [ 5 ]; // light distance from 0 to 255
   int tablex; // x pos : in the dynamic light table
   int tabley; // y pos : in the dynamic light table
}s_Maze_light_info;

// structure used to contain special wall polygon drawing parameters
typedef struct s_Maze_swall_info
{
   short red; // color of the special wall
   short green;
   short blue;
   short alpha; // alpha level of the special wall
   int drawmode; // draw mode of the special wall
   bool smoke; // use smoke in front of screen
}s_Maze_swall_info;

typedef struct s_Maze_xyoffset
{
   int xoff;
   int yoff;
}s_Maze_xyoffset;

typedef struct s_Maze_draw_test
{
   // these bitwise values match correctly with the maze data tiles
   int wall [ 3 ]; // wall 4 bit encoding, identify wall to test
   int door [ 3 ]; // door 4 bit encoding, identify door to test
   int grid [ 3 ]; // door 4 bit encoding, identify door to test
   unsigned short object [ 16 ]; // contains object location 9 bit encoding
   s_Maze_xyoffset offset [ 15 ]; // contains drawinf information for each facing
   int ftex_rotation; // the number of times the floor texture must be rotated
        // according to facing only
}s_Maze_draw_test;

typedef struct s_Maze_draw_info
{
   s_Maze_draw_test test [ 4 ]; // information required for testing tiles
   int tested_wallID [ 15 ] [ 3 ]; // the ID# of the corresponding wall if testing is true
   int trans_wallID [ 15 ] [ 2 ]; // wall to draw for transparency
   // there is 15 tiles to test with a maximum of 3 wals for each
   //notes : floor draw and object loc list is in index 0-14 drawing order
}s_Maze_draw_info;

/*typedef struct dbs_Maze_warp
{
   int target;// ;
   unsigned int tag;// ;
}  dbs_Maze_warp;
*/

// don't know that this struct does
/*typedef struct s_Maze_warp
{
   int target;//  ;
   int tag;//  ;
}  s_Maze_warp;*/


/*typedef struct dbs_ADV_Maze_data
{
   char name [ 31 ];//  ;
   char shortname [ 11 ];//  ;
   int type;//  ;
   short xpos;//  ;
   short ypos;//  ;
   int mapID;//  ;
   int music;//  ;
   int eprob;//  ;
   int ebase;//  ;
   int erange;//  ;
   int einc;//  ;
   //int warp [ Maze_NB_WARP ];//  ;
//   int target [ Maze_NB_WARP ]  ;
}  dbs_ADV_Maze_data;

typedef struct dbs_Maze_data
{
   char name [ 31 ];//  ;
   char shortname [ 11 ];//  ;
   int type;//  ;
   short xpos;//  ;
   short ypos;//  ;
   int mapID;//  ;
   int music;//  ;
   int eprob;//  ;
   int ebase;//  ;
   int erange;//  ;
   int einc;//  ;
   //dbs_Maze_warp warp [ Maze_NB_WARP ];//  ;
//   unsigned int warp [ Maze_NB_WARP ]  ;
//   int target [ Maze_NB_WARP ]  ;
   short savevalue [ Maze_NB_SAVEVALUE ];//  ;
}  dbs_Maze_data;*/

/*-------------------------------------------------------------------------*/
/*-                          Class Definition                             -*/
/*-------------------------------------------------------------------------*/

class Maze
{

   // Properties

   // maze data ( need to be loaded or saved )
      // adventure info structure
   private: char p_name [ 31 ];
   private: char p_shortname [ 11 ];
   private: int p_type;
   private: short p_xpos;
   private: short p_ypos;
   private: int p_mapID;
   private: int p_music;
   private: int p_eprob; // encounter probability
   private: int p_ebase; // encounter base level
   private: int p_erange; // encounter level range
   private: int p_einc; // encounter level increment
   //private: s_Maze_warp p_warp [ Maze_NB_WARP ];

   //private: short p_savevalue [ Maze_NB_SAVEVALUE ];

   // loaded from database
   //s_Maze_event p_event [ Maze_NB_EVENT ];
   //s_Maze_alteration p_alteration [ Maze_NB_ALTERATION ];

   // data loaded from external file
   //private: int p_width;
   //private: int p_depth;
   private: int p_sky; // is -1, no sky on first level
   private: int p_rclevel; // identify the floor which is the RC(Ground FLoor), above is floor, below is basement )
   //private: s_Party_position p_startpos;
   private: BITMAP* p_skybitmap; // preblit sky bitmap with dithering included
   private: BITMAP* p_smokebitmap; // preblit smoke

   // maze control properties ( no need to save )
   private: unsigned char p_darkness; // substracted to the light level
   private: int p_light; // logical light value
   private: bool p_transparency; // flag to set or remove transparency
   private: bool p_adjust_color; // must change the color for no tex walls
   private: short p_polytype; // wall drawing method used
   private: short p_mask_polytype; // mask polygon drawing method
   private: unsigned char p_display; // display switch for party, and status icons
   private: int p_light_table [ 5 ] [ 9 ] [ 5 ]; // dynamic light vertex
      //I have little clue on how p_light_table works.
      //I seems to create a temporary light table used by draign routines
      //afterward

   //private: Party p_party; // party currently played

   // Constructor & Destructor

   public: Maze ( void );
   public: ~Maze ( void );

   // Property Methods

//   public: string& name ( void );
   public: const char* name ( void );
//   public: string& shortname ( void );
   public: const char* shortname ( void );
   public: short xpos ( void );
   public: short ypos ( void );
   public: int type ( void );
   public: int mapID ( void );
//   public: int warp ( int index );
//   public: unsigned int target ( int index );
   public: int eprob ( void );
   public: int ebase ( void );
   public: int erange ( void );
   public: int einc ( void );
   //public: int exitwarptag ( int exitval );


   public: void polytype ( short type );
   public: short polytype ( void );
   public: void mask_polytype ( short type );
   public: short mask_polytype ( void );
   public: unsigned char display ( void );
   public: void display ( unsigned char value );
   public: void transparency ( bool value );
   public: bool transparency ( void );
   public: void adjust_color ( bool value );
   public: bool adjust_color ( void );
   //public: s_Party_position start_position ( void );

   // Methods
   public: void start ( bool demo = false  );
   public: s_Party_position show_entrance ( void );
   public: void light ( int value );
   public: void reference_bitmap ( void );
   public: void reference_adv_bitmap ( void );
   //public: void reference_target ( int citytaglist [ 16 ], int mazetaglist [ 16 ] );
   //public: void load ( void );
   //public: void load_hardcoded (void);
   public: void load_hardcoded_texture (void);
   public: bool save_to_mazefile ( const char* filename );
   public: bool load_from_mazefile ( const char* filename );
   public: bool load_from_adventure ( const char* adventurefile );


   // maze drawing
   private: void draw_wall ( s_Maze_wall_info &wallinfo, BITMAP *texture, bool flipodd );
   private: void draw_masked_wall ( s_Maze_wall_info &wallinfo, BITMAP *texture );
   private: void draw_thickness ( s_Maze_wall_info &wallinfo, BITMAP *texture );
   private: void draw_transparent_wall ( s_Maze_wall_info &wallinfo , unsigned char type, int wallID, bool heavy );

   private: void draw_floor ( s_Maze_floor_info &floorinfo, BITMAP *texture, int nb_rotation );
   private: void draw_masked_floor ( s_Maze_floor_info &floorinfo, BITMAP *texture, int nb_rotation );
   private: void draw_screen ( void );
   private: void draw_sky ( void );
   public: void build_sky_bitmap ( void );
   public: void build_smoke_bitmap ( void );

   //private: void draw_object ( s_Maze_object_info &objinfo, BITMAP *image, unsigned char light );

   private: void draw_floor_object ( int tile, int objectID, int picID );
   private: void draw_ceiling_object ( int tile, int objectID, int picID );
   private: void draw_wall_object ( int tile, int objectID, int picID );
   private: void draw_anchored_object ( BITMAP *image, int x, int y, int w, int h, int light );
      // do the common part of all object drawing methods

   private: void check_palette ( void );

   public: void draw_maze ( bool demo = false );

   //private: bool execute_event ( void );
   private: void evaluate_light ( void );
   private: void draw_maze_icons ( s_mazetile tmptile );
//   private: bool test_front_wall ( unsigned char solid );
//   private: bool test_front_door ( unsigned char solid );



   // virtual functions

   /*public: virtual void objdat_to_strdat ( void *dataptr );
   public: virtual void strdat_to_objdat ( void *dataptr );
   public: virtual void child_DBremove ( void );

   public: virtual void alternate_strdat_to_objdat ( int datatypeID, void *dataptr );
*/
   //Private Method


};

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

extern Maze maze; // maze engine ?? maybe place in main
//                  coordonates      Z               Y                 X
extern s_mazetile mazetile [ Maze_MAXDEPTH ] [ Maze_MAXWIDTH ] [ Maze_MAXWIDTH ];

// Images reference ( no need to save )
//extern BITMAP *p_texset [ Maze_NB_TEXTURESET ] [ 4 ];
//extern BITMAP *p_masktex [ Maze_NB_MASKTEX ];
//extern BITMAP *p_objimg [ Maze_NB_OBJECTIMG ];


//?? wall should be a struct with flip information
extern s_Maze_wall_info WALL [ 27 ]; // maze wall polygon vertex
extern s_Maze_wall_info THICKWALL [ Maze_NB_THICKWALL ]; // thickness of a wall

extern V3D SCREEN [ 4 ]; // transparent special screen tint
extern s_Maze_floor_info FLOOR [ 15 ]; // maze floor data
extern s_Maze_floor_info CEILING [ 15 ]; // maze ceiling data
extern s_Maze_object_info OBJECT [ 15 ] [ 9 ]; // maze object position coordonates
extern s_Maze_object_anchor FLOOR_OBJECT [ 15 ] [ 9 ]; // Anchor points for floor objects
extern s_Maze_object_anchor CEILING_OBJECT [ 15 ] [ 9 ]; // Anchoir points for ceiling objects
// add wall objects anchor points
extern const s_Maze_light_info LIGHT_DISTANCE [ 15 ]; // 4 corner + center light listance 0-255
extern const bool LIGHT_VISION [ 4 ] [ 15 ]; // true if tile is drawn

// wall drawing test order
//extern const s_Maze_test_order Maze_TEST [ 4 ] [ 27 ];
//extern s_Maze_test_swall_order Maze_TEST_SWALL [ 4 ] [ 22 ];
//extern s_Maze_test_space_order Maze_TEST_SPACE [ 4 ] [ 15 ];
extern s_Maze_swall_info Maze_SWALL_INFO [ Maze_NB_FILLING ];

extern const s_Maze_draw_info Maze_DRAW_INFO;

/*-------------------------------------------------------------------------*/
/*-                        Prototypes                                     -*/
/*-------------------------------------------------------------------------*/



