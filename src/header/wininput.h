/***************************************************************************/
/*                                                                         */
/*                         W I N I N P U T . H                             */
/*                         Class Definition                                */
/*                                                                         */
/*     Content : class WinInput                                            */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : July 23rd, 2003                                     */
/*     License : GNU General Public LIcense                                */
/*     Engine : Wizardry window engine                                     */
/*                                                                         */
/*          This class allows to manage text input windows.                */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                         Class Parameter                               -*/
/*-------------------------------------------------------------------------*/

#define WinInput_FONT      FNT_print
#define WinInput_INPUTFONT FNT_script
#define WinInput_COLOR     General_COLOR_TEXT

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class WinInput : public Window
{
   // --- Properties ---

   private: char p_textstring [ 81 ]; // string question drawn in the window
   private: char p_inputstr [ 128 ];  // string containg the input text.
   private: int p_nb_character; // maximum number of character to input

   // --- Cosntructor and Destructor ---

//   public: WinInput ( string &text, int nb_char, short x_pos, short y_pos );
   public: WinInput ( const char *text, int nb_char, short x_pos, short y_pos, bool translucent = false  );
//   public: ~WinInput ( void );

   // --- methods ---

   public: void get_string ( char *copystr );
   public: const char *get_string ( void );

   // --- Virtual Methods ---

   public: virtual void preshow ( void );
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

   //--- privvate methods ---

   private: void construct ( const char *text, int nb_char, short x_pos, short y_pos );

};














