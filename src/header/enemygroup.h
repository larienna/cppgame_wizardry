//----------------------------------------------------------------------
//
//                 E N E M Y G R O U P . H
//
//     Class: EnnemyGroup
//     Programmer: Eric Pietrocupo
//     Start Date: February 2014
//
//     Manage groups of ennemies to encounter in the maze
//
//-----------------------------------------------------------------------


#ifndef ENEMYGROUP_H_INCLUDED
#define ENEMYGROUP_H_INCLUDED

//-----------------------------------------------------------------------
//                          Constants
//-----------------------------------------------------------------------

#define EnemyGroup_NB_ENEMY   8
#define EnemyGroup_NB_PROBABILITY 5

//--- Probability type ---


#define EnemyGroup_PROB_RANDOM      0 // 50% each except first is 100%
#define EnemyGroup_PROB_FIXED       1 // all monsters appear in the list
#define EnemyGroup_PROB_DECREMENTAL 2 // High to low Probabilities
#define EnemyGroup_PROB_LOW         3 // 25% each except first
#define EnemyGroup_PROB_HIGH        4 // 75% each except first


//-----------------------------------------------------------------------
//                      Structure Definition
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
//                        Class Definition
//-----------------------------------------------------------------------

class EnemyGroup : public SQLobject
{
   // Properties

   private: int p_floor;
   private: int p_area;
   private: int p_repeat;
   private: int p_bonus;
   private: unsigned int p_bonus_target;
   private: int p_probability;
   private: int p_enemy [ EnemyGroup_NB_ENEMY ];


   // Constructors and Destructors

   public: EnemyGroup ( void );
   public: ~EnemyGroup ( void );

   // Property Methods

   public: int floor ( void ) { return ( p_floor); }
   public: void floor ( int value ) { p_floor = value; }
   public: int area ( void ) { return ( p_area ); }
   public: void area ( int value ) { p_area = value; }
   public: int repeat ( void ) { return ( p_repeat ); }
   public: void repeat ( int value ) { p_repeat = value; }
   public: int bonus ( void ) { return ( p_bonus ); }
   public: void bonus ( int value ) { p_bonus = value; }
   public: int bonus_target ( void ) { return ( p_bonus_target ); }
   public: void bonus_target ( int value ) { p_bonus_target = value; }
   public: int probability ( void ) { return ( p_probability ); }
   public: void probability ( int value ) { p_probability = value; }
   public: int enemy ( int index ) { return ( p_enemy [ index ] ); }
   public: void enemy ( int index, int value ) { p_enemy [ index ] = value; }

   // Methods

   // Virtual Methods

   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

};

//---------------------------------------------------------------------------
//                        Global Variables
//--------------------------------------------------------------------------

extern int EnemyGroup_PROBABILITY [ EnemyGroup_NB_PROBABILITY ] [ EnemyGroup_NB_ENEMY ];


#endif // ENEMYGROUP_H_INCLUDED
