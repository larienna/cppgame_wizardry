/**
* m a z e t i l e . h
*
* @author Eric Pietrocupo
* @date   February 11th, 2018
*
* In order to avoid bugs and unwanted issues with the maze tile system, I am using
* a C style module to centralise all read and write operations at the same place to
* make sure they are beign done properly. In order to optimise space and speed,
* classes cannot be used, so the tile structure is passed to each call.
*/

#ifndef MAZETILE_H_INCLUDED
#define MAZETILE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

// New data structure mask and bitfield

// --- byte 1 Solid ---

#define MAZETILE_SOLIDTYPE_NONE     0  // 00
#define MAZETILE_SOLIDTYPE_WALL     1  // 01
#define MAZETILE_SOLIDTYPE_GRID     2  // 10
#define MAZETILE_SOLIDTYPE_DOOR     3  // 11

#define MAZETILE_SOLID_NORTH_WALL   64  //  01000000
#define MAZETILE_SOLID_NORTH_GRID   128 //  10000000
#define MAZETILE_SOLID_NORTH_DOOR   192 //  11000000
#define MAZETILE_SOLID_EAST_WALL    16  //  00010000
#define MAZETILE_SOLID_EAST_GRID    32  //  00100000
#define MAZETILE_SOLID_EAST_DOOR    48  //  00110000
#define MAZETILE_SOLID_SOUTH_WALL   4   //  00000100
#define MAZETILE_SOLID_SOUTH_GRID   8   //  00001000
#define MAZETILE_SOLID_SOUTH_DOOR   12  //  00001100
#define MAZETILE_SOLID_WEST_WALL    1   //  00000001
#define MAZETILE_SOLID_WEST_GRID    2   //  00000010
#define MAZETILE_SOLID_WEST_DOOR    3   //  00000011

#define MAZETILE_SOLID_NORTH_MASK   192 //  11000000
#define MAZETILE_SOLID_EAST_MASK     48 //  00110000
#define MAZETILE_SOLID_SOUTH_MASK    12 //  00001100
#define MAZETILE_SOLID_WEST_MASK      3 //  00000011

// shortcut for global

#define WNORTH    64
#define GNORTH    128
#define DNORTH    192
#define WEAST     16
#define GEAST     32
#define DEAST     48
#define WSOUTH    4
#define GSOUTH    8
#define DSOUTH    12
#define WWEST     1
#define GWEST     2
#define DWEST     3

// --- byte 2 Event ---

#define MAZETILE_NO_EVENT   0   // use as index in tile.event for no event

// --- byte 3 Special ---

// NOTE: maybe the 5th bit could be the sky, to allow partially covered and uncovered areas.
// still could not make sense to have flat roof, unless create thin roof.

// value wise (^_^) ( 4 right bits )
#define MAZETILE_FILLING_MASK       15 // 00001111b

#define MAZETILE_FILLING_NONE            0 // 00000000b
#define MAZETILE_FILLING_WATER           1 // 00000001b
#define MAZETILE_FILLING_FIZZLE          2 // 00000010b
#define MAZETILE_FILLING_FOG             3 // 00000011b
#define MAZETILE_FILLING_POISON_GAS      4 // 00000100b
#define MAZETILE_FILLING_DARKNESS        9 // 00001001b

#define MAZETILE_SPECIAL_MASK     0b11110000 // 11110000b

#define MAZETILE_SPECIAL_MAGIKBOUNCE    0b00010000 // 00010000b
#define MAZETILE_SPECIAL_SOLID          0b00100000 // 00100000b
#define MAZETILE_SPECIAL_LIGHT          0b01000000 // 01000000b
#define MAZETILE_SPECIAL_UNKNOWN        0b10000000 // 10000000b
#define MAZETILE_SPECIAL_ALL            0b11110000

// --- byte 4  floor and ceiling object picture ---

#define MAZETILE_FLOOROBJIMG_MASK    0b11110000
#define MAZETILE_FLOOROBJIMG_SHIFT   4
#define MAZETILE_CEILINGOBJIMG_MASK    0b00001111
#define MAZETILE_CEILINGOBJIMG_SHIFT   0

// --- byte 5 palette and wall object picture ---

#define MAZETILE_PALETTE_MASK      0b00011111
#define MAZETILE_PALETTE_SHIFT     0
#define MAZETILE_WALLOBJIMG_MASK      0b11100000
#define MAZETILE_WALLOBJIMG_SHIFT     5

// --- Byte 6 : Object placement ---

//TODO: See if there can be a way to recuperate a bit by making a texture of 0 do nothing.
//Instead of the positionning bit. Still it should be easier to implemnt this way.

// object placement
// there is 9 possible position for placing object on a tile
//
//             1  2  3
//             4  5  6
//             7  8  9
// valid for wall, ceiling and floor
                                                // Locations
#define MAZETILE_WALLOBJ_MASK       0b11000000
#define MAZETILE_WALLOBJ_SHIFT      6
#define MAZETILE_WALLOBJ_NONE       0
#define MAZETILE_WALLOBJ_CENTER     1<<6        // 5
#define MAZETILE_WALLOBJ_PAIRTOP    2<<6        // 1,3
#define MAZETILE_WALLOBJ_PAIRCENTER 3<<6        // 4,6

#define MAZETILE_CEILINGOBJ_MASK    0b00110000
#define MAZETILE_CEILINGOBJ_SHIFT   4
#define MAZETILE_CEILINGOBJ_NONE    0
#define MAZETILE_CEILINGOBJ_CENTER  1<<4        // 5
#define MAZETILE_CEILINGOBJ_4CORNER 2<<4        // 1,3,7,9
#define MAZETILE_CEILINGOBJ_4SIDES  3<<4        // 2,4,6,8
                                              // locations
#define MAZETILE_FLOOROBJ_MASK    0b00001111  //
#define MAZETILE_FLOOROBJ_SHIFT   0
#define MAZETILE_FLOOROBJ_NONE    0           //
#define MAZETILE_FLOOROBJ_CENTER  1           // 5
#define MAZETILE_FLOOROBJ_NORTH   2           // 2
#define MAZETILE_FLOOROBJ_EAST    3           // 6
#define MAZETILE_FLOOROBJ_SOUTH   4           // 8
#define MAZETILE_FLOOROBJ_WEST    5           // 4
#define MAZETILE_FLOOROBJ_TWINN   6           // 1,3
#define MAZETILE_FLOOROBJ_TWINE   7           // 3,9
#define MAZETILE_FLOOROBJ_TWINS   8           // 9,7
#define MAZETILE_FLOOROBJ_TWINW   9           // 1,7
#define MAZETILE_FLOOROBJ_4CORNER 10          // 1,3,7,9
#define MAZETILE_FLOOROBJ_4SIDE   11          // 2,4,6,8
#define MAZETILE_FLOOROBJ_SQUARE  12          // all except 5
#define MAZETILE_FLOOROBJ_LOC13   13          // ?
#define MAZETILE_FLOOROBJ_LOC14   14          // ?
#define MAZETILE_FLOOROBJ_LOC15   15          // ?
//TODO: idea, put stuff into corners, like 1-2-4 and 6-9-8

// --- Byte 7 masked texture id---

#define MAZETILE_MASKTEX_FLOOR_MASK  0b11110000 //240 // 11110000
#define MAZETILE_MASKTEX_FLOOR_SHIFT 4
#define MAZETILE_MASKTEX_WALL_MASK   0b00001111 //15  // 00001111
#define MAZETILE_MASKTEX_WALL_SHIFT  0

// --- byte 8 masked texture position and grid texture id ---

#define MAZETILE_MASKTEXPOS_FLOOR      0b10000000   // 128
#define MAZETILE_MASKTEXPOS_CEILING    0b01000000   // 64
#define MAZETILE_MASKTEXPOS_NORTH      0b00000100   // 4
#define MAZETILE_MASKTEXPOS_EAST       0b00001000   // 8
#define MAZETILE_MASKTEXPOS_SOUTH      0b00010000   //
#define MAZETILE_MASKTEXPOS_WEST       0b00100000   //

#define MAZETILE_MASKPOS_WALL_MASK  0b00111100 // 60 // 00111100
#define MAZETILE_MASKPOS_FLOOR_MASK 0b11000000 // 192 // 11000000
#define MAZETILE_MASKPOS_GRID_MASK  0b00000011   // 3

// --- Structure ---

// contains the data of each tile of the maze
typedef struct s_mazetile
{
   unsigned char solid;   // 4 group of 2 bits
   unsigned char event; // event ID for the floor ( 0 = no event )
   unsigned char special; // 4 bit technical info ; 4 bit filling info
   unsigned char objectpic; //* 4 bit Floor Object, 4 bit ceiling object from palette
   unsigned char wobjpalette; //* 3 bit wall object + 5 bit palette ID
   unsigned char objposition; //* 2 bit wall + 2 bit ceiling + 4 bit floor
   unsigned char masked; //* 4 bit Floor Ceiling texture + 4 bit wall texture
   unsigned char maskposition; //* 2 bit floor ceiling + 4 bit wall position + 2 bit grid tex
}s_mazetile;

// --- Methods ---
//Those are basically getters and setters to interact with the mazetile. They are grouped
//for each byte to modify, A pointer on the whole structure is used to prevent the need to
//extract the information from the structure before passing the parameter. Also avoids
//reaffecting the value to the structure and it could allow methods that modifies multiple
//fields.

/** Return a structure with empty field but with solid state set*/
s_mazetile new_mazetile_empty ( void );
void set_mazetile_empty ( s_mazetile *tile );

//---  byte 1 ---

/** Set the specified wall type on the right side*/
void set_mazetile_wall ( s_mazetile *tile, int side, int type );
/** Return true if the indicated wall is made of the indicated type*/
bool is_mazetile_wall_oftype ( s_mazetile tile, int side, int type );

/** Return the solid information of the indicated direction*/
unsigned char get_mazetile_wall ( s_mazetile tile, int side );

//--- byte 3 ---

void set_mazetile_special ( s_mazetile *tile, int flag );
void unset_mazetile_special ( s_mazetile *tile, int flag );
bool is_mazetile_special ( s_mazetile tile, int flag );
void set_mazetile_filling ( s_mazetile *tile, int filling );
int get_mazetile_filling ( s_mazetile tile );


//--- byte 5 ---

void set_mazetile_palette ( s_mazetile *tile, int paletteid );

#ifdef __cplusplus
}
#endif



#endif // MAZETILE_H_INCLUDED
