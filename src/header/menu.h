/***************************************************************************/
/*                                                                         */
/*                               M E N U . H                               */
/*                                                                         */
/*     Content : Class Menu                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : March 14th, 2002                                    */
/*                                                                         */
/*          This is the header of the menu class which is used to manage   */
/*     different menus used during the game. A class has been made to make */
/*     sure it is reused during the game.                                  */
/*                                                                         */
/*     Requirements : Allegro Video                                        */
/*                    Allegro Text                                         */
/*                    Allegro Keyboard                                     */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                       Constant Class Parameter                        -*/
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/*-                       Constant Parameter                              -*/
/*-------------------------------------------------------------------------*/

#define Menu_FONT                FNT_print
#define Menu_HEADER_FONT         FNT_small
#define Menu_COLOR_TEXT          makecol ( 225, 225, 225 )
#define Menu_COLOR_MASK          makecol ( 150, 150, 150 )
#define Menu_STR_SIZE            81
#define Menu_HEADER_STR_SIZE     121
#define Menu_HEADER_SPACE        2


/*-------------------------------------------------------------------------*/
/*-                          Type definition                              -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Menu_item
{
   char text [ Menu_STR_SIZE ]; // Text written for the menu item
   bool mask;        // mask menuitem if TRUE.
   int answer;       // answer returned when selected. Can contain primary key of fields
   s_Menu_item *ptr_next; // pointer on the nextitem in the list
}s_Menu_item;

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class Menu
{

   // Properties

   private: char p_title [Menu_STR_SIZE]; // Text written at the top of the menu
   private: int  p_nb_item; // count number of add items
//   private: FONT *p_font;   // Font used to draw the menu
   private: s_Menu_item *ptr_first; // Point on the 1st menu item
   private: s_Menu_item *ptr_last; // Point on the last menu item
   private: bool p_showheader; // determine if a header must be displayed
   private: char p_header [ Menu_HEADER_STR_SIZE ]; // text of the header
   private: int p_cursor;


   // Constructor & Destructor

   public: Menu ( void );
   public: Menu ( const char* str );
//   public: Menu ( string &str /*, FONT *fnt */);
   public: ~Menu ( void );

   // Property Methods

//   public: void title ( string &str );
   public: void title ( const char* str );
   public: const char* title ( void );
   public: int nb_item ();
//   public: void textfont ( FONT *fnt );
   public: short char_width ( void );
   public: void header ( const char *text );
   public: bool showheader ( void ); // indicate if header will be shown


   // Methods

//   public: void add_item ( string &str, bool mask = false );
   public: void add_item ( int answer, const char* str, bool mask = false );
   public: void add_itemf ( int answer, const char* format, ... );
   public: void mask_item ( int ID ); // use the ID to find and mask
   public: void mask_answer ( int answer ); // use the answer to find and mask
   public: void unmask_all_item ( void );
   public: short show ( short x, short y, bool nocancel = false, bool reset_cursor = false );
   public: void draw ( short x, short y );

   // SQLite related methods
   public: void add_query ( const char* field, const char* table, const char* condition ); // add all the results of a query using
   public: void add_query_item ( const char* field, const char* table, int key ); // add a specific entry

   // Private methods
   //private: int get_answer ( int ID ); // not required right now

};


