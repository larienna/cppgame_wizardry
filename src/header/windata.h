/***************************************************************************/
/*                                                                         */
/*                            W I N D A T A . H                            */
/*                              Class Header                               */
/*                                                                         */
/*     Content : Class WinData                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 13th, 2002                                 */
/*                                                                         */
/*          Class who inherit from window which manage a window with       */
/*     various data in it.                                                 */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                          Class Definition                             -*/
/*-------------------------------------------------------------------------*/

template<class t_datatype>
class WinData : public Window
{
   /// --- Properties ---

   private: t_datatype *p_parameter; // parameter to pass to the drawing function
   private: void (*p_procedure)(t_datatype&, short, short ); // pointer to the function
   private: void (*p_procedure_i)(int, int, int); //new type of function pointer
   private: bool p_usetypeint; // temporary switch to use new function type.
   private: int p_key;

   // --- Constructor & Destructor ---

   public: WinData ( void (*proc)( t_datatype&, short, short ),
                     t_datatype &param, short x_pos, short y_pos, short width,
                     short height, bool translucent = false );
   public: WinData ( void (*proc)( int, int, int ),
                     int key, int x_pos, int y_pos, int width,
                     int height, bool translucent = false );
//
//   public: ~WinData ( void );

   // --- property methods ---

   public: void parameter ( t_datatype &param );
   public: void key ( int key );

   // --- Virtual Functions ---

   public: virtual void preshow ( void );
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

};
