//-------------------------------------------------------------
//
//                          M A P . H
//
//    Class : Map
//    Programmer: Eric Pietrocupo
//    Starting date: Feb 5 2014
//
//    A simple class used to manage map discovery while walking
//
//-------------------------------------------------------------

#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

//------------------------------------------------------------
//                      Constants
//-----------------------------------------------------------

#define Map_BYTE_WIDTH      13
#define Map_MAXWIDTH        100
#define Map_MAXDEPTH          20
//#define Map_HEIGHT         100
//#define Map_DEPTH          20

//-------------------------------------------------------------
//                      Class Definition
//-------------------------------------------------------------

class Map //: public SQLobject
{
   // properties

   private: unsigned char p_map [ Map_MAXDEPTH ] [ Map_MAXWIDTH ] [ Map_BYTE_WIDTH ];
      // huge bit table containing discovered map areas

   // --- constructor and destructor

   public: Map ( void );
   public: ~Map ( void );

   // --- Property Methods ---

   public: void write (int x, int y, int z);
   public: void write (s_Party_position tmpos) { write (tmpos.x, tmpos.y, tmpos.z); }
   public: bool read (int x, int y, int z);

   // --- Methods ---

   public: void clear ( void );
   public: void load ( void );
   public: void save ( void );
   public: void display ( s_Party_position tmpos, bool partydetail = false );

   // --- Virtual Methods ---

   /*public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj (void);
   public: void virtual obj_to_sqlupdate (void); // format : namex=x, namey=y, namez=z
   public: void virtual obj_to_sqlinsert (void);
*/
};

//--------------------------------------------------------------
//                               Global Variables
//--------------------------------------------------------------


extern Map handmap;


#endif // MAP_H_INCLUDED
