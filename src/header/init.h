/***************************************************************************/
/*                                                                         */
/*                              I N I T . H                                */
/*                                                                         */
/*     Content : Initialisation procedures                                 */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : march 19, 2002                                      */
/*                                                                         */
/*          This module contains all the procedure used to initialise      */
/*     the system, objects and data of the game                            */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                             Constant                                  -*/
/*-------------------------------------------------------------------------*/

#define INIT_SUCCESS    1
#define INIT_FAILURE    2

//#define Init_PARAM_NONE    0
//#define Init_PARAM_SETUP   1
//#define Init_PARAM_EDITOR  2

#define Init_NB_GFXMODE    5

/*-------------------------------------------------------------------------*/
/*-                           Structures                                  -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Init_gfxmode
{
   //char name [9];
   int w;
   int h;
}s_Init_gfxmode;

/*-------------------------------------------------------------------------*/
/*-                           Prototypes                                  -*/
/*-------------------------------------------------------------------------*/

//int decode_parameter ( int nb_param, char *param[] );
int init_game ( void );
//int init_setup ( void );
//int init_editor ( void );
int init_keyboard ( void );
int init_timer ( void );
int init_sound ( void );
int init_other ( void );
int init_graphic ( void );
int init_data ( void );
void exit_game ( void );
//void exit_setup ( void );
//void exit_editor ( void );
//int init_database ( void );
//int init_sqldatabase ( void );

void InitProc_datafile_loading ( DATAFILE *f );
//void InitProc_datafile_editor_loading ( DATAFILE *f );

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

extern s_Init_gfxmode Init_GFXMODE [ Init_NB_GFXMODE ];

extern double datafile_total_size; // used for datafile loading proc
