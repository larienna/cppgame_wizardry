/***************************************************************************/
/*                                                                         */
/*                            G A M E . H                                  */
/*                                                                         */
/*     Content : Class Game                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting date : April 30th, 2002                                    */
/*                                                                         */
/*          This class contains all the information related to a started   */
/*     game. It also make the link with the adventure and the savegame     */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                          Parameters                                   -*/
/*------------------------------------------------------------------------*/

//TEST de make file
//TEST de makefile encore

//#define Game_NB_MAX_RACE      32

#define Game_FILENAME_STR_SIZE  23
#define Game_NAME_STR_SIZE      51
#define Game_AUTHOR_STR_SIZE    31
#define Game_VERSION_STR_SIZE   11
#define Game_DATE_STR_SIZE      31

/*-------------------------------------------------------------------------*/
/*-                           Constants                                   -*/
/*-------------------------------------------------------------------------*/

// new game

#define Game_NEW_TRUE     1
#define Game_NEW_FALSE    0

//#define Game_LAST_TRUE     1
//#define Game_LAST_FALSE    0

// difficulty level

/*#define Game_DIFFICULTY_EASY     0
#define Game_DIFFICULTY_NORMAL   1
#define Game_DIFFICULTY_HARD     2

// Levelup
#define Game_LEVELUP_FAST     0
#define Game_LEVELUP_NORMAL   1
#define Game_LEVELUP_SLOW     2
*/

/*-------------------------------------------------------------------------*/
/*-                         Typedef definition                            -*/
/*-------------------------------------------------------------------------*/

/*typedef struct s_Game_time
{
   int hour;
   int minute;
   int second;
}s_time;

typedef struct s_Game_date
{
   short year;
   int month;
   int day;
}s_date;*/

/*typedef struct dbs_Game
{
//   char adventurefile [ 13 ]  ;
   char savegamefile [ 23 ];//  ;
   char password [ 11 ];//  ;
   int pass_protect;//  ;
   int difficulty;//  ;
   int levelup;//  ;
   unsigned int adventure;//  ;
}  dbs_Game;*/

/*-------------------------------------------------------------------------*/
/*-                         Class definition                              -*/
/*-------------------------------------------------------------------------*/

class Game : public SQLobject
{

   //--- Properties ---

   private: char p_savegamefile [ Game_FILENAME_STR_SIZE ]; // this property is not saved into the database
   private: char p_adventurefile [ Game_FILENAME_STR_SIZE ]; //file name of the adventure to load

   private: char p_name [Game_NAME_STR_SIZE]; // name of the adventure
   private: char p_cityname [Game_NAME_STR_SIZE]; // name of the city
   private: char p_mazename [Game_NAME_STR_SIZE]; // name of the maze
   private: char p_saganame [Game_NAME_STR_SIZE]; // name of the saga, for group only
   private: int p_episode; // number of peisode in the saga
   private: char p_author [Game_AUTHOR_STR_SIZE]; // author name
   private: char p_version [Game_VERSION_STR_SIZE]; // version number
   private: char p_date [Game_DATE_STR_SIZE]; // date of the save game
   private: int p_newgame; // indicate it's a new game just created.
   private: int p_lastgame; // contains the raw date of the last game played.

   private: int p_lastcityday; // contains the day the player pass in the city for the last time
                               // this is used to trigger item generators.
   public: GameClock clock; // contains the time and nb of days passed. made public for easier access
   private: int p_encounter_count; // save the encounter counter.

//   private: s_Game_time p_time;
//   private: s_Game_date p_date;
//   private: string p_admin_password/*10*/ ; // password required for admin operation
//   private: string p_adventurefile;

   //private: char p_password [ 11 ]; // password protected game
   //private: bool p_pass_protect; // true if game is protected by password
   //private: int p_difficulty; // difficulty level of the monsters
   //private: int p_levelup; // level up speed in experience points
   //private: int p_adventure; // Adventure played by the game.

   //--- Constructor & Destructor ---

   public: Game ( void );
   public: ~Game ( void );

   //--- Property Methods ---

   public: const char* savegamefile ( void );
   public: void savegamefile ( const char *str );
   public: const char* adventurefile ( void );
   public: void adventurefile ( const char *str );
   public: const char* name ( void );
   public: void name ( const char *str );
   public: const char* cityname ( void );
   public: void cityname ( const char *str );
   public: const char* mazename ( void );
   public: void mazename ( const char *str );
   public: const char* saganame ( void );
   public: void saganame ( const char *str );
   public: int episode ( void );
   public: void episode ( int value );
   public: const char* author ( void );
   public: void author ( const char *str );
   public: const char* version ( void );
   public: void version ( const char *str );
   public: const char* date ( void );
   public: void date ( const char *str );
   public: int newgame ( void );
   public: void newgame ( int value );
   public: int lastgame ( void );
   public: void lastgame ( int value);
   //public: GameClock clock ( void );
   //public: void clock ( GameClock value );
   public: int lastcityday ( void );
   public: void lastcityday ( int value);
   public: int encounter_count ( void ) { return ( p_encounter_count ); }
   public: void encounter_count ( int value ) { p_encounter_count = value; }



//   public: string& adventure ( void );
//   public: const char* cadventure ( void );
//   public: void adventure ( string &str );
//   public: string& savegame ( void );
   //public: void password ( const char *str );
   //public: int difficulty ( void );
   //public: void difficulty ( int value );
   //public: int levelup ( void );
   //public: void levelup ( int value );
   //public: void protect ( void );
   //public: void open ( void );
   //public: bool pass_protect ( void );
   //public: int adventure ( void );
   //public: void adventure ( int value );

   //--- Method ---

   //public: void increment_clock ( void );
   public: void increment_lastcityday ( void );

   //public: bool verify_password ( const char *str );

   //--- Private Methods ---

//   public: void adjust_overflow ( void );

   //--- Virtual Methods ---

// object relational virtual methods
   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

//   public: virtual int SQLinsert ( void );
//   public: virtual void SQLupdate ( void );
//   public: virtual void SQLselect ( int value, bool readonly = false );
//   public: virtual void SQLselect ( unsigned int index, bool readonly = false );
//   public: virtual void DBremove ( void );

   /*public: virtual void objdat_to_strdat ( void *dataptr );
   public: virtual void strdat_to_objdat ( void *dataptr );
   public: virtual void child_DBremove ( void );*/

};

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

/*extern const char STR_GAM_DIFFICULTY [] [ 6 ];
extern const char STR_GAM_LEVELUP [] [ 6 ];*/

extern Game game;

