/***************************************************************************/
/*                                                                         */
/*                      W I N D I S T R I B . H                            */
/*                         Class Definitnon                                */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinDistributor                                      */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting date : May 18th, 2002                                      */
/*                                                                         */
/*          Class who inherint from window to encapsulate distributors     */
/*                                                                         */
/***************************************************************************/

#ifndef WINDISTRIB_H_INCLUDED
#define WINDISTRIB_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*-                       class definition                                -*/
/*-------------------------------------------------------------------------*/

class WinDistributor : public Window
{
   // --- Property Methods ---

   private: Distributor *p_distributor; // Distributor to show in the window
   private: bool p_no_cancel; // no cancel command enabled in menu
   private: bool p_distribute_all; // reset cursor at start for each show
   //private: short p_cursor; // last cursor position

   // --- Constructor & Destructor ---

   public: WinDistributor ( Distributor &dst, int x_pos, int y_pos,
              bool distribute_all = true, bool no_cancel = true, bool translucent = false  );
//   public: ~WinList ( void );

   // --- Virtual Methods ---

   public: virtual void preshow ( void ); // this funtion does nothing
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

};



#endif // WINDISTRIB_H_INCLUDED
