#ifndef SQLWRAP_H_INCLUDED
#define SQLWRAP_H_INCLUDED
/**

 S Q L w r a p . h

 @author Eric Pietrocupo
 @since  April, 22nd, 2012
 license GNU General Public License

 This module contains various methods designed to also work with the
 SQLobject and EnchancedSQLobject class. It's basically a wrapper around
 the sqlite methods to make them more convenient to use and include
 error control and logging.

 Those methods will declare a global database variable that will be used
 by all the methods and the SQLobjects. It prevents the need to pass the
 database in parameter for each call.
*/
extern sqlite3 *SQLdb;

/**
 ------------------------ Open and Closing ------------------------------

 Before the database can be used, it must be opened and when exiting the
 game, the database must be closed. The following methods can be used.
 Once open, it will be accessible using SQLdb declared above.
*/

int SQLopen ( const char* filename );
int SQLclose ( void );

/**
 If will also set a global variable when the database is open.
 */
extern bool SQLisopen;

/**
 There are other ways to open a database. It's possible to open a database
 archived into an allegro datafile with the following methods.
*/

int SQLopentemp_from_dataobj ( const char *filename, const char *objectname );
int SQLclosetemp ( void );

/**
 The first method requires the allegro datafile name, and the object name
 containing the database. It will create a temporary file named
 "tmpsqldb.sqlite" that will be used for reading the file. Once the necessary
 operations are complete, SQLclosetemp should be called to close the database
 and destroy the temporary sqlite file.

 One thing you can do with temporary database is to duplicate it using the
 following method.
*/
int SQLbackup ( const char *filename);

/**
 It will create a new copy with the supplied file name.

 ------------- Using the database and error control ----------------------

 Once open, various command can be used. Queries that do not select data
 can be executed with the SQLexec method, while SQLcommit is used to save
 the data permanently into the database
*/

int SQLcommit ( void );
int SQLexec ( const char *querystr );

/**
 Those methods returns the SQL error codes and a copy is saved into a
 global variable if necessary.
*/
extern int SQLerror;

/**
 Most methods generate error messages when they occur. There is a global
 variable that determines if they must be shown or not.
 */
extern bool SQLerrormsg_active;

/**
 It might be an overkill, but there are methods that can be used to activate
 or deactivate those message, and I even defined aliases that invert the
 method's keywords because I always got confused and never remembered what was
 the right name of the methods.
*/

void SQLactivate_errormsg (void);
void SQLdeactivate_errormsg (void);
#define SQLerrormsg_activate     SQLactivate_errormsg
#define SQLerrormsg_deactivate   SQLdeactivate_errormsg

/**
 When an error occurs, if you want to know why, you can call the follwing
 method.
*/
void SQLerrormsg ( void );

/**
 Most methods in this file should display the error message if activated, but
 with the method above, you can print it on stdout manually.

 There is this shortcut methods that count the number of records in a table.
 It does not allow combining queries between multiple table and it returns
 the number of occurence.
*/
int SQLcount ( const char *field, const char* table, const char* condition );

/**
 When doing multiple queries one after another like for example a series of
 inserts, or updates, it can eventually take a long time to perform. So in order
 to speed  up the process, It's possible to encapsulate the queries as a series of
 transaction with the following methods:
 */

int SQLbegin_transaction ( void );
int SQLcommit_transaction ( void );

/**
 Before starting intensive queries, you call SQLbegin_transaction. This will
 put the database on hold and accumulate queries it receives. When you are done
 you can call SQLcommit_transaction and it will execute all queries at once.

 --------------------- Querying Records ------------------------------

 The most important use of a database is to query data that will be used in
 the game. This is a multi step process that requires multiple methods.
 First we need to prepare our query which is also called a statement.
 */

int SQLprepare (const char *querystr );
int SQLpreparef ( const char *format, ... );

/**
 Those methods will prepare your statement with the supplied query. The
 SQLpreparef allow using a format string with variable arguments like printf.
 Then the statement will be stored into this global variable that will be
 used by subsequent calls.
*/
extern sqlite3_stmt *SQLstatement;

/**
 Since the same statement will be used by all methods, it is important not
 to prepare a new statement while another statement has already been
 prepared. If SQLprepare returns SQLITE_OK, it means the preparation worked
 and we can now start reading the record set.

 There is a maximum length that query's strings can have which is defined by
 */

#define SQL_QUERYSTR_LEN 1024

/**
 This is to make sure there is no buffer overflow when building queries. But it
 should eventually change by using dynamic allocation of query strings.

 Once ready, you can read the records with :
*/

int SQLstep (void);

/**
 SQLstep will advance to the next record in the record set. It will return
 SQLITE_ROW if there is a valid record available. Else there is no more
 records available.

 When positionned in a new record, it's now time to use the column methods
 to extract the information from the record using the following methods.
*/

int SQLcolumn_int ( int colID );
const char* SQLcolumn_text ( int colID);
const void* SQLcolumn_blob ( int colID );

/**
 There is a method for each type of data, it's your responsibility to make
 sure the column you query is of the specified type (Floats has been
 omitted, since it's not used in this game). The colID refers to
 the columns selected where the first field is 0 and the last field is N-1

 Once the reading process has been completed, you must call:
*/
int SQLfinalize (void);

/**
 Which will clean up the statement and possibly free memory.

 So a typical recordset reading will look like this

 if ( SQLprepare ("SELECT pk, name FROM client") == SQLITE_OK )
 {
    while ( SQLstep() == SQLITE_ROW )
    {
       variable_int = SQLcolumn_int ( 0 );
       variable_char = SQLcolumn_text ( 1 );
       // then do something with those variables
    }
    SQLfinalize();
 }
 else
 {
    printf ("Error: Could not read the list of clients");
 }

 A few other details, the SQLobject classes requires a default name for the
 primary key since they handle that field differently. It is define using
 this constant.
*/

#define SQL_PRIMARY_KEY "pk"

/**
 There is also this bind methods used for special situation. Instead of
 copying the content of a variable in a query, we use special identifiers
 like que question mark to indicate that a variable will be inserted there
 later. Once the statement is prepared we bind the variables using the method
 below before calling the step method.

 Only blob has been defined since all other data format can be inserted
 directly into the query.
*/

int SQLbind_blob ( const void* buffer, int size, int index );

#endif // SQLWRAP_H_INCLUDED

/*--- old code ---

int SQLcommit ( sqlite3 *exSQLdb );
int SQLopen ( sqlite3 **exSQLdb, const char* filename );
int SQLclose ( sqlite3 *exSQLdb );

int SQLprepare (sqlite3 *exSQLdb, sqlite3_stmt **exSQLstatement, const char *querystr );
int SQLpreparef (sqlite3 *exSQLdb, sqlite3_stmt **exSQLstatement, const char *format, ... );
int SQLstep (sqlite3_stmt *exSQLstatement);
int SQLfinalize (sqlite3 *exSQLdb, sqlite3_stmt *exSQLstatement);
void SQLerrormsg ( sqlite3 *exSQLdb );
int SQLcolumn_int ( sqlite3_stmt *exSQLstatement, int colID );
const char* SQLcolumn_text ( sqlite3_stmt *exSQLstatement, int colID);*/
