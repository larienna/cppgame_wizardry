/***************************************************************************/
/*                                                                         */
/*                          W I N T I T L E . H                            */
/*                              Class header                               */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinTitle                                            */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 10th, 2002                                 */
/*                                                                         */
/*          This class inherit from Window to create a title window from   */
/*     a character string. Width and heigth are adjusted automaticly       */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                         Class Parameter                               -*/
/*-------------------------------------------------------------------------*/

#define WinTitle_FONT   FNT_ambrosia24
#define WinTitle_COLOR  General_COLOR_TEXT

/*-------------------------------------------------------------------------*/
/*-                         Constants                                     -*/
/*-------------------------------------------------------------------------*/

#define WinTitle_X 320
#define WinTitle_Y 0

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class WinTitle : public Window
{
   //--- Properties ---

   private: char p_title [ 81 ];
   private: short p_center_x;

   //--- Constructor & Destructor ---

//   public: WinTitle ( string &title, short center_x = 320, short y_pos = 0 );
   public: WinTitle ( const char *title, short center_x = WinTitle_X, short y_pos = WinTitle_Y, bool translucent = false  );
//   public: ~WinTitle ( void );

   //--- Virtual Methods ---

   public: virtual void preshow ( void );
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

};
