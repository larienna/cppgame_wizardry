/***************************************************************************/
/*                                                                         */
/*                                I T E M . H                              */
/*                                                                         */
/*                           Base Class Definition                         */
/*                                                                         */
/*   Content : Item class definition                                       */
/*   Programmer : Eric Pietrocupo                                          */
/*   Starting Date : April 29th, 2002                                      */
/*                                                                         */
/*        This is the base class from which most equipment objects are     */
/*   derived from.                                                         */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                          Class Parameter                              -*/
/*-------------------------------------------------------------------------*/

#define Item_NAME_SIZE        51 // excluding \0 ???

/*-------------------------------------------------------------------------*/
/*-                            Constants                                  -*/
/*-------------------------------------------------------------------------*/

// Ability types of the objects

// most ability affect the user except for spells
// Ability use charges to use

/*#define Item_ABILITY_NONE              0
#define Item_ABILITY_SPELL             1
#define Item_ABILITY_RAISE_MAXHP       2 // raise max HP by value
#define Item_ABILITY_RAISE_MAXSP       3 // raise max SP by value
#define Item_ABILITY_GAIN_EXP          4 // gain value
#define Item_ABILITY_LOWER_AGE         5 // by value
#define Item_ABILITY_RAISE_SOUL        6 // by value
#define Item_ABILITY_RECOVER_HP        7 // random value
#define Item_ABILITY_RECOVER_MP        8 // random value
#define Item_ABILITY_RAISE_ATTRIBUTE   9 // value = attribute ID bitwise
#define Item_ABILITY_CURE_HEALTH       11 // value = healthID bitwise
#define Item_ABILITY_RAISE_STAT        12 // Raise Stat limited time ( considered spell )

//#define Item_ABILITY_RAISE_MAXLP        13 // RAise maxLP by value

// Auto abilities
// These abilities are executed for each step in the maze

#define Item_AUTOABILITY_NONE             0
#define Item_AUTOABILITY_REGEN_HP         1 // +1 HP
#define Item_AUTOABILITY_REGEN_MORE_HP    2 // +3 HP
#define Item_AUTOABILITY_DRAIN_HP         3 // -1 HP
#define Item_AUTOABILITY_DRAIN_MORE_HP    4 // -3 HP
#define Item_AUTOABILITY_REGEN_MP         5 // +1 SP
#define Item_AUTOABILITY_REGEN_MORE_MP    6 // +3 SP
#define Item_AUTOABILITY_DRAIN_MP         7 // -1 SP
#define Item_AUTOABILITY_DRAIN_MORE_MP    8 // -3 SP
#define Item_AUTOABILITY_RANDOM_HEALTH    9 // 10% cahnce receive random health
#define Item_AUTOABILITY_CURE_HEALTH      10 // 5% cure each health
#define Item_AUTOABILITY_REGEN_EXP        11 // + lvl exposant * 5
#define Item_AUTOABILITY_DRAIN_EXP        12 // - lvl exposant * 20
#define Item_AUTOABILITY_REGEN_SOUL       13 // 5% chance +1 soul
#define Item_AUTOABILITY_DRAIN_SOUL       14 // 10% chance -1 soul
#define Item_AUTOABILITY_RAISE_AGE        15 // 5% chance +1 age

// item charges exhausted effect
// The abilities are executed when no more charges left in the Item

#define Item_EXHAUST_BREAK              0
#define Item_EXHAUST_NOTHING            1 // nothing happen, charges are all used.
#define Item_EXHAUST_RECHARGE           2 // allow ITEM to be recharged
#define Item_EXHAUST_DEAD               3 // kill the character
#define Item_EXHAUST_ASHES              4 // send character in ashes
#define Item_EXHAUST_DELETED            5 // delete the character
#define Item_EXHAUST_RAISE_AGE          6 // Age the character from 3% to 8% lifespan
#define Item_EXHAUST_LOWER_MAXHP        7 // lower HP by random ( 2 * exp level )
#define Item_EXHAUST_LOWER_MAXSP        8 // lower SP by random ( 2 * exp level )
#define Item_EXHAUST_LOSE_EXP           9 // 25% next level
#define Item_EXHAUST_LOWER_STRENGTH     10
#define Item_EXHAUST_LOWER_DEXTERITY    11
#define Item_EXHAUST_LOWER_ENDURANCE    12
#define Item_EXHAUST_LOWER_INTELLIGENCE 13
#define Item_EXHAUST_LOWER_CUNNING      14
#define Item_EXHAUST_LOWER_WILLPOWER    15
#define Item_EXHAUST_LOWER_LUCK         16
#define Item_EXHAUST_LOSE_SOUL          17 // lose 5 to 25 soul point
#define Item_EXHAUST_RANDOM_HEALTH      18

// drawbacks

#define Item_DRAWBACK_NOTHING            0 // nothing happen, charges are all used.
#define Item_DRAWBACK_RAISE_AGE          1 // Age the character from 3% to 8% lifespan
#define Item_DRAWBACK_LOWER_MAXHP        2 // lower HP by random ( 2 * exp level )
#define Item_DRAWBACK_LOWER_MAXMP        3 // lower SP by random ( 2 * exp level )
#define Item_DRAWBACK_LOSE_EXP           4 // 25% next level
#define Item_DRAWBACK_LOWER_STRENGTH     5
#define Item_DRAWBACK_LOWER_DEXTERITY    6
#define Item_DRAWBACK_LOWER_ENDURANCE    7
#define Item_DRAWBACK_LOWER_INTELLIGENCE 8
#define Item_DRAWBACK_LOWER_CUNNING      9
#define Item_DRAWBACK_LOWER_WILLPOWER    10
#define Item_DRAWBACK_LOWER_LUCK         11
#define Item_DRAWBACK_LOSE_SOUL          12 // lose 5 to 25 soul point
#define Item_DRAWBACK_RANDOM_HEALTH      13

*/
// effect types of the object

// these effect allows to protect character against something or
// to inflict ennemy with something.

//#define Item_ELMEFFECT_MASK_ELEMENT       // 0000000000111111
//#define Item_ELMEFFECT_MASK_MONSTER       // 1111111111000000

//#define Item_HLTEFFECT_NONE            0
//#define Item_ELMEFFECT_NONE            0

/*#define Item_HLTEFFECT_SLEEP          1     // 0000000000000001
#define Item_HLTEFFECT_PARALYZATION   1<<1  // 0000000000000010
#define Item_HLTEFFECT_PETRIFICATION  1<<2  // 0000000000000100
#define Item_HLTEFFECT_RESERVED       1<<3  // 0000000000001000
#define Item_HLTEFFECT_POISON         1<<4  // 0000000000010000
#define Item_HLTEFFECT_BLINDNESS      1<<5  // 0000000000100000
#define Item_HLTEFFECT_CRIPPLE        1<<6  // 0000000001000000 ?remove
#define Item_HLTEFFECT_RESERVE3       1<<7  // 0000000010000000
#define Item_HLTEFFECT_FEAR           1<<8  // 0000000100000000
#define Item_HLTEFFECT_CURSE          1<<9  // 0000001000000000
#define Item_HLTEFFECT_SEAL           1<<10 // 0000010000000000
#define Item_HLTEFFECT_DOOM           1<<11 // 0000100000000000
#define Item_HLTEFFECT_FADE           1<<12 // 0001000000000000
#define Item_HLTEFFECT_RESERVE5       1<<13 // 0010000000000000
#define Item_HLTEFFECT_RESERVE6       1<<14 // 0100000000000000
#define Item_HLTEFFECT_RESERVE7       1<<15 // 1000000000000000
*/




// Identifications

// example :
// name-type, value-stat, effect, ability-autoability, charge, exhaust-drawback, status
/*#define Item_IDENTIFICATION_ALL        255
#define Item_IDENTIFICATION_NONE       0 //
#define Item_IDENTIFICATION_NAME       1 // name and type
#define Item_IDENTIFICATION_STAT       2 // Stats and Gold value
#define Item_IDENTIFICATION_STATUS     4 // Fine, echant, artifact, key item
#define Item_IDENTIFICATION_EFFECT     8 // Hlt & Elm effect + Mo + MD effect
#define Item_IDENTIFICATION_ABILITY    16 // Ability, autoability, charges
#define Item_IDENTIFICATION_DRAWBACK   32 // Exhaust, Drawback
*/

// Status Id
// maybe not useful anymore
/*#define Item_STATUS_NORMAL       0
//#define Item_STATUS_FINE         1
#define Item_STATUS_ENCHANTED    2
#define Item_STATUS_CURSED       4
#define Item_STATUS_ARTIFACT     8
//#define Item_STATUS_UNIQUE       16
#define Item_STATUS_FOUND        32 // not sold normaly
*/


// Magical Properties

//?? idea : HOming to auto hit target ( currently in dmg type )

/*#define Item_MGKPROP_ELEMENTKILL       1     // 0000000000000001
#define Item_MGKPROP_DEATH             1<<1  // 0000000000000010
#define Item_MGKPROP_ASHES             1<<2  // 0000000000000100
#define Item_MGKPROP_DELETE            1<<3  // 0000000000001000
#define Item_MGKPROP_SOULDRAIN         1<<4  // 0000000000010000
#define Item_MGKPROP_LIFEDRAIN         1<<5  // 0000000000100000
#define Item_MGKPROP_RESERVED2         1<<6  // 0000000001000000
#define Item_MGKPROP_RESERVED3         1<<7  // 0000000010000000
#define Item_MGKPROP_RESERVED4         1<<8  // 0000000100000000
#define Item_MGKPROP_WEAPONIMMUNITY    1<<9  // 0000001000000000
#define Item_MGKPROP_MAGICIMMUNITY     1<<10 // 0000010000000000
#define Item_MGKPROP_RESERVED8         1<<11 // 0000100000000000
#define Item_MGKPROP_RESERVED9         1<<12 // 0001000000000000
#define Item_MGKPROP_RESERVED10        1<<13 // 0010000000000000
#define Item_MGKPROP_RESERVED11        1<<14 // 0100000000000000
#define Item_MGKPROP_RESERVED12        1<<15 // 1000000000000000
*/
// Item Location
/*
#define WEAPON     0
#define SHIELD     1
#define ARMOR      2
#define HEAD       3
#define HAND       4
#define FEET       5
#define OTHER      6
#define NOLOC      7

#define Item_LOCATION_WEAPON     0
#define Item_LOCATION_SHIELD     1
#define Item_LOCATION_ARMOR      2
#define Item_LOCATION_HEAD       3
#define Item_LOCATION_HAND       4
#define Item_LOCATION_FEET       5
#define Item_LOCATION_OTHER      6
#define Item_LOCATION_NOLOC      7
*/

/*-------------------------- Approved with new System ----------------------------*/

// Item types

#define Item_TYPE_EQUIPABLE   5

#define Item_TYPE_WEAPON      1
#define Item_TYPE_SHIELD      2
#define Item_TYPE_ARMOR       3
#define Item_TYPE_ACCESSORY   4 // helmet, gauntlet, boots and accessories
#define Item_TYPE_EXPANDABLE  5 // one use items
#define Item_TYPE_OTHER       0 // key items or any non-equipable itmes

// Category Types

#define Item_CAT_WEAPON_NONE      0
#define Item_CAT_WEAPON_SWORD     1     // 00000001 = 1
#define Item_CAT_WEAPON_MACE      1<<1  // 00000010 = 2
#define Item_CAT_WEAPON_SPEAR     1<<2  // 00000100 = 4
#define Item_CAT_WEAPON_BOW       1<<3  // 00001000 = 8
#define Item_CAT_WEAPON_RIFFLE    1<<4  // 00010000 = 16
#define Item_CAT_WEAPON_WAND      1<<5  // 00100000 = 32
#define Item_CAT_WEAPON_CHAIN     1<<6  // 01000000 = 64
#define Item_CAT_WEAPON_DAGGERS   1<<7  // 10000000 = 128
//Maybe expands weapons to 12 types instead of 6 dual


#define Item_CAT_SHIELD_MINIMAL   1<<8 // = 256
#define Item_CAT_SHIELD_LIGHT     1<<9 // = 512
#define Item_CAT_SHIELD_HEAVY     1<<10 // = 1024
#define Item_CAT_RESERVED07        1<<11 // = 2048
#define Item_CAT_ARMOR_MINIMAL   1<<12 // = 4096
#define Item_CAT_ARMOR_LIGHT     1<<13 // = 8192
#define Item_CAT_ARMOR_MEDIUM    1<<14 // = 16384
#define Item_CAT_ARMOR_HEAVY     1<<15 // = 32768

// not sure if keep detail of accessory and expansable in category
// need a minimum of +1 to give a special effect.
#define Item_CAT_ACCESSORY_PENDANT   1<<16 // = 65536, powerup PS/MS
#define Item_CAT_ACCESSORY_RING      1<<17 // = 131072, powerup MDMGZ
#define Item_CAT_ACCESSORY_BOOTS     1<<18 // = 262144, Power up Init
#define Item_CAT_ACCESSORY_CIRCLET   1<<19 // = 524288, Powerup MD
#define Item_CAT_RESERVED01          1<<20 // = 1048576
#define Item_CAT_RESERVED02          1<<21 // = 2097152
#define Item_CAT_RESERVED03          1<<22 // = 4194304
#define Item_CAT_RESERVED04          1<<23 // = 8388608

// some items needs a special ability attached to it to be effective.
#define Item_CAT_EXPANDABLE_POTION   1<<24 // = 16777216
#define Item_CAT_EXPANDABLE_SCROLL   1<<25 // = 33554432
#define Item_CAT_EXPANDABLE_DART     1<<26 // = 67108864
#define Item_CAT_EXPANDABLE_FLASK    1<<27 // = 134217728
#define Item_CAT_EXPANDABLE_MISSILE  1<<28 // = 268435456
#define Item_CAT_EXPANDABLE_BOMB     1<<29 // = 536870912
#define Item_CAT_RESERVED05          1<<30 // = 1073741824
#define Item_CAT_RESERVED06          1<<31 // = 2147483648


// New category/profiency system

#define Item_PROF_WEAPON_NONE          0
#define Item_PROF_WEAPON_BALANCED      0x0001
#define Item_PROF_WEAPON_UNBALANCED    0x0002
#define Item_PROF_WEAPON_MISSILE       0x0004
#define Item_PROF_WEAPON_POLE          0x0008
#define Item_PROF_WEAPON_MAGICKAL      0x0010
#define Item_PROF_WEAPON_THROWN        0x0020
#define Item_PROF_RESERVED01           0x0040
#define Item_PROF_RESERVED02           0x0080

#define Item_PROF_SHIELD_MINIMAL       0x0100 // = 256
#define Item_PROF_SHIELD_LIGHT         0x0200 // = 512
#define Item_PROF_SHIELD_HEAVY         0x0400 // = 1024
#define Item_PROF_RESERVED07           0x0800 // = 2048
#define Item_PROF_ARMOR_MINIMAL        0x1000 // = 4096
#define Item_PROF_ARMOR_LIGHT          0x2000 // = 8192
#define Item_PROF_ARMOR_MEDIUM         0x4000 // = 16384
#define Item_PROF_ARMOR_HEAVY          0x8000 // = 32768



// old
/*#define Item_WEAPON_NONE      0
#define Item_WEAPON_SWORD     1     // 00000001 = 1
#define Item_WEAPON_MACE      1<<1  // 00000010 = 2
#define Item_WEAPON_SPEAR     1<<2  // 00000100 = 4
#define Item_WEAPON_BOW       1<<3  // 00001000 = 8
#define Item_WEAPON_RIFFLE    1<<4  // 00010000 = 16
#define Item_WEAPON_WAND      1<<5  // 00100000 = 32
#define Item_WEAPON_CHAIN     1<<6  // 01000000 = 64
#define Item_WEAPON_DAGGERS   1<<7  // 10000000 = 128

#define Item_ARMOR_MINIMAL   1 // 00000001
#define Item_ARMOR_LIGHT     2 // 00000010
#define Item_ARMOR_MEDIUM    4 // 00000100
#define Item_ARMOR_HEAVY     8 // 00001000

#define Item_SHIELD_MINIMAL   1 // 00000001
#define Item_SHIELD_LIGHT     2 // 00000010
#define Item_SHIELD_HEAVY     4 // 00000100

// need a minimum of +1 to give a special effect.
#define Item_ACCESSORY_PENDANT   1 // powerup PS/MS
#define Item_ACCESSORY_RING      2 // powerup MDMGZ
#define Item_ACCESSORY_BOOTS     4 // Power up Init
#define Item_ACCESSORY_CIRCLET   8 // Powerup MD

// some items needs a special ability attached to it to be effective.
#define Item_EXPANDABLE_POTION   1
#define Item_EXPANDABLE_SCROLL   2
#define Item_EXPANDABLE_DART     4
#define Item_EXPANDABLE_FLASK    8
#define Item_EXPANDABLE_MISSILE  16
#define Item_EXPANDABLE_BOMB     32
*/
// other stats and properties

//#define Item_ATTRIBUTE_PHYSICAL  1
//#define Item_ATTRIBUTE_MENTAL    2

// nb of steps ahead cna be targetted
#define Item_RANGE_MELEE  1
#define Item_RANGE_SHORT  2
#define Item_RANGE_LONG   3

// Location Type

#define Item_LOCATION_UNKNOWN    0 // temporary items
#define Item_LOCATION_CITY       1 // items for sale in the city
#define Item_LOCATION_PARTY      2 // items in the party inventory
#define Item_LOCATION_CHARACTER  3 // Items equiped by a character
#define Item_LOCATION_CHEST      4 // items located in the chest
#define Item_LOCATION_FOUND      5 // items that can be found ( they are duplicated );
#define Item_LOCATION_ARTIFACT   6 // special items for the quest.

// item template stat to change
// bitfield to determine what stat or ability can be added to the item
// right now, does not consider yet the possibility of optional stat boost, so all bonus are applied.

// bonus and malus will be mandatory for the numerical +X -X modifier.

//D20 stat
#define Item_BONUS_NONE       0
#define Item_BONUS_AD         0x0001  // 1
#define Item_BONUS_MD         0x0002  // 2
#define Item_BONUS_PS         0x0004  // 4
#define Item_BONUS_MS         0x0008  // 8
#define Item_BONUS_MELEE_CS   0x0010  // 16
#define Item_BONUS_RANGE_CS   0x0020  // 32
#define Item_BONUS_DR         0x0040  // 64
#define Item_BONUS_INIT       0x0080  // 128

#define Item_BONUS_D20        0x00FF

//XYZ stat
#define Item_BONUS_DMGZ       0x0100  // 256
#define Item_BONUS_MDMGZ      0x0200  // 512

#define Item_BONUS_XYZ        0x0300

// --- Power up / down ---

// power up and down are optional modifiers. Probably only 1 is selected fromt the list. Each would have a keyword
// prefix for each of them.


#define Item_POWERUP_ATTDIV   0x0001 // Modifiy ATT divider "Quick" -1, "Slow" +2
#define Item_POWERUP_POWDIV   0x0002 // modify POW divider
#define Item_POWERUP_MH       0x0004 // modify MH   "Light" 0 MH  "Heavy" +2
#define Item_POWERUP_ELEMENT  0x0008 // add an element : use element name (
#define Item_POWERUP_PRICE    0x0010 // Increase price of object by a lot 5x or 10x "Jewelered", "Corroded" /10 or /20
#define Item_POWERUP_NOAD     0x0020 // remove full and half active defense
#define Item_POWERUP_NODR     0x0040 // remove full and half damage resistance
#define Item_POWERUP_DMGY     0x0080 // Change die value "Great" +2 die face, "???", -2 die face (not a curse)
#define Item_POWERUP_PSAVE    0x0100 // add a bonus to physical saves ( maybe by +X and -2X or +2x and -4X
#define Item_POWERUP_MSAVE    0x0200 // add a bonus to mental saves
#define Item_POWERUP_CSPENALTY 0x0400 // remove penalty or double penalty
#define Item_POWERUP_MD       0x0800 // add bonus magic defense by +X or -2X
#define Item_POWERUP_DEFELEMENT 0x1000 // by default that item has an element (add after everything else is done)


// list exclude great/small which can be applied to anything. Combinable prefix
#define Item_NB_POWERUP    10
#define Item_NB_POWERDOWN  8
#define Item_NB_ELEMENT 31


// callback ID

#define Item_CB_LOKTOFEIT_DESTROY_ITEM    1

//Custom XYZ stat table

/*#define Item_XYZ_NB_STAT   4

#define Item_XYZ_DMGY      0
#define Item_XYZ_DMGZ      1
#define Item_XYZ_MDMGY     2
#define Item_XYZ_MDMGZ     3*/

/*-------------------------------------------------------------------------*/
/*-                            Type definition                            -*/
/*-------------------------------------------------------------------------*/

/*typedef struct dbs_Item_ability
{
   int type;//  ;
   int value;//  ;
   int exhausted;//  ; // result when all charges used
   int drawback;//  ; // result when using a charge beside the ability.
   int max_charge;//  ;
   int nb_charge;//  ;
} dbs_Item_ability;
*/
/*
   char name [ Item_NAME_SIZE  ];//  ;
   char fname [ Item_NAME_SIZE  ];//  ;
   int type;//  ;
   //int location;//  ;
   dbs_Item_ability ability;//  ;
   int autoability;//  ;
   unsigned int hlteffect;//  ;
   unsigned int elmeffect;//  ;
   //unsigned short magikproperty;//  ;
   unsigned int identification;//  ;
   //float weight;//  ;
   unsigned int status;//  ;
   int rarity;//  ;
   int price;//  ;
} dbs_Item;*/

typedef struct s_Item_powerupdown
{
   char prefix [16]; // string to prefix
   int flag; // bit that must be raised to apply effect
   int rarity; // min rarity of the item
   int costmulti; // cost multiplier
} s_powerupdown;

typedef struct s_Item_element
{
   //bool active; // indicate the effect is active (use offense/defense)
   char prefix [4][16]; // used for weapons, Shield, Armor, Accessory
   int flag; //element flag to raise
   int rarity; // min rarity required
   int costmulti; // multiply the cost by that value
   bool active [4]; // apply to Weapons, shield, Armor, Accessory
}s_Item_element;

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class Item : public EnhancedSQLobject
{

   public: const static int NB_FIELD = 35;
   private: static s_SQLfield p_SQLfield [ NB_FIELD ]; // NEW list the definition of all the fields in order
   private: s_SQLdata p_SQLdata [ NB_FIELD ];

   //----- Properties -----
   private: char p_name [ Item_NAME_SIZE ]; // Real name of the Item
   private: char p_fakename [ Item_NAME_SIZE ]; // name of the unidentified object
   private: char p_profiency [ EnhancedSQLobject_STRFIELD_LEN ];
   private: char p_defense [ EnhancedSQLobject_STRFIELD_LEN ];
   private: char p_element [ EnhancedSQLobject_STRFIELD_LEN ];
   private: char p_bonusmalus [ EnhancedSQLobject_STRFIELD_LEN ];
   private: char p_powerupdown [ EnhancedSQLobject_STRFIELD_LEN ];


   /*private: short p_price; // value in gold of the item.
   private: int p_type; // Item type
   private: int p_category; // sub category for equiping restrictions
   private: int p_clumsy; // weapon clumsiness level
   private: int p_attribute; // use physical or mental stat
   private: int p_hand; // number of hands required to equip the item
   private: int p_range; // attack range of the item
   private: unsigned int p_special; // special effect of the item ( stat restriction, negation, etc)
   private: unsigned int p_property; // elemental and health resistance
   private: int p_location_key; // Foreign key to recover the target location
   private: int p_location_type; // Type of location to search int he right table.
   private: int p_charges_current; // current number of charges
   private: int p_charges_max; // maximum number of charges
   private: int p_key; // indicates that item is a key item for the lock with the lockID number
   private: bool p_cursed; // item is cursed and cannot be unequiped
   private: int p_rarity; // defined from level 1 to 20
   private: bool p_identified; // indicates identified stuff
   private: int p_d20stat [ D20_NB_STAT ]; // contains equipment d20 style modifiers
   private: int p_xyzstat [ Item_XYZ_NB_STAT ]; // contains equipment specific xyz style modifier
   private: int p_statbonus; // determine which stats are modified by the bonus of the item.
*/
   //protected: int p_location; // Item equip location
   //protected: unsigned short p_magikproperty; // magical offense ability


   //protected: float p_weight; // weigth of the item


   //old data
   //protected: dbs_Item_ability p_ability; // infor on the ability of the object
   //protected: int p_autoability; // ability executed for each step
   //protected: unsigned int p_hlteffect; // special resistance and offensiive
   //protected: unsigned int p_elmeffect; // special resistance and offensiive
   //protected: unsigned int p_status; // Status  of the item



   //----- Constructor & destructor -----

   public: Item ( void );

   //public: Item ( int itemtag );
   public: virtual ~Item ( void );

   //----- Property methods -----

   public: const char* name ( void );
   public: const char* fakename ( void );
   public: void name ( const char* real, const char* fake );


   public: const char* profiency_str ( void ) { return p_profiency; }
   public: const char* defense_str ( void ) { return p_defense; }
   public: const char* element_str ( void ) { return p_element; }
   public: const char* bonusmalus_str ( void ) { return p_bonusmalus; }
   public: const char* powerupdown_str ( void ) { return p_powerupdown; }

   /*public: dbs_Item_ability ability ( void );
   public: void ability ( dbs_Item_ability data );
   public: void ability ( int type, int value, int max_charge, int exhausted, int drawback );
   public: int autoability ( void );
   public: void autoability ( int value );*/
   /*public: unsigned int hlteffect ( void );
   public: void hlteffect ( unsigned int value );
   public: unsigned int elmeffect ( void );
   public: void elmeffect ( unsigned int value );*/
   //public: int nb_charge ( void );
   //public: bool cursed ( void );
   public: int key ( void );
   public: bool identified ( void );
   public: void identified ( bool value );
   //public: float weight ( void );
   //public: void weight ( float value );
   /*public: unsigned int status ( void );
   public: void status ( unsigned int value );*/
   public: int rarity ( void );
   public: void rarity ( int value );
   public: int price ( void );
   public: void price ( int value );
   //public: unsigned short magikproperty ( void );
   //public: void magikproperty ( unsigned short value );
   //public: int location ( void );
   //public: void location ( int value );

   public: int profiency ( void );
   public: void profiency ( int value );
   //public: int clumsy ( void );
   //public: void clumsy ( int value );
   public: int attribute_attack ( void );
   public: void attribute_attack ( int value );
   public: int hand ( void );
   public: void hand ( int value );
   public: int range ( void );
   public: void range ( int value );
   public:  int defense ( void );
   public: void defense ( int value );
   public: int d20stat ( int stat );
   public: void d20stat ( int stat, int value );
   //public: int xyzstat ( int stat );
   //public: void xyzstat ( int stat,  int value );

   public:  int element ( void );
   public: void element ( int value );
   public: int location_key ( void );
   public: int location_type ( void );
   public: void location ( int type, int key );
   public: int charges_current ( void );
   public: int charges_max ( void );
   public: void charges_current ( int value) { set_int ( 13, value ); }
   public: void charges ( int current, int max );
   public: bool cursed ( void );
   public: void cursed ( bool value );

   public: int attribute_damage ( void ) { return ( get_int ( 6 ) ); }
   public: void attribute_damage ( int value ) { set_int ( 6, value ); }
   public: int dmgy ( void ) { return ( get_int ( 27 ) ); }
   public: void dmgy ( int value ) { set_int ( 27, value ); }
   public: int dmgz ( void ) { return ( get_int ( 28 ) ); }
   public: void dmgz ( int value ) { set_int ( 28, value ); }
   public: int mdmgz ( void ) { return ( get_int ( 29 ) ); }
   public: void mdmgz ( int value ) { set_int ( 29, value ); }
   public: int mh ( void ) { return ( get_int ( 30 ) ); }
   public: void mh ( int value ) { set_int ( 30, value ); }
   public: int attdiv ( void ) { return ( get_int ( 31 ) ); }
   public: void attdiv ( int value ) { set_int ( 31, value ); }
   public: int powdiv ( void ) { return ( get_int ( 32 ) ); }
   public: void powdiv ( int value ) { set_int ( 32, value ); }
   public: int bonusmalus ( void ) { return ( get_int ( 33 ) ); }
   public: void bonusmalus ( int value ) { set_int ( 33, value ); }
   public: int powerupdown ( void ) { return ( get_int ( 34 ) ); }
   public: void powerupdown ( int value ) { set_int ( 34, value ); }




   public: int type ( void );

   // these methods draw different output according to identification
   public: const char* vname ( void );
   //public: void statusS1 ( char* str );
   public: void statusS2 ( char* str );
   /*public: const char* typeS ( void );
   public: void abilityS ( char* str );
   public: const char* autoabilityS ( void );
   public: const char* exhaustedS( void );
   public: const char* drawbackS( void );*/

   //----- Methods -----

   public: void recharge ( void );

   public: bool is_key ( int lockID );

   public: void generate_ability ( bool cursed_enable = false );
   // use item : abiliity and decharge
   // execute effect
   // identify : character perform an identify ( maybe placed in character )
   //public: bool identified ( unsigned int type );

   //----- Private Methods -----

   //----- Virtual Methods -----

   /*public: virtual void objdat_to_strdat ( void *dataptr );
   public: virtual void strdat_to_objdat ( void *dataptr );
   public: virtual void child_DBremove ( void );*/

   /*public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);*/

   protected: void virtual template_load_completion ( void );

   protected: void virtual callback_handler ( int index );
   protected: void cb_loktofeit_destroy_item (void);
};


/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

//extern const char STR_ITM_ABILITY [][21];
//extern const char STR_ITM_AUTOABILITY [][21];
//extern const char STR_ITM_EXHAUST [][21];
//extern const char STR_ITM_HLTEFFECT [][21];
//extern const char STR_ITM_ELMEFFECT [][21];
//extern const char STR_ITM_DRAWBACK [][21];
//extern const char STR_ITM_MAGIKPROPERTY [][21];
//extern s_Item_mdproperty_info MDPROPERTY_INFO [ Item_NB_MDPROPERTY ];
extern const char STR_ITM_TYPE [] [11];
extern const char STR_ITM_TYPE_CODE [] [4];
//extern const char STR_ITM_LOCATION [][11];
extern const char STR_ITM_RARITY [][11];
extern const char STR_ITM_RANGE [][8];
extern const char STR_ITM_ATTRIBUTE [][4];
//extern const char ITEM_POWERUP [] [11];
//extern const char STR_ITEM_POWERDOWN [] [11];

//extern const char *STR_ITM_STATUS1 [] [11];
//extern const char STR_ITM_CAT_WEAPON [][8];
//extern const char STR_ITM_CAT_ARMOR [][8];
//extern const char STR_ITM_CAT_SHIELD [][8];
//extern const char STR_ITM_CATEGORY [][15];

//extern const char STR_ITM_CAT_ACCESSORY [][8];
//extern const char STR_ITM_CAT_EXPANDABLE [][8];



extern s_EnhancedSQLobject_strfield STRFLD_BONUS [ EnhancedSQLobject_HASH_SIZE ];
extern s_EnhancedSQLobject_strfield STRFLD_POWERUP [ EnhancedSQLobject_HASH_SIZE ];

extern s_Item_powerupdown ITEM_POWERUP [ Item_NB_POWERUP ];
extern s_Item_powerupdown ITEM_POWERDOWN [ Item_NB_POWERDOWN ];
extern s_Item_element ITEM_ELEMENT [ Item_NB_ELEMENT ];










