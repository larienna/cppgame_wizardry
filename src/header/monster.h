/***************************************************************************/
/*                                                                         */
/*                            M O N S T E R . H                            */
/*                                                                         */
/*                         Class Header Definition                         */
/*                                                                         */
/*     Content : Class MOnster                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : April 26, 2002                                      */
/*                                                                         */
/*          This is some sort of template class for monsters. It does not  */
/*     keep track of current HP and status. This will not be stored in the */
/*     class since that information is not kept in the database            */
/*                                                                         */
/***************************************************************************/


#ifndef MONSTER_H_INCLUDED
#define MONSTER_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*                              Constants                                  */
/*-------------------------------------------------------------------------*/

#define Monster_NAME_STRLEN 33

// Extra Reward Values

#define Monster_REWARD_GOLD_0    0  // 00000000
#define Monster_REWARD_GOLD_D6   1  // 00000001
#define Monster_REWARD_GOLD_D12  2  // 00000010
#define Monster_REWARD_GOLD_D20  3  // 00000011
#define Monster_REWARD_EXP_X2    4  // 00000100
#define Monster_REWARD_EXP_X4    8  // 00001000
#define Monster_REWARD_EXP_X8    12 // 00001100
#define Monster_REWARD_DROP_ITEM       0x0010
#define Monster_REWARD_DROP_EXPANDABLE 0x0020
#define Monster_REWARD_ARTIFACT        0x0040
#define Monster_REWARD_KEY_ITEM        0x0080



/*-------------------------------------------------------------------------*/
/*                              Global Variables                           */
/*-------------------------------------------------------------------------*/


//extern int Party_FORMATION_POSITION [ 3 ] [ 6 ] [ 6 ];
extern int Monster_POSITION [ 8 ] [ 8 ];

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class Monster : public Opponent
{

   // --- Properties ---

	private: char p_name	[ Monster_NAME_STRLEN ]; // name of the creature
	private: int p_level; // level
	//private: int p_hpdice;
	//private: int p_mpdice;
	private: int p_dmgy;
	private: int p_size; // inflence hit die and DMGY
	private: int p_power; // influence MP and MDMGY
	//private: int p_wsp; // influence nb of attack
	//private: int p_msp_min; // both influence creature spell list
	//private: int p_msp_max; //
	private: int p_attdiv;
	private: int p_powdiv;

	//private: unsigned int p_magic_type; // type of magic performed
	private: int p_resistance;
	private: char p_resistance_str [ EnhancedSQLobject_STRFIELD_LEN ];
	private: int p_weakness;
	private: char p_weakness_str  [ EnhancedSQLobject_STRFIELD_LEN ];
	private: int p_AD;
	private: int p_MD;
	private: int p_PS;
	private: int p_MS;
	private: int p_CS; // final combat skill, no mental or physical
	private: int p_DR;
	private: int p_init;
	private: int p_range; // for basic attack
	private: int p_mh; // for basic attack
	//private: int p_target; // for basic attack
	private: int p_defense; // for basic attack
	private: char p_defense_str  [ EnhancedSQLobject_STRFIELD_LEN ];
	private: int p_element; // for basic attack
	private: char p_element_str  [ EnhancedSQLobject_STRFIELD_LEN ];
   private: int p_front_attack [ 4 ];
	private: int p_back_attack [ 4 ];
	private: int p_extra_reward; // bitfield: extra reward given by defeating creature
	private: char p_extra_reward_str  [ EnhancedSQLobject_STRFIELD_LEN ];
	private: int p_pictureID;
	private: int p_position; // a value from 1 to 5 that determine the order of the monster. The value range has no importance
   private: int p_current_HP;
   private: int p_max_HP;
   private: int p_current_MP;
   private: int p_max_MP;
   //private: unsigned int p_health;
   private: int p_body;
   private: int p_location;
   private: int p_rank; // temporaty rank of the monster in the party during battles
   //private: int p_position;
   private: int p_FKcategory;
   private: int p_identified; // set to true if monsters are identified
   private: char p_uname [ Monster_NAME_STRLEN ];
   //private: char p_nameletter;

   private: static bool p_recompile_ranks;

   // --- Constructor ---

   public: Monster ( void );
   public: virtual ~Monster ( void );

   // --- property method ---

   // there is no set method because I don't think there will be a reason to do that.

   public: char *name ( void );
   public: void name ( const char *str );
	public: int level ( void );
	public: int size ( void );
	public: int power ( void );
	//public: int wsp ( void );
	//public: int msp_min ( void );
	//public: int msp_max ( void );
	//public: unsigned int magic_type ( void );
	//public: int hpdice ( void ) { return p_hpdice; };
	//public: void hpdice ( int value ) { p_hpdice = value; };
	//public: int mpdice ( void ) { return p_mpdice; };
	//public: void mpdice ( int value ) { p_mpdice = value; };
	public: int powdiv ( void ) { return p_powdiv; };
	public: void powdiv ( int value ) { p_powdiv = value; };
	public: int attdiv ( void ) { return p_attdiv; };
	public: void attdiv ( int value ) { p_attdiv = value; };
	public: int dmgy ( void ) { return p_dmgy; };
	public: int resistance ( void );
	public: const char *resistance_str ( void ) { return p_resistance_str; };
	public: int weakness ( void );
	public: const char *weakness_str (void ) { return p_weakness_str; };
	public: int AD ( void );
	public: int MD ( void );
	public: int PS ( void );
	public: int MS ( void );
	public: int CS ( void );
	public: int DR ( void );
	public: int init ( void );
	public: int range ( void );
	public: int front_attack ( int index ) { return p_front_attack [ index ]; };
	public: int back_attack ( int index ) { return p_back_attack [ index ]; };
	public: int mh ( void ) { return p_mh ; };
	public: int defense ( void ) { return p_defense; };
	public: const char *defense_str ( void ) { return p_defense_str; };
	public: int element ( void ) { return p_element; };
	public: const char *element_str ( void ) { return p_element_str; };
	//public: int clumsiness ( void );
	//public: int target ( void );
	//public: unsigned int special ( void );
	//public: unsigned int property ( void );
	//public: unsigned int action ( void );
	//public: int special_attack_1 ( void );
	//public: int special_attack_2 ( void );
	//public: int special_attack_3 ( void );
	//public: int special_attack_4 ( void );
	public: int extra_reward ( void );
	public: const char *extra_reward_str (void ) { return p_extra_reward_str; };
	public: int pictureID ( void );
	//public: int like_front ( void );
	public: int current_HP ( void ) { return ( p_current_HP ); }
   public: int max_HP ( void ) { return ( p_max_HP ); }
   public: int current_MP ( void ) { return ( p_current_MP ); }
   public: int max_MP ( void ) { return ( p_max_MP ); }
   //public: unsigned int health ( void ) { return ( p_health ); }
   public: int body ( void ) { return ( p_body ); }
   public: int location ( void ) { return ( p_location ); }
   public: int position ( void ) { return ( p_position ); }
   //public: unsigned int health ( unsigned int value ) { return ( p_health ); }
   public: void body ( int value ); // { p_body = value; p_recompile_ranks = true; }
   public: void location ( int value ) { p_location = value; }
   public: void position ( int value ) { p_position = value; }
   public: int rank ( void ) { return ( p_rank ); }
   public: void rank ( int value ) { p_rank = value; }
   public: int FKcategory ( void ) { return ( p_FKcategory ); }
   public: void FKcategory ( int value ) { p_FKcategory = value; }
   public: int identified ( void ) { return (p_identified ); }
   public: void identified ( int value ) { p_identified = value; }
   public: char* uname ( void ) { return ( p_uname ); }
   public: void uname ( const char* str ) { strncpy ( p_uname, str, Monster_NAME_STRLEN ); }
   public: int type ( void ) { return (Opponent_TYPE_ENNEMY ); }
   public: int soul ( void ) { return (100); /*monster has no soul, so get max)*/}


   // --- methods ---

   public: void recover_HP ( int value );
   public: void recover_MP ( int value );
   public: void recover_soul ( int value ) {} // monsters has no soul
   public: void set_HP ( int value ) { if ( p_body == Character_BODY_ALIVE ) p_current_HP = value; }
   public: void lose_HP ( int value );
   public: void lose_MP ( int value );
   public: void lose_soul ( int value ) {} // monsters has no soul
   public: void set_MP ( int value ) { if ( p_body == Character_BODY_ALIVE ) p_current_MP = value; }

   public: void roll_maxHPMP ( void );

   public: static void compile_ranks ( void );
   public: static void force_recompile_ranks ( void );

   public: char* displayname ( void ); //return name to display during battle.

   private: void convert_strfield_to_bitfield ();
   private: void convert_bitfield_to_strfield ();

   // --- call back methods ---

   //public: void cb_apply_spell ( void );

   // --- Virtual Methods ---

   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

   public: void compile_stat ( void );
   public: void virtual callback_handler ( int index );

};

extern s_EnhancedSQLobject_strfield STRFLD_EXTRA_REWARD  [ EnhancedSQLobject_HASH_SIZE ];


#endif // MONSTER_H_INCLUDED
