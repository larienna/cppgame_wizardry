/***************************************************************************/
/*                                                                         */
/*                             P A R T Y . H                               */
/*                           Class definition                              */
/*                                                                         */
/*     Content : Class Party                                               */
/*     Programmer : Eric Pietrocupo                                        */
/*     Stating date : May 1st, 2002                                        */
/*                                                                         */
/*         This class contains a regroupment of characters that will be    */
/*     used by most engine ( like maze and city ) to keep track of who is  */
/*     traveling in those engine.                                          */
/*                                                                         */
/***************************************************************************/

#ifndef PARTY_H_INCLUDED
#define PARTY_H_INCLUDED


// note : since this class use tag to identify party members, it will also be
// possible to make parties of ennemies.

/*-------------------------------------------------------------------------*/
/*-                             Constants                                 -*/
/*-------------------------------------------------------------------------*/

// party spell index
#define Party_NB_SPELL        16
#define Party_NB_MAX_CHAR     6

//?? needs to be redefined
/*#define Party_SPELL_LIGHT           0
#define Party_SPELL_PROTECT         1
#define Party_SPELL_BLESSING        2
#define Party_SPELL_RESERVED1       3
#define Party_SPELL_MAGIC_SCREEN    4
#define Party_SPELL_LEVITATE        5
#define Party_SPELL_ENCHANT_WEAPON  6
#define Party_SPELL_RESERVED2       7
#define Party_SPELL_WATER_BREATH    8
#define Party_SPELL_RESIST          9
#define Party_SPELL_GASEOUS_FORM    10
#define Party_SPELL_RESERVED3       11
#define Party_SPELL_IDENTIFY        12
#define Party_SPELL_FEAR            13
#define Party_SPELL_SEARCH          14
#define Party_SPELL_RESERVED4       15
*/

// party facing

// ?? same as : Maze_FACE_?????
#define Party_FACE_NORTH 0
#define Party_FACE_EAST  1
#define Party_FACE_SOUTH 2
#define Party_FACE_WEST  3

// party status
#define Party_STATUS_UNDEFINED    0
#define Party_STATUS_CITY         1
#define Party_STATUS_MAZE         2
#define Party_STATUS_CAMP         3
#define Party_STATUS_ENCOUNTER    4
#define Party_STATUS_COMBAT       5
#define Party_STATUS_ENDGAME      6

//combat party formation
#define Party_FORMATION_OFFENSIVE   0
#define Party_FORMATION_BALANCED    1
#define Party_FORMATION_DEFENSIVE   2

#define Party_NONE   0
#define Party_FRONT  1
#define Party_BACK   2

// combat attack side

/*#define Party_CMBTSIDE_FRONT     0
#define Party_CMBTSIDE_RIGHT     1
#define Party_CMBTSIDE_LEFT      2
#define Party_CMBTSIDE_BACK      3*/

/*-------------------------------------------------------------------------*/
/*-                           Type definition                             -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Party_position
{
   int x;
   int y;
   int z;
   int facing;
}s_Party_position;

/*typedef struct dbs_Party_position
{
   int x;//  ;
   int y;//  ;
   int z;//  ;
   int facing;//  ;
}  dbs_Party_position;

typedef struct dbs_Party
{
   char name [ 31 ];//  ;
   unsigned int character [ 6 ];//  ;
//   int combat_pos [ 6 ]  ;
   dbs_Party_position pos;//  ;
   short spell_duration [ Party_NB_SPELL ];//  ;
   int nb_character;//  ;
   unsigned int location;//  ;
   int status;//  ;
//   unsigned int player  ;
//   unsigned int game  ;
}  dbs_Party;*/

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class Party : public SQLobject
{

   /*--- Properties ---*/

   //private: char p_name [ 31 ]; // name of the party
   //private: int p_FKcharacter [ 6 ]; // character list
   //private: int p_combat_pos [ 6 ]; // location in combat rank of each character.
   private: s_Party_position p_pos; // location in the maze
   //private: int p_spell_duration [ Party_NB_SPELL ]; // duration of the spell in seconds cast on the party
   //private: int p_nb_character; // keep track of the number of characters
   //private: int p_location; // tag on city when saved game
   private: int p_status; // status of the party ( in/out )
//   private: int p_player; // tag of the player who own the party
//   private: int p_game; // tag of the game where it belong
   private: int p_gold; // party gold pieces
   private: int p_encounter; // decrementing value for triggering encounters
   private: int p_formation; //determines formation type

   /*--- Constructor & Destructor ---*/

   public: Party ( void );
//   public: Party ( const Party &tmpobj );
   public: ~Party ( void );

   /*--- Property Methods ---*/

//   public: string& name ( void );
   //public: const char* name ( void );
   //public: void name ( const char *str );
   //public: int FKcharacter ( int number );
   //public: int rank ( int number );
   //public: void rank ( int number, int rankID );
   public: s_Party_position position ( void );
   public: void position ( s_Party_position position );
   public: void position ( int x, int y, int z, int facing );
   public: void position ( int x, int y, int z );
   public: int facing ( void );
   public: void facing ( int side );
   public: int formation ( void );
   public: void formation ( int value );
   //public: int nb_character ( void );
   //public: int spell_duration ( int spellID );
   //public: void spell_duration ( int spellID, int value );
   //public: int location ( void );
   //public: void location ( int value );
   public: int status ( void );
   public: void status ( int value );
   public: int gold ( void );
   public: void gold ( int value );
   public: void add_gold ( int value );
   public: void remove_gold ( int value );
   public: int encounter ( void );
   public: void encounter ( int value );


//   public: int player ( void );
//   public: void player ( int value );
//   public: int game ( void );
//   public: void game ( int value );


   /*--- Methods ---*/

   //public: void add_character ( int character );
   //public: void remove_character ( int number );
   //public: void remove_all_character ( void );
   public: void turn_left ( void );
   public: void turn_right ( void );
   public: void walk_forward ( void );
   public: void walk_backward ( void );
   public: void move_down ( void );
   public: void move_up ( void );
   public: void move_north ( void ) { p_pos.y++; }
   public: void move_east ( void ) { p_pos.x++; }
   public: void move_south ( void ) { p_pos.y--; }
   public: void move_west ( void ) { p_pos.x--;}


   //public: bool test_front_wall ( unsigned char solid );
   //public: bool test_front_door ( unsigned char solid );
   //public: void decrement_duration ( short seconds );
   //public: void destroy_spell ( int spell_index );
   //public: void destroy_all_spell ( void );
   public: void reset_position ( void );
   //public: void rest ( int nb_days, int room_type );
   public: bool is_functional ( void );

   //public: void autoability ( void );
   //public: void activesort_characters ( void );
   //public: void ennemy_rankprioritysort ( void ); //sort ennemies according to rank priority
//   public: void activesort_ennemies ( void );

//   public: int combat_rank ( int characterID, int side );

   //public: void display_party_frame ( void );

   /*--- Private Methods ---*/

//   public: void compact_characters ( void );


   /*--- Virtual Methods ---*/

// object relational virtual methods
   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

//   public: virtual int SQLinsert ( void );
//   public: virtual void SQLupdate ( void );
//   public: virtual void SQLselect ( int value, bool readonly = false );
//   public: virtual void SQLselect ( unsigned int index, bool readonly = false );
//   public: virtual void DBremove ( void );

//   public: void exDBselect ( Database &datb, unsigned int index, bool readonly = false );
//   public: void exDBremove ( Database &datb );

   /*public: virtual void objdat_to_strdat ( void *dataptr );
   public: virtual void strdat_to_objdat ( void *dataptr );
   public: virtual void child_DBremove ( void );*/


};

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/


extern Party party;

extern const char STR_PAR_STATUS [] [ 12 ];
extern const char STR_PAR_FORMATION [] [ 12 ];
extern const char STR_PAR_FACING [ 4 ] [ 6 ];

// 3D table: Formation type, nb characters, position
extern const int Party_FORMATION_POSITION [ 3 ] [ 6 ] [ 6 ];

extern const char Party_STR_RANK [ 3 ] [ 8 ];

#endif
