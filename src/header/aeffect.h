/************************************************************************/
/*                                                                      */
/*                        a e f f e c t . h                             */
/*                                                                      */
/*     Content: Class ActiveEffect                                      */
/*     Programmer: Eric Pietrocupo                                      */
/*     Starting Date: November 4th, 2014                                */
/*                                                                      */
/*     Data object to manage the active effects created by spells,      */
/*     special attacks, items, and other sources.                       */
/*                                                                      */
/************************************************************************/

#ifndef AEFFECT_H_INCLUDED
#define AEFFECT_H_INCLUDED

/*----------------------------------------------------------------------*/
/*-                      Constant Definition                           -*/
/*----------------------------------------------------------------------*/

// --- enchanced SQL object generic code ---
//#define CLASSNAME ActiveEffect  // used to make code generic
//#define ActiveEffect_NB_INTEGER  16
//#define ActiveEffect_NB_STRING   2

// string variable length

#define ActiveEffect_NAME_LEN 33
#define ActiveEffect_CONDITION_LEN   49 // 16 different conditions,
#define ActiveEffect_KAOMOJI_LEN 4


// take note that status are inclusive, higher status removal trigger should also remove lower status
// ex: enc of combat should also remove end of turn. So remove effects lower and equal than trigger
#define ActiveEffect_EXPIRATION_ENDOFTURN    1 // only cure and death
#define ActiveEffect_EXPIRATION_DISTURBED    2 // if get attacked or slapped
#define ActiveEffect_EXPIRATION_ENDOFCOMBAT  3 // at the end of the battle
#define ActiveEffect_EXPIRATION_EXITREST     4 // only when exiting the maze or resting
#define ActiveEffect_EXPIRATION_PERMANENT    5 // never removed, require cure.

#define ActiveEffect_CONDITION_DISTARGET     0x00000001 //*  hard to implement, do not use for now
#define ActiveEffect_CONDITION_DISACTION     0x00000002 //working in combat and in camp
#define ActiveEffect_CONDITION_DISSPELL      0x00000004 //done for camp, and combat
#define ActiveEffect_CONDITION_DISSKILL      0x00000008 //done for camp, and combat
#define ActiveEffect_CONDITION_RNDTARGET     0x00000010 //
#define ActiveEffect_CONDITION_RNDACTION     0x00000020 // done for camp hard to determine what action to do (need to be valid)
#define ActiveEffect_CONDITION_RNDFRIEND     0x00000040 // done for camp (disable action)
#define ActiveEffect_CONDITION_RNDINSANE     0x00000080 // done for camp (disable action)
#define ActiveEffect_CONDITION_FAILPSAVE     0x00000100 // done for spell
#define ActiveEffect_CONDITION_FAILMSAVE     0x00000200 // done for spell
#define ActiveEffect_CONDITION_FAILDODGE     0x00000400 // done for spell, fight
#define ActiveEffect_CONDITION_FAILATTACK    0x00000800 // done for spell, fight
#define ActiveEffect_CONDITION_REDDAMAGE     0x00001000 // done for fight
#define ActiveEffect_CONDITION_REDMGKDMG     0x00002000 // done for spell
#define ActiveEffect_CONDITION_UNDEFINED1    0x00004000
#define ActiveEffect_CONDITION_UNDEFINED2    0x00008000
#define ActiveEffect_CONDITION_UNDEFINED3    0x00010000
#define ActiveEffect_CONDITION_UNDEFINED4    0x00020000
#define ActiveEffect_CONDITION_UNDEFINED5    0x00040000
#define ActiveEffect_CONDITION_UNDEFINED6    0x00080000
#define ActiveEffect_CONDITION_IRRDISTARGET  0x00100000
#define ActiveEffect_CONDITION_IRRDISACTION  0x00200000
#define ActiveEffect_CONDITION_IRRDISSPELL   0x00400000
#define ActiveEffect_CONDITION_IRRDISSKILL   0x00800000
#define ActiveEffect_CONDITION_IRRRNDTARGET  0x01000000
#define ActiveEffect_CONDITION_IRRRNDACTION  0x02000000
#define ActiveEffect_CONDITION_IRRRNDFRIEND  0x04000000
#define ActiveEffect_CONDITION_IRRRNDINSANE  0x08000000
#define ActiveEffect_CONDITION_IRRFAILACTION 0x10000000
#define ActiveEffect_CONDITION_IRRREDDAMAGE  0x20000000
#define ActiveEffect_CONDITION_IRRREDMGKDMG  0x40000000
// note irregular does not apply outside of battles, since can take time to do what ever you want.
// try to use a random variable inside the character to activate irregular, then convert effect as regular effect
// avoid checking status in double.

#define ActiveEffect_CATEGORY_BODY          1
#define ActiveEffect_CATEGORY_BLOOD         2
#define ActiveEffect_CATEGORY_SOUL          3
#define ActiveEffect_CATEGORY_NEURAL        4
#define ActiveEffect_CATEGORY_PSYCHIC       5
#define ActiveEffect_CATEGORY_SKIN          6
#define ActiveEffect_CATEGORY_SPELL         10       //also a category. Useful for bacortu
#define ActiveEffect_CATEGORY_EXPEDITION    11       // allow display in party screen
#define ActiveEffect_CATEGORY_ACTION        20
#define ActiveEffect_CATEGORY_SKILL         30



// callback ID
#define ActiveEffect_CB_PERSIST_EFFECT       1

/*----------------------------------------------------------------------*/
/*-                         Class Definition                           -*/
/*----------------------------------------------------------------------*/

class ActiveEffect : public EnhancedSQLobject
{
   // --- Properties ---

      // required declaration from EnhancedSQLobject

   public: const static int NB_FIELD = 25   ;
   private: static s_SQLfield p_SQLfield [ NB_FIELD ]; // NEW list the definition of all the fields in order
   private: s_SQLdata p_SQLdata [ NB_FIELD ]; // actual data which can be integer or string pointer

   // --- String data ---
   private: char p_name [ ActiveEffect_NAME_LEN];
   private: char p_condition [ ActiveEffect_CONDITION_LEN];
   private: char p_kaomoji [ ActiveEffect_KAOMOJI_LEN ];


   // --- Constructors ---

   public: ActiveEffect ( void );
   public: ~ActiveEffect ( void );

   // property methods

   public: char* name ( void ) { return ( p_name ); };
   public: void name ( char *str ) { strncpy ( p_name, str, ActiveEffect_NAME_LEN ); };
   public: char* condition_str ( void ) { return ( p_condition ); };
   public: void condition_str ( char *str ) { strncpy ( p_condition, str, ActiveEffect_NAME_LEN ); };
   public: char*  kaomoji ( void ) { return ( p_kaomoji ); };
   public: void kaomoji ( char *str ) { strncpy ( p_kaomoji, str, ActiveEffect_KAOMOJI_LEN ) ; };

   public: int expiration ( void) { return ( get_int (1) ); };
   public: void expiration ( int value ) { set_int (1, value); };
   public: int duration ( void ) { return ( get_int (2) ); };
   public: void duration ( int value ) { set_int ( 2, value ); };
   public: int condition ( void ) { return ( get_int (3) ); };
   public: void condition ( int value ) { set_int ( 3, value ); };
   public: int category ( void) { return ( get_int (4) ); };
   public: void category ( int value ) { set_int (4, value); };
   public: int AD ( void) { return ( get_int (5) ); };
   public: void AD ( int value ) { set_int (5, value); };
   public: int MD ( void) { return ( get_int (6) ); };
   public: void MD ( int value ) { set_int (6, value); };
   public: int PS ( void) { return ( get_int (7) ); };
   public: void PS ( int value ) { set_int (7, value); };
   public: int MS ( void) { return ( get_int (8) ); };
   public: void MS ( int value ) { set_int (8, value); };
   public: int melee_CS ( void) { return ( get_int (9) ); };
   public: void melee_CS ( int value ) { set_int (9, value); };
   public: int range_CS ( void) { return ( get_int (10) ); };
   public: void renge_CS ( int value ) { set_int (10, value); };
   public: int DR ( void) { return ( get_int (11) ); };
   public: void DR ( int value ) { set_int (11, value); };
   public: int INIT ( void) { return ( get_int (12) ); };
   public: void INIT ( int value ) { set_int (12, value); };
   public: int DMG ( void) { return ( get_int (13) ); };
   public: void DMG ( int value ) { set_int (13, value); };
   public: int MDMG ( void) { return ( get_int (14) ); };
   public: void MDMG ( int value ) { set_int (14, value); };
   public: int attdiv ( void) { return ( get_int (15) ); };
   public: void attdiv ( int value ) { set_int (15, value); };
   public: int powdiv ( void) { return ( get_int (16) ); };
   public: void powdiv ( int value ) { set_int (16, value); };

   public: int HP ( void) { return ( get_int (17) ); };
   public: void HP ( int value ) { set_int (17, value); };
   public: int MP ( void) { return ( get_int (18) ); };
   public: void MP ( int value ) { set_int (18, value); };
   public: int modulo ( void) { return ( get_int (19) ); };
   public: void modulo ( int value ) { set_int (19, value); };

   public: int target_type ( void) { return ( get_int (21) ); };
   public: void target_type ( int value ) { set_int (21, value); };
   public: int target_id ( void) { return ( get_int (22) ); };
   public: void target_id ( int value ) { set_int (22, value); };
   public: int template_id ( void) { return ( get_int (23) ); };
   public: void template_id ( int value ) { set_int (23, value); };
   public: int time ( void) { return ( get_int (24) ); };
   public: void time ( int value ) { set_int (24, value); };



   // virtual method from SQL enchanced object
   protected: void virtual template_load_completion ( void );

   protected: void virtual callback_handler ( int index );
   protected: void cb_persist_effect (void);

   //----- Static Method -----

   public: static void trigger_expiration ( int situation );
   public: static void trigger_disturb_expiration ( Opponent *target );
   public: static void trigger_expiration_by_death ( Opponent *target );
   public: static void reduce_time ( int minute );
   public: static void trigger_modulo_effects ( void ); // assume 1 minute, maybe not triggered in city for now
   public: static bool add_effect ( int effect_id , Opponent *target );
};

/*-------------------------------------------------------------------*/
/*-                         Structures                              -*/
/*-------------------------------------------------------------------*/

extern s_EnhancedSQLobject_strfield STRFLD_CONDITION [ EnhancedSQLobject_HASH_SIZE ];
extern const char STR_AEFFECT_EXPIRATION [ 6 ] [ 15 ];


#endif // AEFFECT_H_INCLUDED
