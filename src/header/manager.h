/***************************************************************************/
/*                                                                         */
/*                         M A N A G E R . H                               */
/*                        Class Definition                                 */
/*                                                                         */
/*     Content : Calss Manager                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : July 24th, 2003                                     */
/*     License : GNU General Public LIcense                                */
/*                                                                         */
/*          This class is an engine that manage the Players account and    */
/*     the started games.                                                  */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                       Class Parameter                                 -*/
/*-------------------------------------------------------------------------*/

/*&#define Manager_NB_MAX_ADVENTURE 32
#define Manager_NB_MAX_GAME      32
#define Manager_NB_MAX_ACCOUNT   64
#define Manager_NB_MAX_RACE      32
#define Manager_NB_MAX_CLASS     32*/

#define Manager_FILENAME_LEN 51
#define Manager_MAX_NB_FILE  128
#define Manager_TMP_STR_LEN    256

#define Manager_INTRO_SCROLL_REST   25
#define Manager_ENDING_SCROLL_REST   25
#define Manager_CREDIT_SCROLL_REST   5
#define Manager_INTRO_SCROLL_PAUSE   5000
#define Manager_ENDING_SCROLL_PAUSE  7000
#define Manager_ENDING_THEND_PAUSE  3000

#define Manager_INTEND_NB_PARAGRAPH    16

/*-------------------------------------------------------------------------*/
/*-                          Constants                                    -*/
/*-------------------------------------------------------------------------*/

// manager menu

/*#define Manager_MENU_LOGIN             0
#define Manager_MENU_ADVENTURE         1
#define Manager_MENU_GAME              2
#define Manager_MENU_ACCOUNT           3
#define Manager_MENU_RACE              4
#define Manager_MENU_CLASS             5*/


#define Manager_MENU_CREATE_GAME       0
#define Manager_MENU_LOAD_GAME         1
#define Manager_MENU_INSPECT_GAME      2 //?? maybe include in load
#define Manager_MENU_DELETE_GAME       3
#define Manager_MENU_EXIT              -1

// Manager Sub Menu
/*
#define Manager_SUBMENU_CREATE         0
#define Manager_SUBMENU_INSPECT        1
#define Manager_SUBMENU_DELETE         2
#define Manager_SUBMENU_EXIT1          3
#define Manager_SUBMENU_IMPORT         3
#define Manager_SUBMENU_EXPORT         4
#define Manager_SUBMENU_EXIT2          5*/

// manager return code

//#define Manager_CONTINUE   0 // stay in the manager menu
//#define Manager_PLAY       1 // game is active
//#define Manager_EXIT       2

/*-------------------------------------------------------------------------*/
/*-                       Class Definition                                -*/
/*-------------------------------------------------------------------------*/

class Manager
{
   //--- properties ---

   //private: int p_account_logged; // successfull account login number
   private: char p_savegamename [ Manager_FILENAME_LEN ];

   //--- Constructor & Destructor ---

   public: Manager ( void );
   public: ~Manager ( void );

   //--- Property Methods ---

   //public: int account_logged ( void );
   //public: int nb_game ( void );
   //public: int nb_account ( void );
   //public: int nb_adventure ( void );
   //public: int nb_race ( void );
   //public: int nb_class ( void );

   //--- Methods ---

   public: int start ( void );
   public: void close_game ( void);
   private: int start_game ( void );
   public: int start_last_game (void);

   //public: int show_login ( void );

   //public: void show_adventure ( void );
   //public: void show_game ( void );
   //public: void show_account ( void );
   //public: void show_race ( void );
   //public: void show_class ( void );

   //public: void show_create_adventure ( void );
   //public: void show_inspect_adventure ( void );
   //public: void show_delete_adventure ( void );
   private: void show_create_game ( void );
   private: void show_inspect_game ( void );
   private: void show_delete_game ( void );
   private: int show_load_game ( void );

   private: int get_savegamename ( const char *purpose );
   //public: void show_create_account ( void );
   //public: void show_inspect_account ( void );
   //public: void show_delete_account ( void );
   //public: void show_create_race ( void );
   //public: void show_inspect_race ( void );
   //public: void show_delete_race ( void );
   //public: void show_import_race ( void );
   //public: void show_export_race ( void );
   //public: void show_create_class ( void );
   //public: void show_inspect_class ( void );
   //public: void show_delete_class ( void );
   //public: void show_import_class ( void );
   //public: void show_export_class ( void );



   public: void show_intro ( void );
   public: void show_ending ( void );


};

/*-------------------------------------------------------------------------*/
/*-                           Global Variables                            -*/
/*-------------------------------------------------------------------------*/

//extern Manager manager;
extern char wizcredits [ 1501 ];

