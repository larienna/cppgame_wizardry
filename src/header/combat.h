/***************************************************************************/
/*                                                                         */
/*                             C O M B A T . H                             */
/*                             Class Definition                            */
/*                                                                         */
/*     Content : Class Combat                                              */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : July 2nd, 2002                                      */
/*                                                                         */
/*          This class contain exclusively the combat engine. It use       */
/*     opponent classes to make the battle between characters and monsters */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                          Class Parameter                              -*/
/*-------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------*/
/*-                            Constants                                  -*/
/*-------------------------------------------------------------------------*/

// combat return value

#define Combat_VICTORY     0
#define Combat_DEFEATED    1
#define Combat_RUN         2

#define Combat_IDENTIFY_PROBABILITY    25    //in percentage

#define Combat_OBJECTID_GOLD 1025 // picture ID for gold reward

/*-------------------------------------------------------------------------*/
/*-                       Type Definition                                 -*/
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/*-                     Global Variables Definitions                      -*/
/*-------------------------------------------------------------------------*/

//extern s_Combat_command COMBAT_COMMAND [ Combat_COMMAND_SIZE ];

/*-------------------------------------------------------------------------*/
/*-                         Class Defintion                               -*/
/*-------------------------------------------------------------------------*/

class Combat
{

   //--- Properties ---

   //private: Character p_character [ 6 ]; // list of player's characters
   //private: int p_nb_character;
   //private: Ennemy p_ennemy [ 6 ]; // list of ennemy opponents
   //private: int p_nb_ennemy;

   //private: int p_char_party;
   //private: int p_enmy_party;
   // all list of character and ennemy. Save/update characters
   // but delete ennemies and their party.

   private: int p_pictureID [ 2 ]; // retained picture ID of the monsters.
   private: int p_nb_picture; // number of picture to draw on the screen.

   //--- Constructors & Destructors ---

   public: Combat ( void );
   public: ~Combat ( void );

   //--- Propetry Methods ---

//   public: int nb_character ( void );
//   public: int nb_ennemy ( void );

   //--- Methods ---

//   public: void add_character ( int character );
   //public: void add_character_party ( int party );
//   public: void add_ennemy ( int ennemy );
   //public: void add_ennemy_party ( int party );
//   public: void save_parties ( void );
   //public: void reselect_opponents ( void );
   //public: void clear ( void );
   //public: void add_reward ( void );
   public: void show_cimetary ( void );
//   public: void activesort_characters ( void );
//   public: void activesort_ennemies ( void );
//   public: void remove_character ( int index );
//   public: void remove_ennemy ( int index );

   public: void start ( void );

   public: void load_enemy_picture ( void );
   public: void draw_enemy ( void );
   private: void give_rewards ( void );
   private: void identify_monsters ( void ); // attempt to identify each monster.

   //--- Private Methods ---

//   private: void draw_combat_screen ( void );


};

//extern const char STR_CMB_STATUS [] [ 16 ];

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

extern Combat combat;

extern const int Combat_MONSTER_EXP_VALUE [ 7 ];
//extern const float Combat_MONSTER_EXP_BONUS_VALUE [ 7 ];
