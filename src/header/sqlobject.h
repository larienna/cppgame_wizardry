/************************************************************************/
/*                                                                      */
/*                        S Q L o b j e c t . h                         */
/*                                                                      */
/*   Content: Class SQLobject                                           */
/*   Programmer: Eric Pietrocupo                                        */
/*   Starting Date: April, 20th, 2012                                   */
/*                                                                      */
/*   SQLobject is a wrapper for the sqlite library for data objects.    */
/*   The idea is to have a similar interface to my DDT library to make  */
/*   Sure the transition will be done more easily.                      */
/*                                                                      */
/*   All data objects must inherit from SQLobject in order to integrate */
/*   the SQLlite functionnality. The idea is to make the management of  */
/*   sqlite database easier in the programm.                            */
/*                                                                      */
/************************************************************************/

/* instructions

   it is imported that the derived class define

   p_tablename : which is the name of the table that will be used in queries
   p_template_tablename : Used for loading record from a template. Set only if used.

   and functions:
   void virtual sql_to_obj (void) = 0;
   void virtual template_sql_to_obj (void) = 0;
   void virtual obj_to_sqlupdate (void) = 0; // format : namex=x, namey=y, namez=z
   void virtual obj_to_sqlinsert (void) = 0; // format : x, y, z

   that are used to convert the query data into the object or the objec data into a query.

*/

#ifndef SQLOBJECT_H_INCLUDED
#define SQLOBJECT_H_INCLUDED


/*----------------------------------------------------------------------*/
/*-                         Class Definition                           -*/
/*----------------------------------------------------------------------*/

class SQLobject
{

   // Properties

   protected: int p_primary_key; // default value -1 when not a database object
   protected: sqlite3_stmt *p_sqlstatement;
   //protected: const char *p_sqltail;
   protected: int p_sqlerror;
   protected: char p_tablename [21];
   protected: char p_querystr [SQL_QUERYSTR_LEN];
   protected: char p_fieldstr [SQL_QUERYSTR_LEN];
   protected: bool p_isprepared;
   protected: char p_template_tablename [21];
   protected: bool p_istemplate;


   // Constructor/destructor

   public: SQLobject ( void );
   public: virtual ~SQLobject ( void );

   // Property Methods

   public: void primary_key ( int key ); // use in extreme situations only
   public: int primary_key ( void );
   public: int SQLerror ( void );
   public: void SQLerrormsg ( void );

   // Methods


   public: int SQLprepare (const char *conditionstr = ""); // after table name
   public: int SQLpreparef ( const char *format, ... ); // after table name
   public: int template_SQLprepare (const char *conditionstr = ""); // after table name
   public: int template_SQLpreparef ( const char *format, ... ); // after table name
   public: int SQLstep (void);
   public: int SQLfinalize (void);
   protected: int SQLcolumn_int ( int colID );
   protected: const char* SQLcolumn_text ( int colID );



   public: int SQLselect (int key);

   public: int template_SQLselect ( int key);
   public: int SQLupdate (void);
   public: int SQLinsert (void);
   public: int SQLinsert ( int key ); // specific primary key,
   public: int SQLdelete (void);

   public: int refresh ( void ) { return ( SQLselect ( p_primary_key) ); }

   // Callback Caller Methods

   public: int SQLprocedure ( int index, const char *conditionstr = "" );
   //public: int SQLproceduref ( int (*proc)(void) , const char *format, ... );


   //public: int SQLinsert_blob

   // Virtual Methods

   // object relational virtual methods
   protected: void virtual sql_to_obj (void) = 0;
   protected: void virtual template_sql_to_obj (void) = 0;
   protected: void virtual obj_to_sqlupdate (void) = 0; // format : namex=x, namey=y, namez=z
   protected: void virtual obj_to_sqlinsert (void) = 0; // format : x, y, z

   protected: void virtual callback_handler ( int index );

};



/*-----------------------------------------------------------------------*/
/*-                               Procedures                            -*/
/*-----------------------------------------------------------------------*/

// These functions could be used for database common commands like "COMMIT" that would
// wrap the sqlite_exec function.


#endif // SQLOBJECT_H_INCLUDED

/* documentation

check DBobject.h, might need to add feature eventually according to the needs
try to free pointers and initialise PK to -1 in construct/destruct.

*/
