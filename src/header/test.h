/************************************************************************/
/*                                                                      */
/*                        T E S T . H                                   */
/*                                                                      */
/*   Content:  test methods                                             */
/*   Programmer: Eric Pietrocupo                                        */
/*   Starting Date: October, 14th, 2017                                 */
/*                                                                      */
/*   List of testing method to be called from the main in order to test */
/*   various features. Also liberate the main from this junk code.      */
/*                                                                      */
/************************************************************************/


#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

/** Contains various test, was originally the main method to do all tests
* before separating the tests into various methods */
void alternate_testing_main(void);

/** Empty callback for testing */
int test_callback ( void );

/** This is a procedure used for converting bitmap for datafile processing
*   but it is not used by the game once released */
void tmpmain_convert_bitmap(void);

/** Test methods to debug the new record set system */
void recordset_test_main ( void );



#endif // TEST_H_INCLUDED
