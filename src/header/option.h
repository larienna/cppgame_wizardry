/***************************************************************************/
/*                                                                         */
/*                             O P T I O N . H                             */
/*                            Class definition                             */
/*                                                                         */
/*     Content : Class Option                                              */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : May 12nd, 2002                                      */
/*                                                                         */
/*          This is an option menu engine which looks close to menu but    */
/*     change the states of each inten instead of returning the selected   */
/*     choice.                                                             */
/*                                                                         */
/***************************************************************************/

#ifndef OPTION_H_INCLUDED
#define OPTION_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*-                       Constant Class Parameter                        -*/
/*-------------------------------------------------------------------------*/

#define Option_FONT           FNT_print
#define Option_HEADER_FONT    FNT_small
#define Option_TITLE_SIZE     81
#define Option_DISPLAY_SIZE   81
#define Option_TEXT_SIZE      51
#define Option_ELEM_SIZE      31
#define Option_HEADER_STR_SIZE     121
#define Option_HEADER_SPACE        2
#define Option_COLOR_TEXT          makecol ( 225, 225, 225 )
#define Option_COLOR_DISABLED      makecol ( 150, 150, 150 )
#define Option_NB_HEADER      6


/*-------------------------------------------------------------------------*/
/*-                          Type definition                              -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Option_item
{
   char text [Option_TEXT_SIZE]; // Text written for the menu item
   int nb_elem; // Contains the number of choice
   int selected; // curently selected choice
   int key; // primary key of entry in the database
   char elem[ 5 ][ Option_ELEM_SIZE ]; // Contains the text of the choice
   s_Option_item *ptr_next; // pointer on the nextitem in the list
}s_Option_item;

typedef struct s_Option_result
{
   int selected;
   int key;
}s_Option_result;

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class Option
{

   // Properties

   private: char p_title [Option_TITLE_SIZE]; // Text written at the top of the menu
   private: char p_quit [Option_TEXT_SIZE]; // text for last choice that exit option menu
   private: int  p_nb_item; // count number of add items
   private: s_Option_item *ptr_first; // Point on the 1st menu item
   private: s_Option_item *ptr_last; // Point on the last menu item
   private: bool p_showheader; // determine if a header must be displayed
   private: char p_header [Option_NB_HEADER][ Option_HEADER_STR_SIZE ]; // text of the header

   // Constructor & Destructor

   public: Option ( void );
   public: Option ( const char* str );
//   public: Option ( string &str );
   public: ~Option ( void );

   // Property Methods

//   public: void title ( string &str );
   public: void title ( const char* str );
//   public: void quit ( string &str );
   public: void quit ( const char* str );
   public: int nb_item ();
   public: int nb_element ( int itemID );
   public: int selected ( int itemID );
   public: void selected ( int itemID, int value );
   public: int key ( int itemID );
   public: void key ( int itemID, int value );
   public: s_Option_result result( int itemID );

   public: void header ( const char *text1,  const char *text2, const char *text3,
                        const char *text4, const char *text5, const char *text6 );
   public: bool showheader ( void ); // indicate if header will be shown



   // Methods

   //note : string size 20 Character, element size 8 character
//   public: void add_item ( string &str, string &elem1, string &elem2,
//             string &elem3, string &elem4, string &elem5, int selected = 0 );
   public: void add_item ( int key, const char* str, const char* elem1, const char* elem2,
     const char* elem3, const char* elem4, const char* elem5, int selected = 0 );
   public: void show ( short x, short y, bool readonly = false );

   // Private Methods

   private: void select_right ( int itemID );
   private: void select_left ( int itemID );

};

#endif
