/**
 * t e x t o u t p a t c h . h
 *
 * @author Eric Pietrocupo
 * @date   January 13th, 2018
 *
 * This module patch the allegro textout_old functions by trying to design wrapper
 * to fix the fact that all the mew textout_old function requires an additional
 * argument with the removal of text_mode function.
 *
 * It will still require renaming the textout_old methods using search replace, but
 * it will auto add the additional parameter by itself. Else the new code will
 * need to use the methods that ends with _ex. They can possibly be modified
 * when updating the code.
 */

#ifndef TEXTOUTPATCH_H_INCLUDED
#define TEXTOUTPATCH_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

// --- Constants ---

#define TEXTOUT_BUFFER_MAXSIZE   256 // temporary buffer to convert printf to textout_old.

// --- Public Methods ---

/**
 * Attemps to simulate the original text mode methods, but I don't know the
 * original value returned by this function, so I returned 0;
 *
 * @param  mode   ne color of the background
 * @return ?Unknown?
 */
int text_mode_old ( int mode );

void textout_old(BITMAP *bmp, const FONT *f, const char *s, int x, int y, int color );
void textout_centre_old(BITMAP *bmp, const FONT *f, const char *s, int x, int y, int color );
void textout_right_old(BITMAP *bmp, const FONT *f, const char *s, int x, int y, int color );
void textout_justify_old(BITMAP *bmp, const FONT *f, const char *s, int x1, int x2, int y, int diff, int color );

void textprintf_old(BITMAP *bmp, const FONT *f, int x, int y, int color, const char *fmt, ...);
void textprintf_centre_old(BITMAP *bmp, const FONT *f, int x, int y, int color, const char *fmt, ...);
void textprintf_right_old(BITMAP *bmp, const FONT *f, int x, int y, int color, const char *fmt, ...);
void textprintf_justify_old(BITMAP *bmp, const FONT *f, int x1, int x2, int y, int diff, int color, const char *fmt, ...);





#ifdef __cplusplus
}
#endif


#endif // TEXTOUTPATCH_H_INCLUDED
