/***************************************************************************/
/*                                                                         */
/*                              S Y S T E M . H                            */
/*                             Module Definition                           */
/*                                                                         */
/*     Content : Module System                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 3rd, 2002                                  */
/*                                                                         */
/*          System procedure and variables not related to allegro.         */
/*                                                                         */
/***************************************************************************/

#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*-                     Global Constant Parameters                        -*/
/*-------------------------------------------------------------------------*/


#define System_NB_MUSIC             39
#define System_NB_FONT              23
#define System_NB_DATAFILE          32
#define System_NB_TEXTURE_SET       16
#define System_NB_ADVDATAFILE       16
//#define System_NB_TEXTURE_CATEGORY      2

/*-------------------------------------------------------------------------*/
/*-                            Constants                                  -*/
/*-------------------------------------------------------------------------*/

// general constant

#ifndef VERSION //the real value is received from gcc parameter
   #define VERSION "unset"
#endif

#define YEAR         "2002-2004,2012-"

// constant font definition
#define General_FONT_PARTY          FNT_print
#define General_FONT_INPUT          FNT_script
#define General_FONT_INSTRUCTION    FNT_small
#define System_FONT_NORMAL          FNT_print

//#define General_FILELIST_SIZE    128


// music track identifier

#define System_MUSIC_w1camp      0
#define System_MUSIC_w1city      1
#define System_MUSIC_w1fight     2
#define System_MUSIC_w1credit    3
#define System_MUSIC_w1dungon    4
#define System_MUSIC_w1edge      5
#define System_MUSIC_w1ending    6
#define System_MUSIC_w1inn       7
#define System_MUSIC_w1intro     8
#define System_MUSIC_w1lvl10     9
#define System_MUSIC_w1shop      10
#define System_MUSIC_w1tavern    11
#define System_MUSIC_w1temple    12
#define System_MUSIC_w1victor    13
#define System_MUSIC_w2camp      14
#define System_MUSIC_w2city      15
#define System_MUSIC_w2death     16
#define System_MUSIC_w2dungon    17
#define System_MUSIC_w2edge      18
#define System_MUSIC_w2ending    19
#define System_MUSIC_w2fight     20
#define System_MUSIC_w2inn       21
#define System_MUSIC_w2intro     22
#define System_MUSIC_w2shop      23
#define System_MUSIC_w2tavern    24
#define System_MUSIC_w2temple    25
#define System_MUSIC_w3boss      26
#define System_MUSIC_w3camp      27
#define System_MUSIC_w3city      28
#define System_MUSIC_w3death     29
#define System_MUSIC_w3dungon    30
#define System_MUSIC_w3edge      31
#define System_MUSIC_w3ending    32
#define System_MUSIC_w3fight     33
#define System_MUSIC_w3inn       34
#define System_MUSIC_w3intro     35
#define System_MUSIC_w3shop      36
#define System_MUSIC_w3tavern    37
#define System_MUSIC_w3temple    38

#define System_GAME_STOP   0
#define System_GAME_START  1
#define System_GAME_EXIT   2


// Attack elemental property

// magical-physical
#define PROPERTY_NONE        0
#define PROPERTY_PHYSICAL    1     // 00000000000000000000000000000001 = 1
#define PROPERTY_MENTAL      1<<1  // 00000000000000000000000000000010 = 2
#define PROPERTY_FIRE        1<<2  // 00000000000000000000000000000100 = 4
#define PROPERTY_ICE         1<<3  // 00000000000000000000000000001000 = 8
#define PROPERTY_LIGHTNING   1<<4  // 00000000000000000000000000010000 = 16
#define PROPERTY_POISON      1<<5  // 00000000000000000000000000100000 = 32
#define PROPERTY_RESERVED1   1<<6  // 00000000000000000000000001000000 = 64
#define PROPERTY_RESERVED2   1<<7  // 00000000000000000000000010000000 = 128

//Health Status Category
#define PROPERTY_BODY        1<<8  // 00000000000000000000000100000000 = 256
#define PROPERTY_BLOOD       1<<9  // 00000000000000000000001000000000 = 512
#define PROPERTY_SOUL        1<<10 // 00000000000000000000010000000000 = 1024
#define PROPERTY_NEURAL      1<<11 // 00000000000000000000100000000000 = 2048
#define PROPERTY_PSYCHIC     1<<12 // 00000000000000000001000000000000 = 4096
#define PROPERTY_SKIN        1<<13 // 00000000000000000010000000000000 = 8192
#define PROPERTY_RESERVED3   1<<14 // 00000000000000000100000000000000 = 16384
#define PROPERTY_RESERVED4   1<<15 // 00000000000000001000000000000000 = 32768

//basic undead use: 15104 resist all except soul
//physical undead add: 42 = 15146  resist mental, ice and poison
//Spirit Undead add: 43 = 15147  resist physical, mental, ice and poison

//Creature Type
#define PROPERTY_DRAGON     1<<16  // 00000000000000010000000000000000 = 65536
#define PROPERTY_UNDEAD     1<<17  // 00000000000000100000000000000000 = 131072
#define PROPERTY_BEAST      1<<18  // 00000000000001000000000000000000 = 262144
#define PROPERTY_OUTSIDER   1<<19  // 00000000000010000000000000000000 = 524288
#define PROPERTY_ANIMAL     1<<20  // 00000000000100000000000000000000 = 1048576
#define PROPERTY_GIANT      1<<21  // 00000000001000000000000000000000 = 2097152
#define PROPERTY_SHAPECHANGER 1<<22  // 00000000010000000000000000000000 = 4194304
#define PROPERTY_VERMIN     1<<23  // 00000000100000000000000000000000 = 8388608

//idea giant++, goblinoid, animal, insects
//more were if restore were tiger and were bears
//dire is useless., magic has few for now. same thing for mythical

//Class Type + other
#define PROPERTY_WARRIOR    1<<24  // 00000001000000000000000000000000 = 16777216
#define PROPERTY_ROGUE      1<<25  // 00000010000000000000000000000000 = 33554432
#define PROPERTY_MAGICUSER  1<<26  // 00000100000000000000000000000000 = 67108864
#define PROPERTY_HUMANOID   1<<27  // 00001000000000000000000000000000 = 134217728
#define PROPERTY_PLANT      1<<28  // 00010000000000000000000000000000
#define PROPERTY_OOZE       1<<29  // 00100000000000000000000000000000
#define PROPERTY_MAGICKAL   1<<30  // 01000000000000000000000000000000


// defense type

#define DEFENSE_MAGIC_DEFENSE      0x0001 // apply magic defense (default)
#define DEFENSE_PHYSICAL_SAVE      0x0002 // apply phisical save
#define DEFENSE_MENTAL_SAVE        0x0004 // apply mental save
#define DEFENSE_DAMAGE_RESISTANCE  0x0008 // apply damage resistance (default)
#define DEFENSE_ACTIVE_DEFENSE     0x0010 // apply active defense to avoid effect
#define DEFENSE_HALF_MAGIC_DEFENSE 0x0100 // divide value above by 2
#define DEFENSE_HALF_PHYSICAL_SAVE 0x0200
#define DEFENSE_HALF_MENTAL_SAVE   0x0400
#define DEFENSE_HALF_DAMAGE_RESISTANCE 0x0800
#define DEFENSE_HALF_ACTIVE_DEFENSE 0x1000


// stats constants

#define D20_NB_STAT        8
#define D20_NB_MODIFIER    4
#define XYZ_NB_STAT        4
#define XYZ_NB_MODIFIER    5
#define FLAT_NB_STAT       8

#define D20STAT_AD      0
#define D20STAT_MD      1
#define D20STAT_PS      2
#define D20STAT_MS      3
#define D20STAT_MELEECS 4
#define D20STAT_RANGECS 5
#define D20STAT_DR      6
#define D20STAT_INIT    7

#define D20MOD_BASE        0
#define D20MOD_ATTRIBUTE   1
#define D20MOD_EQUIP       2
#define D20MOD_AEFFECT     3

#define XYZSTAT_DMG     0
#define XYZSTAT_MDMG    1
#define XYZSTAT_HPDICE  2
#define XYZSTAT_MPDICE  3

#define XYZMOD_X           0
#define XYZMOD_Y           1
#define XYZMOD_ZATTRIBUTE  2
#define XYZMOD_ZEQUIP      3
#define XYZMOD_ZAEFFECT    4

//#define FLAT_WS            0 // total weapon skill according to level
//#define FLAT_MS            1 // total spell skill according to level
#define FLAT_ATTDIV        0 // Clumsiness of class + weapon? and active effect modifications
#define FLAT_POWDIV        1 // Efficiency of class + active effect modification
#define FLAT_CS            2 // total combat skill according to equiped weapon.
#define FLAT_RANGE         3 // max range of the weapon
#define FLAT_MULTIHIT      4 // Penalty applied to each successive attack (ignored for now)
#define FLAT_DEFENSE       5 // defense the target is allowed for a regular attack
#define FLAT_CONDITION     6 // condition effect active on the target.
#define FLAT_ATTACKELEMENT 7 // element used when attacking
//#define FLAT_ATTACK        6
//#define FLAT_POWER         7


// Datafile List Item ID

#define DATF_MONSTER         0
#define DATF_TEXHH_FLOOR     1
#define DATF_TEXHH_WALL      2
#define DATF_EDITOR_ICON     3
#define DATF_SKY             4
#define DATF_VARIOUSIMAGE    5
#define DATF_WINTEX          6
#define DATF_PORTRAITW8      7
#define DATF_FONT            8
#define DATF_CHARACTER       9
#define DATF_MUSICMIDI       10
#define DATF_OBJECT          11
#define DATF_UNIDENTIFIED    12
#define DATF_TEXDAVEGH       13
#define DATF_OBJECTHERHEX    14
#define DATF_SOUNDWIZ        15

// adventure datafile list item ID

#define ADATF_STORYIMG       0


// Fonts Shortcuts

#define FNT_print                      datref_font [ 0 ]
#define FNT_script                     datref_font [ 1 ]
#define FNT_small                      datref_font [ 2 ]
#define FNT_bookantiquabold16          datref_font [ 3 ]
#define FNT_bookantiquabold24          datref_font [ 4 ]
#define FNT_bookmanoldstyle16          datref_font [ 5 ]
#define FNT_bookmanoldstyle24          datref_font [ 6 ]
#define FNT_deutschegothicbold32       datref_font [ 7 ]
#define FNT_deutschegothicbold48       datref_font [ 8 ]
#define FNT_elementarygothicbookhand24 datref_font [ 9 ]
#define FNT_elementarygothicbookhand32 datref_font [ 10 ]
#define FNT_haettenscheweiler16        datref_font [ 11 ]
#define FNT_haettenscheweiler24        datref_font [ 12 ]
#define FNT_helenabold24               datref_font [ 13 ]
#define FNT_helenabold32               datref_font [ 14 ]
#define FNT_monotypecorsiva16          datref_font [ 15 ]
#define FNT_monotypecorsiva24          datref_font [ 16 ]
#define FNT_veckerbold16               datref_font [ 17 ]
#define FNT_veckerbold24               datref_font [ 18 ]
#define FNT_verdanabold16              datref_font [ 19 ]
#define FNT_verdanabold24              datref_font [ 20 ]
#define FNT_watergothic32              datref_font [ 21 ]
#define FNT_watergothic48              datref_font [ 22 ]

// font replacement allias for old system

#define FNT_ambrosia24        FNT_helenabold24
#define FNT_blackchancery16   FNT_bookantiquabold16
#define FNT_blackchancery24   FNT_bookantiquabold24
#define FNT_elgar24           FNT_elementarygothicbookhand24
#define FNT_elgar32           FNT_elementarygothicbookhand32
#define FNT_freehand16        FNT_monotypecorsiva16
#define FNT_freehand24        FNT_monotypecorsiva32
#define FNT_helena24          FNT_helenabold24
#define FNT_helena32          FNT_helenabold32
#define FNT_helvetica22       FNT_verdanabold24
#define FNT_kaufman16         FNT_veckerbold16
#define FNT_kaufman24         FNT_veckerbold24



/*-------------------------------------------------------------------------*/
/*-                                Macros                                 -*/
/*-------------------------------------------------------------------------*/

#ifndef ALLEGRO_WINDOWS
   #define rnd(value) ( ( random()%((value)) ) + 1 )
#else
   #define rnd(value) ( ( rand()%((value)) ) + 1 )
#endif

#ifndef NULL
#define NULL   0
#endif


/*-------------------------------------------------------------------------*/
/*-                           Type Definition                             -*/
/*-------------------------------------------------------------------------*/


typedef struct s_music_track
{
   char name [ 51 ];
  // MIDI* music;
}s_music_track;

typedef struct s_font_list
{
   char name [ 31 ];
   //int dataID;
}s_font_list;

typedef struct s_System_datafile_list_item
{
   DATAFILE *datf; // pointer on the datafile to be loaded
   const char filename [ 51 ];
   bool loaded;
   const char display_text [ 101 ];
   int filesize;
}s_System_datafile_list_item;

typedef struct s_System_Adventure_subdatafile
{
   DATAFILE *datf; // pointer on the subdatafile
   DATAFILE *subobj; // pointer on the subdatafile object
   const char objectname [ 31 ];
   bool loaded;
}s_System_Adventure_subdatafile;

/*-------------------------------------------------------------------------*/
/*-                    Global Variables                                   -*/
/*-------------------------------------------------------------------------*/

//extern DATAFILE *datfaudio; // Audio datafile
//extern DATAFILE *datfmaze; // Maze textures datafile
//extern DATAFILE *datfimage; // Image datafile
//extern DATAFILE *datfeditor; // Editor datafile
//extern DATAFILE *datfennemy; // Ennemy Images
//extern DATAFILE *datffont; // Ennemy Images
//extern DATAFILE *adatf; // main datafile
//extern DATAFILE *datfcharacter; // Character Images


//new datafile system
extern s_System_datafile_list_item datafilelist [ System_NB_DATAFILE ];

extern s_System_Adventure_subdatafile advdatafilelist [ System_NB_ADVDATAFILE ];

//extern DATAFILE *datafile_monster; // monster pictures (new format)

extern DatafileReference<BITMAP*> datref_monster;
extern DatafileReference<BITMAP*> datref_texture;
extern DatafileReference<BITMAP*> datref_editoricon;
extern DatafileReference<BITMAP*> datref_sky;
extern DatafileReference<BITMAP*> datref_image;
extern DatafileReference<BITMAP*> datref_portrait;
extern DatafileReference<FONT*>   datref_font;
extern DatafileReference<BITMAP*> datref_character;
extern DatafileReference<MIDI*>   datref_musicmidi;
extern DatafileReference<BITMAP*> datref_object;
extern DatafileReference<SAMPLE*> datref_sound;
extern DatafileReference<BITMAP*> datref_storyimg;
//extern DatafileReference<BITMAP*> datref_unidentified;
//extern BITMAP *datref2_monster [ 256 ];

// double buffer system

extern BITMAP *buffer; // create a double buffer pointer to the right tripple buffer
//extern BITMAP *last_buffer; // point on the previously used buffer.
extern BITMAP *backup; // create screen backup buffer // should become obsolete
extern BITMAP *mazebuffer; // create a buffer screen only for the maze;
extern BITMAP *editorbuffer; // create a buffer screen for the editor ( allow overlap )
extern BITMAP *subbuffer; // sub bitmap of the buffer for the center of the screen
extern BITMAP *subscreen; // sub bitmap of the screen for the center of the screen.
extern BITMAP *subbackup; // sub bitmap of backup.


//extern BITMAP *triplebuffer[2]; // create 2 buffer ans switch from one to another
//extern int bufferid; // number of the current used buffer.

// Keyboard generic keys;

extern int SELECT_KEY;
extern int CANCEL_KEY;
extern int DISPLAY_KEY;


// System music list

extern s_music_track MUSIC_LIST [ System_NB_MUSIC ];

// other
extern s_font_list FONT_LIST [ System_NB_FONT ];

// Graphic mode offset
extern int System_X_OFFSET;
extern int System_Y_OFFSET;

// cascading menu feature

//extern int System_CASCADE;

extern const char STR_SYS_PROPERTY [][21];

extern BITMAP* no_picture;

extern int DIE_SIZE [7]; // use value 1 to size from size/power of monster. 0 is a dummy.

extern const char STR_SYS_D20STAT [ D20_NB_STAT ][21];
extern const char STR_SYS_XYZSTAT [ XYZ_NB_STAT ][21];

extern const char STR_TEXTURE_SET [ System_NB_TEXTURE_SET ][ 21 ];
extern const char STR_SYS_ATTRIBUTE [ 6 ][15];
extern const char STR_SYS_ATTRIBUTE_CODE [ 6 ][15];



/*-------------------------------------------------------------------------*/
/*-                        Prototype                                      -*/
/*-------------------------------------------------------------------------*/


unsigned int hdice ( unsigned int maxvalue ); // half-dice : N/2 to N
unsigned int dice ( unsigned int maxvalue ); // dice : 1 to N
unsigned int rngdice ( unsigned int minvalue, unsigned int maxvalue ); // range dice : N to M

int roll_xdypz ( int x, int y, int z);
int roll_xpypd20 ( int x, int y );

int bitoint ( unsigned int value ); // can only take the first raised bit
unsigned int intobit ( int value );

int bitoindex ( unsigned int value ); // remove 1 from bitoint result
unsigned int indextobit ( int value ); // add 1 from intobit result

//#define debug ( format, ... ) debug_special ( __func__, format,  ... )
//#define debug ( format ) debug_special ( __func__, format,  )
//#define DEBUG (fmt, args...) debug_special( __func__, #fmt, ##args);

//#define SWAP(a,b)({a ^= b; b ^= a; a ^= b;})
void debug_printf (  const char *function, const char *format, ... );



#endif



