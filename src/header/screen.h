/***************************************************************************/
/*                                                                         */
/*                             S C R E E N . H                             */
/*                                                                         */
/*     Content : Game screen management procedures                         */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting date : May 12nd, 2002                                      */
/*                                                                         */
/*          This module contains many HIgh level procedures called by the  */
/*     main programm to manage the various screen of the games which are   */
/*     not a part of any class engine.                                     */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                             Constants                                 -*/
/*-------------------------------------------------------------------------*/

// title menu selection

#define SCREEN_TITLE_START        0
#define SCREEN_TITLE_CONTINUE     1
#define SCREEN_TITLE_GALLERY      2
#define SCREEN_TITLE_OPTION       3
#define SCREEN_TITLE_EDITOR       4
#define SCREEN_TITLE_SETUP        5
#define SCREEN_TITLE_SOURCES      6
#define SCREEN_TITLE_END_GAME     7

#define SCREEN_SYSMENU_RESOLUTION   0
#define SCREEN_SYSMENU_MIDI         1
#define SCREEN_SYSMENU_EXIT         -1

// art gallery menu selection

#define Screen_GALLERY_TEXTURE   0
#define Screen_GALLERY_MASKTEX   1
//#define Screen_GALLERY_MONSTER   2
#define Screen_GALLERY_OBJECT    2
#define Screen_GALLERY_MUSIC     3
#define Screen_GALLERY_SOUND     4
#define Screen_GALLERY_FONT      5
#define Screen_GALLERY_MONSTER  6
#define Screen_GALLERY_IMAGE     7
#define Screen_GALLERY_SKY       8
#define Screen_GALLERY_EXIT      -1

//#define Screen_LOGO_WAIT_TIME   1500
//#define Screen_LOGO_WAIT_TIME   2000
//#define Screen_COPYRIGHT_WAIT_TIME 8000

#if defined DEBUG_WIZARDRY
   //accelerate debugging, variable defined on the gcc commanline
   #define Screen_LOGO_WAIT_TIME   0
   #define Screen_COPYRIGHT_WAIT_TIME 0
#else
   #define Screen_LOGO_WAIT_TIME   1500
   #define Screen_COPYRIGHT_WAIT_TIME 8000
#endif


// constants for slide and fading function

#define Screen_FADE_OUT       1
#define Screen_FADE_IN        2
//#define Screen_NOFADE_         3

#define Screen_SLIDE_UP       0
#define Screen_SLIDE_RIGHT    1
#define Screen_SLIDE_DOWN     2
#define Screen_SLIDE_LEFT     3


/*-------------------------------------------------------------------------*/
/*-                      Procedure Prototypes                             -*/
/*-------------------------------------------------------------------------*/

void show_copyright_screen ( void );
int show_title_screen ( void );
void show_loading_screen ( const char *title, short percentage );
void show_art_gallery ( void );
void show_sources ( void );
void show_transition_algo7 ( BITMAP *before, BITMAP *after, int delay );
void show_transition_slide_fade ( BITMAP *bmp, int fade , int direction, int speed ); // will slide and fade a bitmap in a certain direction

// system setup routines ( could eventually make a new setup engine
void show_system_setup ( void ); // display auto detected system info and allow changing stuff
void show_system_setup_resolution ( void );
void show_system_setup_midi ( void );

void show_art_gallery_font ( void );
void show_art_gallery_texture ( void );
void show_art_gallery_monster ( void );

void show_art_gallery_music ( void );
void show_art_gallery_sound ( void );

void show_art_gallery_object ( void );
void show_art_gallery_image ( void );
void show_art_gallery_sky ( void );

