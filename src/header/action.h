/***************************************************************************/
/*                                                                         */
/*                             A C T I O N . H                             */
/*                             Class Definition                            */
/*                                                                         */
/*     Content : Class Action                                              */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : September 30th, 2012                                */
/*                                                                         */
/*          This file contains the action class used to keep track of      */
/*     combat actions inside the database.                                 */
/*                                                                         */
/***************************************************************************/

#ifndef ACTION_H_INCLUDED
#define ACTION_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*-                            Constants                                  -*/
/*-------------------------------------------------------------------------*/

// --- target type

#define Action_TARGET_ONEENEMY   5 // 0000 0101    target_ID = table monster ID
#define Action_TARGET_GROUPENEMY 6 // 0000 0110    target_ID = 1 = front, 2 = back
#define Action_TARGET_ALLENEMY   7 // 0000 0111    target_ID is ignored
#define Action_TARGET_ONECHARACTER  9 // 0000 1001 target_ID = PK of character
#define Action_TARGET_GROUPCHARACTER 10 // 0000 1010  target_ID = 1 = front, 2 = back
#define Action_TARGET_ALLCHARACTER  11 // 0000 1011   target_ID is ignored.
#define Action_TARGET_EVERYBODY  15 // 0000 1111      target_ID is ignored.
#define Action_TARGET_UTILITY      256 // 1 0000 0000 target ID is ignored (might need various target: item, character, etc)
// note: maybe add chest, door, NPC with target ID once implemented
// note: some actions will assume certain types because there are no other possibilities.
#define Action_TARGET_SHORT_ONEENEMY        0x0015  // dec 21
#define Action_TARGET_SHORT_GROUPENEMY      0x0016 // dec 22

// --- action range, mostly used by monster special attacks
#define Action_TARGET_MELEE_ONEENEMY       0x0025 // dec 37
#define Action_TARGET_MELEE_GROUPENEMY     0x0026 // dec 38

#define Action_TARGET_MELEE_RANGE          0x0020 // dec 32
#define Action_TARGET_SHORT_RANGE          0x0010 // dec 16
#define Action_TARGET_SELF_RANGE           0x0040 // target one friend but only self

//#define Action_TARGET_LONG_ONEENEMY        0x0035 //default
///#define Action_TARGET_LONG_GROUPENEMY      0x0036  //default

//bit 1-0: None (00), One (01), Group (10), All (11)
//bit 3-2: Friends (10), ennemies (01), both (11)
//bit 5: short range ( for the alchemist
#define Action_TARGET_MASK_QTY          0x0003
#define Action_TARGET_MASK_CHARACTER    0x0008
#define Action_TARGET_MASK_ENEMY        0x0004
#define Action_TARGET_MASK_RANGE        0x00F0 // 16
#define Action_TARGET_MASK_PARTY        0x0100

#define Action_TARGET_QTY_NONE          0x0000
#define Action_TARGET_QTY_ONE           0x0001
#define Action_TARGET_QTY_GROUP         0x0002
#define Action_TARGET_QTY_ALL           0x0003

// --- actor type ---

#define Action_ACTOR_TYPE_UNKNOWN   0 // has no effect when resolved.
#define Action_ACTOR_TYPE_CHARACTER 1
#define Action_ACTOR_TYPE_ENNEMY    2

// --- input return value ---

#define Action_INPUT_RETVAL_NONE     0
#define Action_INPUT_RETVAL_PREVIOUS 1
#define Action_INPUT_RETVAL_RETURN   2
#define Action_INPUT_RETVAL_CANCEL   3
#define Action_INPUT_RETVAL_RUN      4

#define Action_RESOLVE_RETVAL_NONE     0
#define Action_RESOLVE_RETVAL_CANCEL   1 // used when a dead actor cannot perform his command. Or active effect cancel action
#define Action_RESOLVE_RETVAL_EXIT     2 // force an exit engine (ex: exit camp to go back to city)
//#define Action_RESOLVE_RETVAL_SKIPPED  1 //for now, show message showing target is out

/*-------------------------------------------------------------------------*/
/*-                         Class Defintion                               -*/
/*-------------------------------------------------------------------------*/

class Action : public SQLobject
{
   //note: instant actions used outside combat are input and resolved now instead of beign s

  // --- Properties ---

   private: int p_actor_type; // Character/ ennemy or nobody
   private: int p_actorID; // ID number of the person who made the action
   private: int p_commandID; // command ID
   private: int p_initiative; // initiative value + d10 roll
   private: int p_target_type; // target ennemy
   private: int p_targetID; //
   private: int p_value; // value used differently according to the command type

  // --- constructor & destructor ---

   public: Action ( void );
   public: Action ( Character &character );
   public: Action ( Monster &monster );
   public: virtual ~Action ( void );

  // --- Property Methods ---

  // following values have no set method because they are set in constructor and input function.
  public: int actor_type ( void ) { return ( p_actor_type); }
  public: int actorID ( void ) { return ( p_actorID ); }
  public: int commandID ( void ) { return ( p_commandID ); }
  public: int initiative ( void ) { return ( p_initiative ); }

  public: int target_type ( void ) { return ( p_target_type); }
  public: void target_type ( int type ) { p_target_type = type; }
  public: int targetID ( void ) { return ( p_targetID ); }
  public: void targetID ( int ID ) { p_targetID = ID; }
  public: int value ( void ) { return ( p_value ); }
  public: void value ( int val ) { p_value = val; }

  // --- Methods ---

  public: void load_character ( Character &character );
  public: void load_monster ( Monster &monster );
  public: int input ( int commandID ); // called the procedure to handle command input.
                                        // Return false if command cancelled
  public: int resolve ( void ); // call the resolution procedure.
  public: void command_str ( char* tmpstr ); // pass a temporary strong to create the command as a string.

  private: void condition_random_friend ( void );
  private: void condition_random_target ( void );
  private: void condition_random_action ( void );
  private: void chose_new_target ( void );

  // --- Virtual Methods ---

  public: void virtual sql_to_obj (void);
  public: void virtual template_sql_to_obj (void);
  public: void virtual obj_to_sqlupdate (void);
  public: void virtual obj_to_sqlinsert (void);

};

#endif // ACTION_H_INCLUDED
