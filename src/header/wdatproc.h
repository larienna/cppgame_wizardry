/***************************************************************************/
/*                                                                         */
/*                           W D A T P R O C . H                           */
/*                             Module Definition                           */
/*                                                                         */
/*     Content : Module WinData Procedures                                 */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 15th, 2002                                 */
/*                                                                         */
/*          This module contains a set of procedures to be used with       */
/*     the WinData template object.                                        */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                        Macro Definition                               -*/
/*-------------------------------------------------------------------------*/

#define WDatProc_POSITION_PARTY_BAR       0, 355, 640, 125
#define WDatProc_POSITION_ENNEMY_BAR      0, 0, 640, 156
#define WDatProc_POSITION_GALLERY_BITMAP  250, 100, 332, 332
#define WDatProc_POSITION_TEXTURE         250, 100, 268, 268
#define WDatProc_POSITION_GALLERY_FONT    300, 60, 332, 332
#define WDatProc_POSITION_WORLDMAP        0, 32, 412, 412
#define WDatProc_POSITION_CHARACTER       0, 0, 640, 480
#define WDatProc_POSITION_PARTY_GOLD      500, 0, 140, 46
#define WDatProc_POSITION_NEW_CHARACTER   384, 50, 240, 176
#define WDatProc_POSITION_CHARACTER_LIST  0, 0, 640, 340
#define WDatProc_POSITION_PARTY_LIST      0, 86, 640, 394
#define WDatProc_POSITION_NEW_ACCOUNT     400, 60, 240, 400
#define WDatProc_POSITION_INSPECT_ITEM    0, 300, 640, 180
#define WDatProc_POSITION_CHARACTER_STAT  0, 0, 640, 480
#define WDatProc_POSITION_LEVELUP_OLD     0, 100, 320, 380
#define WDatProc_POSITION_LEVELUP_NEW     320, 100, 320, 380
#define WDatProc_POSITION_NEW_RACE        400, 60, 240, 420
#define WDatProc_POSITION_OPPONENT_STAT   0, 0, 640, 480
//#define WDatProc_POSITION_ENNEMY_PICTURE  0, 109, 400, 248
#define WDatProc_POSITION_CCLASS          0, 300, 640, 180
#define WDatProc_POSITION_RACE            0, 300, 640, 180
#define WDatProc_POSITION_ATTRIBUTE_INFO  0, 300, 640, 180
#define WDatProc_POSITION_SYSTEM_INFO     320, 50, 320, 430
#define WDatProc_POSITION_CHARACTER_EQUIP 460, 50, 180, 124
#define WDatProc_POSITION_COMBAT_LOG      0, 0, 640, 160
#define WDatProc_POSITION_CHARACTER_ACTION   400, 156, 240, 112
#define WDatProc_POSITION_SPELL_INFO      180, 16, 454, 438
#define WDatProc_POSITION_ACTIVE_EFFECT_DETAIL    0, 320, 640, 160
#define WDatProc_POSITION_COMBAT_ACTIVE_EFFECT  480, 156, 160, 156


#define WDatProc_stat_color(value) ( makecol ( STAT_COLOR [(value)].red, STAT_COLOR [(value)].green, STAT_COLOR [(value)].blue ) )

/*-------------------------------------------------------------------------*/
/*-                             Constants                                 -*/
/*-------------------------------------------------------------------------*/

// Maybe these colors will be removed
#define WDatProc_COLOR_BASE      makecol ( STAT_COLOR [0].red, STAT_COLOR [0].green, STAT_COLOR [0].blue )
#define WDatProc_COLOR_HEALTH    makecol ( STAT_COLOR [1].red, STAT_COLOR [1].green, STAT_COLOR [1].blue )
#define WDatProc_COLOR_WOUND     makecol ( STAT_COLOR [2].red, STAT_COLOR [2].green, STAT_COLOR [2].blue )
#define WDatProc_COLOR_MAZE      makecol ( STAT_COLOR [3].red, STAT_COLOR [3].green, STAT_COLOR [3].blue )
#define WDatProc_COLOR_SPELL     makecol ( STAT_COLOR [4].red, STAT_COLOR [4].green, STAT_COLOR [4].blue )
#define WDatProc_COLOR_RACE      makecol ( STAT_COLOR [5].red, STAT_COLOR [5].green, STAT_COLOR [5].blue )
#define WDatProc_COLOR_SKILL     makecol ( STAT_COLOR [6].red, STAT_COLOR [6].green, STAT_COLOR [6].blue )
#define WDatProc_COLOR_EQUIPMENT makecol ( STAT_COLOR [7].red, STAT_COLOR [7].green, STAT_COLOR [7].blue )
#define WDatProc_COLOR_OTHER     makecol ( STAT_COLOR [8].red, STAT_COLOR [8].green, STAT_COLOR [8].blue )
#define WDatProc_COLOR_BULK      makecol ( STAT_COLOR [9].red, STAT_COLOR [9].green, STAT_COLOR [9].blue )
//?? not sure what it is, maybe source of bonus like stat matrix.

/*-------------------------------------------------------------------------*/
/*-                             type definition                           -*/
/*-------------------------------------------------------------------------*/

typedef struct s_WDatProc_stat_color
{
   unsigned char red;
   unsigned char green;
   unsigned char blue;
}s_WDatProc_stat_color;

/*-------------------------------------------------------------------------*/
/*-                        prototypes                                     -*/
/*-------------------------------------------------------------------------*/

void WDatProc_party_bar ( int key, int x, int y );
void WDatProc_ennemy_bar ( int key, int x, int y );
void WDatProc_gallery_bitmap ( BITMAP &bmp, short x, short y );
void WDatProc_texture_bitmap ( BITMAP &bmp, short x, short y );
void WDatProc_gallery_font ( BITMAP &bmp, short x, short y );
//void WDatProc_worldmap ( BITMAP &bmp, short x, short y );
void WDatProc_character ( Character &character, short x, short y );
void WDatProc_character_gold ( int key, int x, int y );
void WDatProc_new_character ( Character &character, short x, short y );
//void WDatProc_character_list ( Player &player, short x, short y );
//void WDatProc_character_list_masked ( Player &player, short x, short y );
//void WDatProc_party_list ( Player &player, short x, short y );
//void WDatProc_new_account ( Account &account, short x, short y );
void WDatProc_inspect_item ( int key, int x, int y );
//void WDatProc_character_stat ( Character &character, short x, short y );
void WDatProc_character_levelup ( Character &character, short x, short y );
//void WDatProc_new_race ( Race &race, short x, short y );
//void WDatProc_opponent_stat ( Opponent &opponent, short x, short y );
//void WDatProc_ennemy_picture ( Party &party, short x, short y );

// need anotehr constructor for non reference variables.
void WDatProc_cclass ( int key, int x, int y );
void WDatProc_race ( int key, int x, int y );
void WDatProc_attribute_info ( int key, int x, int y );
//void WDatProc_party_bar2 ( int key, int x, int y );
void WDatProc_system_info ( int key, int x, int y );
void WDatProc_character_equip ( int key, int x, int y);
void WDatProc_combat_log ( int key, int x, int y );
void WDatProc_character_actions ( int key, int x, int y );

void WDatProc_spell_info ( int key, int x, int y );

void WDatProc_active_effect_detail ( int key, int x, int y);
void WDatProc_combat_active_effect ( int ket, int x, int y);

// function to build interfaces but not used by WinData
// transfer in another file if too many of them.

//void InterfaceBuild_Party_Bar ( List &tmplist );

/*-------------------------------------------------------------------------*/
/*-                   Module Usage Procedures                             -*/
/*-------------------------------------------------------------------------*/

//void draw_character_list ( Player &player, short x, short y, bool mask );

/*-------------------------------------------------------------------------*/
/*-                          Global Variables                             -*/
/*-------------------------------------------------------------------------*/

//extern s_WDatProc_stat_color STAT_COLOR [ Opponent_NB_STATYPE ];


