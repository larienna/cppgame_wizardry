//----------------------------------------------------------------
//
//                M O N S T E R C A T G O R Y  . H
//
//   Content: Class Unidentified
//   Programmer: Eric Pietrocupo
//   Starting Date: February 16th, 2014
//
//   Class that manage unidentified monster groups
//
//-----------------------------------------------------------------


#ifndef UNIDENTIFIED_H_INCLUDED
#define UNIDENTIFIED_H_INCLUDED

//------------------------------------------------------------------
//                         Constants
//------------------------------------------------------------------

#define Unidentified_NAME_LEN 33

//------------------------------------------------------------------
//                       Class Definition
//------------------------------------------------------------------

class MonsterCategory : public SQLobject
{
   // Properties

   private: char p_name [ Unidentified_NAME_LEN ];
   private: int p_pictureID;

   // Constructor & Destructor

   public: MonsterCategory ( void );
   public: ~MonsterCategory ( void );

   // Property Methods

   public: char* name ( void ) { return ( p_name); }
   public: int pictureID ( void ) { return (p_pictureID); }

   // Virtural MEthods

   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);


};


#endif // UNIDENTIFIED_H_INCLUDED
