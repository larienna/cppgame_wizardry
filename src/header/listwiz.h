/***************************************************************************/
/*                                                                         */
/*                               L I S T . H                               */
/*                                                                         */
/*     Content : Class List                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : August 25th 2002                                    */
/*                                                                         */
/*          This is the header of the List class which is used to manage   */
/*     different List used during the game. A class has been made to make  */
/*     sure it is reused during the game.                                  */
/*                                                                         */
/*     Requirements : Allegro Video                                        */
/*                    Allegro Text                                         */
/*                    Allegro Keyboard                                     */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                       Constant Class Parameter                        -*/
/*-------------------------------------------------------------------------*/

#define List_FONT                FNT_print
#define List_HEADER_FONT         FNT_small
#define List_COLOR_TEXT          makecol ( 225, 225, 225 )
#define List_COLOR_MASK          makecol ( 150, 150, 150 )
#define List_MAX_NB_ITEM         256
#define List_STR_SIZE            81
#define List_HEADER_STR_SIZE     121
#define List_HEADER_SPACE        2


/*-------------------------------------------------------------------------*/
/*-                           Constants                                   -*/
/*-------------------------------------------------------------------------*/

// add item type
// not sure what used for, remove since data specific
/*#define List_ADD_WEAPON_NAME  0
#define List_ADD_WEAPON_BUY   1
#define List_ADD_ARMOR_NAME   0
#define List_ADD_ARMOR_BUY    1*/

/*-------------------------------------------------------------------------*/
/*-                          Type Definition                              -*/
/*-------------------------------------------------------------------------*/

typedef struct s_List_item
{
   char text [ List_STR_SIZE ]; // Text written for the menu item
   bool mask;   // mask list item if TRUE.
   int answer; // generally contains the primary key of the selected item.
}s_List_item;

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class List
{
   // Properties

   private: char p_title [ List_STR_SIZE ]; // Text written at the top of the List
   private: int p_nb_item; // count number of add items
   private: int p_page_size; // number of lines to draw at the same time.
   private: int p_page_pos; // current location of the page
   private: bool p_sensible; // return selection each time cursor moved
   private: int p_cursor; //current cursor position
   private: s_List_item p_item [ List_MAX_NB_ITEM ]; // strings to draw in the list.
   private: bool p_selected; // contains true when a selection is made in sensible mode
   private: bool p_showheader; // determine if a header must be displayed
   private: char p_header [ List_HEADER_STR_SIZE ]; // text of the header

   // Constructor & Destructor

   public: List ( void );
   public: List ( const char* str, int psize = 3, bool sensible = false );
//   public: List ( string &str, int psize = 3, bool sensible = false );
   public: ~List ( void );

   // Property Methods

//   public: void title ( string &str );
   public: void title ( const char* str );
   public: const char* title ( void );
   public: void page_size ( int value );
   public: int page_size ( void );
   public: void page_pos ( int value );
   public: int page_pos ( void );
   public: int nb_item ();
   public: int char_width ( void );
   public: void sensible ( bool value );
   public: bool sensible ( void );
   public: int cursor ( void );
   public: void cursor ( int value );
   public: bool selected ( void );
   public: void selected ( bool value );
   public: int answer ( int index );
   public: int answer ( void ); // answer of currently selected item
   public: void header ( const char *text );
   public: bool showheader ( void ); // indicate if header will be shown

   // Methods

//   public: void add_item ( string &str, bool mask = false );
   public: void add_item ( int answer, const char* str, bool mask = false );
   public: void add_itemf ( int answer, const char* format, ... );
   public: void add_query ( const char* field, const char* table, const char* condition );
   public: void add_query_multi ( const char* field, const char*table, const char* condition, const char* format, int nb_field);
                 // to be able to use format string, there is a restriction of up to 4 fields. Only use strings in format

   //public: void add_command_list ( Character &tmpchr, unsigned int interface );
   //do the opposite, use character to add items
//   public: void update_item ( string &str, int index, bool mask = false );
   public: void update_selected_item ( int answer, const char *str, bool mask = false );
   public: void update_selected_itemf ( int answer, const char *format, ... );
      // maybe remove, since use index for searching.
   public: void remove_selected_item ();
//   public: void add_item ( Weapon &wdat, int style = List_ADD_WEAPON_NAME,
//                                                         bool mask = false );
//   public: void add_item ( Armor &adat, int style = List_ADD_ARMOR_NAME,
//                                                         bool mask = false );
   public: void mask_item ( int ID );
   public: void unmask_all_item ( void );
   public: void clear_all_item ( void );
   public: int show ( int x, int y, bool reset_cursor = false, bool nocancel = false );

   // SQLite related methods
   // might not bet required, place on standby
//   public: void add_query ( const char* field, const char* table, const char* condition ); // add all the results of a query using
//   public: void add_query_item ( const char* field, const char* table, int key ); // add a specific entry

};



