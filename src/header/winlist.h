/***************************************************************************/
/*                                                                         */
/*                         W I N L I S T . H                               */
/*                         Class Definitnon                                */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinList                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting date : November 18th, 2002                                 */
/*                                                                         */
/*          Class who inherint from window to manage a window with a list  */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                       class definition                                -*/
/*-------------------------------------------------------------------------*/

class WinList : public Window
{
   // --- Property Methods ---

   private: List *p_list; // menu to show in the window
   private: bool p_no_cancel; // no cancel command enabled in menu
   private: bool p_reset_cursor; // reset cursor at start for each show
   //private: short p_cursor; // last cursor position

   // --- Constructor & Destructor ---

   public: WinList ( List &lst, short x_pos, short y_pos,
              bool no_cancel = false, bool reset_cursor = false, bool translucent = false //,
              /*bool sensible = false*/ );
//   public: ~WinList ( void );

   // --- Virtual Methods ---

   public: virtual void preshow ( void ); // this funtion does nothing
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

};
