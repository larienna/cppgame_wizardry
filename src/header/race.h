/***************************************************************************/
/*                                                                         */
/*                                R A C E . H                              */
/*                             Class Definition                            */
/*                                                                         */
/*     Content : Class Race                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : February 7th, 2004                                  */
/*     License : GNU General Public License                                */
/*                                                                         */
/*          This class encapsulate and manage the race information of the  */
/*     character.                                                          */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                          Constants                                    -*/
/*-------------------------------------------------------------------------*/

#define Race_NAME_STR_SIZE    13
#define Race_INITIAL_STR_SIZE 4

// aligment restriction

/*
#define Race_ALIGRESTRICT_KIND         1  // life         kn
#define Race_ALIGRESTRICT_PEACEFULL    2  // water        pf
#define Race_ALIGRESTRICT_LIVELY       4  // Air          lv
#define Race_ALIGRESTRICT_SELLFISH     8  // Death        sf
#define Race_ALIGRESTRICT_AGGRESSIVE   16 // Fire         ag
#define Race_ALIGRESTRICT_STOICAL      32 // Earth        st
*/
// health resistance

//  race size
// race abilities

/*
#define Race_ABILITY_NONE           0
#define Race_ABILITY_AMPHIBIOUS     1 // -wil
#define Race_ABILITY_FLYING         2 // -end
#define Race_ABILITY_DARKVISION     3 // -cun
#define Race_ABILITY_ARMOR_SKIN     4 // -dex
#define Race_ABILITY_BREATH         5 // -dex
#define Race_ABILITY_NATURAL_MAGIC  6 // -int
#define Race_ABILITY_RESIST_MAGIC   7 // -str
#define Race_ABILITY_LUCKY          8 // -end
*/

/*-------------------------------------------------------------------------*/
/*-                        Type Definition                                -*/
/*-------------------------------------------------------------------------*/

/*typedef struct dbs_Race
{
   char name [ 13 ];//  ;
   char plural [ 13 ];//  ;
   int attribute [ 6 ];//  ;
   unsigned char aligment_restriction;//  ;
   short lifespan;//  ;
   unsigned short health_resist;//  ;
   int size;//  ;
   int HPdice;//  ;
   int MPdice;//  ;
   int DMGdice;//  ;
   int MDMGdice;//  ;
   int ability [ 3 ];//  ;
}  dbs_Race;*/

/*typedef struct s_Race_stat_modifier
{
   char name [ 16 ];
   char sname [ 5 ];
}s_Race_stat_modifier;*/


/*-------------------------------------------------------------------------*/
/*-                       Class Definition                                -*/
/*-------------------------------------------------------------------------*/

class Race : public SQLobject
{
   // Properties

   private: char p_name [ Race_NAME_STR_SIZE ];
   private: char p_initial [ Race_INITIAL_STR_SIZE ];
   //private: char p_plural [ 13 ];
   private: int p_attribute [ 6 ];
   //private: unsigned char p_aligment_restriction;
   //private: short p_lifespan;
   private: unsigned int p_resistance;
   private: unsigned int p_weakness;
   private: int p_pictureID; // race picture number
   //private: int p_size;
   //private: int p_HPdice;
   //private: int p_MPdice;
   //private: int p_DMGy;
   //private: int p_MDMGy;

   //private: int p_ability [ 3 ];

   // Constructor & Destructor

   public: Race ( void );
   public: ~Race ( void );

   // Property Method

   public: const char* name ( void );
   public: void name (const char* str);
   public: const char* initial ( void );
   public: void initial (const char* str);



   //public: const char* plural ( void );
   public: int attribute ( int attributeID );
   public: void attribute ( int attributeID, int value );
   //public: short lifespan ( void );
   public: unsigned int resistance ( void );
   public: void resistance ( unsigned int value );
   public: unsigned int weakness ( void );
   public: void weakness ( unsigned int value );
   public: int pictureID ( void );
   public: void pictureID ( int value );
   //public: unsigned char aligment_restriction ( void );
   //public: int size ( void );
   /*public: int HPdice ( void );
   public: int MPdice ( void );
   public: int DMGy ( void );
   public: int MDMGy ( void );*/
   //public: int ability ( int index );

   // Method

   //public: void edit ( void ); // race editor

   // Virtual Methods

   //public: int virtual SQLupdate (void);
   //public: int virtual SQLinsert (void);
   //public: int virtual SQLdelete (void);

   // object relational virtual methods
   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

   // Virtual Methods

   //public: virtual void objdat_to_strdat ( void *dataptr );
   //public: virtual void strdat_to_objdat ( void *dataptr );
   //public: virtual void child_DBremove ( void );


};

/*-------------------------------------------------------------------------*/
/*-                      Global Variables                                 -*/
/*-------------------------------------------------------------------------*/

//extern short LIFE_SPAN [ 5 ];
//extern s_Race_stat_modifier STAT_MODIFIER [ 6 ];
