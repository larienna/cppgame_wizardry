/************************************************************************/
/*                                                                      */
/*                E n h a n c e d S Q L o b j e c t . h                 */
/*                                                                      */
/*   Content: Class EnhancedSQLobject                                   */
/*   Programmer: Eric Pietrocupo                                        */
/*   Starting Date: November, 4th, 2014                                 */
/*                                                                      */
/*   An attempt to encapsulate the management of datafield of the child */
/*   object to avoid the requirement of defining Conversion functions.  */
/*                                                                      */
/*   Backward compatible with the original SQLobject because data obj   */
/*   will inherint from Enhanced SQL obj which will inherit from sqlobj */
/*                                                                      */
/************************************************************************/

#ifndef ENHANCEDSQLOBJ_H_INCLUDED
#define ENHANCEDSQLOBJ_H_INCLUDED

/*----------------------------------------------------------------------*/
/*-                      Constant Definition                           -*/
/*----------------------------------------------------------------------*/

#define EnhancedSQLobject_FIELD_NAME_LEN  21
#define EnhancedSQLobject_DISPLAY_NAME_LEN  31
#define EnhancedSQLobject_STRFIELDCODE_LEN 4
#define EnhancedSQLobject_DISPLAY_LEN 31
#define EnhancedSQLobject_HASH_SIZE 31
#define EnhancedSQLobject_STRFIELD_LEN ( ( EnhancedSQLobject_HASH_SIZE * ( EnhancedSQLobject_STRFIELDCODE_LEN - 1 ) ) + 1 )

// the string match a copy paste from phpliteadmin
#define INTEGER   1
#define TEXT      2
#define STRFIELD  3
//#define REAL      3

/*----------------------------------------------------------------------*/
/*-                      Structure Definitions                         -*/
/*----------------------------------------------------------------------*/

/*typedef struct s_SQLfield_int
{
   char SQLname [EnhancedSQLobject_FIELD_NAME_LEN];

} s_SQLfield_int;

typedef struct s_SQLfield_str
{
   char SQLname [EnhancedSQLobject_FIELD_NAME_LEN];
   int length;
} s_SQLfield_str;*/

// --- new method to put everything into 1 table ---

typedef struct s_EnhancedSQLobject_strfield
{
   char code [ EnhancedSQLobject_STRFIELDCODE_LEN ];
   //unsigned int bitfield; //instead use index to calculate bitfield
   char display [ EnhancedSQLobject_DISPLAY_LEN ];
}s_EnhancedSQLobject_strfield;

typedef struct s_SQLfield // this will be static and constant for the whole class, must put data separately
{
   char SQLname [EnhancedSQLobject_FIELD_NAME_LEN];
   int type; // define if integer string or other like boolean, strfield
   int length; // length of the string, only used for string variables
   //int template_field_id; // field number for the template loading. Not in same order as non-template table.
   char display_name [EnhancedSQLobject_DISPLAY_NAME_LEN]; // name to display on the screen for that field.
   bool display; // if true, display data (used by wdatproc to display or not the field
   s_EnhancedSQLobject_strfield *hash; //pointer on the list of codes
}s_SQLfield;

typedef struct s_SQLdata
{
   int value; // integer value if used
   char *str; // pointer on string of data, if used.
  // double fvalue; // float variable if used.
}s_SQLdata;



/*----------------------------------------------------------------------*/
/*-                         Class Definition                           -*/
/*----------------------------------------------------------------------*/

class EnhancedSQLobject : public SQLobject
{
   // --- Properties ---

   //protected: s_SQLfield_int *p_SQLfield_int_def_ptr[]; // reference on the int SQLfield definition
   //protected: s_SQLfield_str *p_SQLfield_str_def_ptr[]; // reference on the field definition for strings
   //protected: int p_nb_integer; // keep track of the number of sqlfield integer (excluding the primary key)
  // protected: int p_nb_string; // keep track of the number of string SQL field
   //protected: int *p_integer_data_ptr[]; // Reference on a table of integer data;
   //protected: char *p_string_data_ptr[]; // reference on a table of string pointers to data;
   protected: s_SQLfield *p_SQLfield_ptr; // pointer on the field definition
   protected: s_SQLdata *p_SQLdata_ptr; // pointer on the data table
   protected: int p_nb_field;

   private: static const char ERRORSTR [ 6 ];


   // --- constructor and destructor ---

   public: EnhancedSQLobject ( void );
   public: ~EnhancedSQLobject ( void );

   // --- Property Methods ---

   public: void set_all_int ( int value );
   public: void set_all_str ( const char *str );

   public: int  get_int ( int index );
   public: void  set_int ( int index, int value );
   public: const char*  get_str ( int index );
   public: void set_str ( int index, const char *str );
   public: const char* get_SQLname ( int index );
   public: int get_length ( int index);
   public: int get_type ( int index );
   //public: int get_template_field_id ( int index );
   public: const char* get_display_name (int index);
   public: bool get_display ( int index );

   // --- Methods ---

   public: void print_table_structure ( void ); // debuggin methods to see if field structure is accessible.
   public: void strfield_to_bitfield (int index); //Convert a variable into another
   public: void bitfield_to_strfield (int index); //convert a variable into another
   public: static int strfield_to_bitfield ( s_EnhancedSQLobject_strfield hash[], const char *str ); // convert external str
   public: static void bitfield_to_strfield ( s_EnhancedSQLobject_strfield hash[], char *str, int bitfield ); // convert external bitfield

   // --- Virtual Functions Redefinition ---

   // object relational virtual methods from SQLobject
   protected: void virtual sql_to_obj (void);
   protected: void virtual template_sql_to_obj (void);
   protected: void virtual obj_to_sqlupdate (void); // format : namex=x, namey=y, namez=z
   protected: void virtual obj_to_sqlinsert (void); // format : x, y, z

   protected: void virtual template_load_completion ( void ) = 0; // indicates the extra operation required fill in the missing fields.

   // Virtual getter and setter to avoid variables duplication, most variables are get only
   // child virtual function is the same for all child. Work around to virtual variables since dont
   // know the size of the array.

   /*protected: virtual char*  SQLfield_int_name ( int index ) = 0;
   protected: virtual char*  SQLfield_str_name ( int index ) = 0;
   protected: virtual int  SQLfield_str_length ( int index ) = 0;
   protected: virtual int  get_int ( int index ) = 0;
   protected: virtual void  set_int ( int index, int value ) = 0;
   protected: virtual char*  get_str ( int index ) = 0;
   protected: virtual void set_str ( int index, const char *str ) = 0;*/

   //protected: virtual s_SQLfield* get_field ( void ) = 0; // used to get pointer on field structure
   //protected: virtual s_SQLdata* get_data ( void ) = 0; // used to get pointer on field structure
   //protected: virtual int get_nb_field ( void ) = 0; // get the nb of field from the child class.
};

/*---------------------------------------------------------------*/
/*-               hash table for string fields                  -*/
/*---------------------------------------------------------------*/

extern s_EnhancedSQLobject_strfield STRFLD_ELEMENTAL_PROPERTY [ EnhancedSQLobject_HASH_SIZE ];
extern s_EnhancedSQLobject_strfield STRFLD_DEFENSE [ EnhancedSQLobject_HASH_SIZE ];
extern s_EnhancedSQLobject_strfield STRFLD_PROFIENCY [ EnhancedSQLobject_HASH_SIZE ];


#endif // ENCHANCEDSQLOBJ_H_INCLUDED
