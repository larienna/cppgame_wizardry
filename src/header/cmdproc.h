/***************************************************************************/
/*                                                                         */
/*                             C M D P R O C . H                           */
/*                             Procedure Definition                        */
/*                                                                         */
/*     Content : Command Procedures                                        */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : September 30th, 2012                                */
/*                                                                         */
/*          This file contains the procedures required to resolve the      */
/*     character and monster commands. THere is a procedure for the        */
/*     assignment and the resolution.                                      */
/*                                                                         */
/***************************************************************************/

#ifndef CMDPROC_H_INCLUDED
#define CMDPROC_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*-                          Class Parameter                              -*/
/*-------------------------------------------------------------------------*/

#define CmdProc_COMMANDLIST_SIZE         64
#define CmdProc_COMMAND_PREVIOUS         62
#define CmdProc_COMMAND_RETURN           63

/*-------------------------------------------------------------------------*/
/*-                            Constants                                  -*/
/*-------------------------------------------------------------------------*/

// --- command requirements ---

#define CmdProc_REQUIREMENT_NONE      0 // there is no requirements
#define CmdProc_REQUIREMENT_FIRST     1 // need to be first character/monster in the list
#define CmdProc_REQUIREMENT_REACH     2 // a *target is reachable with weapon/regular attack
#define CmdProc_REQUIREMENT_ITEM      3 // possess equiped items (maybe expandable + equipment)
#define CmdProc_REQUIREMENT_SPELL     4 // Is profecient in magic (use value to know what type)
#define CmdProc_REQUIREMENT_NOTALONE  5 // must not be alone in the party.
#define CmdProc_REQUIREMENT_NOTFULL   6 // must not be full in the party
#define CmdProc_REQUIREMENT_NOTFIRST  7 // must not be the first character in the party.
#define CmdProc_REQUIREMENT_CLASS     8 // a class is required, PK of the class in value
#define CmdProc_REQUIREMENT_RACE      9 // a race is required, PK of the race in value
#define CmdProc_REQUIREMENT_CHEATING  10 // when in cheating mode, action enabled.
#define CmdProc_REQUIREMENT_SKILL     11 // a special skill is required.
//#define CmdProc_REQUIREMENT_MONSTER   10 // must be a monster to have access to action.

//?? maybe must be available equipment. status
//note: certain restrictions could be grayed out instead of being removed.

/* this is the list of conditions not verified yet. Only character done so far.
#define CmdProc_REQUIREMENT_FIRST     1 // need to be first character/monster in the list
#define CmdProc_REQUIREMENT_NOTALONE  5 // must not be alone in the party.
#define CmdProc_REQUIREMENT_NOTFULL   6 // must not be full in the party
#define CmdProc_REQUIREMENT_NOTFIRST  7 // must not be the first character in the party.
#define CmdProc_REQUIREMENT_CHEATING  10 // when in cheating mode, action enabled.*/



// --- command available ---

#define CmdProc_AVAILABLE_NEVER     0 // command is never available
#define CmdProc_AVAILABLE_CAMP      1 // available during character camp menu.
#define CmdProc_AVAILABLE_COMBAT    2 // available in combat
#define CmdProc_AVAILABLE_BOTH      3 // available in camp and combat

//#define CmdProc_AVAILABLE_LOCKDOOR  4 // available when there is a door locked
//#define CmdProc_AVAILABLE_ENCOUNTER 8 // ?? available before the start of the battle.
//#define CmdProc_AVAILABLE_TRAP      16 // ?? available before a trap get sprung


/*-------------------------------------------------------------------------*/
/*-                       Type Definition                                 -*/
/*-------------------------------------------------------------------------*/


typedef struct s_CmdProc_command
{
   char name [ 21 ];
   bool usablebydead; // indicated if dead can use this command during camp.
   int initiative; // modification to unitiative results
   int requirement; // general requirement
   int value; // value used by requirement parameter. Mostly ID on a skill or magic type.
   //unsigned int monster_action; // monster unlock this action with their bitfield.
   unsigned int available; // when is this command available.
   int (*proc_input)(Action&); // Procedure used to input the parameters of the action.
   int (*proc_AIinput)(Action&); // Procedure used by the AI to input the action.
   int (*proc_resolve)(Action&, Opponent*, Opponent* ); // Procedure used to resolve the action.
}s_Action_command;

/*typedef struct s_CmdProc_param
{
   int target_type;
   int target_ID;
   int value; // variable according to command
}s_CmdProc_param;
*/

/*-------------------------------------------------------------------------*/
/*-                     Global Variables Definitions                      -*/
/*-------------------------------------------------------------------------*/

extern const s_CmdProc_command CmdProc_COMMANDLIST [ CmdProc_COMMANDLIST_SIZE ];

/*-------------------------------------------------------------------------*/
/*-                            Prototypes                                 -*/
/*-------------------------------------------------------------------------*/

int CmdProc_input_zap ( Action &action);
int CmdProc_AIinput_zap ( Action &action );
int CmdProc_resolve_zap ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_input_fight ( Action &action );
int CmdProc_AIinput_fight ( Action &action );
int CmdProc_resolve_fight ( Action &action, Opponent *actor, Opponent *target  );
//int CmdProc_input_parry ( Action &action );
//int CmdProc_AIinput_parry ( Action &action );
int CmdProc_resolve_parry ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_input_protect ( Action &action );
int CmdProc_AIinput_protect ( Action &action );
int CmdProc_resolve_protect ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_input_run ( Action &action );
int CmdProc_AIinput_run ( Action &action );
int CmdProc_resolve_run ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_input_arcane ( Action &action );
int CmdProc_AIinput_arcane ( Action &action );
int CmdProc_resolve_spell ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_input_divine ( Action &action );
int CmdProc_AIinput_divine ( Action &action );
//int CmdProc_resolve_divine ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_input_alchemy ( Action &action );
int CmdProc_AIinput_alchemy ( Action &action );
//int CmdProc_resolve_alchemy ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_input_psionic ( Action &action );
int CmdProc_AIinput_psionic ( Action &action );
//int CmdProc_resolve_psionic ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_input_use_item ( Action &action );
int CmdProc_AIinput_use_item ( Action &action );
int CmdProc_resolve_use_item ( Action &action, Opponent *actor, Opponent *target  );
//int CmdProc_input_call_help ( Action &action );
//int CmdProc_AIinput_call_help ( Action &action );
int CmdProc_resolve_call_help ( Action &action, Opponent *actor, Opponent *target  );
//int CmdProc_input_equip ( Action &action );
//int CmdProc_AIinput_equip ( Action &action );
int CmdProc_resolve_equip ( Action &action, Opponent *actor, Opponent *target  );
//int CmdProc_input_read ( Action &action );
//int CmdProc_AIinput_read ( Action &action );
int CmdProc_resolve_read_arcane ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_resolve_read_divine ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_resolve_read_alchemy ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_resolve_read_psionic ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_resolve_read_other ( Action &action, Opponent *actor, Opponent *target  );

int CmdProc_do_nothing ( Action &action);
//int CmdProc_resolve_do_nothing ( Action &action);

int CmdProc_resolve_nothing ( Action &action, Opponent *actor, Opponent *target );

int CmdProc_resolve_insane_imitation ( Action &action, Opponent *actor, Opponent *target );
int CmdProc_resolve_insane_sing ( Action &action, Opponent *actor, Opponent *target );
int CmdProc_resolve_disturb ( Action &action, Opponent *actor, Opponent *target );

int CmdProc_input_slap ( Action &action );
int CmdProc_AIinput_slap ( Action &action );
int CmdProc_resolve_slap ( Action &action, Opponent *actor, Opponent *target  );
int CmdProc_resolve_active_effect ( Action &action, Opponent *actor, Opponent *target );

int CmdProc_AIinput_special_attack_spell ( Action &action );


// note: not dure if there should be a separate actions for theses.
int CmdProc_input_previous ( Action &action );
//int CmdProc_AIinput_previous ( Action &action );
//int CmdProc_resolve_previous ( Action &action );
int CmdProc_input_return ( Action &action );
//int CmdProc_AIinput_return ( Action &action );
//int CmdProc_resolve_return ( Action &action );

// Encapsulsated Procedures

void CmdProc_resolve_generic_read ( Action &action, Opponent *actor, int school );
int CmdProc_input_generic_spell ( Action &action, int school );
void CmdProc_build_spell_list ( Action &action, int school, char *querystr, bool party_restriction = false );


#endif // CMDPROC_H_INCLUDED
