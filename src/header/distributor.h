/***************************************************************************/
/*                                                                         */
/*                        D I S T R I B U T O R . H                        */
/*                            Class definition                             */
/*                                                                         */
/*     Content : Class Distributor                                         */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : May 17th, 2012                                      */
/*                                                                         */
/*          This is an distributor menu that is primarily used to assign   */
/*     attributes. The idea that that is allows to split a value into      */
/*     different values.                                                   */
/*                                                                         */
/***************************************************************************/

#ifndef DISTRIBUTOR_H_INCLUDED
#define DISTRIBUTOR_H_INCLUDED

#define Distributor_FONT           FNT_print
#define Distributor_HEADER_FONT    FNT_small
#define Distributor_TITLE_SIZE     81
#define Distributor_TEXT_SIZE      21
#define Distributor_HEADER_STR_SIZE     76
#define Distributor_HEADER_SPACE        2
#define Distributor_COLOR_TEXT          makecol ( 225, 225, 225 )
#define Distributor_COLOR_DISABLED      makecol ( 150, 150, 150 )
#define Distributor_NB_ITEM         16
//#define Distributor_MINMAX_FORMAT   "[%-2d]"
//#define Distributor_VALUE_FORMAT    "%-2d"
//#define Distributor_VALUE_NB_DIGIT  3
#define Distributor_ACCEPT          0
#define Distributor_CANCEL          -1

/*-------------------------------------------------------------------------*/
/*-                          Type definition                              -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Distributor_item
{
   char text [Distributor_TEXT_SIZE]; // Text written for the menu item
   int min; // minimum value
   int max; // maximum value
   int current; // current or starting value
}s_Distributor_item;

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class Distributor
{

   // Properties

   private: char p_title [Distributor_TITLE_SIZE]; // Text written at the top of the menu
   private: char p_total [Distributor_TEXT_SIZE]; // text for the total nb of points to distribute
   private: int  p_nb_item; // count number of add items
   private: int p_total_value; // number of points to distribute
   private: s_Distributor_item p_item [ Distributor_NB_ITEM]; // item information
   private: char p_header [ Distributor_HEADER_STR_SIZE ]; // text of the header (exclude min max)
   private: int p_nb_digit; // hold the number of digit to display

   // Constructor & Destructor

   public: Distributor ( void );
   public: Distributor ( const char* title, const char *header, const char *total, int nb_digit = 2);
   public: ~Distributor ( void );

   // Property Methods


   public: void title ( const char* str );
   public: void header ( const char* str );
   public: void total ( const char* str );
   public: void total_value ( int value );
   public: int total_value ( void );
   public: int nb_item ();
   public: int item_value ( int index );
   public: int nb_digit ( void );
   public: void nb_digit ( int value );



   // Methods

   public: void add_item ( const char *text, int min, int max );
   public: int show ( int x, int y, bool distribute_all = true, bool nocancel = true );

   // Private Methods



};

#endif // DISTRIBUTOR_H_INCLUDED
