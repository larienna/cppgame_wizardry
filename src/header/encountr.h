/***************************************************************************/
/*                                                                         */
/*                          E N C O U N T R . H                            */
/*                            Class Definition                             */
/*                                                                         */
/*     Content : Class Encounter                                           */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 1st, 2003                                  */
/*     License : GNU General Public License                                */
/*                                                                         */
/*          This class create a list of selectable monster for encounter   */
/*     according to previously set parameter. It also contains the engine  */
/*     used before combat starts.                                          */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                           Class Parameters                            -*/
/*-------------------------------------------------------------------------*/

#define Encounter_ELIST_SIZE   256

#define Encounter_BASE_COUNTER 200 //basic value to start counter for monster. NOrmally 200.

/*-------------------------------------------------------------------------*/
/*-                              Constants                                -*/
/*-------------------------------------------------------------------------*/

// list building mask

#define Encounter_MASK_DRAGON     1
#define Encounter_MASK_UNDEAD     1<<1
#define Encounter_MASK_MYTHICAL   1<<2
#define Encounter_MASK_DIVINE     1<<3
#define Encounter_MASK_ANIMAL     1<<4
#define Encounter_MASK_MAGIC      1<<5
#define Encounter_MASK_FORMLESS   1<<6
#define Encounter_MASK_REPTILE    1<<7
#define Encounter_MASK_INSECT     1<<8
#define Encounter_MASK_HUMANOID   1<<9
#define Encounter_MASK_TINY       1<<10
#define Encounter_MASK_SMALL      1<<11
#define Encounter_MASK_MEDIUM     1<<12
#define Encounter_MASK_LARGE      1<<13
#define Encounter_MASK_HUGE       1<<14

// encounter engine return value

#define Encounter_END      0
#define Encounter_IGNORE   1
#define Encounter_ENGAGE   2

// Encounter probability in %
// consider that these probabilities are rolled for each side which is open.
// game difficulty also modifies this value

#define Encounter_PROBABILITY_NONE        0
#define Encounter_PROBABILITY_LOW         1
#define Encounter_PROBABILITY_NORMAL      2
#define Encounter_PROBABILITY_HIGH        3
#define Encounter_PROBABILITY_VERYHIGH    4

// Encounter Attack Side

#define Encounter_SIDE_FRONT 0
#define Encounter_SIDE_RIGHT 1
#define Encounter_SIDE_BACK  2
#define Encounter_SIDE_LEFT  3

/*-------------------------------------------------------------------------*/
/*-                           Type Definition                             -*/
/*-------------------------------------------------------------------------*/


typedef struct s_Encounter_enemy
{
   int id; // primary key of the monsters in the template
   int prob; // probability to encounter add this monster to the list
   int bonus; // bonus points given to that monsters.
}s_Encounter_enemy;

/*-------------------------------------------------------------------------*/
/*-                          Class Definition                             -*/
/*-------------------------------------------------------------------------*/

class Encounter
{
   //--- properties ---

   // note: maybe reselect monsters all the time with a query instead of a duplicate list.

   //private: int p_elist [ Encounter_ELIST_SIZE ]; // selection list of ennemy
   private: int p_nb_enemy; // nb of enemy added to the list
   private: s_Encounter_enemy p_etable [ Encounter_ELIST_SIZE ] [ EnemyGroup_NB_ENEMY ];
   //private: int p_probability; // % of chance to encounter an ennemy 0 to 4 % recommended,
      // value rolled for each side open.
   //private: int p_selection; [ EnemyGroup_NB_ENEMY ]; // Ennemy selected
//   private: int p_side; // ennemy attacking side.
   //private: int p_nb_selected; // nb of ennemies selected
   private: int p_counter; // decrementing value for random.
   //private: bool p_door; // encountered in door.
   //private: int p_party; // temporary party used for transfering ennemies

   //--- Constructor & Destructor ---

   public: Encounter ( void );
   public: ~Encounter ( void );

   //--- Property Methods ---

   public: int nb_enemy ( void );
   //public: void probability ( int value );
   //public: int probability ( void );
   public: int counter ( void );
   public: void counter ( int value ); // necessary to allow loading from game entry

   //--- Methods ---

   //public: void build_list ( int base, int range, unsigned short mask = 0);
   //public: void clear_list ( void );
   public: void build_table ( int floorID, int areaID );
   public: void clear_table ( void );
   public: bool check_encounter ( s_mazetile tile, bool door );
   public: int start ( void );
   //public: void clear_selection ( void );
   //public: void clear_objects ( void ); //Delete from db generated ennemies & party
   //public: int get_ennemy_party ( void ); // run in combat after start

   //--- Private Methods ---

   //private: void select_encounter ( void );


};

/*-------------------------------------------------------------------------*/
/*-                        Global Variables                               -*/
/*-------------------------------------------------------------------------*/

extern Encounter encount;



