/***************************************************************************/
/*                                                                         */
/*                              C I T Y . H                                */
/*                                                                         */
/*     Content : Class City                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : March 27th, 2002                                    */
/*                                                                         */
/*          This file contains the definition of the class city which is   */
/*     a used as a data object and as an engine.                           */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                     Constant Class Parameter                          -*/
/*-------------------------------------------------------------------------*/

#define City_FONT_NAME           FNT_helena24
#define City_FONT_INSTRUCTION    FNT_small
#define City_NB_MAX_LOCATION     8
#define City_NB_MAX_SERVICE      8
#define City_NB_SERVICE          32
#define City_NB_ADVSERVICE       16
#define City_NB_MAX_WARP         8
#define City_NB_MAX_ITEM         256

/*-------------------------------------------------------------------------*/
/*-                            Constant                                   -*/
/*-------------------------------------------------------------------------*/

// alternate loading

//#define City_DATATYPE_ADVENTURE  1

// city location types

/*#define City_PLACE_GAME_MENU           0
#define City_PLACE_INN                 1
#define City_PLACE_ADVENTURER_GUILD    2
#define City_PLACE_TAVERN              3
#define City_PLACE_MARKETPLACE         4
#define City_PLACE_BLACKSMITH          5
#define City_PLACE_ARMORER             6
#define City_PLACE_WIZARD_GUILD        7
#define City_PLACE_TEMPLE              8
#define City_PLACE_LIBRARY             9
#define City_PLACE_PORT                10
#define City_PLACE_MAZE                11
#define City_PLACE_EDGE_OF_TOWN        12
#define City_PLACE_THIEVES_GUILD       13
*/

// City services

#define City_MENU_CAMP           98
#define City_MENU_MAZE           99
#define City_MENU_QUIT           -1

#define City_SERVICE_REST        0
#define City_SERVICE_LEVELUP     1
#define City_SERVICE_HOME        2
#define City_SERVICE_HIRE        3
#define City_SERVICE_QUIT        4
#define City_SERVICE_BUY         5
#define City_SERVICE_SELL        6
#define City_SERVICE_FORGE       7
#define City_SERVICE_ENCHANT     8
#define City_SERVICE_IDENTIFY    9
#define City_SERVICE_HEAL        10
#define City_SERVICE_REVIVE      11
#define City_SERVICE_KNOWLEDGE   12
#define City_SERVICE_KEY         13
#define City_SERVICE_UNCURSE     14
#define City_SERVICE_CHARACTERS  15
#define City_SERVICE_PARTY       16

// city generic menu
#define City_CREATE     0
#define City_INSPECT    1
#define City_DELETE     2


// other menu

#define City_RENAME_CHARACTER 3

// party Menu
#define City_PARTYMENU_ADD       0
#define City_PARTYMENU_REMOVE    1
#define City_PARTYMENU_REMOVEALL 2


//#define City_SERVICE_RETIRE      15

//?? implement by default
//#define City_SERVICE_RETURN      4

//#define City_SERVICE_LANDWARP    13
//#define City_SERVICE_SEAWARP     14
//#define City_SERVICE_AIRWARP     15
//#define City_SERVICE_MAGIKWARP   16
//#define City_SERVICE_EXPLORE     17


// exit city engine

//#define City_CONTINUE         0
//#define City_END_GAME         1
//#define City_ENTER_MAZE       2
//#define City_ENTER_CITY     3

// rest room_type constant ( mirror constant Character_ROOMTYPE )

//#define City_ROOMTYPE_COMMON      0
//#define City_ROOMTYPE_FINE        1

// City warping travel type

/*#define City_WARPTYPE_LAND    0
#define City_WARPTYPE_SEA     1
#define City_WARPTYPE_AIR     2
#define City_WARPTYPE_MAGIC   3
#define City_WARPTYPE_EXPLORE 4*/

// City Types

/*#define City_TYPE_VILLAGE     0
#define City_TYPE_CITY        1
#define City_TYPE_CASTLE      2*/

/*-------------------------------------------------------------------------*/
/*-                     Global variable Definition                        -*/
/*-------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------*/
/*-                       Type definition                                 -*/
/*-------------------------------------------------------------------------*/

typedef struct s_City_place
{
   char name [21]; // name drawned in the menu to identify the place
   int service [ City_NB_MAX_SERVICE ]; // Services available in the building
   //int music_track; // Number ID of the music track to play
   int music_table; // table group of music to use (random or user defined)
}s_City_place;

/*typedef struct s_City_warp
{
   int type; // travel type
//   short distance; // determine travel price according to type
   int target; // City tag of the target city
   int tag  ;
}s_City_warp;*/

typedef struct s_City_item
{
   int itemtag; // tag on the item for sale
   int type; // Item type ID
   int quantity; // number of items of this type in stock
//   int modifier; // price modifier -25% to +25%, ( either random or according to sales/rarety )
}s_City_item;

//typedef struct s_City_data
//{
             /* nb char */
//   s_City_item item [ City_NB_MAX_ITEM ]; // item list of the city
//}s_City_data;

// database structure

/*typedef struct dbs_City_place
{
   char name [ 21 ];//  ;
   int service [ City_NB_MAX_SERVICE ];//  ;
   int music_track;//  ;
}  dbs_City_place;

typedef struct dbs_City_warp
{
   int type;//  ;
//   short distance  ;
   int target;//  ;
   int tag;//  ;
}  dbs_City_warp;

typedef struct dbs_City
{
   char name [ 31 ];//  ;
   char shortname [ 11 ];//  ;
   int nb_place;//  ;
   short xpos;//  ;
   short ypos;//  ;
   int type;//  ;
   dbs_City_place place [ City_NB_MAX_LOCATION ];//  ;
   dbs_City_warp warp [ City_NB_MAX_WARP ];//  ;
}  dbs_City;

// adventure entry structure

typedef struct dbs_ADV_City_place
{
   char name [ 21 ];//  ; // name of the place
   int music_track;//  ; // music track to play
//   int service [ City_NB_MAX_SERVICE ]  ;
}  dbs_ADV_City_place;

typedef struct dbs_ADV_City_warp
{
   int type;//  ; // travel type
//   short distance  ; // determine travel price according to type
   int target;//  ; // 0 - 15 city, 16 - 31 maze
}  dbs_ADV_City_warp;
*/
/*typedef struct dbs_ADV_City_service
{
   int type  ;
   int placeID  ;
}dbs_ADV_City_service;*/

/*typedef struct dbs_ADV_City_data
{
   char name [ 31 ];//  ;
   char shortname [ 11 ];//  ;
   int type;//  ;
   short xpos;//  ;
   short ypos;//  ;
   int nb_place;//  ;
   dbs_ADV_City_place place [ City_NB_MAX_LOCATION ];//  ;
   int service_location [ City_NB_ADVSERVICE ];//  ;
   dbs_ADV_City_warp warp [ City_NB_MAX_WARP ];//  ;
}  dbs_ADV_City_data;
*/



/*-------------------------------------------------------------------------*/
/*-                        Class Definition                               -*/
/*-------------------------------------------------------------------------*/

class City
{

   // Properties

   private: char p_name [ 31 ]; // title of the city
  // private: char p_shortname [ 11 ];
   private: int p_nb_place; // number of places in the city
   //private: short p_xpos; // xposition on the map
   //private: short p_ypos; // yposition on the map
   //private: int p_type; // city type
   private: s_City_place p_place [ City_NB_MAX_LOCATION ]; // list of places which can be visited
   //private: s_City_warp p_warp [ City_NB_MAX_WARP ]; // city warping information

   //private: Party p_party;
   private: s_City_item p_item [ City_NB_MAX_ITEM ];
//   private: Party p_party;

   // Constructor and destructor

   public: City ( void );
   public: ~City ( void );

   // Property Methods

//   public: void name ( string &str );
//   public: string& name ( void );
   public: void name ( const char* str );
   public: const char* name ( void );
   public: int nb_place ( void );
   //'public: short xpos ( void );
   //public: short ypos ( void );
   //public: int type ( void );
//   public: void party ( Party &party );

   // Methods

   public: int start ( void );
   public: void generate_item ( void );
   public: void generate_newgame_item ( void );
//   public: void generate ( dbs_ADV_City_data dat );

   //public: void reference_target ( int citytaglist [ 16 ], int mazetaglist [ 16 ] );

   // Private Methods



   private: void show_service_rest ( void );
   private: void show_service_levelup ( void );
   private: void show_service_buy ( void );
   private: void show_service_sell ( void );
   private: void show_service_identify ( void );
   private: void show_service_heal ( void );
   private: void show_service_revive ( void );
   //private: int show_edge_of_town ( void );
   private: void show_service_characters ( void );
   private: void show_service_party ( void );

   private: void show_subservice_create_character ( void );

   // virtual Methods

   /*public: virtual void objdat_to_strdat ( void *dataptr );
   public: virtual void strdat_to_objdat ( void *dataptr );
   public: virtual void child_DBremove ( void );

   public: virtual void alternate_strdat_to_objdat ( int datatypeID, void *dataptr );
*/
};

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

extern City city;
extern const char STR_CTY_BUYTYPE [] [ 81 ];
//extern const char STR_CTY_WARPACTION [] [ 11 ];
extern const char SERVICE_NAME [ City_NB_SERVICE ] [ 26 ];
