/*************************************************************************/
/*                                                                       */
/*                            g r p d b o b j . h                        */
/*                                                                       */
/*    Inclusion group for all data objects                               */
/*                                                                       */
/*************************************************************************/

#ifndef GRPDBOBJ_H_INCLUDED
#define GRPDBOBJ_H_INCLUDED

#include <gameclock.h>
#include <game.h>
#include <party.h>
#include <item.h>
#include <opponent.h>
#include <race.h>
#include <cclass.h>


//#include <ennemy.h>

#include <charactr.h>
#include <monster.h>
#include <action.h>
#include <cmdproc.h>
//#include <texture.h>
#include <texpalette.h>
#include <event.h>
#include <entrance.h>
#include <enemygroup.h>
#include <map.h>
#include <monstercategory.h>
#include <text.h>
#include <aeffect.h>
#include <spell.h>



#endif // GRPDBOBJ_H_INCLUDED
