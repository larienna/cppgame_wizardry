/***************************************************************************/
/*                                                                         */
/*                             E D I T O R . H                             */
/*                            Class Definition                             */
/*                                                                         */
/*     Content : Module Editor                                             */
/*     Starting Date :  February 21st, 2004                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     License : GNU General Public License                                */
/*                                                                         */
/*          This class is the editor main programm. It allows to edit maze */
/*     maps that will be loaded later in the adventure.                    */
/*                                                                         */
/***************************************************************************/


/*-------------------------------------------------------------------------*/
/*-                              Constants                                -*/
/*-------------------------------------------------------------------------*/


#define Editor_TOOL_TILE      0
#define Editor_TOOL_CORRIDOR  1  // corridor maker
//#define Editor_TOOL_RECTANGLE 2
#define Editor_TOOL_RECTANGLE      2 //  rectangular selection
#define Editor_TOOL_BRUSH      3  // Odd shape room selection


// --- Icons ID ---
// maybe add _EVENT_ for event icons

#define EDICON_CURSOR_ARROW   0
#define EDICON_CURSOR_SQUARE  1
#define EDICON_CURSOR_ANGLE   11
#define EDICON_MARKER_FLAG    12
#define EDICON_MARKER_PAINT   13
#define EDICON_SPECIAL_START  14
#define EDICON_TILE_MBOUNCE   15
#define EDICON_TILE_EMPTY     16
#define EDICON_TILE_SOLID     17
#define EDICON_WALL_DOOR      18
#define EDICON_WALL_GRID      19
#define EDICON_WALL_WALL      20
#define EDICON_TILE_LIGHT     21
#define EDICON_CURSOR_BRUSH   22

#define EDICON_MINI_WALL      23
#define EDICON_MINI_EMPTY     24
#define EDICON_MINI_SOLID     25
#define EDICON_MINI_DOOR      26
#define EDICON_MINI_GRID      27

#define EDITOR_POLYGON_WALL      1
#define EDITOR_POLYGON_FLOOR     2
#define EDITOR_POLYGON_CEILING   3
#define EDITOR_POLYGON_TRANSWALL 4

#define EDITOR_TRANSWALL_BLEND   80

// --- menu return value ---

#define Editor_MENU_NONE      0
#define Editor_MENU_EXIT      1

/*-------------------------------------------------------------------------*/
/*-                              Type Definition                          -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Editor_maze_header
{
   int version; // maze version. changes how the tiles are read
   int width; // Width and height of the maze (square). Placed there in case of variable size.
   int depth; // Number of levels in the maze. Placed there in case of variable size.
   int reserved; // standby for future use.
   //int f_sky;
   //int f_rclevel;
   //int f_startx;
   //int f_starty;
   //int f_startz;
   //int f_startface;

}s_Editor_maze_header;

/** Structure that contains information for drawing the preview of the maze tile*/
typedef struct s_Editor_polygon_info
{
   int type; // Indicates if it's a wall, ceiling or floor
   int shift; // amounte of shifting required for bitfield operation
   unsigned int mask; // mask value for desired wall
   unsigned int wall; // wall value
}s_Editor_polygon_info;

typedef struct s_Editor_texpalette_color
{
   int color; // color to display.
   int texture; // Texture ID of the editor icon datafile
}s_Editor_texpalette_color;

/*typedef struct s_Editor_maze_tile
{
   unsigned char solid [4]; // 1 value for each direction
   unsigned char special_tech; // 5 bit technical info ;
   unsigned char special_fill; // 1 value
   unsigned short masktex; // 2 bit mask floor/ceiling :  4 bit mask wall,
   unsigned short object_position; //value
   unsigned short tilesetid; // value
   unsigned char walltex; // masked wall texture ID
   unsigned char floortex; // masked floor texture ID
   unsigned char objectimg; // room object image ID
   unsigned char event; // event ID for the maze ( or the floor ) ( 0 = no event )
}s_Editor_maze_tile;*/


/*-------------------------------------------------------------------------*/
/*-                         Class Declaration                             -*/
/*-------------------------------------------------------------------------*/

class Editor
{
   // Properties

   //private: int p_width; // now fixed
   //private: int p_depth;

   // private: int p_sky; // now in palette
   // most of this information will be in the database, not the map.
   private: int p_rclevel; // Identifies the RC level, above is floor, below is basement
   private: s_Party_position p_startpos;
   //private: s_Maze_tile p_curtile;

   // internal global varaibles ( no need to save

//   private: BITMAP *p_map;
   private: int p_xcur;
   private: int p_ycur;
   private: int p_zcur;
   private: int p_xscroll;
   private: int p_yscroll;
   private: int p_tool; // identify which tool is currently active
   private: unsigned int p_layer; // list of flags with layers of information to be displayed.
   private: int p_xmark; // marks used for the rectangle tool
   private: int p_ymark;
   private: bool p_selection_active; // indicates currently selecting something
   private: bool p_selection [Maze_MAXWIDTH] [Maze_MAXWIDTH];
   private: int p_active_palette;
   private: bool p_adventure_selected;
   private: char p_adventure_mazefile [ Manager_TMP_STR_LEN + 10];
   private: char p_adventure_databasefile [ Manager_TMP_STR_LEN + 10];

   // Constructor and Destructor

   public: Editor ( void );
   public: ~Editor ( void );

   // Property Methods

   // Methods

   public: void start ( void );
   //public: void load_maz_file ( char *filename );
   //public: void save_maz_file ( char *filename );

   public: void start_test_maze ( void );

   public: int menu_file ( void );
   public: int menu_tool ( void );
   public: int menu_texture ( void );
   public: int menu_view ( void );
   public: int menu_layer ( void );
   public: int menu_error ( void );
   public: int menu_data ( void );
   public: int menu_event ( void );

   public: int menu_selection ( void ); // when push R on a selection

   public: void cmd_wall ( int side ); // use maze 2 bit
   public: void cmd_delete ( void );
   public: void cmd_corridor ( int direction ); // use maze 2bit constants

   public: void patch_surrounding_walls ( int z, int y, int x );

   public: void rect_to_sel ( void );
   public: void clear_selection ( void );
   public: void sel_build_room ( void );
   public: void sel_filling ( int fill );
   public: void sel_special ( unsigned int special );
   public: void sel_remove_special ( void ); // remove special tech and filling
   public: void sel_palette ( int palette );
   public: void sel_masked ( int texid, unsigned int location  );
   public: void sel_delete ( void );

   public: void delete_tile ( int z, int y, int x );
   //public: void remove_wall ( int z, int y, int x, int direction);
   //public: void set_wall ( int z, int y, int x, int direction);
   //public: void set_wall_type ( int z, int y, int x, int side, int type );
   //public: bool is_tile_special ( int z, int y, int x, unsigned int special );
   //public: bool is_tile_selected ( int z, int y, int x );
   //public: void remove_tile_special ( int z, int y, int x, unsigned int special );
   //public: void set_tile_special ( int z, int y, int x, unsigned int special );
   public: void clear_maze ( void );
   public: int get_palette ( int z, int y, int x );
   public: void set_palette ( int z, int y, int x, int palette);

   //semi-private methods that are also used by spells to draw the map
   //Else would need to set map drawing into a class of it own.

   //public: void get_wall_type ( void ); // add copy of wall in from non-solid areas


   // Private Methods

   //private: void copy_background ( void );
   /** Draw the maze map for editing on the main screen*/
   private: void draw_map ( void );
   /** Draw the detailed information on the current tile to the side of the main screen*/
   private: void draw_detailed_tile ( void );
   /** Draw the whole map in tiny mode for a more global view with some options*/
   private: void show_full_map ( void );
   private: void select_adventure ( void );
   private: void select_event ( void );
   private: int select_palette ( void ); // return selected palette
   private: int select_active_palette_texture ( void ); // return selected palette index


   //private: void read_curtile ( void ); //s_Maze_tile mtile
   //private: void write_curtile ( void ); //s_Maze_tile &mtile


};

/*-----------------------------------------------------------------------*/
/*-                           Procedures                                -*/
/*-----------------------------------------------------------------------*/

void Editor_draw_space_wall ( s_mazetile tile, int y, int x );
//void Editor_draw_space_special ( s_mazetile tile, int y, int x );
void Editor_draw_space_filling ( s_mazetile tile, int y, int x );


/*void Editor_start ( void );
void Editor_load_maz_file ( char *filename );
void Editor_save_maz_file ( char *filename );

   // Private Methods

void Editor_copy_background ( void );
void Editor_draw_map ( void );
void Editor_draw_detailed_tile ( void );
void Editor_read_curtile ( void ); //s_Maze_tile mtile
void Editor_write_curtile ( void ); //s_Maze_tile &mtile*/

/*-----------------------------------------------------------------------*/
/*-                        Global variables                             -*/
/*-----------------------------------------------------------------------*/

extern const char STR_EDT_SPECIAL_TECH [][16];
extern const char STR_EDT_SPECIAL_FILL [][16];

//extern const char STR_EDT_TOOL [][10];

extern const s_Editor_polygon_info Editor_POLYGON_INFO [6];

extern const s_Editor_texpalette_color Editor_texpalette_color [32];

extern V3D Editor_room [ 6 ][ 4 ];


//extern fs_maz_header Editor_header;
//extern s_Editor_maze_tile Editor_curtile;


// ------------------ Documentation -----------------------------
/*KEY_A ... KEY_Z,
      KEY_0 ... KEY_9,
      KEY_0_PAD ... KEY_9_PAD,
      KEY_F1 ... KEY_F12,

      KEY_ESC, KEY_TILDE, KEY_MINUS, KEY_EQUALS,
      KEY_BACKSPACE, KEY_TAB, KEY_OPENBRACE, KEY_CLOSEBRACE,
      KEY_ENTER, KEY_COLON, KEY_QUOTE, KEY_BACKSLASH,
      KEY_BACKSLASH2, KEY_COMMA, KEY_STOP, KEY_SLASH,
      KEY_SPACE,

      KEY_INSERT, KEY_DEL, KEY_HOME, KEY_END, KEY_PGUP,
      KEY_PGDN, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN,

      KEY_SLASH_PAD, KEY_ASTERISK, KEY_MINUS_PAD,
      KEY_PLUS_PAD, KEY_DEL_PAD, KEY_ENTER_PAD,

      KEY_PRTSCR, KEY_PAUSE,

      KEY_ABNT_C1, KEY_YEN, KEY_KANA, KEY_CONVERT, KEY_NOCONVERT,
      KEY_AT, KEY_CIRCUMFLEX, KEY_COLON2, KEY_KANJI,

      KEY_LSHIFT, KEY_RSHIFT,
      KEY_LCONTROL, KEY_RCONTROL,
      KEY_ALT, KEY_ALTGR,
      KEY_LWIN, KEY_RWIN, KEY_MENU,
      KEY_SCRLOCK, KEY_NUMLOCK, KEY_CAPSLOCK

      KEY_EQUALS_PAD, KEY_BACKQUOTE, KEY_SEMICOLON, KEY_COMMAND

extern volatile int key_shifts;
   Bitmask containing the current state of shift/ctrl/alt, the special
   Windows keys, and the accent escape characters. Wherever possible this
   value will be updated asynchronously, but if keyboard_needs_poll()
   returns TRUE, you must manually call poll_keyboard() to update it with
   the current input state. This can contain any of the flags:

      KB_SHIFT_FLAG
      KB_CTRL_FLAG
      KB_ALT_FLAG
      KB_LWIN_FLAG
      KB_RWIN_FLAG
      KB_MENU_FLAG
      KB_COMMAND_FLAG
      KB_SCROLOCK_FLAG
      KB_NUMLOCK_FLAG
      KB_CAPSLOCK_FLAG
      KB_INALTSEQ_FLAG
      KB_ACCENT1_FLAG
      KB_ACCENT2_FLAG
      KB_ACCENT3_FLAG
      KB_ACCENT4_FLAG

   Example:

      if (key[KEY_W]) {
         if (key_shifts & KB_SHIFT_FLAG) {
            // User is pressing shift + W.
         } else {
            // Hmmm... lower case W then.
         }
      }

*/


