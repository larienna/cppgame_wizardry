/***************************************************************************/
/*                                                                         */
/*              D A T A F I L E R E F E R E N C E . H                      */
/*                                                                         */
/*     Content : DatafileReference Class                                   */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : September 17th, 2012                                */
/*                                                                         */
/*          This is a template class designed to wrap around data files    */
/*     in order to reference many datafiles into 1 table. The class is a   */
/*     template class that autocast the pointers.                          */
/*                                                                         */
/***************************************************************************/

#ifndef DATAFILEREFERENCE_H_INCLUDED
#define DATAFILEREFERENCE_H_INCLUDED

/*-------------------------------------------------------------------------*/
/*-                               Includes                                -*/
/*-------------------------------------------------------------------------*/

//include groups

#include <grpsys.h>

/*-------------------------------------------------------------------------*/
/*-                               Constants                               -*/
/*-------------------------------------------------------------------------*/

#define DatafileReference_NB_MAX_OBJECT   1024
#define DatafileReference_NB_DATAFILE     16


#define DatafileReference_OBJECTID_MASK   1023  // 11 1111 1111
/*-------------------------------------------------------------------------*/
/*-                               Structures                              -*/
/*-------------------------------------------------------------------------*/

/*s_DataArchive_datafile
{
    DATAFILE *datf; // pointer on the datafile.
    int nb_object; // number of object in the datafile.
    bool loaded; // indicate if the datafile is loaded (else could use a null pointer)
};*/

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

template<class t_datatype>
class DatafileReference
{

    /*private: typedef s_DataArchive_object
    {
       t_datatype *data;
       int sourceID;
    };
*/
   //-----  properties -----

   private: DATAFILE* p_datf [ DatafileReference_NB_DATAFILE ];

   //private: t_datatype p_reference [ DatafileReference_NB_MAX_OBJECT ];

   //private: t_datatype *p_referencetest[256]; // pointer on a array of reference
   //private: int p_size;

    //private: int p_current; // ID of the current datafile
  //  private: s_DataArchive_datafile p_datflist [ DataArchive_NB_MAX_DATAFILE ];

/*    private: DATAFILE *p_datafile; // pointer on the datafile loaded
    private: s_DataArchive *p_datatable // dynamic table of data pointer.
    private: int p_size; // size of the archive defined at creation like a table creation
    private: int p_current; // id of the current item to reference
    private: bool p_loaded; // keep track if the datafile is loaded
*/
    // Constructor & Destructor

    public: DatafileReference ( void );
    public: ~DatafileReference ( void );

    // property methods

    //public: int size ( void ); // return the size of the array

    // methods

  //  public: load ( const char filename );
    public: void add ( DATAFILE *datf, int index );
    public: void remove ( int index );
    public: bool is_loaded ( int index ); // check to see if reference loaded
    public: bool eof ( int datfid, int objectid );

    public: t_datatype get ( int datfid, int objectid );
    public: int getid ( int datfid, int objectid );

    // operators

    public: t_datatype operator[](int index); // read data from the datafile

};

#endif // DATAARCHIVE_H_INCLUDED

/* documentation

typedef struct DATAFILE
void *dat; - pointer to the actual data
int type; - type of the data
long size; - size of the data in bytes
void *prop; - list of object properties

note: not sure if loading managed by class since advanture is unloaded all at once,
not datafile per datafile

*/
