/***************************************************************************/
/*                                                                         */
/*                            G E N E R A L . H                            */
/*                                                                         */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : March 14th, 2002                                    */
/*                                                                         */
/*          This file contains general definitions and funtions for the    */
/*     whole wizardry project.                                             */
/*                                                                         */
/***************************************************************************/

#ifndef GENERAL_H
#define GENERAL_H 1

#define Allegro_NB_OPERATING_SYSTEM 18
#define Allegro_NB_CPU_FAMILY       5

/*------------------------------------------------------------------------*/
/*-                          Exclusions                                  -*/
/*------------------------------------------------------------------------*/

#define  alleg_mouse_unused
#define  alleg_joystick_unused
#define  alleg_vidmem_unused
#define  alleg_flic_unused
#define  alleg_gui_unused

/*-------------------------------------------------------------------------*/
/*-                          Type definition                              -*/
/*-------------------------------------------------------------------------*/

//typedef signed char int; // 8 bit signed
//typedef unsigned char unsigned char; // 8 bit : used for bitwise operation
//typedef unsigned short unsigned short; // 16 bit : used for bitwise operation
//typedef unsigned int unsigned int; // 32 bit : used for bitwise operation

//#define string String  // alias
//#define unsigned char unsigned char      // alias
//#define unsigned short unsigned short      // alias
//#define unsigned int unsigned int    // alias

// constant_color_definition
#define General_COLOR_TEXT          makecol ( 225, 225, 225 )
#define General_COLOR_INSTRUCTION   makecol ( 250, 250, 250 )
#define General_COLOR_DISABLE       makecol ( 150, 150, 150 )
#define General_COLOR_BORDER        makecol ( 250, 250, 250 )
#define General_COLOR_FILL          makecol ( 0, 0, 0 )

// Constants for general.h functions

#define General_INPUTSTR_SIZE    121



typedef struct s_OS_Identification
{
   int ID;
   char name [ 16 ];
}s_OS_Identification;

typedef struct s_CPU_family
{
   short ID;
   char name [ 13 ];
}s_CPU_family;


//Placed here instead of opponent for compiling include purpose
/*typedef struct s_Opponent_rollstat
{
   unsigned char min;
   unsigned char max;
   unsigned char modifier; // modify max but this stat can change compared to others
   unsigned char bonus;
//   short other;
}s_Opponent_rollstat;*/


/*-------------------------------------------------------------------------*/
/*-                            Includes                                   -*/
/*-------------------------------------------------------------------------*/

//#include <strclass.h>
  /*
#include <stdio.h>
//#include <stdlib.h>
//#include <typeinfo>
//#include <time.h>
#include <allegro.h>
#include <random.h>
//#include <string.h>
#include <datafile.h>
#include <datmacro.h>
#include <strclass.h>
#include <init.h>
#include <menu.h>
#include <option.h>
#include <screen.h>






#include <listwiz.h>
#include <opponent.h>
#include <charactr.h>
#include <monster.h>
#include <party.h>

#include <game.h>
#include <city.h>
#include <maze.h>

#include <camp.h>
#include <config.h>
#include <draw.h>
#include <global.h>
#include <dialog.h>
#include <combat.h>
*/


/*-------------------------------------------------------------------------*/
/*-                         Constants                                     -*/
/*-------------------------------------------------------------------------*/



//#if ALLEGRO_PLATFORM_STR == ALLEGRO_WINDOWS


//#define    __attribute__ ((packed))

/*------------------------------------------------------------------------*/
/*-                     Global Variables                                 -*/
/*------------------------------------------------------------------------*/

//extern char filelist [ General_FILELIST_SIZE ] [ 13 ];
//extern short flistindex;



/*-------------------------------------------------------------------------*/
/*-                        Prototype                                      -*/
/*-------------------------------------------------------------------------*/


void textinput ( short x, short y, char *str, int nb_char );

//void save_backup_screen ( void ); // save screen to backup
//void load_backup_screen ( void ); // load screen from backup
//void save_backup_buffer ( void ); // save buffer to backup
//void load_backup_buffer ( void ); // load buffer from backup
void copy_buffer ( void );
void copy_backup_buffer ( void );
void copy_buffer_keep ( void );
void copy_backup_buffer_keep ( void );
void blit_mazebuffer ( void );
void blit_editorbuffer ( void );

void switch_in_callback ( void );
void switch_out_callback ( void );

void make_screen_shot ( char *filename );
void play_music_track ( int track, bool loop = true );
void play_music_table ( int table );
void play_sound ( int soundid );

int mainloop_readkeyboard ( void );

//unsigned short statdice ( s_Opponent_rollstat rollstat );
//double exponent ( float x, float y );

void flist_proc ( const char *name, int attribute, int param );

void debug ( const char *str, int var1 = 0, int var2 = 0);

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

// System identification variables

extern s_OS_Identification OS_NAME [ Allegro_NB_OPERATING_SYSTEM ];
extern s_CPU_family CPU_FAMILY_NAME [ Allegro_NB_CPU_FAMILY ];

extern bool switch_redraw;


#endif
