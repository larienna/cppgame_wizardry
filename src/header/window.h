/***************************************************************************/
/*                                                                         */
/*                             W I N D O W . H                             */
/*                        Abstract Class Definition                        */
/*                                                                         */
/*     Engine : Window engine                                              */
/*     Content : Class Window                                              */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 3rd, 2002                                  */
/*                                                                         */
/*          This class is part of a set of classes that will manage the    */
/*     drawing and memorisation of windows with it's content on the screen */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                       Constant Definition                             -*/
/*-------------------------------------------------------------------------*/

// colors

#define Window_COLOR_BORDER        makecol ( 250, 250, 250 )
//#define Window_BASECOLOR_BORDER    250
#define Window_COLOR_FILL          makecol ( 0, 0, 0 )
// ?? offer parametrable colors


//--- Windows type ---

#define Window_TYPE_EMPTY        0
#define Window_TYPE_TITLE        1
#define Window_TYPE_MENU         2
#define Window_TYPE_LIST         3
#define Window_TYPE_DATA         4
#define Window_TYPE_MESSAGE      5
#define Window_TYPE_QUESTION     6
#define Window_TYPE_INPUT        7
#define Window_TYPE_DISTRIBUTOR  8
#define Window_TYPE_IMGLIST      9

// --- Window border type ---

#define Window_BACKGROUND_COLOR     0
#define Window_BACKGROUND_TRANSLUCENT 1
#define Window_BACKGROUND_TEXTURE   2

#define Window_BORDER_THICK  0
#define Window_BORDER_THIN   1
#define Window_BORDER_DOUBLE 2
#define Window_BORDER_FLAT   3
#define Window_BORDER_NONE   4

#define Window_TEXTURE_MARBLE    0
#define Window_TEXTURE_EARTH     1
#define Window_TEXTURE_FOREST    2
#define Window_TEXTURE_SLATE     3
#define Window_TEXTURE_ROCK      4

#define Window_COLOR_BLACK    0
#define Window_COLOR_GREEN    1
#define Window_COLOR_BLUE     2
#define Window_COLOR_PURPLE   3
#define Window_COLOR_BROWN    4

// --- WIndow Instruction ---

#define Window_INSTRUCTION_SELECT  1
#define Window_INSTRUCTION_CANCEL  2
#define Window_INSTRUCTION_DISPLAY 4
#define Window_INSTRUCTION_MENU    8
#define Window_INSTRUCTION_SWITCH  16
#define Window_INSTRUCTION_MOVE    32
#define Window_INSTRUCTION_INPUT   64
#define Window_INSTRUCTION_CAMP    128
#define Window_INSTRUCTION_SCROLL  256
#define Window_INSTRUCTION_VALUE   512 // left right to raise or drop value
#define Window_INSTRUCTION_ACCEPT  1024
#define Window_INSTRUCTION_EXIT    2048 // for exiting maze in demo mode
#define Window_INSTRUCTION_EDITOR  4096 // Common Input for the Editor

/*-------------------------------------------------------------------------*/
/*-                         Type Definition                               -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Window_background
{
   int texturedatID; // texture ID number form the datafile
   unsigned char red;
   unsigned char green;
   unsigned char blue;
}s_Window_background;

/*-------------------------------------------------------------------------*/
/*-                          Global Variables                             -*/
/*-------------------------------------------------------------------------*/

extern int BORDER_CORNER [ 4 ] [ 5 ] [ 5 ];
extern int BORDER_COLOR [ 5 ] [ 5 ];
extern s_Window_background Window_BACKGROUND [ 5 ];


/*-------------------------------------------------------------------------*/
/*-                       Class Definition                                -*/
/*-------------------------------------------------------------------------*/

class Window
{
   // --- Properties ---

   protected: short p_width; // width of the window with the border
   protected: short p_height; // height of the windows with the border
   protected: short p_x_pos; // X relative position of the window
   protected: short p_y_pos; // Y relative position of the window
   private: bool p_hidden; // indicates if the window should be hidden
   private: Window *p_ptr_next; // pointer of the next element
   private: Window *p_ptr_previous; // pointer on the previous object.
   protected: BITMAP* p_backup; // bitmap backuping the display of the window
   protected: bool p_translucent; // Window is allows to be translucent if requested.

   private: static Window *p_ptr_last; // pointer on the last element
   private: static Window *p_ptr_first; // pointer of the first element

   private: static int p_nb_open; // contains the number of windows currently opened
                            // should be initialised to 0
   private: static int p_border_type; // type of border to draw
   private: static int p_bg_mode; // draw color or textured background
   private: static int p_bg_color;
   private: static int p_bg_texture;
   private: static int p_bg_trans_level; // Translucency Level.

   private: static bool p_show_party_frame; // if true, display party frame (match

   // instruction variables
   protected: static int p_inst_order; // instructions to draw on the screen
   protected: static short p_inst_center_x; // center x position of the instruction
   protected: static short p_inst_y; // y position of the instruction
   protected: static const char *p_inst_string; // use string if order = -1

   // cascading menu variables
   protected: static int p_casc_next_level; // contains the level of the next cascading level.
   protected: static bool p_casc_display; // status to activate or deactivate cascade view only
   protected: static bool p_casc_active;// status to add levels to windows or not
   protected: static int p_casc_width; // size in pixel of the cascade
   protected: int p_casc_level; // level of the current window.

   // --- Constructor & Destructors

   public: Window ( void );
   public: virtual ~Window ( void );

   // --- Property Method ---

   public: short width ( void );
   public: short height ( void );
   public: short x_pos ( void );
   public: short y_pos ( void );
   public: Window* next ( void );
   public: Window* previous ( void );

   public: static int nb_open ( void );
   public: static int border_type ( void );
   public: static void border_type ( int type );
   public: static int background_color ( void );
   public: static void background_color ( int color );
   public: static int background_texture ( void );
   public: static void background_texture ( int texture );
   public: static int background_mode ( void );
   public: static void background_mode ( int mode );
   public: static void instruction ( short center_x, short y );
   public: static void instruction ( short center_x, short y, int order );
   public: static void instruction ( short center_x, short y, const char *str );

   public: static void show_party_frame ( void );
   public: static void hide_party_frame ( void );

   public: static int cascade_next_level ( void );
   public: static void enable_cascade ( void );
   public: static void disable_cascade ( void );
   public: static void cascade_width ( int width );
   public: static void cascade_start ( void ); // used to initiate a new cascade
   public: static void cascade_pause ( void );
   public: static void cascade_continue ( void );
   public: static void cascade_stop ( void );
   public: int cascade_level ( void ); //return cascade level of the window
   public: static void translucency_level ( int level );


   // --- Mehtods ---

   public: void hide ( void );  // last window is alsways shown
   public: void unhide ( void );
   public: static short show_all ( void );
   public: static void draw_all ( void ); // the last window is drawn instead of shown
   public: static void refresh_all ( void );
   public: void draw ( void ); // used internaly -> try show_all or draw_all

   public: static void draw_party_frame ( void );

   // --- Private Methods ---

//   private: void draw_border ( void );
   protected: void border_fill ( void );
   protected: void draw_background ( void );
   protected: static void draw_instruction ( void );



   // --- Virtual Methods ---

   public: virtual void preshow ( void ) = 0;
   public: virtual void refresh ( void ) = 0;
   public: virtual short show ( void ) = 0; // used internaly -> try show_all
   public: virtual int type ( void ) = 0;

   // --- Operators ---
};

/*-------------------------------------------------------------------------*/
/*-                     Global Variable Definition                        -*/
/*-------------------------------------------------------------------------*/


