/******************************************************************************/
/*                                                                            */
/*                              S Y S L O G . H                               */
/*                              class Definition                              */
/*                                                                            */
/*    class: Syslog                                                           */
/*    Programmer: Eric Pietrocupo                                             */
/*    Starting Date: October 14th, 2012                                       */
/*                                                                            */
/*         This class is used to manage a system log into memory. The log is  */
/*    is not saved to a file and only the 1024 most recent entries are kept   */
/*                                                                            */
/******************************************************************************/


#ifndef SYSLOG_H_INCLUDED
#define SYSLOG_H_INCLUDED

/******************************************************************************/
/*                          Constant Parameter                                */
/******************************************************************************/

#define Syslog_STR_LEN     121
#define Syslog_NB_ENTRY    1024

/******************************************************************************/
/*                        Class Definition                                    */
/******************************************************************************/

class Syslog
{
   // properties

   private: char p_log [Syslog_NB_ENTRY] [Syslog_STR_LEN]; // content of the log
   private: int p_index; // index of the current entry
   private: int p_nb_entry; // count the number of entries
   private: int p_marker; // set a mark to start with in the log. (counting offset)

   // constructors and destructors

   public: Syslog ( void );
   public: ~Syslog ( void );

   // property method

   public: int nb_entry ( void ) { return ( p_nb_entry ); }
   public: int marker ( void ) { return ( p_marker); }

   // Method

   public: void write ( const char *str );
   public: void writef ( const char *format, ... );
   public: char *read ( int offset = 0 ); // the offset is substracted from index to
                                    // show previous entries.
   public: void clear ( void );
   public: void mark ( void );


};

/******************************************************************************/
/*                        Global Variables                                    */
/******************************************************************************/


extern Syslog system_log;

#endif // SYSLOG_H_INCLUDED
