/***************************************************************************/
/*                                                                         */
/*                         C H A R A C T R . H                             */
/*                                                                         */
/*                         Class Header Definition                         */
/*                                                                         */
/*     Content : Class Character                                           */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : April 26, 2002                                      */
/*                                                                         */
/*          This class is a database object that contains all the data     */
/*     and procedures that are relative to the character.                  */
/*                                                                         */
/***************************************************************************/

#ifndef CHARACTR_H_INCLUDED
#define CHARACTR_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*-                       Constant Parameter                              -*/
/*-------------------------------------------------------------------------*/

//#define Character_NB_RACE           10 //use SQL count instead
//#define Character_INVENTORY_SIZE    10
#define Character_NB_ATTRIBUTE      6
#define Character_EQUIP_SIZE        8
#define Character_MAX_LEVEL         20
#define Character_NB_HEALTH         32

/*-------------------------------------------------------------------------*/
/*                              Constants                                  */
/*-------------------------------------------------------------------------*/

#define Character_NAME_STRLEN    16

// Race Identification
/*
#define Character_RACE_HIGH_MAN     0
#define Character_RACE_JINRUI       1
#define Character_RACE_HIGH_ELF     2
#define Character_RACE_DARK_ELF     3
#define Character_RACE_HALFLING     4
#define Character_RACE_DWARF        5
#define Character_RACE_DRACONIAN    6
#define Character_RACE_LIZARDMAN    7
#define Character_RACE_ORC          8
#define Character_RACE_GOBLIN       9
  */
// Valid aligemnt value bit definition
    /*
#define Character_VALID_ALIGMENT_LAWFULL  1
#define Character_VALID_ALIGMENT_BALANCED 2
#define Character_VALID_ALIGMENT_CHAOTIC  4
#define Character_VALID_ALIGMENT_GOOD     8
#define Character_VALID_ALIGMENT_NEUTRAL  16
#define Character_VALID_ALIGMENT_EVIL     32
      */
// sex value

/*#define Character_FEMALE   0
#define Character_MALE     1*/



// Character Status or location

#define Character_LOCATION_UNDEFINED     0
#define Character_LOCATION_RESERVE       1
#define Character_LOCATION_PARTY         2
#define Character_LOCATION_REORDER       3

//#define Character_IN_PARTY2       1
//#define Character_IN_PARTY3       2
//#define Character_AVAILABLE       3
//#define Character_TOBEDESTROYED   4



//#define Character_LOCATION_UNKNOWN    0
//#define Character_LOCATION_PARTY      1
//#define Character_LOCATION_HOME       2



// Skill identifier
        /*
#define Character_SKILL_WEAPON_MASTERY    0
#define Character_SKILL_WEAPON_ART        1
#define Character_SKILL_WEAPON_MAGIC      2
#define Character_SKILL_FORGE             3
#define Character_SKILL_MECHANICS         4
#define Character_SKILL_STEALTH           5
#define Character_SKILL_ASSASIN           6
#define Character_SKILL_ACROBATICS        7
#define Character_SKILL_MAGIC_WEILDER     8
#define Character_SKILL_SPELL_FIRE        9
#define Character_SKILL_SPELL_WATER       10
#define Character_SKILL_SPELL_AIR         11
#define Character_SKILL_SPELL_EARTH       12
#define Character_SKILL_SPELL_LIFE        13
#define Character_SKILL_SPELL_DEATH       14
#define Character_SKILL_SUMMONING         15
#define Character_SKILL_ENCHANT_ITEM      16
#define Character_SKILL_IDENTIFY          17
#define Character_SKILL_DISPEL            18
#define Character_SKILL_HEALING           19
          */
// character hit location ID used by the opponent class

/*
#define Character_HIT_LOCATION_TORSO       0
#define Character_HIT_LOCATION_LARM        1
#define Character_HIT_LOCATION_RARM        2
#define Character_HIT_LOCATION_LLEG        3
#define Character_HIT_LOCATION_RLEG        4
#define Character_HIT_LOCATION_HEAD        5
  */
// equiping functions return values

/*#define Character_ITEM_SUCCESSFULL       0
#define Character_ITEM_INVENTORY_FULL    1
#define Character_ITEM_ALREADY_EQUIPED   2
#define Character_ITEM_ALREADY_UNEQUIPED 3
#define Character_ITEM_UNEQUIPABLE       4
#define Character_ITEM_CURSED            5
#define Character_ITEM_TWOHAND_NOSHIELD  6
#define Character_ITEM_CLASS_RESTRICTION 7*/
//#define Character_ITEM_HEAVY             6 // require STREND
//#define Character_ITEM_REQUIRE_DEXCUN    7
//#define Character_ITEM_REQUIRE_INTWIL    8

// Equipment location value

/*#define Character_EQUIP_NOLOC           -1
#define Character_EQUIP_WEAPON          0
#define Character_EQUIP_SHIELD          1
#define Character_EQUIP_ARMOR           2
#define Character_EQUIP_ACCESSORY       3
#define Character_EQUIP_EXPANDABLE1     4
#define Character_EQUIP_EXPANDABLE2     5
#define Character_EQUIP_EXPANDABLE3     6
#define Character_EQUIP_EXPANDABLE4     7
*/

/*#define WEAPON          0
#define SHIELD          1
#define ARMOR           2
#define ACCESSORY       3
#define EXPANDABLE1     4
#define EXPANDABLE2     5
#define EXPANDABLE3     6
#define EXPANDABLE4     7
*/
#define Character_STRENGTH       0
#define Character_DEXTERITY      1
#define Character_ENDURANCE      2
#define Character_INTELLIGENCE   3
#define Character_CUNNING        4
#define Character_WILLPOWER      5

// int version
#define Character_STR   0
#define Character_DEX   1
#define Character_END   2
#define Character_INT   3
#define Character_CUN   4
#define Character_WIL   5



// to be redefined
/*#define Character_HEALTH_ASLEEP     1       // cannot move, recover easy
#define Character_HEALTH_PARALYSED  2       // cannot move, recover hard
#define Character_HEALTH_PETRIFIED  4       // cannot move, no recover
#define Character_HEALTH_RESERVED   8       //
#define Character_HEALTH_POISONED   16      // lose HP gradualy
#define Character_HEALTH_BLINDED    32      // combat penalty
#define Character_HEALTH_CRIPPLED   64      // special injury, final penalty to combat
#define Character_HEALTH_RESERVED3  128
#define Character_HEALTH_AFFRAID    256     // combat penalty
#define Character_HEALTH_CURSED     512     // Luck rolls are allways failed
#define Character_HEALTH_SEALED     1024    // cannot cast spells
#define Character_HEALTH_DOOMED     2048    // Probability to die instantly
#define Character_HEALTH_FADING     4096    // lose soul continuously
#define Character_HEALTH_RESERVED5  8192
#define Character_HEALTH_RESERVED6  16384
#define Character_HEALTH_RESERVED7  32768

//to be redefined
#define Character_HEALTHID_SLEEP      1       // cannot move, recover easy
#define Character_HEALTHID_PARALYZE   2       // cannot move, recover hard
#define Character_HEALTHID_PETRIFY    3       // cannot move, no recover
#define Character_HEALTHID_RESERVED   4       //
#define Character_HEALTHID_POISON     5       // LOSE HP
#define Character_HEALTHID_BLIND      6       // combat penalty
#define Character_HEALTHID_CRIPPLE    7
#define Character_HEALTHID_RESERVED3  8
#define Character_HEALTHID_FEAR       9       // combat penalty
#define Character_HEALTHID_CURSE      10      // Luck rolls are allways failed
#define Character_HEALTHID_SEAL       11      // cannot cast spells
#define Character_HEALTHID_DOOM       12      // Probability to die instantly
#define Character_HEALTHID_FADE       13    // lose soul continuously
#define Character_HEALTHID_RESERVED5  14
#define Character_HEALTHID_RESERVED6  15
#define Character_HEALTHID_RESERVED7  16
*/

// body status
#define Character_BODY_ALIVE      0
#define Character_BODY_PETRIFIED  1
#define Character_BODY_DEAD       2
#define Character_BODY_ASHES      3
#define Character_BODY_DELETED    4

#define Character_BODY_DISPELLED  10 // Body status of 10 or above does not gives EXP
#define Character_BODY_RUN        11

// character position

//#define Character_POSITION_UNDEFINED 0
//#define Character_POSITION_FRONT 1
//#define Character_POSITION_BACK  2
// Rules: maybe max 4 in a row, and maybe max -1 in front to block back.
// else use 3:3, 2:4 and 4:2 formation. (only 4 character restriction per row.


//#define Character_UNEQUIP_WEAPON_B        1
// attribute generation method identification

/*#define Character_ATTRIBGEN_AVERAGE   0  // 555555
#define Character_ATTRIBGEN_DECREMENT 1  // 876432
#define Character_ATTRIBGEN_OPPOSED   2  // 888222
#define Character_ATTRIBGEN_EXTREME   3  // 993333
#define Character_ATTRIBGEN_LIGHT     4  // 666444*/

// alligment restriction idestification

/*-------------------------------------------------------------------------*/
/*-                             Type Definition                           -*/
/*-------------------------------------------------------------------------*/

/*typedef struct s_Character_attribgen_info
{
   int minimum [ 6 ];
}s_Character_attribgen_info;*/

/*typedef struct dbs_Character
{
   //dbs_Opponent Character_data;//  ;
   char family[ 16 ];//  ;
   char reputation [ 16 ];//  ;
   int rank;//  ;
   int institution;//  ;
   int FKrace;//  ;
   unsigned int cclass;//  ;
   int sex;//  ;
   //fixed age;//  ;
   int experience;//  ;
   int available;//  ;
//   unsigned int inventory [ Character_INVENTORY_SIZE ];//  ;
   unsigned int equiped [ Character_EQUIP_SIZE ];//  ;
//   unsigned int weapon  ;
//   unsigned int shield  ;
//   unsigned int armor  ;
//   unsigned int feet  ;
//   unsigned int hand  ;
//   unsigned int head  ;
//   unsigned int other  ;
   int nb_inventory;//  ;
   int nb_equiped;//  ;
   int encumbrance;//  ;
  // fixed weight;//  ;
   char combatstr [ 13 ];//  ;
}  dbs_Character;*/

/*typedef struct s_Character_healthC
{
   char letter;
   int color;
}s_Character_healthC;

typedef struct s_Character_hstr
{
   s_Character_healthC character [ Character_NB_HEALTH ];
}s_Character_hstr;

typedef struct s_Character_health_info
{
   unsigned int mask; // mask to apllay to check presence
   char adjective[ 17 ]; // string used for display
   char name [ 17 ]; // string used for display
   int red;  // color that must be used for display
   int green;
   int blue;
   int attribute; // attribute lost when raising this resistance
   int TN; // base TN + level modifier + difficulty modifier
}s_Character_health_info;
*/

/*-------------------------------------------------------------------------*/
/*-                        Global Variable                                -*/
/*-------------------------------------------------------------------------*/

//extern s_Character_attribgen_info ATTRIBGEN_INFO [ 5 ];

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class Character : public Opponent
{

   private: char p_name [ Character_NAME_STRLEN ]; // basic name of the opponent
   private: int p_max_HP; // maximum Hit Point of the character
   private: int p_current_HP; // current Hit Point value
   private: int p_max_MP; // maxium spell point of the character
   private: int p_current_MP; // curent spell point of the character
   private: int p_attribute [ Character_NB_ATTRIBUTE ]; // Opponent_attribute
   private: int p_level; // experience level of the character
   private: int p_exp; // experience points accumulated
   private: int p_soul; // current soul level of the character
   //private: unsigned int p_health; // health status of the character
   private: int p_body; // status of the character
   private: int p_location; // character location: party, home, etc.
   private: int p_position; // character ordering number
   private: int p_FKrace; // foreign key to the race table
   private: int p_FKcclass;
   private: int p_FKparty;
   private: int p_rank; // temporaty rank of the character in the party during battles
   //private: int p_FKequiped [ Character_EQUIP_SIZE ];
   private: int p_portraitid; // picture id of the portrait

   private: static bool p_recompile_ranks; // if = true, recompile ranks because important changes happened

   // Constructor and destructor

   public: Character ( void );
   public: virtual ~Character ( void );

   // Property methods

   public: char* name ( void );
   public: void name ( const char *str );
   public: int max_HP ( void );
   public: int current_HP ( void );
   public: int max_MP ( void );
   public: int current_MP ( void );
   public: void attribute ( int attribID, int value );
   public: int attribute ( int attribID );
   public: int attribute_modifier ( int attribID );
   public: void strength ( int value );
   public: int strength ( void );
   public: int STRmodifier ( void );
   public: void dexterity ( int value );
   public: int dexterity ( void );
   public: int DEXmodifier ( void );
   public: void endurance ( int value );
   public: int endurance ( void );
   public: int ENDmodifier ( void );
   public: void intelligence ( int value );
   public: int intelligence ( void );
   public: int INTmodifier ( void );
   public: void cunning ( int value );
   public: int cunning ( void );
   public: int CUNmodifier ( void );
   public: void willpower ( int value );
   public: int willpower ( void );
   public: int WILmodifier ( void );
   public: int level ( void );
   public: int exp ( void );
   public: int soul ( void );
   //public: bool health ( unsigned int healthID );
   //public: unsigned int health ( void );
   public: int body ( void );
   public: void body ( int value );
   public: int location ( void );
   public: void location ( int value );
   public: int position ( void );
   public: void position ( int value );
   public: void FKrace ( int key );
   public: int FKrace ( void );
   public: void FKcclass ( int value );
   public: int FKcclass ( void );
   public: int FKparty ( void );
   public: void FKparty ( int value );

   public: int rank ( void );
   public: void rank ( int value );
   public: int portraitid ( void ) { return ( p_portraitid ); }
   public: void portraitid ( int value ) { p_portraitid = value; }
   /*public: int FKequiped ( int index );
   public: void FKequiped ( int index, int key );*/

   public: bool is_active ( void );
   public: bool is_equipable ( int key );

   public: int resistance ( void ); // combine resistance of class and Race
   public: int weakness ( void ); // combine weakness of class and Race

   //public: void raceS ( char* str );
   //public: void cclassS ( char* str );
   //public: const char* availableS ( void );

   public: int type ( void ) { return (Opponent_TYPE_CHARACTER ); }

   //--- Methods ---

   public: void recover_HP ( int value );
   public: void recover_MP ( int value );
   public: void recover_soul ( int value );

   public: void set_HP ( int value ) { if ( p_body == Character_BODY_ALIVE ) p_current_HP = value; }
   public: void lose_HP ( int value );
   public: void lose_MP ( int value );
   public: void lose_soul ( int value );
   public: void set_MP ( int value ) { if ( p_body == Character_BODY_ALIVE ) p_current_MP = value; }
   //public: void add_health ( unsigned int healthID );
   //public: void remove_health ( unsigned int healthID );


   public: void clean ( void );
   public: void finalize_newchar_stats ( void );
   public: void init_race_attribute ( void );

   public: void autoability ( void );
   //public: unsigned int use_ability ( int inventoryID );

   public: bool check_levelup ( void );
   public: int next_level_exp ( void );

   public: void raise_level ( void );
   public: void gain_exp ( int value );
   public: void lose_exp ( int value );
   public: void rest ( int nb_days, int HPdice, int MPdice, bool fineroom = false  );

   bool check_command ( int cmd_ID, unsigned int interface );

   public: static void compile_ranks ( void );
   public: static void force_recompile_ranks ( void );
   //public: int compile_condition ( void );

   public: char* displayname ( void );

   public: int size ( void ) { return 3; };
   public: int power ( void ) { return 3; };

   //public: void raise_days ( int nb_days );

   //public: void eval_stat_race ( void );
   //public: void eval_stat_class ( void );
   //public: void eval_stat_equipment ( void );
   //public: void eval_stat_encumbrance ( void );

   //--- Private Methods ---

   /*--- Virtual Methods ---*/

   //public: virtual void eval_stat_other ( void );
   //public: virtual void name_combat ( char *str );

   // object relational virtual methods
   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

   /*public: virtual void objdat_to_strdat ( void *dataptr );
   public: virtual void strdat_to_objdat ( void *dataptr );
   public: virtual void child_DBremove ( void );*/

   public: void compile_stat ( void );


};

/*-------------------------------------------------------------------------*/
/*-                           Global Variables                            -*/
/*-------------------------------------------------------------------------*/


//extern const char STR_CHR_SEXC [] [ 2 ];
//extern const char STR_CHR_SEX [] [ 7 ];
extern const char STR_CHR_LOCATION [] [ 11 ];
extern const int EXP_TABLE [ Character_MAX_LEVEL ];
//extern const char STR_CHR_EQUIPERR [] [ 241 ];
//extern s_Character_health_info HEALTH_INFO [ Character_NB_HEALTH ];
extern const char STR_CHR_BODY [] [ 11 ];
const int Character_LEVELUP_ATTRIBUTE_DIVIDER [ 3 ] = { 2, 4, 6 };


/*-------------------------------------------------------------------------*/
/*-               Non-class procedure prototypes                          -*/
/*-------------------------------------------------------------------------*/



// stuff to add
// available spells, prayer, summon

#endif
