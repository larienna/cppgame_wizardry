//------------------------------------------------------------------
//
//                    E N T R A N C E . H
//
//   Content: Class Entrance
//   Programmer: Eric Pietrocupo
//   Starting Date: Januray 23rd, 2014
//
//   Manage the entrance system to enter the maze at various locations
//
//------------------------------------------------------------------


#ifndef ENTRANCE_H_INCLUDED
#define ENTRANCE_H_INCLUDED

#define Entrance_NAME_STRLEN  31

//-------------------------------------------------------------------
//                        Class Definition
//-------------------------------------------------------------------

class Entrance : public SQLobject
{
   //--- Properties ---

   private: char p_name [ Entrance_NAME_STRLEN ]; // name of the entrance
   private: int p_x; // x position to start
   private: int p_y; // y position to start
   private: int p_z; // z position to start
   private: int p_face; // facing of start
   private: int p_unlocked; // determined if the entrance is accessible

   // --- Constructor & Destructor

   public: Entrance ( void );
   public: ~Entrance ( void );

   // --- Property Methods ---


   public: const char* name ( void ) { return ( p_name); }
   public: void name ( const char* value ) { strncpy ( p_name, value, Entrance_NAME_STRLEN );}
   public: int x ( void ) { return (p_x); }
   public: void x ( int value ) { p_x = value; }
   public: int y ( void ) { return (p_y); }
   public: void y ( int value ) { p_y = value; }
   public: int z ( void ) { return (p_z); }
   public: void z ( int value ) { p_z = value; }
   public: int face ( void ) { return (p_face); }
   public: void face ( int value ) { p_face = value; }
   public: bool unlocked ( void );
   public: void unlocked ( bool value );

   // --- Method ---

   // --- Virtual Methods ---

   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);
};

#endif // ENTRANCE_H_INCLUDED
