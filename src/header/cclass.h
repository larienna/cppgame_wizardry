/**************************************************************************/
/*                                                                        */
/*                              C C L A S S . H                           */
/*                             Class Definition                           */
/*                                                                        */
/*     Content : CClass ( Character Class ) class definition              */
/*     Programmer : Eric PIetroucpo                                       */
/*     Starting Date : June 13th, 2004                                    */
/*     License : GNU General Public License                               */
/*                                                                        */
/**************************************************************************/

/*------------------------------------------------------------------------*/
/*-                           Class Parameter                            -*/
/*------------------------------------------------------------------------*/

//#define CClass_NB_SKILL  2

#define CClass_NAME_STRLEN       13
#define CClass_INITIAL_STRLEN    4

/*------------------------------------------------------------------------*/
/*-                             Constants                                -*/
/*------------------------------------------------------------------------*/

// most of these constrants needs to be redefined

// Skills Constant

/*#define CClass_SKILL_NONE              0
#define CClass_SKILL_MAGIC_WEILDING    1
#define CClass_SKILL_PRAYER            2
#define CClass_SKILL_STEALTH           3 // ( Hide, Scouting )
#define CClass_SKILL_SUMMON            4
#define CClass_SKILL_IDENTIFY          5
#define CClass_SKILL_FORGE_ITEM        6
#define CClass_SKILL_MECHANIC          7
#define CClass_SKILL_ENCHANT_ITEM      8 // ( also create expandables )
#define CClass_SKILL_DISPEL            9
#define CClass_SKILL_HEALING           10
#define CClass_SKILL_MARTIALART        11 // hand to hand combat fighting*/

// Aligment Constants

/*#define CClass_ALIGMENT_KIND         1 // life         kn
#define CClass_ALIGMENT_PEACEFULL    2 // water        pf
#define CClass_ALIGMENT_LIVELY       4 // Air          lv
#define CClass_ALIGMENT_SELLFISH     8 // Death        sf
#define CClass_ALIGMENT_AGGRESSIVE   16 // Fire         ag
#define CClass_ALIGMENT_STOICAL      32 // Earth        st*/

// Sex restriction

/*#define CClass_SEX_NONE       0
#define CClass_SEX_MALE       1
#define CClass_SEX_FEMALE     2*/

// Spell Casting Element Allowed

/*#define CClass_SPELLELEMENT_NONE        0
#define CClass_SPELLELEMENT_LIFE        1 // life         kn
#define CClass_SPELLELEMENT_WATER       2 // water        pf
#define CClass_SPELLELEMENT_AIR         4 // Air          lv
#define CClass_SPELLELEMENT_DEATH       8 // Death        sf
#define CClass_SPELLELEMENT_FIRE       16 // Fire         ag
#define CClass_SPELLELEMENT_EARTH      32 // Earth        st
*/

// check item instead
/*#define CClass_WEAPON_NONE        0
#define CClass_WEAPON_SWORD       1
#define CClass_WEAPON_MACE        2
#define CClass_WEAPON_SPEAR       4
#define CClass_WEAPON_BOW         8
#define CClass_WEAPON_RIFFLE      16
#define CClass_WEAPON_WAND        32
#define CClass_WEAPON_CHAIN       64
#define CClass_WEAPON_DAGGERS     128

#define CClass_ARMOR_MINIMAL   1
#define CClass_ARMOR_LIGHT     2
#define CClass_ARMOR_MEDIUM    4
#define CClass_ARMOR_HEAVY     8

#define CClass_SHIELD_MINIMAL   1
#define CClass_SHIELD_LIGHT     2
#define CClass_SHIELD_HEAVY     4*/

#define CClass_MAGICSCHOOL_NONE        0
#define CClass_MAGICSCHOOL_ARCANE      1        //0000 0001
#define CClass_MAGICSCHOOL_DIVINE      2        //0000 0010
#define CClass_MAGICSCHOOL_ALCHEMY     4        //0000 0100
#define CClass_MAGICSCHOOL_PSIONIC     8        //0000 1000
#define CClass_MAGICSCHOOL_HIGHARCANE  0x0010   //0001 0000
#define CClass_MAGICSCHOOL_HIGHDIVINE  0x0020   //0010 0000
#define CClass_MAGICSCHOOL_HIGHALCHEMY 0x0040   //0100 0000
#define CClass_MAGICSCHOOL_HIGHPSIONIC 0x0080   //1000 0000
#define CClass_MAGICSCHOOL_ALLARCANE   0x0011   //0001 0000
#define CClass_MAGICSCHOOL_ALLDIVINE   0x0022   //0010 0000
#define CClass_MAGICSCHOOL_ALLALCHEMY  0x0044   //0100 0000
#define CClass_MAGICSCHOOL_ALLPSIONIC  0x0088   //1000 0000
#define CClass_MAGICSCHOOL_OTHER       0x0100   //1 0000 0000
#define CClass_MAGICSCHOOL_ALL         0x010F   //1 0000 1111
#define CClass_MAGICSCHOOL_HIGHALL     0x01FF   //1 1111 1111
#define CClass_MAGICSCHOOL_ONLYLOW     0x000F   //0000 1111
#define CClass_MAGICSCHOOL_SPECIALATT  0x0200   //10 0000 0000

/// List of fields for easier use of get set functions

#define CClass_FIELD_NAME     0
#define CClass_FIELD_INITIAL  1
#define CClass_FIELD_AD       2
#define CClass_FIELD_MD       3
#define CClass_FIELD_HPDICE   4
#define CClass_FIELD_MPDICE   5
#define CClass_FIELD_PS       6
#define CClass_FIELD_MS       7
#define CClass_FIELD_ATTDIV   8
#define CClass_FIELD_POWDIV   9
#define CClass_FIELD_MELEECS  10
#define CClass_FIELD_RANGECS  11
#define CClass_FIELD_PROFIENCY   12
#define CClass_FIELD_MAGIC_SCHOOL   13
#define CClass_FIELD_PICTUREID   14
#define CClass_FIELD_RESISTANCE  15
#define CClass_FIELD_WEAKNESS 16
#define CClass_FIELD_PASSIVE_SKILL  17
#define CClass_FIELD_ACTIVE_SKILL   18



/*#define CClass_AD_NONE     0
#define CClass_AD_LOW      4
#define CClass_AD_MEDIUM   8
#define CClass_AD_HIGH     12

#define CClass_MAD_NONE     0
#define CClass_MAD_LOW      4
#define CClass_MAD_MEDIUM   8
#define CClass_MAD_HIGH     12

#define CClass_MHITP_HIGH   2
#define CClass_MHITP_MEDIUM 3
#define CClass_MHITP_LOW    4*/


/*#define CClass_SPELLPROGRESS_NONE     0
#define CClass_SPELLPROGRESS_SLOW     2
#define CClass_SPELLPROGRESS_MEDIUM   3
#define CClass_SPELLPROGRESS_FAST     4*/

// Weapon Restriction

/*official new stat

- Aligment allowed
- Weapon range Restriction
- Weapon attrib Restriction
- Weapon nb_hand Restriction
- Armor Profiency
- Shield Profiency
- Element Resistance
- Element Effect
- Health Effect
- Magik Properties
- Spell Casting Element
- Magic Amplification 1 to 3
- Physical Damage y 4 to 12
- Magikal Damage y  4 to 12
- 3 Skill set
- Base multi hit penalty -10, -8, -6
- Active Defense level ( proportion of stat ) /50 , /5, /4, /3
- Magical Defense ( proportion of stat ) /50, /5, /4, /3
*/

/*------------------------------------------------------------------------*/
/*-                          Structure Definition                        -*/
/*------------------------------------------------------------------------*/

/*typedef struct dbs_CClass
{
   char name [ 21 ];//  ;
   char initial [ 4 ];//  ;
   unsigned char aligment_allowed;//  ;
   unsigned short elm_resist;//  ;
   unsigned short elm_effect;//  ;
   unsigned short hlt_resist;//  ;
   unsigned short hlt_effect;//  ;
   unsigned short magikproperty;//  ;
   unsigned char spell_element;//  ;
   int spell_amplification;//  ;
   int spell_progress;//  ;
   //int skill [ CClass_NB_MAX_SKILL ];//  ;
// unsigned char wprof_range  ;
//   unsigned char wprof_attrib  ;
//   unsigned char wprof_nb_hand  ;
//   int wprof_size  ;
   unsigned char weapon_profiency;//  ;
   int armor_profiency;//  ;
   int shield_profiency;//  ;
   int DMGdiv;//  ;
   int MDMGdiv;//  ;
   int mulhitdiv;//  ;
   int baseAD;//  ;
   int baseMAD;//  ;
   int HPdiv;//  ;
   int MPdiv;//  ;
}  dbs_CClass;*/

/*------------------------------------------------------------------------*/
/*-                         Class Declaration                            -*/
/*------------------------------------------------------------------------*/

class CClass : public EnhancedSQLobject
{

   public: const static int NB_FIELD = 19;
   private: static s_SQLfield p_SQLfield [ NB_FIELD ]; // NEW list the definition of all the fields in order
   private: s_SQLdata p_SQLdata [ NB_FIELD ]; // actual data which can be integer or string pointer

   //--- Properties ---

   private: char p_name [ CClass_NAME_STRLEN ]; // name of the class
   private: char p_initial [ CClass_INITIAL_STRLEN ];

   private: char p_profiency [ EnhancedSQLobject_STRFIELD_LEN ];
   private: char p_magic_school [ EnhancedSQLobject_STRFIELD_LEN ];
   private: char p_resistance [ EnhancedSQLobject_STRFIELD_LEN ];
   private: char p_weakness [ EnhancedSQLobject_STRFIELD_LEN ];

   //private: int p_AD; // active defense bonus
   //private: int p_MD; // Magic defense bonus
   //private: int p_HPdice; // hit die
   //private: int p_MPdice; // mana die
   //private: int p_PS; // Base Physical Save
   //private: int p_MS; // Base Mental Save
   //private: int p_INIT; // base initiative
   //private: int p_WSP; // Weapon skill progress
   //private: int p_MSP; // Magic skill progress
   //private: int p_MDMGY; // Magic damage die
   //private: int p_PCS; // Physical combat skill
   //private: int p_MCS; // Mental combat skill
   //private: unsigned int p_profiency;
   //private: unsigned char p_weapon_profiency;
   //private: unsigned char p_armor_profiency;
   //private: unsigned char p_shield_profiency;
   //private: int p_magic_school;
   //private: int p_skill [ CClass_NB_SKILL ];
   //private: int p_pictureID; // identification number of the class picture
   //private: unsigned int p_resistance; // special resistance of the class
   //private: unsigned int p_weakness; // special weakness property of class

//private: int p_spell_progress; // accumulated point required to learn spell
   //private: int p_baseAD;
   //private: int p_baseMAD;

   //--- Constructor & Destructor ---

   public: CClass ( void );
   public: ~CClass ( void );

   //--- Property Methods ---

   public: const char* name ( void );
   public: void name ( const char* value );
   public: const char* initial ( void );
   public: void initial ( const char* value );
   public: const char* profiency_str ( void ) { return ( p_profiency ); }
   public: const char* magic_school_str ( void ) { return ( p_magic_school ); }
   public: const char* resistance_str ( void ) { return ( p_resistance ); }
   public: const char* weakness_str ( void ) { return ( p_weakness ); }


   //public: int spell_progress ( void );
   //public: void spell_progress ( int value );
   /*public: int skill  ( int index );
   public: void skill  ( int index, int value );
   public: bool have_skill ( int value );*/
/*   public: unsigned char wprof_range ( void );
   public: void wprof_range ( unsigned char value );
   public: unsigned char wprof_attrib ( void );
   public: void wprof_attrib ( unsigned char value );
   public: unsigned char wprof_nb_hand ( void );
   public: void wprof_nb_hand ( unsigned char value );
   public: int wprof_size ( void );
   public: void wprof_size ( int value );*/
   public: int profiency ( void );
   public: void profiency ( int value );
   /*public: int armor_profiency ( void );
   public: void armor_profiency ( int value );
   public: int shield_profiency ( void );
   public: void shield_profiency ( int value );*/

   public: int AD ( void );
   //public: void AD ( int value );
   public: int MD ( void );
   //public: void MD ( int value );
   public: int HPdice ( void );
   //public: void HPdice ( int value );
   public: int MPdice ( void );
   //public: void MPdice ( int value );
   public: int PS ( void );
   //public: void PS ( int value );
   public: int MS ( void );
   //public: void MS ( int value );
   //public: int INIT ( void );
   //public: void INIT ( int value );
   public: int attdiv ( void );
   //public: void attdiv ( int value );
   public: int powdiv ( void );
   //public: void powdiv ( int value );
   //public: int MDMGY ( void );
   //public: void MDMGY ( int value );
   public: int melee_CS ( void );
   //public: void melee_CS ( int value );
   public: int range_CS ( void );
   //public: void range_CS ( int value );

   public: int magic_school ( void );
   //public: void magic_school ( int value );

   public: int pictureID ( void );
   //public: void pictureID ( int value );
   public: int resistance ( void );
   //public: void resistance ( int value );
   public: int weakness ( void );
   //public: void weakness ( int value );


   //--- Methods ---

   //--- Virtual Methods ---

   // object relational virtual methods
   /*public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);*/

   protected: void virtual template_load_completion ( void ) {} // no template to load
};

/*------------------------------------------------------------------------*/
/*-                         Global Variables                             -*/
/*------------------------------------------------------------------------*/

//extern const char STR_CLS_MAGIC [] [ 11 ];

extern s_EnhancedSQLobject_strfield STRFLD_MAGIC_SCHOOL [ EnhancedSQLobject_HASH_SIZE ];
