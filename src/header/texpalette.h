/******************************************************************/
/*                                                                */
/*                        T E X P A L E T T E . H                 */
/*                                                                */
/*    Content: Texture + TexPalette + TexSample Class             */
/*    Programmer: Eric Pietrocupo                                 */
/*    Creation Date: may 5th, 2013                                */
/*                                                                */
/*    This class manage database texture information and preload  */
/*    textures that are going to be used in the maze.             */
/*                                                                */
/******************************************************************/

#ifndef TEXPALETTE_H_INCLUDED
#define TEXPALETTE_H_INCLUDED

// I am not sure yet about the note below, maybe in future version but not for now
// note: Internal texture resolution is 256x256, but source could have a lower resolution.
// I use a larger resolution now to allow using texture with bigger resolutions later
// Front wall in maze id 320x368, so 266x256 is reasonable and should give reasonable file size

// ------------------------- Constant Parameter -----------------------

#define TexPalette_NB_TEXTURE       40

#define TexPalette_NB_GRID          4
#define TexPalette_NB_MASKEDWALL    16
#define TexPalette_NB_MASKEDFLOOR   16
//#define Texture_NB_WALLOBJECT    8
//#define Texture_NB_CEILINGOBJECT 16
//#define Texture_NB_FLOOROBJECT   16

#define Texture_NB_LAYER         4
#define Texture_SIZE             256  // 256x256
#define TEXSIZE                     Texture_SIZE // alias

#define TexSample_NB_TEXPALETTE     32

// -------------------------- Constants --------------------------

#define TexPal_IDX_WALL       0  // 0
#define TexPal_IDX_DOOR       1
#define TexPal_IDX_FLOOR      2
#define TexPal_IDX_CEILING    3
#define TexPal_IDX_GRID       4  // 4-7
#define TexPal_IDX_MWALL      8  // 8-23
#define TexPal_IDX_MFLOOR     24 // 24-39


// --- texture type ---
// should be used for positionning in palette

/*#define Texture_TYPE_UNDEFINED      -1 // mostly indicate that the texture has not been defined, so ignore.
#define Texture_TYPE_FLOOR          0 // include ceiling
#define Texture_TYPE_MASKEDFLOOR    1 // include ceiling
#define Texture_TYPE_WALL           2
#define Texture_TYPE_MASKEDWALL     3
#define Texture_TYPE_GRID           4*/
/*#define Texture_TYPE_OBJECTFLOOR    6
#define Texture_TYPE_OBJECTCEILING  7
#define Texture_TYPE_OBJECTWALL     8*/

/*#define Texture_CATEGORY_FLOOR      0
#define Texture_CATEGORY_WALL       1*/

// --- texture sets ---
/*#define Texture_SET_UNDEFINED       -1 // determine that the texture has not been defined
#define Texture_SET_ADVENTURE       0 // each adventure can be openned as a texture set
#define Texture_SET_HERHEXEN        1 // default texture set from the Heretic/Hexen Video game
*/
// texture multiplier defined in set. Heretic/hexen will have x2

// eveventually, other texture set could be used
//#define Texture_SET_WIZARDRYSNES
//Wizardry PC
//Hexen 2 and Heretic 2 (more limited since all 3D. No object for example


// --- Position ---


/*#define Texture_ALIGN_H_LEFT          1
#define Texture_ALIGN_H_QUARTER       2
#define Texture_ALIGN_H_CENTER        3
#define Texture_ALIGN_H_TQUARTER      4
#define Texture_ALIGN_H_RIGHT         5*/

// maybe use 1/16th offset instead of center positionnning. COuld adjust textures in consequences for centering.
#define Texture_ALIGN_H_MASK          15    //00001111
#define Texture_ALIGN_V_MASK          240   //11110000

/*#define Texture_ALIGN_V_UP            16    //0010000
#define Texture_ALIGN_V_QUARTER       32    //0100000
#define Texture_ALIGN_V_CENTER        48    //0110000
#define Texture_ALIGN_V_TQUARTER      64    //1000000
#define Texture_ALIGN_V_DOWN          80    //1010000*/


#define Texture_TILING_VERTICAL        256  //   0000 0001 00000000
#define Texture_TILING_HORIZONTAL      512  //   0000 0010 00000000
#define Texture_TILING_BOTH            768  //   0000 0011 00000000
#define Texture_TILING_XORVERTICAL    1024  //   0000 0100 00000000
#define Texture_TILING_XORHORIZONTAL  2048  //   0000 1000 00000000
#define Texture_TILING_XORBOTH        3072  //   0000 1100 00000000
#define Texture_TILING_MASK           3840  //   0000 1111 00000000

#define Texture_TRANSPARENT           4096  //   0001 0000 00000000
//#define Texture_UPSIDEDOWN            8192  //   0010 0000 00000000

#define Texture_ZOOM_X2               16384 //   0100 0000 00000000
#define Texture_ZOOM_X4               32768 //   1000 0000 00000000
#define Texture_ZOOM_X8               49152 //   1100 0000 00000000
#define Texture_ZOOM_MASK             49152 //   1100 0000 00000000
//#define TexPalette_ZOOM_DOOR        65536 // 1 0000 0000 00000000 // special zoom for doors


// -------------------------- Class Definition -------------------

class Texture : public SQLobject
{

   // --- Properties ---

   //private: BITMAP *p_tex; // Generated texture ready to use (not saved in database)

   //private: int p_texset; // Texture set to use. Currently, base and adventure are the only available.
                     // But this would allow introducing new set without affecting previous set.
   //private: int p_type; // type of texture: wall, floor, object, etc. load from sub datafile
   private: int p_paletteid; // Palette where the texture will be used
   private: int p_texid; // Identification number of the texture inside the palette
   private: int p_texcode [ Texture_NB_LAYER ]; // identificaiton number of the picture in the datafile for background
   private: unsigned int p_texposition [ Texture_NB_LAYER]; // background alignment and tiling
   //private: int p_fgpicid; // identification number of the picture in the datafile for foreground
   //private: unsigned int p_fgposition; // foreground alignment and tiling

   //private: static int p_editor_paletteid; // ID of the edited palette
   //private: static int p_editor_texid; // index of the edited texture
   private: static int p_editor_active_layer; // ID of the currently edited layer
   private: static BITMAP *p_editor_builtex; // temporary built texture for editor

   // --- Constructor destructor ---

   public: Texture ( void );
   //public: Texture ( Texture &copytex );
   public: virtual ~Texture ( void );
   public: void initialise ( void );
   public: void clear ( void ); // only clear content of the texture but keep ID.

   // --- Property Methods

   //public: BITMAP* tex ( void ) { return ( p_tex ); } // return pointer on internal bitmap
   //public: int texset ( void ) { return ( p_texset ); }
   //public: void texset ( int value ) { p_texset = value; }
   //public: int type ( void ) { return ( p_type ); }
   //public: void type ( int value ) { p_type = value; }
   public: int paletteid ( void ) { return ( p_paletteid ); }
   public: void paletteid ( int value ) { p_paletteid = value; }
   public: int texid ( void ) { return ( p_texid ); }
   public: void texid ( int value ) { p_texid = value; }
   public: int texcode ( int index ) { return ( p_texcode [ index ] ); }
   public: void texcode ( int index, int value ) { p_texcode [ index ] = value; }
   public: int texposition ( int index ) { return ( p_texposition [ index ] ); }
   public: void texposition ( int index, int value, int xoffset, int yoffset );
   //public: void build_texposition ( int layer, int xoffset, int yoffset, int zoom, int tiling);
      // X and Y offset are a value from 0 to 15
   /*public: int fgpicid ( void ) { return ( p_fgpicid ); }
   public: void fgpicid ( int value ) { p_fgpicid = value; }
   public: int fgposition ( void ) { return ( p_fgposition ); }
   public: void fgposition ( int value, int xoffset, int yoffset );*/
      // X and Y offset are a value from 0 to 15

   public: int xoffset ( int layer );
   public: int yoffset ( int layer );
   public: int zoom ( int layer );
   public: bool transparent ( int layer);
   public: int tiling ( int layer );
   public: int tiling_xor ( int layer );
   //public: bool upsidedown ( int layer);
   public: void xoffset_inc ( int layer);
   public: void xoffset_dec ( int layer);
   public: void yoffset_inc ( int layer);
   public: void yoffset_dec ( int layer);
   public: void zoom_inc ( int layer);
   public: void zoom_dec ( int layer);
   public: void transparent_chg ( int layer);
   public: void tiling_chg ( int layer );
   public: void tiling_xor_chg ( int layer );
   //public: void upsidedown_chg ( int layer);



   // --- Methods ---

   public: void edit ( int paletteid, int texid ); //Start texture editor
   public: void build ( BITMAP *bmp, bool door = false ); // create bitmap and the texture

   private: void draw_background ( void );


   // --- Virtual Methods ---

   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

};

// A second class to manage palette of textures more properly than ina structure

class TexPalette
{
   // properties

   public: Texture tex [TexPalette_NB_TEXTURE];
   public: BITMAP* bmp [TexPalette_NB_TEXTURE];
   private: int p_palID; // identification of the palette

   // Constructor & Destructor

   public: TexPalette ( void );
   public: ~TexPalette ( void );

   // Property methods

   /*public: BITMAP* bmp ( int index );
   public: void tex ( int index, int primary_key );
   public: Texture tex ( int index );*/

   // Methods

   public: void load ( int index );
   public: void unload ( void ); // clear current objects and builds
   //public: void update ( void );
   //public: void insert ( int index );
   public: void build ( void );
   public: void clear ( void ); // erase built bmp
   public: void create ( void ); // used to create the bitmaps of the palette
      // which cannot be created before allegro init.

   public: void destroy ( void );

};


class TexSample
{
   // --- Properties ---
   public: BITMAP* bmp [TexSample_NB_TEXPALETTE];

   // --- Constructor & Destructor ---

   public: TexSample ( void ) {}
   public: ~TexSample ( void ) {}

   // --- Method ---

   public: void load_build ( void );

   public: void create ( void ); // used to create the bitmaps of the palette
      // which cannot be created before allegro init.

   public: void destroy ( void );
};
// ------------------------ Structure Definition -----------------

/*typedef struct s_TexPalette_sample
{
   BITMAP* sample [ Maze_NB_TEXPALETTE ]; //32 palettes
}*/

//typedef struct s_Texture_palette
//{
//   BITMAP* tex [ TexPalette_NB_TEXTURE ];

   /*BITMAP* floor;
   BITMAP* ceiling;
   BITMAP* door; // not sure if shrunk smaller than wall. Maybe take full wall.
   BITMAP* wall [ Texture_NB_WALL ]; // odd, even, quad, octo
   BITMAP* grid [ Texture_NB_GRID ];
   BITMAP* maskedwall [ Texture_NB_MASKEDWALL ];
   BITMAP* maskedfloor [ Texture_NB_MASKEDFLOOR ];*/
   //BITMAP* wallobj [ Texture_NB_WALLOBJECT ];
   //BITMAP* ceilingobj [ Texture_NB_CEILINGOBJECT ];
   //BITMAP* floorobj [ Texture_NB_FLOOROBJECT ];

//}s_Texture_palette;

// ---------------------- Procedures ---------------------------

//void build_texture_palette ( s_Texture_palette &tmppal, int paletteid ); //build all textures in texpal global variable

//does not work because information on texture source not saved in palette, only the final results
//void load_texture_palette ( s_Texture_palette &tmppal, int paletteid ); // load a palette from the database to the active palette
//void save_texture_palette ( s_Texture_palette tmppal ); // save active palette in database.
//void destroy_texture_palette ( s_Texture_palette tmppal); // deallocate bitmap before programm closes and before palette change.

// ---------------------- Global Variables -----------------------

extern int mazepalid; // identification number of the current palette for the maze.
extern TexPalette mazepal; // palette used for the maze and for display in the editor of active tile
extern TexPalette editorpal; // palette used for active palette in editor.
extern TexSample palsample;

extern const char STR_TEX_PALETTE [][25];

//extern s_Texture_palette texpal;

//extern

#endif // TEXPALETTE_H_INCLUDED
