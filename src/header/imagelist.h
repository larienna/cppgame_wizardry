/***************************************************************************/
/*                                                                         */
/*                         I M A G E L I S T . H                           */
/*                         Class Definitnon                                */
/*                                                                         */
/*     Content : Class ImageList                                           */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting date : May 21st, 2013                                      */
/*                                                                         */
/*     Interface to select a image from a list of images.                  */
/*                                                                         */
/***************************************************************************/


#ifndef IMAGELIST_H_INCLUDED
#define IMAGELIST_H_INCLUDED

/*-------------------------------------------------------------------------*/
/*-                       Constant Class Parameter                        -*/
/*-------------------------------------------------------------------------*/

#define ImageList_MAX_NB_ITEM         1024
#define ImageList_STR_SIZE            81
#define ImageList_FONT                FNT_print

/*-------------------------------------------------------------------------*/
/*-                          Type Definition                              -*/
/*-------------------------------------------------------------------------*/

typedef struct s_ImageList_item
{
   int answer; // answer value
   BITMAP *bmp; // pointer on the picture to draw. Only pointers, no real bitmaps
}s_ImageList_item;

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class ImageList
{

   // Properties

   private: char p_title [ ImageList_STR_SIZE ]; // Text written at the top of the List
   private: int p_nb_item; // count number of add items
   private: int p_page_width; // nb of picture to draw horozontally
   private: int p_page_height; // nb of picture to draw vertically
   private: int p_picture_width; // Max size of the picture, shrink if necessary
   private: int p_picture_height; // Max size of the picture, shrink if necessary
   private: int p_page_pos; // current location of the page, linear position, x/y adjusted on the fly
   private: int p_cursor; //current cursor position, calculate x/y position on the fly
   //private: int p_cursor_y; //current cursor position
   private: s_ImageList_item p_item [ ImageList_MAX_NB_ITEM ]; // strings to draw in the list.
   private: bool p_stretch; // determine if the bitmap are stretched when the size des not fit.

   // Constructor & Destructor

   public: ImageList ( void );
   public: ImageList ( const char* str , int page_width = 3, int page_height=1, int pic_width=128, int pic_height = 128, bool stretch = false );
   public: ~ImageList ( void );


   // property methods

   public: void title ( const char* str ) { strcpy ( p_title, str ); }
   public: const char* title ( void ) { return ( p_title ); }
   public: void page_size ( int width, int height ) { p_page_width = width; p_page_height = height; }
   public: int page_width ( void ) { return ( p_page_width ); }
   public: int page_height ( void ) { return ( p_page_height ); }
   public: void picture_size ( int width, int height ) { p_picture_width = width; p_picture_height = height; }
   public: int picture_width ( void ) { return ( p_picture_width ); }
   public: int picture_height ( void ) { return ( p_picture_height ); }

   public: void page_pos ( int value ) { p_page_pos = value; }
   public: int page_pos ( void ) { return (p_page_pos); }
   public: int nb_item () { return ( p_nb_item ); }
   public: int cursor ( void ) { return ( p_cursor ); }
   //public: int cursor_y ( void ) = { return ( p_cursor_y ); }
   //public: void cursor ( int x, int y ) = { p_cursor_x = x; p_cursor_y = y; }
   public: int answer ( int index ) { return p_item [ index ].answer; }
   public: int answer ( void ) { return p_item [ p_cursor ].answer; } // answer of currently selected item

   // method

   public: void add_item ( int answer, BITMAP* bmp );
   public: void add_datref ( DatafileReference<BITMAP*> datref, int index );
   public: void add_datref ( DatafileReference<BITMAP*> datref );
   public: int show ( int x, int y, bool reset_cursor = false );
   public: int get_x_pos ( int item );
   public: int get_y_pos ( int item );

};

#endif // IMAGELIST_H_INCLUDED
