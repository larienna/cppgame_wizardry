/***************************************************************************/
/*                                                                         */
/*                        W I N M E S S A . H                              */
/*                         Class Definition                                */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinMessage                                          */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 23rd, 2002                                 */
/*                                                                         */
/*          This class inherit from window to draw a simple notification   */
/*     message on the screen.                                              */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                         Class Parameter                               -*/
/*-------------------------------------------------------------------------*/

#define WinMessage_FONT   FNT_print
#define WinMessage_COLOR  General_COLOR_TEXT

/*-------------------------------------------------------------------------*/
/*-                          Constants                                    -*/
/*-------------------------------------------------------------------------*/

#define WinMessage_X   320
#define WinMessage_Y   100

/*-------------------------------------------------------------------------*/
/*-                         Class Definition                              -*/
/*-------------------------------------------------------------------------*/

class WinMessage : public Window
{
   //--- Properties ---

   private: char p_message [ 241 ];
   private: short p_center_x;

   //--- Constructor & Destructor ---

//   public: WinMessage ( string &message, short center_x = 320, short y_pos = 100 );
   public: WinMessage ( const char *question, short center_x = 320, short y_pos = 100, bool translucent = false  );
//   public: ~WinMessage ( void );

   //--- Virtual Methods ---

   public: virtual void preshow ( void );
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

   //--- private method ---

   private: void construct ( const char *message, short center_x, short y_pos );
   private: int line_count ( void );
   private: int max_line_size ( void );

};
