
/***************************************************************************/
/*                                                                         */
/*                                E V E N T . H                            */
/*                             Class Definition                            */
/*                                                                         */
/*     Content : Class Race                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : 9 janvier, 2014                                     */
/*     License : GNU General Public License                                */
/*                                                                         */
/*          This class encapsulate and manage the events for the maze.     */
/*                                                                         */
/***************************************************************************/

#ifndef EVENT_H_INCLUDED
#define EVENT_H_INCLUDED


/*-------------------------------------------------------------------------*/
/*-                          Constants                                    -*/
/*-------------------------------------------------------------------------*/

#define Event_STR_VARIABLE_LEN  51
#define Event_MSG_VARIABLE_LEN  256
#define Event_NB_EVENT 32

// --- Trigger ---

#define Event_TRIGGER_MOVEIN              0 //
#define Event_TRIGGER_FACE_NORTH          1 //
#define Event_TRIGGER_FACE_EAST           2 //
#define Event_TRIGGER_FACE_SOUTH          3
#define Event_TRIGGER_FACE_WEST           4
#define Event_TRIGGER_UNDEFINED1          5
#define Event_TRIGGER_UNDEFINED2          6
#define Event_TRIGGER_SEARCH_ROOM         7 //
#define Event_TRIGGER_SEARCH_NORTH        8 //
#define Event_TRIGGER_SEARCH_EAST         9
#define Event_TRIGGER_SEARCH_SOUTH        10
#define Event_TRIGGER_SEARCH_WEST         11
#define Event_TRIGGER_USE_ITEM            12 // standby: problem passing item ID to lock event
#define Event_TRIGGER_OPENDOOR_NORTH      13
#define Event_TRIGGER_OPENDOOR_EAST       14
#define Event_TRIGGER_OPENDOOR_SOUTH      15
#define Event_TRIGGER_OPENDOOR_WEST       16 //
//#define Event_TRIGGER_
//#define Event_TRIGGER_
//#define Event_TRIGGER_
//#define Event_TRIGGER_

// ---- lock events ----

#define Event_LOCK_AUTOPASS            0 //
#define Event_LOCK_QUESTION            1 //
#define Event_LOCK_ENCOUNTERLOCK       2 //
#define Event_LOCK_HIDDENLOCK          3 //
#define Event_LOCK_RIDDLE              4
#define Event_LOCK_COMBAT              5
#define Event_LOCK_NPC                 6

// ---- pass/fail event  ----

#define Event_PASSFAIL_DONOTHING       0 //
#define Event_PASSFAIL_ENDGAME         1 //
#define Event_PASSFAIL_MOVE            2 //
#define Event_PASSFAIL_TELEPORT        3 //
#define Event_PASSFAIL_GAINITEM        4 //
#define Event_PASSFAIL_TRADEITEM       5 //
#define Event_PASSFAIL_TRAP            6
#define Event_PASSFAIL_COMBAT          7
#define Event_PASSFAIL_EXITMAZE        8 //
#define Event_PASSFAIL_ROTATE          9 //
#define Event_PASSFAIL_MESSAGE         10 //
#define Event_PASSFAIL_STORYTEXT       11
#define Event_PASSFAIL_NPC             12
#define Event_PASSFAIL_ELEVETOR        13 //
//#define Event_PASSFAIL_

//---------- Event Variables ----------

// Move

#define Event_PF_MOVE_NORTH      1
#define Event_PF_MOVE_EAST       2
#define Event_PF_MOVE_SOUTH      3
#define Event_PF_MOVE_WEST       4
#define Event_PF_MOVE_BACK       5
#define Event_PF_MOVE_FORWARD    6
#define Event_PF_MOVE_UP         7
#define Event_PF_MOVE_DOWN       8

// Rotate

#define Event_PF_ROTATE_LEFT        1
#define Event_PF_ROTATE_RIGHT       2
#define Event_PF_ROTATE_BACK        3
#define Event_PF_ROTATE_RANDOM      4
#define Event_PF_ROTATE_FACENORTH   5
#define Event_PF_ROTATE_FACEEAST    6
#define Event_PF_ROTATE_FACESOUTH   7
#define Event_PF_ROTATE_FACEWEST    8

/*-------------------------------------------------------------------------*/
/*-                        Type Definition                                -*/
/*-------------------------------------------------------------------------*/

typedef struct s_Event_passfail
{
   int event; // type of event if the lock is passed
	int var1; // 1st variable of the pass event
	int var2; // 2nd variable of the pass event
	int var3; // 3rd variable of the pass event
	int var4; // 4th variable of the pass event
	char msg [ Event_MSG_VARIABLE_LEN ]; // pass event message display
	char str [ Event_STR_VARIABLE_LEN ]; // pass event text variable
}s_Event_passfail;

/*-------------------------------------------------------------------------*/
/*-                       Class Definition                                -*/
/*-------------------------------------------------------------------------*/


class Event : public SQLobject
{
   // Properties

	private: int p_trigger; // condition to trigger the event
	private: int p_lock_event; // lock event that occurs when triggered
	private: int p_lock_key; // key necessary to pass the lock event
	private: int p_lock_var; // extra variable that could be used by the lock event
	private: char p_lock_msg [ Event_MSG_VARIABLE_LEN ]; // lock event message to display
	private: char p_lock_str [ Event_STR_VARIABLE_LEN ]; // lock event text variable (ex: riddle answer)

   s_Event_passfail p_pass;
   s_Event_passfail p_fail;

   private: static bool p_demo; // set to true if the demo is active. Makes events behave differently during the editor.

	/*private: int p_pass_event; // type of event if the lock is passed
	private: int p_pass_var1; // 1st variable of the pass event
	private: int p_pass_var2; // 2nd variable of the pass event
	private: int p_pass_var3; // 3rd variable of the pass event
	private: int p_pass_var4; // 4th variable of the pass event
	private: char p_pass_msg [ Event_MSG_VARIABLE_LEN ]; // pass event message display
	private: char p_pass_str [ Event_STR_VARIABLE_LEN ]; // pass event text variable
	private: int p_fail_event; // type of event if the lock is failed
	private: int p_fail_var1; // 1st variable of the fail event
	private: int p_fail_var2; // 2nd variable of the fail event
	private: int p_fail_var3; // 3rd variable of the fail event
	private: int p_fail_var4; // 4th variable of the fail event
	private: char p_fail_msg [ Event_MSG_VARIABLE_LEN ]; // fail event message display
	private: char p_fail_str [ Event_STR_VARIABLE_LEN ]; // fail event text variable*/

   // ----- Constructor -----

   public: Event ( void );
   public: ~Event ( void );

   // ----- Properties method -----

   // NOte: I only defined procedures that could be used by the editor.
   // Since most information will be edited in the data base
   // there is no need to define all access points.

   public: void trigger ( int value );
   public: int trigger ( void );
	public: void lock_event ( int value );
	public: int lock_event ( void ) { return (p_lock_event); }
	public: void lock_key ( int value );
	public: void pass_event ( int value );
	public: int pass_event ( void ) { return (p_pass.event); }
	public: void fail_event ( int value );
	public: int fail_event ( void ) { return (p_fail.event); }

   // ----- Methods -----

   public: bool start ( bool demo = false );

   // ----- private Methods -----

   private: bool start_lock_event ( void );
   private: bool start_passfail_event ( bool eventpassed );
   //private: void start_fail_event ( void );

   private: int get_keyitem_ID ( int item_key );

   //private: void test [10] (void);

   // lock events (return pass or fail)
   private: bool le_autopass (void);
   private: bool le_question (void);
   private: bool le_encounterlock (void);
   private: bool le_hiddenlock (void);
   private: bool le_riddle (void);
   private: bool le_combat (void);
   private: bool le_NPC (void);

   // pass/fail event (return maze interrupt yes no)
   private: bool pfe_endgame ( s_Event_passfail param );
   private: bool pfe_move (s_Event_passfail param);
   private: bool pfe_teleport (s_Event_passfail param);
   private: bool pfe_gainitem (s_Event_passfail param);
   private: bool pfe_tradeitem (s_Event_passfail param);
   private: bool pfe_trap (s_Event_passfail param);
   private: bool pfe_combat (s_Event_passfail param);
   private: bool pfe_exitmaze (s_Event_passfail param);
   private: bool pfe_rotate (s_Event_passfail param);
   private: bool pfe_message (s_Event_passfail param);
   private: bool pfe_storytext (s_Event_passfail param);
   private: bool pfe_NPC (s_Event_passfail param);
   private: bool pfe_elevator (s_Event_passfail param);

   // ----- Virtual Methods -----

   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

};

/*------------------------------------------------------------------------*/
/*-                       Global Variables                               -*/
/*------------------------------------------------------------------------*/

// String of text

extern const char STR_EVE_TRIGGER [][ Event_NB_EVENT];
extern const char STR_EVE_LOCK [][ Event_NB_EVENT];
extern const char STR_EVE_PASSFAIL [] [ Event_NB_EVENT];

// ID of th icon to use to display in the maze
// Pass fail Icon is used by default. If empty, use lock icon
extern int EDICON_EVENT_LOCK [Event_NB_EVENT];
extern int EDICON_EVENT_PASSFAIL [Event_NB_EVENT];

#endif // EVENT_H_INCLUDED
