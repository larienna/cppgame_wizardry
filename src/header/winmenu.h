/***************************************************************************/
/*                                                                         */
/*                         W I N M E N U . H                               */
/*                         Class Definitnon                                */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinMenu                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting date : November 11th, 2002                                 */
/*                                                                         */
/*          Class who inherint from window to manage a window with a menu  */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                       class definition                                -*/
/*-------------------------------------------------------------------------*/

class WinMenu : public Window
{
   // --- Property Methods ---

   private: Menu *p_menu; // menu to show in the window
   private: bool p_no_cancel; // no cancel command enabled in menu
   private: bool p_reset_cursor; // reset cursor at start for each show
   private: int p_cursor; // last cursor position

   // --- Constructor & Destructor ---

   public: WinMenu ( Menu &mnu, short x_pos, short y_pos,
                        bool no_cancel = false, bool reset_cursor = false, bool translucent = false );
//   public: ~WinMenu ( void );

   // --- Virtual Methods ---

   public: virtual void preshow ( void ); // this funtion does nothing
   public: virtual void refresh ( void );
   public: virtual short show ( void );
   public: virtual int type ( void );

};
