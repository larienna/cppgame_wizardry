//--------------------------------------------------------------------
//
//                         T E X T . H
//
//     Content: Text class
//     Programmer: Eric Pietrocupo
//     Starting Date: february 25th, 2014
//
//   This class is designed to manage long string of text for the story
//   and certains events in the game
//
//--------------------------------------------------------------------

#ifndef TEXT_H_INCLUDED
#define TEXT_H_INCLUDED

//-----------------------------------------------------------------------
//                       Constant Definition
//-----------------------------------------------------------------------

#define Text_STR_LEN       1025 //If bigger will be trunc


//-----------------------------------------------------------------------
//-                      Class Definition
//-----------------------------------------------------------------------

class Text : public SQLobject
{
   // --- Properties ---

   private: int p_pictureid; // ID of the picture used for story text
   private: int p_rank; // Identification of story text and their order.
   private: char p_content [ Text_STR_LEN ]; // original text

   // internal management of formating
   private: char p_formatted [ Text_STR_LEN ]; // formatted text
   private: int p_nb_lines;
   private: char* p_strptr; // used by strtok for line read

   // --- constructor and destructor ---

   public: Text ( void );
   public: Text ( const Text &obj);
   public: ~Text ( void );

   // --- Property Methods ---

   public: int pictureid ( void ) { return (p_pictureid); }
   public: void pictureid ( int value ) { p_pictureid = value; }
   public: int rank ( void ) { return (p_rank); }
   public: void rank ( int value ) { p_rank = value; }
   public: char *content ( void ) { return ( p_content ); }
   public: void content ( char *value ) { strncpy ( p_content, value, Text_STR_LEN); }

   // --- Methods ---

   public: void format ( int maxwidth, FONT *fnt);
   public: char* readline_start ( void );
   public: char* readline_next ( void );

   // --- Operators ---

   public: Text& operator=( const Text &obj );

   // --- virtual methods ---

   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

};


#endif // TEXT_H_INCLUDED
