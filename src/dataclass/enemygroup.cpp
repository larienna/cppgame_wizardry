//----------------------------------------------------------------------
//
//                 E N E M Y G R O U P . C P P
//
//     Class: EnnemyGroup
//     Programmer: Eric Pietrocupo
//     Start Date: February 2014
//
//     Manage groups of ennemies to encounter in the maze
//
//-----------------------------------------------------------------------


#ifndef ENEMYGROUP_CPP_INCLUDED
#define ENEMYGROUP_CPP_INCLUDED

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>

//--------------------------------------------------------------------------
//                        Constructors and Destructors
//--------------------------------------------------------------------------

EnemyGroup::EnemyGroup ( void )
{
   int i;

   p_floor = -1;
   p_area = -1;
   p_repeat = 1;
   p_bonus = 0;
   p_bonus_target = 0;
   p_probability = 0;

   for ( i = 0 ; i < EnemyGroup_NB_ENEMY ; i++ )
      p_enemy [ i ] = 0;

   strcpy ( p_tablename, "EnemyGroup" );

}

EnemyGroup::~EnemyGroup ( void )
{

}

//---------------------------------------------------------------------------
//                        Methods
//--------------------------------------------------------------------------

//---------------------------------------------------------------------------
//                        Virtual Methods
//--------------------------------------------------------------------------

   // object relational virtual methods
void EnemyGroup::sql_to_obj (void)
{

   p_primary_key = SQLcolumn_int (0);
   p_floor = SQLcolumn_int(1);
   p_area = SQLcolumn_int(2);
   p_repeat = SQLcolumn_int(3);
   p_bonus = SQLcolumn_int(4);
   p_bonus_target = SQLcolumn_int(5);
   p_probability = SQLcolumn_int(6);
   p_enemy [ 0 ] = SQLcolumn_int(7);
   p_enemy [ 1 ] = SQLcolumn_int(8);
   p_enemy [ 2 ] = SQLcolumn_int(9);
   p_enemy [ 3 ] = SQLcolumn_int(10);
   p_enemy [ 4 ] = SQLcolumn_int(11);
   p_enemy [ 5 ] = SQLcolumn_int(12);
   p_enemy [ 6 ] = SQLcolumn_int(13);
   p_enemy [ 7 ] = SQLcolumn_int(14);

}

void EnemyGroup::template_sql_to_obj (void)
{



   // will not be used
}

void EnemyGroup::obj_to_sqlupdate (void)
{

   sprintf (p_querystr, "floor=%d, area=%d, repeat=%d, bonus=%d, bonus_target=%d, probability=%d, enemy1=%d, enemy2=%d, enemy3=%d, enemy4=%d, enemy5=%d, enemy6=%d, enemy7=%d, enemy8=%d",
            p_floor,
            p_area,
            p_repeat,
            p_bonus,
            p_bonus_target,
            p_probability,
            p_enemy [ 0 ],
            p_enemy [ 1 ],
            p_enemy [ 2 ],
            p_enemy [ 3 ],
            p_enemy [ 4 ],
            p_enemy [ 5 ],
            p_enemy [ 6 ],
            p_enemy [ 7 ] );

}

void EnemyGroup::obj_to_sqlinsert (void)
{
   sprintf (p_querystr, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
            p_floor,
            p_area,
            p_repeat,
            p_bonus,
            p_bonus_target,
            p_probability,
            p_enemy [ 0 ],
            p_enemy [ 1 ],
            p_enemy [ 2 ],
            p_enemy [ 3 ],
            p_enemy [ 4 ],
            p_enemy [ 5 ],
            p_enemy [ 6 ],
            p_enemy [ 7 ] );

}

//---------------------------------------------------------------------------
//                        Global Variables
//--------------------------------------------------------------------------

int EnemyGroup_PROBABILITY [ EnemyGroup_NB_PROBABILITY ] [ EnemyGroup_NB_ENEMY ] =
{
  // EnemyGroup_PROB_RANDOM
  { 100,  50,  50,  50,  50,  50,  50,  50 },
  // EnemyGroup_PROB_FIXED
  { 100, 100, 100, 100, 100, 100, 100, 100 },
  // EnemyGroup_PROB_DECREMENTAL
  { 100, 75, 50, 25, 75, 25, 75, 25 },
  // EnemyGroup_PROB_LOW
  { 100, 25, 25, 25, 25, 25, 25, 25 },
  // EnemyGroup_PROB_HIGH
  { 100, 75, 75, 75, 75, 75, 75, 75 }
};



#endif // ENEMYGROUP_CPP_INCLUDED
