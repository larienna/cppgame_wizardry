/**************************************************************************/
/*                                                                        */
/*                           C C L A S S . C P P                          */
/*                           Class source code                            */
/*                                                                        */
/*     Content : Class CClass source code                                 */
/*     Programmer : Eric Pietrocupo                                       */
/*     Starting Date : june 14th 2004                                     */
/*     License : GNU General Public LIcense                               */
/*                                                                        */
/**************************************************************************/

// Include Groups
#include <grpsys.h> //<tmpinc>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>


//#include <cclass.h>


/*
#include <stdio.h>
//#include <math.h>
//#include <stdlib.h>
//#include <time.h>
#include <allegro.h>

//#include <random.h>
#include <string.h>
//#include <datafile.h>
//#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>








#include <listwiz.h>

#include <cclass.h>
#include <opponent.h>
#include <charactr.h>
//#include <ennemy.h>

#include <party.h>

//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winmenu.h>
#include <winempty.h>
#include <wintitle.h>
#include <windata.h>
//#include <wdatproc.h>
#include <winmessa.h>
#include <winquest.h>
#include <wininput.h>
*/



s_SQLfield CClass::p_SQLfield [ NB_FIELD ] =
{

{ "name",	      TEXT     ,CClass_NAME_STRLEN              ,"Name",                true, NULL},
{ "initial",      TEXT     ,CClass_INITIAL_STRLEN           ,"Initial",             false, NULL },
{ "ad",		      INTEGER  ,0                               ,"Active Defense",      true, NULL },
{ "md",		      INTEGER  ,0                               ,"Magic Defense",       true, NULL },
{ "hpdice",	      INTEGER  ,0                               ,"HP Dice",             true, NULL },
{ "mpdice",	      INTEGER  ,0                               ,"MP Dice",             true, NULL },
{ "ps",		      INTEGER  ,0                               ,"Physical Saves",      true, NULL },
{ "ms",		      INTEGER  ,0                               ,"Mental Saves",        true, NULL },
{ "attdiv",		   INTEGER  ,0                               ,"Extra attack divider",true, NULL },
{ "powdiv",		   INTEGER  ,0                               ,"Extra power divider", true, NULL },
{ "melee_cs",		INTEGER  ,0                               ,"Melee Combat Skill",  true, NULL },
{ "range_cs",		INTEGER  ,0                               ,"Range Combat",        true, NULL },
{ "profiency",		STRFIELD ,EnhancedSQLobject_STRFIELD_LEN  ,"Profiency",           true, STRFLD_PROFIENCY },
{ "magic_school",	STRFIELD ,EnhancedSQLobject_STRFIELD_LEN  ,"Magic School",        true, STRFLD_MAGIC_SCHOOL },
{ "pictureID",		INTEGER  ,0                               ,"Picture",             false, NULL },
{ "resistance",	STRFIELD ,EnhancedSQLobject_STRFIELD_LEN  ,"Resistance",          true, STRFLD_ELEMENTAL_PROPERTY },
{ "weakness",		STRFIELD ,EnhancedSQLobject_STRFIELD_LEN  ,"Weakness",            true, STRFLD_ELEMENTAL_PROPERTY },
{ "passive_skill",INTEGER  ,0                               ,"Passive Skill",       false, NULL },
{ "active_skill",	INTEGER  ,0                               ,"Active Skill",        false, NULL },

};

/*------------------------------------------------------------------------*/
/*-                         Constructor & Destructor                     -*/
/*------------------------------------------------------------------------*/


CClass::CClass ( void )
{
   //int i;


   strcpy (p_tablename, "class");
   //strcpy ( p_name, "" );
   //strcpy ( p_initial, "" );
   //strcpy

   p_SQLdata [ 0 ] . str = p_name;
   p_SQLdata [ 1 ] . str = p_initial;
   p_SQLdata [ 12 ] . str = p_profiency;
   p_SQLdata [ 13 ] . str = p_magic_school;
   p_SQLdata [ 15 ] . str = p_resistance;
   p_SQLdata [ 16 ] . str = p_weakness;

   p_SQLfield_ptr = p_SQLfield;
   p_SQLdata_ptr = p_SQLdata;
   p_nb_field = NB_FIELD;

   set_all_int ( 0 );
   set_all_str  ( "" );

   /*p_profiency = 0;
   //p_weapon_profiency = 0;
   //p_armor_profiency = 0;
   //p_shield_profiency = 0;
   p_AD = 0;
   p_MD = 0;

   p_HPdice = 0; // hit die
   p_MPdice = 0; // mana die
   p_PS = 0; // Base Physical Save
   p_MS = 0; // Base Mental Save
   p_INIT = 0; // base initiative
   p_WSP = 0; // Weapon skill progress
   p_MSP = 0; // Magic skill progress
   p_MDMGY = 0; // Magic damage die
   p_PCS = 0; // Physical combat skill
   p_MCS = 0; // Mental combat skill
   p_magic_school = 0;


   //for ( i = 0 ; i < CClass_NB_SKILL ; i++ )
   //   p_skill [ i ] = 0;

   p_pictureID = 0;
   p_resistance = 0;
   p_weakness = 0;*/


}


CClass::~CClass ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                            Property Methods                           -*/
/*-------------------------------------------------------------------------*/

const char* CClass::name ( void )
{
   return ( p_name );
}

void CClass::name ( const char* value )
{
   strncpy ( p_name, value, CClass_NAME_STRLEN );
}

const char* CClass::initial ( void )
{
   return ( p_initial );
}

void CClass::initial ( const char* value )
{
   strncpy ( p_initial, value, CClass_INITIAL_STRLEN );
}

/*unsigned char CClass::aligment_allowed ( void )
{
   return ( p_aligment_allowed );
}

void CClass::aligment_allowed ( int value )
{
   p_aligment_allowed = value;
}

unsigned short CClass::elm_resist ( void )
{
   return ( p_elm_resist );
}

void CClass::elm_resist ( unsigned short value )
{
   p_elm_resist = value;
}

unsigned short CClass::elm_effect ( void )
{
   return ( p_elm_effect );
}

void CClass::elm_effect ( unsigned short value )
{
   p_elm_effect = value;
}

unsigned short CClass::hlt_resist ( void )
{
   return ( p_hlt_resist );
}

void CClass::hlt_resist ( unsigned short value )
{
   p_hlt_resist = value;
}


unsigned short CClass::hlt_effect ( void )
{
   return ( p_hlt_effect );
}

void CClass::hlt_effect ( unsigned short value )
{
   p_hlt_effect = value;
}

unsigned short CClass::magikproperty ( void )
{
   return ( p_magikproperty );
}

void CClass::magikproperty ( unsigned short value )
{
   p_magikproperty = value;
}


unsigned char CClass::spell_element ( void )
{
   return ( p_spell_element );
}

void CClass::spell_element ( unsigned char value )
{
   p_spell_element = value;
}

int CClass::spell_amplification ( void )
{
   return ( p_spell_amplification );
}

void CClass::spell_amplification ( int value )
{
   p_spell_amplification = value;
}*/

/*int CClass::spell_progress ( void )
{
   return ( p_spell_progress );
}

void CClass::spell_progress ( int value )
{
   p_spell_progress = value;
}*/

/*int CClass::skill  ( int index )
{
   return ( p_skill [ index ] );
}

void CClass::skill  ( int index, int value )
{
   p_skill [ index ] = value;
}

bool CClass::have_skill ( int value )
{
   bool retval = false;
   int i;

   for ( i = 0 ; i < CClass_NB_SKILL ; i++ )
   {
      if ( p_skill [ i ] == value )
         retval = true;
   }

   return ( retval );
}*/

/*unsigned char CClass::wprof_range ( void )
{
   return ( p_wprof_range );
}

void CClass::wprof_range ( unsigned char value )
{
   p_wprof_range = value;
}

unsigned char CClass::wprof_attrib ( void )
{
   return ( p_wprof_attrib );
}

void CClass::wprof_attrib ( unsigned char value )
{
   p_wprof_attrib = value;
}

unsigned char CClass::wprof_nb_hand ( void )
{
   return ( p_wprof_nb_hand );
}

void CClass::wprof_nb_hand ( unsigned char value )
{
   p_wprof_nb_hand = value;
} */

 int CClass::profiency ( void )
{
   return ( get_int ( CClass_FIELD_PROFIENCY ) );
}

void CClass::profiency (  int value )
{
   set_int ( CClass_FIELD_PROFIENCY ,  value);
}

/*
int CClass::armor_profiency ( void )
{
   return ( p_armor_profiency );
}

void CClass::armor_profiency ( int value )
{
   p_armor_profiency = value;
}

int CClass::shield_profiency ( void )
{
   return ( p_shield_profiency );
}

void CClass::shield_profiency ( int value )
{
   p_shield_profiency = value;
}
*/
/*int CClass::DMGdiv ( void )
{
   return ( p_DMGdiv );
}

void CClass::DMGdiv ( int value )
{
   value = p_DMGdiv ;
}
int CClass::MDMGdiv ( void )
{
   return ( p_MDMGdiv );
}

void CClass::MDMGdiv ( int value )
{
   value = p_MDMGdiv;
}
int CClass::mulhitdiv ( void )
{
   return ( p_mulhitdiv );
}

void CClass::mulhitdiv ( int value )
{
   value = p_mulhitdiv;
}*/

int CClass::AD ( void )
{
   return ( get_int ( CClass_FIELD_AD ) );
}

/*void CClass::AD ( int value )
{
   set_int ( CClass_FIELD_AD, value );
}*/

int CClass::MD ( void )
{
   return ( get_int ( CClass_FIELD_MD ) );
}

/*void CClass::MD ( int value )
{
   set_int ( CClass_FIELD_MD, value );
}*/

int CClass::HPdice ( void )
{
   return ( get_int ( CClass_FIELD_HPDICE ) );
}

/*void CClass::HPdice ( int value )
{
   p_HPdice = value;
}*/

int CClass::MPdice ( void )
{
   return ( get_int ( CClass_FIELD_MPDICE ) );
}

/*void CClass::MPdice ( int value )
{
   p_MPdice = value;
}*/

int CClass::PS ( void )
{
   return ( get_int ( CClass_FIELD_PS ) );
}

/*void CClass::PS ( int value )
{
   p_PS = value;
}*/

int CClass::MS ( void )
{
   return ( get_int ( CClass_FIELD_MS ) );
}

/*void CClass::MS ( int value )
{
   p_MS = value;
}*/

/*int CClass::INIT ( void )
{
   return ( get_int ( CClass_FIELD_ ) );
}*/

/*void CClass::INIT ( int value )
{
   p_INIT = value;
}*/

int CClass::attdiv ( void )
{
   return ( get_int ( CClass_FIELD_ATTDIV ) );
}

/*void CClass::WSP ( int value )
{
   p_WSP = value;
}*/

int CClass::powdiv ( void )
{
   return ( get_int ( CClass_FIELD_POWDIV ) );
}

/*void CClass::MSP ( int value )
{
   p_MSP = value;
}*/

/*int CClass::MDMGY ( void )
{
   return ( p_MDMGY );
}*/

/*void CClass::MDMGY ( int value )
{
   p_MDMGY = value;
}*/

int CClass::melee_CS ( void )
{
   return ( get_int ( CClass_FIELD_MELEECS ) );
}

/*void CClass::PCS ( int value )
{
   p_PCS = value;
}*/

int CClass::range_CS ( void )
{
   return ( get_int ( CClass_FIELD_RANGECS ) );
}

/*void CClass::MCS ( int value )
{
   p_MCS = value;
}*/


int CClass::magic_school ( void )
{
   return ( get_int ( CClass_FIELD_MAGIC_SCHOOL ) );
}

/*void CClass::magic_school ( int value )
{
   p_magic_school = value;
}*/

int CClass::pictureID ( void )
{
   return ( get_int ( CClass_FIELD_PICTUREID ) );
}

/*void CClass::pictureID ( int value )
{
   p_pictureID = value;
}*/

int CClass::resistance ( void )
{
   return ( get_int ( CClass_FIELD_RESISTANCE ) );
}

/*void CClass::resistance ( unsigned int value )
{
   p_resistance = value;
}*/

int CClass::weakness ( void )
{
   return ( get_int ( CClass_FIELD_WEAKNESS ) );
}

/*void CClass::weakness ( unsigned int value )
{
   p_weakness = value;
}*/




/*int CClass::HPdiv ( void )
{
   return ( p_HPdiv );
}

void CClass::HPdiv ( int value )
{
   value = p_HPdiv ;
}
int CClass::MPdiv ( void )
{
   return ( p_MPdiv );
}

void CClass::MPdiv ( int value )
{
   value = p_MPdiv ;
}
*/

/*-------------------------------------------------------------------------*/
/*-                          Virtual Methods                              -*/
/*-------------------------------------------------------------------------*/

/*void CClass::sql_to_obj (void)
{


   p_primary_key = SQLcolumn_int (0);
   strncpy ( p_name, SQLcolumn_text(1), CClass_NAME_STRLEN);
   strncpy ( p_initial, SQLcolumn_text(2), CClass_INITIAL_STRLEN);
   p_AD = SQLcolumn_int(3);
   p_MD = SQLcolumn_int(4);
   p_HPdice = SQLcolumn_int(5);
   p_MPdice = SQLcolumn_int(6);
   p_PS = SQLcolumn_int(7);
   p_MS = SQLcolumn_int(8);
   p_INIT = SQLcolumn_int(9);
   p_WSP = SQLcolumn_int(10);
   p_MSP = SQLcolumn_int(11);
   p_MDMGY = SQLcolumn_int(12);
   p_PCS = SQLcolumn_int(13);
   p_MCS = SQLcolumn_int(14);
   p_profiency = SQLcolumn_int(15);
   //p_weapon_profiency = SQLcolumn_int(15);
   //p_armor_profiency = SQLcolumn_int(16);
   //p_shield_profiency = SQLcolumn_int(17);
   p_magic_school = SQLcolumn_int(16);
   //p_skill [ 0 ] = SQLcolumn_int(17);
   //p_skill [ 1 ] = SQLcolumn_int(18);
   p_pictureID = SQLcolumn_int(17);
   p_resistance = SQLcolumn_int(18);
   p_weakness = SQLcolumn_int(19);

}

void CClass::template_sql_to_obj (void)
{

   // will not be used
}

void CClass::obj_to_sqlupdate (void)
{

   sprintf (p_querystr, "name='%s', initial='%s', ad=%d, md=%d, hpdice=%d, mpdice=%d, ps=%d, ms=%d, init=%d, wsp=%d, msp=%d, mdmgy=%d, pcs=%d, mcs=%d, profiency=%d, magic_school=%d, pictureID=%d, resistance=%d, weakness=%d",
            p_name,
            p_initial,
            p_AD,
            p_MD,
            p_HPdice,
            p_MPdice,
            p_PS,
            p_MS,
            p_INIT,
            p_WSP,
            p_MSP,
            p_MDMGY,
            p_PCS,
            p_MCS,
            p_profiency,
            //p_weapon_profiency,
            //p_armor_profiency,
            //p_shield_profiency,
            p_magic_school,
            //p_skill [ 0 ],
            //p_skill [ 1 ],
            p_pictureID,
            p_resistance,
            p_weakness );

}

void CClass::obj_to_sqlinsert (void)
{

   sprintf (p_querystr, "'%s', '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, ",
            p_name,
            p_initial,
            p_AD,
            p_MD,
            p_HPdice,
            p_MPdice,
            p_PS,
            p_MS,
            p_INIT,
            p_WSP,
            p_MSP,
            p_MDMGY,
            p_PCS,
            p_MCS,
            p_profiency,
            //p_weapon_profiency,
            //p_armor_profiency,
            //p_shield_profiency,
            p_magic_school,
            //p_skill [ 0 ],
            //p_skill [ 1 ],
            p_pictureID,
            p_resistance,
            p_weakness );
}
*/

/*
void CClass::objdat_to_strdat ( void *dataptr )
{
   int i;

   dbs_CClass &tmpdat = *(static_cast<dbs_CClass*> ( dataptr ));

   strncpy ( tmpdat.name , p_name , 21 );
   strncpy ( tmpdat.initial , p_initial , 4 );
   tmpdat.aligment_allowed = p_aligment_allowed;
   tmpdat.elm_resist = p_elm_resist;
   tmpdat.elm_effect = p_elm_effect;
   tmpdat.hlt_resist = p_hlt_resist;
   tmpdat.hlt_effect = p_hlt_effect;
   tmpdat.magikproperty = p_magikproperty;
   tmpdat.spell_element = p_spell_element;
   tmpdat.spell_amplification = p_spell_amplification;
   tmpdat.spell_progress = p_spell_progress;
   tmpdat.weapon_profiency = p_weapon_profiency;
   tmpdat.armor_profiency = p_armor_profiency;
   tmpdat.shield_profiency = p_shield_profiency;
   tmpdat.DMGdiv = p_DMGdiv;
   tmpdat.MDMGdiv = p_MDMGdiv;
   tmpdat.mulhitdiv = p_mulhitdiv;
   tmpdat.baseAD = p_baseAD;
   tmpdat.baseMAD = p_baseMAD;
   tmpdat.HPdiv = p_HPdiv;
   tmpdat.MPdiv = p_MPdiv;

   for ( i = 0 ; i < 3 ; i++ )
      tmpdat.skill [ i ] = p_skill [ i ];

}

void CClass::strdat_to_objdat ( void *dataptr )
{
   int i;
   dbs_CClass &tmpdat = *(static_cast<dbs_CClass*> ( dataptr ));

   strcpy ( p_name, tmpdat.name );
   strcpy ( p_initial, tmpdat.initial );
   p_aligment_allowed = tmpdat.aligment_allowed;
   p_elm_resist = tmpdat.elm_resist;
   p_elm_effect = tmpdat.elm_effect;
   p_hlt_resist = tmpdat.hlt_resist;
   p_hlt_effect = tmpdat.hlt_effect;
   p_magikproperty = tmpdat.magikproperty;
   p_spell_element = tmpdat.spell_element;
   p_spell_amplification = tmpdat.spell_amplification;
   p_spell_progress = tmpdat.spell_progress;
   p_weapon_profiency = tmpdat.weapon_profiency;
   p_armor_profiency = tmpdat.armor_profiency;
   p_shield_profiency = tmpdat.shield_profiency;
   p_DMGdiv = tmpdat.DMGdiv;
   p_MDMGdiv = tmpdat.MDMGdiv;
   p_mulhitdiv = tmpdat.mulhitdiv;
   p_baseAD = tmpdat.baseAD;
   p_baseMAD = tmpdat.baseMAD;
   p_HPdiv = tmpdat.HPdiv;
   p_MPdiv = tmpdat.MPdiv;


   for ( i = 0 ; i < 3 ; i++ )
      p_skill [ i ] = tmpdat.skill [ i ];


}

void CClass::child_DBremove ( void )
{

}

*/

/*-------------------------------------------------------------------------*/
/*-                           Global Variables                            -*/
/*-------------------------------------------------------------------------*/


/*const char STR_CLS_MAGIC [] [ 11 ] =
{
   {"None"},
   {"Arcane"},
   {"Divine"},
   {"Alchemy"},
   {"Psionic"}
};*/


s_EnhancedSQLobject_strfield STRFLD_MAGIC_SCHOOL [ EnhancedSQLobject_HASH_SIZE ] =
{
   { "Lar", "Arcane" },
   { "Ldi", "Divine" },
   { "Lal", "Alchemy" },
   { "Lps", "Psionic" },

   { "Har", "High Arcane" },
   { "Hdi", "High Divine" },
   { "Hal", "High Alchemy" },
   { "Hps", "High Psionic" },

   { "Oth", "Other" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },

   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },

   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" }
};

