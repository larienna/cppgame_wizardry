//----------------------------------------------------------------
//
//               M O N S T E R C A T E G O R Y . C P P
//
//   Content: Class Unidentified
//   Programmer: Eric Pietrocupo
//   Starting Date: February 16th, 2014
//
//   Class that manage unidentified monster groups
//
//-----------------------------------------------------------------

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>

//----------------------------------------------------------------------
// Constructors
//----------------------------------------------------------------------

MonsterCategory::MonsterCategory ( void )
{
   strcpy ( p_tablename, "monster_category" );
}

MonsterCategory::~MonsterCategory ( void)
{

}


//----------------------------------------------------------------------
// Virtural Methods
//----------------------------------------------------------------------

void MonsterCategory::sql_to_obj (void)
{
   p_primary_key = SQLcolumn_int (0);
   strncpy ( p_name, SQLcolumn_text(1), Unidentified_NAME_LEN);
   p_pictureID = SQLcolumn_int(2);
}

void MonsterCategory::template_sql_to_obj ( void)
{
   // this will not be used
}

void MonsterCategory::obj_to_sqlupdate (void)
{
   sprintf (p_querystr, "name='%s', pictureID=%d",
            p_name,
            p_pictureID);
}

void MonsterCategory::obj_to_sqlinsert (void)
{
   sprintf (p_querystr, "'%s', %d",
            p_name,
            p_pictureID);
}
