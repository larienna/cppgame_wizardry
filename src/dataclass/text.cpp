//-------------------------------------------------------------------------
//
//                      T E X T . C P P
//
//     Content: Text class
//     Programmer: Eric Pietrocupo
//     Starting Date: february 25th, 2014
//
//   This class is designed to manage long string of text for the story
//   and certains events in the game
//
//--------------------------------------------------------------------

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>

#include <text.h>

//--------------------------------------------------------------------
//                     Static variables
//--------------------------------------------------------------------

//static char* p_strptr = NULL; // used by strtok for line read

//--------------------------------------------------------------------
//                     Constructor and Destructors
//--------------------------------------------------------------------

Text::Text ( void )
{
   p_pictureid = 0;
   p_rank = 0;
   strcpy ( p_content, "");
   strcpy ( p_formatted, "");
   p_nb_lines = 0;
   p_strptr = NULL;

   strcpy ( p_tablename, "text");
}

/*Text::Text ( const Text &obj)
{
   //p_primary_key = obj.p_primary_key;
   p_pictureid = obj.p_pictureid;
   p_rank = obj.p_rank;
   strncpy ( p_content, obj.p_content, Text_STR_LEN );
   strncpy ( p_formatted, obj.p_formatted, Text_STR_LEN );
   p_nb_lines = obj.p_nb_lines;
   p_strptr = NULL;

   printf ("Text:Copy: Passed into the procedure\n");
}*/

Text::~Text ( void )
{
   //printf ("Unloading and Text Object\n");
}

//--------------------------------------------------------------------
//                     Methods
//--------------------------------------------------------------------


void Text::format ( int maxwidth, FONT *fnt)
{
   char* tmpstrptr; // pointer for strtok
   int linewidth=0; // current width of the line
   int tmplen = 0; // temp len of the string.
   int spacelen = text_length ( fnt, " ");


   tmpstrptr = strtok ( p_content, " " );
   strcpy ( p_formatted, "" );

   while ( tmpstrptr != NULL)
   {
      tmplen = text_length ( fnt, tmpstrptr );

      if ( (linewidth + tmplen + spacelen ) <= maxwidth )
      {
         if ( linewidth > 0 )
         {
            strcat ( p_formatted, " ");
            tmplen += spacelen;
         }

         linewidth += tmplen;
         //printf ("linewidth=%d\n", linewidth);
      }
      else
      {
         strcat ( p_formatted, "\n");
         linewidth = tmplen;
      }


      strcat ( p_formatted, tmpstrptr );
      tmpstrptr = strtok ( NULL, " ");
   }

/* strtok example
#include <stdio.h>
#include <string.h>

int main ()
{
  char str[] ="- This, a sample string.";
  char * pch;
  printf ("Splitting string \"%s\" into tokens:\n",str);
  pch = strtok (str," ,.-");
  while (pch != NULL)
  {
    printf ("%s\n",pch);
    pch = strtok (NULL, " ,.-");
  }
  return 0;
}*/

}

char* Text::readline_start ( void )
{
   return ( strtok ( p_formatted, "\n") );
}

char* Text::readline_next ( void )
{
   return ( strtok ( NULL, "\n") );
}

//--------------------------------------------------------------------
//                     Operators
//--------------------------------------------------------------------


Text& Text::operator=( const Text &obj )
{
   //printf ("I entered the assigment operator\n");
   if ( this != &obj)
   {
      //printf ("I entered the assigment operator2\n");
      p_pictureid = obj.p_pictureid;
      p_rank = obj.p_rank;
      strncpy ( p_content, obj.p_content, Text_STR_LEN );
      strncpy ( p_formatted, obj.p_formatted, Text_STR_LEN );
      p_nb_lines = obj.p_nb_lines;
      p_strptr = NULL;

      //printf ("Text:Copy: Passed into the procedure\n");



   }

   return (*this);
}

//--------------------------------------------------------------------
//                     virtual methods
//--------------------------------------------------------------------

void Text::sql_to_obj (void)
{
   p_primary_key = SQLcolumn_int (0);
   p_pictureid = SQLcolumn_int(1);
   p_rank = SQLcolumn_int(2);
   strncpy ( p_content, SQLcolumn_text(3), Text_STR_LEN );
}

void Text::template_sql_to_obj ( void)
{
   /// this will not be used
}

void Text::obj_to_sqlupdate (void)
{
   sprintf (p_querystr, "pictureid=%d, rank=%d, content='%s'",
            p_pictureid,
            p_rank,
            p_content );

}

void Text::obj_to_sqlinsert (void)
{
   sprintf (p_querystr, "%d, %d, '%s'",
            p_pictureid,
            p_rank,
            p_content );

}


