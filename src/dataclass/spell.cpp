
/*************************************************************************/
/*                                                                       */
/*                              S P E L L . C P P                        */
/*                             Class Definition                          */
/*                                                                       */
/*     Content: Spell Class                                              */
/*     Programmer: Eric Pietrocupo                                       */
/*     Creation Date: November 9th, 2014                                 */
/*                                                                       */
/*          This class is used to manage spells castable by characters   */
/*     and monsters.                                                     */
/*                                                                       */
/*************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>

#include <grpinterface.h>
#include <grpengine.h>


s_SQLfield Spell::p_SQLfield [ NB_FIELD ] =
{
   { "word",	TEXT, Spell_WORD_STRLEN, "Word             ", true, NULL},
	{ "name",	TEXT, Spell_NAME_STRLEN, "Name             ", true, NULL},
	{ "level",	INTEGER, 0,              "Other Class Level", true, NULL},
	{ "mp_cost",	INTEGER, 0,           "MP Cost          ", true, NULL},
	{ "target",	INTEGER, 0,              "Target Type      ", false, NULL},
	{ "castable",	INTEGER, 0,           "Castable when    ", false, NULL},
	{ "item",	INTEGER, 0,              "Itemizable", false, NULL},
	{ "school",	INTEGER	, 0,            "Magic School     ", false, NULL},
	{ "element",	STRFIELD, Spell_ELEMENT_STRLEN, "Element          ", true, STRFLD_ELEMENTAL_PROPERTY },
	{ "defense",	STRFIELD, Spell_PROPERTY_STRLEN, "Defense          ", true, STRFLD_DEFENSE },
	{ "description", TEXT, Spell_DESCRIPTION_STRLEN, "Description         ", false, NULL},
	{ "effect_id",	INTEGER	, 0,            "Effect Type", false, NULL},
	{ "param1",	INTEGER, 0,           "Paramter 1", false , NULL},
	{ "param2",	INTEGER, 0,     "Parameter 2", false, NULL},
	{ "param3",	INTEGER, 0,              "Parameter 3", false, NULL},
	{ "highlevel", INTEGER, 0,           "Magic User Level ", true, NULL }


};


/*----------------------------------------------------------------------*/
/*-                             Constructor and destructor             -*/
/*----------------------------------------------------------------------*/


Spell::Spell ( void )
{
   strcpy ( p_tablename, "spell");

   p_SQLdata [ 0 ] . str = p_word;
   p_SQLdata [ 1 ] . str = p_name;
   p_SQLdata [ 8 ] . str = p_element;
   p_SQLdata [ 9 ] . str = p_property;
   p_SQLdata [ 10 ] . str = p_description;

   // for enhanced SQL object, link right data.
   p_SQLfield_ptr = p_SQLfield;
   p_SQLdata_ptr = p_SQLdata;
   p_nb_field = NB_FIELD;

   set_all_int ( 0 );
   set_all_str  ( "" );

}

Spell::~Spell ( void )
{

}

/*----------------------------------------------------------------------*/
/*-                           Methods                                  -*/
/*----------------------------------------------------------------------*/

int Spell::cast ( Action &action, Opponent *actor, Opponent *target )
{
   if ( p_primary_key >= 300 )
   {
      system_log.writef ( "%s performs %s", actor->displayname(), p_name );
   }
   else
      system_log.writef ( "%s cast %s [%s]", actor->displayname(), p_word, p_name );


   int error;
  // bool donothing = false;
   //char conditionstr [ 128 ];
   //printf ("Cast : actor name = %s\n", actor->name() );

   //printf ("Debug: Cast: before lose MP\n");
   actor->lose_MP ( MP_cost() );

   //printf ("Debug: Cast: after lose MP\n");

   //bool notarget = false;

   // todo: add effect dodging with MD or other save


   // loop target effect here instead of sub proc

   /*#define Action_TARGET_ONEENEMY   5 // target_ID = table monster ID
#define Action_TARGET_GROUPENEMY 6 // target_ID = 1 = front, 2 = back
#define Action_TARGET_ALLENEMY   7 // target_ID is ignored
#define Action_TARGET_ONECHARACTER  9 // target_ID = PK of character
#define Action_TARGET_GROUPCHARACTER 10 // target_ID = 1 = front, 2 = back
#define Action_TARGET_ALLCHARACTER  11 // target_ID is ignored.
#define Action_TARGET_EVERYBODY  15 //*/

   // monsters first
   if ( ( action.target_type() & Action_TARGET_MASK_ENEMY ) > 0)
   {
      //printf ("Debug: Spell: cast: passed in ennemy\n");
      Monster tmpmonster;

      switch ( ( action.target_type() & Action_TARGET_MASK_QTY ) )
      {
         case Action_TARGET_QTY_ONE:
            error = tmpmonster.SQLpreparef ( "WHERE pk=%d", action.targetID()/*, Character_BODY_ALIVE*/  );
            //sprintf (conditionstr, "WHERE pk=%d AND body=%d ", action.targetID(), Character_BODY_ALIVE );
           // printf ("Debug: Spell: cast: passed in one\n");
           //printf ("Debug: Cast: Pass in prepare for 1 target\n");
         break;
         case Action_TARGET_QTY_GROUP:
            error = tmpmonster.SQLpreparef ( "WHERE rank=%d", action.targetID()/*, Character_BODY_ALIVE*/  );
            //sprintf (conditionstr, "WHERE rank=%d AND body=%d", action.targetID(), Character_BODY_ALIVE  );
           // printf ("Debug: Spell: cast: passed in group\n");
         break;
         case Action_TARGET_QTY_ALL:
            error = tmpmonster.SQLpreparef ( ""/*, Character_BODY_ALIVE*/  );
            //sprintf (conditionstr, "WHERE body=%d", Character_BODY_ALIVE);
            //printf ("Debug: Spell: cast: passed in all\n");
         break;
         default:
            error = SQLITE_ERROR;
         break;
      }

/*      if ( donothing == false)
      {
         tmpmonster.SQLprocedure ( Monster_CB_APPLY_SPELL, conditionstr );
      }*/
      //printf ("Debug: Cast: before SQLloop\n");
      if ( error == SQLITE_OK)
      {
         error = tmpmonster.SQLstep ();
         //SQLactivate_errormsg();
         while ( error == SQLITE_ROW )
         {
       //     printf ("Debug: Cast: before compile stats\n");

            tmpmonster.compile_stat();
            //printf ( "Debug: Spell: cast: Before cast on target: %s (%d/%d)\n", target->displayname(), target->current_HP()
             //            , target->max_HP() );
            cast_on_target ( action, actor, &tmpmonster );
            //printf ( "Debug: Spell: Cast: After cast on target: %s (%d/%d)\n", target->displayname(), target->current_HP()
             //            , target->max_HP() );

            //SQLerrormsg_activate();
            //tmpmonster.SQLupdate();
            //SQLerrormsg_deactivate();
            error = tmpmonster.SQLstep();

         }
         //SQLdeactivate_errormsg();

         tmpmonster.SQLfinalize ();
      }
     // printf ("Debug: Cast: exit SQL loop\n");
   }

   //characters second
   if ( ( action.target_type() & Action_TARGET_MASK_CHARACTER ) > 0 )
   {
      Character tmpchar;
    //  donothing = false;

      switch ( ( action.target_type() & Action_TARGET_MASK_QTY ) )
      {
         case Action_TARGET_QTY_ONE:
            error = tmpchar.SQLpreparef ( "WHERE pk=%d ", action.targetID() );
            //sprintf (conditionstr, "WHERE pk=%d AND body=%d ", action.targetID(), Character_BODY_ALIVE );
           // printf ("Debug: Spell: cast: passed in one\n");
           //printf ("Debug: Cast: Pass in prepare for 1 target\n");
         break;
         case Action_TARGET_QTY_GROUP:
            error = tmpchar.SQLpreparef ( "WHERE rank=%d AND location=2", action.targetID() /*, Character_BODY_ALIVE*/  );
            //sprintf (conditionstr, "WHERE rank=%d AND body=%d", action.targetID(), Character_BODY_ALIVE  );
           // printf ("Debug: Spell: cast: passed in group\n");
         break;
         case Action_TARGET_QTY_ALL:
            error = tmpchar.SQLpreparef ( "WHERE location=2"/*, Character_BODY_ALIVE*/  );
            //sprintf (conditionstr, "WHERE body=%d", Character_BODY_ALIVE);
            //printf ("Debug: Spell: cast: passed in all\n");
         break;
         default:
            error = SQLITE_ERROR;
         break;
      }


      if ( error == SQLITE_OK)
      {
         error = tmpchar.SQLstep ();

         while ( error == SQLITE_ROW )
         {
       //     printf ("Debug: Cast: before compile stats\n");
            tmpchar.compile_stat();
            cast_on_target ( action, actor, &tmpchar );

            //if ( party.status() != Party_STATUS_COMBAT)
            //tmpchar.SQLupdate();
            error = tmpchar.SQLstep();
         }

         tmpchar.SQLfinalize ();
      }

   }

   if (  action.target_type() == Action_TARGET_SELF_RANGE )
   {

      actor->compile_stat();
      cast_on_target ( action, actor, actor );
   }

   actor->SQLupdate();


   //printf ( "Debug: Spell: Cast: %s's effect id=%d", p_name, effect_id() );

   if ( effect_id() == Spell_EFFECT_UTILITY )
   {   return cast_utility();
   }
   else
   {   return Action_RESOLVE_RETVAL_NONE;
   }
}

void Spell::cast_on_target ( Action &action, Opponent *actor, Opponent *target )
{
//   printf ("Cast on target: actor name = %s\n", actor->name() );
   bool applyspell = true;
  // printf ( "Debug:spell:cast on target: effect id = %d, monstername = %s\n", effect_id(), target->name());

   // check for save, magic defense, skip dead if applicable (ex damage)


   if ( (defense() & DEFENSE_MAGIC_DEFENSE) > 0
       ||  (defense() & DEFENSE_HALF_MAGIC_DEFENSE) > 0)
   {
      int md = target->d20stat ( D20STAT_MD );
      int md_roll = dice ( 20 );

      if ( ( defense() & DEFENSE_HALF_MAGIC_DEFENSE) > 0 )
         md /= 2;

      if ( md_roll <  md )
      {
         applyspell = false;
         system_log.writef ( "%s deflect the spell", target->displayname() );
      }
   }

   if ( (defense() & DEFENSE_ACTIVE_DEFENSE) > 0
       ||  (defense() & DEFENSE_HALF_ACTIVE_DEFENSE) > 0)
   {
      int tmpcondition_target = target->compile_condition();
      int tmpcondition_actor = actor->compile_condition();

      //if (  )
      //{
         int ad = target->d20stat ( D20STAT_AD );
         int attackroll = dice ( 20 );
         bool attacksuccessful = false;

         if ( ( defense() & DEFENSE_HALF_ACTIVE_DEFENSE) > 0 )
            ad /= 2;

         if ( ( actor->flatstat( FLAT_CS) + attackroll ) > ad + 10 )
            attacksuccessful = true;

         if ( ( tmpcondition_target & ActiveEffect_CONDITION_FAILDODGE) > 0 )
            attacksuccessful = true;

         if ( attackroll <=2 ) //automatic Failure.
            attacksuccessful = false;


         if ( ( tmpcondition_actor & ActiveEffect_CONDITION_FAILATTACK ) > 0 )
            attacksuccessful = false;

         if ( attackroll >= 19 ) // roll of 19+ automatically hit, so always 10% chance to hit.
            attacksuccessful = true;

         if ( attacksuccessful == false )
         {
            applyspell = false;
            system_log.writef ( "%s avoid the attack", target->displayname() );
         }
      //}
   }

   if ( (defense() & DEFENSE_PHYSICAL_SAVE) > 0
       || (defense() & DEFENSE_HALF_PHYSICAL_SAVE) > 0 )
   {
      int tmpcondition = target->compile_condition();

      if ( ( tmpcondition & ActiveEffect_CONDITION_FAILPSAVE) == 0 )
      {


         int ps =  target->d20stat ( D20STAT_PS );
         int ps_roll = dice (20);

         if ( (defense() & DEFENSE_HALF_PHYSICAL_SAVE) > 0 )
            ps /= 2;


         if ( ps_roll < ps || ps_roll >= 19 )
         {
            applyspell = false;
            system_log.writef ( "%s resist the effect", target->displayname() );
         }
      }
   }

   if ( (defense() & DEFENSE_MENTAL_SAVE) > 0
       || (defense() & DEFENSE_HALF_MENTAL_SAVE) > 0 )
   {
      int tmpcondition = target->compile_condition();

      if ( ( tmpcondition & ActiveEffect_CONDITION_FAILMSAVE) == 0 )
      {
         int ms = target->d20stat ( D20STAT_MS );
         int ms_roll = dice ( 20 );

         if ( (defense() & DEFENSE_HALF_MENTAL_SAVE) > 0 )
            ms /= 2;

         if ( ms_roll <  ms || ms_roll >= 19 )
         {
            applyspell = false;
            system_log.writef ( "%s resist the effect", target->displayname() );
         }
      }
   }


   if ( applyspell == true )
   switch ( effect_id ())
   {
      case Spell_EFFECT_ATTACK_DICE :

         adjust_apply_damage ( actor, target, cast_attack_dice (  actor, target) );
      break;
      case Spell_EFFECT_ATTACK_FIXED :

         adjust_apply_damage ( actor, target, cast_attack_fixed ( actor, target ) );
      break;
      case Spell_EFFECT_ATTACK_TYPE_DICE:
         if ( ( target->weakness() &  param2() ) > 0 )
            adjust_apply_damage ( actor, target, cast_attack_dice (  actor, target) );
         else
            system_log.writef ( "No Effect" );
      break;
      case Spell_EFFECT_ATTACK_TYPE_FIXED :
         if ( ( target->weakness() &  param2() ) > 0 )
            adjust_apply_damage ( actor, target, cast_attack_fixed ( actor, target ) );
         else
            system_log.writef ( "No Effect" );
      break;
      case Spell_EFFECT_ATTACK_DRAIN_DICE :

         cast_drain_heal ( actor, adjust_apply_damage( actor, target, cast_attack_dice (actor, target)));
      break;
      case Spell_EFFECT_ATTACK_DRAIN_FIXED :

         cast_drain_heal ( actor, adjust_apply_damage( actor, target, cast_attack_fixed (actor, target)));
      break;
      case Spell_EFFECT_HEAL_DICE :

         cast_heal_dice ( actor, target );
      break;
      case Spell_EFFECT_HEAL_FIXED :

         cast_heal_fixed ( actor, target );
         //printf ( "Debug: Spell: Cast on target: %s (%d/%d)\n", target->displayname(), target->current_HP()
         //                , target->max_HP() );
      break;
      case Spell_EFFECT_CURE :
         cast_cure ( actor, target );
      break;
      case Spell_EFFECT_CURE_BODY :
         cast_cure_body ( actor, target );
      break;
      case Spell_EFFECT_BUFF:
      case Spell_EFFECT_BUFF_EXPEDITION:
         cast_active_effect ( actor, target );
      break;
      case Spell_EFFECT_DEBUFF:
      case Spell_EFFECT_CONDITION:
         cast_negative_active_effect ( actor, target );
      break;
      break;
      case Spell_EFFECT_CONDITION_BODY:
         cast_condition_body ( actor, target );
      break;
      case Spell_EFFECT_CONDITION_RANDOM:
         cast_condition_random ( actor, target );
      break;
      case Spell_EFFECT_DESTROY_ALL_EFFECT:
         cast_not_implemented ( actor, target );
      break;

      case Spell_EFFECT_SA_ATTRIBUTE_LOST:
         special_attack_attribute_lost ( actor, target );
      break;
      case Spell_EFFECT_SA_BREATH:
         adjust_apply_damage ( actor, target, special_attack_breath ( actor, target ) );
      break;
      case Spell_EFFECT_SA_SOUL_DRAIN:
         adjust_apply_damage ( actor, target, special_attack_soul_drain ( actor, target ) );
      break;
      case Spell_EFFECT_SA_PHYSICAL:
         adjust_apply_damage ( actor, target, special_attack_physical ( actor, target ) );
      break;
      case Spell_EFFECT_SA_DRAIN_DICE :
         cast_drain_heal ( actor, adjust_apply_damage( actor, target, special_attack_breath (actor, target)));
      break;
      case Spell_EFFECT_SA_DRAIN_FIXED :
         cast_drain_heal ( actor, adjust_apply_damage( actor, target, cast_attack_fixed (actor, target)));
      break;
      default:
         cast_not_implemented ( actor, target );
      break;
   }

   //target->SQLupdate();


   /*

#define Spell_EFFECT_UTILITY              70
#define Spell_EFFECT_OTHER                80*/

}

int Spell::adjust_apply_damage ( Opponent *actor, Opponent *target, int totaldamage )
{

   char tmpstr [100] = "";

   //printf ("Debug: Cast: ajust apply damage:\n");

   // check damage resistance

   if ( ( defense() & DEFENSE_DAMAGE_RESISTANCE ) > 0
       || ( defense() & DEFENSE_HALF_DAMAGE_RESISTANCE ) > 0 )
   {
      int dr = target->d20stat (D20STAT_DR);

      if ( ( defense() & DEFENSE_HALF_DAMAGE_RESISTANCE ) > 0 )
         dr /= 2;

      int dr_roll = dice (20);

      if ( dr_roll <= dr  || dr_roll >= 19 )
         totaldamage /= 2;

      if ( dr_roll == 1 ) // critical success 1/4
         totaldamage /= 2;

      if ( totaldamage == 0)
         totaldamage = 1;

      sprintf ( tmpstr, "Dmg Resist.: DR=%d, roll=%d -> %d Dmg"
                                 , dr
                                 , dr_roll
                                 , totaldamage);
   }


   // check reduce damage condition
   int tmpcondition = actor->compile_condition();

   if ( ( tmpcondition & ActiveEffect_CONDITION_REDMGKDMG) > 0 )
      totaldamage /= 4;

   if ( totaldamage <= 0)
         totaldamage = 1;


   // check resistance / 8 damage

   int tmpelement = element ();
   int targetelement = target ->resistance ();

   if ( ( tmpelement & targetelement ) > 0 )
   {
      // note: resistance will counter a weakness.

       switch ( config.get ( Config_WEAKRESIST_STRENGTH) )
               {
                  case Config_DIF_LOW: totaldamage /= 4; break;
                  case Config_DIF_NORMAL: totaldamage /= 8; break;
                  default: totaldamage /= 4;
               }

      if ( totaldamage <= 0)
         totaldamage = 1;

      sprintf  ( tmpstr, "%s RESISTANCE -> %d", tmpstr, totaldamage);
   }
   else
   {

   // check weakness *4 DMG


      targetelement = target->weakness();

      if ( ( tmpelement & targetelement ) > 0)
      {
                  switch ( config.get ( Config_WEAKRESIST_STRENGTH) )
                  {
                  case Config_DIF_LOW: totaldamage *= 2; break;
                  case Config_DIF_NORMAL: totaldamage *= 4; break;
                  default: totaldamage *= 4;
                  }
        // totaldamage *= 4;

         sprintf  ( tmpstr, "%s WEAKNESS -> %d", tmpstr, totaldamage);
      }
   }




   if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
      system_log.writef ( tmpstr);


   target->lose_HP ( totaldamage );
   target->SQLupdate();

   if ( target->body () != Character_BODY_ALIVE )
   {  system_log.writef ( "%s takes %d damage and is Killed", target->displayname(), totaldamage );
   }
   else
   {  system_log.writef ( "%s takes %d damage", target->displayname(), totaldamage );
   }

   return ( totaldamage );

}

int Spell::cast_attack_dice ( Opponent *actor, Opponent *target )
{
   //param1: if non-zero, set the nb of dice, else use X
   //param2: added effect ID
   //param3: DY required DMGZ is added to supplied DY

   //printf ("Debug: Cast: cast_attack_dice\n");

   int totaldamage;
   int dY = param3();
   int Xd = param1();

   if ( Xd == 0 )
      Xd = actor->xyzstat ( XYZSTAT_MDMG, XYZMOD_X); //already pre-devided with caster level

   // apply damage

   totaldamage  = roll_xdypz ( Xd , dY , actor->xyzstat ( XYZSTAT_MDMG ) );

   if ( totaldamage <= 0)
      totaldamage = 1;

   if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Damage Roll: %d (D%d + %d) = %d", Xd, dY,
                         actor->xyzstat ( XYZSTAT_MDMG ), totaldamage );


   if (param2() != 0 )
      if (   ActiveEffect::add_effect (param2(), target ) == true )
      {
         ActiveEffect tmpeffect;

         tmpeffect.template_SQLselect ( param2());

         system_log.writef ( "%s is inflicted on %s", tmpeffect.name(), target->displayname());
      }

   return ( totaldamage );

}

int Spell::cast_attack_fixed ( Opponent *actor, Opponent *target )
{
   //param 1: amount of damage
   //param 2: added effect
   //param 3: if non zero, replace param 1, inflict % of target's current HP.

   int totaldamage;

   if ( param3() == 0 )
   {


      totaldamage = param1();
      if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Fixed damage: %d", param1() );

   }
   else
   {


      totaldamage = ( param3() * target->current_HP() ) / 100;
      if ( totaldamage <= 0)
         totaldamage = 1;

      if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Proportion Damage: %d % of %d HP = %d damage", param3(),
                               target->current_HP(), totaldamage );

   }

   if (param2() != 0 )
      if ( ActiveEffect::add_effect (param2(), target ) == true )
      {

         ActiveEffect tmpeffect;

         tmpeffect.template_SQLselect ( param2());

         system_log.writef ( "%s is inflicted on %s", tmpeffect.name(), target->displayname());

      }


   return totaldamage;
}

/*int Spell::cast_attack_type_dice ( Opponent *actor, Opponent *target)
{
   //param1: nb_dice
   //param2: Monster category affected by the spell (not working)
   //param3: DY if non-zero, DMGZ is currently added to supplied DY

   int totaldamage = cast_attack_dice ( actor, target );

   // check if monster type listed

   return totaldamage;
}

int Spell::cast_attack_type_fixed ( Opponent *actor, Opponent *target )
{
   //param 1: amount of damage
   //param 2: Monster category affected by the spell
   //param 3: if non zero, replace param 1, inflict % of target's current HP.

   int totaldamage = cast_attack_fixed ( actor, target );

   //todo check if monster type listed, else return 0 damage

   return totaldamage;
}*/

void Spell::cast_drain_heal ( Opponent *actor, int HP )
{
   //if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)


   if ( actor->body () == Character_BODY_ALIVE )
   {
      if ( actor->type() == Opponent_TYPE_ENNEMY)
         system_log.writef ( "%s recover %d HP", actor->displayname(), HP );
      else
         system_log.writef ( "%s recover %d HP -> %d/%d", actor->displayname(), HP, actor->current_HP()
                         , actor->max_HP()  );

      actor->recover_HP ( HP );
      actor->SQLupdate();
   //   actor->SQLupdate();
   }
}

void Spell::cast_heal_dice ( Opponent *actor, Opponent *target)
{
   //param 1: Nb Dice if non Zero, else use X
   //Param 3: Dice Y to use, add MDMG Z

   //printf ("Debug: spell: cast healdice: passed here\n");
   char tmpstr [ 100 ];

   int totalheal;
   int dY = param3();
   int Xd = param1();

   if ( Xd == 0 )
      Xd = actor->xyzstat ( XYZSTAT_MDMG, XYZMOD_X);

   // apply damage

   totalheal  = roll_xdypz ( Xd , dY , actor->xyzstat ( XYZSTAT_MDMG ) );

   if ( totalheal <= 0)
      totalheal = 1;

   if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Heal Roll: %d (D%d + %d) = %d", Xd, dY,
                         actor->xyzstat ( XYZSTAT_MDMG ), totalheal );

   //printf ( "debug: heal: total hea = %d", totalheal);

  if ( target->body () == Character_BODY_ALIVE )
  {
      target->recover_HP ( totalheal );
      target->SQLupdate();
      //if ( target.type())
      if ( target->type() == Opponent_TYPE_ENNEMY)
         system_log.writef ( "%s recover %d HP", actor->displayname(), totalheal );
      else
         system_log.writef ( "%s recover %d HP -> %d/%d", target->displayname(), totalheal, target->current_HP()
                         , target->max_HP() );

      if ( party.status() != Party_STATUS_COMBAT)
      {
         sprintf ( tmpstr, "%s recover %d HP -> %d/%d", target->displayname(), totalheal, target->current_HP()
                         , target->max_HP() );
         WinMessage wmsg_results ( tmpstr );
         Window::show_all();
      }


  }

}

void Spell::cast_heal_fixed ( Opponent *actor, Opponent *target )
{

 //param 1: amount of damage

   //param 3: if non zero, replace param 1, heal % of target max HP

   int totalheal;
   char tmpstr [ 100 ];

   //printf ("Debug: Spell: Passed in Heal Fixed\n");

   if ( param3() == 0 )
   {


      totalheal = param1();
      if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Fixed Heal: %d HP", param1() );

   }
   else
   {


      totalheal = ( param3() * target->max_HP() ) / 100;

      if (totalheal <= 0 )
         totalheal=1;

      if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Proportion heal: %d % of %d max HP = %d HP", param3(),
                               target->current_HP(), totalheal );

   }

   if ( target->body () == Character_BODY_ALIVE )
   {
      target->recover_HP ( totalheal );
      target->SQLupdate();

      if ( target->type() == Opponent_TYPE_ENNEMY)
         system_log.writef ( "%s recover %d HP", actor->displayname(), totalheal );
      else
         system_log.writef ( "%s recover %d HP -> %d/%d", target->displayname(), totalheal, target->current_HP()
                         , target->max_HP() );


      if ( party.status() != Party_STATUS_COMBAT)
      {
         sprintf ( tmpstr, "%s recover %d HP -> %d/%d", target->displayname(), totalheal, target->current_HP()
                         , target->max_HP() );
         WinMessage wmsg_results ( tmpstr );
         Window::show_all();
      }


   }

   //printf ( "Debug: Spell: Cast Heal Fixed: %s (%d/%d)\n", target->displayname(), target->current_HP()
   //                      , target->max_HP() );

}


/*void Spell::cast_condition ( Opponent *actor, Opponent *target )
{

}*/

void Spell::cast_condition_body ( Opponent *actor, Opponent *target )
{
   // param 1 is the target condition of the effect.

   target->body ( param1());

   system_log.writef ( "%s is %s", target->displayname(), STR_CHR_BODY [ param1() ] );

   /*if ( party.status() != Party_STATUS_COMBAT)
         {
            WinMessage wmsg_results ( "Spell effect not functional" );
            Window::show_all();
         }*/

   // target->SQLupdate();
}

void Spell::cast_condition_random ( Opponent *actor, Opponent *target )
{
   /*system_log.writef ( "Spell effect not functional" );

   if ( party.status() != Party_STATUS_COMBAT)
         {
            WinMessage wmsg_results ( "Spell effect not functional" );
            Window::show_all();
         }*/

   //each param is a random category of effect to select. So can inflict up to 3 active effects

   ActiveEffect tmpeffect;
   int error;
   char tmpstr [ 100 ] = "";
   bool atleastoneeffect = false;

   int paramval [ 3 ] = { param1(), param2(), param3() };

   SQLbegin_transaction();

   for ( int i = 0; i < 3 ; i++ )
   {
      if ( paramval [ i ] > 0)
      {
         error = tmpeffect.template_SQLpreparef ( "WHERE category=%d ORDER BY random()", paramval [ i ]);

         if ( error == SQLITE_OK )
         {
            error = tmpeffect.SQLstep();

            if ( error == SQLITE_ROW )
            {

                  if ( ActiveEffect::add_effect ( tmpeffect.template_id(), target ) == true )
                  {
                     if ( atleastoneeffect == true )
                        strcat ( tmpstr, ", ");

                     strcat ( tmpstr, tmpeffect.name() );

                     atleastoneeffect = true;
                  }
            }


            tmpeffect.SQLfinalize();
         }
      }
   }

   if ( atleastoneeffect == true )
   {


      system_log.writef ( "%s is inflicted on %s", tmpstr, target->displayname());
   }

   target->SQLupdate();

   SQLcommit_transaction();
}

void Spell::cast_cure ( Opponent *actor, Opponent *target )
{
   //param1: cure type ID
   char querystr [101] = "";

   if ( target->body () == Character_BODY_ALIVE )
   {
      sprintf ( querystr, "DELETE FROM effect WHERE target_type=%d AND target_id=%d AND category=%d"
               , target->type(), target->primary_key(), param1() );
      SQLexec (querystr);

      system_log.writef ( "All %s 's %s effects are cured", target->displayname(), STR_SPELL_CURE [ param1()] );

      if ( party.status() != Party_STATUS_COMBAT)
         {
            sprintf ( querystr, "All %s 's %s effects are cured", target->displayname(), STR_SPELL_CURE [ param1()-1] );
            WinMessage wmsg_results ( querystr );
            Window::show_all();
         }

   }
   else
   {


      system_log.writef ( "%s is not alive, so no effect", target->displayname() );

      if ( party.status() != Party_STATUS_COMBAT)
         {
            sprintf ( querystr, "%s is not alive, so no effect", target->displayname() );
            WinMessage wmsg_results ( querystr );
            Window::show_all();
         }
   }
}

void Spell::cast_cure_body ( Opponent *actor, Opponent *target )
{
   // param 1: maximum level that can be cured
   // param 2: NB of HP recovered in % if greater than 0. and HP is not = 0;

char tmpstr [ 100 ];

   if ( target->body () <= param1() )
   {
      target->revive ();

      if ( target->body() == Character_BODY_ALIVE)
      {
         /*if ( target->current_HP() <= 0)
         {*/
            int HP = ( param2 () * target->max_HP()) / 100;
            if ( HP <= 0)
               HP = 1;

            if ( HP > target->current_HP() )
               target->set_HP ( HP );
         //}
          system_log.writef ( "%s is revived (%d/%d HP)", target->displayname(), target->current_HP(), target->max_HP() );


         if ( party.status() != Party_STATUS_COMBAT)
         {
            sprintf ( tmpstr, "%s is revived (%d/%d HP)", target->displayname(), target->current_HP()
                         , target->max_HP() );
            WinMessage wmsg_results ( tmpstr );
            Window::show_all();
         }
      }
      else
      {
         system_log.writef ( "%s 's revival failed.", target->displayname() );

         if ( party.status() != Party_STATUS_COMBAT)
         {
            sprintf ( tmpstr, "%s 's revival failed.", target->displayname() );
            WinMessage wmsg_results ( tmpstr );
            Window::show_all();
         }
      }
   }
   else
   {
      system_log.writef ( "Spell is not strong enough to revive %s ", target->displayname() );

      if ( party.status() != Party_STATUS_COMBAT)
         {
            sprintf ( tmpstr, "Spell is not strong enough to revive %s ", target->displayname() );
            WinMessage wmsg_results ( tmpstr );
            Window::show_all();
         }
   }
}

void Spell::cast_active_effect ( Opponent *actor, Opponent *target )
{
   //param1: id of the spell effect
   //param2: element type affected

    if ( param2() > 0)
    {
       if ( ( target->weakness() &  param2() ) > 0 )
         ActiveEffect::add_effect ( param1(), target );
    }
    else
       ActiveEffect::add_effect ( param1(), target );
   /*ActiveEffect tmpeffect;

   tmpeffect.template_SQLselect ( param1());

   tmpeffect.target_type ( target->type());
   tmpeffect.target_id ( target->primary_key() );

   char querystr [101];

   sprintf ( querystr, "DELETE FROM effect WHERE target_type=%d AND target_id=%d AND template_id=%d",
            target->type(), target->primary_key(), param1() );

   SQLexec ( querystr );

   tmpeffect.SQLinsert ();*/


}

void Spell::cast_negative_active_effect ( Opponent *actor, Opponent *target )
{
   // This version logs the effect to the system, since there is a chance of failure.
   //param1: if of the spell effect
   //param2: element type affected

   bool cast_effect = false;

   if ( param2() > 0 )
   {
      if ( ( target->weakness() &  param2() ) > 0 )
         cast_effect = true;
   }
   else
      cast_effect = true;

   if ( cast_effect == true )
         if ( ActiveEffect::add_effect ( param1(), target ) == true )
         {
            ActiveEffect tmpeffect;

            tmpeffect.template_SQLselect ( param1());

            system_log.writef ( "%s is inflicted on %s", tmpeffect.name(), target->displayname());
         }
         /*else
            system_log.writef ( "%s has no effect", target->displayname());*/

}

void Spell::cast_not_implemented ( Opponent *actor, Opponent *target )
{
   system_log.writef ( "Spell effect not functional" );

   if ( party.status() != Party_STATUS_COMBAT)
         {

            WinMessage wmsg_results ( "Spell effect not functional" );
            Window::show_all();
         }
}

void Spell::special_attack_attribute_lost ( Opponent *actor, Opponent *target )
{
   //param 1: attribute ID, 0 = random
   //not used param 2: Att Divider ( not sure if multiple att possible, physical of magick
   // idea: maybe stat 1-3 use physical stats, while attribute 4-6 use mental stats

   //if ( target.type() == )

   // problem: need to cast back to character, because if reselect data as character, opponent will overwrite changes when he will update
   // disable for now since no application for now.
}

int Spell::special_attack_breath ( Opponent *actor, Opponent *target )
{
   //param 1: divider 0 = 1 attack
   //param 2: added effect
   //param 3: DY, if 0 else  use size for dice size instead of dmgy
   //

   int totaldamage;
   int dY = DIE_SIZE [ actor->size() ];
   int Xd;

   if ( param3() > 0)
      dY = param3();

   if ( param1() > 0 )
      Xd = ( actor->level() / param1() ) + 1;
   else
      Xd = 1;

// apply damage

   totaldamage  = roll_xdypz ( Xd , dY , 0/*actor->xyzstat ( XYZSTAT_MDMG )*/ );

   if ( totaldamage <= 0)
      totaldamage = 1;

   if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Damage Roll: %d (D%d + %d) = %d", Xd, dY,
                         0, totaldamage );

   if (param2() != 0 )
      if ( ActiveEffect::add_effect (param2(), target ) )
      {
         ActiveEffect tmpeffect;

         tmpeffect.template_SQLselect ( param2());

         system_log.writef ( "%s is inflicted on %s", tmpeffect.name(), target->displayname());
      }

   return ( totaldamage );


}

int Spell::special_attack_soul_drain ( Opponent *actor, Opponent *target )
{
   //param 1: Power Divider 0 = 1 attack only
   //param 2: added effect
   //param 3: Nb soul lost, 0 = regular attack that use power
   // MDMGY: use power for dice size
   // heal 1 hit dice per soul point drained

   int totaldamage;
   int soulheal;
   int dY = DIE_SIZE [ actor->power() ];
   int Xd;

   if (dY <= 0)
      dY = 2;

   if ( param1() > 0 )
      Xd = ( actor->level() / param1() ) + 1;
   else
      Xd = 1;

   totaldamage  = roll_xdypz ( Xd , dY , 0/*actor->xyzstat ( XYZSTAT_MDMG )*/ );

   if ( totaldamage <= 0)
      totaldamage = 1;


   if ( param3() > 0 )
   {
      target->lose_soul ( param3() );
      soulheal = roll_xdypz ( param3(), DIE_SIZE [ actor->size()], 0 );
      actor->recover_HP ( soulheal );
      system_log.writef ( "%s loses %d soul and %s recover %d HP",
                         target->displayname(), param3(), actor->displayname(), soulheal );
   }

   if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Damage Roll: %d (D%d + %d) = %d", Xd, dY,
                         0, totaldamage );


   if (param2() != 0 )
      if ( ActiveEffect::add_effect (param2(), target ) == true )

      {
         ActiveEffect tmpeffect;

         tmpeffect.template_SQLselect ( param2());

         system_log.writef ( "%s is inflicted on %s", tmpeffect.name(), target->displayname());
      }


   return ( totaldamage );

}

int Spell::special_attack_physical ( Opponent *actor, Opponent *target )
{
   //param 1: Att divider 0 = 1 attack
   //param 2: added effect id
   //use dmgy of the monster

   int totaldamage;
   //int dY = DICE_SIZE [ actor.size() ];
   int Xd;

   if ( param1() > 0 )
      Xd = ( actor->level() / param1() ) + 1;
   else
      Xd = 1;

// apply damage

   totaldamage  = roll_xdypz ( Xd , actor->xyzstat ( XYZSTAT_DMG, XYZMOD_Y ) , 0/*actor->xyzstat ( XYZSTAT_MDMG )*/ );

   if ( totaldamage <= 0)
      totaldamage = 1;

   if ( config.get (Config_DETAIL_COMBAT_ROLL) == Config_YES)
            system_log.writef ( "Damage Roll: %d (D%d + %d) = %d", Xd, actor->xyzstat ( XYZSTAT_DMG, XYZMOD_Y ),
                         0, totaldamage );

   if (param2() != 0 )
      if ( ActiveEffect::add_effect (param2(), target ) == true )
      {
         ActiveEffect tmpeffect;

         tmpeffect.template_SQLselect ( param2());

         system_log.writef ( "%s is inflicted on %s", tmpeffect.name(), target->displayname());
      }

   return ( totaldamage );

}

void Spell::special_attack_call_help ( Opponent *actor, Opponent *target )
{
   // param 1: if > 0 call specified monster ID. Else call self.

   // notes
   // cannot exceed config limit, of opponents alive, but I don't think there is a restriction for
   // dead monsters regarding party ranks for example.
   // will need to search with full name since no template record id.

   system_log.writef ( "%s call for help ( not implemented)", actor->displayname());
}

int Spell::cast_utility (  )
{
   // param1: instant effect ID
   // param2: active effect ID

   // note: soe effect like chest and lock open/disarm is just about casting the spell or not. There is nothing to display
   // to the user when casting the spell besides affecting the current door, chest, NPC, etc.
   int retval = Action_RESOLVE_RETVAL_NONE ;
   //printf ( "Debug: Spell: Passes in cast_utility \n");
  // printf ( "Debug: Spell: Cast utility: param 1 = %d\n", param1() );

   switch ( param1() )
   {
      // switch for each effect
      case 1:
         retval = cast_utility_DUMAPIC ( );
      break;
      case 2:
         retval = cast_utility_MALKAMA ( );
      break;
      case 3:
         retval = cast_utility_MALOR (  );
      break;
      case 4:
         retval = cast_utility_CALFO ( );
      break;
      case 5:
         retval = cast_utility_ZILFE ( );
      break;
      case 6:
         retval = cast_utility_LOKTOFEIT (  );
      break;
      case 7:
         retval = cast_utility_DESTO (  );
      break;
      case 8:
         retval = cast_utility_CALNOVA ( );
      break;
      case 9:
         retval = cast_utility_CALDU ( );
      break;
      case 10:
         retval = cast_utility_ELEMOS (  );
      break;
      case 11:
         retval = cast_utility_NOBAIS ();
      break;
      case 12:
         retval = cast_utility_KATU ( );
      break;
   }

   // to do : add active effect if param 2

   return ( retval );

}

int Spell::cast_utility_DUMAPIC (  )
{
   handmap.display( party.position(), true );

   maze.draw_maze();

   return Action_RESOLVE_RETVAL_NONE;
}

int Spell::cast_utility_MALKAMA ( )
{
   // clairvoyance
   // can be done dnow, adapt editor map display

   //------------------------------------------------------------------------------
   short x;
   short y;
   //int markx;
   //int marky;
   int i;
   int j;
   short basex;
   short basey;
   //s_Maze_tile tmptile;
   //unsigned char solid;
   //bool draw_selection;
   //bool xok = false;
   //bool yok = false;
   //int tmpfilling;
   Event tmpevent;
   //int error;

   clear_bitmap ( editorbuffer );

   // draw numbers

   basex = 234;
   basey = 312;
   y = 0;

   s_Party_position tmpos = party.position();
   int yscroll  = tmpos.y -4;
   int xscroll = tmpos.x -4;
   int zcur = tmpos.z;

   for ( i = yscroll ; i < yscroll + 9 ; i++ )
   {
      textprintf_old ( editorbuffer, FNT_small, basex, basey - y, General_COLOR_TEXT, "%d", i );
      y = y + 16;
   }

   basey = 328;
   x = 250;

   for ( i = xscroll ; i < xscroll + 9 ; i++ )
   {
      textprintf_old ( editorbuffer, FNT_small, x, basey, General_COLOR_TEXT, "%d", i );
      x = x + 16;
   }

   // draw floor level

   rectfill ( editorbuffer, 230, 328, 246, 344, makecol ( 200, 200, 200 ) );

   /*old code to be replaced with internal number for simplicity
   if ( p_zcur == p_rclevel )
      textprintf_old ( editorbuffer, FNT_small, 0, 436, makecol ( 0, 0, 0 ), "RC" );

   if ( p_zcur < p_rclevel )
      textprintf_old ( editorbuffer, FNT_small, 0, 436, makecol ( 0, 0, 0 ),
         "F%d", ( p_zcur - p_rclevel) );

   if ( p_zcur > p_rclevel )
      textprintf_old ( editorbuffer, FNT_small, 0, 436, makecol ( 0, 0, 0 ),
         "B%d", abs (  p_rclevel - p_zcur  ) );*/

   textprintf_old ( editorbuffer, FNT_small, 234, 328, makecol ( 0, 0, 0 ),
         "%d", zcur );

   // draw grid ?? not sure what it does, maybe draw 4 corners here
   //drawing_mode(DRAW_MODE_MASKED_PATTERN, BMP_patgrid, 0, 0 );
   for ( x = 248 ; x < 392; x = x + 16 )
      for ( y = 184 ; y < 328 ; y = y + 16 )
         draw_sprite ( editorbuffer, datref_editoricon [ EDICON_TILE_EMPTY ], x, y );

      //hline ( editorbuffer, 16, y, 432, makecol ( 150, 150, 150 ) );
      //vline ( editorbuffer, x, 16, 432, makecol ( 150, 150, 150 ) );

   //drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0 );

   // ----- main drawing loop -----

   x = 248;
   y = 312;

   for ( j = yscroll ; j < yscroll + 9 ; j++ )
   {
      for ( i = xscroll ; i < xscroll + 9 ; i++ )
      {
         // ----- draw special -----

         if ( i >= 0 && i < Maze_MAXWIDTH && j >= 0 && j < Maze_MAXWIDTH && zcur >= 0 && zcur < Maze_MAXDEPTH )

            if ( ( mazetile [ zcur ][ j ][ i].special & MAZETILE_SPECIAL_SOLID ) == MAZETILE_SPECIAL_SOLID )

               draw_sprite ( editorbuffer, datref_editoricon [ EDICON_TILE_SOLID], x, y );



         //solid = mazetile [ zcur ] [ j ] [ i ]  . solid;
         Editor_draw_space_wall( mazetile [ zcur ] [ j ] [ i ], y, x );


         // draw light

        /* if ( is_tile_special ( zcur, j, i, Maze_SPECIAL_LIGHT) == true )
         {
            //set_trans_blender(0, 0, 0, 200);
            //drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );
            //rectfill ( editorbuffer, x, y, x+15, y+15, makecol ( 255, 255, 255) );
            draw_sprite ( editorbuffer, datref_editoricon [EDICON_TILE_LIGHT], x, y );
            //set_trans_blender(0, 0, 0, 255);
         }*/


         // draw filling
         Editor_draw_space_filling( mazetile [zcur][j][i], y, x );
         /*tmpfilling = mazetile [zcur][j][i].special & MAZETILE_FILLING_MASK;
         if ( tmpfilling > 0 )
         {
            set_trans_blender(0, 0, 0, 100);
            drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );

            if (Maze_SWALL_INFO[ tmpfilling].drawmode == DRAW_MODE_TRANS)
            {
               rectfill ( editorbuffer, x, y, x+15, y+15,
                      makecol ( Maze_SWALL_INFO [ tmpfilling ].red,
                         Maze_SWALL_INFO [ tmpfilling ].green,
                         Maze_SWALL_INFO [ tmpfilling ].blue ) );

            }
            else
            {
               if ( tmpfilling == MAZETILE_FILLING_DARKNESS )
                  draw_trans_sprite ( editorbuffer, datref_editoricon [EDICON_TILE_LIGHT], x, y );
            }

            drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
            set_trans_blender(0, 0, 0, 255);
         }*/


         x = x + 16;
      }
      x = 248;
      y = y - 16;
   }

   // draw start position

   draw_sprite ( editorbuffer, datref_editoricon [ EDICON_SPECIAL_START ], 312, 248 );


    //---------------------------------------------------------------------

    textprintf_old ( editorbuffer, FNT_small, 200, 417, makecol ( 255, 255, 255 ),
      "Press any key to return" );

   blit_editorbuffer();
   copy_buffer ();

   mainloop_readkeyboard();

   drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );

   return Action_RESOLVE_RETVAL_NONE;
}

int Spell::cast_utility_MALOR (  )
{
   // check for solid, kill party if it applies.

   // check for magic bounce, prevent teleporting there

   // If warp at city or above, fall and get damage equal to height (See D&D damage chart)

   // maybe targetting inside input spell, else need to refund the spell because spell is cancellable.
   // see if there are other impacts.
   // Use distributor, can cast 0,0,0 to cancel spell

   /*Distributor dst_location;
   dst_location.title ("Where do you want to teleport");
   dst_location.total ("SHould not be displayed");

   dst_location.add_item ( "X: -West +East", -99, 99 );
   dst_location.add_item( "Y: -South +North", -99 , 99 );
   // need to set starting value, min, max, not just min max unless behavior changer
   dst_location.add_item( "Z: -Down +Up", -20, 20 );

   dst_location.total_value ( 100 );

   WinDistributor wdst_location ( dst_location, 10, 50 );

   Window::refresh_all();
   Window::show_all();*/

   // need to enchance distributor to allow unlimited points and starting point.

   system_log.writef ( "%s is not yet implemented", name());

   if ( party.status() != Party_STATUS_COMBAT )
   {
       WinMessage wmsg_notavailable ( "This spell is not implemented yet." );
       Window::show_all();
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int Spell::cast_utility_CALFO ( )
{
   // identify chest, wait for chest event
   system_log.writef ( "%s is not yet implemented", name());

   if ( party.status() != Party_STATUS_COMBAT )
   {
       WinMessage wmsg_notavailable ( "This spell is not implemented yet." );
       Window::show_all();
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int Spell::cast_utility_ZILFE (  )
{
   // uncurse item
   // can be done now.
   // simply unequip all cursed items from a character.
   // maybe input character here

   system_log.writef ( "%s is not yet implemented", name());

   if ( party.status() != Party_STATUS_COMBAT )
   {
       WinMessage wmsg_notavailable ( "This spell is not implemented yet." );
       Window::show_all();
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int Spell::cast_utility_LOKTOFEIT (  )
{
   int goldroll = dice (40) + 10;
   int lostgold = ( party.gold() * goldroll) / 100;

   party.remove_gold ( lostgold );

   Item tmpitem;

   tmpitem.SQLprocedure ( Item_CB_LOKTOFEIT_DESTROY_ITEM, "WHERE loctype=2 AND key=0" );

   system_log.writef ( "Warp to city, Lost %d gold and maybe items in inventory", lostgold );

   //printf ("Debug: Spell: Cast_utility LOKTOFEIT: passed here \n");

   party.status ( Party_STATUS_CITY );

   WinMessage wmsg_effect ( "You create a portal that warps you back to the city\nYou might lose gold and items in the process.");
   Window::show_all();

   return ( Action_RESOLVE_RETVAL_EXIT );
}

int Spell::cast_utility_DESTO (  )
{
   // melt lock (use in event)
   // test with door event.

   system_log.writef ( "%s is not yet implemented", name());

   if ( party.status() != Party_STATUS_COMBAT )
   {
       WinMessage wmsg_notavailable ( "This spell is not implemented yet." );
       Window::show_all();
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int Spell::cast_utility_CALNOVA (  )
{
   //Disarm Trap
   system_log.writef ( "%s is not yet implemented", name());

   if ( party.status() != Party_STATUS_COMBAT )
   {
       WinMessage wmsg_notavailable ( "This spell is not implemented yet." );
       Window::show_all();
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int Spell::cast_utility_CALDU ( )
{
   //Identify Item
   // can be done now

   // need target item, else need to refund mana.
   system_log.writef ( "%s is not yet implemented", name());

   if ( party.status() != Party_STATUS_COMBAT )
   {
       WinMessage wmsg_notavailable ( "This spell is not implemented yet." );
       Window::show_all();
   }

   return Action_RESOLVE_RETVAL_NONE;
}

int Spell::cast_utility_ELEMOS (  )
{
   // Identify monster and stats

   // wait for eSQL monster so that you could list field to display on a full screen for all 8 monsters.

   SQLexec ( "UPDATE monster SET identified=2 ");

   return Action_RESOLVE_RETVAL_NONE;
}
int Spell::cast_utility_NOBAIS (  )
{
   // Mind Read: NPC
   system_log.writef ( "%s is not yet implemented", name());

   if ( party.status() != Party_STATUS_COMBAT )
   {
       WinMessage wmsg_notavailable ( "This spell is not implemented yet." );
       Window::show_all();
   }

   return Action_RESOLVE_RETVAL_NONE;
}
int Spell::cast_utility_KATU (  )
{
   // see if can make a combat spell with it and a utility spell for NPC
   // Else KATU and NOBAIS similar, for NPC usage

   system_log.writef ( "%s is not yet implemented", name());

   if ( party.status() != Party_STATUS_COMBAT )
   {
       WinMessage wmsg_notavailable ( "This spell is not implemented yet." );
       Window::show_all();
   }

   return Action_RESOLVE_RETVAL_NONE;
}


//-----------------------------------------------------------------------------------
//-                        Callback methods for SQLprocedure
//-----------------------------------------------------------------------------------

void Spell::callback_handler ( int index )
{

   switch ( index )
   {
      case Spell_CB_PRINTF_NAME:
         cb_printf_name();
      break;
   }

}

void Spell::cb_printf_name ( void )
{


   printf ("Spell Name: %s\n", p_name );
}

//-----------------------------------------------------------------------------------
//-                     virtual method from SQL enchanced object
//-----------------------------------------------------------------------------------

void Spell::template_load_completion ( void )
{
   // does nothing, no need to load template
}

/*----------------------------------------------------------------------------------*/
/*-                       Hash Table                                               -*/
/*----------------------------------------------------------------------------------*/



