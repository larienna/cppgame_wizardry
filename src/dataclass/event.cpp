/***************************************************************************/
/*                                                                         */
/*                                E V E N T . H                            */
/*                             Class Definition                            */
/*                                                                         */
/*     Content : Class Race                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : 9 janvier, 2014                                     */
/*     License : GNU General Public License                                */
/*                                                                         */
/*          This class encapsulate and manage the events for the maze.     */
/*                                                                         */
/***************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

//#include <typeinfo>

bool Event::p_demo = false;

/*-------------------------------------------------------------------------*/
/*-                          Constructor                                  -*/
/*-------------------------------------------------------------------------*/

Event::Event ( void )
{
   p_trigger = 0; // condition to trigger the event
	p_lock_event = 0; // lock event that occurs when triggered
   p_lock_key = 0; // key necessary to pass the lock event
	p_lock_var = 0; // extra variable that could be used by the lock event
   p_pass.event = 0; // type of event if the lock is passed
	p_pass.var1 = 0; // 1st variable of the pass event
   p_pass.var2 = 0; // 2nd variable of the pass event
	p_pass.var3 = 0; // 3rd variable of the pass event
	p_pass.var4 = 0; // 4th variable of the pass event
	p_fail.event = 0; // type of event if the lock is failed
	p_fail.var1 = 0; // 1st variable of the fail event
	p_fail.var2 = 0; // 2nd variable of the fail event
	p_fail.var3 = 0; // 3rd variable of the fail event
	p_fail.var4 = 0; // 4th variable of the fail event

   strcpy ( p_lock_msg, "");
	strcpy ( p_lock_str, "");
	strcpy ( p_pass.msg, "");
	strcpy ( p_pass.str, "");
	strcpy ( p_fail.msg, "");
	strcpy ( p_fail.str, "");

   strcpy (p_tablename, "event");
}

Event::~Event ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                       Properties Methods                              -*/
/*-------------------------------------------------------------------------*/


void Event::trigger ( int value )
{
   p_trigger = value;
}

int Event::trigger ( void)
{
   return ( p_trigger );
}

void Event::lock_event ( int value )
{
   p_lock_event = value;
}

void Event::lock_key ( int value )
{
   p_lock_key = value;
}

void Event::pass_event ( int value )
{
   p_pass.event = value;
}

void Event::fail_event ( int value )
{
   p_fail.event = value;
}

/*-------------------------------------------------------------------------*/
/*-                          Methods                                      -*/
/*-------------------------------------------------------------------------*/


bool Event::start ( bool demo )
{
   //printf ("Enter the event start\n");
   bool maze_interrupt = false;
   p_demo = demo;
   bool lockpassed = false;

   lockpassed = start_lock_event ( );

   maze_interrupt = start_passfail_event( lockpassed );

   return ( maze_interrupt );
}

/*-------------------------------------------------------------------------*/
/*-                      Private Methods                                  -*/
/*-------------------------------------------------------------------------*/

bool Event::start_lock_event ( void )
{

   bool eventpassed = false;

   blit_mazebuffer();

   switch ( p_lock_event)
   {
      case Event_LOCK_AUTOPASS:
         eventpassed = le_autopass();
      break;
      case Event_LOCK_QUESTION:
         eventpassed = le_question();
      break;
      case Event_LOCK_ENCOUNTERLOCK:
         eventpassed = le_encounterlock();
      break;
      case Event_LOCK_HIDDENLOCK:
         eventpassed = le_hiddenlock();
      break;
      case Event_LOCK_RIDDLE:
         eventpassed = le_riddle();
      break;
      case Event_LOCK_COMBAT:
         eventpassed = le_combat();
      break;
      case Event_LOCK_NPC:
         eventpassed = le_NPC();
      break;
      default:
         printf ("Error: Event.start_lock_event: event %d is not in the list\n", p_lock_event );
      break;
   }
   return (eventpassed);
}

bool Event::start_passfail_event ( bool eventpassed )
{
   s_Event_passfail eventparam;
   bool maze_interrupt = false;

   if (eventpassed == true)
      eventparam = p_pass;
   else
      eventparam = p_fail;

   blit_mazebuffer();

   switch (eventparam.event)
   {
      case Event_PASSFAIL_DONOTHING:
      break;
      case Event_PASSFAIL_ENDGAME:
          maze_interrupt = pfe_endgame(eventparam);
      break;
      case Event_PASSFAIL_MOVE:
         maze_interrupt = pfe_move(eventparam);
      break;
      case Event_PASSFAIL_TELEPORT:
         maze_interrupt = pfe_teleport(eventparam);
      break;
      case Event_PASSFAIL_GAINITEM:
         maze_interrupt = pfe_gainitem(eventparam);
      break;
      case Event_PASSFAIL_TRADEITEM:
         maze_interrupt = pfe_tradeitem(eventparam);
      break;
      case Event_PASSFAIL_TRAP:
         maze_interrupt = pfe_trap(eventparam);
      break;
      case Event_PASSFAIL_COMBAT:
         maze_interrupt = pfe_combat(eventparam);
      break;
      case Event_PASSFAIL_EXITMAZE:
         maze_interrupt = pfe_exitmaze(eventparam);
      break;
      case Event_PASSFAIL_ROTATE:
         maze_interrupt = pfe_rotate(eventparam);
      break;
      case Event_PASSFAIL_MESSAGE:
         maze_interrupt = pfe_message(eventparam);
      break;
      case Event_PASSFAIL_STORYTEXT:
         maze_interrupt = pfe_storytext(eventparam);
      break;
      case Event_PASSFAIL_NPC:
         maze_interrupt = pfe_NPC(eventparam);
      break;
      case Event_PASSFAIL_ELEVETOR:
         maze_interrupt = pfe_elevator( eventparam);
      break;
      default:
         printf ("Error: Event.start_passfail_event: event %d is not in the list\n", eventparam.event );
      break;
   }

   return (maze_interrupt);

}

int Event::get_keyitem_ID ( int item_key )
{
   Item tmpitem;
   int error;
   //int retval = false;
   int retval = -1;

   //printf ("Event: Check for key: Before Prepare\n");

   //SQLactivate_errormsg();

   error = tmpitem.SQLpreparef ( "WHERE key=%d AND ( loctype=%d OR loctype=%d )",
                                item_key,
                                Item_LOCATION_PARTY,
                                Item_LOCATION_CHARACTER );

   if ( error == SQLITE_OK)
   {
      //printf ("Event: Check for key: prepare works\n");
      error = tmpitem.SQLstep ();

      if ( error == SQLITE_ROW)
      {
      //printf ("Event: Check for key: row is returned\n");
         if ( tmpitem.key() == item_key)
         {

             //printf ("Event: Check for key: key match is true\n");

            retval = tmpitem.primary_key();
         }
      }

      tmpitem.SQLfinalize();
   }

   return ( retval );
}

/*void Event::start_fail_event ( void )
{

}*/

   //private: void test [10] (void);

//--------------------------- lock events ----------------------------------
bool Event::le_autopass (void)
{
   // msg: Display a message before passing the lock is non-empty
   if ( p_lock_msg [0] != '\0' )
   {
      WinMessage wmsg_autopass ( p_lock_msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }

   return (true);
}

bool Event::le_question (void)
{
   // key: If You do not have the key, fail and no qst message
   // msg: Ask a question to the player
   int answer;
   bool askqst = true;
   int keyID = -1;
   char tmpstr [150];

      if ( p_lock_key > 0 )
      {
         if ( p_demo == false)
         {

         //printf ( "key? = %s\n", typeid(key).name());

         keyID = get_keyitem_ID( p_lock_key );
         //printf ( "Looking for key item %d, ID = %d\n", p_lock_key, keyID);
         if ( keyID == -1 )
            askqst = false;
         }
         else
         {
            sprintf ( tmpstr, "DEMO: I would ask a question here only if you had the key item no %d.\nDo you want to have the key item for event check?", p_lock_key);
            WinQuestion wqst_demo ( tmpstr, WinMessage_X, WinMessage_Y, true  );
            answer = Window::show_all();
            if ( answer == WinQuestion_ANSWER_YES )
               askqst = true;
            else
               askqst = false;
         }

      }

   if ( askqst == true )
   {
      blit_mazebuffer();
      WinQuestion wqst_eventquestion ( p_lock_msg, WinQuestion_X, WinQuestion_Y, true  );
      answer = Window::show_all();

      if ( answer == WinQuestion_ANSWER_YES )
         return ( true );
      else
         return (false);
   }
   return (false);
}

bool Event::le_encounterlock (void)
{
   // key: ID of the key required to pass
   // var: level of the lock for lock picking. level 21 is unpickable
   //     maybe spell level of caster must match lock level too.
   // msg: describe the lock
   int answer = 0;
   Item tmpitem;
   int error;
   int retval = false;
   char tmpstr [100];
   //bool nokey = false;
   Menu mnu_lockoptions ("What do you want to do?");

   int nb_item= 0;
   int answer2=0;

   WinMessage wmsg_error ( p_lock_msg, WinMessage_X, WinMessage_Y, true  );
   Window::show_all();

   mnu_lockoptions.add_item ( 0, "Use Key Item");
   //printf ("debug:0\n");
   mnu_lockoptions.add_item ( 1, "Lock Pick", true);
   //printf ("debug:1\n");
   mnu_lockoptions.add_item ( 2, "DESTO", true);
   //printf ("debug:2\n");
   mnu_lockoptions.add_item ( -1, "Return" );
  // printf ("debug:3\n");

   WinMenu wmnu_lockoptions (mnu_lockoptions, 200, 150, false, false, true);
//printf ("debug:4\n");
   wmsg_error.hide();
//printf ("debug:5\n");
   // fill up inventory (need to do manually for vname()


   if ( p_demo == false)
   {
  // printf ("Debug: Event:LE Lock Encounter: got in non-demo loop\n");
      while ( answer != -1 && retval == false)
      {

      //wmsg_error.unhide();
      wmnu_lockoptions.unhide();
      blit_mazebuffer();
      answer = Window::show_all();
      wmnu_lockoptions.hide();

      //nokey = false;

      if ( answer == 0)
      {
         if ( p_demo == false )
         {


         List lst_inventory ("Which item do you want to use?", 12);
         //SQLactivate_errormsg();

         error = tmpitem.SQLpreparef ( "WHERE loctype=%d OR loctype=%d",
                                //p_lock_key,
                                Item_LOCATION_PARTY,
                                Item_LOCATION_CHARACTER );

         if ( error == SQLITE_OK)
         {

            error = tmpitem.SQLstep ();

            while ( error == SQLITE_ROW)
            {
               lst_inventory.add_item ( tmpitem.primary_key(), tmpitem.vname());
               error = tmpitem.SQLstep ();
               nb_item++;

            }

            tmpitem.SQLfinalize();

            if (nb_item > 0)
            {
               WinList wlst_inventory ( lst_inventory, 200, 120, false, false, true);
               blit_mazebuffer();
               answer2 = Window::show_all();
               wlst_inventory.hide();

               if ( answer2 != -1)
               {
                  error = tmpitem.SQLselect ( answer2 );

                  if ( error == SQLITE_ROW)
                  {
                     if ( tmpitem.key() == p_lock_key)
                     {
                        sprintf ( tmpstr, "Using item %s\nWith Success", tmpitem.vname() );
                        retval = true;
                     }
                     else
                        sprintf ( tmpstr, "Using item %s\nFailed", tmpitem.vname() );

                     blit_mazebuffer();
                     WinMessage wmdg_useitem ( tmpstr, WinMessage_X, WinMessage_Y, true  );
                     Window::show_all();

                  }
                  else
                  {
                     blit_mazebuffer();
                     WinMessage wmsg_nokey ("ERROR: I cannot find the selected item in the database.", WinMessage_X, WinMessage_Y, true );
                     Window::show_all();
                  }

               }
            }

            else
            {
               blit_mazebuffer();
               WinMessage wmsg_nokey ("You do not have any key items in your inventory", WinMessage_X, WinMessage_Y, true );
               Window::show_all();
            }
         }
         else
         {
               blit_mazebuffer();
               WinMessage wmsg_nokey ("ERROR: Cannot Prepare Inventory Selection", WinMessage_X, WinMessage_Y, true );
               Window::show_all();
         }

         }
         else
         {
            sprintf ( tmpstr, "DEMO: I would show your inventory and hope you key item no %d.\nDo you want to have the key item for event check?", p_lock_key);
            WinQuestion wqst_demo ( tmpstr, WinMessage_X, WinMessage_Y, true  );
            answer = Window::show_all();
            if ( answer == WinQuestion_ANSWER_YES )
               retval = true;
            else
               retval = false;
         }

         //SQLdeactivate_errormsg();

      }
      else if ( answer == 1)
      {

      }
      else if ( answer == 2)
      {

      }


      }
   }

   if ( p_demo == true )
   {
      sprintf ( tmpstr, "DEMO: I would make a encounter to see if you had key item no %d.\nDo you want to have the key item for event check?", p_lock_key);
      WinQuestion wqst_demo ( tmpstr, WinMessage_X, WinMessage_Y, true  );
      answer = Window::show_all();
      if ( answer == WinQuestion_ANSWER_YES )
         return ( true);
      else
         return (false);
   }

   // required to be triggered from inside the maze
   return (retval);
}

bool Event::le_hiddenlock (void)
{
   // msg: display flavor text if available
   // Key: ID of the key required to pass
   char tmpstr [150];
   int answer;

   if ( p_lock_msg [0] != '\0' )
   {
      WinMessage wmsg_flavor ( p_lock_msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }

   if ( p_demo == false)
   {
      if ( get_keyitem_ID( p_lock_key) != -1 )
         return (true);
   }
   else
   {
      sprintf ( tmpstr, "DEMO: I would make a hidden check to see if you had key item no %d.\nDo you want to have the key item for event check?", p_lock_key);
      WinQuestion wqst_demo ( tmpstr, WinMessage_X, WinMessage_Y, true  );
      answer = Window::show_all();
      if ( answer == WinQuestion_ANSWER_YES )
         return ( true);
      else
         return (false);
   }
   return (false);
}

bool Event::le_riddle (void)
{
   // var: ID of the Long text to use for the riddle
   // msg: Description before the riddle.
   //   Maybe as a question if player want to answer the riddle
   // str: Answer of the riddle

   WinMessage wmsg_error ( "ADVENTURE ERROR\nThis lock event is not yet implemented (event failed)", WinMessage_X, WinMessage_Y, true  );
   Window::show_all();
   // requires long text interface
   return (false);
}

bool Event::le_combat (void)
{
   WinMessage wmsg_error ( "ADVENTURE ERROR\nThis lock event is not yet implemented (event failed)", WinMessage_X, WinMessage_Y, true  );
   Window::show_all();
   // requires combat encounter table of some kind
   return (false);
}

bool Event::le_NPC(void)
{
   WinMessage wmsg_error ( "ADVENTURE ERROR\nThis lock event is not yet implemented (event failed)", WinMessage_X, WinMessage_Y, true  );
   Window::show_all();
   // requires NPC table of some kind
   return (false);
}

//---------------------------------- pass/fail event ------------------------------
bool Event::pfe_endgame (s_Event_passfail param)
{
   char tmpstr [150];

   if ( param.msg [0] != '\0' )
   {
      WinMessage wmsg_flavor ( param.msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }

   blit_mazebuffer();
   WinMessage wmsg_error ( "Adventure completed!", WinMessage_X, WinMessage_Y, true  );
   Window::show_all();

   if ( p_demo == false)
   {
    // show ending here
     // printf ("Event: Endgame: pass though endgame\n");


      party.status (Party_STATUS_ENDGAME);
   }
   else
   {
      sprintf ( tmpstr, "DEMO: The game would end here. I would have show ending, credits, and return party to the city.");
      WinMessage wmsg_demo ( tmpstr, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }
   return (true);
}

bool Event::pfe_move (s_Event_passfail param)
{
   // var1: Direction to move. Use constants Event_PF_MOVE_...
   // var2: Nb of times to repeat the movement (Ex: move left 5 spaces)
   //       Note, this will display the maze each time (notsure). So players will see the movement.
   // msg: Display a message before moving if non-empty
   int repeat = param.var2;


   if ( param.msg [0] != '\0' )
   {
      WinMessage wmsg_flavor ( param.msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }

   if ( config.get ( Config_ANIMATION) == Config_ANI_YES)
   {
      maze.draw_maze();
      clear ( buffer);
   }

   while (repeat > 0)
   {
      switch ( param.var1 )
      {


         case Event_PF_MOVE_NORTH :
            party.move_north ();
         break;
         case Event_PF_MOVE_EAST :
            party.move_east ();
         break;
         case Event_PF_MOVE_SOUTH :
            party.move_south ();
         break;
         case Event_PF_MOVE_WEST :
            party.move_west ();
         break;
         case Event_PF_MOVE_BACK :
            party.walk_backward ();
         break;
         case Event_PF_MOVE_FORWARD :
            party.walk_forward();
         break;
         case Event_PF_MOVE_UP :
            party.move_up();
         break;
         case Event_PF_MOVE_DOWN :
            party.move_down();
         break;
      }
      repeat--;
   }

   if ( param.var1 == Event_PF_MOVE_UP )
   {
      if ( config.get ( Config_ANIMATION) == Config_ANI_YES)
      {
         show_transition_slide_fade( mazebuffer, Screen_FADE_OUT, Screen_SLIDE_DOWN, 8 );
         maze.draw_maze();
         clear (buffer);
         show_transition_slide_fade( mazebuffer, Screen_FADE_IN, Screen_SLIDE_DOWN, 8 );
      }
   }
   else
      if ( param.var1 == Event_PF_MOVE_DOWN)
      {
         if ( config.get ( Config_ANIMATION) == Config_ANI_YES)
         {
            show_transition_slide_fade( mazebuffer, Screen_FADE_OUT, Screen_SLIDE_UP, 8 );
            maze.draw_maze();
            clear (buffer);
            show_transition_slide_fade( mazebuffer, Screen_FADE_IN, Screen_SLIDE_UP, 8 );
         }
      }

   return (false);
}

bool Event::pfe_teleport (s_Event_passfail param)
{
   // var1: X position
   // var2: Y position
   // var3: Z position
   // var4: Facing of the party. Use constants Party_FACE_...
   // msg: display before teleport

   if ( param.msg [0] != '\0' )
   {
      WinMessage wmsg_flavor ( param.msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }

   party.position ( param.var1, param.var2, param.var3, param.var4 );

   if ( config.get ( Config_ANIMATION ) == Config_ANI_YES )
   {
      blit_mazebuffer();
      Window::draw_party_frame();
      copy_buffer();
      blit ( screen, backup, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
      //blit_mazebuffer();

      maze.draw_maze ();
      blit_mazebuffer();
      Window::draw_party_frame();

      show_transition_algo7 ( backup, buffer, 15);
   }

   //WinMessage wmsg_error ( "ADVENTURE ERROR\nThis pass/fail event is not yet implemented" );
   //Window::show_all();

   return (false);
}

bool Event::pfe_gainitem (s_Event_passfail param)
{
   // var1: Item ID to duplicate and give to the player
   // msg: Flavor text to explain the acquisition of the event.
   Item tmpitem;
   int error;
   char tmpstr [100];

   if ( param.msg [0] != '\0' )
   {
      WinMessage wmsg_flavor ( param.msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }



   error = tmpitem.SQLselect ( param.var1 );

   if ( error == SQLITE_ROW)
   {

      if ( p_demo == false)
      {
         tmpitem.location (Item_LOCATION_PARTY, party.primary_key());
         tmpitem.identified ( false );
         tmpitem.SQLinsert();

         system_log.writef ("Event: Found Item %s ", tmpitem.vname() );

         sprintf ( tmpstr, "Found Item \n %s", tmpitem.vname() );
         blit_mazebuffer();
         WinMessage wmsg_found ( tmpstr, WinMessage_X, WinMessage_Y, true  );
         Window::show_all();
      }
      else
      {
         sprintf ( tmpstr, "DEMO: I would give you the following item here: %s", tmpitem.name());
         WinMessage wmsg_demo ( tmpstr, WinMessage_X, WinMessage_Y, true  );
         Window::show_all();
      }
   }
   else
   {
      blit_mazebuffer();
      WinMessage wmsg_error ( "ADVENTURE ERROR\nI cannot find the item I am supposed to give to the player.", WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }



   //WinMessage wmsg_error ( "This pass/fail event is not yet implemented" );
   //Window::show_all();
   // need to be done in maze with a party

   return (false);
}

bool Event::pfe_tradeitem (s_Event_passfail param)
{
   // var1: Key ID of the item to give away
   // var2: Item ID to be duplicated and given to the player
   // msg: Describe the trade.
   //WinMessage wmsg_error ( "This pass/fail event is not yet implemented" );
   //Window::show_all();
   // need to be done in maze with a party
   int error;
   char tmpstr [100];
   Item givenitem;
   Item gaineditem;
   int keyID;

   if ( param.msg [0] != '\0' )
   {
      WinMessage wmsg_flavor ( param.msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }

   keyID = get_keyitem_ID( param.var1 );

   //error = givenitem.SQLpreparef ( "WHERE key=%d AND ( loctype=%d OR locktype=%d )",
   //                             param.var1,
   //                             Item_LOCATION_PARTY,
   //                             Item_LOCATION_CHARACTER );

   if ( p_demo == false)
   {


   if ( keyID != -1)
   {
      error = givenitem.SQLselect ( keyID );

      if ( error == SQLITE_ROW)
      {

         error = gaineditem.SQLselect (param.var2);

         if ( error == SQLITE_ROW)
         {

               gaineditem.location (Item_LOCATION_PARTY, party.primary_key());
               gaineditem.identified ( false );
               gaineditem.SQLinsert();

               system_log.writef ("Event: Given %s and received %s ", givenitem.vname(), gaineditem.vname() );

               sprintf ( tmpstr, "Given %s and received %s ", givenitem.vname(), gaineditem.vname() );
               blit_mazebuffer();
               WinMessage wmsg_found ( tmpstr, WinMessage_X, WinMessage_Y, true  );
               Window::show_all();

               givenitem.SQLdelete ();

         }
         else
         {
            blit_mazebuffer();
            WinMessage wmsg_error ( "ADVENTURE ERROR\nI cannot find the item I am supposed to give to the player.", WinMessage_X, WinMessage_Y, true  );
            Window::show_all();
         }

      }
      else
      {
         blit_mazebuffer();
         WinMessage wmsg_error ( "ADVENTURE ERROR\nI cannot select the item\n I am supposed to trade with the player.", WinMessage_X, WinMessage_Y, true  );
         Window::show_all();
      }
   }
   else
   {
      blit_mazebuffer();
      WinMessage wmsg_error ( "ADVENTURE ERROR\nI cannot find in your inventory the item\n I am supposed to trade with the player.", WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }
 }
else
{
   sprintf ( tmpstr, "DEMO: I would have exchanged item with key %d for the ID %d", param.var1, param.var2);
   WinMessage wmsg_demo ( tmpstr, WinMessage_X, WinMessage_Y, true  );
   Window::show_all();
}



   return (false);
}

bool Event::pfe_trap (s_Event_passfail param)
{
   // var1: ID to the trap to trigger
   // msg: Text to display if not empty before encountering the trap
   WinMessage wmsg_error ( "ADVENTURE ERROR\nThis pass/fail event is not yet implemented", WinMessage_X, WinMessage_Y, true  );
   Window::show_all();
   // Need a table of traps with a class to run them.

   return (false);
}

bool Event::pfe_combat (s_Event_passfail param)
{
   WinMessage wmsg_error ( "ADVENTURE ERROR\nThis pass/fail event is not yet implemented", WinMessage_X, WinMessage_Y, true  );
   Window::show_all();
   // need encounter table system.

   return (false);
}

bool Event::pfe_exitmaze (s_Event_passfail param)
{
   // var1: If non-zero, then unlock and entrance
   // msg: Display message and exit the maze if non empty

   //WinMessage wmsg_error ( "ADVENTURE ERROR\nThis pass/fail event is not yet implemented" );
   //Window::show_all();

   char tmpstr [150];
   Entrance tmpenter;
   int errorsql;

   if ( param.msg [0] != '\0' )
   {
      WinMessage wmsg_flavor ( param.msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }

   if ( p_demo == false)
   {
      party.status (Party_STATUS_CITY);

      if ( param.var1 != 0)
      {

         errorsql = tmpenter.SQLselect ( param.var1 );

         if ( errorsql == SQLITE_ROW)
         {
            tmpenter.unlocked (true);
            tmpenter.SQLupdate();
         }
         else
         {
            WinMessage wmsg_error("ADVENTURE ERROR: I cannot select the entrance to unlock it\n no entrance unlocked");
            Window::show_all();
         }
      }

   }
   else
      {
         sprintf ( tmpstr, "DEMO: You can exit from the maze here using entrance %d", param.var1);
         WinMessage wmsg_demo ( tmpstr, WinMessage_X, WinMessage_Y, true  );
         Window::show_all();
         return (false);
      }

   return (true);
}

bool Event::pfe_rotate (s_Event_passfail param)
{
   // var1: determines direction to rotate. Use constants Event_PF_ROTATE_...
   int rndturns = dice (3);

   switch ( param.var1 )
   {
      case Event_PF_ROTATE_LEFT :
         party.turn_left();
      break;
      case Event_PF_ROTATE_RIGHT :
         party.turn_right();
      break;
      case Event_PF_ROTATE_BACK :
         party.turn_right();
         party.turn_right();
      break;
      case Event_PF_ROTATE_RANDOM :
         while (rndturns > 0 )
         {
            party.turn_right ();
            rndturns--;
         }
      break;
      case Event_PF_ROTATE_FACENORTH :
         party.facing ( Party_FACE_NORTH );
      break;
      case Event_PF_ROTATE_FACEEAST :
         party.facing ( Party_FACE_EAST );
      break;
      case Event_PF_ROTATE_FACESOUTH :
         party.facing ( Party_FACE_SOUTH );
      break;
      case Event_PF_ROTATE_FACEWEST :
         party.facing ( Party_FACE_WEST );
      break;

   }




//   WinMessage wmsg_error ( "ADVENTURE ERROR\nThis pass/fail event is not yet implemented" );
//   Window::show_all();

   return (false);
}

bool Event::pfe_message(s_Event_passfail param)
{
   // msg = Used to display a message

   WinMessage wmsg_error ( param.msg, WinMessage_X, WinMessage_Y, true );
   Window::show_all();

   return (false);
}

bool Event::pfe_storytext(s_Event_passfail param)
{
   WinMessage wmsg_error ( "ADVENTURE ERROR\nThis pass/fail event is not yet implemented", WinMessage_X, WinMessage_Y, true  );
   Window::show_all();
   // Wait for long test interface to be in place.

   return (false);
}

bool Event::pfe_NPC (s_Event_passfail param)
{
   WinMessage wmsg_error ( "ADVENTURE ERROR\nThis pass/fail event is not yet implemented", WinMessage_X, WinMessage_Y, true  );
   Window::show_all();
   // wait for NPC system to be in place

   return (false);
}

bool Event::pfe_elevator (s_Event_passfail param)
{
   // var1 = Highest level (small number)
   // var2 = Lowest Level (high number)
   // msg = Text to display that describe the elevator. Single line (not sure)
//printf ("Debug: Event: Passed in the elevator event\n");
   //WinMessage wmsg_error ( "ADVENTURE ERROR\nThis pass/fail event is not yet implemented", WinMessage_X, WinMessage_Y, true  );
   //Window::show_all();
   int nb_button = param.var2 - param.var1 +1;
   int pagesize;
   bool elevatorerror = false;
   s_Party_position tmpos = party.position();
   int currentfloor = 0;
   int floorcount = 0;
   int answer;
   char tmpstr [100];

   if ( param.msg [0] != '\0' )
   {
      WinMessage wmsg_flavor ( param.msg, WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
   }

   if ( nb_button > 10)
      pagesize=10;
   else
      pagesize = nb_button;

   if ( param.var1 < 0 || param.var2 >= 20)
   {
      blit_mazebuffer();
      WinMessage wmsg_error ( "ADVENTURE ERROR\nElevator out of bound\n Top or bottom exit the limit of the maze\nNote: floor must range between 0 and 19", WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
      elevatorerror = true;
   }

   if ( param.var1 > param.var2 )
   {
      blit_mazebuffer();
      WinMessage wmsg_error ( "ADVENTURE ERROR\nElevator:The last floor is higher than the bottom floor.\nNote: 0 is highest floor, 19 is the lowest floor", WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
      elevatorerror = true;
   }

   if ( param.var1 == param.var2 )
   {
      blit_mazebuffer();
      WinMessage wmsg_error ( "ADVENTURE ERROR\nElevator: Top and bottom floor are the same", WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
      elevatorerror = true;
   }

   if ( tmpos.z < param.var1 || tmpos.z > param.var2  )
   {
      blit_mazebuffer();
      WinMessage wmsg_error ( "ADVENTURE ERROR\nElevator: The current floor is not located between\nthe elevator's first and last floor", WinMessage_X, WinMessage_Y, true  );
      Window::show_all();
      elevatorerror = true;
   }

   if ( elevatorerror == false )
   {
      currentfloor =  tmpos.z - param.var1;
      List lst_floor ( "Elevator: Select a floor to move to", pagesize );

      while ( floorcount < nb_button)
      {
         if (floorcount == currentfloor)
            lst_floor.add_itemf (floorcount, "%c (*) Floor", 'A' + floorcount );
         else
            lst_floor.add_itemf (floorcount, "%c ( ) Floor", 'A' + floorcount );

         floorcount++;
      }

      blit_mazebuffer();
      WinList wlst_floor (lst_floor, 200, 100, false, false, true);
      answer = Window::show_all();
      wlst_floor.hide();

      if ( answer != -1)
      {
         tmpos.z = answer + param.var1;
         party.position ( tmpos );
         sprintf ( tmpstr , "Floor %c", 'A' + answer );
         maze.draw_maze();
         WinMessage wmsg_error ( tmpstr, WinMessage_X, WinMessage_Y, true  );
         Window::show_all();
      }
   }


   return (false);
}

/*-------------------------------------------------------------------------*/
/*-                       Virtual Methods                                 -*/
/*-------------------------------------------------------------------------*/



void Event::sql_to_obj (void)
{

   p_primary_key = SQLcolumn_int (0);
   p_trigger = SQLcolumn_int (1);
   p_lock_event = SQLcolumn_int (2);
   p_lock_key = SQLcolumn_int (3);
   p_lock_var = SQLcolumn_int (4);
   strncpy ( p_lock_msg, SQLcolumn_text(5), Event_MSG_VARIABLE_LEN );
	strncpy ( p_lock_str, SQLcolumn_text(6), Event_STR_VARIABLE_LEN );
	p_pass.event = SQLcolumn_int (7);
	p_pass.var1 = SQLcolumn_int (8);
	p_pass.var2 = SQLcolumn_int (9);
	p_pass.var3 = SQLcolumn_int (10);
	p_pass.var4 = SQLcolumn_int (11);
	strncpy ( p_pass.msg, SQLcolumn_text(12), Event_MSG_VARIABLE_LEN );
	strncpy ( p_pass.str, SQLcolumn_text(13), Event_STR_VARIABLE_LEN );
	p_fail.event = SQLcolumn_int (14);
	p_fail.var1 = SQLcolumn_int (15);
	p_fail.var2 = SQLcolumn_int (16);
	p_fail.var3 = SQLcolumn_int (17);
	p_fail.var4 = SQLcolumn_int (18);
	strncpy ( p_fail.msg, SQLcolumn_text(19), Event_MSG_VARIABLE_LEN );
	strncpy ( p_fail.str, SQLcolumn_text(20), Event_STR_VARIABLE_LEN );


}

void Event::template_sql_to_obj ( void)
{
   // will not be used
}

void Event::obj_to_sqlupdate (void)
{

sprintf (p_querystr, "lock_trigger=%d, lock_event=%d, lock_key=%d, lock_var=%d, lock_msg='%s', lock_str='%s', pass_event=%d, pass_var1=%d, pass_var2=%d, pass_var3=%d, pass_var4=%d, pass_msg='%s', pass_str='%s', fail_event=%d, fail_var1=%d, fail_var2=%d, fail_var3=%d, fail_var4=%d, fail_msg='%s', fail_str='%s' ",
	p_trigger,
	p_lock_event,
	p_lock_key,
	p_lock_var,
	p_lock_msg,
	p_lock_str,
	p_pass.event,
	p_pass.var1,
	p_pass.var2,
	p_pass.var3,
	p_pass.var4,
	p_pass.msg,
	p_pass.str,
	p_fail.event,
	p_fail.var1,
	p_fail.var2,
	p_fail.var3,
	p_fail.var4,
	p_fail.msg,
	p_fail.str );
}

void Event::obj_to_sqlinsert (void)
{
sprintf (p_querystr, "%d, %d, %d, %d, '%s', '%s', %d, %d, %d, %d, %d, '%s', '%s', %d, %d, %d, %d, %d, '%s', '%s'",
	p_trigger,
	p_lock_event,
	p_lock_key,
	p_lock_var,
	p_lock_msg,
	p_lock_str,
	p_pass.event,
	p_pass.var1,
	p_pass.var2,
	p_pass.var3,
	p_pass.var4,
	p_pass.msg,
	p_pass.str,
	p_fail.event,
	p_fail.var1,
	p_fail.var2,
	p_fail.var3,
	p_fail.var4,
	p_fail.msg,
	p_fail.str );
}

/*-------------------------------------------------------------------------*/
/*-                       Global Variables                                -*/
/*-------------------------------------------------------------------------*/


const char STR_EVE_TRIGGER [][32] =
{
  {"Move In"},
  {"Face North"},
  {"Face East"},
  {"Face South"},
  {"Face West"},
  {"N/A"},
  {"N/A"},
  {"Search Room"},

  {"Search North"},
  {"Search East"},
  {"Search South"},
  {"Search West"},
  {"Use Item"},
  {"Open Door North"},
  {"Open Door East"},
  {"Open Door South"},

  {"Open Door West"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},

  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"}
};

const char STR_EVE_LOCK [][32] =
{
  {"Auto Pass"},
  {"Question"},
  {"Encounter Lock"},
  {"Hidden Lock"},
  {"Riddle"},
  {"Combat"},
  {"NPC"},
  {"N/A"},

  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},

  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},

  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"}
};

const char STR_EVE_PASSFAIL [] [32] =
{
  {"Do Nothing"},
  {"End Game"},
  {"Move"},
  {"Teleport"},
  {"Gain Item"},
  {"Trade Item"},
  {"Trap"},
  {"Combat"},

  {"Exit Maze"},
  {"Rotate"},
  {"Message"},
  {"Story Text"},
  {"NPC"},
  {"Elevator"},
  {"N/A"},
  {"N/A"},

  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},

  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"},
  {"N/A"}
};

int EDICON_EVENT_LOCK [Event_NB_EVENT] =
{
   28, 37, 29, 38, 37, 33, 36, 28,
   28, 28, 28, 28, 28, 28, 28, 28,
   28, 28, 28, 28, 28, 28, 28, 28,
   28, 28, 28, 28, 28, 28, 28, 28
};

int EDICON_EVENT_PASSFAIL [Event_NB_EVENT] =
{
   28, 29, 30, 9, 31, 32, 8, 33,
   6, 7, 34, 35, 36, 5, 28, 28,
   28, 28, 28, 28, 28, 28, 28, 28,
   28, 28, 28, 28, 28, 28, 28, 28
};


