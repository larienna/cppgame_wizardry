//------------------------------------------------------------------
//
//                    E N T R A N C E . C P P
//
//   Content: Class Entrance
//   Programmer: Eric Pietrocupo
//   Starting Date: Januray 23rd, 2014
//
//   Manage the entrance system to enter the maze at various locations
//
//------------------------------------------------------------------

// include groups

#include <grpstd.h>
#include <grpsql.h>

#include <entrance.h>

//-------------------------------------------------------------------
//                        Class Definition
//-------------------------------------------------------------------

Entrance::Entrance ( void )
{

   strcpy ( p_name, "");
   p_x = 0;
   p_y = 0;
   p_z = 0;
   p_face = 0;
   p_unlocked = false;

   strcpy ( p_tablename, "entrance" );
}

Entrance::~Entrance ( void )
{

}

//-------------------------------------------------------------------
//                        Virtual Methods
//-------------------------------------------------------------------

bool Entrance::unlocked ( void )
{
   if ( p_unlocked == 1)
      return ( true );
   else
      return ( false );
}

void Entrance::unlocked ( bool value )
{
   if ( value == true )
      p_unlocked = 1;
   else
      p_unlocked = 0;
}

//-------------------------------------------------------------------
//                        Virtual Methods
//-------------------------------------------------------------------


void Entrance::sql_to_obj (void)
{
   p_primary_key = SQLcolumn_int (0);
   strncpy ( p_name, SQLcolumn_text(1), Entrance_NAME_STRLEN );
   p_x = SQLcolumn_int(2);
   p_y = SQLcolumn_int(3);
   p_z = SQLcolumn_int(4);
   p_face = SQLcolumn_int(5);
   p_unlocked = SQLcolumn_int(6);

}

void Entrance::template_sql_to_obj (void)
{
   // this is not used
}

void Entrance::obj_to_sqlupdate (void)
{
   sprintf (p_querystr, "name='%s', x=%d, y=%d, z=%d, face=%d, unlocked=%d",
            p_name,
            p_x,
            p_y,
            p_z,
            p_face,
            p_unlocked );
}

void Entrance::obj_to_sqlinsert (void)
{


   sprintf (p_querystr, "'%s', %d, %d, %d, %d, %d",
            p_name,
            p_x,
            p_y,
            p_z,
            p_face,
            p_unlocked );


}
