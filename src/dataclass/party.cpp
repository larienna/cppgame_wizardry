/***************************************************************************/
/*                                                                         */
/*                            P A R T Y . C P P                            */
/*                            Class Source code                            */
/*                                                                         */
/*     Content : Class PArty source code                                   */
/*     Programmer : Eric Pietrocupo                                        */
/*     Stating Date : May 1st, 2002                                        */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
//#include <grpengine.h>

#include <mazetile.h>
#include <encountr.h>
//#include <maze.h> // try to remove

/*
//#include <time.h>
#include <allegro.h>
//#include <datafile.h>
//#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>


//
//
//
//
//
//
//#include <list.h>
#include <opponent.h>
#include <charactr.h>
#include <ennemy.h>
#include <party.h>
//
//#include <game.h>
//#include <city.h>
#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>

*/


/*-------------------------------------------------------------------------*/
/*-                    Constructor & Destructor                           -*/
/*-------------------------------------------------------------------------*/

Party::Party ( void )
{
   //int i;

   reset_position ();
   //destroy_all_spell ();

   //p_nb_character = 0;
   /*for ( i = 0 ; i < 6 ; i++ )
   {
      p_FKcharacter [ i ] = 0;
      p_combat_pos [ i ] = 0;
   }*/
 //  p_location.number ( 0 );
   p_status = Party_STATUS_UNDEFINED;
   p_gold = 0;
   p_formation = Party_FORMATION_BALANCED;

   //p_dblength = sizeof ( dbs_Party );
   //p_dbtableID = DBTABLE_PARTY;
//   p_player.number (0 );
//   p_game.number ( 0 );

/*   // to remove, spell icon testing
   for ( i = 0 ; i < Party_NB_SPELL ; i ++ )
      p_spell_duration [ i ] = 500;

   p_spell_duration [ Party_SPELL_FEAR ] = 0;
   p_spell_duration [ Party_SPELL_LIGHT ] = 0;*/

   strcpy (p_tablename, "party");

}

/*Party::Party ( const Party &tmpobj )
{
   int i;
   DBObject ( dynamic_cast<DBObject*>(tmpobj) );

   for ( i = 0 ; i < 6 ; i++ )
   {
      p_FKcharacter [ i ] = tmpobj.p_FKcharacter [ i ];
      p_combat_pos [ i ] = tmpobj.p_combat_pos [ i ];
   }

   p_pos = tmpobj.p_pos;
   for ( i = 0 ; i < Party_NB_SPELL ; i ++ )
      p_spell_duration [ i ] = tmpobj.p_spell_duration [ i ];

   p_nb_character = tmpobj.p_nb_character;
//   p_player = tmpobj.p_player;
//   p_game = tmpobj.p_game;

} */

Party::~Party ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                        Property Methods                               -*/
/*-------------------------------------------------------------------------*/

/*string& Party::name ( void )
{
   return ( p_name );
} */

/*const char* Party::name ( void )
{
   return ( p_name );
}

void Party::name ( const char *str )
{
   strncpy ( p_name, str, 31 );
}*/

/*int Party::FKcharacter ( int number )
{
   return ( p_FKcharacter [ number ] );
}*/

/*int Party::rank ( int number )
{
   return ( p_combat_pos [ number ] );
}

void Party::rank ( int number, int rankID )
{
   p_combat_pos [ number ] = rankID;
}*/

s_Party_position Party::position ( void )
{
   return ( p_pos );
}

void Party::position ( s_Party_position position )
{
   if ( position.z != p_pos.z )
      encount.build_table ( position.z, mazepalid );
   p_pos = position;
}

void Party::position ( int x, int y, int z, int facing )
{
   p_pos.x = x;
   p_pos.y = y;

   if ( z != p_pos.z )
      encount.build_table ( z, mazepalid );
   p_pos.z = z;
   p_pos.facing = facing;
}

void Party::position ( int x, int y, int z )
{
   p_pos.x = x;
   p_pos.y = y;

   if ( z != p_pos.z )
      encount.build_table ( z, mazepalid );

   p_pos.z = z;
}

int Party::facing ( void )
{
   return ( p_pos.facing );
}

void Party::facing ( int side )
{
   p_pos.facing = side;
}

int Party::status ( void )
{
   return ( p_status );
}

void Party::status ( int value )
{
   p_status = value;
}

/*int Party::nb_character ( void )
{
   return ( p_nb_character );
}*/

/*int Party::spell_duration ( int spellID )
{
   return ( p_spell_duration [ spellID ] );
}

void Party::spell_duration ( int spellID, int value )
{

   p_spell_duration [ spellID ] = value;
}*/

int Party::gold ( void )
{
   return (p_gold);
}

void Party::gold ( int value )
{
   p_gold =  value;
}

void Party::add_gold ( int value )
{
   p_gold += value;
}

void Party::remove_gold ( int value )
{
   p_gold -= value;

}

int Party::encounter ( void )
{
   return (p_encounter);
}

void Party::encounter ( int value )
{
   p_encounter = value;
}

int Party::formation ( void )
{
   return (p_formation);
}

void Party::formation ( int value )
{
   p_formation = value;
}


/*int Party::location ( void )
{
   return ( p_location );
}

void Party::location ( int value )
{
   p_location = value;
}*/


/*int Party::player ( void )
{
   return ( p_player );
}
void Party::player ( int value )
{
   p_player = value;
}
int Party::game ( void )
{
   return ( p_game );
}
void Party::game ( int value )
{
   p_game = value;
}

  */
/*-------------------------------------------------------------------------*/
/*-                             Methods                                   -*/
/*-------------------------------------------------------------------------*/

/*void Party::add_character ( int character )
{
   if ( p_nb_character < 6 )
   {
      p_FKcharacter [ p_nb_character ] = character;
      p_nb_character++;
   }
}

void Party::remove_character ( int number )
{
   p_FKcharacter [ number ] = 0;
   compact_characters ();
   p_nb_character--;
}

void Party::remove_all_character ( void )
{
   int i;

   for ( i = 0 ; i < 6 ; i++ )
      p_FKcharacter [ i ] = 0;

   p_nb_character = 0;

}*/

void Party::turn_left ( void )
{
   p_pos.facing--;

   if ( p_pos.facing < 0 )
      p_pos.facing = 3;

}

void Party::turn_right ( void )
{
   p_pos.facing++;

   if ( p_pos.facing > 3 )
      p_pos.facing = 0;

}

void Party::walk_forward ( void )
{
   switch ( p_pos.facing )
   {
      case Party_FACE_NORTH :
         p_pos.y++;
      break;

      case Party_FACE_EAST :
         p_pos.x++;
      break;

      case Party_FACE_SOUTH :
         p_pos.y--;
      break;

      case Party_FACE_WEST :
         p_pos.x--;
      break;
   }
   game.clock.add_minute ( 1 );
}

void Party::walk_backward ( void )
{
   switch ( p_pos.facing )
   {
      case Party_FACE_NORTH :
         p_pos.y--;
      break;

      case Party_FACE_EAST :
         p_pos.x--;
      break;

      case Party_FACE_SOUTH :
         p_pos.y++;
      break;

      case Party_FACE_WEST :
         p_pos.x++;
      break;
   }
}

void Party::move_down ( void )
{
   p_pos.z++;
}

void Party::move_up ( void )
{
   p_pos.z--;
}

/*bool Party::test_front_wall ( unsigned char solid )
{
   unsigned char mask = Maze_SOLID_NORTH_MASK;

   switch ( p_pos.facing )
   {
      case Maze_FACE_NORTH :
         mask = Maze_SOLID_NORTH_MASK;
      break;

      case Maze_FACE_EAST :
         mask = Maze_SOLID_EAST_MASK;
      break;

      case Maze_FACE_SOUTH :
         mask = Maze_SOLID_SOUTH_MASK;
      break;

      case Maze_FACE_WEST :
         mask = Maze_SOLID_WEST_MASK;
      break;
   }

  if ( ( solid & mask ) > 0 )
     return ( true );
  else
     return ( false );

}

bool Party::test_front_door ( unsigned char solid )
{
   unsigned char mask = Maze_SOLID_NORTH_DOOR;

   switch ( p_pos.facing )
   {
      case Maze_FACE_NORTH :
         mask = Maze_SOLID_NORTH_DOOR;
      break;

      case Maze_FACE_EAST :
         mask = Maze_SOLID_EAST_DOOR;
      break;

      case Maze_FACE_SOUTH :
         mask = Maze_SOLID_SOUTH_DOOR;
      break;

      case Maze_FACE_WEST :
         mask = Maze_SOLID_WEST_DOOR;
      break;
   }

  if ( ( solid & mask ) == mask )
     return ( true );
  else
     return ( false );
}*/


/*void Party::decrement_duration ( short seconds )
{
   int i;

   for ( i = 0 ; i < Party_NB_SPELL ; i++ )
   {
      p_spell_duration [ i ] = p_spell_duration [ i ] - seconds;
      if ( p_spell_duration [ i ] < 0 )
         p_spell_duration [ i ] = 0;
   }

}

void Party::destroy_spell ( int spell_index )
{
   p_spell_duration [ spell_index ] = 0;
}

void Party::destroy_all_spell ( void )
{
   int i;

   for ( i = 0 ; i < Party_NB_SPELL ; i++ )
      p_spell_duration [ i ] = 0;

}*/

void Party::reset_position ( void )
{
   p_pos.x = 0;
   p_pos.y = 0;
   p_pos.z = 0;
   p_pos.facing = Party_FACE_NORTH;
}

/*void Party::rest ( int nb_days, int room_type )
{
   Character tmpchar;
   int i;

   for ( i = 0 ; i < p_nb_character ; i++ )
   {
      tmpchar.SQLselect ( p_FKcharacter [ i ] );
      tmpchar.rest ( nb_days, room_type );
      tmpchar.SQLupdate();
   }
}

void Party::autoability ( void )
{
   Character tmpchar;
   int i;

   for ( i = 0 ; i < p_nb_character ; i++ )
   {
      tmpchar.SQLselect ( p_FKcharacter [ i ] );
      tmpchar.autoability ();
      tmpchar.SQLupdate();
   }
}

int Party::combat_rank ( int characterID, int side )
{
   return ( 0 );
}*/

/*void Party::activesort_characters ( void )
{
   int i;
   int j;
   int tmptag;
   Character tmpopp1; // sort also ennemies
   Character tmpopp2; // using opponent instead does not work
   int maxi = p_nb_character - 1;
   int maxj = p_nb_character;

   for ( i = 0 ; i < maxi ; i++ )
   {
      for ( j = i + 1 ; j < maxj ; j++ )
      {
//      if ( p_FKcharacter [ i ] != 0 )
//      {

         tmpopp1.SQLselect ( p_FKcharacter [ i ] );
         tmpopp2.SQLselect ( p_FKcharacter [ j ] );

         textprintf_old ( buffer, font, 0, 0, General_COLOR_TEXT,
            "Character I:%d J:%d Ia:%d Ja:%d", i, j , tmpopp1.is_active(),
            tmpopp2.is_active() );
         textprintf_old ( buffer, font, 0, 16, General_COLOR_TEXT,
            "#:%d maxJ:%d maxJ:%d Is:%d Js:%d", p_nb_character, maxi, maxj ,tmpopp1.status(),
               tmpopp2.status() );
         textprintf_old ( buffer, font, 0, 32, General_COLOR_TEXT,
            "I : %s  %d  %s", tmpopp1.name(), tmpopp1.level(),
               STR_OPP_STATUS [ tmpopp1.status() ] );
         textprintf_old ( buffer, font, 0, 48, General_COLOR_TEXT,
            "J : %s  %d  %s", tmpopp2.name(), tmpopp2.level(),
               STR_OPP_STATUS [ tmpopp2.status() ] );


         copy_buffer();
         while ( ( readkey() >> 8 ) != KEY_ENTER );

         if ( tmpopp1.is_active() == false && tmpopp2.is_active() == true )
         {
//            debug ( "swap" );
            tmptag = p_FKcharacter [ i ];
            p_FKcharacter [ i ] = p_FKcharacter [ j ];
            p_FKcharacter [ j ] = tmptag;
         }
      }
   }
}*/


/*void Party::ennemy_rankprioritysort ( void )
{
   int i;
   int j;
   int tmptag;
   Ennemy tmpopp1;
   Ennemy tmpopp2;
   int maxi = p_nb_character - 1;
   int maxj = p_nb_character;

   for ( i = 0 ; i < maxi ; i++ )
   {
      for ( j = i + 1 ; j < maxj ; j++ )
      {
         tmpopp1.SQLselect ( p_FKcharacter [ i ] );
         tmpopp2.SQLselect ( p_FKcharacter [ j ] );

         if ( tmpopp1.rankpriority() > tmpopp2.rankpriority() )
         {
            tmptag = p_FKcharacter [ i ];
            p_FKcharacter [ i ] = p_FKcharacter [ j ];
            p_FKcharacter [ j ] = tmptag;
         }
      }
   }

}*/

/*-------------------------------------------------------------------------*/
/*-                        Private Methods                                -*/
/*-------------------------------------------------------------------------*/

/*void Party::compact_characters ( void )
{

   int i;
   int j;

   for ( i = 0 ; i < 5 ; i++ )
   {
      if ( p_FKcharacter [ i ] == 0 )
      {
         j = i + 1;
         while ( p_FKcharacter [ j ] == 0 && j < 6 )
            j++;

         if ( p_FKcharacter [ j ] != 0 )
         {
            p_FKcharacter [ i ] = p_FKcharacter [ j ];
            p_FKcharacter [ j ] = 0;
         }
      }
   }
}*/

bool Party::is_functional ( void )
{
   bool retval = false;
   int answer;
   //char tmpstr[31];
   Character tmpchar;

   answer = tmpchar.SQLpreparef ( "WHERE FKparty=%d", primary_key() );

   if ( answer == SQLITE_OK  )
   {
      answer = tmpchar.SQLstep();
      //debug_printf( __func__, "After step 1: answer=%d", answer);

      while ( answer == SQLITE_ROW )
      {
         if ( tmpchar.body() == Character_BODY_ALIVE )
            retval = true;
         answer = tmpchar.SQLstep();
     //    debug_printf( __func__, "After step 2: answer=%d", answer);
      }
   }
   tmpchar.SQLfinalize();
   //debug_printf( __func__, "After Finalize");

   return ( retval );
}

//void Party::display_party_frame ( void )
//{
   // maybe set in window show all or something high use to avoid
   // calling it everywhere ( only not in game is the time not to call)
   // after maze drawing there might not be any windows showall.

//   if ( config.get ( Config_DISPLAY_PARTY_FRAME) == Config_PAR_YES )
//   {
      // check if game started before draw


 //  }

//}

/*-------------------------------------------------------------------------*/
/*-                        Virtual Methods                                -*/
/*-------------------------------------------------------------------------*/

// object relational virtual methods
void Party::sql_to_obj (void)
{
   //int i;

   p_primary_key = SQLcolumn_int (0);
   p_status = SQLcolumn_int (1);
   p_gold = SQLcolumn_int (2);
   p_encounter = SQLcolumn_int (3);
   p_pos.x = SQLcolumn_int (4);
   p_pos.y = SQLcolumn_int (5);
   p_pos.z = SQLcolumn_int (6);
   p_pos.facing = SQLcolumn_int (7);
   p_formation = SQLcolumn_int (8);
   /*for (i=0; i < Party_NB_SPELL ; i++)
   {
      p_spell_duration [ i ]= SQLcolumn_int ( i + 8 );
   }*/

}

void Party::template_sql_to_obj (void)
{

   // will not be used
}

void Party::obj_to_sqlupdate (void)
{
   sprintf (p_querystr, "status=%d, gold=%d, encounter=%d, x=%d, y=%d, z=%d, facing=%d, formation=%d",
             p_status,
             p_gold,
             p_encounter,
             p_pos.x,
             p_pos.y,
             p_pos.z,
             p_pos.facing,
             p_formation );
}

void Party::obj_to_sqlinsert (void)
{

   sprintf (p_querystr, "%d, %d, %d, %d, %d, %d, %d, %d",
             p_status,
             p_gold,
             p_encounter,
             p_pos.x,
             p_pos.y,
             p_pos.z,
             p_pos.facing,
             p_formation );

}

/*
int Party::SQLinsert ( void )
{
   dbs_Party tmpdat;
   int tmptag;
   int i;

   strncpy ( tmpdat.name, p_name.data(), 30 );
   tmpdat.pos.x = p_pos.x;
   tmpdat.pos.y = p_pos.y;
   tmpdat.pos.z = p_pos.z;
   tmpdat.pos.facing = p_pos.facing;
   tmpdat.nb_character = p_nb_character;
//   tmpdat.player = p_player.number();
//   tmpdat.game = p_game.number();

   for ( i = 0 ; i < 6 ; i++ )
   {
      tmpdat.character [ i ] = p_FKcharacter [ i ].number();
      tmpdat.combat_pos [ i ] = p_combat_pos [ i ];
   }
   for ( i = 0 ; i < Party_NB_SPELL ; i++ )
      tmpdat.spell_duration [ i ]= p_spell_duration [ i ];

   tmptag = db.insert ( DBTABLE_PARTY,
      static_cast<void*>(&tmpdat), sizeof ( dbs_Party ) );
   end_insert ( tmptag );

   return ( tmptag );
}

void Party::SQLupdate ( void )
{
   if ( p_readonly == false )
   {
      dbs_Party tmpdat;
      int i;

      strncpy ( tmpdat.name, p_name.data(), 30 );
      tmpdat.pos.x = p_pos.x;
      tmpdat.pos.y = p_pos.y;
      tmpdat.pos.z = p_pos.z;
      tmpdat.pos.facing = p_pos.facing;
      tmpdat.nb_character = p_nb_character;
//      tmpdat.player = p_player.number();
//      tmpdat.game = p_game.number();

      for ( i = 0 ; i < 6 ; i++ )
      {
         tmpdat.character [ i ] = p_FKcharacter [ i ].number();
         tmpdat.combat_pos [ i ] = p_combat_pos [ i ];
      }
      for ( i = 0 ; i < Party_NB_SPELL ; i++ )
         tmpdat.spell_duration [ i ]= p_spell_duration [ i ];

      db.update ( p_reftag,
         static_cast<void*>(&tmpdat), sizeof ( dbs_Party ) );
      end_update();
   }

}

void Party::SQLselect ( int value, bool readonly )
{
   dbs_Party tmpdat;
   if ( db.select ( &tmpdat, value, sizeof ( dbs_Party ) ) == true )
   {
      int i;

      p_name = tmpdat.name;
      p_pos.x = tmpdat.pos.x;
      p_pos.y = tmpdat.pos.y;
      p_pos.z = tmpdat.pos.z;
      p_pos.facing = tmpdat.pos.facing;
      p_nb_character = tmpdat.nb_character;
//      p_player.number( tmpdat.player );
//      p_game.number( tmpdat.game );


      for ( i = 0 ; i < 6 ; i++ )
      {
         p_FKcharacter [ i ].number(tmpdat.character [ i ]);
         p_combat_pos [ i ] = tmpdat.combat_pos [ i ];
      }
      for ( i = 0 ; i < Party_NB_SPELL ; i++ )
         p_spell_duration [ i ] = tmpdat.spell_duration [ i ];

      end_select ( value, readonly );
   }
}

void Party::SQLselect ( unsigned int index, bool readonly )
{
   dbs_Party tmpdat;
   if ( db.select ( &tmpdat, index, sizeof ( dbs_Party ) ) == true )
   {
      int i;

      p_name = tmpdat.name;
      p_pos.x = tmpdat.pos.x;
      p_pos.y = tmpdat.pos.y;
      p_pos.z = tmpdat.pos.z;
      p_pos.facing = tmpdat.pos.facing;
      p_nb_character = tmpdat.nb_character;
//      p_player.number( tmpdat.player );
//      p_game.number( tmpdat.game );

      for ( i = 0 ; i < 6 ; i++ )
      {
         p_FKcharacter [ i ].number(tmpdat.character [ i ]);
         p_combat_pos [ i ] = tmpdat.combat_pos [ i ];
      }
      for ( i = 0 ; i < Party_NB_SPELL ; i++ )
         p_spell_duration [ i ] = tmpdat.spell_duration [ i ];

      end_select ( db.get_tag ( index ), readonly );
   }
}

void Party::DBremove ( void )
{
   db.remove ( p_reftag );
   end_remove();
}

void Party::exDBselect ( Database &datb, unsigned int index, bool readonly = false )
{
   dbs_Party tmpdat;
   if ( datb.select ( &tmpdat, index, sizeof ( dbs_Party ) ) == true )
   {
      int i;

      p_name = tmpdat.name;
      p_pos.x = tmpdat.pos.x;
      p_pos.y = tmpdat.pos.y;
      p_pos.z = tmpdat.pos.z;
      p_pos.facing = tmpdat.pos.facing;
      p_nb_character = tmpdat.nb_character;
//      p_player.number( tmpdat.player );
//      p_game.number( tmpdat.game );

      for ( i = 0 ; i < 6 ; i++ )
      {
         p_FKcharacter [ i ].number(tmpdat.character [ i ]);
         p_combat_pos [ i ] = tmpdat.combat_pos [ i ];
      }
      for ( i = 0 ; i < Party_NB_SPELL ; i++ )
         p_spell_duration [ i ] = tmpdat.spell_duration [ i ];

      end_select ( datb.get_tag ( index ), readonly );
   }

}

void Party::exDBremove ( Database &datb )
{
   datb.remove ( p_reftag );
   end_remove();
}
                                                            */

/*
void Party::objdat_to_strdat ( void *dataptr )
{
   dbs_Party &tmpdat = *(static_cast<dbs_Party*> ( dataptr ));

   int i;

   strncpy ( tmpdat.name, p_name, 31 );
   tmpdat.pos.x = p_pos.x;
   tmpdat.pos.y = p_pos.y;
   tmpdat.pos.z = p_pos.z;
   tmpdat.pos.facing = p_pos.facing;
   tmpdat.nb_character = p_nb_character;
   tmpdat.location = p_location.number();
   tmpdat.status = p_status;
//   tmpdat.player = p_player.number();
//   tmpdat.game = p_game.number();

   for ( i = 0 ; i < 6 ; i++ )
   {
      tmpdat.character [ i ] = p_FKcharacter [ i ].number();
//      tmpdat.combat_pos [ i ] = p_combat_pos [ i ];
   }
   for ( i = 0 ; i < Party_NB_SPELL ; i++ )
      tmpdat.spell_duration [ i ]= p_spell_duration [ i ];

}

void Party::strdat_to_objdat ( void *dataptr )
{
   dbs_Party &tmpdat = *(static_cast<dbs_Party*> ( dataptr ));

      int i;

      strcpy ( p_name, tmpdat.name );
      p_pos.x = tmpdat.pos.x;
      p_pos.y = tmpdat.pos.y;
      p_pos.z = tmpdat.pos.z;
      p_pos.facing = tmpdat.pos.facing;
      p_nb_character = tmpdat.nb_character;
      p_location.number ( tmpdat.location );
      p_status = tmpdat.status;
//      p_player.number( tmpdat.player );
//      p_game.number( tmpdat.game );

      for ( i = 0 ; i < 6 ; i++ )
      {
         p_FKcharacter [ i ].number(tmpdat.character [ i ]);
//         p_combat_pos [ i ] = tmpdat.combat_pos [ i ];
      }
      for ( i = 0 ; i < Party_NB_SPELL ; i++ )
         p_spell_duration [ i ] = tmpdat.spell_duration [ i ];

}

void Party::child_DBremove ( void )
{
   // no child to remove
}
*/

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

Party party;

const char STR_PAR_STATUS [] [ 12 ] =
{
   {"Unknown"},
   {"City"},
   {"Maze"},
   {"Camp"},
   {"Encounter"},
   {"Combat"}
};

const char STR_PAR_FORMATION [] [ 12 ] =
{
   {"Offensive"},
   {"Balanced"},
   {"Defensive"}
};

const char STR_PAR_FACING [ 4 ] [ 6 ] =
{
   {"North"},
   {"East"},
   {"South"},
   {"West"}
};

int const Party_FORMATION_POSITION [ 3 ] [ 6 ] [ 6 ] =
{
  //Party_POSITION_OFFENSIVE
  {
     { Party_FRONT, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 1 character
     { Party_FRONT, Party_FRONT, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 2 character
     { Party_FRONT, Party_FRONT, Party_BACK, Party_NONE, Party_NONE, Party_NONE }, // 3 character
     { Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_NONE, Party_NONE }, // 4 character
     { Party_FRONT, Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_NONE }, // 5 character
     { Party_FRONT, Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_BACK }, // 6 character
  },
  //Party_POSITION_BALANCED
  {
     { Party_FRONT, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 1 character
     { Party_FRONT, Party_BACK, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 2 character
     { Party_FRONT, Party_FRONT, Party_BACK, Party_NONE, Party_NONE, Party_NONE }, // 3 character
     { Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_NONE, Party_NONE }, // 4 character
     { Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_NONE }, // 5 character
     { Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_BACK }, // 6 character
  },
  //Party_POSITION_DEFENSIVE
  {
     { Party_FRONT, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 1 character
     { Party_FRONT, Party_BACK, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 2 character
     { Party_FRONT, Party_BACK, Party_BACK, Party_NONE, Party_NONE, Party_NONE }, // 3 character
     { Party_FRONT, Party_BACK, Party_BACK, Party_BACK, Party_NONE, Party_NONE }, // 4 character
     { Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_BACK, Party_NONE }, // 5 character
     { Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_BACK, Party_BACK }, // 6 character
  }
};

const char Party_STR_RANK [ 3 ] [ 8 ] =
{
   // Party_NONE
   "-----",
   // Party_FRONT
   "Front",
   // Party_BACK
   "Back"
};
