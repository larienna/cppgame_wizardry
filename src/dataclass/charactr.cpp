/***************************************************************************/
/*                                                                         */
/*                          C H A R A C T R . C P P                        */
/*                                                                         */
/*                            Class source code                            */
/*                                                                         */
/*     Content : Class Character source code                               */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : April 26, 2002                                      */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h> //<tmpinc>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <option.h>
#include <config.h>
//#include <charactr.h>


/*
#include <allegro.h>






//#include <time.h>

//#include <datafile.h>
//#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>







//#include <list.h>

#include <cclass.h>
#include <opponent.h>
#include <charactr.h>
#include <ennemy.h>
//#include <party.h>
//
#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
//#include <window.h>   // to comment
//#include <winmessa.h> // to comment
*/


//int Character::p_levelup_speed = Game_LEVELUP_NORMAL;

bool Character::p_recompile_ranks = false;

/*-------------------------------------------------------------------------*/
/*-                       Constructor and destructor                      -*/
/*-------------------------------------------------------------------------*/

Character::Character ( void )
{
   clean ();

  // p_dblength = sizeof ( dbs_Character );
  // p_dbtableID = DBTABLE_CHARACTER;
  // p_type = Character_TYPE_CHARACTER;

   // new initialization
   //p_FKrace = -1;
   //p_FKcclass = -1;
   //p_FKparty = -1;
   p_portraitid = 0;
   strcpy (p_tablename, "character");

   clear_stat();
}


Character::~Character ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                       Property Method                                 -*/
/*-------------------------------------------------------------------------*/


char* Character::name ( void )
{
   return ( p_name );
}

void Character::name ( const char *str )
{
   strncpy ( p_name, str, Character_NAME_STRLEN );
}

int Character::max_HP ( void )
{
   return ( p_max_HP );
}

int Character::current_HP ( void )
{
   return ( p_current_HP );
}

int Character::max_MP ( void )
{
   return ( p_max_MP );
}

int Character::current_MP ( void )
{
   return ( p_current_MP );
}

void Character::attribute ( int attribID, int value )
{

   p_attribute [ attribID ] = value;
}

int Character::attribute ( int attribID )
{
   return ( p_attribute [ attribID ] );
}

int Character::attribute_modifier ( int attribID )
{
   int tmpval;

   if ( p_attribute [ attribID ] > 10 )
   {
      tmpval = p_attribute [ attribID ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ attribID ];
      return ( tmpval / 2 );
   }
}

void Character::strength ( int value )
{
   p_attribute [ Character_STRENGTH ] = value;
}

int Character::strength ( void )
{
   return ( p_attribute [ Character_STRENGTH ] );
}

int Character::STRmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Character_STRENGTH ] > 10 )
   {
      tmpval = p_attribute [ Character_STRENGTH ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Character_STRENGTH ];
      return ( tmpval / 2 );
   }
}

void Character::dexterity ( int value )
{

   p_attribute [ Character_DEXTERITY ] = value;
}

int Character::dexterity ( void )
{
   return ( p_attribute [ Character_DEXTERITY ] );
}

int Character::DEXmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Character_DEXTERITY ] > 10 )
   {
      tmpval = p_attribute [ Character_DEXTERITY ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Character_DEXTERITY ];
      return ( tmpval / 2 );
   }
}


void Character::endurance ( int value )
{

   p_attribute [ Character_ENDURANCE ] = value;
}

int Character::endurance ( void )
{
   return ( p_attribute [ Character_ENDURANCE ] );
}

int Character::ENDmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Character_ENDURANCE ] > 10 )
   {
      tmpval = p_attribute [ Character_ENDURANCE ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Character_ENDURANCE ];
      return ( tmpval / 2 );
   }

}


void Character::intelligence ( int value )
{

   p_attribute [ Character_INTELLIGENCE ] = value;
}

int Character::intelligence ( void )
{
   return ( p_attribute [ Character_INTELLIGENCE ] );
}

int Character::INTmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Character_INTELLIGENCE ] > 10 )
   {
      tmpval = p_attribute [ Character_INTELLIGENCE ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Character_INTELLIGENCE ];
      return ( tmpval / 2 );
   }

}


void Character::cunning ( int value )
{

   p_attribute [ Character_CUNNING ] = value;
}

int Character::cunning ( void )
{
   return ( p_attribute [ Character_CUNNING ] );
}

int Character::CUNmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Character_CUNNING ] > 10 )
   {
      tmpval = p_attribute [ Character_CUNNING ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Character_CUNNING ];
      return ( tmpval / 2 );
   }

}


void Character::willpower ( int value )
{

   p_attribute [ Character_WILLPOWER ] = value;
}

int Character::willpower ( void )
{
   return ( p_attribute [ Character_WILLPOWER ] );
}

int Character::WILmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Character_WILLPOWER ] > 10 )
   {
      tmpval = p_attribute [ Character_WILLPOWER ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Character_WILLPOWER ];
      return ( tmpval / 2 );
   }
}

int Character::level ( void )
{
   return ( p_level );
}

int Character::exp ( void )
{
   return ( p_exp );
}


int Character::soul ( void )
{
   return ( p_soul );
}

/*bool Character::health ( unsigned int healthID )
{
   if ( p_health & healthID > 0 )
      return ( true );
   else
      return ( false );
}

unsigned int Character::health ( void )
{
   return ( p_health );
}*/

int Character::body ( void )
{
   return ( p_body );
}

void Character::body ( int value )
{
   p_body = value;
   p_recompile_ranks = true;

   if ( p_body != Character_BODY_ALIVE )
      ActiveEffect::trigger_expiration_by_death ( this );

   if ( p_body > Character_BODY_PETRIFIED )
      p_current_HP = 0;
}

int Character::location ( void )
{
   return ( p_location );
}

void Character::location ( int value )
{
   p_location = value;
}

int Character::position ( void )
{
   return ( p_position );
}

void Character::position ( int value )
{
   if ( p_body > Character_BODY_ALIVE )
      //p_position = 4294967296;
      p_position = 2147483647;
   else
      p_position = value;
}

void Character::FKrace ( int key )
{
   p_FKrace = key;
}

int Character::FKrace ( void )
{
   return ( p_FKrace );
}

void Character::FKcclass ( int value )
{
   p_FKcclass = value;
}

int Character::FKcclass ( void )
{
   return ( p_FKcclass );
}

int Character::FKparty ( void )
{
   return ( p_FKparty );
}

void Character::FKparty ( int value )
{
   p_FKparty = value;
}

int Character::rank ( void )
{
   return ( p_rank );
}

void Character::rank ( int value )
{
   p_rank = value;
}


/*int Character::FKequiped ( int index )
{
   return ( p_FKequiped [ index ] );
}

void Character::FKequiped ( int index, int key )
{
   p_FKequiped [ index ] = key;
}*/


bool Character::is_active ( void )
{
   bool retval = true;

   //?? need to use the health status table next time the status changes.

/*   if ( (  p_health & Character_HEALTH_ASLEEP ) > 0 )
      retval = false;

   if ( (  p_health & Character_HEALTH_PARALYSED ) > 0 )
      retval = false;

   if ( (  p_health & Character_HEALTH_PETRIFIED ) > 0 )
      retval = false;*/

   if ( p_body != Character_BODY_ALIVE )
      retval = false;

   return ( retval );

}

bool Character::is_equipable ( int key )
{
   bool retval = true;
   Item tmpitem;
   CClass tmpclass;

   tmpitem.SQLselect( key );
   tmpclass.SQLselect ( p_FKcclass );

   //printf ("debug: Character: Is_equipable:\n Character=%s, class=%s, item=%s, itemprof=%d, classprof=%d\n"
   //        , p_name, tmpclass.name(), tmpitem.name(), tmpitem.profiency(), tmpclass.profiency());

   if ( ( tmpitem.profiency() & tmpclass.profiency() ) == 0 )
      retval = false;

   if ( tmpitem.type() == Item_TYPE_ACCESSORY )
      retval=true;

   if ( tmpitem.type() == Item_TYPE_EXPANDABLE )
      retval=true;

//printf ("Classname=%s", tmpclass.name());

   //printf ( "category=%d  ", tmpitem.category());
   /*switch ( tmpitem.type() )
   {
      case Item_TYPE_WEAPON:
         if ( ( tmpitem.category() & tmpclass.weapon_profiency() ) == 0)
            retval = false;
      //printf ( "Item Cat: %d, Weapon Prof: %d\r\n", tmpitem.category(), tmpclass.weapon_profiency());
      break;
      case Item_TYPE_SHIELD:
         if ( ( tmpitem.category() & tmpclass.shield_profiency() ) == 0)
            retval = false;
      //printf ( "Item Cat: %d, Shield Prof: %d\r\n", tmpitem.category(), tmpclass.shield_profiency());
      break;
      case Item_TYPE_ARMOR:
         if ( ( tmpitem.category() & tmpclass.armor_profiency() ) == 0)
            retval = false;
      //printf ( "Item Cat: %d, Armor Prof: %d\r\n", tmpitem.category(), tmpclass.armor_profiency());
      break;
   }*/
   //printf ("\r\n");

   return ( retval );

}


int Character::resistance ( void ) // combine resistance of class and Race
{
   CClass tmpclass;
   Race tmprace;
   int tmpresistance;
   Item tmpitem;
   int error;

   tmpclass.SQLselect ( p_FKcclass );
   tmprace.SQLselect ( p_FKrace );

   tmpresistance  = tmpclass.resistance() | tmprace.resistance();

   error = tmpitem.SQLpreparef ( "WHERE loctype=%d AND lockey=%d AND NOT element=0 AND ( type=%d or type=%d or type=%d)",
                             Item_LOCATION_CHARACTER, p_primary_key, Item_TYPE_SHIELD, Item_TYPE_ARMOR, Item_TYPE_ACCESSORY );
   if ( error == SQLITE_OK )
   {
      error = tmpitem.SQLstep();

      if ( error == SQLITE_ROW)
      {
         tmpresistance = tmpresistance | tmpitem.element();

         error = tmpitem.SQLstep();
      }

      tmpitem.SQLfinalize();
   }

   return ( tmpresistance );
}

int Character::weakness ( void ) // combine weakness of class and Race
{
   CClass tmpclass;
   Race tmprace;

   tmpclass.SQLselect ( p_FKcclass );
   tmprace.SQLselect ( p_FKrace );

   return ( (tmpclass.weakness() | tmprace.weakness()));
}



/*void Character::raceS ( char *str )
{
   Race tmprace;

   if ( p_FKrace != -1 )
   {
      tmprace.SQLselect ( p_FKrace );
      strcpy ( str, tmprace.name() );
   }
   else
      strcpy ( str, "" );



}

void Character::cclassS ( char* str )
{
   CClass tmpcclass;

   if ( p_FKcclass != -1 )
   {
      tmpcclass.SQLselect ( p_FKcclass );
      strcpy ( str, tmpcclass.name() );
   }
   else
      strcpy ( str, "" );

};*/




/*-------------------------------------------------------------------------*/
/*-                           Methods                                     -*/
/*-------------------------------------------------------------------------*/

void Character::recover_HP ( int value )
{
   if ( value < 0)
      value = 0;

   if ( p_body == Character_BODY_ALIVE)
   {
      p_current_HP = p_current_HP + value;
      if ( p_current_HP > p_max_HP )
         p_current_HP = p_max_HP;
      if ( p_current_HP > 0 )
         p_body = Character_BODY_ALIVE;

      if ( p_current_HP < 0)
         p_current_HP = 0;
   }
}

void Character::recover_MP ( int value )
{
   if ( value < 0)
      value = 0;

   p_current_MP = p_current_MP + value;
   if ( p_current_MP > p_max_MP )
      p_current_MP = p_max_MP;

   if ( p_current_MP < 0)
      p_current_MP = 0;

}

void Character::recover_soul ( int value )
{
   p_soul = p_soul + value;
   if ( p_soul > 100 )
      p_soul = 100;
}

void Character::lose_HP ( int value )
{
   p_current_HP = p_current_HP - value;
   if ( p_current_HP <= 0 )
   {
      p_current_HP = 0;
      p_body = Character_BODY_DEAD;
      p_recompile_ranks = true;
      ActiveEffect::trigger_expiration_by_death ( this );
   }
}



void Character::lose_MP ( int value )
{
   p_current_MP = p_current_MP - value;
   if ( p_current_MP < 0 )
      p_current_MP = 0;

}

void Character::lose_soul ( int value )
{
   p_soul = p_soul - value;
   if ( p_soul <= 0 )
   {
      p_soul = 0;
      p_body = Character_BODY_DELETED;
      p_recompile_ranks = true;
   }
}

/*void Character::add_health ( unsigned int healthID )
{

   p_health = p_health | healthID;

}

void Character::remove_health ( unsigned int healthID )
{
   p_health = p_health & ( !healthID );
}*/


void Character::clean ( void )
{
   int i;
   //int j;

   strcpy ( p_name,"");

   for ( i = 0 ; i < Character_NB_ATTRIBUTE ; i++ )
      p_attribute [ i ] = 0;

   p_level = 0;

   p_exp = 0;
   p_max_HP = 0;
   p_current_HP = 0;
   p_max_MP = 0;
   p_current_MP = 0;
   p_soul = 0;
   //p_health = 0;
   p_body = -1;
   p_location = 0;
   p_position = 0;
   p_FKrace = -1;
   p_FKcclass = -1;
   p_FKparty = -1;
   p_rank = 0;

   /*for ( i = 0 ; i < Character_EQUIP_SIZE ; i++ )
      p_FKequiped [ i ] =  -1;*/



}

void Character::finalize_newchar_stats ( void )
{
   //int i;

   //note: level must be raise first, this is just a safety.
   if ( p_level == 0)
      raise_level();

   if (p_level > 1)
      p_exp = EXP_TABLE [ p_level - 2 ];
   else
      p_exp = 0;

   p_soul = 100;
   //p_health = 0;
   p_body = Character_BODY_ALIVE;

   p_current_HP = p_max_HP;
   p_current_MP = p_max_MP;

   p_location = Character_LOCATION_RESERVE;
   p_position = 0;
}

void Character::init_race_attribute ( void )
{
   int i;
   Race tmprace;

   if ( p_FKrace != -1)
   {

      tmprace.SQLselect ( p_FKrace );

      for ( i = 0 ; i < 6 ; i++ )
         p_attribute [ i ] = tmprace.attribute ( i );
   }
   // set sex attributes

/*   if ( p_sex == Character_FEMALE )
      p_attribute [ Character_CUNNING ]++;
   else
      p_attribute [ Character_STRENGTH ]++;*/

   /*p_attribute [ Character_LUCK ] = rnd (11) + 4;*/
}

void Character::autoability ( void )
{
//?? not sure if set like magik effect
}

/*unsigned int Character::use_ability ( int inventoryID )
{
   return ( 0 );
}*/



bool Character::check_levelup ( void )
{
   if ( p_level < Character_MAX_LEVEL ) // maximum level is 20
   {
      if ( p_exp >= EXP_TABLE [ p_level - 1 ]  )
         return ( true );
      else
         return ( false );
   }
   //printf ("%s has")
   return ( false );
}

int Character::next_level_exp ( void )
{
   if (p_level < Character_MAX_LEVEL)
      return ( EXP_TABLE [ p_level - 1 ] );
   else
      return ( 0 );
}


void Character::raise_level ( void )
{
   CClass tmpclass;
   int tmphpinc;
   int tmpmpinc;

   tmpclass.SQLselect ( p_FKcclass );


   tmphpinc = roll_xdypz ( 1, tmpclass.HPdice(), ENDmodifier() );
   tmpmpinc = roll_xdypz ( 1, tmpclass.MPdice(), WILmodifier() );

   p_max_HP += tmphpinc;
   p_max_MP += tmpmpinc;

   system_log.writef ("Character: %s raised a level: HP + %d ( 1D%d + %d ) | MP +%d ( 1D%d + %d )"
      , p_name, tmphpinc, tmpclass.HPdice(), ENDmodifier(), tmpmpinc, tmpclass.MPdice(), WILmodifier() );

   // raise soul by

   switch ( config.get (Config_SOUL_RECOVERY))
   {
      case Config_DIF_LOW:
         recover_soul ( 4 );
      break;
      case Config_DIF_NORMAL:
         recover_soul ( 2 );
      break;
      case Config_DIF_HIGH:
         recover_soul ( 1 );
      break;
   }

   // raise level
   p_level++;

   // raise HP
/* //tmp comment
   tmpval = hdice ( tmprace.HPdice() ) + ENDmodifier() - hppenalty;
   if ( tmpval < 2 )
      tmpval = 2;
   p_max_HP = p_max_HP + tmpval;
   p_current_HP = p_current_HP + tmpval;

   // raise MP
   tmpval = hdice ( tmprace.MPdice() ) + WILmodifier() - mppenalty;
   if ( tmpval < 2 )
      tmpval = 2;
   p_max_MP = p_max_MP + tmpval;
   p_current_MP = p_current_MP + tmpval;
*/


}

void Character::gain_exp ( int value )
{
   p_exp = p_exp + value;
   if ( p_exp > 2000000000L )
      p_exp = 2000000000L;
}


void Character::lose_exp ( int value )
{
   p_exp = p_exp - value;
   if ( p_exp < 0 )
      p_exp = 0;

}


void Character::rest ( int nb_days, int HPdice, int MPdice, bool fineroom )
{
   int i;
   //int j;
   //int k;
   //int levelmod;
   //Race tmprace;
   CClass tmpclass;
   int tmphplus;
   int tmpmplus;

   //tmpclass.SQLselect ( p_FKcclass );

   for ( i = 0 ; i < nb_days ; i++ )
   {
      if ( fineroom == true)
      {
         tmphplus = HPdice + ENDmodifier();
         tmpmplus = MPdice + WILmodifier();
      }
      else
      {
         tmphplus = roll_xdypz ( 1, HPdice, ENDmodifier() );
         tmpmplus = roll_xdypz ( 1, MPdice, WILmodifier() );
      }

      recover_HP ( tmphplus );
      recover_MP ( tmpmplus );

      system_log.writef ("Character: %s rest and gain: HP + %d ( 1D%d + %d ) | MP +%d ( 1D%d + %d )"
      , p_name, tmphplus, HPdice, ENDmodifier(), tmpmplus, MPdice, WILmodifier() );
   }

   //levelmod = ( p_level / 5 );
/*
   int tmpval;
   bool allocated;

   int recHP = 0;
   int recMP = 0;

   switch ( room_type )
   { //tmp comment
      case 0: // City_ROOMTYPE_COMMON
         recHP = tmprace.HPdice() + ENDmodifier() + levelmod;
         if ( recHP < 4 )
            recHP = 4;
         recMP = tmprace.MPdice() + WILmodifier() + levelmod;
         if ( recMP < 4 )
            recMP = 4;
      break;
      case 1: //City_ROOMTYPE_FINE
         recHP = tmprace.HPdice() + ENDmodifier() + levelmod;
         if ( recHP < 4 )
            recHP = 4;
         recHP += ( recHP / 2 );
         recMP = tmprace.MPdice() + WILmodifier() + levelmod;
         if ( recMP < 4 )
            recMP = 4;
         recMP += ( recMP / 2 );
      break;
   }*/


//   raise_days ( nb_days );
}

bool Character::check_command ( int i, unsigned int interface )
{
   //int i;
   bool answer = false;
   CClass tmpclass;
   int retval;
   Item tmpitem;
   int max_range = 0;
   char condition [100];

   tmpclass.SQLselect ( p_FKcclass );

   if ( ( interface & CmdProc_COMMANDLIST [ i ] . available) > 0 )
   // check if commande is availble for desired interface (camp or combat)
   {
      // ?todo?:  need to check for requirements

      switch ( CmdProc_COMMANDLIST [ i ] . requirement )
      {

      case CmdProc_REQUIREMENT_REACH :
      // assume that there will always be a monster at the front, so check if can at least target front
      // assume that multiple weapons can be equiped, just in case
         retval = tmpitem.SQLpreparef( "WHERE loctype=%d AND lockey=%d AND type=%d", Item_LOCATION_CHARACTER, primary_key(), Item_TYPE_WEAPON );
//printf ("pass 4-1\r\n");
         if ( retval == SQLITE_OK)
         {
            retval = tmpitem.SQLstep();

            while ( retval == SQLITE_ROW)
            {
               if ( tmpitem.range() > max_range)
                  max_range = tmpitem.range();

               retval = tmpitem.SQLstep();
            }


         }
         else
            tmpitem.SQLerrormsg();

         SQLfinalize();

         switch ( p_rank )
         {
            case Party_FRONT:
               if ( max_range >= 1 )
                  answer=true;
            break;
            case Party_BACK:
               if ( max_range >= 2 )
                  answer=true;
            break;
            case Party_NONE:
            default:
            break;
         }
         //printf ("%s Range=%d, rank=%d\n", p_name, max_range, p_rank );

         //if ( Party_FORMATION_POSITION [ party.formation() ] [party.nb_character()] [ character_id] )

         //todo, use max_range to know if in range. Problem, do not know the position of the
         // character inside the party to make the verification. use following table to check
         // if the character is in from or in the back
         // Party_FORMATION_POSITION [ party.formation() ] [nb_character] [ character_id]


      break;
      case CmdProc_REQUIREMENT_ITEM :
         sprintf ( condition, "WHERE loctype=%d and lockey=%d", Item_LOCATION_CHARACTER, p_primary_key );
         if ( SQLcount ("*", "item", condition ) > 0 )
            answer = true;
      // if have at least 1 equiped item, it enabled
      // maybe later could enable only if have usable items.
      break;
      case CmdProc_REQUIREMENT_SPELL :
         if ( ( tmpclass.magic_school() & CmdProc_COMMANDLIST [ i ] . value ) > 0 )
            answer=true;
      break;
      case CmdProc_REQUIREMENT_CLASS :
         if ( p_FKcclass == CmdProc_COMMANDLIST [ i ] . value)
            answer = true;
      break;

      case CmdProc_REQUIREMENT_RACE :
         if ( p_FKrace == CmdProc_COMMANDLIST [ i ] . value)
            answer = true;
      break;

      case CmdProc_REQUIREMENT_NONE :
      default :
         answer = true;
      break;

      }

   }

   return ( answer );

   /*int nb_cmd_added = 0;

   for ( i = 0; i < CmdProc_COMMANDLIST_SIZE ; i++ )
   {

      if ( ( interface & CmdProc_COMMANDLIST [ i ] . available) > 0 )
      {

         // ?todo?:  need to check for requirements

         tmplist.add_item ( i , CmdProc_COMMANDLIST [ i ] . name);
         nb_cmd_added++;

      }
   }

   return ( nb_cmd_added );*/
}

void Character::compile_ranks ( void )
{
   Character tmpcharacter;
   //Monster tmpmonster;
   int character_id = 0;
   //int monster_id = 0;
   int nb_character = 0;
   int error;
   //int nb_monster = 0;
   char tmpstr [100] = "";

   if ( p_recompile_ranks == true )
   {

      sprintf ( tmpstr, "UPDATE character SET position=%d, rank=%d WHERE body>%d", 214748364, Party_NONE, Character_BODY_ALIVE );

      SQLexec ( tmpstr );

      //----- Compile Character Ranks -----

      nb_character = SQLcount ("name", "character", "WHERE location=2 AND body=0") - 1;

      error = tmpcharacter.SQLprepare ("WHERE location=2 AND body=0 ORDER BY position");

      if ( error == SQLITE_OK )
      {

         error = tmpcharacter.SQLstep();

         SQLbegin_transaction();

         while ( error == SQLITE_ROW )
         {

         //if ( tmpcharacter.body() == Character_BODY_ALIVE )
         //{

            switch ( Party_FORMATION_POSITION [ party.formation() ] [nb_character] [ character_id])
            {
               case Party_FRONT: tmpcharacter.rank(Party_FRONT); break;
               case Party_BACK: tmpcharacter.rank(Party_BACK); break;
               case Party_NONE: tmpcharacter.rank(Party_NONE); break;
            }

            character_id++;
         //}
         //else
           // tmpcharacter.rank (Party_BACK);

            tmpcharacter.SQLupdate();
            error = tmpcharacter.SQLstep();

         }

      SQLcommit_transaction();

      }


      tmpcharacter.SQLfinalize();

   }

   p_recompile_ranks = false;

}

void Character::force_recompile_ranks ( void )
{
   p_recompile_ranks = true;
}

/*-------------------------------------------------------------------------*/
/*-                       Private Methods                                 -*/
/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/*-                       Virtual Methods                                 -*/
/*-------------------------------------------------------------------------*/



/*void Character::name_combat ( char *str )
{
   sprintf ( str, "%s the %s", p_name, p_reputation );
}*/
/*
void Character::save_vs ( unsigned int healthID, int level )
{
   int tmpattrib;
   int value;
   unsigned int mask = 1;
   unsigned int tmphealth;
   int i;

   eval_stat_all();

   for ( i = 0 ; i < 16 ; i++ )
   {
      tmphealth = ( healthID & mask );

      if ( tmphealth > 0 )
      {

         if ( tmphealth > Character_PHYSICAL_HEALTH_LIMIT )
         {
            tmpattrib = p_attribute [ Character_WILLPOWER ] + p_stat.MSAVE;
         }
         else
            tmpattrib = p_attribute [ Character_ENDURANCE ] + p_stat.PSAVE;

         value = rnd(20) + ( level / 5 );

         if ( ( tmphealth & p_stat.hlt_resist ) == 0 ) // health resistance
         {
            if ( value > tmpattrib )
            {
               value = rnd(20) + ( level / 5 );
               // when a roll is failed, a luck roll is allowed
               if ( health ( Character_HEALTH_CURSED ) == false )
                  if ( value > p_attribute [ Character_LUCK ] )
                  {
                     add_health ( tmphealth );
                  }

            }
         }
      }
      mask = mask << 1;
   }
}
                                                                */


// object relational virtual methods

void Character::sql_to_obj (void)
{

   p_primary_key = SQLcolumn_int (0);
   strncpy ( p_name, SQLcolumn_text(1), Character_NAME_STRLEN);
   p_max_HP = SQLcolumn_int (2);
   p_current_HP = SQLcolumn_int (3);
   p_max_MP = SQLcolumn_int (4);
   p_current_MP = SQLcolumn_int (5);
   p_attribute [ 0 ] = SQLcolumn_int (6);
   p_attribute [ 1 ] = SQLcolumn_int (7);
   p_attribute [ 2 ] = SQLcolumn_int (8);
   p_attribute [ 3 ] = SQLcolumn_int (9);
   p_attribute [ 4 ] = SQLcolumn_int (10);
   p_attribute [ 5 ] = SQLcolumn_int (11);
   p_level = SQLcolumn_int (12);
   p_exp = SQLcolumn_int (13);
   p_soul = SQLcolumn_int (14);
   //p_health = SQLcolumn_int (15);
   p_body = SQLcolumn_int (15);
   p_location = SQLcolumn_int (16);
   p_position = SQLcolumn_int (17);
   p_FKrace = SQLcolumn_int (18);
   p_FKcclass = SQLcolumn_int (19);
   p_FKparty = SQLcolumn_int (20);
   p_rank = SQLcolumn_int (21);
   p_portraitid = SQLcolumn_int (22);
   /*p_FKequiped [ 0 ] = SQLcolumn_int (22);
   p_FKequiped [ 1 ] = SQLcolumn_int (23);
   p_FKequiped [ 2 ] = SQLcolumn_int (24);
   p_FKequiped [ 3 ] = SQLcolumn_int (25);
   p_FKequiped [ 4 ] = SQLcolumn_int (26);
   p_FKequiped [ 5 ] = SQLcolumn_int (27);
   p_FKequiped [ 6 ] = SQLcolumn_int (28);
   p_FKequiped [ 7 ] = SQLcolumn_int (29);*/





}

void Character::template_sql_to_obj (void)
{

   // will not be used
}

void Character::obj_to_sqlupdate (void)
{

   sprintf (p_querystr, "name='%s', max_hp=%d, hp=%d, max_mp=%d, mp=%d, str=%d, dex=%d, end=%d, int=%d, cun=%d, wil=%d, level=%d, exp=%d, soul=%d, body=%d, location=%d, position=%d, FKrace=%d, FKcclass=%d, FKparty=%d, rank=%d, portraitid=%d",
   p_name,
   p_max_HP,
   p_current_HP,
   p_max_MP,
   p_current_MP,
   p_attribute [ 0 ],
   p_attribute [ 1 ],
   p_attribute [ 2 ],
   p_attribute [ 3 ],
   p_attribute [ 4 ],
   p_attribute [ 5 ],
   p_level,
   p_exp,
   p_soul,
   //p_health,
   p_body,
   p_location,
   p_position,
   p_FKrace,
   p_FKcclass,
   p_FKparty,
   p_rank,
   p_portraitid);





}

void Character::obj_to_sqlinsert (void)
{
   sprintf (p_querystr, "'%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
   p_name,
   p_max_HP,
   p_current_HP,
   p_max_MP,
   p_current_MP,
   p_attribute [ 0 ],
   p_attribute [ 1 ],
   p_attribute [ 2 ],
   p_attribute [ 3 ],
   p_attribute [ 4 ],
   p_attribute [ 5 ],
   p_level,
   p_exp,
   p_soul,
   //p_health,
   p_body,
   p_location,
   p_position,
   p_FKrace,
   p_FKcclass,
   p_FKparty,
   p_rank,
   p_portraitid);

}

void Character::compile_stat ( void )
{
   Item tmpitem;
   CClass tmpclass;
   ActiveEffect tmpeffect;
   int error;
   int i;
   int tmp_attribute = 0;
   //int tmp_range = 0;

   clear_stat();

   // ---- Add class Base modifiers ----

   tmpclass.SQLselect ( p_FKcclass );

   p_stat.d20 [ D20STAT_AD ] [ D20MOD_BASE ] = tmpclass.AD();
   p_stat.d20 [ D20STAT_MD ] [ D20MOD_BASE ] = tmpclass.MD();
   p_stat.d20 [ D20STAT_PS ] [ D20MOD_BASE ] = tmpclass.PS();
   p_stat.d20 [ D20STAT_MS ] [ D20MOD_BASE ] = tmpclass.MS();
   //p_stat.d20 [ D20STAT_INIT ] [ D20MOD_BASE ] = tmpclass.INIT();
   p_stat.d20 [ D20STAT_MELEECS ] [ D20MOD_BASE ] = tmpclass.melee_CS();
   p_stat.d20 [ D20STAT_RANGECS ] [ D20MOD_BASE ] = tmpclass.range_CS();
   //p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_Y ] = tmpclass.MDMGY();
   //printf ("debug:Character:compile_stat: MDMGY = %d\n", p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_Y ]);
   p_stat.xyz [ XYZSTAT_HPDICE ] [ XYZMOD_Y ] = tmpclass.HPdice();
   p_stat.xyz [ XYZSTAT_MPDICE ] [ XYZMOD_Y ] = tmpclass.MPdice();

   p_stat.flat [ FLAT_ATTDIV ] = tmpclass.attdiv() ;
   p_stat.flat [ FLAT_POWDIV ] = tmpclass.powdiv() ;



   // ---- add Attribute modifiers ----

   p_stat.d20 [ D20STAT_DR ] [ D20MOD_ATTRIBUTE ] = STRmodifier();
   p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_ZATTRIBUTE ] = STRmodifier();

   p_stat.d20 [ D20STAT_MELEECS ] [ D20MOD_ATTRIBUTE ] = DEXmodifier();
   p_stat.d20 [ D20STAT_AD ] [ D20MOD_ATTRIBUTE ] = DEXmodifier();

   p_stat.d20 [ D20STAT_PS ] [ D20MOD_ATTRIBUTE ] = ENDmodifier();
   p_stat.xyz [ XYZSTAT_HPDICE ] [ XYZMOD_ZATTRIBUTE ] = ENDmodifier();


   p_stat.d20 [ D20STAT_INIT ] [ D20MOD_ATTRIBUTE ] = INTmodifier();
   p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_ZATTRIBUTE ] = INTmodifier();

   p_stat.d20 [ D20STAT_RANGECS ] [ D20MOD_ATTRIBUTE ] = CUNmodifier();
   p_stat.d20 [ D20STAT_MD ] [ D20MOD_ATTRIBUTE ] = CUNmodifier();

   p_stat.d20 [ D20STAT_MS ] [ D20MOD_ATTRIBUTE ] = WILmodifier();
   p_stat.xyz [ XYZSTAT_MPDICE ] [ XYZMOD_ZATTRIBUTE ] = WILmodifier();


   // ---- add equipment modifiers ----

   error = tmpitem.SQLpreparef ("WHERE loctype=3 AND lockey=%d", p_primary_key );

   if ( error == SQLITE_OK)
   {
      error = tmpitem.SQLstep();

      while ( error == SQLITE_ROW)
      {
         for ( i = 0 ; i < D20_NB_STAT; i++ )
            //if ( tmpitem.d20stat ( i ) > p_stat.d20 [ i ][ D20MOD_EQUIP ] )
            p_stat.d20 [ i ][ D20MOD_EQUIP ] += tmpitem.d20stat ( i );

         if ( tmpitem.dmgy() > p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_Y ] )
            p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_Y ] = tmpitem.dmgy();

         //if ( tmpitem.mdmgy() > p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_Y ] )
         //   p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_Y ] = tmpitem.xyzstat ( Item_XYZ_MDMGY );

         //if ( tmpitem.xyzstat ( Item_XYZ_DMGZ ) > p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_ZEQUIP ] )
            p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_ZEQUIP ] +=  tmpitem.dmgz();

         //if ( tmpitem.xyzstat ( Item_XYZ_MDMGZ ) > p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_ZEQUIP ] )
            p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_ZEQUIP ] +=  tmpitem.mdmgz();

         if ( tmpitem.type() == Item_TYPE_WEAPON )
         {
            //p_stat.flat [ FLAT_ATTDIV ] += tmpitem.clumsy ();
            //p_stat.flat [ FLAT_MULTIHIT ] = tmpitem.clumsy () / 10;
            p_stat.flat [ FLAT_RANGE ] = tmpitem.range();
            tmp_attribute = tmpitem.attribute_attack() - 1;

            switch (  p_stat.flat [ FLAT_RANGE ] )
            {
               case 1:
                  p_stat.d20 [ D20STAT_MELEECS ] [ D20MOD_ATTRIBUTE ] = attribute_modifier ( tmp_attribute );
               break;
               case 2:
               case 3:
                  p_stat.d20 [ D20STAT_RANGECS ] [ D20MOD_ATTRIBUTE ] = attribute_modifier ( tmp_attribute );
               break;
            }

            p_stat.flat [ FLAT_DEFENSE ] = tmpitem.defense();
            p_stat.flat [ FLAT_MULTIHIT ] = tmpitem.mh ();
            p_stat.flat [ FLAT_ATTACKELEMENT ] = tmpitem.element();

            // Make a correction if weapon is a mental weapon, apply intel bonus instead
            //if ( tmp_attribute == Item_ATTRIBUTE_MENTAL)
                p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_ZATTRIBUTE ] = attribute_modifier ( tmpitem.attribute_damage() - 1);


         }

         error = tmpitem.SQLstep();
      }
   }

   tmpitem.SQLfinalize();



   //add active effects



   error = tmpeffect.SQLpreparef ( "WHERE target_type=%d AND target_id=%d", Opponent_TYPE_CHARACTER, p_primary_key );

   if ( error == SQLITE_OK)
   {
      error = tmpeffect.SQLstep ();

      while ( error == SQLITE_ROW )
      {
         p_stat.d20 [ D20STAT_AD ] [D20MOD_AEFFECT] += tmpeffect.AD ();
         p_stat.d20 [ D20STAT_MD ] [D20MOD_AEFFECT] += tmpeffect.MD ();
         p_stat.d20 [ D20STAT_PS ] [D20MOD_AEFFECT] += tmpeffect.PS ();
         p_stat.d20 [ D20STAT_MS ] [D20MOD_AEFFECT] += tmpeffect.MS ();
         p_stat.d20 [ D20STAT_MELEECS ] [D20MOD_AEFFECT] += tmpeffect.melee_CS ();
         p_stat.d20 [ D20STAT_RANGECS ] [D20MOD_AEFFECT] += tmpeffect.range_CS ();
         p_stat.d20 [ D20STAT_DR ] [D20MOD_AEFFECT] += tmpeffect.DR ();
         p_stat.d20 [ D20STAT_INIT ] [D20MOD_AEFFECT] += tmpeffect.INIT();
         p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_ZAEFFECT ] += tmpeffect.DMG();
         p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_ZAEFFECT ] += tmpeffect.MDMG();
         p_stat.flat [ FLAT_ATTDIV ] += tmpeffect.attdiv();
         p_stat.flat [ FLAT_POWDIV ] += tmpeffect.powdiv();
         //p_stat.flat [ FLAT_CONDITION ] = ( p_stat.flat [ FLAT_CONDITION] | tmpeffect.condition() );



         error = tmpeffect.SQLstep ();
      }

      tmpeffect.SQLfinalize ();
   }

   p_stat.flat [ FLAT_CONDITION ] = compile_condition();

   // sumarise all stats

   sum_stat();

   // add the right combat skill
   switch (  p_stat.flat [ FLAT_RANGE ] )
   {
      case 1:
         p_stat.flat [ FLAT_CS ] = p_stat.d20 [ D20STAT_MELEECS ] [ D20_NB_MODIFIER ];
      break;
      case 2:
      case 3:
         p_stat.flat [ FLAT_CS ] = p_stat.d20 [ D20STAT_RANGECS ] [ D20_NB_MODIFIER ];
      break;
   }

   // precalculate nb of attacks and power of magic (mostly for display)

   if ( p_stat.flat [ FLAT_ATTDIV ] > 0 )
      p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_X ] = ( p_level / p_stat.flat [ FLAT_ATTDIV ] ) + 1;
   else
      p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_X ] = 0;

   if ( p_stat.flat [ FLAT_POWDIV ] > 0 )
      p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_X ] = ( p_level / p_stat.flat [ FLAT_POWDIV ] ) + 1;
   else
      p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_X ] = 0;


}



char* Character::displayname ( void )
{
   return ( p_name );
}

/*-----------------------------------------------------------------------------*/
/*-                          Global Variables                                 -*/
/*-----------------------------------------------------------------------------*/


/*const char STR_CHR_EQUIPERR [] [ 241 ] =
{
   {"Equiping Successfull!"},
   {"Your inventory is full"},
   {"You already have another item equiped!"},
   {"You already have unequiped! this item"},
   {"This Item cannot be equiped!"},
   {"This item is  C U R S E D  !"},
   {"2 Handed Weapon and shield\ncannot be equiped together"},
   {"Your class cannot equip this item"},
};*/

/*const char STR_CHR_SEXC [] [ 2 ] =
{
   {"F"},
   {"M"},
};

const char STR_CHR_SEX [] [ 7 ] =
{
   {"Female"},
   {"Male"},
};*/

const char STR_CHR_LOCATION [] [ 11 ] =
{
   {"Unknown"},
   {"Reserve"},
   {"Party"}
};

 const int EXP_TABLE [ Character_MAX_LEVEL ] =
{// triangular number table used in D&D 3rd edition
         1000 , // 1
         3000 , // 2
         6000 , // 3
        10000 , // 4
        15000 , // 5
        21000 , // 6
        28000 , // 7
        36000 , // 8
        45000 , // 9
        55000 , // 10
        66000 , // 11
        78000 , // 12
        91000 , // 13
       105000 , // 14
       120000 , // 15
       136000 , // 16
       153000 , // 17
       171000 , // 18
       190000 , // 19
       210000  // 20  // this should be never used.

};

/*s_Character_attribgen_info ATTRIBGEN_INFO [ 5 ] =
{
   { { 3 , 3 , 3 , 3 , 3 , 3 } }, // AVERAGE ( 555555 )
   { { 6 , 5 , 4 , 2 , 1 , 0 } }, // DECREMENTING ( 876432 )
   { { 6 , 6 , 6 , 0 , 0 , 0 } }, // OPPOSED ( 888222 )
   { { 7 , 7 , 1 , 1 , 1 , 1 } }, // EXTREME ( 993333 )
   { { 4 , 4 , 4 , 2 , 2 , 2 } }, // LIGHT ( 666444 )
};*/


/*s_Character_health_info HEALTH_INFO [ Character_NB_HEALTH ] =
{
   { Character_HEALTH_ASLEEP,    "Asleep          ", "Sleep        ",
      0,  50, 200, Character_ENDURANCE, 15 },
   { Character_HEALTH_PARALYSED, "Paralyzed       ", "Paralysation ",
      200, 255,   0, Character_DEXTERITY, 11 },
   { Character_HEALTH_PETRIFIED, "Petrified       ", "Petrification",
      150, 150, 150, Character_STRENGTH, 7 },
   { Character_HEALTH_RESERVED,   "???            ", "???          ",
      100, 120,   0, -1, 0},
   { Character_HEALTH_POISONED,  "Poisoned        ", "Poison       ",
      50, 255,   0, Character_ENDURANCE, 10 },
   { Character_HEALTH_BLINDED,   "Blinded         ", "Blind        ",
      165,  65, 215, Character_DEXTERITY, 12 },
   { Character_HEALTH_CRIPPLED,  "Crippled        ", "Crippling    ",
      255,  50,  50, Character_ENDURANCE, 10 },
   { Character_HEALTH_RESERVED3, "???             ", "???          ",
      150, 150, 150, -1, 0},
   { Character_HEALTH_AFFRAID,   "Affraid         ", "Fear         ",
      200,   0, 200, Character_INTELLIGENCE, 13 },
   { Character_HEALTH_CURSED,    "Cursed          ", "Curse        ",
      165,  65,  65, Character_CUNNING, 15 },
   { Character_HEALTH_SEALED,    "Sealed          ", "Seal         ",
      0, 200, 200, Character_WILLPOWER, 12 },
   { Character_HEALTH_DOOMED,    "Doomed          ", "Doom         ",
      135,  95,  95, Character_CUNNING, 6 },
   { Character_HEALTH_FADING,    "Fading          ", "Fade         ",
      225, 225, 225, Character_WILLPOWER, 5 },
   { Character_HEALTH_RESERVED5, "???             ", "???          ",
      150,150, 150, -1, 0 },
   { Character_HEALTH_RESERVED6, "???             ", "???          ",
      150,150, 150, -1, 0 },
   { Character_HEALTH_RESERVED7, "???             ", "???          ",
      150,150, 150, -1, 0 }
};*/

const char STR_CHR_BODY [] [ 11 ] =
{
   {"Alive"},
   {"Petrified"},
   {"Dead"},
   {"Ashes"},
   {"Deleted"},
   {""},
   {""},
   {""},
   {""},
   {""},
   {"Dispelled"},
   {"Run"}

};

/*-------------------------------------------------------------------------*/
/*-                      Non Class Procedure                              -*/
/*-------------------------------------------------------------------------*/


//------------------------------- OLD CODE -------------------------------------------

/*const char* Character::family ( void )
{
   return ( p_family );
}

void Character::family ( const char *str )
{
   strncpy ( p_family, str, 16 );
}


const char* Character::reputation ( void )
{
   return ( p_reputation );
}

void Character::reputation ( const char *str )
{
   strncpy ( p_reputation, str, 16 );
}

void Character::rank ( int rankID )
{
   p_rank = rankID;
}

int Character::rank ( void )
{
   return ( p_rank );
}

void Character::institution ( int institutionID )
{
   p_institution = institutionID;
}

int Character::institution ( void )
{
   return ( p_institution );
}*/


/*void Character::sex ( int value )
{
   p_sex = value;
}

int Character::sex ( void )
{
   return ( p_sex );
}

float Character::age ( void )
{
   return ( static_cast <float>(p_age) );
}

void Character::age ( float value )
{
   p_age = static_cast<fix>(value);
}*/

/*int Character::reward_exp ( void )
{
   return ( p_reward_exp );
}

void Character::reward_exp ( int value )
{
   p_reward_exp = value;
}*/

/*int Character::available ( void )
{
   return ( p_available );
}

void Character::available ( int value )
{
   p_available = value;
}*/

/*void Character::status ( int statusID )
{
   p_body = statusID;
}

int Character::status ( void )
{
   return ( p_body );
}*/


/*const char* Character::statusS ( void )
{
   return ( STR_CHR_STATUS [ p_body ] );
}*/



/*int Character::encumbrance ( void )
{
   return ( p_encumbrance );
}

float Character::weight ( void )
{
   return ( p_weight );
}

int Character::maxweight ( void )
{
   int tmpvalue;
   tmpvalue = ( p_attribute [ Character_STRENGTH ] +
               p_attribute [ Character_ENDURANCE ] ) * 3;


   switch ( p_size )
   {
      case Character_SIZE_TINY :
         tmpvalue = tmpvalue - ( tmpvalue / 2 );
      break;
      case Character_SIZE_int :
         tmpvalue = tmpvalue - ( tmpvalue / 4 );
      break;
      case Character_SIZE_LARGE :
         tmpvalue = tmpvalue + ( tmpvalue / 4 );
      break;
      case Character_SIZE_HUGE :
         tmpvalue = tmpvalue + ( tmpvalue / 2 );
      break;

   }

   if ( tmpvalue == 0 )
      tmpvalue = 1;

   return ( tmpvalue );

}

const char* Character::combatstr ( void )
{
   return ( p_combatstr );
}

void Character::combatstr ( const char *str )
{
   strncpy ( p_combatstr, str, 13 );
//   p_combatstr [ 13 ] = '\0';
}
*/

/*void Character::levelup_speed ( unsigned char value )
{
   p_levelup_speed = value;

}
*/

/*------------------------ Equipment Methods ------------------------------*/

/*int Character::inventory ( int index )
{
  return ( p_inventory [ index ] );
}*/

/*
int Character::weapon ( void )
{
   return ( p_weapon );
}

int Character::shield ( void )
{
   return ( p_shield);
}


int Character::armor ( void )
{
   return ( p_armor);
}


int Character::feet ( void )
{
   return ( p_feet);
}


int Character::hand ( void )
{
   return ( p_hand);
}


int Character::head ( void )
{
   return ( p_head );
}


int Character::other ( void )
{
   return ( p_other );
} */


/*int Character::new_inventory ( int itemtag )
{
   int invent_size;
   int tmptag;

   invent_size = p_nb_inventory;

   if ( invent_size < Character_INVENTORY_SIZE )
   {
      tmptag = db.copy ( itemtag, DBSOURCE_SAVEGAME );
      p_inventory [ invent_size ] = tmptag;
      p_nb_inventory++;
      eval_stat_all();
   }
   else
      return ( Character_ITEM_INVENTORY_FULL );
}

int Character::delete_inventory ( int inventoryID )
{
   int i;

   if ( p_nb_inventory > 0 )
   {
      db.remove ( p_inventory [ inventoryID ] );
      p_inventory [ inventoryID ] .number ( 0 );

      for ( i = inventoryID; i < Character_INVENTORY_SIZE - 1; i++ )
         p_inventory [ i ] = p_inventory [ i + 1 ];

      p_inventory [ Character_INVENTORY_SIZE - 1 ] . number( 0 );
      eval_stat_all();
      p_nb_inventory--;
   }
   return ( Character_ITEM_SUCCESSFULL );
}

int Character::remove_inventory ( int inventoryID )
{
   int i;

//   delete p_inventory [ inventoryID ];
   p_inventory [ inventoryID ] .number ( 0 );

   for ( i = inventoryID; i < Character_INVENTORY_SIZE - 1; i++ )
      p_inventory [ i ] = p_inventory [ i + 1 ];

   p_inventory [ Character_INVENTORY_SIZE - 1 ] . number( 0 );
   eval_stat_all();
   p_nb_inventory--;

   return ( Character_ITEM_SUCCESSFULL );
}
int Character::add_inventory ( int itemtag )
{
   int invent_size;

   invent_size = nb_inventory ();

   if ( invent_size < Character_INVENTORY_SIZE - 1 )
   {
      p_inventory [ invent_size ] = itemtag;
      eval_stat_all();
      p_nb_inventory++;
//      allocate_new_item ( invent_size, obj );
   }
   else
      return ( Character_ITEM_INVENTORY_FULL );
}
*/

/*int Character::equip_item ( int inventoryID )
{

//   int item_type;
   int accessory_location;
   Item tmpitem;
   //Accessory tmpacc;
   //Weapon tmpwpn;
   int error = Character_ITEM_SUCCESSFULL;

   if ( p_inventory [ inventoryID ] != 0 )
   {
      tmpitem.SQLselect ( p_inventory [ inventoryID ] );

      if ( is_equipable ( tmpitem.tag() ) == false )
         return ( Character_ITEM_CLASS_RESTRICTION );

//?? Need to check for 2 hand weapon and shield
               tmpwpn.SQLselect ( p_inventory [ inventoryID ] );

               if ( tmpwpn.nb_hand() == Weapon_TWOHANDED &&
                  p_shield != 0 )
                  error = Character_ITEM_TWOHAND_NOSHIELD;
               else
       if ( tmpitem.location() != Item_LOCATION_NOLOC )
       {
          if ( p_equiped [ tmpitem.location() ] == 0 )
             p_equiped [ tmpitem.location() ]  = p_inventory [ inventoryID ];
          else
             error = Character_ITEM_ALREADY_EQUIPED;
       }
       else
          error = Character_ITEM_UNEQUIPABLE;
   }*/


/*      switch ( tmpitem.type() )
      {
         case Item_TYPE_WEAPON :
            if ( p_weapon == 0 )
            {
                  p_weapon = p_inventory [ inventoryID ];
            }
            else
               error = Character_ITEM_ALREADY_EQUIPED;

         break;

         case Item_TYPE_SHIELD :
            if ( p_shield == 0 )
            {
                  p_shield = p_inventory [ inventoryID ];
            }
            else
               error = Character_ITEM_ALREADY_EQUIPED;
         break;

         case Item_TYPE_ARMOR :
            if ( p_armor == 0 )
               p_armor = p_inventory [ inventoryID ];
            else
               error = Character_ITEM_ALREADY_EQUIPED;
         break;

         case Item_TYPE_ACCESSORY :
            tmpacc.SQLselect ( p_inventory [ inventoryID ] );
            accessory_location = tmpacc.location();
            switch ( accessory_location )
            {
               case Accessory_LOCATION_HEAD  :
                  if ( p_head == 0 )
                     p_head = p_inventory [ inventoryID ];
                  else
                     error = Character_ITEM_ALREADY_EQUIPED;
               break;

               case Accessory_LOCATION_FEET  :
                  if ( p_feet == 0 )
                     p_feet = p_inventory [ inventoryID];
                  else
                     error = Character_ITEM_ALREADY_EQUIPED;
               break;

               case Accessory_LOCATION_HAND  :
                  if ( p_hand == 0 )
                     p_hand = p_inventory [ inventoryID];
                  else
                     error = Character_ITEM_ALREADY_EQUIPED;
               break;

               case Accessory_LOCATION_OTHER :
                  if ( p_other == 0 )
                     p_other = p_inventory [ inventoryID];
                  else
                     error = Character_ITEM_ALREADY_EQUIPED;
               break;
            }
         break;

         default :
            error = Character_ITEM_UNEQUIPABLE;
         break;
      }
   }*/

   /*if ( error == Character_ITEM_SUCCESSFULL )
   {
      eval_stat_all();
      remove_inventory ( inventoryID );
      p_nb_equiped++;

   }
   //return ( error );
}*/

/*int Character::unequip_item ( int location )
{
  Item tmpitem;

   if ( p_equiped [ location ] != 0 )
   {
      tmpitem.SQLselect ( p_equiped [ location ] );

      if ( tmpitem.cursed () == true )
         return ( Character_ITEM_CURSED );

      if ( nb_inventory () < Character_INVENTORY_SIZE )
      {
          add_inventory ( p_equiped [ location ] );
          p_equiped [ location ].number ( 0 );
          p_nb_equiped--;
      }
      else
         return (Character_ITEM_INVENTORY_FULL);

   }
   else
      return (Character_ITEM_ALREADY_UNEQUIPED );
*/
/*   switch ( location )
   {
      case Character_UNEQUIP_WEAPON :
         if ( p_weapon != 0)
            tmpitem.SQLselect ( p_weapon );
      break;

      case Character_UNEQUIP_SHIELD :
         if ( p_shield != 0)
            tmpitem.SQLselect ( p_shield );
      break;

      case Character_UNEQUIP_ARMOR :
         if ( p_armor != 0)
            tmpitem.SQLselect ( p_armor );
      break;

      case Character_UNEQUIP_HEAD :
         if ( p_head != 0)
            tmpitem.SQLselect ( p_head );
      break;

      case Character_UNEQUIP_HAND :
         if ( p_hand != 0)
            tmpitem.SQLselect ( p_hand );
      break;

      case Character_UNEQUIP_FEET :
         if ( p_feet != 0)
            tmpitem.SQLselect ( p_feet );
      break;

      case Character_UNEQUIP_OTHER :
         if ( p_other != 0)
            tmpitem.SQLselect ( p_other );
      break;
   }*/



/*   if ( nb_inventory () < Character_INVENTORY_SIZE )
   {
      switch ( location )
      {
         case Character_UNEQUIP_WEAPON :

            if ( p_weapon != 0)
            {
               add_inventory ( p_weapon );
               p_weapon.number ( 0 );
               p_nb_equiped--;
            }
         break;


         case Character_UNEQUIP_SHIELD :
            if ( p_shield != 0)
            {
               add_inventory ( p_shield );
               p_shield.number ( 0 );
               p_nb_equiped--;
            }
         break;

         case Character_UNEQUIP_ARMOR :
            if ( p_armor != 0)
            {
               add_inventory ( p_armor );
               p_armor.number ( 0 );
               p_nb_equiped--;
            }
         break;

         case Character_UNEQUIP_HEAD :
            if ( p_head != 0)
            {
               add_inventory ( p_head );
               p_head.number ( 0 );
               p_nb_equiped--;
            }
         break;

         case Character_UNEQUIP_HAND :
            if ( p_hand != 0)
            {
               add_inventory ( p_hand );
               p_hand.number ( 0 );
               p_nb_equiped--;
            }
         break;

         case Character_UNEQUIP_FEET :
            if ( p_feet != 0)
            {
               add_inventory ( p_feet );
               p_feet.number ( 0 );
               p_nb_equiped--;
            }
         break;

         case Character_UNEQUIP_OTHER :
            if ( p_other != 0)
            {
               add_inventory ( p_other );
               p_other.number ( 0 );
               p_nb_equiped--;
            }
         break;
      }
   }*/
/*   else
      return (Character_ITEM_INVENTORY_FULL);

   //return ( Character_ITEM_SUCCESSFULL );
}*/

/*int Character::nb_inventory ( void )
{

   return ( p_nb_inventory );
}
*/

/*int Character::nb_equiped ( void )
{
   return ( p_nb_equiped );
}*/


/*----------------- Data to string interpretation -------------------------*/


/*const char* Character::sexC ( void )
{
   return ( STR_CHR_SEXC [ p_sex ] );
}


const char* Character::sexS ( void )
{
   return ( STR_CHR_SEX [ p_sex ] );
}*/

/*const char* Character::availableS ( void )
{
   return ( STR_CHR_AVAILABLE [ p_available ] );
}*/

/*void Character::objdat_to_strdat ( void *dataptr )
{
   dbs_Character &tmpdat = *(static_cast<dbs_Character*> ( dataptr ));

   int i;
//   int j;

   Character::objdat_to_strdat ( dataptr );

   tmpdat.rank = p_rank;
   tmpdat.institution = p_institution;
   tmpdat.FKrace = p_FKrace;//.number();
   tmpdat.cclass = p_cclass.number();
   tmpdat.sex = p_sex;
   tmpdat.age = p_age;
   tmpdat.experience = p_exp;
   tmpdat.available = p_available;
   tmpdat.encumbrance = p_encumbrance;
   tmpdat.weight = p_weight;

   strncpy ( tmpdat.family, p_family, 16 );
   strncpy ( tmpdat.reputation, p_reputation, 16 );
   strncpy ( tmpdat.combatstr, p_combatstr, 13 );

   for ( i = 0 ; i < Character_INVENTORY_SIZE ; i++ )
      tmpdat.inventory [ i ] = p_inventory [ i ].number();

   for ( i = 0 ; i < Character_EQUIP_SIZE ; i++ )
      tmpdat.equiped [ i ] = p_equiped [ i ].number();

   tmpdat.nb_inventory = p_nb_inventory;
   tmpdat.nb_equiped = p_nb_equiped;

}

void Character::strdat_to_objdat ( void *dataptr )
{
   dbs_Character &tmpdat = *(static_cast<dbs_Character*> ( dataptr ));

   int i;
//   int j;
   Character::strdat_to_objdat ( dataptr );

   p_rank = tmpdat.rank;
   p_institution = tmpdat.institution;
   p_FKrace = tmpdat.FKrace;
   p_cclass.number ( tmpdat.cclass );
   p_sex = tmpdat.sex;
   p_age = tmpdat.age;
   p_exp = tmpdat.experience;
   p_available = tmpdat.available;
   p_encumbrance = tmpdat.encumbrance;
   p_weight = tmpdat.weight;
   strcpy ( p_family, tmpdat.family );
   strcpy ( p_reputation, tmpdat.reputation );
   strcpy ( p_combatstr, tmpdat.combatstr );

   for ( i = 0 ; i < Character_INVENTORY_SIZE ; i++ )
      p_inventory [ i ].number(tmpdat.inventory [ i ]);
   for ( i = 0 ; i < Character_EQUIP_SIZE ; i++ )
      p_equiped [ i ].number(tmpdat.equiped [ i ]);

   p_nb_inventory = tmpdat.nb_inventory;
   p_nb_equiped = tmpdat.nb_equiped;


}

void Character::child_DBremove ( void )
{
 */ /* Item tmpitem;
   int i;

   for ( i = 0 ; i < Character_INVENTORY_SIZE ; i++ )
   {
      if ( p_inventory [ i ] != 0 )
      {
         tmpitem.SQLselect (  p_inventory [ i ].number() );
         tmpitem.DBremove( );
      }
   }

   for ( i = 0 ; i < Character_EQUIP_SIZE ; i++ )
   {
      if ( p_equiped [ i ] != 0 )
      {
         tmpitem.SQLselect (  p_equiped [ i ].number() );
         tmpitem.DBremove( );
      }
   }

*/
/*   if ( p_weapon != 0 )
   {
      tmpitem.SQLselect (  p_weapon.number() );
      tmpitem.DBremove ( );
   }
   if ( p_armor != 0 )
   {
      tmpitem.SQLselect (  p_armor.number() );
      tmpitem.DBremove (  );
   }
   if ( p_shield != 0 )
   {
      tmpitem.SQLselect (  p_shield.number() );
      tmpitem.DBremove ( );
   }
   if ( p_head != 0 )
   {
      tmpitem.SQLselect (  p_head.number() );
      tmpitem.DBremove ( );
   }
   if ( p_feet != 0 )
   {
      tmpitem.SQLselect (  p_feet.number() );
      tmpitem.DBremove ( );
   }
   if ( p_hand != 0 )
   {
      tmpitem.SQLselect (  p_hand.number() );
      tmpitem.DBremove ( );
   }
   if ( p_other != 0 )
   {
      tmpitem.SQLselect (  p_other.number() );
      tmpitem.DBremove ( );
   }                                   */

/*}*/

/*void Character::raise_days ( int nb_days )
{
   p_age = p_age + ( 0.001 * nb_days );

   int tmpage = static_cast<int>(p_age);
   float restage = p_age - tmpage;

   if ( restage > 0.320 )
   {
      tmpage = tmpage + 1;
      p_age = tmpage;
      recover_soul ( dice ( 5 ) );

      //?? todo : add aging effect
   }

}*/

/*void Character::eval_stat_race ( void )
{
   int i;
   int j;
   Race tmprace;

   if ( p_FKrace != -1 )
   {
      tmprace.SQLselect ( p_FKrace );

      p_stat.dmg_y += tmprace.DMGy();
      p_stat.mdmg_y += tmprace.MDMGy();

      p_stat.hlt_resist = ( p_stat.hlt_resist | tmprace.resistance() );

      // ability bonus
*/
/* removed in the new rules
      for ( i = 0 ; i < 3 ; i++ )
      {
         switch ( tmprace.ability ( i ) )
         {
            case Race_ABILITY_FLYING :
               p_stat.AD += 1;
               p_stat.init += 1;
            break;
            case Race_ABILITY_ARMOR_SKIN :
               p_stat.PD += 3;
               p_stat.DR += 2;
            break;
            case Race_ABILITY_RESIST_MAGIC :
               p_stat.MPD += 3;
               p_stat.MDR += 2;
            break;
            case Race_ABILITY_LUCKY :
               p_stat.PSAVE += 3;// maybe OK
               p_stat.MSAVE += 3; // maybe OK
            break;
         }
      }

   }
}*/

/*void Character::eval_stat_class ( void )
{
   CClass tmpclass;

   if ( p_cclass != 0 )
   {
      tmpclass.SQLselect ( p_cclass );
      p_stat.elm_resist = ( p_stat.elm_resist | tmpclass.elm_resist () );
      p_stat.elm_effect = ( p_stat.elm_effect | tmpclass.elm_effect () );
      p_stat.hlt_effect = ( p_stat.hlt_effect | tmpclass.hlt_effect () );
      p_stat.hlt_resist = ( p_stat.hlt_resist | tmpclass.hlt_resist() );
      p_stat.magikproperty = ( p_stat.magikproperty & tmpclass.magikproperty () );
      p_stat.dmg_y += ( p_level / tmpclass.DMGdiv () );
      p_stat.mdmg_y += ( p_level / tmpclass.MDMGdiv () );
      p_stat.multihitmod += 16 - ( p_level / tmpclass.mulhitdiv () );
      p_stat.AD += tmpclass.baseAD ();
      p_stat.MAD += tmpclass.baseMAD ();

      if ( tmpclass.baseMAD() > 0 )
         p_stat.MAD += CUNmodifier();


      // skill influence ???
   }
}*/

/*void Character::eval_stat_equipment ( void )
{
   int i;
   int j;
   unsigned int mask;
   Item tmpitem;
   Armor tmparmor;
   Weapon tmpweapon;
   Shield tmpshield;
   //Accessory tmpaccessory;
   CClass tmpclass;

   // compile Health , Elemental resist and effect

   for ( i = Character_EQUIP_SHIELD ; i < Character_EQUIP_SIZE ; i++ )
   {
      if ( p_equiped [ i ] != 0 )
      {
         tmpitem.SQLselect ( p_equiped [ i ] );
         p_stat.elm_resist = ( p_stat.elm_resist & tmpitem.elmeffect() );
         p_stat.hlt_resist = ( p_stat.hlt_resist & tmpitem.hlteffect() );
         p_stat.magikproperty = ( p_stat.magikproperty & tmpitem.magikproperty() );
      }
   }

   // Armor

   if ( p_equiped [ ARMOR ] != 0 )
   {
      tmparmor.SQLselect ( p_equiped [ ARMOR ] );
      p_stat.PD = p_stat.PD + tmparmor.PD();
      p_stat.AD = p_stat.AD + tmparmor.AD();
      p_stat.DR = p_stat.DR + tmparmor.DR();
      p_stat.MPD = p_stat.MPD + tmparmor.MPD();
      p_stat.MDR = p_stat.MDR + tmparmor.MDR();
   }

   if ( p_equiped [ SHIELD ] != 0 )
   {
      tmpshield.SQLselect ( p_equiped [ SHIELD ] );
      p_stat.PD = p_stat.PD + tmpshield.PD();
      p_stat.AD = p_stat.AD + tmpshield.AD();
      p_stat.MPD = p_stat.MPD + tmpshield.MPD();
      p_stat.MAD = p_stat.MAD + tmpshield.MAD();
   }

   if ( p_equiped [ WEAPON ] != 0 )
   {
      tmpweapon.SQLselect ( p_equiped [ WEAPON ] );
      p_stat.dmg_x = p_stat.dmg_x + tmpweapon.dmg_x();
      p_stat.dmg_z = p_stat.dmg_z + tmpweapon.dmg_z();
      p_stat.dmg_type = tmpweapon.dmg_type();
//      p_stat.size = tmpweapon.size();
      p_stat.range = tmpweapon.range();
//      p_stat.nb_hand = tmpweapon.nb_hand();
      p_stat.nb_max_attack = tmpweapon.nb_max_attack();
      p_stat.AD = p_stat.AD + tmpweapon.AD();
      p_stat.hitbonus = p_stat.hitbonus + tmpweapon.hitbonus();
      p_stat.elm_effect = ( p_stat.elm_effect & tmpitem.elmeffect() );
      p_stat.hlt_effect = ( p_stat.hlt_effect & tmpitem.hlteffect() );


      if ( tmpweapon.static_dmg() > 0 )
      {
         p_stat.dmg_y = tmpweapon.static_dmg();
      }
      else
         p_stat.dmg_z += STRmodifier();

      switch ( tmpweapon.attribute() )
      {
         case Weapon_ATTRIB_STR :
            p_stat.hitbonus += STRmodifier();//p_attribute [ Character_STRENGTH ];
         break;
         case Weapon_ATTRIB_INT :
            p_stat.hitbonus += INTmodifier();//p_attribute [ Character_INTELLIGENCE ];
         break;
         case Weapon_ATTRIB_DEX :
            p_stat.hitbonus += DEXmodifier(); //p_attribute [ Character_DEXTERITY ];
         break;
      }

   }
   else
   {   // bare hand combat

      p_stat.dmg_x += 1;
      p_stat.dmg_type = Weapon_DMGTYPE_NORMAL;
      p_stat.range = Weapon_RANGE_MELEE;
//      p_stat.nb_hand = 1;
      p_stat.nb_max_attack = 2;
//      p_stat.AD = p_stat.AD + 1;
      p_stat.hitbonus += STRmodifier();//p_attribute [ Character_STRENGTH ];
      p_stat.dmg_z += STRmodifier();

      tmpclass.SQLselect ( p_cclass );
      if ( tmpclass.have_skill ( CClass_SKILL_MARTIALART ) == true )
      {
         p_stat.dmg_x += p_level / 5;
         p_stat.AD += p_level / 5;
         p_stat.nb_max_attack = 2 + ( p_level / 8 );
//         if ( p_stat.nb_max_attack > 5 )
//            p_stat.nb_max_attack = 5;
      }


   }

   for ( i = Character_EQUIP_HEAD ; i <= Character_EQUIP_OTHER ; i++ )
   {
      if ( p_equiped [ i ] != 0 )
      {
         tmpaccessory.SQLselect ( p_equiped [ i ] );

         for ( j = 0 ; j < 3 ; j++ )
         {
            switch ( tmpaccessory.statID ( j ) )
            {
               case Accessory_STAT_NONE     :
               break;
               case Accessory_STAT_PD       :
                  p_stat.PD += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_AD       :
                  p_stat.AD += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_MPD      :
                  p_stat.MPD += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_MAD      :
                  p_stat.MAD += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_DR       :
                  p_stat.DR += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_MDR      :
                  p_stat.MDR += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_PSAVE    :
                  p_stat.PSAVE += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_MSAVE    :
                  p_stat.MSAVE += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_HIT      :
                  p_stat.hitbonus += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_INIT      :
                  p_stat.init += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_DMG_Z     :
                  p_stat.dmg_z += tmpaccessory.bonus ();
               break;
               case Accessory_STAT_MDMG_Z    :
                  p_stat.mdmg_z += tmpaccessory.bonus ();
               break;
            }
         }
      }
   }

}*/

/*void Character::eval_stat_encumbrance ( void )
{

   Item tmpitem;
   int i;
   p_weight = 0;

   for ( i = 0 ; i < Character_EQUIP_SIZE ; i++ )
   {
      if ( p_equiped [ i ] != 0 )
      {
         tmpitem.SQLselect ( p_equiped [ i ] );
         p_weight += tmpitem.weight ();
      }
   }

   for ( i = 0 ; i < nb_inventory () ; i++ )
      if ( p_inventory [ i ] != 0 )
      {
         tmpitem.SQLselect ( p_inventory [ i ] );
         p_weight += tmpitem.weight ();
      }

   //---------------  encumbrance -------------------
   p_encumbrance = ( p_weight * 100 ) / maxweight ();

   p_stat.encmod = p_encumbrance / 20;

//   p_stat.AD -= p_stat.encmod;

}*/


/*void Character::eval_stat_other ( void )
{
   eval_stat_race ();
   eval_stat_class ();
   eval_stat_equipment ();
   eval_stat_encumbrance ();
}*/
