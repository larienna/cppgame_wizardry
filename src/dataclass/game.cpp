/***************************************************************************/
/*                                                                         */
/*                             G A M E . C P P                             */
/*                           Class Source Code                             */
/*                                                                         */
/*     Content : Game class source code                                    */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : April 30th, 2002                                    */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <gameclock.h>
//#include <game.h>


/*
//#include <time.h>
#include <allegro.h>


//#include <datafile.h>
//#include <datmacro.h>
//#include <system.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
*/


/*-------------------------------------------------------------------------*/
/*-                     Constructor & Destructor                          -*/
/*-------------------------------------------------------------------------*/

Game::Game ( void )
{
//   p_adventurefile = "";
   strncpy ( p_savegamefile, "", 1 );
   strncpy ( p_adventurefile, "", 1 );
   strncpy ( p_name, "", 1 );
   strncpy ( p_cityname, "", 1 );
   strncpy ( p_mazename, "", 1 );
   strncpy ( p_saganame, "", 1 );
   strncpy ( p_version, "", 1 );
   strncpy ( p_author, "", 1 );
   strncpy ( p_date, "", 1 );
   p_episode = 0;
   p_newgame = 0;
   p_lastgame = 0;
   clock.stamp ( 0 );
   p_lastcityday = 0;
   //strcpy ( p_password, "" );
   //p_pass_protect = 0;
   //p_difficulty = 0;
   //p_levelup = 0;

   //p_dblength = sizeof ( dbs_Game );
   //p_dbtableID = DBTABLE_GAME;
   strcpy (p_tablename, "game");
}

Game::~Game ( void )
{

}


/*-------------------------------------------------------------------------*/
/*-                        Property Methods                               -*/
/*-------------------------------------------------------------------------*/

const char* Game::savegamefile ( void )
{
   return ( p_savegamefile );
}

void Game::savegamefile ( const char *str )
{
   strncpy ( p_savegamefile, str, Game_FILENAME_STR_SIZE );
}

const char* Game::adventurefile ( void )
{
   return ( p_adventurefile );
}

void Game::adventurefile ( const char *str )
{
   strncpy ( p_adventurefile, str, Game_FILENAME_STR_SIZE );
}


const char* Game::name ( void )
{
   return ( p_name );
}

void Game::name ( const char *str )
{
   strncpy ( p_name, str, Game_NAME_STR_SIZE );
}

const char* Game::cityname ( void )
{
   return ( p_cityname );
}

void Game::cityname ( const char *str )
{
   strncpy ( p_cityname, str, Game_NAME_STR_SIZE );
}

const char* Game::mazename ( void )
{
   return ( p_mazename );
}

void Game::mazename ( const char *str )
{
   strncpy ( p_mazename, str, Game_NAME_STR_SIZE );
}

const char* Game::saganame ( void )
{
   return ( p_saganame );
}

void Game::saganame ( const char *str )
{
   strncpy ( p_saganame, str, Game_NAME_STR_SIZE );
}

int Game::episode ( void )
{

   return p_episode;
}

void Game::episode ( int value )
{

   p_episode = value;
}


const char* Game::author ( void )
{
   return ( p_author );
}

void Game::author ( const char *str )
{
   strncpy ( p_author, str, Game_AUTHOR_STR_SIZE );
}

const char* Game::version ( void )
{
   return ( p_version );
}

void Game::version ( const char *str )
{
   strncpy ( p_version, str, Game_VERSION_STR_SIZE );
}

const char* Game::date ( void )
{
   return ( p_date );
}

void Game::date ( const char *str )
{
   strncpy ( p_date, str, Game_DATE_STR_SIZE );
}

int Game::newgame ( void )
{
   return ( p_newgame );
}

void Game::newgame ( int value )
{
   p_newgame = value;
}

int Game::lastgame ( void )
{
   return (p_lastgame);
}

void Game::lastgame ( int value)
{
   p_lastgame = value;
}

/*GameClock Game::clock ( void )
{
   return ( p_clock );
}

void Game::clock ( GameClock value )
{
   p_clock = value;
}*/

int Game::lastcityday ( void )
{
   return (p_lastcityday);
}

void Game::lastcityday ( int value)
{
   p_lastcityday = value;
}


/*---------------------------------------------------------------------------*/
/*                               Method                                     -*/
/*---------------------------------------------------------------------------*/

/*void Game::increment_clock ( void )
{
   clock.add_minute ( 1 );
   ActiveEffect::reduce_time();
}*/

void Game::increment_lastcityday ( void )
{
   p_lastcityday++;
}

/*
s_Game_time Game::time ( void )
{
   return ( p_time );
}

s_Game_date Game::date ( void )
{
   return ( p_date );
}

void Game::admin_password ( string &str )
{

}
  */
/*string& Game::adventure ( void )
{
   return ( p_adventurefile );
}

const char* Game::cadventure ( void )
{
   return (  p_adventurefile.data() );
}

void Game::adventure ( string &str )
{
   p_adventurefile = str;
} */

/*string& Game::savegame ( void )
{
   return ( p_savegamefile );
} */


/*void Game::password ( const char *str )
{
   strcpy ( p_password, str );
}

int Game::difficulty ( void )
{
   return ( p_difficulty );
}
void Game::difficulty ( int value )
{
   p_difficulty = value;
}
int Game::levelup ( void )
{
   return ( p_levelup );
}

void Game::levelup ( int value )
{
   p_levelup = value;
}
void Game::protect ( void )
{
   p_pass_protect = true;
}
void Game::open ( void )
{
   p_pass_protect = false;
}
bool Game::pass_protect ( void )
{
   return ( p_pass_protect );
}

int Game::adventure ( void )
{
   return p_adventure;
}

void Game::adventure ( int value )
{
   p_adventure = value;
}*/


/*-------------------------------------------------------------------------*/
/*-                            Method                                     -*/
/*-------------------------------------------------------------------------*/

/*
void Game::inc_second ( int value )
{
   p_time.second = p_time.second + value;
   adjust_overflow ();
}

void Game::inc_minute ( int value )
{
   p_time.minute = p_time.minute + value;
   adjust_overflow ();
}

void Game::inc_hour ( int value )
{
   p_time.hour = p_time.hour + value;
   adjust_overflow ();
}

void Game::inc_day ( int value )
{
   p_date.day = p_date.day + value;
   adjust_overflow ();
}

bool Game::verify_admin_password ( string &str )
{

}
  */
/*bool Game::verify_password ( const char *str )
{
   if ( strcmp ( p_password, str ) == 0 )
      return ( true );
   else
      return ( false );
}*/

/*-------------------------------------------------------------------------*/
/*-                      Private Methods                                  -*/
/*-------------------------------------------------------------------------*/
/*
void Game::adjust_overflow ( void )
{
   while ( p_time.second >= 60 )
   {
      p_time.minute++;
      p_time.second = p_time.second - 60;
   }

   while ( p_time.minute >= 60 )
   {
      p_time.hour++;
      p_time.minute = p_time.minute - 60;
   }

   while ( p_time.second >= 24 )
   {
      p_date.day++;
      p_time.hour = p_time.hour - 24;
   }

   while ( p_date.day > 28 )
   {
      p_date.month++;
      p_date.day = p_date.day - 28;
   }

   while ( p_date.month > 12 )
   {
      p_date.year++;
      p_date.month = p_date.month - 12;
   }
}
  */


/*-------------------------------------------------------------------------*/
/*-                        Virtual Methods                                -*/
/*-------------------------------------------------------------------------*/

void Game::sql_to_obj (void)
{

   //printf ("\r\ncol 0");
   p_primary_key = SQLcolumn_int (0);
   //printf ("\r\ncol 1");
   strncpy ( p_name, SQLcolumn_text(1), Game_NAME_STR_SIZE);
   //printf ("\r\ncol 2");
   strncpy ( p_cityname, SQLcolumn_text(2), Game_NAME_STR_SIZE);
   //printf ("\r\ncol 3");
   strncpy ( p_mazename, SQLcolumn_text(3), Game_NAME_STR_SIZE);
   //printf ("\r\ncol 4");
   strncpy ( p_saganame, SQLcolumn_text(4), Game_NAME_STR_SIZE);
   //printf ("\r\ncol 5");
   p_episode = SQLcolumn_int (5);
   //printf ("\r\ncol 6");
   strncpy ( p_author, SQLcolumn_text(6), Game_AUTHOR_STR_SIZE);
   //printf ("\r\ncol 7");
   strncpy ( p_version, SQLcolumn_text(7), Game_VERSION_STR_SIZE);
   //printf ("\r\ncol 8");
   strncpy ( p_date, SQLcolumn_text(8), Game_DATE_STR_SIZE);
   //printf ("\r\ncol 9");
   strncpy ( p_adventurefile, SQLcolumn_text(9), Game_FILENAME_STR_SIZE);

   p_newgame = SQLcolumn_int (10);
   p_lastgame = SQLcolumn_int (11);
   clock.stamp ( SQLcolumn_int (12) );
   p_lastcityday = SQLcolumn_int (13);
   p_encounter_count = SQLcolumn_int (14);


}

void Game::template_sql_to_obj (void)
{

   // will not be used
}

void Game::obj_to_sqlupdate (void)
{

   sprintf (p_querystr, "name='%s', cityname='%s', mazename='%s', saganame='%s', episode=%d, author='%s', version='%s', date='%s', adventurefile='%s', newgame=%d, lastgame=%d, clock=%d, lastcityday=%d, encounter_count=%d",
       p_name,
       p_cityname,
       p_mazename,
       p_saganame,
       p_episode,
       p_author,
       p_version,
       p_date,
       p_adventurefile,
       p_newgame,
       p_lastgame,
       clock.stamp(),
       p_lastcityday,
       p_encounter_count );

}

void Game::obj_to_sqlinsert (void)
{

    sprintf (p_querystr, "'%s', '%s', '%s', '%s', %d, '%s', '%s', '%s', '%s', %d, %d, %d, %d, %d",
       p_name,
       p_cityname,
       p_mazename,
       p_saganame,
       p_episode,
       p_author,
       p_version,
       p_date,
       p_adventurefile,
       p_newgame,
       p_lastgame,
       clock.stamp(),
       p_lastcityday,
       p_encounter_count );

}

/*
int Game::SQLinsert ( void )
{
   dbs_Game tmpdat;
   int tmptag;

   strncpy ( tmpdat.adventurefile, p_adventurefile, 12 );
   strncpy ( tmpdat.savegamefile, p_savegamefile, 12 );
   strncpy ( tmpdat.password, p_password, 10 );
   if ( p_pass_protect == true )
      tmpdat.pass_protect = 1;
   else
      tmpdat.pass_protect = 0;
   tmpdat.difficulty = p_difficulty;
   tmpdat.levelup = p_levelup;

   tmptag = db.insert ( DBTABLE_GAME,
      static_cast<void*>(&tmpdat), sizeof ( dbs_Game ) );
   end_insert ( tmptag );

   return ( tmptag );
}

void Game::SQLupdate ( void )
{
   if ( p_readonly == false )
   {
      dbs_Game tmpdat;

   strncpy ( tmpdat.adventurefile, p_adventurefile, 12 );
   strncpy ( tmpdat.savegamefile, p_savegamefile, 12 );
   strncpy ( tmpdat.password, p_password, 10 );
   if ( p_pass_protect == true )
      tmpdat.pass_protect = 1;
   else
      tmpdat.pass_protect = 0;
   tmpdat.difficulty = p_difficulty;
   tmpdat.levelup = p_levelup;

      db.update ( p_reftag,
         static_cast<void*>(&tmpdat), sizeof ( dbs_Game ) );
      end_update();
   }

}

void Game::SQLselect ( int value, bool readonly )
{
   dbs_Game tmpdat;
   if ( db.select ( &tmpdat, value, sizeof ( dbs_Game ) ) == true )
   {

      p_adventurefile = tmpdat.adventurefile;
      p_savegamefile = tmpdat.savegamefile;
      p_password = tmpdat.password;
      if ( tmpdat.pass_protect == 1 )
         p_pass_protect = true;
      else
         p_pass_protect = false;
      p_difficulty = tmpdat.difficulty;
      p_levelup = tmpdat.levelup;

      end_select ( value, readonly );
   }
}

void Game::SQLselect ( unsigned int index, bool readonly )
{
   dbs_Game tmpdat;
   if ( db.select ( &tmpdat, index, sizeof ( dbs_Game ) ) == true )
   {
      p_adventurefile = tmpdat.adventurefile;
      p_savegamefile = tmpdat.savegamefile;
      p_password = tmpdat.password;
      if ( tmpdat.pass_protect == 1 )
         p_pass_protect = true;
      else
         p_pass_protect = false;
      p_difficulty = tmpdat.difficulty;
      p_levelup = tmpdat.levelup;

      end_select ( db.get_tag ( index ), readonly );
   }
}

void Game::DBremove ( void )
{
   // todo remove all player in games
   delete_file ( p_savegamefile.data() );

   db.remove ( p_reftag );
   end_remove();
}                                           */

/*void Game::objdat_to_strdat ( void *dataptr )
{
   dbs_Game &tmpdat = *(static_cast<dbs_Game*> ( dataptr ));

//   strncpy ( tmpdat.adventurefile, p_adventurefile, 12 );
   strncpy ( tmpdat.savegamefile, p_savegamefile, 23 );
   strncpy ( tmpdat.password, p_password, 11 );
   if ( p_pass_protect == true )
      tmpdat.pass_protect = 1;
   else
      tmpdat.pass_protect = 0;
   tmpdat.difficulty = p_difficulty;
   tmpdat.levelup = p_levelup;

   tmpdat.adventure = p_adventure.number();

}

void Game::strdat_to_objdat ( void *dataptr )
{
   dbs_Game &tmpdat = *(static_cast<dbs_Game*> ( dataptr ));

//      p_adventurefile = tmpdat.adventurefile;
      strcpy ( p_savegamefile, tmpdat.savegamefile );
      strcpy ( p_password, tmpdat.password );
      if ( tmpdat.pass_protect == 1 )
         p_pass_protect = true;
      else
         p_pass_protect = false;
      p_difficulty = tmpdat.difficulty;
      p_levelup = tmpdat.levelup;

      p_adventure.number( tmpdat.adventure );


}

void Game::child_DBremove ( void )
{
   // todo remove all player in games ( not sure )
   delete_file ( p_savegamefile );

}*/

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/


/*
const char STR_GAM_DIFFICULTY [] [ 6 ] =
{
   {"Easy "},
   {"Norm "},
   {"Hard "},
};

const char STR_GAM_LEVELUP [] [ 6 ] =
{
   {"Fast "},
   {"Norm "},
   {"Slow "},
};*/

Game game;


