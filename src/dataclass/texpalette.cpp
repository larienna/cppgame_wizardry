/******************************************************************/
/*                                                                */
/*                        T E X P A L E T T E . C P P             */
/*                                                                */
/*    Content: Texture + TexPalette + TexSample Class             */
/*    Programmer: Eric Pietrocupo                                 */
/*    Creation Date: may 5th, 2013                                */
/*                                                                */
/*    This class manage database texture information and preload  */
/*    textures that are going to be used in the maze.             */
/*                                                                */
/******************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

int mazepalid = -1;

//int Texture::p_editor_paletteid = -1; // ID of the edited palette
//int Texture::p_editor_texid = -1; // index of the edited texture
int Texture::p_editor_active_layer = 0; // ID of the currently edited layer
BITMAP *Texture::p_editor_builtex = NULL;


//------------------------------------------------------------------
//-                       Constructor destructor
//------------------------------------------------------------------

Texture::Texture ( void )
{
   //p_tex = create_bitmap ( Texture_SIZE, Texture_SIZE );
   //clear_to_color ( p_tex, makecol (255, 0, 255));

   //p_tex = NULL;
   initialise();

   strcpy (p_tablename, "texture");

}

/*Texture::Texture ( Texture &copytex )
{
   Texture();

   if ( copytex.tex() != NULL )
      blit ( copytex.tex(), p_tex, 0, 0, 0, 0, Texture_SIZE, Texture_SIZE );
   //else
      //p_tex = NULL;

   p_texset = copytex.texset();
   p_type = copytex.type();
   p_paletteid = copytex.paletteid();
   p_texid = copytex.texid();
   p_bgpicid = copytex.bgpicid();
   p_bgposition = copytex.bgposition();
   p_fgpicid = copytex.fgpicid();
   p_fgposition = copytex.fgposition();
}*/

Texture::~Texture ( void )
{
   //destroy_bitmap ( p_tex );
}

void Texture::initialise ( void )
{
   int i;


   //p_primary_key = -1;
   p_paletteid = -1;
   p_texid = -1;

   for ( i = 0; i < Texture_NB_LAYER ; i++ )
   {
      p_texcode [ i ] = -1;
      p_texposition [ i ] = 0;
   }
}

void Texture::clear ( void )
{
   int i;

   for ( i = 0; i < Texture_NB_LAYER ; i++ )
   {
      p_texcode [ i ] = -1;
      p_texposition [ i ] = 0;
   }
}

//------------------------------------------------------------------
//-                        Protperty Methods
//------------------------------------------------------------------

 void Texture::texposition ( int index, int value, int xoffset, int yoffset )
 {
   p_texposition [index] = value;
   p_texposition [index]= p_texposition [index] + xoffset;
   p_texposition [index]= p_texposition [index] + ( yoffset << 4 );

 }

 /*void Texture::build_texposition ( int layer, int xoffset, int yoffset, int zoom, int tiling)
 {
    p_texposition [ layer ] = 0;

    p_texposition [ layer ] += xoffset;
    p_texposition [ layer ] += ( yoffset << 4 );
    p_texposition [ layer ] += (zoom << 14);
 }*/

 int Texture::xoffset ( int layer )
 {
    return ( ( p_texposition [ layer ] & Texture_ALIGN_H_MASK ) );
 }

 int Texture::yoffset ( int layer )
 {
   return ((( p_texposition [ layer ] & Texture_ALIGN_V_MASK ) >> 4 ));
 }

 int Texture::zoom ( int layer )
 {
    return ( (( p_texposition [ layer ] & Texture_ZOOM_MASK ) >> 14 ));
 }
 bool Texture::transparent ( int layer)
 {
    if (  (p_texposition [ layer ] & Texture_TRANSPARENT ) > 0)
       return ( true );
    else
       return ( false );
 }

int Texture::tiling ( int layer )
{
    return ((( p_texposition [ layer ] & Texture_TILING_BOTH ) >> 8 ));
}

int Texture::tiling_xor ( int layer )
{
   return ((( p_texposition [ layer ] & Texture_TILING_XORBOTH ) >> 10 ));
}


 /*bool Texture::upsidedown ( int layer)
 {
    if (  (p_texposition [ layer ] & Texture_UPSIDEDOWN ) > 0)
       return ( true );
    else
       return ( false );
 }*/

void Texture::xoffset_inc ( int layer)
{
   int x = ( p_texposition [ layer ] & Texture_ALIGN_H_MASK );
   int tmpposition = ( p_texposition [ layer ] & ~Texture_ALIGN_H_MASK );

   if ( x < 15)
   {
      x++;
      tmpposition += x;
      p_texposition [ layer ] = tmpposition;
   }
}
void Texture::xoffset_dec ( int layer)
{
   int x = ( p_texposition [ layer ] & Texture_ALIGN_H_MASK );
   int tmpposition = ( p_texposition [ layer ] & ~Texture_ALIGN_H_MASK );

   if ( x > 0)
   {
      x--;
      tmpposition += x;
      p_texposition [ layer ] = tmpposition;
   }
}
void Texture::yoffset_inc ( int layer)
{
   int y = (( p_texposition [ layer ] & Texture_ALIGN_V_MASK ) >> 4 );
   int tmpposition = ( p_texposition [ layer ] & ~Texture_ALIGN_V_MASK );

   if ( y < 15)
   {
      y++;
      tmpposition += ( y << 4);
      p_texposition [ layer ] = tmpposition;
   }
}
void Texture::yoffset_dec ( int layer)
{
   int y = (( p_texposition [ layer ] & Texture_ALIGN_V_MASK ) >> 4 );
   int tmpposition = ( p_texposition [ layer ] & ~Texture_ALIGN_V_MASK );

   if ( y > 0)
   {
      y--;
      tmpposition += ( y << 4);
      p_texposition [ layer ] = tmpposition;
   }
}
void Texture::zoom_inc ( int layer)
{
   int z = (( p_texposition [ layer ] & Texture_ZOOM_MASK ) >> 14 );
   int tmpposition = ( p_texposition [ layer ] & ~Texture_ZOOM_MASK );

   if ( z < 3)
   {
      z++;
      tmpposition += ( z << 14);
      p_texposition [ layer ] = tmpposition;
   }
}
void Texture::zoom_dec ( int layer)
{
   int z = (( p_texposition [ layer ] & Texture_ZOOM_MASK ) >> 14 );
   int tmpposition = ( p_texposition [ layer ] & ~Texture_ZOOM_MASK );

   if ( z > 0)
   {
      z--;
      tmpposition += ( z << 14);
      p_texposition [ layer ] = tmpposition;
   }
}
void Texture::transparent_chg ( int layer)
{

    if (  (p_texposition [ layer ] & Texture_TRANSPARENT ) > 0)
       p_texposition [layer]= ( p_texposition [layer]& ~Texture_TRANSPARENT );
    else
       p_texposition [layer] += Texture_TRANSPARENT;

}

void Texture::tiling_chg ( int layer )
{
   int tiling = (( p_texposition [ layer ] & Texture_TILING_BOTH ) >> 8 );

   int tmpposition = ( p_texposition [ layer ] & ~Texture_TILING_BOTH );

   tiling++;
   if ( tiling > 3)
      tiling = 0;

   tmpposition += ( tiling << 8);
   p_texposition [ layer ] = tmpposition;


}

void Texture::tiling_xor_chg ( int layer )
{
   int tiling = (( p_texposition [ layer ] & Texture_TILING_XORBOTH ) >> 10 );

   int tmpposition = ( p_texposition [ layer ] & ~Texture_TILING_XORBOTH );

   tiling++;
   if ( tiling > 3)
      tiling = 0;

   tmpposition += ( tiling << 10);
   p_texposition [ layer ] = tmpposition;
}


/*#define Texture_TILING_BOTH            768  //   0000 0011 00000000
#define Texture_TILING_XORVERTICAL    1024  //   0000 0100 00000000
#define Texture_TILING_XORHORIZONTAL  2048  //   0000 1000 00000000
#define Texture_TILING_XORBOTH        3072  //   0000 1100 00000000
#define Texture_TILING_MASK           3840  //   0000 1111 00000000
*/

/*void Texture::upsidedown_chg ( int layer)
{
    if (  (p_texposition [ layer ] & Texture_UPSIDEDOWN ) > 0)
       p_texposition [layer] = ( p_texposition [layer]& ~Texture_UPSIDEDOWN );
    else
       p_texposition [layer] += Texture_UPSIDEDOWN;
}*/

/*void Texture::bgposition ( int value, int xoffset, int yoffset )
{
   p_bgposition = value;
   p_bgposition = p_bgposition + xoffset;
   p_bgposition = p_bgposition + ( yoffset << 4 );
}


void Texture::fgposition ( int value, int xoffset, int yoffset )
{
   p_fgposition = value;
   p_fgposition = p_fgposition + xoffset;
   p_fgposition = p_fgposition + ( yoffset << 4 );
}*/

//------------------------------------------------------------------
//-                        Methods
//------------------------------------------------------------------

void Texture::edit ( int paletteid, int texid )
{
   bool exit = false;
   int answer;
   int answer2;
   //int xoffset = 0;
   //int yoffset = 0;
   //unsigned int zoom= 0;
   //unsigned int tiling=0;
   //int i;
   //bool adjust_position = false;

   //p_editor_paletteid = paletteid; // ID of the edited palette
   //p_editor_texid = texid; // index of the edited texture
   p_editor_active_layer = 0;
   p_editor_builtex = create_bitmap ( 256, 256 );

   if ( p_primary_key == -1)
   {
       p_paletteid = paletteid;
       p_texid = texid;

   }

//printf ("Debug:TexPalette:Edit: After init var pal=%d, tex=%d\n", p_paletteid, p_texid );

   clear_to_color ( p_editor_builtex, makecol ( 0, 0, 0));

   //mainloop_readkeyboard();

   ImageList imglst_texsample ("Select your texture sample", 4, 3, 128, 128, true);

   imglst_texsample.add_datref ( datref_texture, 2 );

   // temporary code, need to load whole datatile
   //for ( i = 2048 ; i < 2248 ; i++ )
   //   imglst_texsample.add_item ( i, datref_texture [ i ]);

   WinImageList wimglst_texsample (imglst_texsample, 52, 0, false);
   wimglst_texsample.hide();

   while ( exit == false)
   {
      /*if ( adjust_position == true)
      {
         build_texposition ( p_editor_active_layer, xoffset, yoffset, zoom, tiling);
         adjust_position == false;
      }*/
      draw_background();

      answer = mainloop_readkeyboard();

      switch ( answer )
      {
         case KEY_OPENBRACE:
            if ( p_editor_active_layer > 0)
               p_editor_active_layer--;
         break;

         case KEY_CLOSEBRACE:
            if ( p_editor_active_layer < Texture_NB_LAYER - 1)
               p_editor_active_layer++;
         break;

         case KEY_Z:
            wimglst_texsample.unhide();
            answer2 = Window::show_all();
            if ( answer2 != -1 )
            {
               p_texcode [ p_editor_active_layer ] = answer2;
            }
            wimglst_texsample.hide();
         break;

         case KEY_C:
            clear();
         break;

         case KEY_S:
            if ( p_primary_key == -1)
            {
               //p_paletteid = p_editor_paletteid;
               //p_texid = p_editor_texid;
               //printf ("Debug:TexPalette: insert: pal=%d, tex=%d\n", p_paletteid, p_texid);
               SQLinsert();
            }
            else
            {
               //printf ("Debug:TexPalette: update: pal=%d, tex=%d\n", p_paletteid, p_texid);

               SQLupdate();

            }
            exit = true;
         break;

         case KEY_R:
            if ( p_primary_key > -1 )
               SQLselect ( p_primary_key );
         break;

         case KEY_ESC:
            exit = true;
            if ( p_primary_key == -1 )
               initialise();
         break;

         case KEY_UP:
            yoffset_dec( p_editor_active_layer);
         break;
         case KEY_DOWN:
            yoffset_inc( p_editor_active_layer);
         break;
         case KEY_LEFT:
            xoffset_dec(p_editor_active_layer);
         break;
         case KEY_RIGHT:
            xoffset_inc(p_editor_active_layer);
         break;
         case KEY_EQUALS:
            zoom_inc( p_editor_active_layer);
         break;
         case KEY_MINUS:
            zoom_dec (p_editor_active_layer);
         break;
         case KEY_T:
            transparent_chg (p_editor_active_layer);
         break;
         case KEY_I:
            tiling_chg ( p_editor_active_layer);
         break;
         case KEY_O:
            tiling_xor_chg ( p_editor_active_layer);
         break;
         case KEY_SPACE:

            //sprintf ( shotstr, "imglistshot%03d.bmp", shotID);
//            make_screen_shot ( "textureditshot.bmp"  );
            //shotID++;
         break;
         /*case KEY_U:
            upsidedown_chg (p_editor_active_layer);
         break;*/
      }
   }

   destroy_bitmap ( p_editor_builtex );
}

void Texture::build ( BITMAP *bmp, bool door )
{
   //BITMAP* tmptex;
   BITMAP* tmpdoor;
   int initx;
   int inity;
   int x;
   int y;
   int width;
   int height;
   bool xorx = true;
   bool xory = true;
   BITMAP* tmppic;
   int i;
   bool drawpic = false;
   unsigned int tmposition;
   int xoffset = 0;
   int yoffset = 0;
   int zoom = 0;
   int zoomexp = 0;
   bool texisempty = true;
   //BITMAP* tmptex = create_bitmap ( Texture_SIZE, Texture_SIZE );
   //int category = 0;

   /*if ( p_type >= Texture_TYPE_FLOOR && p_type <= Texture_TYPE_MASKEDFLOOR )
      category = Texture_CATEGORY_FLOOR;

   if ( p_type >= Texture_TYPE_WALL && p_type <= Texture_TYPE_GRID )
      category = Texture_CATEGORY_WALL;*/


   //tmptex = create_bitmap ( Texture_SIZE, Texture_SIZE );
   clear_to_color ( bmp, makecol (255, 0, 255));
   //clear_to_color ( tmptex, makecol (255, 0, 255));

   // security check for undefined texture set and type (worst case picture is left empty
   //if ( p_texset != -1 && p_type != -1)
   //{


      // ----- Background  and foreground -----

      //printf ("Texture: PAss -1\r\n");



      for ( i = 0; i < Texture_NB_LAYER; i++ )
      {
         drawpic = false;

         //printf ("Texture: PAss 0\r\n");

         // security check and chose the right information from the right layer.
         if ( p_texcode [ i ] != -1 )
         {
            tmppic = datref_texture [ p_texcode [ i ] ];
            width = tmppic->w;
            height = tmppic->h;
            tmposition = p_texposition [ i ];

            drawpic = true;
            //printf ("Texture: PAss 1\r\n");
         }

         if ( drawpic == true)
         {
            texisempty = false;
            //printf ("Texture: Pass 3\r\n");

            // --- offset position ---

            xoffset = ( tmposition & Texture_ALIGN_H_MASK );
            yoffset = ( ( tmposition & Texture_ALIGN_V_MASK) >> 4);

            initx =  xoffset * ( Texture_SIZE / 16);
            inity =  yoffset * ( Texture_SIZE / 16);

            // --- zoom ---


            zoomexp = ( ( tmposition & Texture_ZOOM_MASK ) >> 14 );
            zoom =  (int) pow (2,zoomexp);

            //printf ("debug: zoomexp=%d, zoom=%d\n", zoomexp, zoom);

            // --- Tiling ---

            x = initx;
            y = inity;
            xorx = true;
            xory = true;

            do
            {
               do
               {
                  if ( xorx == xory )
                  {


                     /*if ( zoom == 1 )
                     {
                        if ( ( tmposition & TexPalette_TRANSPARENT) > 0 )
                           masked_blit ( tmppic, tmptex, 0, 0, x, y, width, height );
                        else
                           blit ( tmppic, tmptex, 0, 0, x, y, width, height );
                     }
                     else
                     {*/

                     /*if ( config.get ( Config_ANTI_ALLIASING) == Config_YES)
                     {
                        if ( ( tmposition & Texture_TRANSPARENT) > 0 )
                           aa_stretch_blit ( tmppic, bmp, 0, 0, width, height, x, y, width * zoom, height * zoom );
                        else
                        {
                           aa_set_mode ( 0 );

                           aa_stretch_blit ( tmppic, bmp, 0, 0, width, height, x, y, width * zoom, height * zoom );
                           aa_set_mode ( AA_MASKED );
                        }
                     }
                     else
                     {*/

                        if ( ( tmposition & Texture_TRANSPARENT) > 0 )
                           masked_stretch_blit ( tmppic, bmp, 0, 0, width, height, x, y, width * zoom, height * zoom );
                        else
                           stretch_blit ( tmppic, bmp, 0, 0, width, height, x, y, width * zoom, height * zoom );
                     //}
                     //}

                     /*if ( ( tmposition & Texture_UPSIDEDOWN) > 0 )
                        draw_sprite_v_flip ( bmp, bmp, 0, 0);*/
                  }
/*
void stretch_blit(BITMAP *source, BITMAP *dest,
                  int source_x, source_y, source_width, source_height,
                  int dest_x, dest_y, dest_width, dest_height);

                  void masked_stretch_blit(BITMAP *source, BITMAP *dest,
                         int source_x, source_y, source_w, source_h,
                         int dest_x, dest_y, dest_w, dest_h);*/

                  y = y + height * zoom;

                  // abort vertical tiling
                  if ( ( tmposition & Texture_TILING_VERTICAL ) == 0)
                     y = Texture_SIZE + 1;
                  // switch xor tiling
                  if ( ( tmposition & Texture_TILING_XORVERTICAL) > 0 )
                     {  xory == true ? xory = false : xory = true; }
                     /*if ( xory == true)
                        { xory = false; }
                     else
                        { xory = true; }*/
               }
               while ( y + height * zoom <= Texture_SIZE);

               x = x + width * zoom;
               y = inity;

               // abort horizontal tiling
               if ( ( tmposition & Texture_TILING_HORIZONTAL ) == 0)
                  x = Texture_SIZE + 1;
               if ( ( tmposition & Texture_TILING_XORHORIZONTAL) > 0 )
                  { xorx == true ? xorx = false : xorx = true; }
                     //if ( xorx == true) { xorx = false;  } else { xorx = true; }
            }
            while ( x + width * zoom <= Texture_SIZE );

         }

      }

   //}



   if ( door == true )
   {
      texisempty = false;
      tmpdoor =  create_bitmap ( Texture_SIZE, Texture_SIZE );

      clear_to_color ( tmpdoor, makecol (255, 0, 255));
      int threequart = (TEXSIZE * 3) / 4;
      int quart = TEXSIZE / 4;

      //if ( config.get ( Config_ANTI_ALLIASING) == Config_YES)
      //   aa_stretch_blit ( tmptex, tmpdoor, 0, 0, TEXSIZE, TEXSIZE, quart / 2, quart, threequart, threequart );
      //else
         stretch_blit ( bmp, tmpdoor, 0, 0, TEXSIZE, TEXSIZE, quart / 2, quart, threequart, threequart );

      blit (tmpdoor, bmp, 0, 0, 0, 0, TEXSIZE, TEXSIZE );

      destroy_bitmap (tmpdoor);

//      draw_sprite ( screen, tmpdoor, 0, 0 );
  //    draw_sprite ( screen, tmptex, 256, 0 );

    //  while ( mainloop_readkeyboard() != KEY_ENTER );

      //return (tmpdoor);
   }
   /*else
   {
      if ( config.get ( Config_ANTI_ALLIASING) == Config_YES)
      {
         aa_set_mode ( 0 );
         aa_stretch_blit ( tmptex, bmp, 0, 0, TEXSIZE, TEXSIZE, 0, 0, TEXSIZE, TEXSIZE );
         aa_set_mode ( AA_MASKED );
      }
      else
         blit ( tmptex, bmp, 0, 0, 0, 0, TEXSIZE, TEXSIZE );
   }*/

   //destroy_bitmap (tmptex);

   if ( texisempty == true)
      clear_to_color ( bmp, makecol (128, 128, 255));
   //else
     // return (tmptex);

}

void Texture::draw_background ( void  )
{
   int i;
   int x;
   //unsigned int tmposition = 0;
   char tmpstr [100] = "";
   //BITMAP *tmpbuild = create_bitmap (256, 256);

   textprintf_centre_old ( buffer, FNT_print, 320, 0, General_COLOR_TEXT
                      , "Texture Editor" );

   if ( p_primary_key == -1)
   {
      textprintf_old (buffer, FNT_print, 0, 16, General_COLOR_TEXT,
               "Editing palette: %d and texture: %s (NEW TEXTURE)",
               p_paletteid, STR_TEX_PALETTE [ p_texid ] );
   }
   else
   textprintf_old (buffer, FNT_print, 0, 16, General_COLOR_TEXT,
               "Editing palette: %d and texture: %s",
               p_paletteid, STR_TEX_PALETTE [ p_texid ] );

   // ----- Layer Textures -----

   x = 0;
   for ( i = 0 ; i < Texture_NB_LAYER; i++)
   {
      textprintf_centre_old ( buffer, FNT_small, x+66, 32, General_COLOR_TEXT,
                         "Layer %d", i );
      if ( i == p_editor_active_layer)
         rect ( buffer, x+1, 48, x+130, 177, makecol (255, 255, 255 ) );

      if ( p_texcode [ i ] != -1)
      {
         clear_to_color ( p_editor_builtex, makecol (255, 0, 255));
         draw_sprite (p_editor_builtex, datref_texture [ p_texcode [ i ] ], 0, 0);
         stretch_sprite ( buffer, p_editor_builtex, x+2, 49, 128, 128);

         //tmposition = p_texposition [ i ];

         sprintf ( tmpstr, "X:%2d Y:%2d %dx ",
                  xoffset (i),
                  yoffset (i),
                  zoom (i) + 1 );

         if ( transparent( i) == true)
            strcat ( tmpstr, " Tr");

         if ( tiling ( i ) > 0 )
            strcat ( tmpstr, " Ti");

         if ( tiling_xor ( i ) > 0 )
            strcat ( tmpstr, " Xor");

         //if ( upsidedown( p_editor_active_layer) == true)
         //   strcat ( tmpstr, "U");

         textout_old ( buffer, FNT_small, tmpstr, x, 178, General_COLOR_TEXT );

      }
      else
      {
         line ( buffer, x, 48, x+130, 177, makecol (255, 255, 255 ) );
         line ( buffer, x, 177, x+130, 48, makecol (255, 255, 255 ) );
      }

      x+=132;
   }

   // ----- Built Texture -----

   clear_to_color ( p_editor_builtex, makecol (255, 0, 255));
   build( p_editor_builtex );

   rect (buffer, 0, 192, 257, 449, makecol (255, 255, 255 ) );
   draw_sprite ( buffer, p_editor_builtex, 1, 193 );

   // ----- Display menu/controls -----

   textprintf_old (buffer, FNT_print, 300, 192, General_COLOR_TEXT,
               "ESC: Exit without saving" );
   textprintf_old (buffer, FNT_print, 300, 208, General_COLOR_TEXT,
               "[ ]: Change active layer" );
   textprintf_old (buffer, FNT_print, 300, 224, General_COLOR_TEXT,
               "+-: Change texture zoom (not keypad +-)" );
   textprintf_old (buffer, FNT_print, 300, 240, General_COLOR_TEXT,
               "UpDnLfRg: Move texture" );
   textprintf_old (buffer, FNT_print, 300, 256, General_COLOR_TEXT,
               "Z: Select Texture" );
   textprintf_old (buffer, FNT_print, 300, 272, General_COLOR_TEXT,
               "C: Clear Texture" );

   if ( p_primary_key == -1 )
   {
      textprintf_old (buffer, FNT_print, 300, 288, General_COLOR_TEXT,
               "S: Save Texture (Insert as new)" );
      textprintf_old (buffer, FNT_print, 300, 304, General_COLOR_DISABLE ,
               "R: Reload Texture" );
   }
   else
   {
      textprintf_old (buffer, FNT_print, 300, 288, General_COLOR_TEXT,
               "S: Save Texture (Update)" );
      textprintf_old (buffer, FNT_print, 300, 304, General_COLOR_TEXT ,
               "R: Reload Texture" );
   }

   textprintf_old (buffer, FNT_print, 300, 320, General_COLOR_TEXT ,
               "T: Transparent" );

   textprintf_old (buffer, FNT_print, 300, 336, General_COLOR_TEXT ,
               "I: Tiling" );

   textprintf_old (buffer, FNT_print, 300, 352, General_COLOR_TEXT ,
               "O: ExclusiveOR Tiling" );


   //textprintf_old (buffer, FNT_print, 300, 336, General_COLOR_TEXT ,
   //            "U: Up Side Down" );


   copy_buffer();
}

//------------------------------------------------------------------
//-                       Virtual Methods
//------------------------------------------------------------------

void Texture::sql_to_obj (void)
{


   p_primary_key = SQLcolumn_int (0);
   p_paletteid = SQLcolumn_int (1);
   p_texid = SQLcolumn_int (2);
   p_texcode [0] = SQLcolumn_int (3);
   p_texcode [1] = SQLcolumn_int (4);
   p_texcode [2] = SQLcolumn_int (5);
   p_texcode [3] = SQLcolumn_int (6);
   p_texposition [0] = SQLcolumn_int (7);
   p_texposition [1] = SQLcolumn_int (8);
   p_texposition [2] = SQLcolumn_int (9);
   p_texposition [3] = SQLcolumn_int (10);



}

void Texture::template_sql_to_obj ( void)
{
   // not used
}

void Texture::obj_to_sqlupdate (void)
{
   sprintf ( p_querystr, "paletteid=%d, texid=%d, texcode1=%d, texcode2=%d, texcode3=%d, texcode4=%d, texposition1=%d, texposition2=%d, texposition3=%d, texposition4=%d",
      p_paletteid,
      p_texid,
      p_texcode [0],
      p_texcode [1],
      p_texcode [2],
      p_texcode [3],
      p_texposition [0],
      p_texposition [1],
      p_texposition [2],
      p_texposition [3] );


}

void Texture::obj_to_sqlinsert (void)
{
   sprintf ( p_querystr, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
      p_paletteid,
      p_texid,
      p_texcode [0],
      p_texcode [1],
      p_texcode [2],
      p_texcode [3],
      p_texposition [0],
      p_texposition [1],
      p_texposition [2],
      p_texposition [3] );
}

//------------------------------------------------------------------
//-                        Procedures
//------------------------------------------------------------------

/*void build_texture_palette ( s_Texture_palette &tmppal, int paletteid )
{
   int i;
   // add loading loop here for SQL data
   Texture tmptex;

   for ( i = 0 ; i < TexPalette_NB_TEXTURE; i++ )
      tmppal.tex [i] = tmptex.build();
*/
   /*tmppal.floor = tmptex.build();
   tmppal.ceiling = tmptex.build();

   for ( i = 0; i < Texture_NB_WALL; i++ )
      tmppal.wall [ i ] = tmptex.build();

   for ( i = 0; i < Texture_NB_GRID; i++)
      tmppal.grid [ i ] = tmptex.build();

   for ( i = 0; i < Texture_NB_MASKEDWALL; i++)
      tmppal.maskedwall [ i ] = tmptex.build();

   for ( i = 0; i < Texture_NB_MASKEDFLOOR; i++)
      tmppal.maskedfloor [ i ] = tmptex.build();*/

//}

/*void load_texture_palette ( s_Texture_palette &tmppal  )
{

}

void save_texture_palette ( s_Texture_palette tmppal  )
{

}*/

/*void destroy_texture_palette ( s_Texture_palette tmppal)
{
   int i;

   for ( i = 0 ; i < TexPalette_NB_TEXTURE; i++ )
      destroy_bitmap ( tmppal.tex [i]);
*/
   /*destroy_bitmap ( tmppal.floor );
   destroy_bitmap ( tmppal.ceiling );

   for ( i = 0; i < Texture_NB_WALL; i++ )
      destroy_bitmap ( tmppal.wall [ i ] );

   for ( i = 0; i < Texture_NB_GRID; i++)
      destroy_bitmap ( tmppal.grid [ i ] );

   for ( i = 0; i < Texture_NB_MASKEDWALL; i++)
      destroy_bitmap ( tmppal.maskedwall [ i ] );

   for ( i = 0; i < Texture_NB_MASKEDFLOOR; i++)
      destroy_bitmap ( tmppal.maskedfloor [ i ] );*/

//}

//------------------------------------------------------------------
//-
//-                    TexPalette Class
//-
//------------------------------------------------------------------

TexPalette::TexPalette ( void )
{

}

TexPalette::~TexPalette ( void )
{
   //destroy(); //creates a bug if unload after allegroexit
}

// Property methods

/*BITMAP* TexPalette::bmp ( int index )
{
   return ( p_bmp[i] );
}

void TexPalette::tex ( int index, int primary_key )
{

}

int TexPalette::tex ( int index )
{
   return ( p_tex[i].primary_key() );
}*/

   // Methods

void TexPalette::load ( int index )
{
   int errorsql;
   Texture tmptex;

   unload();

   errorsql = tmptex.SQLpreparef ( "WHERE paletteid=%d", index );

   if ( errorsql == SQLITE_OK )
   {
      errorsql = tmptex.SQLstep();

      while ( errorsql == SQLITE_ROW )
      {
         tex [ tmptex.texid()].SQLselect ( tmptex.primary_key());
         //tex [ tmptex.texid() ] = tmptex;

         errorsql = tmptex.SQLstep();
      }

      tmptex.SQLfinalize();
   }


}

void TexPalette::unload ( void )
{

   int i;

   clear ();

   for ( i = 0 ; i < TexPalette_NB_TEXTURE; i++ )
   {
      tex [ i ].initialise ();
      tex [ i ].primary_key ( -1 );
   }

}

/*void TexPalette::update ( void )
{
   int i;

   for ( i = 0; i < TexPalette_NB_TEXTURE ; i++)
   {
      tex [ i ]. SQLupdate();
   }
}

void TexPalette::insert ( int index )
{
   int i;

   for ( i = 0; i < TexPalette_NB_TEXTURE ; i++)
   {
      tex [ i ].paletteid ( index ); // in case it was not set. Avoid inconsistencies
      tex [ i ].texid (i);
      tex [ i ].SQLinsert();
   }
}*/

void TexPalette::build ( void )
{
   int i;
   // add loading loop here for SQL data
   //Texture tmptex;

   for ( i = 0 ; i < TexPalette_NB_TEXTURE; i++ )
   {
      if ( i == TexPal_IDX_DOOR )
         tex[i].build( bmp [i], true );
      else
         tex[i].build( bmp [i] );

   }
}

void TexPalette::clear ( void )

{
   int i;

   for ( i = 0 ; i < TexPalette_NB_TEXTURE; i++ )
   {
      clear_to_color ( bmp [ i ], makecol ( 128, 128, 255) );
      //textout_centre_old ( bmp [ i ], FNT_print, "Empty Texture", 128, 120, General_COLOR_TEXT );
   }
}

void TexPalette::create ( void )
{
   int i;

   for ( i = 0; i < TexPalette_NB_TEXTURE; i++)
      bmp [i] = create_bitmap ( Texture_SIZE, Texture_SIZE );

   clear();
}

void TexPalette::destroy ( void )
{
   int i;

   for ( i = 0 ; i < TexPalette_NB_TEXTURE; i++ )
   {


      destroy_bitmap ( bmp [i] );
      //printf ("TexPalette::destroy: i=%d", i );
   }
}

//------------------------------------------------------------------
//-
//-                    TexSample Class
//-
//------------------------------------------------------------------


void TexSample::load_build ( void )
{
   int errorsql;
   Texture tmptex;
   BITMAP* tmpbmp = create_bitmap (256, 256);

   errorsql = tmptex.SQLpreparef ( "WHERE texid BETWEEN %d AND %d",
                                   TexPal_IDX_WALL,
                                   TexPal_IDX_CEILING );

   if ( errorsql == SQLITE_OK )
   {
      errorsql = tmptex.SQLstep();

      while ( errorsql == SQLITE_ROW )
      {
         if ( tmptex.texid() == TexPal_IDX_DOOR )
            tmptex.build ( tmpbmp, true );
         else
            tmptex.build ( tmpbmp );

         switch ( tmptex.texid())
         {
            case TexPal_IDX_WALL :
               stretch_sprite ( bmp [ tmptex.paletteid()], tmpbmp, 0,0, 128, 128 );
            break;
            case TexPal_IDX_DOOR :
               stretch_sprite ( bmp [ tmptex.paletteid()], tmpbmp, 128,0, 128, 128 );
            break;
            case TexPal_IDX_FLOOR :
               stretch_sprite ( bmp [ tmptex.paletteid()], tmpbmp, 0,128, 128, 128 );
            break;
            case TexPal_IDX_CEILING :
               stretch_sprite ( bmp [ tmptex.paletteid()], tmpbmp, 128,128, 128, 128 );
            break;

         }
         //tex [ tmptex.texid() ] = tmptex;

         errorsql = tmptex.SQLstep();
      }

      tmptex.SQLfinalize();
   }

   destroy_bitmap (tmpbmp);

   /*for ( i = 0 ; i < TexPalette_NB_TEXTURE; i++ )
   {
      if ( i == TexPal_IDX_DOOR )
         tex[i].build( bmp [i], true );
      else
         tex[i].build( bmp [i] );

   }*/

}

void TexSample::create ( void )
{
   int i;

   for ( i = 0 ; i < TexSample_NB_TEXPALETTE; i++)
   {
      bmp [i] = create_bitmap (256, 256);
      clear_to_color ( bmp[i], makecol (128, 128, 255));
   }
}

void TexSample::destroy ( void )
{
   int i;

   for ( i = 0 ; i < TexSample_NB_TEXPALETTE; i++)
   {
      destroy_bitmap ( bmp[i]);
   }
}

//------------------------------------------------------------------
//-                       global variables
//------------------------------------------------------------------

TexPalette mazepal; // palette used for the maze and for display in the editor of active tile
TexPalette editorpal; // palette used for active palette in editor.
TexSample palsample;

const char STR_TEX_PALETTE [][25] =
{
   {"Wall"},
   {"Door"},
   {"Floor"},
   {"Ceiling"},
   {"Grid 01"},
   {"Grid 02"},
   {"Grid 03"},
   {"Grid 04"},

   {"Masked Wall 01"},
   {"Masked Wall 02"},
   {"Masked Wall 03"},
   {"Masked Wall 04"},
   {"Masked Wall 05"},
   {"Masked Wall 06"},
   {"Masked Wall 07"},
   {"Masked Wall 08"},

   {"Masked Wall 09"},
   {"Masked Wall 10"},
   {"Masked Wall 11"},
   {"Masked Wall 12"},
   {"Masked Wall 13"},
   {"Masked Wall 14"},
   {"Masked Wall 15"},
   {"Masked Wall 16"},

   {"Masked Floor/Ceiling 01"},
   {"Masked Floor/Ceiling 02"},
   {"Masked Floor/Ceiling 03"},
   {"Masked Floor/Ceiling 04"},
   {"Masked Floor/Ceiling 05"},
   {"Masked Floor/Ceiling 06"},
   {"Masked Floor/Ceiling 07"},
   {"Masked Floor/Ceiling 08"},

   {"Masked Floor/Ceiling 09"},
   {"Masked Floor/Ceiling 10"},
   {"Masked Floor/Ceiling 11"},
   {"Masked Floor/Ceiling 12"},
   {"Masked Floor/Ceiling 13"},
   {"Masked Floor/Ceiling 14"},
   {"Masked Floor/Ceiling 15"},
   {"Masked Floor/Ceiling 16"}


};


//s_Texture_palette texpal;

//BITMAP* texpal [ TexPalette_NB_TEXTURE ];
