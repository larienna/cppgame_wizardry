/************************************************************************/
/*                                                                      */
/*                E n h a n c e d S Q L o b j e c t . c p p             */
/*                                                                      */
/*   Content: Class EnhancedSQLobject                                   */
/*   Programmer: Eric Pietrocupo                                        */
/*   Starting Date: November, 4th, 2014                                 */
/*                                                                      */
/*   An attempt to encapsulate the management of datafield of the child */
/*   object to avoid the requirement of defining Conversion functions.  */
/*                                                                      */
/*   Backward compatible with the original SQLobject because data obj   */
/*   will inherint from Enhanced SQL obj which will inherit from sqlobj */
/*                                                                      */
/************************************************************************/

#include <grpstd.h>
#include <grpsql.h>


const char EnhancedSQLobject::ERRORSTR [ 6 ] = "Error";

/*------------------------------------------------------------------------*/
/*-                   Constructor and Destructor                         -*/
/*------------------------------------------------------------------------*/

EnhancedSQLobject::EnhancedSQLobject ( void )
{
   p_SQLfield_ptr = NULL;
   p_SQLdata_ptr = NULL;
   p_nb_field = 0;
}

EnhancedSQLobject::~EnhancedSQLobject ( void )
{


}

/*------------------------------------------------------------------------*/
/*-                    Property Methods                                  -*/
/*------------------------------------------------------------------------*/

void EnhancedSQLobject::set_all_int ( int value )
{
   for ( int i = 0; i < p_nb_field ; i++)
      set_int ( i, value );
}

void EnhancedSQLobject::set_all_str ( const char *str )
{
   for ( int i = 0; i < p_nb_field ; i++)
      set_str ( i, str );

}

int EnhancedSQLobject::get_int ( int index )
{
   if ( index < p_nb_field && index >= 0 )
      if ( p_SQLfield_ptr [index].type == INTEGER
          || p_SQLfield_ptr [index].type == STRFIELD )
         return ( p_SQLdata_ptr [ index ] . value );


   return (0);
}

void EnhancedSQLobject::set_int ( int index, int value )
{
   if ( index < p_nb_field && index >= 0 )
      if ( p_SQLfield_ptr [ index ] . type == INTEGER
          || p_SQLfield_ptr [index].type == STRFIELD )
         p_SQLdata_ptr [ index ] . value = value;

};

const char* EnhancedSQLobject::get_str ( int index )
{
   if ( index < p_nb_field && index >= 0 )
      if ( p_SQLfield_ptr [ index ] . type == TEXT
          || p_SQLfield_ptr [index].type == STRFIELD )
         return ( p_SQLdata_ptr [ index ] . str );

   return ( ERRORSTR );
};

void EnhancedSQLobject::set_str ( int index, const char *str )
{
   bool error = true;
   if ( index < p_nb_field && index >= 0 )
      if ( p_SQLfield_ptr [ index ] . type == TEXT
          || p_SQLfield_ptr [index].type == STRFIELD )
      {
         strncpy ( p_SQLdata_ptr [index] . str, str, p_SQLfield_ptr [ index ] . length);
         error = false;
      }

   if ( error == true)
      strncpy ( p_SQLdata_ptr [index] . str, ERRORSTR, p_SQLfield_ptr [ index ] . length);

};

const char* EnhancedSQLobject::get_SQLname ( int index )
{
   if ( index < p_nb_field && index >= 0 )
      return ( p_SQLfield_ptr [ index ] . SQLname);

   return ( ERRORSTR );

}

int EnhancedSQLobject::get_length ( int index)
{
   if ( index < p_nb_field && index >= 0 )
      return ( p_SQLfield_ptr [ index ] . length );

   return ( 0 );
}

int EnhancedSQLobject::get_type ( int index )
{
   if ( index < p_nb_field && index >= 0 )
      return ( p_SQLfield_ptr [ index ] . type );

   return ( 0 );
}

/*int EnhancedSQLobject::get_template_field_id ( int index )
{
   if ( index < p_nb_field && index >= 0 )
      return ( p_SQLfield_ptr [ index ] . template_field_id );

   return ( -1);
}*/

const char* EnhancedSQLobject::get_display_name (int index)
{
   if ( index < p_nb_field && index >= 0 )
      return ( p_SQLfield_ptr [ index ] . display_name);

   return ( ERRORSTR );
}

bool EnhancedSQLobject::get_display ( int index )
{
    if ( index < p_nb_field && index >= 0 )
      return ( p_SQLfield_ptr [ index ] . display );

   return ( false);
}


/*------------------------------------------------------------------------*/
/*-                                        Methods                       -*/
/*------------------------------------------------------------------------*/


void EnhancedSQLobject::print_table_structure ( void )
{
   for ( int i = 0; i < p_nb_field; i++ )
   {
      printf ( "Field %d: name: %s, type %d, length %d\n", i, p_SQLfield_ptr [ i ] . SQLname,
               p_SQLfield_ptr [ i ] . type, p_SQLfield_ptr [ i ] . length );
   }
}

void EnhancedSQLobject::strfield_to_bitfield (int index)
{
  // printf ("Debug: ESQLOBJ: strfield to bitfield: Start\n");
   if ( index < p_nb_field && index >= 0)
   {
      int bitfield = 1;
      int i;

      ///s_EnhancedSQLobject_strfield

      if ( p_SQLfield_ptr [ index ] . type == STRFIELD )
      {

         p_SQLdata_ptr [ index ] . value = 0;

         for ( i = 0; i < EnhancedSQLobject_HASH_SIZE ; i++ )
         {
           /*if ( strcmp ( p_tablename, "effect") == 0)
               printf ( "Debug: ESQLobj: strfield to bitfield: i = %d, code = %s\n",
                       p_SQLfield_ptr [ index ].hash[i].code);
*/
            if ( p_SQLfield_ptr [ index ].hash[i].code[0] != '?')
            {
               if ( strstr ( p_SQLdata_ptr [ index] . str , p_SQLfield_ptr [ index ].hash[i].code ) != NULL )
               {
                  p_SQLdata_ptr [ index ] . value += bitfield;
               }
            }
            bitfield = bitfield << 1;
         }
      }
   }
}

void EnhancedSQLobject::bitfield_to_strfield (int index)
{
  // printf ("Debug: ESQLOBJ: bitfield to strfield: Start\n");
   if ( index < p_nb_field && index >= 0)
   {
      int bitfield = 1;
      int i;

      ///s_EnhancedSQLobject_strfield

      if ( p_SQLfield_ptr [ index ] . type == STRFIELD )
      {
         strcpy ( p_SQLdata_ptr [ index ] . str, "" );
         for ( i = 0; i < EnhancedSQLobject_HASH_SIZE ; i++ )
         {
            /*if ( strcmp ( p_tablename, "effect") == 0)
               printf ( "Debug: ESQLobj:  bitfield to strfield : i = %d, code = %s\n",
                       p_SQLfield_ptr [ index ].hash[i].code);
*/
            if ( p_SQLfield_ptr [ index ].hash[i].code[0] != '?')
            {
               if ( (bitfield & p_SQLdata_ptr [ index ] . value ) > 0 )
               {
                  strcat ( p_SQLdata_ptr [ index ] . str, p_SQLfield_ptr [ index ].hash[i].code ) ;
               }
            }
            bitfield = bitfield << 1;
         }
      }
   }
}

int EnhancedSQLobject::strfield_to_bitfield ( s_EnhancedSQLobject_strfield hash[], const char *str ) // convert external str
{

      int bitfield = 1;
      int i;
      int value = 0;

      ///s_EnhancedSQLobject_strfield


         value = 0;

         for ( i = 0; i < EnhancedSQLobject_HASH_SIZE ; i++ )
         {

            if ( hash[i].code[0] != '?')
            {
               if ( strstr (  str , hash[i].code ) != NULL )
               {
                  value += bitfield;
               }
            }
            bitfield = bitfield << 1;
         }

   return value;

}

void EnhancedSQLobject::bitfield_to_strfield ( s_EnhancedSQLobject_strfield hash[], char *str, int bitfield ) // convert external bitfield
{
      int tmpbitfield = 1;
      int i;

      ///s_EnhancedSQLobject_strfield


         strcpy ( str, "" );
         for ( i = 0; i < EnhancedSQLobject_HASH_SIZE ; i++ )
         {
            /*if ( strcmp ( p_tablename, "effect") == 0)
               printf ( "Debug: ESQLobj:  bitfield to strfield : i = %d, code = %s\n",
                       p_SQLfield_ptr [ index ].hash[i].code);
*/
            if ( hash[i].code[0] != '?')
            {
               if ( (tmpbitfield & bitfield ) > 0 )
               {
                  strcat ( str, hash[i].code ) ;
               }
            }
            tmpbitfield = tmpbitfield << 1;
         }


}

/*------------------------------------------------------------------------*/
/*-                    Virtual Function Definition                       -*/
/*------------------------------------------------------------------------*/

void EnhancedSQLobject::sql_to_obj (void)
{
   //int j = 1;
   p_primary_key = SQLcolumn_int (0);

   for ( int i = 0; i < p_nb_field; i++)
   {
      switch ( get_type (i))
      {
         case INTEGER:
            set_int ( i , SQLcolumn_int ( i + 1 ));
         break;
         case TEXT:
            set_str ( i, SQLcolumn_text ( i + 1 ) );
         break;
         case STRFIELD:
            set_str ( i, SQLcolumn_text ( i + 1 ));
            strfield_to_bitfield (i);
         break;
         default:
            printf ("Error: EnhancedSQLobject::sql_to_obj: Unknown Type or index out of bound\n");
         break;
      }
      //j++;
   }



}

void EnhancedSQLobject::template_sql_to_obj (void)
{
   //int j;
   p_primary_key = -1;

   for ( int i = 0; i < p_nb_field; i++)
   {
     // j = get_template_field_id ( i );

//      if ( j > 0 )
  //    {
         switch ( get_type (i))
         {
            case INTEGER:
               set_int ( i , SQLcolumn_int ( i + 1) );
            break;
            case TEXT:
               set_str ( i, SQLcolumn_text ( i + 1) );
            break;
            case STRFIELD:
               set_str ( i, SQLcolumn_text ( i + 1) );
               strfield_to_bitfield (i);
            break;
            default:
               printf ("Error: EnhancedSQLobject::template_sql_to_obj: Unknown Type or index out of bound\n");
            break;
         }
   //   }
   }


   template_load_completion ();

}

void EnhancedSQLobject::obj_to_sqlupdate (void)
{
   strcpy (p_querystr, "");
   char buffer [ 32 ];

   for ( int i = 0 ; i < p_nb_field ; i++)
   {
      switch ( get_type (i))
      {
         case INTEGER:
            if ( i != 0)
               strcat ( p_querystr, ", ");

            sprintf ( buffer, "%s=%d", get_SQLname(i), get_int (i));
            strcat (p_querystr, buffer);
         break;
         case STRFIELD:
            bitfield_to_strfield (i);
         case TEXT:
            if ( i != 0 )
               strcat ( p_querystr, ", ");


            strcat (p_querystr, get_SQLname (i));
            strcat (p_querystr, "='");
            strcat (p_querystr, get_str (i));
               strcat ( p_querystr, "'");
         break;
         default:
            printf ("Error: EnchancedSQLobject: obj_to_sqlupdate: Unknown type or Index out of bound\n");
         break;
      }
   }

}

void EnhancedSQLobject::obj_to_sqlinsert (void)
{
   strcpy ( p_querystr, "");
   strcpy ( p_fieldstr, "( pk,");
   char buffer [ 16 ];


   for ( int i = 0; i < p_nb_field; i++)
   {
      // --- field name ---
      if ( i != 0 )
         strcat ( p_fieldstr, ", ");

      strcat ( p_fieldstr, get_SQLname ( i ) );

      // --- field data ---
      switch ( get_type (i))
      {
         case INTEGER:
            if ( i != 0)
               strcat ( p_querystr, ", ");

            sprintf ( buffer, "%d", get_int ( i ));
            strcat ( p_querystr, buffer);
         break;
         case STRFIELD:
            bitfield_to_strfield (i);
         case TEXT:
            if ( i != 0)
               strcat ( p_querystr, ", '");
            else
               strcat ( p_querystr, "'");

            strcat ( p_querystr, get_str ( i ) );
            strcat ( p_querystr, "'");
         break;
         default:
            printf ("Error: EnchancedSQLobject: obj_to_sqlinsert: Unknown type or Index out of bound\n");
         break;
      }
   }

   strcat ( p_fieldstr, " )");

   //printf ("p_fieldstr = %s\n", p_fieldstr );
  // printf ("p_querystr = %s\n", p_querystr );


}

/*-------------------------------------------------------------------------------------*/
/*-                            Hash Table Declaration                                 -*/
/*-------------------------------------------------------------------------------------*/


//NOte: some publicate info in "STR_SYS_PROPERTY"
s_EnhancedSQLobject_strfield STRFLD_ELEMENTAL_PROPERTY  [ EnhancedSQLobject_HASH_SIZE ] =
{
   { "Phy", /*PROPERTY_PHYSICAL    */ "Physical" },
   { "Men", /*PROPERTY_MENTAL      */ "Mental" },
   { "Fir", /*PROPERTY_FIRE        */ "Fire" },
   { "Ice", /*PROPERTY_ICE         */ "Ice" },
   { "Lit", /*PROPERTY_LIGHTNING   */ "Lightning" },
   { "Poi", /*PROPERTY_POISON      */ "Poison" },
   { "???", /*PROPERTY_RESERVED1   */ "???" },
   { "???", /*PROPERTY_RESERVED2   */ "???" },
   { "Bod", /*PROPERTY_BODY        */ "Body" },
   { "Blo", /*PROPERTY_BLOOD       */ "Blood" },
   { "Sou", /*PROPERTY_SOUL        */ "Soul" },
   { "Neu", /*PROPERTY_NEURAL      */ "Neural" },
   { "Psy", /*PROPERTY_PSYCHIC     */ "Psychic" },
   { "Ski", /*PROPERTY_SKIN        */ "Skin" },
   { "???", /*PROPERTY_RESERVED3   */ "???" },
   { "???", /*PROPERTY_RESERVED4   */ "???" },
   { "Dra", /*PROPERTY_DRAGON      */ "Dragon" },
   { "Und", /*PROPERTY_UNDEAD      */ "Undead" },
   { "Bea", /*PROPERTY_BEAST       */ "Beast" },
   { "Out", /*PROPERTY_OUTSIDER    */ "Outsider" },
   { "Ani", /*PROPERTY_ANIMAL      */ "Animal" },
   { "Gia", /*PROPERTY_GIANT      */  "Giant" },
   { "Sha", /*PROPERTY_SHAPECHANGER*/ "Shapechanger" },
   { "Ver", /*PROPERTY_VERMIN      */ "Vermin" },
   { "War", /*PROPERTY_WARRIOR     */ "Warrior" },
   { "Rog", /*PROPERTY_ROGUE       */ "Rogue" },
   { "Mag", /*PROPERTY_MAGICUSER   */ "Magic User" },
   { "Hum", /*PROPERTY_HUMANOID    */ "Humanoid" },
   { "Pla", /*PROPERTY_PLANT       */ "Plant" },
   { "Ooz", /*PROPERTY_OOZE        */ "Ooze" },
   { "Mgk", /*PROPERTY_MAGICAL     */ "Magickal" }
};

s_EnhancedSQLobject_strfield STRFLD_DEFENSE [ EnhancedSQLobject_HASH_SIZE ] =
{
   { "Mdf", "Magic Defense" }, // Spell_PROPERTY_MAGIC_DEFENSE
   { "Psv", "Physical Save" }, // Spell_PROPERTY_PHYSICAL_SAVE
   { "Msv", "Mental Save" }, // Spell_PROPERTY_MENTAL_SAVE
   { "Drs", "Damage Resistance" }, // Spell_PROPERTY_DAMAGE_RESISTANCE
   { "Adf", "Active Defense" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },

   { "Hmd", "1/2 Magic Defense" },
   { "Hps", "1/2 Physical Save" },
   { "Hms", "1/2 Mental Save" },
   { "Hdr", "1/2 Damage Resistance" },
   { "Had", "1/2 Active Defense" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" }
};

s_EnhancedSQLobject_strfield STRFLD_PROFIENCY [ EnhancedSQLobject_HASH_SIZE ] =
{
   { "Bal", "Balanced" },
   { "Unb", "Unbalanced" },
   { "Pol", "Pole" },
   { "Mis", "Missile" },
   { "Mag", "Magickal" },
   { "Thr", "Thrown" },
   { "???", "???" },
   { "???", "???" },

   { "Nsh", "Min Shld" },
   { "Lsh", "Lght Shld" },
   { "Hsh", "Hvy Shld" },
   { "???", "???" },
   { "Nar", "Min Arm" },
   { "Lar", "Lght Arm" },
   { "Mar", "Med Arm" },
   { "Har", "Hvy Arm" },

   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },

   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  },
   { "???", "???"  }
};




