/************************************************************************/
/*                                                                      */
/*                        S Q L o b j e c t . c p p                     */
/*                                                                      */
/*   Content: Class SQLobject                                           */
/*   Programmer: Eric Pietrocupo                                        */
/*   Starting Date: April, 22th, 2012                                   */
/*                                                                      */
/*   SQLobject is a wrapper for the sqlite library for data objects.    */
/*   The idea is to have a similar interface to my DDT library to make  */
/*   Sure the transition will be done more easily.                      */
/*                                                                      */
/*   All data objects must inherit from SQLobject in order to integrate */
/*   the SQLlite functionnality. The idea is to make the management of  */
/*   sqlite database easier in the programm.                            */
/*                                                                      */
/************************************************************************/

// Include Groups
#include <grpstd.h>
#include <grpsql.h>



/*-------------------------------------------------------------------------*/
/*-                        Constructor / Destructor                       -*/
/*-------------------------------------------------------------------------*/

   // Constructor/destructor

SQLobject::SQLobject ( void )
{

   p_primary_key = -1;
   p_sqlstatement = NULL;
   //p_sqltail = NULL;
   p_sqlerror = -1;
   p_isprepared = false;
   strcpy ( p_tablename, "");
   strcpy ( p_template_tablename, "");
   p_istemplate = false;

}
SQLobject::~SQLobject ( void )
{
   if (p_isprepared == true)
      SQLfinalize();

   if ( p_sqlstatement != NULL)
      sqlite3_free (p_sqlstatement);

   /*if ( p_sqltail != NULL)
      sqlite3_free ((void*) p_sqltail);*/
}

/*-------------------------------------------------------------------------*/
/*-                        Property methods                               -*/
/*-------------------------------------------------------------------------*/

void SQLobject::primary_key ( int key )
{
   p_primary_key = key;
}

int SQLobject::primary_key ( void )
{
   return p_primary_key;
}

int SQLobject::SQLerror ( void )
{
   return p_primary_key;
}

void SQLobject::SQLerrormsg ( void )
{

   printf("\r\nError: SQLerrormsg: %s\r\n", sqlite3_errmsg(SQLdb));
}


/*-------------------------------------------------------------------------*/
/*-                               Methods                                 -*/
/*-------------------------------------------------------------------------*/

int SQLobject::SQLprepare (const char *conditionstr )
{
   //char querystr[SQL_QUERYSTR_LEN];

   if (p_isprepared == false)
   {



   //printf ("pass 100\r\n");
      sprintf (p_querystr, "SELECT * FROM %s %s;", p_tablename, conditionstr);
   //printf ("pass 101\r\n");
   //printf ("vars: PK=%d, stmt=%d, error=%d, isprep=%d, %s\r\n", p_primary_key, p_sqlstatement, p_sqlerror,
     //      p_isprepared, p_tablename);

   //printf ("query: %s\r\n", querystr);

      p_sqlerror = sqlite3_prepare_v2 (SQLdb, p_querystr, -1, &p_sqlstatement, NULL );
//printf ("pass 102\r\n");
      if ( p_sqlerror != SQLITE_OK )
      {
         if ( SQLerrormsg_active == true )
         {
            printf("Error: SQLprepare: %s\r\n", sqlite3_errmsg(SQLdb));
            printf("Query: %s", p_querystr);
         }
      }
      else
         p_isprepared = true;
   }
   else
      printf("Error: SQLprepare: Statement already prepared\r\n");

   return p_sqlerror;

}

int SQLobject::SQLpreparef ( const char *format, ... )
{
   va_list arguments;
   //int i;
   char tmpstr[SQL_QUERYSTR_LEN] = "";


   va_start (arguments, format);
   vsprintf (tmpstr,format, arguments);
   va_end (arguments);

   SQLprepare (tmpstr);

   //add_item(tmpstr);
   return p_sqlerror;
}

int SQLobject::template_SQLprepare (const char *conditionstr)
{
   //char querystr[SQL_QUERYSTR_LEN];

   if (p_isprepared == false)
   {



   //printf ("pass 100\r\n");
      sprintf (p_querystr, "SELECT * FROM %s %s;", p_template_tablename, conditionstr);
   //printf ("pass 101\r\n");

   //printf ("que            ry: %s\r\n", querystr);

      p_sqlerror = sqlite3_prepare_v2 (SQLdb, p_querystr, -1, &p_sqlstatement, NULL );
//printf ("pass 102\r\n");
//printf ("vars: PK=%d, stmt=%d, error=%d, isprep=%d, %s\r\n", p_primary_key, p_sqlstatement, p_sqlerror,
//           p_isprepared, p_tablename);

      if ( p_sqlerror != SQLITE_OK )
      {
         if (SQLerrormsg_active == true )
         {


            printf("Error: SQLprepare: %s\r\n", sqlite3_errmsg(SQLdb));
            printf("Query: %s", p_querystr);
         }
      }
      else
      {

//printf ("pass 103\r\n");
         p_isprepared = true;
         p_istemplate = true;
      }
   }
   else
      printf("Error: SQLprepare: Statement already prepared\r\n");

   return p_sqlerror;

}

int SQLobject::template_SQLpreparef ( const char *format, ... )
{
   va_list arguments;
   //int i;
   char tmpstr[SQL_QUERYSTR_LEN] = "";


   va_start (arguments, format);
   vsprintf (tmpstr,format, arguments);
   va_end (arguments);

   template_SQLprepare (tmpstr);

   //add_item(tmpstr);
   return p_sqlerror;
}


int SQLobject::SQLstep (void)
{
   p_sqlerror = sqlite3_step (p_sqlstatement);

   if ( p_sqlerror == SQLITE_ROW)
   {

      if ( p_istemplate == true)
         template_sql_to_obj();
      else
         sql_to_obj();
   }

   if ( (p_sqlerror != SQLITE_ROW && p_sqlerror != SQLITE_OK && p_sqlerror != SQLITE_DONE ) && SQLerrormsg_active == true )
   {
      printf("\r\nError: SQLstep:[%d] %s\r\n", p_sqlerror, sqlite3_errmsg(SQLdb));
      printf("Query: %s", p_querystr);
   }

   //if (p_sqlerror != SQLITE_ROW && SQLerrormsg_active == true )
   //      printf("\r\nError: SQLstep: %s\r\n", p_sqlerror, sqlite3_errmsg(SQLdb));

   return p_sqlerror;
}

int SQLobject::SQLfinalize (void)
{
//printf("debug: 7\r\n");
   p_sqlerror = sqlite3_finalize (p_sqlstatement);

//printf("debug: 8\r\n");
   if (p_sqlerror != SQLITE_OK && SQLerrormsg_active == true )
   {
         printf("\r\nError: SQLfinalize: %s\r\n", sqlite3_errmsg(SQLdb));
         printf("Query: %s", p_querystr);
   }
//printf("debug: 9\r\n");
   p_isprepared = false;
   p_istemplate = false;
   p_sqlstatement = NULL;
   return p_sqlerror;
}

int SQLobject::SQLcolumn_int ( int colID )
{

   return sqlite3_column_int (p_sqlstatement, colID);
}

/* ----- demo fonction to write into a blob ---

int InsertFile(const string& db_name)
{
    ifstream file("Sql.pdf", ios::in | ios::binary);
    if (!file) {
        cerr << "An error occurred opening the file\n";
        return 12345;
    }
    file.seekg(0, ifstream::end);
    streampos size = file.tellg();
    file.seekg(0);

    char* buffer = new char[size];
    file.read(buffer, size);

    sqlite3 *db = NULL;
    int rc = sqlite3_open_v2(db_name.c_str(), &db, SQLITE_OPEN_READWRITE, NULL);
    if (rc != SQLITE_OK) {
        cerr << "db open failed: " << sqlite3_errmsg(db) << endl;
    } else {
        sqlite3_stmt *stmt = NULL;
        rc = sqlite3_prepare_v2("INSERT INTO ONE(ID, NAME, LABEL, GRP, FILE)"
                                " VALUES(NULL, 'fedfsdfss', NULL, NULL, ?)",
                                -1, &stmt, NULL);
      if (rc != SQLITE_OK) {
            cerr << "prepare failed: " << sqlite3_errmsg(db) << endl;
        } else {
            // SQLITE_STATIC because the statement is finalized
            // before the buffer is freed:
            rc = sqlite3_bind_blob(stmt, 1, buffer, size, SQLITE_STATIC);
            if (rc != SQLITE_OK) {
                cerr << "bind failed: " << sqlite3_errmsg(db) << endl;
            } else {
                rc = sqlite3_step(stmt);
                if (rc != SQLITE_DONE)
                    cerr << "execution failed: " << sqlite3_errmsg(db) << endl;
            }
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(db);

    delete[] buffer;
}
-------------------------------------------------*/

// const void *sqlite3_column_blob(sqlite3_stmt*, int iCol);
// use memcpy like column_text. Store in bytes
// void * memcpy ( void * destination, const void * source, size_t num );

const char* SQLobject::SQLcolumn_text ( int colID )
{
 //  printf("\r\ndebug: column value %s", sqlite3_column_text (p_sqlstatement, colID));

   const char* tmpstr = (const char*) sqlite3_column_text (p_sqlstatement, colID);
   if (tmpstr == NULL )
      return ("");
   else
      return tmpstr;

}

// Virtual Methods

int SQLobject::SQLupdate (void)
{

   char tmpstr[SQL_QUERYSTR_LEN * 2];

   if ( p_istemplate == false)
   {


      if (p_primary_key > -1 )
      {


         obj_to_sqlupdate();

         sprintf ( tmpstr, "UPDATE %s SET %s WHERE %s=%d;", p_tablename, p_querystr, SQL_PRIMARY_KEY, p_primary_key);

         p_sqlerror = sqlite3_exec ( SQLdb, tmpstr, NULL, NULL, NULL);

         if ( p_sqlerror != SQLITE_OK && SQLerrormsg_active == true)
         {
            printf("\r\nError: SQLupdate: %s\r\n", sqlite3_errmsg(SQLdb));
            printf("\r\n   Query: %s", tmpstr);
            //printf("\r\n   Query should be: UPDATE %s SET %s WHERE %s=%d;",  p_tablename, p_querystr, SQL_PRIMARY_KEY, p_primary_key);
         }
      }
      else
      {


         printf("\r\nError: SQLupdate: cannot update since primary key = -1\r\n");

      }
   }
   else
      printf ("\r\nError: SQLupdate: Cannot insert in template reading mode\r\n");

   return p_sqlerror;
}

int SQLobject::SQLinsert (void)
{
   //if ( p_istemplate == false)
   //{


      char tmpstr[SQL_QUERYSTR_LEN * 2];
      strcpy ( p_fieldstr, ""); // initialise for enhanced sql object, if not used, will add empty string

      obj_to_sqlinsert();

      sprintf ( tmpstr, "INSERT INTO %s %s VALUES ( NULL, %s );", p_tablename, p_fieldstr, p_querystr);

      p_sqlerror = sqlite3_exec ( SQLdb, tmpstr, NULL, NULL, NULL);

      if ( p_sqlerror != SQLITE_OK && SQLerrormsg_active == true)
      {
         printf("\r\nError: SQLinsert: %s\r\n", sqlite3_errmsg(SQLdb));
         printf("\r\n   Query: %s", tmpstr);
      }

   //}
   //else
     // printf ("\r\nError: SQLinsert: Cannot insert in template reading mode\r\n");
   return p_sqlerror;
}

int SQLobject::SQLinsert ( int key )
{
   if ( p_istemplate == false)
   {

      char tmpstr[SQL_QUERYSTR_LEN * 2];
      strcpy ( p_fieldstr, ""); // initialise for enhanced sql object, if not used, will add empty string
//      printf ("insert: pass1\r\n");
      obj_to_sqlinsert();
//      printf ("insert: pass2\r\n");
      sprintf ( tmpstr, "INSERT INTO %s %s VALUES ( %d, %s );", p_tablename, p_fieldstr, key, p_querystr);
//      printf ("query: %s\r\n", tmpstr);
      p_sqlerror = sqlite3_exec ( SQLdb, tmpstr, NULL, NULL, NULL);

      if ( p_sqlerror != SQLITE_OK && SQLerrormsg_active == true)
      {
         printf("\r\nError: SQLinsert(int): %s\r\n", sqlite3_errmsg(SQLdb));
         printf("\r\n   Query: %s", tmpstr);
      }

   }
   else
      printf ("\r\nError: SQLinsert(int): Cannot insert a specific key in template reading mode\r\n");

   return p_sqlerror;
}

int SQLobject::SQLdelete (void)
{
   if ( p_istemplate == false)
   {
      sprintf (p_querystr, "DELETE from %s WHERE %s=%d;", p_tablename, SQL_PRIMARY_KEY, p_primary_key);

      p_sqlerror = sqlite3_exec ( SQLdb, p_querystr, NULL, NULL, NULL);


      if ( p_sqlerror == SQLITE_OK)
         p_primary_key = -1;
      else
         if ( SQLerrormsg_active == true)
         {
            printf("\r\nError: SQLdelete: %s\r\n", sqlite3_errmsg(SQLdb));
            printf("\r\n   Query: %s", p_querystr);
         }

   }
   else
      printf ("\r\nError: SQLdelete: Cannot delete in template reading mode\r\n");


   return p_sqlerror;
}

int SQLobject::SQLselect (int key)
{

   //sprintf (p_querystr, "SELECT * FROM %s WHERE %s=%d;", p_tablename, SQL_PRIMARY_KEY, key);
//printf ("Debug: SQLselect: pass0\n");
   char tmpstr [SQL_QUERYSTR_LEN];
   int tmperror = SQLITE_ERROR;

//printf ( "istemplate enter=%d\n", p_istemplate );

   if ( key != -1 )
   {
//printf ("Debug: SQLselect: pass1\n");
      sprintf (tmpstr, "WHERE %s=%d", SQL_PRIMARY_KEY, key);
   //p_sqlerror = sqlite3_exec ( SQLdb, p_querystr, NULL, NULL, NULL);
// printf ("pass 11\r\n");
      if ( p_istemplate == true)
         tmperror = template_SQLprepare ( tmpstr );
      else
         tmperror = SQLprepare (tmpstr);
//   printf ("pass 13\r\n");
      if ( tmperror == SQLITE_OK)
      {
//printf ("Debug: SQLselect: pass2\n");
         tmperror = SQLstep();
//printf ("pass 14\r\n");
         if ( tmperror == SQLITE_ROW)
         {
//printf ("pass 15\r\n");
//printf ("Debug: SQLselect: pass3\n");
//printf ( "istemplate about to use=%d\n", p_istemplate );
            if ( p_istemplate == true)
            {
               //printf ("SQLselect: Using template function\n");

               template_sql_to_obj();
            }
            else
            {
         //      printf ("SQLselect: Using regular function\n");

               sql_to_obj();
            }

         }
         else
            if ( SQLerrormsg_active == true)
            {
               printf("\r\nError: SQLselect (step failed): %s\r\n",  sqlite3_errmsg(SQLdb));
               printf("\r\n   Query: %s", tmpstr);
            }
      }
      else
         if ( SQLerrormsg_active == true)
         {
            printf("\r\nError: SQLselect (prepare failed): %s\r\n",  sqlite3_errmsg(SQLdb));
            printf("\r\n   Query: %s", tmpstr);
         }

      SQLfinalize();

   }

   return tmperror;
}

int SQLobject::template_SQLselect ( int key)
{
   p_istemplate = true;

  // printf ( "istemplate before=%d\n", p_istemplate );
   return SQLselect ( key );
}

int SQLobject::SQLprocedure ( int index, const char *conditionstr )
{

   int error;

   error = SQLprepare ( conditionstr );

   if ( error == SQLITE_OK )
   {
      error = SQLstep ( );


      while ( error == SQLITE_ROW )
      {
         callback_handler ( index );

         error = SQLstep ();
      }


      SQLfinalize();
   }

   return ( error);

}

void SQLobject::callback_handler ( int index )
{
   // do nothing if not redefined by subclass
}

/*int SQLobject::SQLproceduref ( int (*proc)(void), const char *format, ... )
{

}*/
