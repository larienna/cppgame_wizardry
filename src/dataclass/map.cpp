//-------------------------------------------------------------
//
//                          M A P . C P P
//
//    Class : Map
//    Programmer: Eric Pietrocupo
//    Starting date: Feb 5 2014
//
//    A simple class used to manage map discovery while walking
//
//-------------------------------------------------------------

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>

//------------------------------------------------------------
//                      Constructor
//-----------------------------------------------------------

Map::Map ( void )
{
   clear();
}

Map::~Map ( void )
{

}

//------------------------------------------------------------
//                      Property Methods
//-----------------------------------------------------------


void Map::write (int x, int y, int z)
{
   int shift = x % 8;
   int bytex = x / 8;
   unsigned char mask = 128;
   //unsigned char tmpval;

   mask = mask >> shift;

   p_map [z][y][ bytex] = ( p_map [z][y][ bytex] | mask);

}

bool Map::read (int x, int y, int z)
{
   //bool retval = false;
   int shift = x % 8;
   int bytex = x / 8;
   unsigned char mask = 128;
   unsigned char tmpval;

   mask = mask >> shift;

   tmpval = p_map [z][y][ bytex];

   if ( ( tmpval & mask) > 0 )
      return ( true);
   else
      return (false);
}

//------------------------------------------------------------
//                      Methods
//-----------------------------------------------------------

void Map::clear ( void )
{
   int k;
   int j;
   int i;

   for ( k = 0 ; k < Map_MAXDEPTH; k++ )
      for ( j = 0 ; j < Map_MAXWIDTH; j++)
         for ( i = 0; i < Map_BYTE_WIDTH; i++)
         {
            p_map [ k ][ j ][ i ] = 0;
         }

}

void Map::load ( void )
{
   int i, j, k, z;
   int errorsql;
   //const unsigned char (*tmpmap) [ Map_MAXDEPTH ] [ Map_MAXWIDTH ] [ Map_BYTE_WIDTH ];

   unsigned char *tmpmap;

   //SQLactivate_errormsg();

   errorsql = SQLprepare ( "SELECT * FROM handmap;");

   if ( errorsql == SQLITE_OK)
   {
      errorsql = SQLstep ();

      if ( errorsql == SQLITE_ROW )
      {
         //tmpmap = reinterpret_cast<const unsigned char ( *)[ Map_MAXDEPTH ] [ Map_MAXWIDTH ] [ Map_BYTE_WIDTH ]>( SQLcolumn_blob (1) );

         tmpmap = (unsigned char*) SQLcolumn_blob ( 1 );

         z = 0;
         for ( k = 0 ; k < Map_MAXDEPTH; k++ )
            for ( j = 0 ; j < Map_MAXWIDTH; j++)
               for ( i = 0; i < Map_BYTE_WIDTH; i++)
               {
                  //p_map [ k ][ j ][ i ] = *tmpmap [ k ] [ j ] [ i ];
                  p_map [ k ][ j ][ i ] = tmpmap [ z ];
                  z++;
               }
      }
      else
         printf ("HandMap.load: Cannot step into the DB (Normal if New Game)\n");

      SQLfinalize();
   }
   else
      printf ("HandMap.load: Cannot Prepare statement to save map into the DB\n");

   //SQLdeactivate_errormsg();
}

void Map::save ( void )
{
   int errorsql;

//   SQLactivate_errormsg();

   SQLexec ( "DELETE FROM handmap;");

   errorsql = SQLprepare ( "INSERT INTO handmap (pk, map) VALUES ( 1, ?)");

   if ( errorsql == SQLITE_OK)
   {
      errorsql = SQLbind_blob ( p_map, sizeof ( p_map ), 1);

      if ( errorsql == SQLITE_OK )
      {
         errorsql = SQLstep ();

         if ( errorsql != SQLITE_DONE )
            printf ("Map.save: SQL step failled to insert row\n");
      }
      else
         printf ("Map.save: Cannot bind blob for map insertion\n");

      SQLfinalize();
   }
   else
      printf ("Map.save: Cannot Prepare statement to save map into the DB\n");

  // SQLdeactivate_errormsg();

}

void Map::display ( s_Party_position tmpos, bool partydetail )
{

   int x;
   int y;
   //unsigned char mask;
   int i; // pixel coordinates
   //int ioff; // added offset for bytes
   int j; // pixel coordinates
   //int palette;
   //bool solid = true;
   //int key;
   //int exit = false;
   int tmpcolor;

   clear_bitmap (mazebuffer);
   drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
   rect ( mazebuffer , 15, 15, 416, 416, makecol (255, 255, 255));
   i=16;
   j=412;
   textprintf_old ( mazebuffer, FNT_small, 0, 417, makecol ( 255, 255, 255 ),
      "Press any key to return" );

   if ( partydetail == true )
   {
      textprintf_old ( mazebuffer, FNT_print, 420, 0, General_COLOR_TEXT,  "Level (Z) = %d", tmpos.z);
      textprintf_old ( mazebuffer, FNT_print, 420, 16, General_COLOR_TEXT, "East  (X) = %d", tmpos.x);
      textprintf_old ( mazebuffer, FNT_print, 420, 32, General_COLOR_TEXT, "North (Y) = %d", tmpos.y);
      textprintf_old ( mazebuffer, FNT_print, 420, 48, General_COLOR_TEXT, "Facing    = %s", STR_PAR_FACING [ tmpos.facing ]);
   }

   for ( x = 0 ; x < Map_MAXWIDTH; x++ )
   {
      //mask = 128;
      //ioff = 0;
      //while ( mask != 0)
      //{
         for ( y = 0 ; y < Map_MAXWIDTH ; y++ )
         {
            if ( read ( x, y, tmpos.z ) == false )
            {
                  drawing_mode ( DRAW_MODE_MASKED_PATTERN, datref_editoricon [ 25 ], 0, 0);
                  tmpcolor = 100;
                  //if ( x < 10 && y < 10)
                  //   printf ("%d:%d:%d = false\n", x, y, tmpos.z);
            }
            else
            {
                  drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
                  tmpcolor = 200;

                  //if ( x < 10 && y < 10)
                  //   printf ("%d:%d:%d = true\n", x, y, tmpos.z);
            }


            rectfill ( mazebuffer, i, j, i+3, j+3, makecol (tmpcolor, tmpcolor, tmpcolor)  );

            if ( tmpos.x == x && tmpos.y == y && partydetail == true)
            {

               drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
               rectfill ( mazebuffer, i, j, i+3, j+3, makecol (255, 0, 0) );
               //line ( mazebuffer, i, j, i+3, j+3, makecol (255, 0, 0) );
               //line ( mazebuffer, i, j+3, i+3, j, makecol (255, 0, 0) );
            }

            j = j - 4;
         }

         i = i + 4;
         j = 412;
 //        mask = mask >> 1;
   ////      ioff = ioff + 4;
      //}

   }

   blit_mazebuffer();
   copy_buffer ();

   mainloop_readkeyboard();

   drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );


}

//------------------------------------------------------------
//                   Virtual Methods
//-----------------------------------------------------------


/*
void Map::sql_to_obj (void)
{
   // this function will not be used
}

void Map::template_sql_to_obj (void)
{
   // this function will not be used.
}

void Map::obj_to_sqlupdate (void)
{
   // this function will not be used
}

void Map::obj_to_sqlinsert (void)
{
   // this function will not be used
}*/

//------------------------------------------------------------
//                   Global Variables
//-----------------------------------------------------------

Map handmap;
