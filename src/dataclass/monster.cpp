/***************************************************************************/
/*                                                                         */
/*                            M O N S T E R . C P P                        */
/*                                                                         */
/*                         Class Source code                               */
/*                                                                         */
/*     Content : Class Monster                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : April 26, 2002                                      */
/*                                                                         */
/*          This is some sort of template class for monsters. It does not  */
/*     keep track of current HP and status. This will not be stored in the */
/*     class since that information is not kept in the database            */
/*                                                                         */
/***************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

bool Monster::p_recompile_ranks = false;

/*-------------------------------------------------------------------------*/
/*-                       Constructor and Destructor                      -*/
/*-------------------------------------------------------------------------*/

Monster::Monster ( void )
{
   strncpy ( p_name, "", Monster_NAME_STRLEN );
	p_level = 0;
	//p_hpdice = 0;
	//p_mpdice = 0;
	p_attdiv = 0;
	p_powdiv = 0;
	p_dmgy = 4;
	p_size = 0;
	p_power = 0;
	//p_wsp = 0;
	//p_msp_min = 0;
	//p_msp_max = 0;
	//p_magic_type = 0;
	p_resistance = 0;
	strcpy ( p_resistance_str, "");
	p_weakness = 0;
	strcpy ( p_weakness_str, "");
	p_AD = 0;
	p_MD = 0;
	p_PS = 0;
	p_MS = 0;
	p_CS = 0;
	p_DR = 0;
	p_init = 0;
	p_range = 0;
	p_mh = 0;
	//p_target = 0;
	p_defense = 0;
	strcpy ( p_defense_str, "" );
	p_element = 0;
	strcpy ( p_element_str, "" );

   for ( int i = 0 ; i < 4 ; i++)
   {
      p_front_attack [ i ] = 0;
      p_back_attack [ i ] = 0;
   }

	p_extra_reward = 0;
   strcpy ( p_extra_reward_str, "");
	p_pictureID = 0;
	p_position = 0;
	p_current_HP = 0;
	p_current_MP = 0;
	p_max_HP = 0;
	p_max_MP = 0;
	//p_health = 0;
	p_body = Character_BODY_ALIVE;
	p_location = 0;
//	p_position = 0;
	p_rank = 0;
	p_FKcategory = 1;
	p_identified = 0;
	strncpy ( p_uname, "", Monster_NAME_STRLEN );
	//p_nameletter = '?';


	strcpy ( p_tablename, "monster" );
	strcpy ( p_template_tablename, "monster_template");

   clear_stat();
}

Monster::~Monster ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                       Property Method                                 -*/
/*-------------------------------------------------------------------------*/


void Monster::body ( int value )
{
   p_body = value;
   p_recompile_ranks = true;

   if ( p_body != Character_BODY_ALIVE )
      ActiveEffect::trigger_expiration_by_death ( this );

   if ( p_body > Character_BODY_PETRIFIED )
      p_current_HP = 0;
}

char *Monster::name ( void )
{
   return ( p_name );
}

void Monster::name ( const char* str )
{
   strncpy ( p_name, str, Monster_NAME_STRLEN );
}

int Monster::level ( void )
{
   return ( p_level );
}

int Monster::size ( void )
{
   return ( p_size );
}

int Monster::power ( void )
{
   return ( p_power );
}
/*
int Monster::wsp ( void )
{
   return ( p_wsp );
}

int Monster::msp_min ( void )
{
   return ( p_msp_min );
}


int Monster::msp_max ( void )
{
   return ( p_msp_max );
}

unsigned int Monster::magic_type ( void )
{
   return ( p_magic_type );
}*/
int Monster::resistance ( void )
{
   return ( p_resistance );
}

int Monster::weakness ( void )
{
   return ( p_weakness );
}

int Monster::AD ( void )
{
   return ( p_AD );
}

int Monster::MD ( void )
{
   return ( p_MD );
}

int Monster::PS ( void )
{
   return ( p_PS );
}

int Monster::MS ( void )
{
   return ( p_MS );
}

int Monster::CS ( void )
{
   return ( p_CS );
}

int Monster::DR ( void )
{
   return ( p_DR );
}

int Monster::init ( void )
{
   return ( p_init );
}

int Monster::range ( void )
{
   return ( p_range );
}

/*int Monster::clumsiness ( void )
{
   return ( p_clumsiness );
}

int Monster::target ( void )
{
   return ( p_target );
}

unsigned int Monster::special ( void )
{
   return ( p_special );
}

unsigned int Monster::property ( void )
{
   return ( p_property );
}

unsigned int Monster::action ( void )
{
   return ( p_action );
}

int Monster::special_attack_1 ( void )
{
   return ( p_special_attack_1 );
}

int Monster::special_attack_2 ( void )
{
   return ( p_special_attack_2 );
}

int Monster::special_attack_3 ( void )
{
   return ( p_special_attack_3 );
}

int Monster::special_attack_4 ( void )
{
   return ( p_special_attack_4 );
}*/

int Monster::extra_reward ( void )
{
   return ( p_extra_reward );
}

int Monster::pictureID ( void )
{
   return ( p_pictureID );
}

/*int Monster::position ( void )
{
   return ( p_position );
}*/

/*-------------------------------------------------------------------------*/
/*-                                Method                                 -*/
/*-------------------------------------------------------------------------*/

void Monster::recover_HP ( int value )
{
   p_current_HP = p_current_HP + value;
   if ( p_current_HP > p_max_HP )
      p_current_HP = p_max_HP;
   if ( p_current_HP > 0 )
      p_body = Character_BODY_ALIVE;
}

void Monster::recover_MP ( int value )
{
   p_current_MP = p_current_MP + value;
   if ( p_current_MP > p_max_MP )
      p_current_MP = p_max_MP;
}


void Monster::lose_HP ( int value )
{
   p_current_HP = p_current_HP - value;
   if ( p_current_HP <= 0 )
   {
      p_current_HP = 0;
      p_body = Character_BODY_DEAD;
      p_recompile_ranks = true;
      ActiveEffect::trigger_expiration_by_death ( this );
   }
}

void Monster::lose_MP ( int value )
{
   p_current_MP = p_current_MP - value;
   if ( p_current_MP < 0 )
      p_current_MP = 0;

}

void Monster::roll_maxHPMP ( void )
{
   switch ( config.get (Config_MONSTER_HPMP ))
   {
      case Config_DIF_LOW:
         p_max_HP = p_level * ( DIE_SIZE [ p_size ] / 2 );
         p_max_MP = p_level * ( DIE_SIZE [ p_power ] / 2 );
      break;
      case Config_DIF_NORMAL:
         p_max_HP = roll_xdypz ( p_level, DIE_SIZE [ p_size ], 0);
         p_max_MP = roll_xdypz ( p_level, DIE_SIZE [ p_power ], 0);

      break;
      case Config_DIF_HIGH:
         p_max_HP = p_level * DIE_SIZE [ p_size ];
         p_max_MP = p_level * DIE_SIZE [ p_power ];
      break;
   }

   p_current_HP = p_max_HP;
   p_current_MP = p_max_MP;
}

void Monster::compile_ranks ( void )
{
   //Character tmpcharacter;
   Monster tmpmonster;
   //int character_id = 0;
   int monster_id = 0;
   //int nb_character = 0;
   int error;
   int nb_monster = 0;

   //printf ("pass before\n");
   if ( p_recompile_ranks == true )
   {
      //printf ("pass after\n");
      SQLexec ("UPDATE monster SET rank=0 WHERE body>0");

      // --- compile monster ranks ---

      nb_monster = SQLcount ("name", "monster", "WHERE body=0") - 1;

      error = tmpmonster.SQLprepare ("WHERE body=0 ORDER BY position");



      if ( error == SQLITE_OK )
      {

         error = tmpmonster.SQLstep();

         SQLbegin_transaction();



         while ( error == SQLITE_ROW )
         {

         /*if ( tmpmonster.body() == Character_BODY_ALIVE )
         {*/

            switch ( Monster_POSITION [nb_monster] [ monster_id])
            {
               case Party_FRONT: tmpmonster.rank(Party_FRONT); break;
               case Party_BACK: tmpmonster.rank(Party_BACK); break;
               case Party_NONE: tmpmonster.rank(Party_NONE); break;

            }

            monster_id++;
         /*}
         else
            tmpmonster.rank (Party_BACK);*/

            //printf ("Monster: Compile ranks: Before update\n");
            //printf ("pk=%d, name=%s, position=%d, rank=%d", tmpmonster.primary_key(), tmpmonster.name(), tmpmonster.position(), tmpmonster.rank() );
            tmpmonster.SQLupdate();
            //printf ("Monster: Compile ranks: After update\n");
            error = tmpmonster.SQLstep();
         }

         SQLcommit_transaction();
      }

      tmpmonster.SQLfinalize();

   }

   p_recompile_ranks = false;
}

void Monster::force_recompile_ranks ( void )
{
   p_recompile_ranks = true;
}

char* Monster::displayname ( void )
{
   if ( p_identified > 0 )
      return ( p_name );
   else
      return (p_uname );
}

void Monster::convert_strfield_to_bitfield ()
{
/*   strncpy ( p_resistance_str, SQLcolumn_text (8), EnhancedSQLobject_STRFIELD_LEN;
	strncpy ( p_weakness_str, SQLcolumn_text (9), EnhancedSQLobject_STRFIELD_LEN;
	strncpy ( p_defense_str, SQLcolumn_text (19), EnhancedSQLobject_STRFIELD_LEN);
	strncpy ( p_property_str, SQLcolumn_text (20), EnhancedSQLobject_STRFIELD_LEN);
	strncpy ( p_extra_reward_str, SQLcolumn_text (29), EnhancedSQLobject_STRFIELD_LEN);*/

	p_resistance = EnhancedSQLobject::strfield_to_bitfield ( STRFLD_ELEMENTAL_PROPERTY, p_resistance_str );
	p_weakness = EnhancedSQLobject::strfield_to_bitfield ( STRFLD_ELEMENTAL_PROPERTY, p_weakness_str );

	p_defense = EnhancedSQLobject::strfield_to_bitfield ( STRFLD_DEFENSE, p_defense_str );
	p_element = EnhancedSQLobject::strfield_to_bitfield ( STRFLD_ELEMENTAL_PROPERTY, p_element_str );

	p_extra_reward = EnhancedSQLobject::strfield_to_bitfield ( STRFLD_EXTRA_REWARD, p_extra_reward_str );
}

void Monster::convert_bitfield_to_strfield ()
{
   //char **tmptr = &p_resistance_str;

   //**tmptr = &p_resistance_str;
   EnhancedSQLobject::bitfield_to_strfield ( STRFLD_ELEMENTAL_PROPERTY, p_resistance_str, p_resistance );
   //**tmptr = &p_weakness_str;
   EnhancedSQLobject::bitfield_to_strfield ( STRFLD_ELEMENTAL_PROPERTY, p_weakness_str, p_weakness );

   //**tmptr = &p_defense_str;
   EnhancedSQLobject::bitfield_to_strfield ( STRFLD_DEFENSE, p_defense_str, p_defense );
   //**tmptr = &p_element_str;
   EnhancedSQLobject::bitfield_to_strfield ( STRFLD_ELEMENTAL_PROPERTY, p_element_str, p_element );
   //**tmptr = &p_extra_reward_str;
   EnhancedSQLobject::bitfield_to_strfield ( STRFLD_EXTRA_REWARD, p_extra_reward_str, p_extra_reward );

}

/*--------------------------------------------------------------------------*/
/*-                        Callback methods                                -*/
/*--------------------------------------------------------------------------*/

//void Monster::cb_apply_spell ( void )
//{
   //des not work since cannot use spell procedure in routine
//}

/*-------------------------------------------------------------------------*/
/*-                        Virtual Method                                 -*/
/*-------------------------------------------------------------------------*/

void Monster::sql_to_obj (void)
{
   //int tmpidentified;
   //char tmpstr [3];

   /*p_primary_key = SQLcolumn_int (0);
   strncpy ( p_name, SQLcolumn_text (1), Monster_NAME_STRLEN );
   p_level = SQLcolumn_int (2);
	p_size = SQLcolumn_int (3);
	p_power = SQLcolumn_int (4);
	p_wsp = SQLcolumn_int (5);
	p_msp_min = SQLcolumn_int (6);
	p_msp_max = SQLcolumn_int (7);
	p_magic_type = SQLcolumn_int (8);
	p_resistance = SQLcolumn_int (9);
	p_weakness = SQLcolumn_int (10);
	p_AD = SQLcolumn_int (11);
	p_MD = SQLcolumn_int (12);
	p_PS = SQLcolumn_int (13);
	p_MS = SQLcolumn_int (14);
	p_CS = SQLcolumn_int (15);
	p_DR = SQLcolumn_int (16);
	p_init = SQLcolumn_int (17);
	p_range = SQLcolumn_int (18);
	p_clumsiness = SQLcolumn_int (19);
	p_target = SQLcolumn_int (20);
	p_special = SQLcolumn_int (21);
	p_property = SQLcolumn_int (22);
	p_action = SQLcolumn_int (23);
	p_special_attack_1 = SQLcolumn_int (24);
	p_special_attack_2 = SQLcolumn_int (25);
	p_special_attack_3 = SQLcolumn_int (26);
	p_special_attack_4 = SQLcolumn_int (27);
	p_extra_reward = SQLcolumn_int (28);
	p_pictureID = SQLcolumn_int (29);
	p_position = SQLcolumn_int (30);
	p_current_HP = SQLcolumn_int (31);
	p_max_HP = SQLcolumn_int (32);
	p_current_MP = SQLcolumn_int (33);
	p_max_MP = SQLcolumn_int (34);
	//p_health = SQLcolumn_int (35);
	p_body = SQLcolumn_int (35);
	p_location = SQLcolumn_int (35);
	p_rank = SQLcolumn_int (37);
	p_FKcategory = SQLcolumn_int (38);
	p_identified = SQLcolumn_int (39);
	strncpy ( p_uname, SQLcolumn_text (40), Monster_NAME_STRLEN );
   //strncpy ( tmpstr, SQLcolumn_text (41), 2 );*/

   //p_nameletter = tmpstr [ 0 ];


	/*if ( tmpidentified == 1 )
	   p_identified = true;
	else
	   p_identified = false;*/
	//p_position = SQLcolumn_int (38);

	//printf ("Monster: passed through SQL to obj\n");

   p_primary_key = -1;
   p_primary_key = SQLcolumn_int (0);
   strncpy ( p_name, SQLcolumn_text (1), Monster_NAME_STRLEN );
   p_level = SQLcolumn_int (2);
	p_size = SQLcolumn_int (3);
	p_power  = SQLcolumn_int (4);
	p_dmgy  = SQLcolumn_int (5);
	p_attdiv  = SQLcolumn_int (6);
	p_powdiv  = SQLcolumn_int (7);
	strncpy ( p_resistance_str, SQLcolumn_text (8), EnhancedSQLobject_STRFIELD_LEN );
	strncpy ( p_weakness_str, SQLcolumn_text (9), EnhancedSQLobject_STRFIELD_LEN );
	p_AD = SQLcolumn_int (10);
	p_MD = SQLcolumn_int (11);
	p_PS = SQLcolumn_int (12);
	p_MS = SQLcolumn_int (13);
	p_CS = SQLcolumn_int (14);
	p_DR = SQLcolumn_int (15);
	p_init = SQLcolumn_int (16);
	p_range = SQLcolumn_int (17);
	p_mh = SQLcolumn_int (18);
	strncpy ( p_defense_str, SQLcolumn_text (19), EnhancedSQLobject_STRFIELD_LEN);
	strncpy ( p_element_str, SQLcolumn_text (20), EnhancedSQLobject_STRFIELD_LEN);
	for ( int i = 0 ; i < 4 ; i++)
	{
	   p_front_attack [ i ] = SQLcolumn_int (21 + i);
      p_back_attack [ i ] = SQLcolumn_int (25 + i);
	}
	strncpy ( p_extra_reward_str, SQLcolumn_text (29), EnhancedSQLobject_STRFIELD_LEN);
	p_pictureID = SQLcolumn_int (30);
	p_position = SQLcolumn_int (31);
   p_FKcategory = SQLcolumn_int (32);
   p_current_HP = SQLcolumn_int (33);
   p_max_HP = SQLcolumn_int (34);
   p_current_MP = SQLcolumn_int (35);
   p_max_MP = SQLcolumn_int (36);
   p_body = SQLcolumn_int (37);
   p_location = SQLcolumn_int (38);
   p_rank = SQLcolumn_int (39);
   p_identified = SQLcolumn_int (40);
   strncpy ( p_uname, SQLcolumn_text (41), Monster_NAME_STRLEN );


   convert_strfield_to_bitfield();

}

void Monster::template_sql_to_obj (void)
{
   p_primary_key = -1;
   strncpy ( p_name, SQLcolumn_text (1), Monster_NAME_STRLEN );
   p_level = SQLcolumn_int (2);
	p_size = SQLcolumn_int (3);
	p_power  = SQLcolumn_int (4);
	p_dmgy  = SQLcolumn_int (5);
	p_attdiv  = SQLcolumn_int (6);
	p_powdiv  = SQLcolumn_int (7);
	strncpy ( p_resistance_str, SQLcolumn_text (8), EnhancedSQLobject_STRFIELD_LEN );
	strncpy ( p_weakness_str, SQLcolumn_text (9), EnhancedSQLobject_STRFIELD_LEN );
	p_AD = SQLcolumn_int (10);
	p_MD = SQLcolumn_int (11);
	p_PS = SQLcolumn_int (12);
	p_MS = SQLcolumn_int (13);
	p_CS = SQLcolumn_int (14);
	p_DR = SQLcolumn_int (15);
	p_init = SQLcolumn_int (16);
	p_range = SQLcolumn_int (17);
	p_mh = SQLcolumn_int (18);
	strncpy ( p_defense_str, SQLcolumn_text (19), EnhancedSQLobject_STRFIELD_LEN);
	strncpy ( p_element_str, SQLcolumn_text (20), EnhancedSQLobject_STRFIELD_LEN);
	for ( int i = 0 ; i < 4 ; i++)
	{
	   p_front_attack [ i ] = SQLcolumn_int (21 + i);
      p_back_attack [ i ] = SQLcolumn_int (25 + i);
	}
	strncpy ( p_extra_reward_str, SQLcolumn_text (29), EnhancedSQLobject_STRFIELD_LEN);
	p_pictureID = SQLcolumn_int (30);
	p_position = SQLcolumn_int (31);
   p_FKcategory = SQLcolumn_int (32);

   // not sure how boss bonus will be applied. Maybe modify stats afterward since many stats to change.
   MonsterCategory tmpcategory;

   tmpcategory.SQLselect ( p_FKcategory );

   strncpy ( p_uname, tmpcategory.name(), Monster_NAME_STRLEN );
  // p_nameletter = '?';


   roll_maxHPMP();
   //p_health = 0;
   p_body = Character_BODY_ALIVE;
   p_location = 0;
   p_rank=0;
   p_identified = 0;
   //p_position = 0;

	//printf ("Monster: passed through template SQL to obj\n");

   convert_strfield_to_bitfield();
}

void Monster::obj_to_sqlupdate (void)
{
//   int tmpidentified = 0;

   convert_bitfield_to_strfield();


   //if ( p_identified == true )
   //   tmpidentified = 1;

   sprintf (p_querystr, "name='%s', level=%d, size=%d, power=%d, dmgy=%d, attdiv=%d, powdiv=%d, resistance='%s', weakness='%s', AD=%d, MD=%d, PS=%d, MS=%d, CS=%d, DR=%d, init=%d, range=%d, mh=%d, defense='%s', element='%s', frontatt1=%d, frontatt2=%d, frontatt3=%d, frontatt4=%d, backatt1=%d, backatt2=%d, backatt3=%d, backatt4=%d, extra_reward='%s', pictureID=%d, position=%d, current_HP=%d, max_HP=%d, current_MP=%d, max_MP=%d, body=%d, location=%d, rank=%d, FKcategory=%d, identified=%d, uname='%s'",
   p_name,
   p_level,
	p_size,
	p_power,
	p_dmgy,
	p_attdiv,
	p_powdiv,
	p_resistance_str,
	p_weakness_str,
	p_AD,
	p_MD,
	p_PS,
	p_MS,
	p_CS,
	p_DR,
	p_init,
	p_range,
	p_mh,
	p_defense_str,
	p_element_str,
	p_front_attack [ 0 ],
	p_front_attack [ 1 ],
	p_front_attack [ 2 ],
	p_front_attack [ 3 ],
	p_back_attack [ 0 ],
	p_back_attack [ 1 ],
	p_back_attack [ 2 ],
	p_back_attack [ 3 ],
	p_extra_reward_str,
	p_pictureID,
	p_position,
	p_current_HP,
	p_max_HP,
	p_current_MP,
	p_max_MP,
	//p_health,
	p_body,
	p_location,
	p_rank,
	p_FKcategory,
	p_identified,
	p_uname );

/*
	Edit	Delete	0	pk	INTEGER	Yes	None	Yes
	Edit	Delete	1	name	TEXT	No	None	No
	Edit	Delete	2	level	INTEGER	No	None	No
	Edit	Delete	3	hpdice	INTEGER	No	None	No
	Edit	Delete	4	mpdice	INTEGER	No	None	No
	Edit	Delete	5	dmgy	INTEGER	No	None	No
	Edit	Delete	6	attdiv	INTEGER	No	None	No
	Edit	Delete	7	powdiv	INTEGER	No	None	No
	Edit	Delete	8	resistance	TEXT	No	None	No
	Edit	Delete	9	weakness	TEXT	No	None	No
	Edit	Delete	10	ad	INTEGER	No	None	No
	Edit	Delete	11	md	INTEGER	No	None	No
	Edit	Delete	12	ps	INTEGER	No	None	No
	Edit	Delete	13	ms	INTEGER	No	None	No
	Edit	Delete	14	cs	INTEGER	No	None	No
	Edit	Delete	15	dr	INTEGER	No	None	No
	Edit	Delete	16	init	INTEGER	No	None	No
	Edit	Delete	17	range	INTEGER	No	None	No
	Edit	Delete	18	mh	INTEGER	No	None	No
	Edit	Delete	19	defense	TEXT	No	None	No
	Edit	Delete	20	element	TEXT	No	None	No
	Edit	Delete	21	frontatt1	INTEGER	No	None	No
	Edit	Delete	22	frontatt2	INTEGER	No	None	No
	Edit	Delete	23	frontatt3	INTEGER	No	None	No
	Edit	Delete	24	frontatt4	INTEGER	No	None	No
	Edit	Delete	25	backatt1	INTEGER	No	None	No
	Edit	Delete	26	backatt2	INTEGER	No	None	No
	Edit	Delete	27	backatt3	INTEGER	No	None	No
	Edit	Delete	28	backatt4	INTEGER	No	None	No
	Edit	Delete	29	extra_reward	TEXT	No	None	No
	Edit	Delete	30	pictureid	INTEGER	No	None	No
	Edit	Delete	31	position	INTEGER	No	None	No
	Edit	Delete	32	FKcategory	INTEGER	No	None	No
	Edit	Delete	33	current_hp	INTEGER	No	None	No
	Edit	Delete	34	max_hp	INTEGER	No	None	No
	Edit	Delete	35	current_mp	INTEGER	No	None	No
	Edit	Delete	36	max_mp	INTEGER	No	None	No
	Edit	Delete	37	body	INTEGER	No	None	No
	Edit	Delete	38	location	INTEGER	No	None	No
	Edit	Delete	39	rank	INTEGER	No	None	No
	Edit	Delete	40	identified	INTEGER	No	None	No
	Edit	Delete	41	uname	TEXT	No	None	No
*/
}

void Monster::obj_to_sqlinsert (void)
{
   //int tmpidentified = 0;

  // if ( p_identified == true )
   //   tmpidentified = 1;

   convert_bitfield_to_strfield();

   sprintf ( p_querystr, "'%s', %d, %d, %d, %d, %d, %d, '%s', '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s', '%s', %d, %d, %d, %d, %d, %d, %d, %d, '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, '%s'",
   p_name,
   p_level,
	p_size,
	p_power,
	p_dmgy,
	p_attdiv,
	p_powdiv,
	p_resistance_str,
	p_weakness_str,
	p_AD,
	p_MD,
	p_PS,
	p_MS,
	p_CS,
	p_DR,
	p_init,
	p_range,
	p_mh,
	p_defense_str,
	p_element_str,
	p_front_attack [ 0 ],
	p_front_attack [ 1 ],
	p_front_attack [ 2 ],
	p_front_attack [ 3 ],
	p_back_attack [ 0 ],
	p_back_attack [ 1 ],
	p_back_attack [ 2 ],
	p_back_attack [ 3 ],
	p_extra_reward_str,
	p_pictureID,
	p_position,
	p_FKcategory,
	p_current_HP,
	p_max_HP,
	p_current_MP,
	p_max_MP,
	//p_health,
	p_body,
	p_location,
	p_rank,
	p_identified,
	p_uname );

   /*p_name,
   p_level,
	p_size,
	p_power,
	p_wsp,
	p_msp_min,
	p_msp_max,
	p_magic_type,
	p_resistance,
	p_weakness,
	p_AD,
	p_MD,
	p_PS,
	p_MS,
	p_CS,
	p_DR,
	p_init,
	p_range,
	p_clumsiness,
	p_target,
	p_special,
	p_property,
	p_action,
	p_special_attack_1,
	p_special_attack_2,
	p_special_attack_3,
	p_special_attack_4,
	p_extra_reward,
	p_pictureID,
	p_position,
	p_current_HP,
	p_max_HP,
	p_current_MP,
	p_max_MP,
	//p_health,
	p_body,
	p_location,
	p_rank,
	p_FKcategory,
	p_identified,
	p_uname );*/
}

void Monster::compile_stat ( void )
{


 //  Item tmpitem;
 //  CClass tmpclass;
   int error;
   //int i;
   //int tmp_attribute = 0;

   clear_stat();

   //to do: eventually add the possibility to have bonus.

   // copy monster stats directly

   p_stat.d20 [ D20STAT_AD ] [ D20MOD_BASE ] = p_AD;
   p_stat.d20 [ D20STAT_MD ] [ D20MOD_BASE ] = p_MD;
   p_stat.d20 [ D20STAT_PS ] [ D20MOD_BASE ] = p_PS;
   p_stat.d20 [ D20STAT_MS ] [ D20MOD_BASE ] = p_MS;
   p_stat.d20 [ D20STAT_DR ] [ D20MOD_BASE ] = p_DR;
   p_stat.d20 [ D20STAT_INIT ] [ D20MOD_BASE ] = p_init;

   p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_Y ] = p_dmgy;
   //p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_Y ] = DIE_SIZE [ p_power ]; // now depends on spell
   p_stat.xyz [ XYZSTAT_HPDICE ] [ XYZMOD_Y ] = DIE_SIZE [ size() ];
   p_stat.xyz [ XYZSTAT_MPDICE ] [ XYZMOD_Y ] = DIE_SIZE [ power() ];






   sum_stat();

   p_stat.flat [ FLAT_CS ] = p_CS;
   p_stat.flat [ FLAT_ATTDIV ] = p_attdiv;
   p_stat.flat [ FLAT_POWDIV ] = p_powdiv;
   //p_stat.flat [ FLAT_CLUMSINESS ] = p_clumsiness;
   p_stat.flat [ FLAT_RANGE ] = p_range;
   p_stat.flat [ FLAT_MULTIHIT ] = p_mh;
   p_stat.flat [ FLAT_DEFENSE ] = p_defense;


   // Implement active effects

   ActiveEffect tmpeffect;

   error = tmpeffect.SQLpreparef ( "WHERE target_type=%d AND target_id=%d", Opponent_TYPE_ENNEMY, p_primary_key );

   if ( error == SQLITE_OK)
   {
      error = tmpeffect.SQLstep ();

      while ( error == SQLITE_ROW )
      {
         p_stat.d20 [ D20STAT_AD ] [D20MOD_AEFFECT] += tmpeffect.AD ();
         p_stat.d20 [ D20STAT_MD ] [D20MOD_AEFFECT] += tmpeffect.MD ();
         p_stat.d20 [ D20STAT_PS ] [D20MOD_AEFFECT] += tmpeffect.PS ();
         p_stat.d20 [ D20STAT_MS ] [D20MOD_AEFFECT] += tmpeffect.MS ();
         p_stat.d20 [ D20STAT_MELEECS ] [D20MOD_AEFFECT] += tmpeffect.melee_CS ();
         p_stat.d20 [ D20STAT_RANGECS ] [D20MOD_AEFFECT] += tmpeffect.range_CS ();
         p_stat.d20 [ D20STAT_DR ] [D20MOD_AEFFECT] += tmpeffect.DR ();
         p_stat.d20 [ D20STAT_INIT ] [D20MOD_AEFFECT] += tmpeffect.INIT();
         p_stat.xyz [ XYZSTAT_DMG ] [ XYZMOD_ZAEFFECT ] += tmpeffect.DMG();
         p_stat.xyz [ XYZSTAT_MDMG ] [ XYZMOD_ZAEFFECT ] += tmpeffect.MDMG();
         p_stat.flat [ FLAT_ATTDIV ] += tmpeffect.attdiv();
         p_stat.flat [ FLAT_POWDIV ] += tmpeffect.powdiv();



         error = tmpeffect.SQLstep ();
      }

      tmpeffect.SQLfinalize ();
   }

   p_stat.flat [ FLAT_CONDITION ] = compile_condition();

   // min 3
   if ( p_stat.flat [ FLAT_ATTDIV ] < 3 )
      p_stat.flat [ FLAT_ATTDIV ] = 3;

   if ( p_stat.flat [ FLAT_POWDIV ] < 3 && p_stat.flat [ FLAT_POWDIV ] != 0 )
      p_stat.flat [ FLAT_POWDIV ] = 3;


}

void Monster::callback_handler ( int index )
{
   /*switch ( index )
   {
      case Monster_CB_APPLY_SPELL:
         cb_apply_spell();
      break;
   }*/
}

/*-------------------------------------------------------------------------*/
/*                              Global Variables                           */
/*-------------------------------------------------------------------------*/

int Monster_POSITION [ 8 ] [ 8 ] =
{
   // monsters always have a semi-offensive (Fibonacci)formation 5:3, 3:2, 2:1
   { Party_FRONT, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 1 Monster
   { Party_FRONT, Party_FRONT, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 2 Monster
   { Party_FRONT, Party_FRONT, Party_BACK, Party_NONE, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 3 Monster
   { Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_NONE, Party_NONE, Party_NONE, Party_NONE }, // 4 Monster
   { Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_NONE, Party_NONE, Party_NONE }, // 5 Monster
   { Party_FRONT, Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_NONE, Party_NONE }, // 6 Monster
   { Party_FRONT, Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_BACK, Party_NONE }, // 7 Monster
   { Party_FRONT, Party_FRONT, Party_FRONT, Party_FRONT, Party_FRONT, Party_BACK, Party_BACK, Party_BACK }, // 8 Monster
};


s_EnhancedSQLobject_strfield STRFLD_EXTRA_REWARD  [ EnhancedSQLobject_HASH_SIZE ] =
{

   {"Gd1", "Gold d6" },
   {"Gd2", "Gold d12" },
   {"Ep2", "Exp x2" },
   {"Ep4", "Exp x4" },
   {"Itm", "Drop Item" },
   {"Exp", "Drop Expandable" },
   {"Art", "Drop Artifact" },
   {"Key", "Drop Key Items" },

   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },

   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },

   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" },
   {"???", "" }

};
