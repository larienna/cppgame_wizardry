/***************************************************************************/
/*                                                                         */
/*                          O P P O N E N T . C P P                        */
/*                             Class Source Code                           */
/*                                                                         */
/*     Content : Class Opponent                                            */
/*     Programmer : Eric PIetrocupo                                        */
/*     Starting Date : June 9th, 2002                                      */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>

//#include <combat.h>
//#include <opponent.h>

/*
#include <allegro.h>



//#include <time.h>


//#include <datafile.h>
//#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>


//
//
//
//
//
//#include <list.h>
#include <opponent.h>

#include <cclass.h>
#include <charactr.h>
#include <ennemy.h>
//#include <monster.h>
#include <party.h>


//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
#include <combat.h>
#include <window.h> // temporary
#include <windata.h>
//#include <wdatproc.h>

*/


/*-------------------------------------------------------------------------*/
/*-                          Static variable                              -*/
/*-------------------------------------------------------------------------*/


//unsigned char Opponent::p_maze_special; // contains maze special unsigned char

/*-------------------------------------------------------------------------*/
/*-                     Constructor and Destructors                       -*/
/*-------------------------------------------------------------------------*/

Opponent::Opponent ( void )
{
   clear_stat();
   //note: Character and monster does not call this constructor
}

Opponent::~Opponent ( void )
{

}



/*void Opponent::compile_ranks ( void )
{
   Character tmpcharacter;
   Monster tmpmonster;
   int character_id = 0;
   int monster_id = 0;
   int nb_character = 0;
   int error;
   int nb_monster = 0;


   //----- Compile Character Ranks -----



   nb_character = SQLcount ("name", "character", "WHERE location=2 AND body=0") - 1;

   error = tmpcharacter.SQLprepare ("WHERE location=2 AND body=0 ORDER BY position");



   if ( error == SQLITE_OK )
   {

      SQLbegin_transaction();

      error = tmpcharacter.SQLstep();

      while ( error == SQLITE_ROW )
      {

         if ( tmpcharacter.body() == Character_BODY_ALIVE )
         {

            switch ( Party_FORMATION_POSITION [ party.formation() ] [nb_character] [ character_id])
            {
               case Party_FRONT: tmpcharacter.rank(Party_FRONT); break;
               case Party_BACK: tmpcharacter.rank(Party_BACK); break;
               case Party_NONE: tmpcharacter.rank(Party_NONE); break;
            }

            character_id++;
         }
         else
            tmpcharacter.rank (Party_BACK);

         tmpcharacter.SQLupdate();
         error = tmpcharacter.SQLstep();

      }

   SQLcommit_transaction();

   }


   tmpcharacter.SQLfinalize();



   // --- compile monster ranks ---

   nb_monster = SQLcount ("name", "monster", "WHERE body=0") - 1;

   error = tmpmonster.SQLprepare ("WHERE body=0 ORDER BY position");



   if ( error == SQLITE_OK )
   {

      SQLbegin_transaction();

      error = tmpmonster.SQLstep();

      while ( error == SQLITE_ROW )
      {

         //if ( tmpmonster.body() == Character_BODY_ALIVE )
         //{

            switch ( Monster_POSITION [nb_monster] [ monster_id])
            {
               case Party_FRONT: tmpmonster.rank(Party_FRONT); break;
               case Party_BACK: tmpmonster.rank(Party_BACK); break;
               case Party_NONE: tmpmonster.rank(Party_NONE); break;

            }

            monster_id++;
         //}
         else
         //   tmpmonster.rank (Party_BACK);

         tmpmonster.SQLupdate();
         error = tmpmonster.SQLstep();
      }

      SQLcommit_transaction();
   }

   tmpmonster.SQLfinalize();


}*/

/*--------------------------------------------------------------------------*/
/*-                                 Methods                                -*/
/*--------------------------------------------------------------------------*/

void Opponent::revive ( void )
{

      switch ( body() )
      {


         case Character_BODY_PETRIFIED :
            body ( Character_BODY_ALIVE );
            // rule note: Recovering from petrified does not imply any risk
         break;
         case Character_BODY_DEAD :
            if ( dice (100) < (unsigned int) soul () )
            {
               body ( Character_BODY_ALIVE);
               lose_soul (4);
            }
            else
               body ( Character_BODY_ASHES );
         break;
         case Character_BODY_ASHES :
            if ( dice (100) < (unsigned int) soul () )
            {
               body ( Character_BODY_ALIVE);
               lose_soul (4);
            }
            else
               body ( Character_BODY_DELETED);
         break;

      }





}



int Opponent::d20stat ( int statID )
{
   return ( p_stat.d20 [ statID ][ D20_NB_MODIFIER ]);
}

int Opponent::d20stat ( int statID, int modifier )
{
   return ( p_stat.d20 [ statID ][ modifier ]);
}

int Opponent::xyzstat ( int statID )
{
   return ( p_stat.xyz [ statID ][ XYZ_NB_MODIFIER ]);
}

int Opponent::xyzstat ( int statID, int modifier )
{
   return ( p_stat.xyz [ statID ][ modifier ]);
}


void Opponent::clear_stat ( void )
{
   int i;
   int j;


   for ( i = 0; i < D20_NB_STAT; i++)
      for ( j = 0; j < D20_NB_MODIFIER + 1; j++ )
         p_stat.d20 [ i ] [ j ] = 0;

   for ( i = 0; i < XYZ_NB_STAT; i++)
      for ( j = 0; j < XYZ_NB_MODIFIER + 1; j++ )
         p_stat.xyz [ i ] [ j ] = 0;

   for ( i = 0 ; i < FLAT_NB_STAT; i++ )
      p_stat.flat [ i ] = 0;
}

void Opponent::sum_stat (void)
{
   int i;
   int j;

   for ( i = 0; i < D20_NB_STAT; i++ )
      for ( j = 0; j < D20_NB_MODIFIER; j++ )
         p_stat.d20 [ i ] [ D20_NB_MODIFIER ] +=  p_stat.d20 [ i ] [ j ];

   for ( i = 0; i < XYZ_NB_STAT; i++)
      for ( j = XYZMOD_ZATTRIBUTE; j < XYZ_NB_MODIFIER; j++ )
         p_stat.xyz [ i ] [ XYZ_NB_MODIFIER ] += p_stat.xyz [ i ] [ j ];

   // min 3
   if ( p_stat.flat [ FLAT_ATTDIV ] < 3 )
      p_stat.flat [ FLAT_ATTDIV ] = 3;

   if ( p_stat.flat [ FLAT_POWDIV ] < 3 && p_stat.flat [ FLAT_POWDIV ] != 0 )
      p_stat.flat [ FLAT_POWDIV ] = 3;

}


int Opponent::compile_condition ( void )
{
   ActiveEffect tmpeffect;
   int tmpcondition = 0;
   int error;

   error = tmpeffect.SQLpreparef ( "WHERE target_type=%d AND target_id=%d", type() , p_primary_key );

   if ( error == SQLITE_OK)
   {
      error = tmpeffect.SQLstep ();

      while ( error == SQLITE_ROW )
      {
        // printf ( "Debug: Character: Compile Condition: effect condition=%d conditionstr=",
          //       tmpeffect.condition(), tmpeffect.condition_str() );

         tmpcondition = ( tmpcondition | tmpeffect.condition() );


         error = tmpeffect.SQLstep ();
      }

      tmpeffect.SQLfinalize ();
   }
   return (tmpcondition);
}

//-----------------------------------------------------------------------------------------
//-------------------------------- OLD CODE -----------------------------------------------
//-----------------------------------------------------------------------------------------

/*-------------------------------------------------------------------------*/
/*-                          Property Methods                             -*/
/*-------------------------------------------------------------------------*/
/*
const char* Opponent::name ( void )
{
   return ( p_name );
}

void Opponent::name ( const char *str )
{
   strncpy ( p_name, str, 16 );
}


void Opponent::attribute ( int attribID, int value )
{

   p_attribute [ attribID ] = value;
}

int Opponent::attribute ( int attribID )
{
   return ( p_attribute [ attribID ] );
}

void Opponent::strength ( int value )
{
   p_attribute [ Opponent_STRENGTH ] = value;
}

int Opponent::strength ( void )
{
   return ( p_attribute [ Opponent_STRENGTH ] );
}

int Opponent::STRmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Opponent_STRENGTH ] > 10 )
   {
      tmpval = p_attribute [ Opponent_STRENGTH ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Opponent_STRENGTH ];
      return ( tmpval / 2 );
   }
}

void Opponent::dexterity ( int value )
{

   p_attribute [ Opponent_DEXTERITY ] = value;
}

int Opponent::dexterity ( void )
{
   return ( p_attribute [ Opponent_DEXTERITY ] );
}

int Opponent::DEXmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Opponent_DEXTERITY ] > 10 )
   {
      tmpval = p_attribute [ Opponent_DEXTERITY ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Opponent_DEXTERITY ];
      return ( tmpval / 2 );
   }
}


void Opponent::endurance ( int value )
{

   p_attribute [ Opponent_ENDURANCE ] = value;
}

int Opponent::endurance ( void )
{
   return ( p_attribute [ Opponent_ENDURANCE ] );
}

int Opponent::ENDmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Opponent_ENDURANCE ] > 10 )
   {
      tmpval = p_attribute [ Opponent_ENDURANCE ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Opponent_ENDURANCE ];
      return ( tmpval / 2 );
   }

}


void Opponent::intelligence ( int value )
{

   p_attribute [ Opponent_INTELLIGENCE ] = value;
}

int Opponent::intelligence ( void )
{
   return ( p_attribute [ Opponent_INTELLIGENCE ] );
}

int Opponent::INTmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Opponent_INTELLIGENCE ] > 10 )
   {
      tmpval = p_attribute [ Opponent_INTELLIGENCE ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Opponent_INTELLIGENCE ];
      return ( tmpval / 2 );
   }

}


void Opponent::cunning ( int value )
{

   p_attribute [ Opponent_CUNNING ] = value;
}

int Opponent::cunning ( void )
{
   return ( p_attribute [ Opponent_CUNNING ] );
}

int Opponent::CUNmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Opponent_CUNNING ] > 10 )
   {
      tmpval = p_attribute [ Opponent_CUNNING ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Opponent_CUNNING ];
      return ( tmpval / 2 );
   }

}


void Opponent::willpower ( int value )
{

   p_attribute [ Opponent_WILLPOWER ] = value;
}

int Opponent::willpower ( void )
{
   return ( p_attribute [ Opponent_WILLPOWER ] );
}

int Opponent::WILmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Opponent_WILLPOWER ] > 10 )
   {
      tmpval = p_attribute [ Opponent_WILLPOWER ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Opponent_WILLPOWER ];
      return ( tmpval / 2 );
   }
}

void Opponent::luck ( int value )
{
   if ( value > 19 )
      value = 19;

   if ( value < 1 )
      value = 1;

   p_attribute [ Opponent_LUCK ] = value;
}

int Opponent::luck ( void )
{
   return ( p_attribute [ Opponent_LUCK ] );
}

int Opponent::LUCKmodifier ( void )
{
   int tmpval;

   if ( p_attribute [ Opponent_LUCK ] > 10 )
   {
      tmpval = p_attribute [ Opponent_LUCK ] - 10;
      return ( tmpval / 2 );
   }
   else
   {
      tmpval = -10 + p_attribute [ Opponent_LUCK ];
      return ( tmpval / 2 );
   }
}


int Opponent::level ( void )
{
   return ( p_level );
}
*/
/*void Opponent::aligment ( int value )
{
   p_aligment = value;
}

int Opponent::size ( void )
{
   return ( p_size );
}

int Opponent::aligment ( void )
{
   return ( p_aligment );
}*/

/*
short Opponent::max_HP ( void )
{
   return ( p_max_HP );
}

short Opponent::current_HP ( void )
{
   return ( p_current_HP );
}

short Opponent::max_MP ( void )
{
   return ( p_max_MP );
}

short Opponent::current_MP ( void )
{
   return ( p_current_MP );
}

int Opponent::soul ( void )
{
   return ( p_soul );
}

bool Opponent::health ( unsigned short healthID )
{
   if ( p_health & healthID > 0 )
      return ( true );
   else
      return ( false );
}

unsigned short Opponent::health ( void )
{
   return ( p_health );
}

void Opponent::status ( int statusID )
{
   p_status = statusID;
}

int Opponent::status ( void )
{
   return ( p_status );
}

short Opponent::reward_exp ( void )
{
   return ( p_reward_exp );
}

void Opponent::reward_exp ( short value )
{
   p_reward_exp = value;
}

short Opponent::gold ( void )
{
   return ( p_gold );
}

void Opponent::gold ( short value )
{
   p_gold = value;
}

unsigned char Opponent::pictureID ( void )
{
   return ( p_pictureID );
}

void Opponent::pictureID ( unsigned char value )
{
   p_pictureID = value;
}

int Opponent::type ( void )
{
   return ( p_type );
}

*/
/*--------------------------- String Functions ----------------------------*/



/*const char* Opponent::aligmentC ( void )
{
   if ( p_aligment != -1 )
      return ( ALIGMENT_INFO [ p_aligment ] . initial );
   else
      return ("?");
}*/

/*const char* Opponent::aligmentS ( void )
{
   if ( p_aligment != -1 )
      return ( ALIGMENT_INFO [ p_aligment ] . name );
   else
      return ("Unknown");
}*/


/*const char* Opponent::statusS ( void )
{
   return ( STR_OPP_STATUS [ p_status ] );
}*/

/*
s_Opponent_hstr Opponent::healthS ( void )
{
   int basechar = 0;
   int hchar = 0;
   int strchar = 0;
   int tmpcolor;
   s_Opponent_hstr tmphstr;
   bool write;
   int i;

   for ( i = 0 ; i < 16 ; i++ )
   {
      tmphstr.character[ i ].letter = ' ';
      tmphstr.character[ i ].color = makecol ( 0, 0, 0 );
   }

   basechar = 0;

   for ( i = 0 ; i < 16 ; i++ )
   {
      if ( ( p_health & HEALTH_INFO [ i ] . mask ) > 0 )
         write = true;
      else
         write = false;


      if ( write == true )
      {
         tmpcolor = makecol ( HEALTH_INFO [ i ] . red,
                              HEALTH_INFO [ i ] . green,
                              HEALTH_INFO [ i ] . blue );

         hchar = basechar;
         strchar = 0;
         while ( hchar < 16)
         {
            tmphstr.character[ hchar ].letter
               = HEALTH_INFO [ i ] . adjective [ strchar ];
            tmphstr.character[ hchar ].color = tmpcolor;
            strchar++;
            hchar++;
         }
         basechar++;
         write = false;
      }

   }

   return ( tmphstr );
}


s_Opponent_stat Opponent::stat ( void )
{
   return ( p_stat );
}*/

/*-------------------------------------------------------------------------*/
/*-                               Methods                                 -*/
/*-------------------------------------------------------------------------*/

/*
void Opponent::recover_HP ( short value )
{
   p_current_HP = p_current_HP + value;
   if ( p_current_HP > p_max_HP )
      p_current_HP = p_max_HP;
   if ( p_current_HP > 0 )
      p_status = Opponent_STATUS_ALIVE;
}

void Opponent::recover_MP ( short value )
{
   p_current_MP = p_current_MP + value;
   if ( p_current_MP > p_max_MP )
      p_current_MP = p_max_MP;
}

void Opponent::recover_soul ( int value )
{
   p_soul = p_soul + value;
   if ( p_soul > 100 )
      p_soul = 100;
}

void Opponent::lose_HP ( short value )
{
   p_current_HP = p_current_HP - value;
   if ( p_current_HP <= 0 )
   {
      p_current_HP = 0;
      p_status = Opponent_STATUS_DEAD;
   }
}



void Opponent::lose_MP ( short value )
{
   p_current_MP = p_current_MP - value;
   if ( p_current_MP < 0 )
      p_current_MP = 0;

}

void Opponent::lose_soul ( int value )
{
   p_soul = p_soul - value;
   if ( p_soul <= 0 )
   {
      p_soul = 0;
      p_status = Opponent_STATUS_DELETED;
   }
}

void Opponent::add_health ( unsigned short healthID )
{

   p_health = p_health | healthID;

}

void Opponent::remove_health ( unsigned short healthID )
{
   p_health = p_health & ( !healthID );
}

void Opponent::gain_gold ( short value )
{
   p_gold = p_gold + value;
   if ( p_gold > 30000 )
      p_gold = 30000;
}

void Opponent::lose_gold ( short value )
{
   p_gold = p_gold - value;
   if ( p_gold < 0 )
      p_gold = 0;
}

void Opponent::save_vs ( unsigned short healthID, int level )
{
   int modifier;
   int value;
   int TN;
   unsigned short mask = 1;
   unsigned short tmphealth;
   int i;

   eval_stat_all();

   for ( i = 0 ; i < 16 ; i++ )
   {
      tmphealth = ( healthID & mask );

      if ( tmphealth > 0 )
      {

         if ( tmphealth > Opponent_PHYSICAL_HEALTH_LIMIT )
         {
            modifier = p_stat.PSAVE;
         }
         else
            modifier = p_stat.MSAVE;

         value = rnd(20) + modifier;
         //TN = HEALTH_INFO [ i ] . TN + ( level / 5 ); //?? + difficulty modifier

         if ( ( tmphealth & p_stat.hlt_resist ) == 0 ) // health resistance
         {
            if ( value < TN )
            {
               if ( p_type != Opponent_TYPE_ENNEMY )
                  value = rnd( luck() ) + modifier;
               else
                  value = 0;
               // when a roll is failed, a luck roll is allowed
               if ( health ( Opponent_HEALTH_CURSED ) == false )
                  if ( value < TN )
                  {
                     add_health ( tmphealth );
                  }

            }
         }
      }
      mask = mask << 1;
   }
}

void Opponent::eval_stat_all ( void )
{
   eval_stat_clear ();
   eval_stat_base ();
   eval_stat_other ();
   // ?? add eval spell
   eval_stat_maze ();
   eval_stat_health ();
   eval_stat_enc_modifier ();
   eval_stat_wound ();

   p_stat.AD -= p_stat.encmod;
   p_stat.hitbonus -= p_stat.encmod;

   eval_stat_limit ();
}

void Opponent::eval_stat_clear ( void )
{
   p_stat.elm_resist = 0; // |
   p_stat.hlt_resist = 0; // |
   p_stat.elm_effect = 0; // |
   p_stat.hlt_effect = 0; // |
   p_stat.magikproperty = 0; // |
   p_stat.multihitmod = 0;
   p_stat.hitbonus = 0; // |
   p_stat.encmod= 0;
   p_stat.dmg_x = 0; // |
   p_stat.dmg_y = 0;
   p_stat.dmg_z = 0; // ||
   p_stat.dmg_type = 0; // |
   p_stat.mdmg_x = 0; // |
   p_stat.mdmg_y = 0;
   p_stat.mdmg_z = 0; // ||
   p_stat.range = 0; // |
//   p_stat.hitroll = 0; // |
   p_stat.nb_max_attack = 0; // |
   p_stat.PD = 0; // |
   p_stat.AD = 0; // |||
   p_stat.DR = 0; // |
   p_stat.MDR = 0; // |
   p_stat.MPD = 0; // |
   p_stat.MAD = 0; // |
   p_stat.PSAVE = 0; // |
   p_stat.MSAVE = 0; // |
   p_stat.init = 0; // ||

}
*/

/*
void Opponent::eval_stat_base ( void )
{
   int levelmod;

   // random Damage bonus from level
   levelmod = p_level / 5;
   p_stat.dmg_y += levelmod;
   p_stat.mdmg_y += levelmod;

   // Damage bonus from attributes
//   p_stat.dmg_z += STRmodifier();
   p_stat.mdmg_z += INTmodifier();

   // Base Evade
   // maybe determined by class
//   p_stat.AD += ( cunning() + dexterity() ) / 4;

   p_stat.init += CUNmodifier();
   p_stat.PSAVE += ENDmodifier();
   p_stat.MSAVE += WILmodifier();
   p_stat.AD += DEXmodifier();


   // size modification
   switch ( p_size )
   {
      case Opponent_SIZE_TINY :
         p_stat.AD = p_stat.AD + 4;
         p_stat.init = p_stat.init + 4;
      break;
      case Opponent_SIZE_SHORT :
         p_stat.AD = p_stat.AD + 2;
         p_stat.init = p_stat.init + 2;
      break;
      case Opponent_SIZE_LARGE :
         p_stat.AD = p_stat.AD - 2;
         p_stat.init = p_stat.init - 2;
      break;
      case Opponent_SIZE_HUGE :
         p_stat.AD = p_stat.AD - 4;
         p_stat.init = p_stat.init - 4;
      break;
   }

//?? to add maybe : Multi hit modifier according to size

}
void Opponent::eval_stat_wound ( void )
{
   short penalty = p_stat.multihit / 4;

   // reduce multi hit accourding to lost HP

   if ( p_current_HP < ( p_max_HP / 2 ) )
   {
      p_stat.multihit = p_stat.multihit - penalty;

      if ( p_current_HP < ( p_max_HP / 4 ) )
         p_stat.multihit = p_stat.multihit - penalty;
   }

}


void Opponent::eval_stat_health ( void )
{

   if ( (  p_health & Opponent_HEALTH_CRIPPLED ) > 0 )
   {
      p_stat.multihitmod += 2;
      p_stat.hitbonus -= 2;
      p_stat.dmg_z -= 2;
      p_stat.mdmg_z -= 2;
      p_stat.AD -= 2;
      p_stat.MAD -= 2;
      p_stat.init -=2;
   }

   if ( (  p_health & Opponent_HEALTH_BLINDED ) > 0 )
   {
      p_stat.multihitmod += 4;
      p_stat.hitbonus -= 4;
      p_stat.AD -= 4;
      p_stat.MAD -= 4;
   }

   if ( (  p_health & Opponent_HEALTH_AFFRAID ) > 0 )
   {
      p_stat.multihitmod += 2;
      p_stat.hitbonus -= 2;
      p_stat.init -= 4;
   }

   if ( (  p_health & Opponent_HEALTH_SEALED ) > 0 )
   {
      p_stat.mdmg_z -= 4;
      p_stat.MAD = 0;
   }

   if ( (  p_health & Opponent_HEALTH_CURSED ) > 0 )
   {
      p_stat.PSAVE -= 8;
      p_stat.MSAVE -= 8;
   }

   if ( (  p_health & Opponent_HEALTH_ASLEEP ) > 0 )
   {
      p_stat.AD = 0;
      p_stat.MAD = 0;
   }

   if ( (  p_health & Opponent_HEALTH_PARALYSED ) > 0 )
   {
      p_stat.AD = 0;
      p_stat.MAD = 0;
   }

   if ( (  p_health & Opponent_HEALTH_PETRIFIED ) > 0 )
   {
      p_stat.AD = 0;
      p_stat.MAD = 0;
   }

}

void Opponent::eval_stat_maze ( void )
{

}

void Opponent::eval_stat_enc_modifier ( void )
{
   if ( p_stat.bulk > 0 )
   {
      p_stat.roll [ Opponent_ROLLSTAT_EVADE ] . modifier -= p_stat.bulk;
      p_stat.roll [ Opponent_ROLLSTAT_INITIATIVE ] . modifier -= p_stat.bulk;
      p_stat.roll [ Opponent_ROLLSTAT_BLOCK ] . modifier -= p_stat.bulk;
      p_stat.roll [ Opponent_ROLLSTAT_PARRY ] . modifier -= p_stat.bulk;
      p_stat.roll [ Opponent_ROLLSTAT_HIT ] . modifier -= p_stat.bulk;
   }
}

void Opponent::eval_stat_limit ( void )
{
   if ( p_stat.AD < 0 )
      p_stat.AD = 0;

   if ( p_stat.dmg_y < 2 )
      p_stat.dmg_y = 2;

   if ( p_stat.mdmg_y < 2 )
      p_stat.mdmg_y = 2;

   if ( p_stat.multihitmod < 0 )
      p_stat.multihitmod = 0;


}

int Opponent::initiative ( void )
{
   return ( dice ( 20 ) + p_stat.init );
}

bool Opponent::is_active ( void )
{
   bool retval = true;

   if ( (  p_health & Opponent_HEALTH_ASLEEP ) > 0 )
      retval = false;

   if ( (  p_health & Opponent_HEALTH_PARALYSED ) > 0 )
      retval = false;

   if ( (  p_health & Opponent_HEALTH_PETRIFIED ) > 0 )
      retval = false;

   if ( p_status != Opponent_STATUS_ALIVE )
      retval = false;

   return ( retval );

}

void Opponent::combat_action ( int action_type,
   int value, char *result, Opponent &target  )
{

   switch ( action_type )
   {
      //case Combat_COMMAND_ZAP :
      //   cmbact_zap ( value, result, target );
      //break;
      case Combat_COMMAND_FIGHT :
         cmbact_fight ( value, result, target );
      break;
      case Combat_COMMAND_PARRY :
         cmbact_parry ( value, result );
      break;
      case Combat_COMMAND_HIDE :
      break;
      case Combat_COMMAND_USE_ITEM :
      break;
//      case Combat_COMMAND_SWITCH_STYLE :
//      break;
//      case Combat_COMMAND_MOVE :
//      break;
//      case Combat_COMMAND_CAST_SPELL :
//      break;
//      case Combat_COMMAND_DISPEL :
//      break;
//      case Combat_COMMAND_WEILD_MAGIC :
//      break;
//      case Combat_COMMAND_PREVIOUS :
//      break;
      default:
         sprintf ( result, "!Command not Interpreted!" );
      break;
   }
}
*/
/*-------------------------------------------------------------------------*/
/*-                         Private Methods                               -*/
/*-------------------------------------------------------------------------*/

/*
void Opponent::cmbact_zap ( int value, char *result, Opponent &target )
{
   target.p_status = Opponent_STATUS_DEAD;
   target.p_current_HP = 0;

   sprintf ( result,"%s zap instantly %s", p_name, target.p_name );
}

void Opponent::cmbact_fight ( int value, char *result, Opponent &target )
{
   int attackroll;
   int attackno;
   int i;
   bool attackmissed;
   short totaldamage;
   short tmpval;
   int mhitmod;
   int defense;
   int dicedamage;
   int j;
   int hltbackup;
   unsigned short mask;
//   short mindamage;

   mhitmod = 0;
   attackno = 0;
   attackmissed = false;
   totaldamage = 0;

   WinData<Opponent> wdat_opponnent_stat ( WDatProc_opponent_stat,
      *this, WDatProc_POSITION_OPPONENT_STAT );
   Window::show_all();
   copy_buffer();
   while ( ( readkey() >> 8 ) != KEY_ENTER );


   while ( attackmissed == false && attackno < p_stat.nb_max_attack )
   {
      if ( attackno == 1 )
         mhitmod = p_stat.multihitmod;
      else
         if ( attackno > 1 )
            mhitmod += 5;

      attackroll = dice ( 20 ) + p_stat.hitbonus - mhitmod;
      defense = target.p_stat.PD + dice ( target.p_stat.AD );

      if ( attackroll > defense )
      {
         for ( j = 0 ; j < p_stat.dmg_x ; j++ )
         {
            dicedamage = dice ( p_stat.dmg_y ) + p_stat.dmg_z - target.p_stat.DR;
            if ( dicedamage < 1 )
               dicedamage = 1;
            totaldamage += dicedamage;
         }

         attackno++;
      }
      else
         attackmissed = true;
   }

   hltbackup = p_health;
   if ( totaldamage > 0 )
   {

      totaldamage = totaldamage - target.p_stat.DR;
      if ( totaldamage < 1 )
         totaldamage = 1;
      totaldamage += p_stat.dmg_z;

      target.lose_HP ( totaldamage );
      save_vs ( target.p_stat.hlt_effect, target.p_level );
   }

   strcpy ( result, "???" );

   if ( attackno > 0 )
   {
      sprintf ( result, "%s attack %s \n %d time for %d damage",
         p_name, target.p_name, attackno, totaldamage );
   }
   else
      sprintf ( result, "%s attack %s and miss", p_name, target.p_name );

   if ( target.status() != Opponent_STATUS_ALIVE )
   {
      strcat ( result, "\n" );
      strcat ( result, target.p_name );
      strcat ( result, " " );
      strcat ( result, STR_CMB_STATUS [ target.status() ] );
   }
   else
   {
      if ( p_health > hltbackup )
      {
         strcat ( result, "\n" );
         strcat ( result, p_name );
         strcat ( result, " " );

         hltbackup = p_health = hltbackup;
         mask = 1;

         for ( i = 0 ; i < 16 ; i++ )
         {
            if ( ( hltbackup & mask ) > 0 )
            {
               strcat ( result, HEALTH_INFO [ i ] . adjective );
               strcat ( result, ", " );
            }
         }
      }
   }





}

void Opponent::cmbact_parry ( int value, char *result )
{
   p_stat.AD += 4;
   p_stat.PSAVE += 2;
   p_stat.MSAVE += 2;

   if ( p_stat.MAD > 0 )
      p_stat.MAD += 4;

   sprintf ( result,"%s stay on the defensive", p_name );

}

void Opponent::cmbact_use_item ( int value, char *result )
{

}


void Opponent::cmbact_move ( int value, char *result )
{

} */



/*-------------------------------------------------------------------------*/
/*-                       Virtual Functions                               -*/
/*-------------------------------------------------------------------------*/

/*void Opponent::eval_stat_other ( void )
{
   // job done by derived class
}


void Opponent::name_combat ( char *str )
{
   // job done by derived class
} */

/*
void Opponent::objdat_to_strdat ( void *dataptr )
{
   dbs_Opponent &tmpdat = *(static_cast<dbs_Opponent*> ( dataptr ));

   int i;

   strncpy ( tmpdat.name, p_name, 16 );
   tmpdat.level = p_level;
   tmpdat.type = p_type;
   tmpdat.size = p_size;
   tmpdat.aligment = p_aligment;
   tmpdat.max_HP = p_max_HP;
   tmpdat.current_HP = p_current_HP;
   tmpdat.max_MP = p_max_MP;
   tmpdat.current_MP = p_current_MP;
   tmpdat.soul = p_soul;
   tmpdat.health = p_health;
   tmpdat.status = p_status;
   tmpdat.reward_exp = p_reward_exp;
   tmpdat.gold = p_gold;
   tmpdat.pictureID = p_pictureID;

   for ( i = 0 ; i < 7 ; i++ )
      tmpdat.attribute [ i ] = p_attribute [ i ];

}

void Opponent::strdat_to_objdat ( void *dataptr )
{
   dbs_Opponent &tmpdat = *(static_cast<dbs_Opponent*> ( dataptr ));

   int i;

   strcpy ( p_name, tmpdat.name );
   p_type = tmpdat.type;
   p_size = tmpdat.size;
   p_level = tmpdat.level;
   p_aligment = tmpdat.aligment;
   p_max_HP = tmpdat.max_HP;
   p_current_HP = tmpdat.current_HP;
   p_max_MP = tmpdat.max_MP;
   p_current_MP = tmpdat.current_MP;
   p_soul = tmpdat.soul;
   p_health = tmpdat.health;
   p_status = tmpdat.status;
   p_reward_exp = tmpdat.reward_exp;
   p_gold = tmpdat.gold;
   p_pictureID = tmpdat.pictureID;

   for ( i = 0 ; i < 7 ; i++ )
      p_attribute [ i ] = tmpdat.attribute [ i ];
}
*/
/*
void Opponent::child_DBremove ( void )
{
   // no work to be done
}
  */


/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

/*const char STR_OPP_SIZE [][7] =
{
   {"Tiny"},
   {"Small"},
   {"Medium"},
   {"Large"},
   {"Huge"},
};




const char STR_OPP_DEFENSE [] [ 11 ] =
{
   {"Parried"},
   {"Blocked"},
   {"Evaded"},
   {"Deflected"},
};



s_Opponent_aligment_info ALIGMENT_INFO [ Opponent_NB_ALIGMENT ] =
{
   { "Kind","KN" },
   { "Peacefull","PF" },
   { "Lively","LV" },
   { "Sellfish","SF" },
   { "Aggressive","AG" },
   { "Stoical","ST" }
};
*/




