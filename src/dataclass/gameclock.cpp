/****************************************************************************/
/*                                                                          */
/*                       G A M E C L O C K . C P P                          */
/*                                                                          */
/*   Content: Class GameClock                                               */
/*   Programmer: Eric Pietrocupo                                            */
/*   Starting Date: June 24th, 2012                                         */
/*                                                                          */
/*   This is some sort of time class that allows to keep track of the game  */
/*   clock. Only minute, hours and days are recorded. There is no calendar. */
/*                                                                          */
/****************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <gameclock.h>


/*--------------------------------------------------------------------------*/
/*-                      Constructor and destructor                        -*/
/*--------------------------------------------------------------------------*/

GameClock::GameClock ( unsigned short day, unsigned char hour, unsigned char minute )
{
   p_day = day;
   p_hour = hour;
   p_minute = minute;
   strcpy ( clockstr, "");
}

GameClock::~GameClock ( void )
{

}

/*--------------------------------------------------------------------------*/
/*-                           Property Methods                             -*/
/*--------------------------------------------------------------------------*/



unsigned short GameClock::day ( void )
{
   return ( p_day);
}

void GameClock::day ( unsigned short value )
{
   p_day = value;
}

unsigned char GameClock::hour ( void )
{
   return ( p_hour);
}

void GameClock::hour ( unsigned char value )
{
   if ( value < 24)
      p_hour = value;
}

unsigned char GameClock::minute ( void )
{
   return ( p_minute );
}

void GameClock::minute ( unsigned char value )
{
   if ( value < 60 )
      p_minute = value;
}

unsigned int GameClock::stamp ( void )
{
   int stamp = 0;

   stamp += p_day << 16;
   stamp += p_hour << 8;
   stamp += p_minute;

   return stamp;
}

void GameClock::stamp ( unsigned int value )
{

   p_day = value >> 16;
   p_hour = ( value & GameClock_STAMP_HOUR) >> 8;
   p_minute = ( value & GameClock_STAMP_MINUTE);

}


/*--------------------------------------------------------------------------*/
/*-                               Methods                                  -*/
/*--------------------------------------------------------------------------*/


void GameClock::add_day ( int value )
{
   p_day += value;
   ActiveEffect::reduce_time( 1440 );

  /* SQLbegin_transaction();
   for ( int i; i < ( value * 1440 ); i++)
      ActiveEffect::trigger_modulo_effects ();
   SQLcommit_transaction();*/
}

void GameClock::add_hour ( int value )
{
   p_hour += value;

   while ( p_hour >= 24)
   {
      p_day++;
      p_hour -= 24;
   }
   ActiveEffect::reduce_time( value * 60);

   /*SQLbegin_transaction();
   for ( int i; i < (value *60 ); i++)
      ActiveEffect::trigger_modulo_effects ();
   SQLcommit_transaction();*/
}

void GameClock::add_minute ( int value )
{
   p_minute += value;

   while ( p_minute >=60)
   {
      add_hour( 1 );
      p_minute -= 60;
   }
   ActiveEffect::reduce_time( value );

   SQLbegin_transaction();
   for ( int i = 0; i < value; i++)
      ActiveEffect::trigger_modulo_effects ();
   SQLcommit_transaction();
}

const char* GameClock::complete_str ( void )
{

   sprintf ( clockstr, "%5d:%02d:%02d", p_day, p_hour, p_minute );

   return ( clockstr);
}

const char* GameClock::time_str ( void )
{

   sprintf ( clockstr, "%02d:%02d", p_hour, p_minute );

   return ( clockstr );
}
