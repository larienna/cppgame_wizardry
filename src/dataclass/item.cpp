/***************************************************************************/
/*                                                                         */
/*                             I T E M . C P P                             */
/*                            Class source code                            */
/*                                                                         */
/*     Content : Class Item source code                                    */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : April 30th 2002                                     */
/*                                                                         */
/***************************************************************************/



// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>



/*
//#include <time.h>
#include <allegro.h>  //?? temporary, to remove
//#include <allegrowrapper.h>

//#include <datafile.h>
//#include <datmacro.h>
//#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
//#include <ddt.h>
#include <dbdef.h>

//

//
//
//
//#include <list.h>
#include <opponent.h> //?? temporary, to remove
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
*/


s_SQLfield Item::p_SQLfield [ NB_FIELD ] =
{
	{ "name",		TEXT, Item_NAME_SIZE,                  "Name", true, NULL},
	{ "fakename",	TEXT, Item_NAME_SIZE,                  "Fake Name", true, NULL},
	{ "price",		INTEGER, 0,                            "Price", true, NULL},
	{ "type",		INTEGER, 0,                            "Type", true, NULL},
	{ "profiency",	STRFIELD, EnhancedSQLobject_STRFIELD_LEN,  "Profiency", true, STRFLD_PROFIENCY},
   { "attrib_att",INTEGER, 0,                            "Attack Modifier", true, NULL},
	{ "attrib_dmg",INTEGER, 0,                            "Damage Modifier", true, NULL},
	{ "hand",		INTEGER, 0,                            "Nb Hand", true, NULL},
	{ "range",		INTEGER, 0,                            "Range", true, NULL},
	{ "defense",	STRFIELD, EnhancedSQLobject_STRFIELD_LEN,   "Defense", true, STRFLD_DEFENSE},
	{ "element",	STRFIELD, EnhancedSQLobject_STRFIELD_LEN,  "Element", true, STRFLD_ELEMENTAL_PROPERTY},
	{ "lockey",		INTEGER, 0,                            "Location Key", false, NULL},
	{ "loctype",	INTEGER, 0,                            "Location Type", false, NULL},
	{ "charge",		INTEGER, 0,                            "Charge", true, NULL},
	{ "max_charge",INTEGER, 0,                            "Max Charge", true, NULL},
	{ "key",		   INTEGER, 0,                            "Key", false, NULL},
	{ "cursed",		INTEGER, 0,                            "Cursed", true, NULL},
	{ "rarity",		INTEGER, 0,                            "Rarity", true, NULL},
	{ "identified",INTEGER, 0,                            "Identified", true, NULL},
	{ "ad",		   INTEGER, 0,                            "Active Defense", true, NULL},
	{ "md",		   INTEGER, 0,                            "Magic Defense", true, NULL},
	{ "ps",		   INTEGER, 0,                            "Physical Saves", true, NULL},
	{ "ms",		   INTEGER, 0,                            "Mental Saves", true, NULL},
	{ "melee_cs",	INTEGER, 0,                            "Meele Combat Skills", true, NULL},
	{ "range_cs",	INTEGER, 0,                            "Range Combat Skills", true, NULL},
	{ "dr",		   INTEGER, 0,                            "Damage Resistance", true, NULL},
	{ "init",		INTEGER, 0,                            "Initiative", true, NULL},
	{ "dmgy",		INTEGER, 0,                            "Damage Y", true, NULL},
	{ "dmgz",		INTEGER, 0,                            "Damage Z", true, NULL},
	{ "mdmgz",		INTEGER, 0,                            "Magic Damage Z", true, NULL},
	{ "mh",		   INTEGER, 0,                            "Multi Hit Penalty", true, NULL},
	{ "attdiv",		INTEGER, 0,                            "Attack Divider", true, NULL},
	{ "powdiv",		INTEGER, 0,                            "Power Divider", true, NULL},
	{ "bonusmalus",STRFIELD, EnhancedSQLobject_STRFIELD_LEN,  "Bonus or Malus", false, STRFLD_BONUS },
	{"powerupdown",STRFIELD, EnhancedSQLobject_STRFIELD_LEN,  "Power up or down", false, STRFLD_POWERUP}
};
/*-------------------------------------------------------------------------*/
/*-                    Constructor & destructor                           -*/
/*-------------------------------------------------------------------------*/


Item::Item ( void )
{
   //int i;
   //int j;

   p_SQLdata [ 0 ] . str = p_name;
   p_SQLdata [ 1 ] . str = p_fakename;
   p_SQLdata [ 4 ] . str = p_profiency;
   p_SQLdata [ 9 ] . str = p_defense;
   p_SQLdata [ 10 ] . str = p_element;
   p_SQLdata [ 33 ] . str = p_bonusmalus;
   p_SQLdata [ 34 ] . str = p_powerupdown;

// for enhanced SQL object, link right data.
   p_SQLfield_ptr = p_SQLfield;
   p_SQLdata_ptr = p_SQLdata;
   p_nb_field = NB_FIELD;

   set_all_int ( 0 );
   set_all_str  ( "" );
/*
   strcpy ( p_name, "" );
   strcpy ( p_fakename, "" );
   p_type = Item_TYPE_OTHER;

   p_key = -1;


*/
/*   p_ability.type = Item_ABILITY_NONE;
   p_ability.value = 0;
   p_ability.exhausted = Item_EXHAUST_BREAK;
   p_ability.max_charge = 0;
   p_ability.drawback = 0;
   p_ability.nb_charge = 0;
   p_autoability = Item_AUTOABILITY_NONE;
   p_hlteffect = Item_HLTEFFECT_NONE;
   p_elmeffect = Item_ELMEFFECT_NONE;*/
   //p_magikproperty = 0;
/*   p_price = 0;

   p_category = 0;
   p_clumsy = 0;
   p_attribute = 0;
   p_hand = 0;
   p_range = 0;
   p_special = 0;
   p_property = 0;
   p_location_key = 0;
   p_location_type = Item_LOCATION_UNKNOWN;
   p_charges_current = 0;
   p_charges_max = 0;
   p_cursed = true;
   p_rarity = 0;
   p_identified = false;
   p_statbonus = 0;

   for ( i = 0; i < D20_NB_STAT; i++)
      //for ( j = 0; j < D20_NB_MODIFIER; j++)
         p_d20stat [ i ] = 0;

   for ( i = 0; i < Item_XYZ_NB_STAT; i++)
      //for ( j = 0; j < XYZ_NB_MODIFIER; j++)
         p_xyzstat [ i ] = 0;*/


   strcpy (p_tablename, "item");
   strcpy (p_template_tablename, "item_template");

}

Item::~Item ( void )
{

}
/*-------------------------------------------------------------------------*/
/*-                           Property Methods                            -*/
/*-------------------------------------------------------------------------*/


const char* Item::name ( void )
{
   return ( p_name );
}

const char* Item::fakename ( void )
{
   return ( p_fakename );
}

void Item::name ( const char* real, const char* fake )
{
   strncpy ( p_name, real, Item_NAME_SIZE  );
   strncpy ( p_fakename, fake, Item_NAME_SIZE  );
}
/*
dbs_Item_ability Item::ability ( void )
{
   return ( p_ability );
}

void Item::ability ( dbs_Item_ability data )
{
   p_ability = data;
   p_ability.nb_charge = p_ability.max_charge;
}

void Item::ability ( int type, int value, int max_charge, int exhausted,
    int drawback )
{
   p_ability.type = type;
   p_ability.value = value;
   p_ability.max_charge = max_charge;
   p_ability.exhausted = exhausted;
   p_ability.drawback = drawback;
   p_ability.nb_charge = max_charge;
}

int Item::autoability ( void )
{
   return ( p_autoability );
}

void Item::autoability ( int value )
{
   p_autoability = value;
}


unsigned int Item::hlteffect ( void )
{
   return ( p_hlteffect );
}

void Item::hlteffect ( unsigned int value )
{
   p_hlteffect = value;
}

unsigned int Item::elmeffect ( void )
{
   return ( p_elmeffect );
}

void Item::elmeffect ( unsigned int value )
{
   p_elmeffect = value;
}
*/

/*int  Item::nb_charge ( void )
{
   return ( p_ability.nb_charge );
}*/

/*bool  Item::cursed ( void )
{
   if (( p_status & Item_STATUS_CURSED ) > 0 )
      return ( true );
   else
      return ( false );
}*/

int  Item::key ( void )
{
   return ( get_int ( 15 ) );
}

bool  Item::identified ( void )
{
   if ( get_int (18) == 1)
     return true;
   else
      return false;
   //return ( p_identified );
}

void Item::identified ( bool value )
{
   if ( value == true )
      set_int ( 18, 1 );
   else
      set_int ( 18, 0 );

   //p_identified = value;
}

/*float Item::weight ( void )
{
   return ( p_weight );
}

void Item::weight ( float value )
{
   p_weight = value;
}*/

int  Item::price ( void )
{
   return ( get_int (2) );
}

void Item::price ( int value )
{
   set_int ( 2, value );
}

/*unsigned int Item::status ( void )
{
   return ( p_status );
}

void Item::status ( unsigned int value )
{
   p_status = value;
}*/

int Item::rarity ( void )
{
   return ( get_int ( 17 ) );
}

void Item::rarity ( int value )
{
   set_int (17, value);
}

int Item::profiency ( void )
{
   return ( get_int (4));
}

void Item::profiency ( int value )
{
   set_int ( 4, value );
}

/*int Item::clumsy ( void )
{
   return ( p_clumsy);
}

void Item::clumsy ( int value )
{
   p_clumsy = value;
}*/

int Item::attribute_attack ( void )
{
   return ( get_int ( 5 ));
}

void Item::attribute_attack ( int value )
{
   set_int ( 5 , value);
}

int Item::hand ( void )
{
   return ( get_int ( 7 ));
}

void Item::hand ( int value )
{
   set_int ( 7 , value );
}

int Item::range ( void )
{
   return ( get_int ( 8 ) );
}

void Item::range ( int value )
{
   set_int ( 8, value);
}

int Item::defense ( void )
{
   return ( get_int ( 9 ));
}

void Item::defense ( int value )
{
   set_int ( 9 , value );
}

int Item::d20stat ( int stat  )
{
   return ( get_int ( 19 + stat ));
}

void Item::d20stat ( int stat, int value )
{
   set_int ( 19 + stat, value );
}

/*int Item::xyzstat ( int stat )
{
   return ( p_xyzstat [ stat ]);
}

void Item::xyzstat ( int stat, int value )
{
   p_xyzstat[stat] = value;
}*/

int Item::element ( void )
{
   return ( get_int ( 10 ));
}

void Item::element ( int value )
{
   set_int ( 10 , value );
}

int Item::location_key ( void )
{
   return ( get_int ( 11 ));
}

int Item::location_type ( void )
{
   return ( get_int ( 12 ));
}

void Item::location ( int type, int key )
{
   set_int ( 11, key );
   set_int ( 12, type );
}

int Item::charges_current ( void )
{
   return ( get_int ( 13 ));
}

int Item::charges_max ( void )
{
   return ( get_int ( 14 ));
}

void Item::charges ( int current, int max )
{
   set_int ( 13, current );
   set_int ( 14, max );
}

bool Item::cursed ( void )
{
   if ( get_int ( 16 ) == 1)
      return true;
   else
      return false;
}

void Item::cursed ( bool value )
{
   if ( value == true)
      set_int ( 16, 1 );
   else
      set_int ( 16, 0 );

}


/*unsigned int Item::magikproperty ( void )
{
   return ( p_magikproperty );
}

void Item::magikproperty ( unsigned int value )
{
   p_magikproperty = value;
}

int Item::location ( void )
{
   return ( p_location );
}

void Item::location ( int value )
{
   p_location = value;
}
*/

const char* Item::vname ( void )
{
   if ( identified() == true )
      return ( p_name );
   else
      return ( p_fakename );
}

/*void Item::statusS1 ( char *str )
{
   strcpy ( str, "" );

   if ( p_status == 0 )
      strcat ( str , "Normal " );

   if ( ( p_status & Item_STATUS_FOUND ) > 0 )
      strcat ( str, "Normal " );

   strcat ( str, STR_ITM_RARITY [ p_rarity ] );

//   if ( ( p_status & Item_STATUS_FINE ) > 0 )
//      strcat ( str , "Fine " );

   if ( ( p_status & Item_STATUS_ENCHANTED ) > 0 )
      strcat ( str, "Enchanted ");

   if ( ( p_status & Item_STATUS_ARTIFACT ) > 0 )
      strcat ( str , "Artifact " );


}*/


void Item::statusS2 ( char *str )
{
   strcpy ( str, "" );

   if ( cursed() == true )
      strcat ( str, "Cursed " );

//   if ( ( p_status & Item_STATUS_UNIQUE ) > 0 )
//      strcat ( str, "Unique " );

   if ( key() > 0 )
      strcat ( str, "Key " );

   strcat ( str, "Item" );


}

/*const char* Item::typeS ( void )
{
   return ( STR_ITM_TYPE [ p_type ] );
}

void Item::abilityS ( char* str )
{

   // utiliser une table de string printf avec 1 variable en parametre

   switch ( p_ability.type )
   {
      case Item_ABILITY_NONE :
      break;
      case Item_ABILITY_SPELL :
         sprintf ( str, "%s Spell", STR_ITM_ABILITY [ p_ability.type ] );
      break;
      case Item_ABILITY_RAISE_MAXHP :
      case Item_ABILITY_RAISE_MAXSP :
      case Item_ABILITY_GAIN_EXP :
      case Item_ABILITY_LOWER_AGE :
      case Item_ABILITY_RAISE_SOUL :
         sprintf ( str, "%s %d", STR_ITM_ABILITY [ p_ability.type ],
            p_ability.value );
      break;
      case Item_ABILITY_RECOVER_HP :
      case Item_ABILITY_RECOVER_MP :
         sprintf ( str, "%s %d-%d", STR_ITM_ABILITY[ p_ability.type ],
            ( p_ability.value / 2 ), p_ability.value );
      break;
      case Item_ABILITY_RAISE_ATTRIBUTE :
      break;
      case Item_ABILITY_CURE_HEALTH :
      break;
      case Item_ABILITY_RAISE_STAT :
      break;
   }
}
const char* Item::autoabilityS ( void )
{
   return ("");
}


const char* Item::exhaustedS( void )
{
   return ("-");
}


const char* Item::drawbackS( void )
{
   return ("-");
}*/


/*-------------------------------------------------------------------------*/
/*-                              Methods                                  -*/
/*-------------------------------------------------------------------------*/

void Item::recharge ( void )
{
   //p_ability.nb_charge = p_ability.max_charge;
   charges_current (charges_max() );
}

bool Item::is_key ( int lockID )
{
   if ( key() == lockID )
      return ( true );
   else
      return ( false );
}

void Item::generate_ability ( /*int rarity,*/ bool cursed_enable )
{
   //int fibonnaci [ 6 ] = { 1, 2, 3, 5, 8, 13 };
   int costable [ 6 ] = { 1, 3, 8, 21, 55, 144 };
   int final_price = 0;
   //int tmprarity = rarity * 10;
   int modulo;
   int bonus = 0;
   int result;
   char tmpstr [100];
   //unsigned int tmpbitfield;
   unsigned int testbit;
   int i;

   if ( cursed_enable == true)
   {
      if ( dice(4) == 1 &&
          config.get (Config_CURSED_ITEM ) == Config_DIF_ENABLED )
         cursed (true);
   }

   // -------------- Size Modifier (great, short) --------------------------

   if ( ( powerupdown () & Item_POWERUP_DMGY ) > 0 )
   {
      switch ( dice ( 10 ))
      {
         case 1:
         case 2:
            sprintf ( tmpstr, "Great %s", p_name);
            strncpy ( p_name, tmpstr, Item_NAME_SIZE );
            price ( ( price() * 125 ) / 100 ); // price +50% or original price (new is 66% of old)
            dmgy ( dmgy() + 2);
            mh ( mh() -1 ); // maybe too strong, counter balance each other.
         break;
         case 3:
         case 4:
            sprintf ( tmpstr, "Short %s", p_name);
            strncpy ( p_name, tmpstr, Item_NAME_SIZE );
            price ( (price() * 80) / 100 );  // price 66% of original price
            dmgy ( dmgy() - 2 );
            mh ( mh() + 1 );
         break;

      }
   }
   //printf ("Generating and item\r\n");
   // ----------- +X bonus ----------

   //todo: check for cursed items - apply minus if true. Check if config allows it. 20% cursed
   //also use "bool cursed_enable"

   bonus = rarity() / 4;
   modulo = rarity() % 4;
   //printf ("base Bonus=%d, rarity=%d, modulo=%d \r\n", bonus, rarity,  modulo);

   result = dice(4);
   if ( result <= modulo )
      bonus++;

   if ( bonus == 0 && type() == Item_TYPE_ACCESSORY)
      bonus=1;

   if ( type() == Item_TYPE_EXPANDABLE)
   {
      bonus = 0;
      cursed( false );
   }

   //printf ("base Bonus=%d, result=%d, modulo=%d \r\n", bonus, result, modulo);

   final_price = price() * costable[ bonus ];

   // apply D20 stats
   //tmpbitfield = p_statbonus & Item_BONUS_D20;

   if ( bonus == 0)
      cursed(false );

   if ( cursed() == true )
      bonus = 0 - bonus;



   testbit = 1;
   for ( i = 0 ; i < 8; i++)
   {
      if ( ( testbit & bonusmalus() ) > 0)
         d20stat ( i , d20stat( i ) + bonus );
      testbit = testbit<<1;
   }

   // apply XYZ stats
   //if ( ( Item_BONUS_DMGY & p_statbonus ) > 0)
   //   p_xyzstat [ Item_XYZ_DMGY ] += bonus;
   if ( ( Item_BONUS_DMGZ & bonusmalus() ) > 0)
      dmgz ( dmgz() + bonus );
   //if ( ( Item_BONUS_MDMGY & p_statbonus ) > 0)
   //   p_xyzstat [ Item_XYZ_MDMGY ] += bonus;
   if ( ( Item_BONUS_MDMGZ & bonusmalus() ) > 0)
      mdmgz ( mdmgz() + bonus );

   if ( bonus != 0)
   {
      sprintf (tmpstr, "%s %+d", p_name, bonus);
      strncpy ( p_name, tmpstr, Item_NAME_SIZE);
   }



    //--- Elemental Effect ---

//-------------- Default Element -------------------

   if ( type() == Item_TYPE_WEAPON )
            element( PROPERTY_PHYSICAL );

   if ( (powerupdown() & Item_POWERUP_DEFELEMENT ) > 0 )
   {
      if ( type() >= Item_TYPE_WEAPON && type() <= Item_TYPE_ACCESSORY )
      {
         int powerid;
         //int nb_try = 0;
         //bool valid = false;

         //do
         //{
         //   nb_try++;
            powerid = dice ( 4 ) + 1;

         //}
         //while ( ITEM_ELEMENT [ powerid] . active [ type() -1 ] == false && nb_try < 5 );

            sprintf ( tmpstr, "%s %s", ITEM_ELEMENT [ powerid ] . prefix [ type () -1], p_name);

            strncpy ( p_name, tmpstr, Item_NAME_SIZE );
            //final_price += price() * ITEM_ELEMENT [ powerid ] . costmulti;
            element (  ITEM_ELEMENT [ powerid] . flag);

         //else
         //   printf ("Debug: Item: Fail to find element\n");

      }
   }
   else

      // check for elemental effect first, then if does not work check for other effects.
      // maybe 25% elemental, if missing add 25% other effect.
      // elements does not work if cursed.


   if ( cursed() == false )
   {
      if ( dice (10) <= 5 && ( (powerupdown() & Item_POWERUP_ELEMENT ) > 0)
          && type() >= Item_TYPE_WEAPON && type() <= Item_TYPE_ACCESSORY )
      {
         int powerid;
         int nb_try = 0;
         //bool valid = false;

         do
         {
            nb_try++;
            powerid = dice ( Item_NB_ELEMENT ) - 1;

         }
         while ( ITEM_ELEMENT [ powerid] . active [ type() -1 ] == false && nb_try < 5 );

         if ( ITEM_ELEMENT [ powerid] . active [ type() -1] == true && rarity() >= ITEM_ELEMENT [ powerid ] . rarity )
         {
            sprintf ( tmpstr, "%s %s", ITEM_ELEMENT [ powerid ] . prefix [ type () -1], p_name);

            strncpy ( p_name, tmpstr, Item_NAME_SIZE );
            final_price += price() * ITEM_ELEMENT [ powerid ] . costmulti;
            element (  ITEM_ELEMENT [ powerid] . flag);
         }


      }
      else
      {
  //---------------- Power up -----------------------



         if ( dice (10) <= 5  && powerupdown() > 0)
         {
            int powerid;
            int nb_try = 0;

            do
            {
               nb_try++;
               powerid = dice ( Item_NB_POWERUP ) - 1;
            /*if ( type() == Item_TYPE_ARMOR)
               printf ("%s: %s=%d | powerid=%d, flag=%d\n"
                       , p_name, powerupdown_str(), powerupdown(), powerid
                       , ITEM_POWERUP [ powerid ] . flag );*/
            }
            while ( ( ITEM_POWERUP [ powerid ] . flag & powerupdown() ) != ITEM_POWERUP [ powerid ] . flag && nb_try < 5);

         /*if ( type() == Item_TYPE_ARMOR)
            printf ("----- selected -----");*/

            if ( rarity() >= ITEM_POWERUP [ powerid ] . rarity && nb_try >= 5 )
            {
               sprintf ( tmpstr, "%s %s", ITEM_POWERUP [ powerid ] . prefix, p_name);
               strncpy ( p_name, tmpstr, Item_NAME_SIZE );
               final_price += price() * ITEM_POWERUP [ powerid ] . costmulti;
//            powerup = true;

               switch ( powerid )
               {
                  case 0: // swift: --- attdiv ---
                     attdiv ( attdiv() - 1 );
                  break;
                  case 1 :
                     powdiv ( powdiv() - 1 );
                  break;
                  case 2:
                     mh ( mh() + 1);

               break;
               case 3:
                  final_price *= 2;
               break;
               case 4:
                  defense ( defense() & ~DEFENSE_ACTIVE_DEFENSE );
                  defense ( defense() & ~DEFENSE_HALF_ACTIVE_DEFENSE );
               break;
               case 5:
                  defense ( defense() & ~DEFENSE_DAMAGE_RESISTANCE );
                  defense ( defense() & ~DEFENSE_HALF_DAMAGE_RESISTANCE );
               break;
               case 6:
                  d20stat ( D20STAT_PS,  d20stat ( D20STAT_PS) + 4 );
               break;
               case 7:
                  d20stat ( D20STAT_MS,  d20stat ( D20STAT_MS) +4 );
               break;
               case 8:
                  if ( d20stat ( D20STAT_MELEECS) < -2)
                     d20stat ( D20STAT_MELEECS, d20stat ( D20STAT_MELEECS) / 2 );
                  else
                     d20stat ( D20STAT_MELEECS, 0 );
                  if ( d20stat ( D20STAT_RANGECS) < -2)
                     d20stat ( D20STAT_RANGECS, d20stat ( D20STAT_RANGECS) / 2 );
                  else
                     d20stat ( D20STAT_RANGECS, 0 );
               break;
               case 9:
                  d20stat ( D20STAT_MD, d20stat ( D20STAT_MD) + 4 );
               break;

               }

            }

         }
      }
   }
   else
   //-------------------- Powerdown --------------------------
   {

      if ( dice (10) <= 5 && element () == 0 && powerupdown() > 0)
      {
         int powerid;
         int nb_try = 0;

         do
         {
            nb_try++;
            powerid = dice ( Item_NB_POWERDOWN ) - 1;

         }
         while ( ( ITEM_POWERDOWN [ powerid ] . flag & powerupdown() ) !=  ITEM_POWERDOWN [ powerid ] . flag
                && nb_try < 5);


         if ( rarity() >= ITEM_POWERDOWN [ powerid ] . rarity && nb_try >= 5 )
         {
            sprintf ( tmpstr, "%s %s", ITEM_POWERDOWN [ powerid ] . prefix, p_name);
            strncpy ( p_name, tmpstr, Item_NAME_SIZE );
            final_price += price() * ITEM_POWERDOWN [ powerid ] . costmulti;
  //          powerup = true;

            switch ( powerid )
            {// ??? probably use a linear value because constants are bitfield
               case 0: // swift: --- attdiv ---
                  attdiv ( attdiv() + 2 );
               break;
               case 1 :
                  powdiv ( powdiv() + 2 );
               break;
               case 2:
                  mh ( mh() -1);
                  if ( mh() < 0)
                     mh ( 0 );
               break;
               case 3:
                  final_price /= 10;
               break;
               case 4:
                  d20stat ( D20STAT_PS, d20stat ( D20STAT_PS) - 8 );
               break;
               case 5:
                  d20stat ( D20STAT_MS, d20stat ( D20STAT_MS) - 8 );
               break;
               case 6:

                     d20stat ( D20STAT_MELEECS, d20stat ( D20STAT_MELEECS) * 2 );

                     d20stat ( D20STAT_MELEECS, d20stat ( D20STAT_RANGECS) * 2 );

               break;
               case 7:
                  d20stat ( D20STAT_MD, d20stat ( D20STAT_MD) - 8 );
               break;
            }

         }

      }
   }



   //-------------- ending item creation -------------

   price (final_price);

}


/*-------------------------------------------------------------------------*/
/*-                         Private Methods                               -*/
/*-------------------------------------------------------------------------*/

/*bool Item::identified ( unsigned int type )
{
   if ( ( p_identificati )
      return ( true );
   else
      return ( false );
}*/


/*-------------------------------------------------------------------------*/
/*-                        Virtual Methods                                -*/
/*-------------------------------------------------------------------------*/

int Item::type ( void )
{
   return ( get_int (3) );
}

void Item::template_load_completion ( void )
{
   char tmpstr [ Item_NAME_SIZE];

   location (Item_LOCATION_UNKNOWN, -1);

   strcpy ( tmpstr, p_name);
   strncat ( tmpstr, " ?", 2 );
   strcpy ( p_fakename, tmpstr );


}

void Item::callback_handler ( int index )
{
   switch  ( index )
   {
      case Item_CB_LOKTOFEIT_DESTROY_ITEM:
         cb_loktofeit_destroy_item();
      break;
   }
}

void Item::cb_loktofeit_destroy_item (void)
{
   if ( dice(10) <= 3 ) //30% chance to destroy and item
   {
      SQLdelete();
   }
}

/*void Item::sql_to_obj (void)
{
   int i;
   int j;

   for ( i = 0; i < D20_NB_STAT; i++)
      //for ( j = 0; j < D20_NB_MODIFIER; j++)
         p_d20stat [ i ]  = 0;

   for ( i = 0; i < Item_XYZ_NB_STAT; i++)
      //for ( j = 0; j < XYZ_NB_MODIFIER; j++)
         p_xyzstat [ i ]  = 0;

   p_primary_key = SQLcolumn_int (0);
   strncpy ( p_name, SQLcolumn_text(1), Item_NAME_SIZE);
   strncpy ( p_fakename, SQLcolumn_text(2), Item_NAME_SIZE);
   p_price = SQLcolumn_int(3);
   p_type = SQLcolumn_int(4);
   p_category = SQLcolumn_int(5);
   p_clumsy = SQLcolumn_int(6);
   p_attribute = SQLcolumn_int(7);
   p_hand = SQLcolumn_int(8);
   p_range = SQLcolumn_int(9);
   p_special = SQLcolumn_int(10);
   p_property = SQLcolumn_int(11);
   p_location_key = SQLcolumn_int(12);
   p_location_type = SQLcolumn_int(13);
   p_charges_current = SQLcolumn_int(14);
   p_charges_max = SQLcolumn_int(15);
   p_key = SQLcolumn_int(16);
   p_cursed = SQLcolumn_int(17);
   p_rarity = SQLcolumn_int(18);
   p_identified = SQLcolumn_int(19);
   p_d20stat[D20STAT_AD] = SQLcolumn_int(20);
   p_d20stat[D20STAT_MD] = SQLcolumn_int(21);
   p_d20stat[D20STAT_PS] = SQLcolumn_int(22);
   p_d20stat[D20STAT_MS] = SQLcolumn_int(23);
   p_d20stat[D20STAT_MELEECS] = SQLcolumn_int(24);
   p_d20stat[D20STAT_RANGECS] = SQLcolumn_int(25);
   p_d20stat[D20STAT_DR] = SQLcolumn_int(26);
   p_d20stat[D20STAT_INIT] = SQLcolumn_int(27);
   p_xyzstat[Item_XYZ_DMGY] = SQLcolumn_int(28);
   p_xyzstat[Item_XYZ_DMGZ] = SQLcolumn_int(29);
   p_xyzstat[Item_XYZ_MDMGY] = SQLcolumn_int(30);
   p_xyzstat[Item_XYZ_MDMGZ] = SQLcolumn_int(31);
   p_statbonus = SQLcolumn_int(32);




}

void Item::template_sql_to_obj (void)
{
   int i;
   int j;
   char tmpstr [ Item_NAME_SIZE];

   for ( i = 0; i < D20_NB_STAT; i++)
      //for ( j = 0; j < D20_NB_MODIFIER; j++)
         p_d20stat [ i ]  = 0;

   for ( i = 0; i < Item_XYZ_NB_STAT; i++)
      //for ( j = 0; j < XYZ_NB_MODIFIER; j++)
         p_xyzstat [ i ]  = 0;


   p_primary_key = -1;
   strncpy ( p_name, SQLcolumn_text(1), Item_NAME_SIZE);
   p_price = SQLcolumn_int(2);
   p_type = SQLcolumn_int(3);
   p_category = SQLcolumn_int(4);
   p_d20stat[D20STAT_AD] = SQLcolumn_int(5);
   p_d20stat[D20STAT_MD] = SQLcolumn_int(6);
   p_d20stat[D20STAT_INIT] = SQLcolumn_int(7);
   p_d20stat[D20STAT_MELEECS] = SQLcolumn_int(8);
   p_d20stat[D20STAT_RANGECS] = SQLcolumn_int(8);
   p_attribute = SQLcolumn_int(9);
   p_hand = SQLcolumn_int(10);
   p_range = SQLcolumn_int(11);
   p_clumsy = SQLcolumn_int(12);
   p_special = SQLcolumn_int(13);
   p_d20stat[D20STAT_DR] = SQLcolumn_int(14);
   p_xyzstat[Item_XYZ_DMGY] = SQLcolumn_int(15);
   p_charges_current = SQLcolumn_int(16);
   p_charges_max = SQLcolumn_int(16);
   p_statbonus = SQLcolumn_int(17);

   p_property = 0;
   p_location_key = -1;
   p_location_type = Item_LOCATION_UNKNOWN;
   p_key = -1;
   p_cursed = false;
   p_rarity = 0;
   p_identified = false;
   strcpy ( tmpstr, p_name);
   strncat ( tmpstr, " ?", 2 );
   strcpy ( p_fakename, tmpstr );

}


void Item::obj_to_sqlupdate (void)
{

   sprintf ( p_querystr, "name='%s', fakename='%s', price=%d, type=%d, category=%d, clumsy=%d, attribute=%d, hand=%d, range=%d, special=%d, property=%d, lockey=%d, loctype=%d, charge=%d, max_charge=%d, key=%d, cursed=%d, rarity=%d, identified=%d, ad=%d, md=%d, ps=%d, ms=%d, pcs=%d, mcs=%d, dr=%d, init=%d, dmgy=%d, dmgz=%d, mdmgy=%d, mdmgz=%d, stat_bonus=%d",
      p_name,
      p_fakename,
      p_price,
      p_type,
      p_category,
      p_clumsy,
      p_attribute,
      p_hand,
      p_range,
      p_special,
      p_property,
      p_location_key,
      p_location_type,
      p_charges_current,
      p_charges_max,
      p_key,
      p_cursed,
      p_rarity,
      p_identified,
      p_d20stat[D20STAT_AD],
      p_d20stat[D20STAT_MD],
      p_d20stat[D20STAT_PS],
      p_d20stat[D20STAT_MS],
      p_d20stat[D20STAT_MELEECS],
      p_d20stat[D20STAT_RANGECS],
      p_d20stat[D20STAT_DR],
      p_d20stat[D20STAT_INIT],
      p_xyzstat[Item_XYZ_DMGY],
      p_xyzstat[Item_XYZ_DMGZ],
      p_xyzstat[Item_XYZ_MDMGY],
      p_xyzstat[Item_XYZ_MDMGZ],
      p_statbonus );
*/

   /*
   	edit	delete	0	pk	INTEGER	yes		yes
	edit	delete	1	name	TEXT	no		no
	edit	delete	2	fakename	TEXT	no		no
	edit	delete	3	price	INTEGER	no	0	no
	edit	delete	4	type	INTEGER	no	0	no
	edit	delete	5	category	INTEGER	no	0	no
	edit	delete	6	clumsy	INTEGER	no	0	no
	edit	delete	7	attribute	INTEGER	no	0	no
	edit	delete	8	hand	INTEGER	no	0	no
	edit	delete	9	range	INTEGER	no	0	no
	edit	delete	10	special	INTEGER	no	0	no
	edit	delete	11	property	INTEGER	no	0	no
	edit	delete	12	lockey	INTEGER	no	0	no
	edit	delete	13	loctype	INTEGER	no	0	no
	edit	delete	14	charge	INTEGER	no	0	no
	edit	delete	15	max_charge	INTEGER	no	0	no
	edit	delete	16	key	INTEGER	no	0	no
	edit	delete	17	cursed	INTEGER	no	0	no
	edit	delete	18	rarity	INTEGER	no	0	no
	edit	delete	19	identified	INTEGER	no	0	no
	edit	delete	20	ad	INTEGER	no	0	no
	edit	delete	21	md	INTEGER	no	0	no
	edit	delete	22	ps	INTEGER	no	0	no
	edit	delete	23	ms	INTEGER	no	0	no
	edit	delete	24	pcs	INTEGER	no	0	no
	edit	delete	25	mcs	INTEGER	no	0	no
	edit	delete	26	dr	INTEGER	no	0	no
	edit	delete	27	init	INTEGER	no	0	no
	edit	delete	28	dmgy	INTEGER	no	0	no
	edit	delete	29	dmgz	INTEGER	no	0	no
	edit	delete	30	mdmgy	INTEGER	no	0	no
	edit	delete	31	mdmgz	INTEGER	no	0	no*/

//}
/*
void Item::obj_to_sqlinsert (void)
{
   sprintf ( p_querystr, "'%s', '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
      p_name,
      p_fakename,
      p_price,
      p_type,
      p_category,
      p_clumsy,
      p_attribute,
      p_hand,
      p_range,
      p_special,
      p_property,
      p_location_key,
      p_location_type,
      p_charges_current,
      p_charges_max,
      p_key,
      p_cursed,
      p_rarity,
      p_identified,
      p_d20stat[D20STAT_AD],
      p_d20stat[D20STAT_MD],
      p_d20stat[D20STAT_PS],
      p_d20stat[D20STAT_MS],
      p_d20stat[D20STAT_MELEECS],
      p_d20stat[D20STAT_RANGECS],
      p_d20stat[D20STAT_DR],
      p_d20stat[D20STAT_INIT],
      p_xyzstat[Item_XYZ_DMGY],
      p_xyzstat[Item_XYZ_DMGZ],
      p_xyzstat[Item_XYZ_MDMGY],
      p_xyzstat[Item_XYZ_MDMGZ],
      p_statbonus );

}*/

/*
void Item::objdat_to_strdat ( void* dataptr )
{
   dbs_Item &tmpdat = *(static_cast<dbs_Item*> ( dataptr ));

   strncpy ( tmpdat.name, p_name, Item_NAME_SIZE + 1);
   strncpy ( tmpdat.fname, p_fname, Item_NAME_SIZE + 1);
   tmpdat.type = p_type;
   tmpdat.location = p_location;
   tmpdat.ability.type = p_ability.type;
   tmpdat.ability.value = p_ability.value;
   tmpdat.ability.max_charge = p_ability.max_charge;
   tmpdat.ability.exhausted = p_ability.exhausted;
   tmpdat.ability.drawback = p_ability.drawback;
   tmpdat.ability.nb_charge = p_ability.nb_charge;
   tmpdat.autoability = p_autoability;
   tmpdat.hlteffect = p_hlteffect;
   tmpdat.elmeffect = p_elmeffect;
   tmpdat.magikproperty = p_magikproperty;
   tmpdat.key = p_key;
   tmpdat.identification = p_identification;
   tmpdat.weight = p_weight;
   tmpdat.status = p_status;
   tmpdat.rarity = p_rarity;
   tmpdat.price = p_price;

}

void Item::strdat_to_objdat ( void *dataptr )
{
   dbs_Item &tmpdat = *(static_cast<dbs_Item*> ( dataptr ));

   strcpy ( p_name, tmpdat.name );
   strcpy ( p_fname, tmpdat.fname );
   p_type = tmpdat.type;
   p_location = tmpdat.location;
   p_ability.type = tmpdat.ability.type;
   p_ability.value = tmpdat.ability.value;
   p_ability.max_charge = tmpdat.ability.max_charge;
   p_ability.exhausted = tmpdat.ability.exhausted;
   p_ability.drawback = tmpdat.ability.drawback;
   p_ability.nb_charge = tmpdat.ability.nb_charge;
   p_autoability = tmpdat.autoability;
   p_hlteffect = tmpdat.hlteffect;
   p_elmeffect = tmpdat.elmeffect;
   p_magikproperty = tmpdat.magikproperty;
   p_key = tmpdat.key;
   p_identification = tmpdat.identification;
   p_weight = tmpdat.weight;
   p_status = tmpdat.status;
   p_rarity = tmpdat.rarity;
   p_price = tmpdat.price;

}

void Item::child_DBremove ( void )
{
   // no child to remove
}
*/

/*-----------------------------------------------------------------------------*/
/*-                          Global Variables                                 -*/
/*-----------------------------------------------------------------------------*/

/*const char STR_ITM_ABILITY [][21] =
{
   { "None" }, //#define Item_ABILITY_NONE              0
   { "Cast " }, //#define Item_ABILITY_SPELL             1
   { "Max HP +" }, //#define Item_ABILITY_RAISE_MAXHP       2
   { "Max MP +" }, //#define Item_ABILITY_RAISE_MAXSP       3
   { "Exp +" }, //#define Item_ABILITY_GAIN_EXP          4
   { "Age -" }, //#define Item_ABILITY_LOWER_AGE         5
   { "Soul +" }, //#define Item_ABILITY_RAISE_SOUL        6
   { "HP +" }, //#define Item_ABILITY_RECOVER_HP        7
   { "SP +" }, //#define Item_ABILITY_RECOVER_SP        8
   { "Raise " }, //#define Item_ABILITY_RAISE_ATTRIBUTE   9
   { "Cure " }, //#define Item_ABILITY_CURE_HEALTH       11
   { "Boost " }, //#define Item_ABILITY_RAISE_STAT        12
};

const char STR_ITM_AUTOABILITY [][21] =
{
   { "None" },//#define Item_AUTOABILITY_NONE             0
   { "HP+" },//#define Item_AUTOABILITY_REGEN_HP         1
   { "HP+3" },//#define Item_AUTOABILITY_REGEN_MORE_HP    2
   { "HP-" },//#define Item_AUTOABILITY_DRAIN_HP         3
   { "HP-3" },//#define Item_AUTOABILITY_DRAIN_MORE_HP    4
   { "MP+" },//#define Item_AUTOABILITY_REGEN_SP         5
   { "MP+3" },//#define Item_AUTOABILITY_REGEN_MORE_SP    6
   { "MP-" },//#define Item_AUTOABILITY_DRAIN_SP         7
   { "MP-3" },//#define Item_AUTOABILITY_DRAIN_MORE_SP    8
   { "Inflict Health" },//#define Item_AUTOABILITY_RANDOM_HEALTH    9
   { "Cure Health" },//#define Item_AUTOABILITY_CURE_HEALTH      10
   { "Exp+" },//#define Item_AUTOABILITY_REGEN_EXP        11
   { "Exp-" },//#define Item_AUTOABILITY_DRAIN_EXP        12
   { "Soul+" },//#define Item_AUTOABILITY_REGEN_SOUL       13
   { "Soul-" },//#define Item_AUTOABILITY_DRAIN_SOUL       14
   { "Age+" },//#define Item_AUTOABILITY_RAISE_AGE        15
};

const char STR_ITM_EXHAUST [][21] =
{
   { "Break" },//#define Item_EXHAUST_BREAK              0
   { "Nothing" },//#define Item_EXHAUST_NOTHING            1
   { "Rechargable" },//#define Item_EXHAUST_RECHARGE           2
   { "Fall Dead" },//#define Item_EXHAUST_DEAD               3
   { "Turn to Ashes" },//#define Item_EXHAUST_ASHES              4
   { "Get Deleted" },//#define Item_EXHAUST_DELETED            5
   { "Raise Age" },//#define Item_EXHAUST_RAISE_AGE          6
   { "Lower MaxHP" },//#define Item_EXHAUST_LOWER_MAXHP        7
   { "Lower MaxMP" },//#define Item_EXHAUST_LOWER_MAXSP        8
   { "Lose Exp" },//#define Item_EXHAUST_LOSE_EXP           9
   { "Lose STRENGTH" },//#define Item_EXHAUST_LOWER_STRENGTH     10
   { "Lose DEXTERITY" },//#define Item_EXHAUST_LOWER_DEXTERITY    11
   { "Lose ENDURANCE" },//#define Item_EXHAUST_LOWER_ENDURANCE    12
   { "Lose INTELLIGE" },//#define Item_EXHAUST_LOWER_INTELLIGENCE 13
   { "Lose CUNNING" },//#define Item_EXHAUST_LOWER_CUNNING      14
   { "Lose WILLPOWER" },//#define Item_EXHAUST_LOWER_WILLPOWER    15
   { "Lose LUCK" },//#define Item_EXHAUST_LOWER_LUCK         16
   { "Lose soul" },//#define Item_EXHAUST_LOSE_SOUL          17
   { "Inflict Health" },//#define Item_EXHAUST_RANDOM_HEALTH      18
};

const char STR_ITM_HLTEFFECT [][21] =
{ /// constant name is Item HLTEFFECT_???
   { "Sleep  " },// Item_EFFECT_HEALTH_SLEEP          1<<6
   { "Stun   " },// Item_EFFECT_HEALTH_PARALYZATION   1<<7
   { "Stone  " },// Item_EFFECT_HEALTH_PETRIFICATION  1<<8
   { "???    " },// Item_EFFECT_HEALTH_DISEASE        1<<9
   { "Poison " },// Item_EFFECT_HEALTH_POISON         1<<10
   { "Blind  " },// Item_EFFECT_HEALTH_BLINDNESS      1<<11
   { "Cripple" },// Item_EFFECT_HEALTH_CRIPPLE        1<<12
   { "???    " },// Item_EFFECT_HEALTH_RESERVE3       1<<13
   { "Fear   " },// Item_EFFECT_HEALTH_FEAR           1<<14
   { "Curse  " },// Item_EFFECT_HEALTH_CURSE          1<<15
   { "Seal   " },// Item_EFFECT_HEALTH_SEAL           1<<16
   { "Doom   " },// Item_EFFECT_HEALTH_DOOM           1<<17
   { "Fade   " },// Item_EFFECT_HEALTH_FADE           1<<18
   { "???    " },// Item_EFFECT_HEALTH_RESERVE5       1<<19
   { "???    " },// Item_EFFECT_HEALTH_RESERVE6       1<<20
   { "???    " },// Item_EFFECT_HEALTH_RESERVE7       1<<21
};

const char STR_ITM_ELMEFFECT [][21] =
{
   { "Life   " },
   { "Water  " },
   { "Air    " },
   { "Death  " },
   { "Fire   " },
   { "Earth  " },
   { "Dragon " },
   { "Undead " },
   { "Mythic." },
   { "Divine " },
   { "Dire   " },
   { "Magic  " },
   { "Were   " },
   { "?      " },
   { "?      " },
   { "?      " }
};

const char STR_ITM_DRAWBACK_INFO [][21] =
{
   { "Nothing" },
   { "Raise Age" },//#define Item_EXHAUST_RAISE_AGE          6
   { "Lower MaxHP" },//#define Item_EXHAUST_LOWER_MAXHP        7
   { "Lower MaxMP" },//#define Item_EXHAUST_LOWER_MAXSP        8
   { "Lose Exp" },//#define Item_EXHAUST_LOSE_EXP           9
   { "Lose STRength" },//#define Item_EXHAUST_LOWER_STRENGTH     10
   { "Lose DEXterity" },//#define Item_EXHAUST_LOWER_DEXTERITY    11
   { "Lose ENDurance" },//#define Item_EXHAUST_LOWER_ENDURANCE    12
   { "Lose INTelligence" },//#define Item_EXHAUST_LOWER_INTELLIGENCE 13
   { "Lose CUNning" },//#define Item_EXHAUST_LOWER_CUNNING      14
   { "Lose WILlpower" },//#define Item_EXHAUST_LOWER_WILLPOWER    15
   { "Lose LuCK" },//#define Item_EXHAUST_LOWER_LUCK         16
   { "Lose soul" },//#define Item_EXHAUST_LOSE_SOUL          17
   { "Inflict Health" },//#define Item_EXHAUST_RANDOM_HEALTH      18
};
*/
  // to redefine
/*const char STR_ITM_MAGIKPROPERTY [][21] =
{
   {"Element kill"},
   {"Instant death"},
   {"Cremate"},
   {"Delete"},
   {"Drain Soul"},
   {"Drain Life"},
   {"     "},
   {"     "},
   {"     "},
   {"Weapon Immunity"},
   {"Magic Immunity"},
   {"     "},
   {"     "},
   {"     "},
   {"     "},
   {"     "},
};*/


/*const char STR_ITM_LOCATION [][11] =
{
  {"Weapon"},
  {"Shield"},
  {"Armor"},
  {"Head"},
  {"Hand"},
  {"Feet"},
  {"Other"},
  {"-----"},
};*/




// globals from child class.

/*const char STR_WPN_CATEGORY [][12] =
{
   {"Melee"},
   {"Sword"},
   {"Bow"},
   {"Mechanical"},
   {"Unbalanced"},
   {"Thrown"},
   {"Magical"},
};

const char STR_WPN_RANGE [][7] =
{
   { "Melee" },
   { "Short" },
   { "Long" },
};

const char STR_WPN_ATTRIBUTE [][4] =
{
   { "STR" },
   { "INT" },
   { "DEX" },
};

const char STR_WPN_DMGTYPE [][16] =
{
   { "Normal" },
   { "Armor Piercing" },
   { "Splash" },
   { "Drain" },
   { "Undefendable" },
   { "Auto Hit" },
};*/

/*extern const char STR_ARM_CATEGORY [] [9] =
{
   {"Minimal"},
   {"Light"},
   {"Heavy"},
};

extern const char STR_SHL_CATEGORY [][9] =
{
   {"Minimal"},
   {"Light"},
   {"Heavy"},
};*/

/*const char STR_ACC_STAT [] [21] =
{
   {"---"},
   {"Passive Defense"},
   {"Active Defense"},
   {"Magik Passive Def."},
   {"Magik Active Def."},
   {"Damage Resistance"},
   {"Magik Damage Resist"},
   {"Physical Saves"},
   {"Magik Saves"},
   {"Hit roll"},
   {"Initiative"},
   {"Physical Damage"},
   {"Magikal Damage"},
};*/
/*
const char *STR_ITM_STATUS1 [] [11] =
{
   {"Normal "},
   {"Fine "},
   {"Enchanted "},
   {"Artifact "},
} */

/*const char STR_ACC_LOCATION [] [6] =
{
   {"Head"},
   {"Feet"},
   {"Hand"},
   {"Other"}
};*/


const char STR_ITM_RARITY [][11] =
{
   {"Common"}, // 0
   {"Fine"}, // 1
   {"Fine"}, //
   {"Fine"}, //
   {"Fine"}, //
   {"Precious"}, // 5
   {"Precious"}, //
   {"Precious"}, //
   {"Precious"}, //
   {"Exquisite"}, //
   {"Exquisite"}, // 10
   {"Exquisite"}, //
   {"Exquisite"}, //
   {"Rare"}, //
   {"Rare"}, //
   {"Rare"}, // 15
   {"Rare"}, //
   {"Legendary"}, //
   {"Legendary"}, //
   {"Legendary"}, //
   {"Legendary"}, // 20
};

const char STR_ITM_TYPE [] [11] =
{
   {"Unknown"},
   {"Weapon"},
   {"Shield"},
   {"Armor"},
   {"Accessory"},
   {"Expandable"},
   {"Item"},
};

extern const char STR_ITM_TYPE_CODE [] [4] =
{
    {"???"},
    {"WPN"},
    {"SHD"},
    {"ARM"},
    {"ACC"},
    {"EXP"},
    {"ITM"},
};

const char STR_ITM_RANGE [][8] =
{
   {"No"},
   {"Melee"},
   {"Short"},
   {"Long"},
};

const char STR_ITM_ATTRIBUTE [][4] =
{
   {"---"},
   {"DEX"},
   {"CUN"},
};

/*const char STR_ITM_CAT_WEAPON [][8]=
{
   {"Sword"},
   {"Mace"},
   {"Spear"},
   {"Bow"},
   {"Riffle"},
   {"Wand"},
   {"Chain"},
   {"Daggers"},
};

const char STR_ITM_CAT_SHIELD [][8] =
{
   {"Minimal"},
   {"Light"},
   {"Heavy"},
};

const char STR_ITM_CAT_ARMOR [][8] =
{
   {"Minimal"},
   {"Light"},
   {"Medium"},
   {"Heavy"},
};*/

/*extern const char STR_ITM_CATEGORY [][15] =
{
   {"Sword"},
   {"Mace"},
   {"Spear"},
   {"Bow"},
   {"Riffle"},
   {"Wand"},
   {"Chain"},
   {"Daggers"},
   //--------------
   {"Min.Shield"},
   {"Lgt.Shield"},
   {"Hvy.Shield"},
   {"Unknown"},
   {"Min.Armor"},
   {"Lgt.Armor"},
   {"Med.Armor"},
   {"Hvy.Armor"},
   //--------------
   {"Pendant"},
   {"Ring"},
   {"Boots"},
   {"Bracelet"},
   {"Unknown"},
   {"Unknown"},
   {"Unknown"},
   {"Unknown"},
   //--------------
   {"Potion"},
   {"Scroll"},
   {"Dart"},
   {"Flask"},
   {"Misile"},
   {"Bomb"},
   {"Unknown"},
   {"Unknown"}
};*/

s_EnhancedSQLobject_strfield STRFLD_BONUS [ EnhancedSQLobject_HASH_SIZE ] =

{
   { "Adf", "Active Defense" },
   { "Mdf", "Magic Defense" },
   { "Psv", "Physical Saves" },
   { "Msv", "Mental Saves" },
   { "Mcs", "Melee Combat Skill" },
   { "Rcs", "Range Combat Skill" },
   { "Drs", "Damage Resistance" },
   { "Ini", "Initiative" },

   { "Dmz", "Damage Z" },
   { "Mdz", "Magic Damage Z" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },

   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },

   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" }

};

s_EnhancedSQLobject_strfield STRFLD_POWERUP [ EnhancedSQLobject_HASH_SIZE ] =

{
   { "Adv", "Attack Divider" },
   { "Pdv", "Power Divider" },
   { "Mth", "Multi Hit Penalty" },
   { "Ele", "Element" },
   { "Pri", "Price" },
   { "Nad", "No Active Defense" },
   { "Ndr", "No Damage Resistance" },
   { "Dmy", "Damage Y" },

   { "Psv", "Increase Phys. Saves" },
   { "Msv", "Increase Mental Saves" },
   { "Csp", "Remove Combat Skill Penalty" },
   { "Mdf", "Add Magic Defense" },
   { "Del", "Default Element" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },

   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },

   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" }

};



s_powerupdown ITEM_POWERUP [ Item_NB_POWERUP ] =
{
   {"Swift", Item_POWERUP_ATTDIV, 10, 35},
   {"Almighty", Item_POWERUP_POWDIV, 10, 50},
   {"Light", Item_POWERUP_MH, 5,  5},
   {"Jewelered", Item_POWERUP_PRICE, 5, 10},
   {"Striking", Item_POWERUP_NOAD, 10, 50},
   {"Piercing", Item_POWERUP_NODR, 5, 25},
   {"Vitality", Item_POWERUP_PSAVE, 5, 5 },
   {"Essence", Item_POWERUP_MSAVE, 5, 5 },
   {"Light", Item_POWERUP_CSPENALTY, 5, 10 },
   {"Dispersion", Item_POWERUP_MD, 5, 25 }

};

s_powerupdown ITEM_POWERDOWN [ Item_NB_POWERDOWN ] =
{
   {"Clumsy", Item_POWERUP_ATTDIV, 10, 3},
   {"Powerless", Item_POWERUP_POWDIV, 10, 5},
   {"Heavy", Item_POWERUP_MH , 5 , 2}, // even if keyword prefex repeated, they are mutually exclusive (weapon or armor)
   {"Corroded", Item_POWERUP_PRICE, 5, 1},
   {"Weakness", Item_POWERUP_PSAVE, 5, 2 },
   {"Vulnerability", Item_POWERUP_MSAVE, 5, 2 },
   {"Heavy", Item_POWERUP_CSPENALTY, 5, 3 },
   {"Absorption", Item_POWERUP_MD, 5, 5 }
};


s_Item_element ITEM_ELEMENT [ Item_NB_ELEMENT ] =
{
   { {"---", "---", "Hardened", "Iron"}, PROPERTY_PHYSICAL, 10, 25, {false,  false, true, true} },
   { {"Etheral", "---","Spirit", "Glass"}, PROPERTY_MENTAL, 5, 10, {true,  false, true, true} },
   { {"Fire", "Copper","Dew", "Copper"}, PROPERTY_FIRE, 5, 3, {true, true, true, true} },
   { {"Ice", "Silver","Warm", "Silver"}, PROPERTY_ICE, 5, 3, {true, true, true, true} },
   { {"Lightning", "Wooden","Grounded", "Brass"}, PROPERTY_LIGHTNING, 5, 3, { true, true, true, true} },
   { {"Poison", "Glass","Gloss", "Bronze"}, PROPERTY_POISON, 5, 3, { true, true, true, true} },
   { {"---", "---","", ""}, PROPERTY_RESERVED1, 5, 3, { false, false, false, false} },
   { {"---", "---","", ""}, PROPERTY_RESERVED2, 5, 3, { false, false, false, false} },

   { {"---", "","Steady", "Stone"}, PROPERTY_BODY, 5, 5, {false,  false, true, true} },
   { {"---", "","Hardskin", "Antidode"}, PROPERTY_BLOOD, 5, 13, {false, false, true, true} },
   { {"---", "","Blessed", "Blessed"}, PROPERTY_SOUL, 10, 34, {false, false, true, true} },
   { {"---", "","Living", "Organic"}, PROPERTY_NEURAL, 5, 8, {false, false, true, true} },
   { {"---", "","Psionic", "Psionic"}, PROPERTY_PSYCHIC, 10, 21, {false, false, true,true} },
   { {"---", "","Treated", "Shell"}, PROPERTY_SKIN, 5, 3, {false, false, true, true} },
   { {"---", "","---", ""}, PROPERTY_RESERVED2, 5, 3, {false, false, false, false} },
   { {"---", "","---", ""}, PROPERTY_RESERVED2, 5, 3, {false, false, false, false} },

   { {"Slaying", "---","---", "---"}, PROPERTY_DRAGON, 5, 6, {true, false, false, false} },
   { {"Holy", "---","---", "---"}, PROPERTY_UNDEAD, 5, 10, {true, false, false, false} },
   { {"Heroic", "---","---", "---"}, PROPERTY_BEAST, 5, 6, {true, false, false, false} },
   { {"Banishing", "---","---", "---"}, PROPERTY_OUTSIDER, 5, 6, {true, false, false, false} },
   { {"Hunting", "---","---", "---"}, PROPERTY_ANIMAL, 5, 10, {true, false, false, false} },
   { {"Champion''s", "---","---", "---"}, PROPERTY_GIANT, 5, 6, {true, false, false, false} },
   { {"Silver", "---","---", "---"}, PROPERTY_SHAPECHANGER, 5, 3, {true, false, false, false} },
   { {"Exterminator''s", "---","---", "---"}, PROPERTY_VERMIN, 5, 6, {true, false, false, false} },

   { {"Brute Finishing", "---","---", "---"}, PROPERTY_WARRIOR, 10, 6, {true, false, false, false} }, //cost depends on qty of monsters
   { {"Thug Butchering", "---","---", "---"}, PROPERTY_ROGUE, 10, 6, {true, false, false, false} }, //depends on qty of monsters
   { {"Mage Mashing", "---","---", "---"}, PROPERTY_MAGICUSER, 10, 6, {true, false, false, false} }, //depends on qty of monsters
   { {"Assassin''s", "---","---", "---"}, PROPERTY_HUMANOID, 10, 15, {true, false, false, false} },
   { {"Weed Eating", "---","---", "---"}, PROPERTY_PLANT, 5, 3, {false, false, false, false} },
   { {"Sponge Steel", "---","---", "---"}, PROPERTY_OOZE, 5, 3, {false, false, false, false} },
   { {"Runic", "---","---", "---"}, PROPERTY_MAGICKAL, 5, 6, {false, false, false, false} }

};

/*typedef struct s_Item_element
{
   bool active; // indicate the effect is active
   char prefix_offense [16]; // used for weapons
   char prefix_defense [16]; // used for armors
   int flag; //element flag to raise
   int rarity; // min rarity required
   int costmulti; // multiply the cost by that value
   bool offense; // apply to offensive Items
   bool defense; // apply to defensive Items
}s_Item_element;*/

/*

//Creature Type
#define PROPERTY_DRAGON     1<<16  // 00000000000000010000000000000000 = 65536
#define PROPERTY_UNDEAD     1<<17  // 00000000000000100000000000000000 = 131072
#define PROPERTY_MYTHICAL   1<<18  // 00000000000001000000000000000000 = 262144
#define PROPERTY_DIVINE     1<<19  // 00000000000010000000000000000000 = 524288
#define PROPERTY_DIRE       1<<20  // 00000000000100000000000000000000 = 1048576
#define PROPERTY_MAGIC      1<<21  // 00000000001000000000000000000000 = 2097152
#define PROPERTY_WERE       1<<22  // 00000000010000000000000000000000 = 4194304
#define PROPERTY_RESERVED5  1<<23  // 00000000100000000000000000000000 = 8388608

//--- ideas ---

Animal
Insect
Reptile
Undead
Beast
Divine
Dragon & maybe Beast
Humanoid
were, add shapechanger
Outsider (Divine)
Magical Beast, beast
construct
Fey
Giant
Ooze
Plant


//idea giant++, goblinoid, animal, insects
//more were if restore were tiger and were bears
//dire is useless., magic has few for now. same thing for mythical


*/


