/***************************************************************************/
/*                                                                         */
/*                            R A C E . C P P                              */
/*                           Class Source Code                             */
/*                                                                         */
/*     Content : Class Race                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : February 7th, 2004                                  */
/*     License : GNU General Public License                                */
/*                                                                         */
/***************************************************************************/

// include group
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>





//#include <allegro.h>
//#include <allegrowrapper.h>
//#include <random.h>

//#include <datafile.h>
//#include <datmacro.h>
//#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
//#include <ddt.h>
//#include <dbdef.h>
//
//
//
//
//
//
//#include <listwiz.h>


//#include <opponent.h>
//#include <charactr.h>
//#include <ennemy.h>
//
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
//#include <window.h>
//#include <winmenu.h>
//#include <winempty.h>
//#include <wintitle.h>
//#include <windata.h>
//#include <wdatproc.h>
//#include <winmessa.h>
//#include <winquest.h>
//#include <wininput.h>

/*------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                        -*/
/*------------------------------------------------------------------------*/

Race::Race ( void )
{
   int i;

   strcpy ( p_name, "" );
   //strcpy ( p_plural, "" );
   //p_aligment_restriction = 0;
   //p_lifespan = 0;
   p_resistance = 0;
   p_weakness = 0;
   //p_size = 0;
   //p_HPdice = 8;
   //p_MPdice = 8;
   //p_DMGy = 8;
   //p_MDMGy = 8;

   for ( i = 0 ; i < 6 ; i++ )
      p_attribute [ i ] = 0;

   p_pictureID = 0;

   //for ( i = 0 ; i < 3 ; i++ )
   //   p_ability [ i ] = Race_ABILITY_NONE;

   //p_dblength = sizeof ( dbs_Race );
   //p_dbtableID = DBTABLE_RACE;

   strcpy (p_tablename, "race");
}

Race::~Race ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                           Property Method                             -*/
/*-------------------------------------------------------------------------*/

/*string& Race::name ( void )
{
   return ( p_name );
} */

const char* Race::name ( void )
{
   return ( p_name );
}

void Race::name (const char* str)
{
   strncpy ( p_name, str, Race_NAME_STR_SIZE);
}

const char* Race::initial ( void )
{
   return (p_initial);
}

void Race::initial (const char* str)
{
   strncpy (p_initial, str, Race_INITIAL_STR_SIZE);
}


/*string& Race::plural ( void )
{
   return ( p_plural );
} */

/*const char* Race::plural ( void )
{
   return ( p_plural );
}*/

int Race::attribute ( int attributeID )
{
   return ( p_attribute [ attributeID ] );
}

void Race::attribute ( int attributeID, int value )
{
   p_attribute [ attributeID ] = value;
}

/*short Race::lifespan ( void )
{
   return ( p_lifespan );
}*/

unsigned int Race::resistance ( void )
{
   return ( p_resistance );
}

void Race::resistance ( unsigned int value )
{
   p_resistance = value;
}

unsigned int Race::weakness ( void )
{
   return ( p_weakness );
}

void Race::weakness ( unsigned int value )
{
   p_weakness = value;
}

int Race::pictureID ( void )
{
   return ( p_pictureID);
}

void Race::pictureID ( int value )
{
   p_pictureID = value;
}

/*unsigned char Race::aligment_restriction ( void )
{
   return ( p_aligment_restriction );
}*/

/*int Race::size ( void )
{
   return ( p_size );
}*/

/*unsigned char Race::stat_bonus ( void )
{
   return ( p_stat_bonus );
} */

/*int Race::HPdice ( void )
{
   return ( p_HPdice );
}

int Race::MPdice ( void )
{
   return ( p_MPdice );
}

int Race::DMGy ( void )
{
   return ( p_DMGy );
}

int Race::MDMGy ( void )
{
   return ( p_MDMGy );
}*/

/*int Race::ability ( int index )
{
   return ( p_ability [ index ] );
}*/


/*-------------------------------------------------------------------------*/
/*-                              Method                                   -*/
/*-------------------------------------------------------------------------*/

/*void Race::edit ( void )
{
   int i;
   short answer;
   short answer2;
   int point;
   char tmpstr [ 101 ];
   bool exit_attribute = false;
   bool exit_modifier = false;
   unsigned short wordmask;
   unsigned char bytemask;
   unsigned short value;
   bool disable;

   p_lifespan = 80;
   for ( i = 0 ; i < 6 ; i++ )
      p_attribute [ i ] = 5;

   Window::instruction ( 200, 460 );
   WinData<Race> wdat_race ( WDatProc_new_race, *this,
      WDatProc_POSITION_NEW_RACE );
   wdat_race.preshow();

   //--- name ---
   WinInput winp_name ( "Enter the name of the race", 12, 20, 50 );
   Window::show_all();
   winp_name.hide();
   winp_name.get_string ( p_name );
   wdat_race.refresh();
   winp_name.hide();

   //--- plural ---
   WinInput winp_plural ( "Enter the plural name of the race", 12, 20, 50 );
   Window::show_all();
   winp_plural.hide();
   winp_plural.get_string ( p_plural );
   wdat_race.refresh();
   winp_plural.hide();

   //--- size ---
   Menu mnu_size ( "Select the size of your race" );
   for ( i = 0 ; i < 4 ; i++ )
      mnu_size.add_item ( STR_OPP_SIZE [ i ] );

   WinMenu wmnu_size ( mnu_size, 20, 50, true );
   answer = Window::show_all();
   p_size = answer;
   wdat_race.refresh();
   wmnu_size.hide();

   //--- attribute ---
   point = 18;

   sprintf ( tmpstr, "Distribute your points ( %d Points )", point );
   Menu mnu_attribute ( tmpstr );
   mnu_attribute.add_item ("STRength");
   mnu_attribute.add_item ("DEXterity");
   mnu_attribute.add_item ("ENDurance");
   mnu_attribute.add_item ("INTelligence");
   mnu_attribute.add_item ("CUNning");
   mnu_attribute.add_item ("WILpower");
   mnu_attribute.add_item ("Keep points");

   WinMenu wmnu_attribute ( mnu_attribute, 20, 50, true, false );
   while ( exit_attribute == false )
   {
      wdat_race.refresh();
      wmnu_attribute.refresh();
      answer = Window::show_all();

      if ( answer < 6 )
      {
         p_attribute [ answer ]++;
         point--;

         if ( p_attribute [ answer ] >=10 )
            mnu_attribute.mask_item ( answer );

         sprintf ( tmpstr, "Distribute your points ( %d Points )", point );
         mnu_attribute.title ( tmpstr );
      }
      else
         exit_attribute = true;

      if ( point <= 0 )
         exit_attribute = true;
   }

   wdat_race.refresh();
   wmnu_attribute.hide();

   //--- Modifiers ---

   sprintf ( tmpstr, "What do you want to change? ( %d points )", point );
   Menu mnu_category ( tmpstr );
   mnu_category.add_item ( "Aligment Restriction" );
   mnu_category.add_item ( "Longevity" );
   mnu_category.add_item ( "Stat Modifier" );
   mnu_category.add_item ( "Health Resistance" );
   mnu_category.add_item ( "End" );

   while ( exit_modifier == false )
   {
      wdat_race.refresh();
      mnu_category.unmask_all_item ();
      if ( point != 0 )
         mnu_category.mask_item ( 4 );
      WinMenu wmnu_category ( mnu_category, 20, 50, true );
      answer = Window::show_all();
      wmnu_category.hide();
      Menu submenu;

      // building menus
      switch ( answer )
      {
         case 0 : // aligment restriction
            submenu.title ( "Which aligment to restrict?" );
            bytemask = 1;
            for ( i = 0 ; i < 6 ; i++ )
            {
               if ( ( p_aligment_restriction & bytemask ) > 0 )
               {
//                  if ( point > 0 )
                     sprintf ( tmpstr, "Remove %s", ALIGMENT_INFO [ i ] . name );
//                  else
//                    sprintf ( tmpstr, "Remove %s", ALIGMENT_INFO [ i ] . name.data(), true );
               }
               else
               {
                  sprintf ( tmpstr, "Add    %s", ALIGMENT_INFO [ i ] . name );
               }
               submenu.add_item ( tmpstr );
               bytemask = bytemask << 1;
            }
         break;
         case 1 : // longevity
            submenu.title ( "Select a life span" );
            value = 0;
            for ( i = 0 ; i < 5 ; i++ )
            {
               if ( i > 1 )
                  value++;
               sprintf ( tmpstr, "%d Years", LIFE_SPAN [ i ] );
//               if ( point >= value )
                  submenu.add_item ( tmpstr );
//               else
//                  submenu.add_item ( tmpstr, true );
            }
         break;
         case 2 : // stat modifier
            bytemask = 1;
            submenu.title ("Select stat modifier" );
            for ( i = 0 ; i < 6 ; i++ )
            {
               disable = false;
               if ( ( p_stat_bonus & bytemask ) > 0 )
                  sprintf ( tmpstr, "Remove %s", STAT_MODIFIER [ i ].name );
               else
               {

//                  if ( ( i % 2 ) == 0 )
//                     if ( p_stat_bonus & ( bytemask << 1 ) > 0 )
//                     { disable = true; }
//                  else
//                     if ( p_stat_bonus & ( bytemask >> 1 ) > 0 )
//                     { disable = true; }

//                  if ( point == 0 )
//                     disable = true;

                  sprintf ( tmpstr, "Add    %s", STAT_MODIFIER [ i ].name );
               }

               submenu.add_item ( tmpstr, disable );
               bytemask = bytemask << 1;
            }
         break;
         case 3 : // health resistance
            submenu.title ("Select Health resistance" );
            wordmask = 1;
            for ( i = 0 ; i < Opponent_NB_HEALTH ; i++ )
            {
               if ( ( p_health_resist & wordmask ) > 0 )
                  sprintf ( tmpstr, "Remove %s", HEALTH_INFO [ i ] . name );
               else
               {
//                  if ( point > 0 )
                     sprintf ( tmpstr, "Add    %s", HEALTH_INFO [ i ] . name );
//                  else
//                     sprintf ( tmpstr, "Add    %s", HEALTH_INFO [ i ] . name, true );
               }
               submenu.add_item ( tmpstr );
               wordmask = wordmask << 1;
            }
         break;
      }

      if ( answer != 4  ) // end
      {
         WinMenu wmnu_submenu ( submenu, 20, 50, false, false );
         answer2 = Window::show_all();

         if ( answer2 != -1 )
         {
            switch ( answer )
            {
               case 0 : // aligment restriction
                  value = p_aligment_restriction;
                  bytemask = 1;
                  bytemask = bytemask << answer2;
                  p_aligment_restriction = p_aligment_restriction ^ bytemask;
                  if ( value < p_aligment_restriction )
                     point++;
                  else
                     point--;
               break;
               case 1 : // longevity
                  switch ( p_lifespan )
                  {
                     case 50 :
                        point--;
                     break;
                     case 320 :
                        point = point + 3;
                     break;
                     case 200 :
                        point = point + 2;
                     break;
                     case 120 :
                        point++;
                     break;
                  }
                  p_lifespan = LIFE_SPAN [ answer2 ];
                  switch ( p_lifespan )
                  {
                     case 50 :
                        point++;
                     break;
                     case 320 :
                        point = point -3;
                     break;
                     case 200 :
                        point = point -2;
                     break;
                     case 120 :
                        point--;
                     break;
                  }

               break;
               case 2 : // stat modifier
                  value = p_stat_bonus;
                  bytemask = 1;
                  bytemask = bytemask << answer2;
                  p_stat_bonus = p_stat_bonus ^ bytemask;

                  if ( answer2 % 2 == 0 )
                  {
                     if ( value > p_stat_bonus )
                        point++;
                     else
                        point--;
                  }
                  else
                  {
                     if ( value > p_stat_bonus )
                        point--;
                     else
                        point++;
                  }
               break;
               case 3 : // health resistance
                  value = p_health_resist;
                  wordmask = 1;
                  wordmask = wordmask << answer2;
                  p_health_resist = p_health_resist ^ wordmask;
                  if ( value < p_health_resist )
                     point--;
                  else
                     point++;
               break;
            }
         }
      }
      else
         exit_modifier = true;
      sprintf ( tmpstr, "What do you want to change? ( %d points )", point );
      mnu_category.title ( tmpstr );
   }

   wdat_race.refresh();
   //--- Special Abilities ---

//   private: int p_ability [ 3 ];

}
*/

/*-------------------------------------------------------------------------*/
/*-                         Virtual Methods                               -*/
/*-------------------------------------------------------------------------*/



   // object relational virtual methods
void Race::sql_to_obj (void)
{

   p_primary_key = SQLcolumn_int (0);
   strncpy ( p_name, SQLcolumn_text(1), Race_NAME_STR_SIZE);
   strncpy ( p_initial, SQLcolumn_text(2), Race_INITIAL_STR_SIZE);
   p_attribute [ 0 ] = SQLcolumn_int(3); // STR
   p_attribute [ 1 ] = SQLcolumn_int(4); // DEX
   p_attribute [ 2 ] = SQLcolumn_int(5); // CON
   p_attribute [ 3 ] = SQLcolumn_int(6); // INT
   p_attribute [ 4 ] = SQLcolumn_int(7); // CUN
   p_attribute [ 5 ] = SQLcolumn_int(8); // WIL
   p_resistance = SQLcolumn_int(9);
   p_pictureID = SQLcolumn_int(10);
   p_weakness = SQLcolumn_int(11);
   /*p_HPdice = SQLcolumn_int(8);;
   p_MPdice = SQLcolumn_int(9);;
   p_DMGy = SQLcolumn_int(10);;
   p_MDMGy = SQLcolumn_int(11);;*/


}

void Race::template_sql_to_obj (void)
{

   // will not be used
}

void Race::obj_to_sqlupdate (void)
{

   sprintf (p_querystr, "name='%s', initial='%s', str=%d, dex=%d, end=%d, int=%d, cun=%d, wil=%d, resistance=%d, pictureID=%d, weakness=%d",
            p_name,
            p_initial,
            p_attribute[0],
            p_attribute[1],
            p_attribute[2],
            p_attribute[3],
            p_attribute[4],
            p_attribute[5],
            p_resistance,
            p_pictureID,
            p_weakness );

}

void Race::obj_to_sqlinsert (void)
{
   sprintf (p_querystr, "'%s', '%s', %d, %d, %d, %d, %d, %d, %d, %d, %d",
            p_name,
            p_initial,
            p_attribute[0],
            p_attribute[1],
            p_attribute[2],
            p_attribute[3],
            p_attribute[4],
            p_attribute[5],
            p_resistance,
            p_pictureID,
            p_weakness );
}

/*
void Race::objdat_to_strdat ( void *dataptr )
{
   dbs_Race &tmpdat = *(static_cast<dbs_Race*> ( dataptr ));
   int i;

   strncpy ( tmpdat.name, p_name, 13 );
   strncpy ( tmpdat.plural , p_plural , 13 );

   tmpdat.aligment_restriction = p_aligment_restriction;
   tmpdat.lifespan = p_lifespan;
   tmpdat.health_resist = p_health_resist;
   tmpdat.size = p_size;

   tmpdat.HPdice = p_HPdice;
   tmpdat.MPdice = p_MPdice;
   tmpdat.DMGdice = p_DMGdice;
   tmpdat.MDMGdice = p_MDMGdice;

   for ( i = 0 ; i < 6 ; i++ )
      tmpdat.attribute [ i ] = p_attribute [ i ];

   for ( i = 0 ; i < 3 ; i++ )
      tmpdat.ability[ i ] = p_ability [ i ];
}

void Race::strdat_to_objdat ( void *dataptr )
{
   dbs_Race &tmpdat = *(static_cast<dbs_Race*> ( dataptr ));
   int i;

   strcpy ( p_name, tmpdat.name );
   strcpy ( p_plural, tmpdat.plural );

   p_aligment_restriction = tmpdat.aligment_restriction;
   p_lifespan = tmpdat.lifespan;
   p_health_resist = tmpdat.health_resist;
   p_size = tmpdat.size;

   p_HPdice = tmpdat.HPdice;
   p_MPdice = tmpdat.MPdice;
   p_DMGdice = tmpdat.DMGdice;
   p_MDMGdice = tmpdat.MDMGdice;

   for ( i = 0 ; i < 6 ; i++ )
      p_attribute [ i ] = tmpdat.attribute [ i ];

   for ( i = 0 ; i < 3 ; i++ )
     p_ability [ i ] = tmpdat.ability [ i ];
}

void Race::child_DBremove ( void )
{

}

*/
