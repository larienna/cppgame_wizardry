/***************************************************************************/
/*                                                                         */
/*                             A C T I O N . C P P                         */
/*                             Class Definition                            */
/*                                                                         */
/*     Content : Class Action                                              */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : September 30th, 2012                                */
/*                                                                         */
/*          This file contains the action class used to keep track of      */
/*     combat actions inside the database.                                 */
/*                                                                         */
/***************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>

/*-------------------------------------------------------------------------*/
/*-                         Constructor / destructor                      -*/
/*-------------------------------------------------------------------------*/

Action::Action ( void )
{
   strcpy (p_tablename, "action");
   p_actor_type = -1;
   p_actorID = -1;
   p_initiative = -1;
   p_commandID = -1;
   p_targetID = -1;
   p_target_type = -1;
   p_value = -1;
}

Action::Action ( Character &character )
{
   load_character (character);

   strcpy (p_tablename, "action");
}

Action::Action ( Monster &monster )
{
   load_monster (monster);

   strcpy (p_tablename, "action");
}

Action::~Action ( void )
{

}


/*-------------------------------------------------------------------------*/
/*-                         Methods                                       -*/
/*-------------------------------------------------------------------------*/

void Action::load_character ( Character &character)
{
   CClass tmpclass;

   tmpclass.SQLselect ( character.FKcclass() );

   p_actor_type = Action_ACTOR_TYPE_CHARACTER;
   p_actorID = character.primary_key();
   //?? need to use another routine that accumulate all modifiers
   p_initiative = dice ( 10 ) + ( character.d20stat ( D20STAT_INIT ));
   p_commandID = -1;
   p_targetID = -1;
   p_target_type = -1;
   p_value = -1;
}

void Action::load_monster ( Monster &monster )
{
   p_actor_type = Action_ACTOR_TYPE_ENNEMY;
   p_actorID = monster.primary_key();
   p_initiative = roll_xpypd20( monster.init() , 0 );
   p_commandID = -1;
   p_targetID = -1;
   p_target_type = -1;
   p_value = -1;

}

int Action::input ( int commandID )
{
   int retval = Action_INPUT_RETVAL_NONE;
   p_commandID = commandID;

   if ( commandID >= 0 && commandID <= CmdProc_COMMANDLIST_SIZE )
   {
      switch ( p_actor_type )
      {
         case Action_ACTOR_TYPE_CHARACTER:
            //printf ("input retval A(%d)\n", retval );
            retval = CmdProc_COMMANDLIST [ commandID ] . proc_input ( *this );
         break;
         case Action_ACTOR_TYPE_ENNEMY:
            retval = CmdProc_COMMANDLIST [ commandID ] . proc_AIinput ( *this );
         break;
      }
      //printf ("input retval B(%d)\n", retval );

      p_initiative += CmdProc_COMMANDLIST [ commandID] . initiative;
   }

   //printf ("input retval C(%d)\n", retval );

   return ( retval );

}

int Action::resolve ( void )
{
   int retval = Action_INPUT_RETVAL_NONE;
   Character tmpactor_char;
   Character tmptarget_char;
   Monster tmpactor_mons;
   Monster tmptarget_mons;
   Opponent *actor = NULL;
   Opponent *target = NULL;
   //int error;
   int tmpcondition = 0;

   //printf ( "debug: Action: Resolve: entering procedure: commandID=%d\n", p_commandID );

   //printf ("Debug: Action: Resolve: Showing function name: %s | %s\n", __FUNCTION__ , __func__ );

   //debug_printf ( __func__, "Showing Function name and some vars=%d, vars=%d", 2, 4 );
  /* int x=2;
   int y=4;

   SWAP ( x, y );*/

   // --- apply condtions ----

   if ( p_actor_type == Opponent_TYPE_CHARACTER && party.status() == Party_STATUS_COMBAT )
   {
      tmpactor_char.SQLselect ( p_actorID );
      tmpcondition = tmpactor_char.compile_condition();


      if ( (tmpcondition & ActiveEffect_CONDITION_RNDACTION ) > 0 )
      {
         condition_random_action();
      }

      if ( (tmpcondition & ActiveEffect_CONDITION_RNDTARGET ) > 0 )
      {
         condition_random_target();
      }

      if ( (tmpcondition & ActiveEffect_CONDITION_RNDFRIEND ) > 0 )
      {
         condition_random_friend();
      }

      if ( (tmpcondition & ActiveEffect_CONDITION_RNDINSANE ) > 0 )
      {
         if ( dice ( 20 ) <= 5) //25% chance
         {
            if ( dice ( 10 ) <= 5) // 50% chance one action or the other
               p_commandID = 48; // imitation
            else
               p_commandID = 49; // Sing
         }
      }

   }
   else
   if ( p_actor_type == Opponent_TYPE_ENNEMY )
   {
      tmpactor_mons.SQLselect ( p_actorID );
      tmpcondition = tmpactor_mons.compile_condition();


      if ( ( tmpcondition & ActiveEffect_CONDITION_RNDACTION ) > 0 )
      {
         //condition_random_action(); // monsters are already doing random actions
      }

      if ( (tmpcondition & ActiveEffect_CONDITION_RNDTARGET ) > 0 )
      {
         condition_random_target();
      }

      if ( (tmpcondition & ActiveEffect_CONDITION_RNDFRIEND ) > 0 )
      {
         condition_random_friend();
      }

      if ( (tmpcondition & ActiveEffect_CONDITION_RNDINSANE ) > 0 )
      {
         if ( dice ( 20 ) <= 5) //25% chance
         {
            p_commandID = 50; // disturbed
         }
      }
   }

   // to change: use fail action instead
   if ( (tmpcondition & ActiveEffect_CONDITION_DISACTION ) > 0 )
         p_commandID = -1;


   //--- resolve command ---

   //printf ( "debug: Action: Resolve: commandID=%d\n", p_commandID );

   if ( p_commandID != -1 )
   {

      switch ( p_actor_type )
      {
      case Action_ACTOR_TYPE_CHARACTER :

         tmpactor_char.SQLselect ( p_actorID );
         actor = &tmpactor_char;

         switch ( p_target_type )
         {
            case Action_TARGET_ONEENEMY:
               tmptarget_mons.SQLselect ( p_targetID );
               target = &tmptarget_mons;
            break;
            case Action_TARGET_ONECHARACTER:
               tmptarget_char.SQLselect ( p_targetID );
               target = &tmptarget_char;
            break;
         }

      break;
      case Action_ACTOR_TYPE_ENNEMY :

         tmpactor_mons.SQLselect ( p_actorID );
         actor = &tmpactor_mons;

         switch ( p_target_type )
         {
            case Action_TARGET_ONEENEMY:
               tmptarget_mons.SQLselect ( p_targetID );
               target = &tmptarget_mons;
            break;
            case Action_TARGET_ONECHARACTER:
               tmptarget_char.SQLselect ( p_targetID );
               target = &tmptarget_char;
            break;

         }
      break;
      }


      //float tmpbefore = clock();
      if ( actor->body() == Character_BODY_ALIVE || CmdProc_COMMANDLIST [ p_commandID ] . usablebydead == true)
      {
         if ( actor != NULL )
            actor->compile_stat();

         if ( target != NULL )
            target->compile_stat();

         //printf ( "debug: Action: Resolve: Before run command\n");
         retval = CmdProc_COMMANDLIST [ p_commandID ] . proc_resolve ( *this, actor, target );

      }
      else
         retval = Action_RESOLVE_RETVAL_CANCEL;
      //float tmpafter = clock();
      //tmpafter = ( tmpafter - tmpbefore )  / CLOCKS_PER_SEC;
      //printf ("Resolve proc Time: %f\n", tmpafter);

   }
   else
      retval = Action_RESOLVE_RETVAL_CANCEL;

   return ( retval );
}

void Action::command_str ( char* tmpstr )
{
   //char tmpstr [100] = "";
   //char target

   //impossible to return a tmpstring because considered a local variable.
   //Else would have to use a copy of a string in reference to be modified and return.

   if ( p_commandID != -1)
   {

      sprintf ( tmpstr, "%s", CmdProc_COMMANDLIST [ p_commandID ].name );
   }

   //return ( tmpstr );
}

void Action::condition_random_friend ( void )
{

         if ( (p_target_type & Action_TARGET_MASK_ENEMY ) > 0 )
         {
            p_target_type = p_target_type & ~Action_TARGET_MASK_ENEMY;
            p_target_type = p_target_type | Action_TARGET_MASK_CHARACTER;
         }
         else if ( ( p_target_type & Action_TARGET_MASK_CHARACTER ) > 0)
         {
            p_target_type = p_target_type & ~Action_TARGET_MASK_CHARACTER;
            p_target_type = p_target_type | Action_TARGET_MASK_ENEMY;
         }

   chose_new_target();

}

void Action::condition_random_target ( void )
{
   p_target_type = p_target_type & ~Action_TARGET_MASK_ENEMY;
   p_target_type = p_target_type & ~Action_TARGET_MASK_CHARACTER;


   if ( dice (10) <= 5) //50% target friend or ennemy
   {
      p_target_type = p_target_type | Action_TARGET_MASK_CHARACTER;
   }
   else
   {
      p_target_type = p_target_type | Action_TARGET_MASK_ENEMY;
   }

   chose_new_target();

}

void Action::condition_random_action ( void )
{
   // not much actions can be done randomly, maybe use item could be used
   // note: only used by characters, since monster already have random action
   Character tmpchar;

   if ( p_actor_type == Opponent_TYPE_CHARACTER )
   {
      tmpchar.SQLselect ( p_actorID );

      if ( dice(10) <= 5) //50% chance to try casting a spell
      {
         CClass tmpclass;
         int school = 1;
         p_commandID = 5;

         tmpclass.SQLselect ( tmpchar.FKcclass() );

         while ( ( tmpclass.magic_school() & school ) == 0 && school <= CClass_MAGICSCHOOL_PSIONIC )
         {
            p_commandID++;

            school = school << 1;
         }

         if ( p_commandID >= 5 && p_commandID <= 8)
         {
//-------------------------------------
            //char levelstr [21] = "level";
            int error;
             // high level spells cannot be cast randomly
            Spell tmpspell;

            error = tmpspell.SQLpreparef ( "WHERE school=%d AND (castable=2 OR castable=3) AND level <= %d ORDER BY random()",
               school, tmpchar.level() );

            if ( error == SQLITE_OK )
            {
               error = tmpspell.SQLstep();

               if ( error == SQLITE_ROW )
               {
                  p_value = tmpspell.primary_key();
                  p_target_type = tmpspell.target ();
               }
               else
               {
                  p_commandID = 1; // fight
                  p_target_type = Action_TARGET_ONEENEMY;
               }

               tmpspell.SQLfinalize();

            }
//------------------------------------------
         }
         else
         {
            p_commandID = 1; // fight
            p_target_type = Action_TARGET_ONEENEMY;
         }



      }
      else
      {
         p_commandID = 1; // fight
         p_target_type = Action_TARGET_ONEENEMY;
      // range is no used for now
      }

      chose_new_target();
   }

}

void Action::chose_new_target ( void )
{
   Character tmptarget_char;

   Monster tmptarget_mons;
   int error;

         if ( p_target_type == Action_TARGET_ONECHARACTER)
         {
            error = tmptarget_char.SQLpreparef ( "WHERE body=%d AND location=%d ORDER BY random()", Character_BODY_ALIVE, Character_LOCATION_PARTY  );

            if ( error == SQLITE_OK )
            {
               error = tmptarget_char.SQLstep();



               if ( error == SQLITE_ROW)
               {
                  if ( actor_type() == Opponent_TYPE_CHARACTER && tmptarget_char.primary_key() == actorID() )
                     error = tmptarget_char.SQLstep();// actor and target is the same, chose a new target.

                  if ( error == SQLITE_ROW )
                     p_targetID = tmptarget_char.primary_key();
               }

               tmptarget_char.SQLfinalize();
            }
         }
         else
         if ( p_target_type == Action_TARGET_ONEENEMY)
         {
            error = tmptarget_mons.SQLpreparef ( "WHERE body=%d ORDER BY random()", Character_BODY_ALIVE, Character_LOCATION_PARTY  );

            if ( error == SQLITE_OK )
            {
               error = tmptarget_mons.SQLstep();

               if ( error == SQLITE_ROW)
               {
                  if ( actor_type() == Opponent_TYPE_ENNEMY && tmptarget_mons.primary_key() == actorID() )
                     error = tmptarget_mons.SQLstep();// actor and target is the same, chose a new target.

                  if ( error == SQLITE_ROW )
                     p_targetID = tmptarget_mons.primary_key();
               }

               tmptarget_mons.SQLfinalize();
            }
         }
         else
         if ( p_target_type == Action_TARGET_GROUPCHARACTER || p_target_type == Action_TARGET_GROUPENEMY )
            p_targetID = dice ( 2 );
}

/*-------------------------------------------------------------------------*/
/*-                         Virtual Methods                               -*/
/*-------------------------------------------------------------------------*/

 void Action::sql_to_obj (void)
 {
   p_primary_key = SQLcolumn_int (0);
   p_actor_type  = SQLcolumn_int (1);
   p_actorID  = SQLcolumn_int (2);
   p_commandID = SQLcolumn_int (3);
   p_initiative = SQLcolumn_int (4);
   p_target_type = SQLcolumn_int (5);
   p_targetID = SQLcolumn_int (6);
   p_value = SQLcolumn_int (7);
 }

 void Action::template_sql_to_obj (void)
 {
    // not used since require function pointers which cannot be located in database.
    // command ID is used instead to point to the commandlist table.
 }

 void Action::obj_to_sqlupdate (void)
 {
    sprintf ( p_querystr, "actor_type=%d, actorid=%d, commandid=%d, initiative=%d, target_type=%d, targetid=%d, value=%d",
    p_actor_type,
    p_actorID,
    p_commandID,
    p_initiative,
    p_target_type,
    p_targetID,
    p_value );

 }

 void Action::obj_to_sqlinsert (void)
 {
    sprintf ( p_querystr, "%d, %d, %d, %d, %d, %d, %d",
    p_actor_type,
    p_actorID,
    p_commandID,
    p_initiative,
    p_target_type,
    p_targetID,
    p_value );
 }



