/************************************************************************/
/*                                                                      */
/*                        a e f f e c t . c p p                         */
/*                                                                      */
/*     Content: Class ActiveEffect                                      */
/*     Programmer: Eric Pietrocupo                                      */
/*     Starting Date: November 4th, 2014                                */
/*                                                                      */
/*     Data object to manage the active effects created by spells,      */
/*     special attacks, items, and other sources.                       */
/*                                                                      */
/************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>

/*----------------------------------------------------------------------*/
/*-                         Static Variables                           -*/
/*----------------------------------------------------------------------*/

/*s_SQLfield_int ActiveEffect::p_SQLfield_int_def [NB_INTEGER] =
 {
    { "expiration" },
    { "category" },
    { "AD" },
    { "MD" },
    { "PS" },
    { "MS" },
    { "PCS" },
    { "MCS" },
    { "DR" },
    { "INIT" },
    { "DMG" },
    { "MDMG" },
    { "HP" },
    { "MP" },
    { "persist_modulo" },
    { "source_id" },
    { "time" },
    { "target_type" },
    { "target_id" },
    { "duration" }
 }; //  int SQLfield definition

 s_SQLfield_str ActiveEffect::p_SQLfield_str_def [NB_STRING] =
 {
    { "name", ActiveEffect_NAME_LEN },
    { "condition", ActiveEffect_CONDITION_LEN }
 };*/

s_SQLfield ActiveEffect::p_SQLfield [ NB_FIELD ] =
{
   { "name", TEXT, ActiveEffect_NAME_LEN,            "Name                  ", true, NULL},
	{ "expiration",	INTEGER, 0 ,                    "Expiration            ", false, NULL},
	{ "duration",	INTEGER, 0 ,                       "Original Duration     ", false, NULL},
	{ "condition",	STRFIELD, ActiveEffect_CONDITION_LEN,  "Condition             ", true, STRFLD_CONDITION },
	{ "category",	INTEGER, 0 ,                       "Category              ", false, NULL},
	{ "AD",	INTEGER, 0 ,                             "Active Defense        ", true, NULL},
	{ "MD",	INTEGER, 0 ,                             "Magick Defense        ", true, NULL},
	{ "PS",	INTEGER, 0 ,                             "Physical Saves        ", true, NULL},
	{ "MS",	INTEGER, 0 ,                             "Mental Saves          ", true, NULL},
	{ "melee_CS",	INTEGER, 0 ,                       "Physical Combat Skills", true, NULL},
	{ "range_CS",	INTEGER, 0 ,                       "Mental Combat Skills  ", true, NULL},
	{ "DR",	INTEGER, 0 ,                             "Damage Resistance     ", true, NULL},
	{ "INIT",	INTEGER, 0 ,                          "Initiative            ", true, NULL},
	{ "DMG",	INTEGER, 0 ,                             "Damage Z              ", true, NULL},
	{ "MDMG",	INTEGER, 0 ,                          "Magic Damage Z        ", true, NULL},
	{ "attdiv",	INTEGER, 0 ,                          "Attack Divider        ", true, NULL},
	{ "powdiv",	INTEGER, 0 ,                          "Power Divider         ", true, NULL},
	{ "HP",	INTEGER, 0 ,                             "Hit Points            ", true, NULL},
	{ "MP",	INTEGER, 0 ,                             "Magick Points         ", true, NULL},
	{ "modulo",	INTEGER, 0,                           "Nb turns to apply     ", true, NULL },
	{ "kaomoji",   TEXT, ActiveEffect_KAOMOJI_LEN,    "Kaomoji               ", true, NULL},
	{ "target_type",	INTEGER, 0 ,                    "Target Type           ", false, NULL},
	{ "target_id",	INTEGER, 0 ,                       "Target ID             ", false, NULL},
	{ "template_id", INTEGER, 0,                      "Template Key          ", false, NULL },// makes each effect unique
	{ "time",	INTEGER, 0 ,                          "Time left             ", false, NULL}
};

/*----------------------------------------------------------------------*/
/*-                         Constructor and destructor                 -*/
/*----------------------------------------------------------------------*/

ActiveEffect::ActiveEffect ( void )
{
   //p_nb_integer = NB_INTEGER;
   //p_nb_string = NB_STRING;
   strcpy ( p_tablename, "effect");
   strcpy ( p_template_tablename, "effect_template ");

   p_SQLdata [ 0 ] . str = p_name;
   p_SQLdata [ 3 ] . str = p_condition;
   p_SQLdata [ 20 ] . str = p_kaomoji;

   p_SQLfield_ptr = p_SQLfield;
   p_SQLdata_ptr = p_SQLdata;
   p_nb_field = NB_FIELD;

   set_all_int ( 0 );
   set_all_str  ( "" );




}

ActiveEffect::~ActiveEffect ( void )
{

}

/*----------------------------------------------------------------------*/
/*-              Virtual methods from Enchanced SQL object             -*/
/*----------------------------------------------------------------------*/


void ActiveEffect::template_load_completion ( void )
{

   if ( duration() > 0 )
      time ( duration () );

   target_id ( 0 );
   target_type ( 0 );
   template_id ( SQLcolumn_int (0) );

   //printf ("Debug: ActiveEffect: Template_load_completion\n");


};

void ActiveEffect::callback_handler ( int index )
{

   switch ( index )
   {
      case ActiveEffect_CB_PERSIST_EFFECT:
         cb_persist_effect();
      break;
   }

}

void ActiveEffect::cb_persist_effect ( void )
{
  // printf ( "Debug: AE:cb_persist_effect: clock=%d, modulo=%d, result=%d\n",
//           game.clock.minute(), modulo(), game.clock.minute() % modulo() );
   if ( ( game.clock.minute() % modulo() ) == 0  )
   {// bug here, minute does not change when repeat days (else only apply in maze)

      //printf ( "Debug: AE: target_type=%d, HP=%d, target_id=%d\n", target_type(), HP(), target_id() );

      if ( target_type() == Opponent_TYPE_CHARACTER)
      {
         Character tmpchar;

         tmpchar.SQLselect ( target_id () );

         tmpchar.adjust_HP( HP() );
         tmpchar.adjust_MP( MP() );
         tmpchar.SQLupdate();

         //printf ( "Debug: AE:cb_persist_effect: passed in for: %s", tmpchar.name() );

         // add HP/MP minus/bonus flag to use in party bar, maybe a single character
         //need to remove flag elsewhere?
      }
      else
         if ( target_type() == Opponent_TYPE_ENNEMY)
         {
            Monster tmpmonster;

            tmpmonster.SQLselect ( target_id());

            tmpmonster.adjust_HP( HP() );
            tmpmonster.adjust_MP( MP() );
            tmpmonster.SQLupdate ();

         }

   }
}


/*----------------------------------------------------------------------*/
/*-                    Static Methods                                  -*/
/*----------------------------------------------------------------------*/

void ActiveEffect::trigger_expiration ( int situation )
{
   char querystr [101] = "";

   sprintf ( querystr, "DELETE FROM effect WHERE expiration<=%d", situation );

   SQLexec ( querystr );
}

void ActiveEffect::trigger_disturb_expiration ( Opponent *target )
{
   char querystr [101] = "";

   sprintf ( querystr, "DELETE FROM effect WHERE expiration=%d AND target_type=%d AND target_id=%d"
            , ActiveEffect_EXPIRATION_DISTURBED, target->type(), target->primary_key() );

   SQLexec ( querystr );
}

void ActiveEffect::trigger_expiration_by_death ( Opponent *target )
{
   char querystr [101] = "";

   sprintf ( querystr, "DELETE FROM effect WHERE expiration<=%d AND target_type=%d AND target_id=%d"
            , ActiveEffect_EXPIRATION_PERMANENT, target->type(), target->primary_key() );

   SQLexec ( querystr );
}

void ActiveEffect::reduce_time ( int minute )
{
   char querystr [101] = "";

   sprintf ( querystr, "UPDATE effect SET time = time - %d WHERE time > 0 ", minute);

   SQLexec ( querystr );

   SQLexec ("DELETE FROM effect WHERE time = 0 AND duration > 0");

}

void ActiveEffect::trigger_modulo_effects ( void )
{
  // Character tmpchar;
  // Monster tmpmonster;
   ActiveEffect tmpeffect;
   //int error;

   //printf ("Debug: Active Effect: Trigger Modulo\n");

   tmpeffect.SQLprocedure ( ActiveEffect_CB_PERSIST_EFFECT, "WHERE modulo > 0");




}

bool ActiveEffect::add_effect ( int effect_id , Opponent *target )
{
   bool ignore_effect = false;
   int error;
   bool effect_added = false;

  // printf ( "debug: ActiveEffect: Add_effect: start proc. effect_id=%d\n", effect_id);
   if ( effect_id > 0)
   {
    //  printf ( "debug: ActiveEffect: Add_effect: effect_id > 0\n");
      ActiveEffect tmpeffect;

  //    SQLactivate_errormsg();
      error = tmpeffect.template_SQLselect ( effect_id );

      if ( config.get ( Config_HEALTH_STATUS ) == Config_DIF_DISABLED )
      {
         if ( target->type() == Opponent_TYPE_CHARACTER )
            if ( tmpeffect.category() > 0 && tmpeffect.category() < 10 )
               ignore_effect = true;
      }

    //  printf ( "debug: ActiveEffect: Add_effect: effect to add %s, error=%d \n", tmpeffect.name(), error );



      if ( error == SQLITE_ROW && ignore_effect == false )
      {
          // --- check elemental resistance toward the effect category ---

         int tmpelement = (1 << 7) + tmpeffect.category ();

         if ( ( tmpelement & target->resistance() ) > 0 )
         ignore_effect = true;

         if ( ignore_effect == false )
         {
            int effectcategory = tmpeffect.category();
            int tmpcondition = target->compile_condition();

            if ( effectcategory == ActiveEffect_CATEGORY_BODY
                || effectcategory == ActiveEffect_CATEGORY_BLOOD
                || effectcategory ==  ActiveEffect_CATEGORY_SKIN )

            {

               if ( ( tmpcondition & ActiveEffect_CONDITION_FAILPSAVE) == 0 )
               {
                  int ps =  target->d20stat ( D20STAT_PS );
                  int ps_roll = dice (20);

                  if ( ps_roll < ps || ps_roll >= 19 )
                  {
                     ignore_effect = true;
                     system_log.writef ( "%s save the effect", target->displayname() );
                  }
               }
            }
            else
               if ( effectcategory == ActiveEffect_CATEGORY_SOUL
                || effectcategory == ActiveEffect_CATEGORY_NEURAL
                || effectcategory ==  ActiveEffect_CATEGORY_PSYCHIC )

               {
                  if ( ( tmpcondition & ActiveEffect_CONDITION_FAILMSAVE) == 0 )
                  {
                     int ms =  target->d20stat ( D20STAT_MS );
                     int ms_roll = dice (20);

                     if ( ms_roll < ms || ms_roll >= 19 )
                     {
                        ignore_effect = true;
                        system_log.writef ( "%s save the effect", target->displayname() );
                     }
                  }
               }


            if ( ignore_effect == false )
            {


               char querystr [101] = "";

               sprintf ( querystr, "DELETE FROM effect WHERE template_id=%d AND target_type=%d AND target_id=%d"
                  , effect_id, target->type(), target->primary_key() );

               SQLexec ( querystr );

               tmpeffect.target_id ( target->primary_key() );
               tmpeffect.target_type ( target->type() );

            //printf ( "debug: ActiveEffect: Add_effect: added %s \n", tmpeffect.name() );

               //SQLactivate_errormsg();
               tmpeffect.SQLinsert();
               effect_added = true;
               //SQLdeactivate_errormsg();

               /*printf ("Debug: Aeffect: add_effect: target_id=%d, target_type=%d, template_id=%d\n", tmpeffect.target_id(),
                       tmpeffect.target_type(), tmpeffect.template_id() );
               printf ("--- active effect list ---\n");

               ActiveEffect tmpeffect2;
               int error2 = tmpeffect2.SQLprepare ("");

               if ( error2 == SQLITE_OK )
               {
                  error2 = tmpeffect2.SQLstep();

                  while ( error == SQLITE_ROW )
                  {
                     printf ( "%s target_id=%d, target_type=%d\n", tmpeffect2.name(), tmpeffect2.target_id(),
                       tmpeffect2.target_type());
                     error2 = tmpeffect2.SQLstep();
                  }

                  tmpeffect2.SQLfinalize();
               }

               printf ("--- active effect end ---\n");
*/
            }
         }
         else
            system_log.writef ( "%s resist the effect", target->displayname() );
      }
      else
         system_log.writef ( "%s ignore the effect", target->displayname() );

      tmpeffect.SQLfinalize();
   }

  /// SQLdeactivate_errormsg();

   /*if ( ignore_effect == true )
      return false;
   else
      return true;*/
   return effect_added;

}

/*----------------------------------------------------------------------*/
/*-                    Hash Table                                      -*/
/*----------------------------------------------------------------------*/


s_EnhancedSQLobject_strfield STRFLD_CONDITION [ EnhancedSQLobject_HASH_SIZE ] =
{
  //{ "Irr", "Irregular" },
  { "Dta", "Disable Targeting" },
  { "Dac", "Disable Actions" },
  { "Dsp", "Disable Spell Casting" },
  { "Dsk", "Disable Skills" },
  { "Rdt", "Random Target" },
  { "Rda", "Random Action" },
  { "Rdf", "Random Friend" },
  { "Rdi", "Random Insane Action" },
  { "Fps", "Fail Physical Saves" },
  { "Fms", "Fail Mental Saves" },
  { "Fdo", "Fail Dodge" },
  { "Fat", "Fail Attacks" },
  { "Rdm", "Reduce Damage" },
  { "Rmd", "Reduce Magick Damage" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "Idt", "Irregular Disable Targeting" },
   { "Ida", "Irregular Disable Action" },
   { "Ids", "Irregular Disable Spell" },
   { "Idk", "Irregular Disable Skill" },
   { "Irt", "Irregular Random Target" },
   { "Ira", "Irregular Random Action" },
   { "Irf", "Irregular Random Friend" },
   { "Iri", "Irregular Random Insane" },
   { "Ifa", "Irregular Fail Action" },
   { "Ird", "Irregular Reduce Dmg" },
   { "Irm", "Irregular Reduce Mdmg" }
};

/*"{^_^}(^_^)"

[^_^]|^_^|._.|+_+|x_x|
(._.)(@_@)
!"#$%&'*+,-./;:=?@\^_'|~
()[]<>{}
«» ¼ ¡
|¬_¬|^_^|#_#|'o'|¼_¼|«_»|¡_¡|<_>|>_<|Ɵ_Ɵ|¶_¶|-_-|;_;|=_=|
|º_º|°.°|^.^|*_*|^_'|ò_ó|~_~|

  { "Dta", "Disable Targeting" },
|+_+|  { "Dac", "Disable Actions" },
|?_?|  { "Dsp", "Disable Spell Casting" },
  { "Dsk", "Disable Skills" },
  { "Rdt", "Random Target" },
  { "Rda", "Random Action" },
|  { "Rdf", "Random Friend" },
  { "Rdi", "Random Insane Action" },
  { "Fps", "Fail Physical Saves" },
  { "Fms", "Fail Mental Saves" },
  { "Fdo", "Fail Dodge" },
  { "Fat", "Fail Attacks" },
  { "Rdm", "Reduce Damage" },
  { "Rmd", "Reduce Magick Damage" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "???", "???" },
   { "Idt", "Irregular Disable Targeting" },
   { "Ida", "Irregular Disable Action" },
   { "Ids", "Irregular Disable Spell" },
   { "Idk", "Irregular Disable Skill" },
   { "Irt", "Irregular Random Target" },
   { "Ira", "Irregular Random Action" },
   { "Irf", "Irregular Random Friend" },
   { "Iri", "Irregular Random Insane" },
   { "Ifa", "Irregular Fail Action" },
   { "Ird", "Irregular Reduce Dmg" },
   { "Irm", "Irregular Reduce Mdmg" }
*/
