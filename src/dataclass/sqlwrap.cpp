/************************************************************************/
/*                                                                      */
/*                        S Q L w r a p . c p p                         */
/*                                                                      */
/*   Content: Module of SQL wrapping functions                          */
/*   Programmer: Eric Pietrocupo                                        */
/*   Starting Date: April, 22nd, 2012                                   */
/*                                                                      */
/*   This is a wrapper library for SQLite related to SQLobject. This    */
/*   file contains procedure to be used for non-dataobject operations.  */
/*   The database pointer is shared by sqlwrap and sqlobject.           */
/*                                                                      */
/************************************************************************/

// include group
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>


/*-------------------------------------------------------------------------*/
/*-                        Global Variables                               -*/
/*-------------------------------------------------------------------------*/

bool SQLerrormsg_active = false;
sqlite3 *SQLdb = NULL;
sqlite3_stmt *SQLstatement;
//const char *SQLtail;
int SQLerror;
bool SQLisopen = false;

//sqlite3 *sqldb; // SQLite database pointer;

/*-------------------------------------------------------------------------*/
/*-                        Class Methods                                  -*/
/*-------------------------------------------------------------------------*/


int SQLcommit ( void )
{

   return sqlite3_exec ( SQLdb, "COMMIT", NULL, NULL, NULL);
}

int SQLexec ( const char *querystr )
{

   SQLerror =  sqlite3_exec ( SQLdb, querystr, NULL, NULL, NULL);

   if ( SQLerror != SQLITE_OK)
   {
      printf("\r\nError: SQLexec: %s\r\n",  sqlite3_errmsg(SQLdb));
      printf("\r\n   Query: %s", querystr);
   }

   return SQLerror;
}

int SQLcount ( const char *field, const char* table, const char* condition )
{

   char tmpstr [SQL_QUERYSTR_LEN];
   int retval = 0;

   sprintf (tmpstr, "SELECT COUNT(%s) FROM %s %s", field, table, condition);
   //p_sqlerror = sqlite3_exec ( SQLdb, p_querystr, NULL, NULL, NULL);

   SQLerror = SQLprepare (tmpstr);

   if ( SQLerror == SQLITE_OK)
   {
      SQLerror = SQLstep();

      if ( SQLerror == SQLITE_ROW)
      {

//printf("\r\n pass 2");
         retval = SQLcolumn_int (0);
         //printf("\r\n pass 3");
      }
      else
         if ( SQLerrormsg_active == true)
         {
            printf("\r\nError: SQLcount (step failed): %s\r\n",  sqlite3_errmsg(SQLdb));
            printf("\r\n   Query: %s", tmpstr);
         }
   }
   else
      if ( SQLerrormsg_active == true)
      {
         printf("\r\nError: SQLcount (prepare failed): %s\r\n",  sqlite3_errmsg(SQLdb));
         printf("\r\n   Query: %s", tmpstr);
      }

   SQLfinalize();


   return retval;

}

int SQLbegin_transaction ( void )
{
   SQLerror = sqlite3_exec ( SQLdb, "BEGIN TRANSACTION", NULL, NULL, NULL);

   return ( SQLerror );
}

int SQLcommit_transaction ( void )
{
   SQLerror = sqlite3_exec ( SQLdb, "COMMIT TRANSACTION", NULL, NULL, NULL);

   return ( SQLerror );
}


int SQLbackup ( const char *filename)
{

  sqlite3 *newdb;
  sqlite3_backup *bakpage;
  int error;

  error = sqlite3_open_v2( filename, &newdb, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL );

  //printf ("pass 1 \r\n");
  if ( error == SQLITE_OK )
  {
   //printf ("pass 2 \r\n");
     bakpage = sqlite3_backup_init( newdb, "main", SQLdb, "main" );


     if ( bakpage != NULL )
     {
        //printf ("pass 3 \r\n");

        error = sqlite3_backup_step (bakpage, -1);
        if ( error != SQLITE_DONE)
        {


           //printf("Error: SQLbackup: %s\r\n", sqlite3_errmsg(newdb));
           printf ("pass 4 \r\n");
        }

        error = sqlite3_backup_finish ( bakpage );
        if ( error != SQLITE_OK)
        {

           //printf ("pass 5 \r\n");
           printf("Error: SQLbackup: %s\r\n", sqlite3_errmsg(newdb));
        }

     }
     else
        printf("Error: SQLbackup: cannot init backup\r\n");


  }
  else
     printf("Error: SQLbackup: %s\r\n", sqlite3_errmsg(newdb));

   sqlite3_close (newdb);

   return error;
}

int SQLopentemp_from_dataobj ( const char *filename, const char *objectname )
{
   DATAFILE *tmpobj;
   unsigned char *tmpptr;
   FILE *fileptr;

   tmpobj = load_datafile_object (filename, objectname);

   if ( tmpobj != NULL )
   {

      tmpptr = static_cast<unsigned char*>(tmpobj -> dat);

      fileptr = fopen ("tmpsqldb.sqlite", "wb");

      if (fileptr != NULL)
      {
         fwrite (tmpptr, sizeof ( unsigned char ), tmpobj -> size, fileptr);
         fclose (fileptr);

         SQLerror = SQLopen ("tmpsqldb.sqlite");

         if ( SQLerror != SQLITE_OK)
            printf("Error: SQLopentemp_from_datobj: %s\r\n", sqlite3_errmsg(SQLdb));
      }
      else
         printf ("Error: SQLopentemp_from_dataobj: Cannot create temporary database\r\n");

      unload_datafile_object (tmpobj);

   }
   else
      printf ("Error: SQLopentemp_from_dataobj: Cannot open Datafile Object\r\n");

   return SQLerror;
}

int SQLclosetemp ( void )
{
   SQLclose();
   remove ("tmpsqldb.sqlite");
   return SQLITE_OK;
}

int  SQLopen ( const char* filename )
{
   SQLerror = -1;

   if ( SQLisopen == false)
   {

      SQLerror = sqlite3_open_v2(filename, &SQLdb, SQLITE_OPEN_READWRITE, NULL );
      if ( SQLerror == SQLITE_OK )
         SQLisopen = true;
   }
   else
   {
      printf ("Error: SQLopen: Database already open\r\n");
   }

   return SQLerror;
}

int  SQLclose ( void )
{

   SQLerror =  sqlite3_close(SQLdb);
   SQLisopen = false;

   return SQLerror;
}

void  SQLactivate_errormsg ( void )
{
   SQLerrormsg_active = true;
}

void  SQLdeactivate_errormsg ( void )
{
   SQLerrormsg_active = false;
}

int  SQLprepare (const char *querystr )
{
   //const char *SQLtail;

//printf ( "Debug: SQLprepare: allo comment ca va = %s\n", querystr);

   int SQLerror = sqlite3_prepare_v2 (SQLdb, querystr, -1, &SQLstatement, NULL);

   if ( SQLerror != SQLITE_OK && SQLerrormsg_active == true )
   {
      printf("\r\nError: SQLprepare: %s\r\n", sqlite3_errmsg(SQLdb));
      printf("\r\n   Query: %s", querystr);
   }


   return SQLerror;
}

int  SQLpreparef ( const char *format, ... )
{
   va_list arguments;
   //int i;
   char *tmpstr;
   int length;

   //get the length by supplying an empty destination to allocate a buffer.
   va_start (arguments, format);
   length = vsnprintf (NULL, 0, format, arguments) + 1;
   tmpstr = (char*) malloc ( length);
   va_end (arguments);

   //redo the same process for real with an allocated string
   va_start (arguments, format);
   vsnprintf (tmpstr, length, format, arguments);
   va_end (arguments);

   SQLerror = SQLprepare (tmpstr);

   return SQLerror;
}

int  SQLstep (void)
{
   SQLerror =  sqlite3_step (SQLstatement);

   if ( (SQLerror != SQLITE_ROW && SQLerror != SQLITE_OK && SQLerror != SQLITE_DONE ) && SQLerrormsg_active == true )
   {
      printf("Error: SQLstep: %s\r\n", sqlite3_errmsg(SQLdb));
      //printf("\r\n   Query: %s", p_querystr);
   }

   return SQLerror;
}

int  SQLfinalize (void)
{
   SQLerror = sqlite3_finalize (SQLstatement);

   if (SQLerror != SQLITE_OK && SQLerrormsg_active == true )
         printf("Error: SQLfinalize_s: %s\r\n", sqlite3_errmsg(SQLdb));

   SQLstatement = NULL;

   return SQLerror;
}


void  SQLerrormsg ( void )
{

   printf("Error: SQLerrormsg_s: %s\r\n", sqlite3_errmsg(SQLdb));

}

int  SQLcolumn_int ( int colID )
{

   //return SQLcolumn_int (SQLstatement, colID);
   return sqlite3_column_int (SQLstatement, colID);

}

const char*  SQLcolumn_text ( int colID )
{

   return (const char*) sqlite3_column_text (SQLstatement, colID);


}

const void *SQLcolumn_blob ( int colID )
{
   return sqlite3_column_blob (SQLstatement, colID);
}

//            rc = sqlite3_bind_blob(stmt, 1, buffer, size, SQLITE_STATIC);

int SQLbind_blob ( const void* buffer, int size, int index )
{

   SQLerror =  sqlite3_bind_blob (SQLstatement, index, buffer, size, SQLITE_STATIC );

   if ( SQLerror != SQLITE_OK && SQLerrormsg_active == true )
   {
      printf("Error: SQLbind_blob: %s\r\n", sqlite3_errmsg(SQLdb));

   }

   return SQLerror;

}



/*---------------------------------------------------------------------*/
/*-                  Methods that use and non-default database        -*/
/*---------------------------------------------------------------------*/

/*int SQLcommit ( sqlite3 *exSQLdb )
{

}

int SQLopen ( sqlite3 **exSQLdb, const char* filename )
{

}

int SQLclose ( sqlite3 *exSQLdb )
{


}

int SQLprepare (sqlite3 *exSQLdb, sqlite3_stmt **exSQLstatement, const char *querystr )
{



}

int SQLpreparef (sqlite3 *exSQLdb, sqlite3_stmt **exSQLstatement, const char *format, ... )
{
   va_list arguments;
   //int i;
   char tmpstr[SQL_QUERYSTR_LEN] = "";


   va_start (arguments, format);
   vsprintf (tmpstr,format, arguments);
   va_end (arguments);

   return SQLprepare (exSQLdb, exSQLstatement, tmpstr);



}

int SQLstep (sqlite3_stmt *exSQLstatement)
{


}

int SQLfinalize (sqlite3 *exSQLdb, sqlite3_stmt *exSQLstatement)
{


}

void SQLerrormsg ( sqlite3 *exSQLdb )
{

}

int SQLcolumn_int ( sqlite3_stmt *exSQLstatement, int colID )
{

}

const char* SQLcolumn_text ( sqlite3_stmt *exSQLstatement, int colID )
{

}*/


