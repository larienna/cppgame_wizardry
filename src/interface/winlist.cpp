/***************************************************************************/
/*                                                                         */
/*                       W I N L I S T . C P P                             */
/*                        Class Source Code                                */
/*                                                                         */
/*     Content : Class WinList                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : Novemberth, 11th, 2002                              */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>


/*
//#include <time.h>
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <dbobject.h>
//#include <database.h>
//#include <dbdef.h>
//
//
//
//
//
//
#include <listwiz.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
//#include <winempty.h>
//#include <wintitle.h>
//#include <winmenu.h>
#include <winlist.h>
*/

/*-------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                         -*/
/*-------------------------------------------------------------------------*/

WinList::WinList ( List &lst, short x_pos, short y_pos,
            bool no_cancel, bool reset_cursor, bool translucent//,
            /*bool sensible = false*/  )
{
   short chr_width;
   int font_width = text_length ( List_FONT, "a" );
   int font_height = text_height ( List_FONT );
   int header_font_height = text_height ( List_HEADER_FONT );

   p_translucent = translucent;
   p_list = &lst;
   p_x_pos = x_pos;
   p_y_pos = y_pos;
   p_no_cancel = no_cancel;
   p_reset_cursor = reset_cursor;
   chr_width = p_list->char_width();
   p_width = 6 + ( font_width * ( chr_width ) ) + 6 + 1;
   p_height = 6 + ( font_height * p_list->page_size() ) + 6 + 1;

   if ( p_list->showheader() == true)
   {
      p_height += header_font_height + List_HEADER_SPACE;
   }
//   p_cursor = 0;

//   p_list->sensible ( sensible );

   if ( strcmp ( p_list->title(),  "" ) != 0 )
      p_height = p_height + font_height + ( font_height / 2 );

   p_backup = create_bitmap ( p_width, p_height );

   if ( p_casc_active == true && p_casc_display == true)
   {
      p_casc_level = p_casc_next_level;
      p_casc_next_level++;
      p_x_pos += p_casc_width * p_casc_level;
      p_y_pos += p_casc_width * p_casc_level;
   }


}

/*
WinList::~WinList ( void )
{

} */

/*-------------------------------------------------------------------------*/
/*-                         Virtual Methods                               -*/
/*-------------------------------------------------------------------------*/

void WinList::preshow ( void )
{
   // this function to make class non abstract
}

void WinList::refresh ( void )
{
   // this function does nothing
}

short WinList::show ( void )
{
   short tmpx;
   short tmpy;
   int answer;

   tmpx = p_x_pos + 6;
   tmpy = p_y_pos + 6;

   //if ( p_list->sensible() == false )
      p_inst_order = Window_INSTRUCTION_SELECT +
         Window_INSTRUCTION_MENU + Window_INSTRUCTION_SCROLL;
   //else
      //p_inst_order = Window_INSTRUCTION_CANCEL + Window_INSTRUCTION_MENU;

   if ( p_no_cancel == false)
      p_inst_order += Window_INSTRUCTION_CANCEL;

   border_fill ();
   //if ( p_reset_cursor == true )
   //   p_cursor = 0;

   draw_instruction();
   answer = p_list->show( tmpx, tmpy, p_reset_cursor, p_no_cancel );

   blit ( subscreen, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );

   return ( answer );
}

int WinList::type ( void )
{
   return ( Window_TYPE_LIST );
}



