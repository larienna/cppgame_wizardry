/***************************************************************************/
/*                                                                         */
/*                        W I N I N P U T . C P P                          */
/*                         Class Source COde                               */
/*                                                                         */
/*     Content : Class WinInput source code                                */
/*     Programmer : Eric PIetrocupo                                        */
/*     Starting DAte : July 23rd, 2003                                     */
/*     License : GNU General Public LIcense                                */
/*     Engine : Wizardry window engine                                     */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>



/*
//#include <time.h>
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
//#include <winempty.h>
//#include <wintitle.h>
//#include <winmessa.h>
#include <wininput.h>
*/

/*-------------------------------------------------------------------------*/
/*-                    Constructor and Destructor                         -*/
/*-------------------------------------------------------------------------*/

/*WinInput::WinInput ( string &text, int nb_char, short x_pos, short y_pos )
{
   construct ( text, nb_char, x_pos, y_pos );
} */

WinInput::WinInput ( const char *text, int nb_char, short x_pos, short y_pos, bool translucent )
{
//   string *tmpstr = new string;
//   *tmpstr = text;

   construct ( text, nb_char, x_pos, y_pos );

   p_translucent = translucent;
   if ( p_casc_active == true && p_casc_display == true)
   {
      p_casc_level = p_casc_next_level;
      p_casc_next_level++;
      p_x_pos += p_casc_width * p_casc_level;
      p_y_pos += p_casc_width * p_casc_level;
   }


//   delete tmpstr;
}

/*
WinInput::~WinInput ( void )
{

} */

/*-------------------------------------------------------------------------*/
/*-                          Methods                                      -*/
/*-------------------------------------------------------------------------*/

void WinInput::get_string ( char *copystr )
{
   strcpy ( copystr, p_inputstr );
}

const char *WinInput::get_string ( void )
{
   return ( p_inputstr);
}

/*-------------------------------------------------------------------------*/
/*-                          Virtual Methods                              -*/
/*-------------------------------------------------------------------------*/

void WinInput::preshow ( void )
{
   // This function does nothing
}

void WinInput::refresh ( void )
{
   // This function does nothing
}

short WinInput::show ( void )
{
   short tmpx;
   short tmpy;
   short font_height = text_height ( WinInput_FONT );
   //int casc = 0;

   //if ( p_casc_level > 0 )
   //   casc = p_casc_width * p_casc_level;

   tmpx = p_x_pos + 6;
   tmpy = p_y_pos + 6;

   p_inst_order = Window_INSTRUCTION_INPUT;
   border_fill ();

   draw_instruction();
   textout_old ( subbuffer, WinInput_FONT, p_textstring, tmpx, tmpy,
      WinInput_COLOR );

   textinput ( tmpx, tmpy + font_height, p_inputstr, p_nb_character );

   blit ( subscreen, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );

   return ( 0 );

}

int WinInput::type ( void )
{
   return ( Window_TYPE_INPUT );
}

/*-------------------------------------------------------------------------*/
/*-                          Private Mehtods                              -*/
/*-------------------------------------------------------------------------*/

void WinInput::construct ( const char *text, int nb_char, short x_pos, short y_pos )
{
   int textsize;
   short font_height = text_height ( WinInput_FONT );
   short font_input_height = text_height ( WinInput_INPUTFONT );
   short font_width = text_length ( WinInput_FONT, "a" );

   strncpy ( p_textstring, text, 81 );
   strcpy ( p_inputstr,"" );
   textsize = strlen ( p_textstring );
   p_nb_character = nb_char;

   if ( nb_char > textsize )
      textsize = nb_char;

   p_x_pos = x_pos;
   p_y_pos = y_pos;
   p_width = 6 + ( font_width * textsize ) + 6 + 1 + 8;
   p_height = 6 + ( font_height + font_input_height ) + 6 + 1;
   p_backup = create_bitmap ( p_width, p_height );
}

