/***************************************************************************/
/*                                                                         */
/*                      W I N D I S T R I B . C P P                        */
/*                         Class Definitnon                                */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinDistributor                                      */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting date : May 18th, 2002                                      */
/*                                                                         */
/*          Class who inherint from window to encapsulate distributors     */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>

/*-------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                         -*/
/*-------------------------------------------------------------------------*/

WinDistributor::WinDistributor ( Distributor &dst, int x_pos, int y_pos,
            bool distribute_all, bool no_cancel, bool translucent )
{
   int chr_width;
   //int chr_width2;
   int font_width = text_length ( Distributor_FONT, "a" );
   int font_height = text_height ( Distributor_FONT );
   int header_font_height = text_height ( Distributor_HEADER_FONT );

   p_distributor = &dst;
   p_x_pos = x_pos;
   p_y_pos = y_pos;
   p_no_cancel = no_cancel;
   p_distribute_all = distribute_all;
   //chr_width = p_distributor->char_width();
   chr_width = Distributor_TEXT_SIZE + ( dst.nb_digit() * 3 ) + 5 + 4;
//   chr_width2 = strlen ( p_distributor->title() );
   p_width = 6 + ( font_width *  chr_width  ) + 6 + 1;
   p_height = 6 + ( font_height * ( p_distributor->nb_item() + 2 ) )  + 6 + 1;
   p_height += header_font_height + Distributor_HEADER_SPACE; //header
   p_height += ( font_height / 2 ); // title
   p_translucent = translucent;

   p_backup = create_bitmap ( p_width, p_height );

   if ( p_casc_active == true && p_casc_display == true)
   {
      p_casc_level = p_casc_next_level;
      p_casc_next_level++;
      p_x_pos += p_casc_width * p_casc_level;
      p_y_pos += p_casc_width * p_casc_level;
   }


}

/*
WinDistributor::~WinDistributor ( void )
{

} */

/*-------------------------------------------------------------------------*/
/*-                         Virtual Methods                               -*/
/*-------------------------------------------------------------------------*/

void WinDistributor::preshow ( void )
{
   // this function to make class non abstract
}

void WinDistributor::refresh ( void )
{
   // this function does nothing
}

short WinDistributor::show ( void )
{
   short tmpx;
   short tmpy;
   int answer;


   tmpx = p_x_pos + 6;
   tmpy = p_y_pos + 6;

   //if ( p_list->sensible() == false )
      p_inst_order = Window_INSTRUCTION_ACCEPT +
         Window_INSTRUCTION_MENU + Window_INSTRUCTION_VALUE;
   //else
      //p_inst_order = Window_INSTRUCTION_CANCEL + Window_INSTRUCTION_MENU;

   if ( p_no_cancel == false)
      p_inst_order += Window_INSTRUCTION_CANCEL;

   border_fill ();
   //if ( p_reset_cursor == true )
   //   p_cursor = 0;

   draw_instruction();
   answer = p_distributor->show( tmpx , tmpy , p_distribute_all, p_no_cancel );

   blit ( subscreen, p_backup, p_x_pos , p_y_pos, 0, 0, p_width, p_height );

   return ( answer );
}

int WinDistributor::type ( void )
{
   return ( Window_TYPE_DISTRIBUTOR );
}



