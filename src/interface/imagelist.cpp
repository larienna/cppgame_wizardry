/***************************************************************************/
/*                                                                         */
/*                      I M A G E L I S T . C P P                          */
/*                                                                         */
/*     Content : Class List                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : May 21st, 2013                                      */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>


/*-------------------------------------------------------------------------*/
/*-                  Constructor and Destructor                           -*/
/*-------------------------------------------------------------------------*/


ImageList::ImageList ( void )
{
   ImageList ( "" );
}

ImageList::ImageList ( const char* str , int page_width, int page_height, int pic_width, int pic_height, bool stretch )
{
   int i;

   strncpy ( p_title, str, ImageList_STR_SIZE );
   p_nb_item = 0;
   p_page_width = page_width;
   p_page_height = page_height;
   p_picture_width = pic_width;
   p_picture_height = pic_height;
   p_page_pos = 0;
   p_cursor = 0;
   p_stretch = stretch;

   for ( i = 0; i < ImageList_MAX_NB_ITEM ; i++ )
   {
      p_item [ i ] . answer = -1;
      p_item [ i ] . bmp = NULL;
   }

}

ImageList::~ImageList ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                           Methods                                     -*/
/*-------------------------------------------------------------------------*/

void ImageList::add_item ( int answer, BITMAP* bmp )
{
   if ( p_nb_item <= ImageList_MAX_NB_ITEM )
   {
      p_item [ p_nb_item ] . answer = answer;
      p_item [ p_nb_item ] . bmp = bmp;
      p_nb_item++;
   }
}

void ImageList::add_datref ( DatafileReference<BITMAP*> datref, int index )
{
   int i = index;
   int j = 0;
   //BITMAP* tmpbmp;

   //for ( i = 0 ; i < DatafileReference_NB_DATAFILE ; i++)
   //{
      if ( datref.is_loaded( i ) == true )
      {
         j = 0;

         while ( j < DatafileReference_NB_MAX_OBJECT)
         {
            //tmpbmp = datref.get (i, j);

            if ( datref.eof ( i, j ) == false )
            {
               add_item ( datref.getid ( i, j), datref.get ( i, j) );
               j++;
            }
            else
            {
               //printf ("Debug: ImageList: exits at %d, %d\n", i, j );
               j = DatafileReference_NB_MAX_OBJECT;

            }

         }
      }
   //}


   /*#define DatafileReference_NB_MAX_OBJECT   1024
#define DatafileReference_NB_DATAFILE     16*/
   //Use reference to know the number of object, their primary key and the BMP pointer
}

void ImageList::add_datref ( DatafileReference<BITMAP*> datref )
{
   int i;

   for ( i = 0; i < DatafileReference_NB_DATAFILE; i++)
   {
      add_datref ( datref, i);
   }
}

int ImageList::show ( int x, int y, bool reset_cursor )
{
   int i;
   int font_height = text_height ( ImageList_FONT );
   int base_y = 0;
   //int yinc;
   int tmpx;
   int tmpy;
   int tmpw;
   int tmph;
   bool answer_found = false;
   //bool title = false;

   bool scroll = true; // set to true to make sure the picture get displayed once
   bool scrollup = false;
   bool scrolldown = false;
   int page_size = p_page_width * p_page_height;
   int item_count = 0;
   //int cursor_color;
   int page_backup;
   //int cursor_backup;
   //int page_pos_backup;
   int finalw;
   int finalh;

   char kbvalue;

   int tmpanswer;// = cursor_pos;

   bool rereadkb = false;

   char shotstr [ 18 ];
   static int shotID = 0;

   //printf ("ImageList.show: Pass 1\n" );

   if ( reset_cursor == true )
   {
      p_page_pos = 0;
      p_cursor = 0;
   }


   //cursor_backup = p_cursor;



   //page_pos_backup = p_page_pos;
   clear_keybuf();

  // printf ("ImageList.show: Pass 2\n" );

   while ( answer_found == false )
   {

      if ( p_page_pos >= p_nb_item )
      {
      // safety in case of bugs
         p_cursor = 0;
         p_page_pos = 0;
      }

      // adjust for page up and page down
      if ( p_cursor < p_page_pos )
         p_cursor = p_page_pos;

      if ( p_cursor > p_page_pos + page_size )
         p_page_pos = p_cursor;


      //Display the pictures if the page has scrolled

      if ( scroll == true )
      {
        // printf ("ImageList.show: Pass 3\n" );
         scroll = false;
         blit ( buffer, backup, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
         // draw or skip title
         if ( strcmp ( p_title, "" ) != 0 )
         {
            textprintf_old ( subbackup, ImageList_FONT, x, y, makecol (255, 255, 255),
               "%s", p_title );
            base_y = y + font_height;
            //title = true;
         }
         else
            base_y = y;

         item_count = 0;
         scrollup = false;
         scrolldown = false;

         base_y += 8; // height of the scroll arrow

        // printf ("ImageList.show: Pass 4:\n" );

         i = p_page_pos;
         item_count = 0;
         while ( i < p_page_pos + page_size && i < p_nb_item )
         {
            //printf ("ImageList.show: Pass 4.1: i = %d, ppos = %d, p_size=%d, nb_item=%d \n"
            //        , i, p_page_pos, page_size, p_nb_item );



            //printf ("List test: %s", p_item [ i ] . text );

            tmpx = get_x_pos ( item_count ) + 2 + x;
            tmpy = get_y_pos ( item_count ) + 2 + base_y;
            tmpw = p_item [ i ] . bmp -> w;
            tmph = p_item [ i ] . bmp -> h;

           // printf ( "ImageList.show: pass 4.2: tmpx=%d, tmpy=%d\n", tmpx, tmpy );

            //printf ("ImageList.show: Before blit\n" );

            if ( tmpw <= p_picture_width
                &&  tmph <= p_picture_height )
            {

               if ( tmpw != p_picture_width
                  ||  tmph != p_picture_height)
                  rectfill ( subbackup, tmpx, tmpy, tmpx + p_picture_width,
                             tmpy + p_picture_height, makecol (0, 0, 0));

               blit ( p_item [ i ] . bmp, subbackup, 0, 0, tmpx, tmpy
                  ,tmpw, tmph );
               //printf ("ImageList.show: after regular blit\n" );
            }
            else
            {
               //to do, stretch to /2 increments, or keep proportions (still need something fast)

               if ( p_stretch == true )
               {
                  int ratiow = 100;
                  int ratioh = 100;
                  int finalratio = 100;

                  if ( tmpw > p_picture_width )
                     ratiow = ( p_picture_width * 100 ) / tmpw;
                  if ( tmph > p_picture_height )
                     ratioh = ( p_picture_height * 100 ) / tmph;

                  if ( ratiow > ratioh)
                     finalratio = ratioh;
                  else
                     finalratio = ratiow;

                  finalw = ( tmpw * finalratio ) / 100;
                  finalh = ( tmph * finalratio ) / 100;

                  //printf ( "tmpw=%d, tmph=%d, ratw=%d, rath=%d, frat=%d, finw=%d, finh=%d\n",
                  //        tmpw, tmph, ratiow, ratioh, finalratio, finalw, finalh );

                  if ( finalw != p_picture_width
                  ||  finalh != p_picture_height)
                      rectfill ( subbackup, tmpx, tmpy, tmpx + p_picture_width,
                             tmpy + p_picture_height, makecol (0, 0, 0));


                  stretch_blit ( p_item [ i ] . bmp, subbackup, 0, 0, tmpw, tmph, tmpx, tmpy
                     ,finalw, finalh );

                  tmpx = tmpx + p_picture_width - 2;
                  tmpy = tmpy + p_picture_height - 2;

                  triangle( subbackup, tmpx, tmpy, tmpx-4, tmpy, tmpx, tmpy-4, makecol ( 255, 255, 255 )  );

               }
               else
               {


                  if ( tmpw > p_picture_width )
                     finalw = p_picture_width;
                  else
                     finalw = tmpw;

                  if ( tmph > p_picture_height )
                     finalh = p_picture_height;
                  else
                     finalh = tmph;

                  blit ( p_item [ i ] . bmp, subbackup, 0, 0, tmpx, tmpy, finalw, finalh );
               }
                  //printf ("ImageList.show: after stretch blit\n" );
            }

            /*
            void blit(BITMAP *source, BITMAP *dest, int source_x, int source_y,
          int dest_x, int dest_y, int width, int height);

            void stretch_blit(BITMAP *source, BITMAP *dest,
                  int source_x, source_y, source_width, source_height,
                  int dest_x, dest_y, dest_width, dest_height);
*/

            item_count++;
            i++;
         }

         //printf ("ImageList.show: Pass 5\n" );

      }



      // display + or - for scorlling. Maybe use wide triangle arrows 32 x 8
      tmpx = x +  ( p_page_width * ( p_picture_width +4 ) / 2);

      if ( p_page_pos > 0 )
      {
               //textprintf_old ( subbackup, ImageList_FONT, x, yinc, clr, "  -%s",
               //   p_item [ i ] . text );

         tmpy = y + font_height;

         triangle( subbackup, tmpx, tmpy, tmpx+16, tmpy+7, tmpx-16, tmpy+7, makecol ( 255, 255, 255));

         scrollup = true;
      }
      if ( /*item_count == ( p_page_size - 1 ) && */
         ( p_page_pos + page_size ) < ( p_nb_item ) )
      {
                  //textprintf_old ( subbackup, List_FONT, x, yinc, clr, "  +%s",
                     //p_item [ i ] . text );
         tmpy = y + font_height + 8 + (  p_page_height * ( p_picture_height +4 ) );

         triangle( subbackup, tmpx, tmpy+8, tmpx+16, tmpy+1, tmpx-16, tmpy+1, makecol ( 255, 255, 255));

         scrolldown = true;
      }



      copy_backup_buffer_keep();
      //copy_buffer();

      //yinc = base_y;
      //item_count = 0;

      //i = p_page_pos;

      // drawing cursor ( and drawing over previous cursor

//printf ("ImageList.show: Pass 6\n" );

      //while ( i < p_page_pos + page_size && i < p_nb_item )
      //{

         // erase old cursor

         /*tmpx = get_x_pos ( cursor_backup - page_pos_backup );
         tmpy = get_y_pos ( cursor_backup - page_pos_backup );

         rect ( subscreen, tmpx + x, tmpy + base_y, tmpx + p_picture_width + x + 4,
               tmpy + p_picture_height + base_y + 4, makecol (0, 0, 0));*/

         // draw cursor
         tmpx = get_x_pos ( p_cursor - p_page_pos );
         tmpy = get_y_pos ( p_cursor - p_page_pos );

         tmpanswer = p_item [ p_cursor ] . answer;
         rect ( subscreen, tmpx + x, tmpy + base_y, tmpx + p_picture_width + x + 4,
               tmpy + p_picture_height + base_y + 4, makecol (255, 255, 255));

         //if ( item_count + p_page_pos == p_cursor )
         //{
           // cursor_color = makecol ( 255, 255, 255 );


         //}
         //else
         //   cursor_color = makecol ( 0, 0, 0 );




         //yinc = yinc + font_height;
         //item_count++;
        // i++;
      //}
      //cursor_backup = p_cursor;
      //page_pos_backup = p_page_pos;
//printf ("ImageList.show: Pass 7\n" );


      // Keyboard reading

      rereadkb = true;

      while ( rereadkb == true )
      {
         kbvalue = mainloop_readkeyboard();
         clear_keybuf();

//printf ("ImageList.show: Pass 8\n" );
         if ( kbvalue == -1)
            rereadkb = false;
         else
            rereadkb = true;


         if ( kbvalue == SELECT_KEY )
         {
            rereadkb = false;
            answer_found = true;
         }

         if ( kbvalue == CANCEL_KEY )
         {

            tmpanswer = -1;
            rereadkb = false;
            answer_found = true;

         }

         if ( kbvalue == KEY_UP )
         {
            if ( p_cursor >= p_page_width )
            {
               p_cursor -= p_page_width;
               rereadkb = false;
               if ( p_cursor < p_page_pos )
               {
                  p_page_pos -= p_page_width;
                  scroll = true;
               }
               /*if ( p_sensible == true )
               {
                  tmpanswer = p_item [ p_cursor ] . answer;
                  rereadkb = false;
                  answer_found = true;
               }*/
            }
         }

         if ( kbvalue == KEY_LEFT )
         {
            if ( p_cursor > 0  && p_cursor % p_page_width != 0)
            {
               p_cursor -= 1;
               rereadkb = false;
               if ( p_cursor < p_page_pos )
               {
                  p_page_pos -= p_page_width;
                  scroll = true;
               }
               /*if ( p_sensible == true )
               {
                  tmpanswer = p_item [ p_cursor ] . answer;
                  rereadkb = false;
                  answer_found = true;
               }*/
            }
         }

         if ( kbvalue == KEY_DOWN )
         {
            if ( p_cursor < ( p_nb_item ) - p_page_width )

               //printf ("Keymove debug: cursor=%d, page_width = %d\n", p_cursor, p_page, )
               p_cursor += p_page_width;
            else
               p_cursor = p_nb_item -1;

            rereadkb = false;
            if ( p_cursor > ( p_page_pos + page_size - 1 ) )
            {
               p_page_pos += p_page_width;
               scroll = true;
            }

               /*if ( p_sensible == true )
               {
                  tmpanswer = p_item [ p_cursor ] . answer;
                  rereadkb = false;
                  answer_found = true;
               }*/

         }

         if ( kbvalue == KEY_RIGHT )
         {
            if ( p_cursor < ( p_nb_item -1 ) && p_cursor % p_page_width != p_page_width -1 )
            {
               //printf ("Keymove debug: cursor=%d, page_width = %d\n", p_cursor, p_page, )
               p_cursor += 1;
               rereadkb = false;
               if ( p_cursor > ( p_page_pos + page_size - 1 ) )
               {
                  p_page_pos += p_page_width;
                  scroll = true;
               }

               /*if ( p_sensible == true )
               {
                  tmpanswer = p_item [ p_cursor ] . answer;
                  rereadkb = false;
                  answer_found = true;
               }*/
            }
         }


         if ( kbvalue == KEY_PGDN )
         {
            if ( scrolldown == true)
            {
               rereadkb = false;

               if ( p_page_pos < p_nb_item - page_size)
               {
                  scroll = true;
                  page_backup = p_page_pos;
                  p_page_pos += page_size;
                  //p_cursor += p_page_size -1;

                  if ( p_page_pos > p_nb_item - page_size)
                     p_page_pos = p_nb_item - page_size;

                  p_cursor += p_page_pos - page_backup;

                  if ( p_cursor >= p_nb_item )
                     p_cursor = p_nb_item -1;
               }
            }
         }

         if ( kbvalue == KEY_PGUP )
         {
            if ( scrollup == true)
            {
               rereadkb = false;

               if ( p_page_pos > 0)
               {
                  scroll = true;
                  page_backup = p_page_pos;
                  p_page_pos -= ( page_size);
                  //p_cursor -= ( p_page_size - 1);

                  if ( p_page_pos < 0)
                     p_page_pos = 0;

                  p_cursor += p_page_pos - page_backup;

                  if ( p_cursor < 0 )
                     p_cursor = 0;
               }
            }
         }

         if ( kbvalue == KEY_SPACE )
         {
            sprintf ( shotstr, "imglistshot%03d.bmp", shotID);
            make_screen_shot ( shotstr  );
            shotID++;
         }

      }
   }

   //destroy_bitmap ( backbuf );
   //p_page_pos = page;
   clear ( buffer );
   return ( tmpanswer );





}

int ImageList::get_x_pos ( int item )
{
   int xloc;

   xloc = item % p_page_width;

   return ( xloc * ( p_picture_width + 4 ) );
}

int ImageList::get_y_pos ( int item )
{
   int yloc;
   int i;

   i = p_page_width;
   yloc=0;
   while ( i <= item )
   {
      yloc++;
      i = i + p_page_width;
   }

//printf ("ImageList.show: after get_y_pos\n" );

   return ( yloc * ( p_picture_height + 4 ) );
}

