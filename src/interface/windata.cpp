/***************************************************************************/
/*                                                                         */
/*                        W I N D A T A . C P P                            */
/*                          Class Definition                               */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinData                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 13th, 2002                                 */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                          Includes                                     -*/
/*-------------------------------------------------------------------------*/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>



/*
//#include <time.h>
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>
#include <sqlite3.h>
#include <sqlobject.h>



//
//
//
//
//
//#include <list.h>

#include <opponent.h>
#include <charactr.h>
//#include <monster.h>
#include <party.h>

#include <game.h>

//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
//#include <winempty.h>
//#include <wintitle.h>
//#include <winmenu.h>
#include <windata.h>
*/

/*-------------------------------------------------------------------------*/
/*-                    Constructor & Destructor                           -*/
/*-------------------------------------------------------------------------*/

template<class t_datatype>WinData<t_datatype>::
WinData ( void (*proc)( t_datatype&, short, short ), t_datatype &param,
                     short x_pos, short y_pos, short width, short height, bool translucent )
{
   p_parameter = &param;
   p_procedure = proc;
   p_x_pos = x_pos;
   p_y_pos = y_pos;
   p_height = height;
   p_width = width;
   p_backup = create_bitmap ( p_width, p_height );
   p_usetypeint = false;
   p_translucent = translucent;
   preshow();
}
/*
template<class t_datatype>WinData<t_datatype>::
~WinData ( void )
{

} */

template<class t_datatype>WinData<t_datatype>::
WinData ( void (*proc)( int, int, int ),
                     int key, int x_pos, int y_pos, int width,
                     int height, bool translucent )
{

   //p_parameter = &param;
   p_key = key;
   p_procedure_i = proc;
   p_x_pos = x_pos;
   p_y_pos = y_pos;
   p_height = height;
   p_width = width;
   p_backup = create_bitmap ( p_width, p_height );
   p_usetypeint = true;
   p_translucent = translucent;
   preshow();

}

/*-------------------------------------------------------------------------*/
/*-                       Property Methods                                -*/
/*-------------------------------------------------------------------------*/

template<class t_datatype> void WinData<t_datatype>::parameter ( t_datatype &param )
{
   p_parameter = &param;
}

template<class t_datatype> void WinData<t_datatype>::key ( int key )
{
   p_key = key;
}

/*-------------------------------------------------------------------------*/
/*-                        Virtual Functions                              -*/
/*-------------------------------------------------------------------------*/

template<class t_datatype> void WinData<t_datatype>::preshow ( void )
{
   clear_to_color (subbuffer, makecol ( 255, 0, 255) );
   WinData::show();
   clear( buffer );
}

template<class t_datatype> void WinData<t_datatype>::refresh ( void )
{
   clear_to_color (subbuffer, makecol ( 255, 0, 255) );
   WinData::show();
   clear( buffer );
}

template<class t_datatype> short WinData<t_datatype>::show ( void )
{

   border_fill ();
   if ( p_usetypeint == true)
      p_procedure_i (p_key, p_x_pos, p_y_pos );
   else
      p_procedure ( *p_parameter, p_x_pos, p_y_pos );
   blit ( subbuffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );
   draw_instruction();
   return (0);
}

template<class t_datatype> int WinData<t_datatype>::type ( void )
{
   return ( Window_TYPE_DATA );
}

/*-------------------------------------------------------------------------*/
/*-                            Templates                                  -*/
/*-------------------------------------------------------------------------*/

template class WinData<Character>;
template class WinData<Party>;
//template class WinData<Item>;
template class WinData<BITMAP>;
template class WinData<int>;
//template WinData<int>;
//template class WinData<Player>;
//template class WinData<Account>;
//template class WinData<Game>;
//template class WinData<Race>;
//template class WinData<Opponent>;
