/***************************************************************************/
/*                                                                         */
/*                         M E N U . C P P                                 */
/*                                                                         */
/*     Content : Class Menu                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : March 14th , 2002                                   */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>



/*
//#include <time.h>
#include <allegro.h>
//#include <allegrowrapper.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>



#include <ddt.h>
//#include <dbdef.h>
//#include <memory>
*/


/*-------------------------------------------------------------------------*/
/*-                  Constructor and Destructor                           -*/
/*-------------------------------------------------------------------------*/

Menu::Menu ( void )
{
   strcpy ( p_title, "" );
//   p_font = font;
   p_nb_item = 0;
   ptr_first = NULL;
   ptr_last = NULL;
   strncpy ( p_header, "" , Menu_HEADER_STR_SIZE );
   p_showheader = false;
   p_cursor = 0;
}

Menu::Menu ( const char* str )
{
//   string *tmp = new string;

//   *tmp = dat;

   strncpy ( p_title, str , Menu_STR_SIZE );
//   p_font = fnt;
   p_nb_item = 0;
   ptr_first = NULL;
   ptr_last = NULL;
   strncpy ( p_header, "" , Menu_HEADER_STR_SIZE );
   p_showheader = false;
   p_cursor = 0;
//   delete tmp;

}
/*Menu::Menu ( string &str )
{
   p_title = str;
//   p_font = fnt;
   p_nb_item = 0;
   ptr_first = NULL;
   ptr_last = NULL;
} */

Menu::~Menu ( void )
{
   s_Menu_item *tmp_ptr;
   s_Menu_item *del_ptr;

   tmp_ptr = ptr_first;

   while ( tmp_ptr != NULL )
   {
      del_ptr = tmp_ptr;
      tmp_ptr = tmp_ptr -> ptr_next;
      free ( del_ptr );
   }

}

/*-------------------------------------------------------------------------*/
/*-                         Property Methods                              -*/
/*-------------------------------------------------------------------------*/


/*void Menu::title ( string &str )
{
   p_title = str;
} */

void Menu::title ( const char* str )
{
//   string *tmp = new string;

//   *tmp= dat ;

   strncpy ( p_title, str, Menu_STR_SIZE );

//   delete tmp;
}

const char* Menu::title ( void )
{
   return ( p_title );
}

int Menu::nb_item ()
{
   return ( p_nb_item );
}

/*void Menu::textfont ( FONT *fnt )
{
   p_font = fnt;
}*/

short Menu::char_width ( void )
{
   short max_width = strlen ( p_title );
   short tmp_width;
   s_Menu_item *tmp_ptr;

   tmp_ptr = ptr_first;

   while ( tmp_ptr != NULL )
   {
      tmp_width = strlen ( tmp_ptr->text );
      if ( tmp_width > max_width )
         max_width = tmp_width;

      tmp_ptr = tmp_ptr -> ptr_next;
   }

   max_width = max_width + 3;

   return ( max_width );

}

void Menu::header ( const char *text )
{
   p_showheader = true;
   strncpy ( p_header, text, Menu_HEADER_STR_SIZE);
}

bool Menu::showheader ( void )
{
    return ( p_showheader );
}


/*-------------------------------------------------------------------------*/
/*-                              Methods                                  -*/
/*-------------------------------------------------------------------------*/

void Menu::add_item ( int answer, const char* str, bool mask )
{

   s_Menu_item *ptr_new_item;

   ptr_new_item = new s_Menu_item;
//   ptr_new_item = (s_Menu_item*) malloc ( sizeof (s_Menu_item) );

   strncpy ( ptr_new_item -> text, str, Menu_STR_SIZE );
   ptr_new_item -> mask = mask;
   ptr_new_item -> answer = answer;

   if ( p_nb_item == 0 )
   {
      // insert the first item
      ptr_first = ptr_new_item;
      ptr_last = ptr_new_item;
      ptr_new_item -> ptr_next = NULL;
   }
   else
   {
      // add item at the end of the list
      ptr_last -> ptr_next = ptr_new_item;
      ptr_last = ptr_new_item;
      ptr_new_item -> ptr_next = NULL;
   }

   p_nb_item++;

}

void Menu::add_itemf ( int answer, const char* format,  ... )
{
   va_list arguments;
   //int i;
   char tmpstr[Menu_STR_SIZE];


   va_start (arguments, format);
   vsprintf (tmpstr,format, arguments);
   va_end (arguments);

   add_item(answer, tmpstr);

   //va_start (arguments, nb_arg );

   //for ( i=0; i < nb_arg; i++ )
   //{

      //do stuff va_arg ( arguments, datatype);
   //}

   //va_end();

}

/*void Menu::add_item ( const char* dat, bool mask)
{
//   string* tmp = new string;

//   *tmp = dat;

   add_item ( dat, mask );
//   delete tmp;
} */

void Menu::mask_item ( int ID )
{
//note: the ID is used to track the entry, not the answer
   s_Menu_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != ID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

    if ( count == ID )
         tmp_ptr -> mask = true;

}

void Menu::mask_answer ( int answer )
{
//note: the ID is used to track the entry, not the answer
   s_Menu_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( tmp_ptr != NULL )
   {
      if ( tmp_ptr->answer == answer)
         tmp_ptr -> mask = true;

      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }
}

void Menu::unmask_all_item ( void )
{
   s_Menu_item *tmp_ptr;

   tmp_ptr = ptr_first;

   while ( tmp_ptr != NULL )
   {
      tmp_ptr -> mask = FALSE;

      tmp_ptr = tmp_ptr -> ptr_next;
   }
}

short Menu::show ( short x, short y, bool nocancel, bool reset_cursor )
{
   int font_height = text_height ( Menu_FONT );
   int space = font_height / 2;
   int header_font_height = text_height ( Menu_HEADER_FONT );
   int arrow_space = text_length ( Menu_FONT, "-> ");
   s_Menu_item *tmp_ptr;
   //int cursor = position;
   int item_count = 0;
//   String tmp_str;
   char kbvalue;
   short base_y;
   short yinc;
   //int answer = position;
   int tmpanswer = -1;
   bool answer_found = false;
   bool rereadkb = false;
   bool selected_masked = false;
   int clr = Menu_COLOR_TEXT;
   bool title = false;
//   string tmpstr;

   if ( p_cursor < 0 )
      p_cursor = 0;

   if ( reset_cursor == true )
      p_cursor = 0;

   if (p_cursor >= p_nb_item )
      p_cursor = p_nb_item - 1;

   char shotstr [ 18 ]; /*= { "mnushot1.bmp", "mnushot2.bmp", "mnushot3.bmp",
      "mnushot4.bmp", "mnushot5.bmp", "mnushot6.bmp", "mnushot7.bmp", "mnushot8.bmp" };*/
   static int shotID = 0;


   if ( strcmp ( p_title, "" ) != 0 )
   {
      textprintf_old ( subbuffer , Menu_FONT, x, y, Menu_COLOR_TEXT,
         "   %s", p_title );
      base_y = y + font_height;
      title = true;
   }
   else
      base_y = y;

   if ( p_showheader == true)
   {
      textout_old ( subbuffer, Menu_HEADER_FONT, p_header, x + arrow_space, base_y, Menu_COLOR_TEXT );
      base_y += header_font_height + Menu_HEADER_SPACE;
   }

   if ( title == true)
      base_y += space;

   yinc = base_y;
   tmp_ptr = ptr_first;
   item_count = 0;
   selected_masked = false;

   while ( tmp_ptr != NULL )
   {
      if ( tmp_ptr -> mask == true )
         clr = Menu_COLOR_MASK;
      else
         clr = Menu_COLOR_TEXT;

//      if ( item_count == cursor )
//      {
//            textprintf_old ( subscreen, Menu_FONT, x, yinc, clr, "-> %s",
//               tmp_ptr -> text.data() );
//            selected_masked = tmp_ptr->mask;
//         }
//         else
//         {
      textprintf_old ( subbuffer, Menu_FONT, x, yinc, clr, "   %s",
         tmp_ptr -> text );
//         }

      yinc = yinc + font_height;
//         item_count++;
      tmp_ptr = tmp_ptr -> ptr_next;
   }

   clear_keybuf();

   clr = Menu_COLOR_TEXT;
   while ( answer_found == false )
   {
      yinc = base_y;
      tmp_ptr = ptr_first;
      item_count = 0;
      selected_masked = false;

      //vsync ();
      //blit ( buffer, screen , 0 , 0 , 0 , 0 , 640 , 480 );
      copy_buffer_keep();

      while ( tmp_ptr != NULL )
      {
//         if ( tmp_ptr -> mask == true )
//            clr = Menu_COLOR_MASK;
//         else
//            clr = Menu_COLOR_TEXT;

         if ( item_count == p_cursor )
         {
            textprintf_old ( subscreen, Menu_FONT, x, yinc, clr, "->" );
            selected_masked = tmp_ptr->mask;
            tmpanswer = tmp_ptr->answer;
         }
//         else
//         {
//            textprintf_old ( subscreen, Menu_FONT, x, yinc, clr, "   %s",
//               tmp_ptr -> text.data() );
//         }

         yinc = yinc + font_height;
         item_count++;
         tmp_ptr = tmp_ptr -> ptr_next;
      }

      // Keyboard reading

      rereadkb = true;

      while ( rereadkb == true )
      {
         kbvalue = mainloop_readkeyboard();
         clear_keybuf();

         if ( kbvalue == -1 )
            rereadkb = false;
         else
            rereadkb = true;

         if ( kbvalue == SELECT_KEY )
         {
            if ( selected_masked == false )
            {
               //answer = get_answer (cursor); //use tmpanswer
               rereadkb = false;
               answer_found = true;
            }
         }

         if ( kbvalue == CANCEL_KEY )
         {
            if ( nocancel == true )
               rereadkb = true;
            else
            {
               tmpanswer = -1;
               rereadkb = false;
               answer_found = true;
            }
         }

         if ( kbvalue == KEY_UP )
         {
            if ( p_cursor > 0 )
            {
               p_cursor--;
               rereadkb = false;
            }
         }

         if ( kbvalue == KEY_DOWN )
         {
            if ( p_cursor < ( p_nb_item - 1) )
            {
               p_cursor++;
               rereadkb = false;
            }
         }

         if ( kbvalue == KEY_SPACE )
         {
            sprintf ( shotstr, "menushot%03d.bmp", shotID);
            make_screen_shot ( shotstr );
            shotID++;
         }
      }
   }
//?? implement masking

   clear ( buffer );
   return ( tmpanswer );
}


void Menu::draw ( short x, short y )
{
   int font_height = text_height ( Menu_FONT );
   int space = font_height / 2;
   s_Menu_item *tmp_ptr;
   short base_y;

   if ( strcmp ( p_title, "") != 0 )
   {
      textprintf_old ( subbuffer , Menu_FONT, x, y, Menu_COLOR_TEXT,
         "   %s", p_title );
      base_y = y + font_height + space;
   }
   else
      base_y = y;


   tmp_ptr = ptr_first;
   y = base_y;

   while ( tmp_ptr != NULL )
   {
      textprintf_old ( subbuffer, Menu_FONT, x, y, Menu_COLOR_TEXT, "   %s",
         tmp_ptr -> text );

      y = y + font_height;
      tmp_ptr = tmp_ptr -> ptr_next;
   }

   vsync ();
   blit ( buffer, screen , 0 , 0 , 0 , 0 , SCREEN_W, SCREEN_H );

}

/*-------------------------------------------------------------------------------*/
/*-                             SQLite Methods                                  -*/
/*-------------------------------------------------------------------------------*/

// add all the results of a query, using only column 0.
void Menu::add_query ( const char* field, const char* table, const char* condition )
{

   //sqlite3_stmt *sqlstatement;
   //const char *sqltail;
   int error;
   char tmpstr[81];
   char querystr[SQL_QUERYSTR_LEN];
   //unsigned char *tmpstr2;

   //printf("debug: 1\r\n");
   //error = sqlite3_prepare_v2 (SQLobject::p_sqldb, querystr, -1, &sqlstatement, &sqltail);
   sprintf (querystr, "SELECT %s,%s FROM %s %s", SQL_PRIMARY_KEY, field, table, condition);
   error = SQLprepare (querystr);

   //printf("debug: 2\r\n");

   if (error == SQLITE_OK )
   {
      //error = sqlite3_step (sqlstatement);
      error = SQLstep ();

      if (error != SQLITE_ROW)
         SQLerrormsg ();
         //printf("\r\nError: Menu::add_query: SQLsteps: %s\r\n", error, sqlite3_errmsg(SQLobject::p_sqldb));

      while (error == SQLITE_ROW)
      {
        // printf ("debug: 3");
         strncpy ( tmpstr, SQLcolumn_text (1), Menu_STR_SIZE);
         add_item (SQLcolumn_int (0), tmpstr);
         error = SQLstep();
      }

      error = SQLfinalize();
      //printf ("debug: 4");
      if ( error != SQLITE_OK)
         SQLerrormsg();
        // printf("Error: Menu::add_query: sqlite: %s\r\n", error, sqlite3_errmsg(SQLobject::p_sqldb));
   }
   else
   {

      //printf ("debug: 2");
      SQLerrormsg();
   }
   //else
     // printf("Error: Menu::add_query: sqlite: %s\r\n", error, sqlite3_errmsg(SQLobject::p_sqldb));

}

// add the first result of a query, and use column 0
void Menu::add_query_item ( const char* field, const char* table, int key )
{

   //sqlite3_stmt *sqlstatement;
   //const char *sqltail;
   int error;
   char tmpstr[81];
   //unsigned char *tmpstr2;
   char querystr[SQL_QUERYSTR_LEN];

   sprintf (querystr, "SELECT %s,%s FROM %s WHERE %s=%d", SQL_PRIMARY_KEY, field, table, SQL_PRIMARY_KEY, key);
   //printf("debug: 1\r\n");
//   error = sqlite3_prepare_v2 (SQLobject::p_sqldb, querystr, -1, &sqlstatement, &sqltail);

   error = SQLprepare (querystr);
   //error = SQLprepare ("SELECT name FROM race WHERE pk=1");

   //printf("debug: 2\r\n");

   if (error == SQLITE_OK )
   {
      error = SQLstep();

      //error = sqlite3_step (SQLstatement);

      //printf("\r\nError: Menu::add_query: sqlite: %s\r\n", error, sqlite3_errmsg(qldb));

      //printf("\r\nMenu::add_query_item: tmpstr = %s", SQLcolumn_text(1));
      //   printf("\r\nMenu::add_query_item: tmpstr = %s", sqlite3_column_text (SQLstatement, 1));

      if (error == SQLITE_ROW)
      {
      //   printf("\r\nMenu::add_query_item: tmpstr = %s", SQLcolumn_text(1));
      //   printf("\r\nMenu::add_query_item: tmpstr = %s", sqlite3_column_text (SQLstatement, 1));

         strncpy ( tmpstr, SQLcolumn_text(1), Menu_STR_SIZE);
         add_item (key, tmpstr);
         //error = sqlite3_step (sqlstatement);
      }
      //else
        // SQLerrormsg();

      SQLfinalize();
      //error = sqlite3_finalize (sqlstatement);
      if ( error != SQLITE_OK)
         SQLerrormsg();
        // printf("Error: Menu::add_query: sqlite: %s\r\n", error, sqlite3_errmsg(SQLobject::p_sqldb));
   }
   else
      SQLerrormsg();
   //else
     // printf("Error: Menu::add_query: sqlite: %s\r\n", error, sqlite3_errmsg(SQLobject::p_sqldb));

}

/*int Menu::get_answer ( int ID )
{
   s_Menu_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != ID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

    if ( count == ID )
         return ( tmp_ptr -> answer);
    else
       return (-1);

}
*/
