/***************************************************************************/
/*                                                                         */
/*                       W I N I M G L I S T . C P P                       */
/*                        Class Source Code                                */
/*                                                                         */
/*     Content : Class WinImageList                                        */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : May, 25th, 2013                                     */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>




/*-------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                         -*/
/*-------------------------------------------------------------------------*/

WinImageList::WinImageList ( ImageList &lst, short x_pos, short y_pos,
             bool reset_cursor, bool translucent )
{
   //short chr_width;
   //int font_width = text_length ( List_FONT, "a" );
   int font_height = text_height ( List_FONT );
   //int header_font_height = text_height ( List_HEADER_FONT );

   p_list = &lst;
   p_x_pos = x_pos;
   p_y_pos = y_pos;
   //p_no_cancel = no_cancel;
   p_reset_cursor = reset_cursor;
   //chr_width = p_list->char_width();
   p_width = 6 + ( ( lst.picture_width() + 4 )  * lst.page_width() ) + 6 + 1;
   p_height = 6 + ( ( lst.picture_height() + 4 )  * lst.page_height() )+ 6 + 1 + 16 /*arrows*/;
   p_translucent = translucent;

   /*if ( p_list->showheader() == true)
   {
      p_height += header_font_height + List_HEADER_SPACE;
   }*/
//   p_cursor = 0;

//   p_list->sensible ( sensible );

   if ( strcmp ( p_list->title(),  "" ) != 0 )
      p_height = p_height + font_height /*+ ( font_height / 2 )*/;

   p_backup = create_bitmap ( p_width, p_height );

   if ( p_casc_active == true && p_casc_display == true)
   {
      p_casc_level = p_casc_next_level;
      p_casc_next_level++;
      p_x_pos += p_casc_width * p_casc_level;
      p_y_pos += p_casc_width * p_casc_level;
   }


}

/*
WinList::~WinList ( void )
{

} */

/*-------------------------------------------------------------------------*/
/*-                         Virtual Methods                               -*/
/*-------------------------------------------------------------------------*/

void WinImageList::preshow ( void )
{
   // this function to make class non abstract
}

void WinImageList::refresh ( void )
{
   // this function does nothing
}

short WinImageList::show ( void )
{
   short tmpx;
   short tmpy;
   int answer;

   tmpx = p_x_pos + 6;
   tmpy = p_y_pos + 6;

   //if ( p_list->sensible() == false )
      p_inst_order = Window_INSTRUCTION_SELECT +
         Window_INSTRUCTION_MENU + Window_INSTRUCTION_SCROLL;
   //else
      //p_inst_order = Window_INSTRUCTION_CANCEL + Window_INSTRUCTION_MENU;

   //if ( p_no_cancel == false)
   //   p_inst_order += Window_INSTRUCTION_CANCEL;

   border_fill ();
   //if ( p_reset_cursor == true )
   //   p_cursor = 0;

   draw_instruction();
   answer = p_list->show( tmpx, tmpy, p_reset_cursor );

   blit ( subscreen, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );

   return ( answer );
}

int WinImageList::type ( void )
{
   return ( Window_TYPE_IMGLIST );
}



