/***************************************************************************/
/*                                                                         */
/*                        W I N D O W . C P P                              */
/*                          Class Source code                              */
/*                                                                         */
/*     Content : Class Window                                              */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : November 4th, 2002                                  */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>

#include <config.h>


/*
//#include <time.h>
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
*/

/*-------------------------------------------------------------------------*/
/*-                         Class Variable                                -*/
/*-------------------------------------------------------------------------*/

Window *Window::p_ptr_last = NULL;
Window *Window::p_ptr_first = NULL;
int Window::p_nb_open = 0;
int Window::p_border_type = Window_BORDER_THICK;
int Window::p_bg_mode = Window_BACKGROUND_TEXTURE;
int Window::p_bg_color = Window_COLOR_BLACK;
int Window::p_bg_texture = Window_TEXTURE_SLATE;
int Window::p_bg_trans_level = Config_TRANSLUCENCY_LEVEL [ Config_WTL_MED ];

bool Window::p_show_party_frame = false;

int Window::p_inst_order = -1;
short Window::p_inst_center_x = 320;
short Window::p_inst_y = 460;
const char *Window::p_inst_string = "";

// cascading menu variables
int Window::p_casc_next_level = 0; // contains the level of the next cascading level.
bool Window::p_casc_display = true; // status to activate or deactivate cascade view only
bool Window::p_casc_active = false; // status to add levels to windows or not
int Window::p_casc_width = 24; // size in pixel of the cascade

/*-------------------------------------------------------------------------*/
/*-                     Constructor & Destructor                          -*/
/*-------------------------------------------------------------------------*/

Window::Window ( void )
{
/*   p_width = 0;
   p_height = 0;
   p_x_pos = 0;
   p_y_pos = 0;
   p_backup = NULL;*/
   //int have_input = false;

   p_hidden = false;


   if ( p_nb_open == 0 )
   {
      p_ptr_first = this;
      p_ptr_last = this;
      p_ptr_next = NULL;
      p_ptr_previous = NULL;
   }
   else
   {
      p_ptr_last->p_ptr_next = this;
      p_ptr_previous = p_ptr_last;
      p_ptr_last = this;
      p_ptr_next = NULL;
   }


   p_casc_level = -1;
   p_translucent = false;


   p_nb_open++;
}

Window::~Window ( void )
{
   destroy_bitmap ( p_backup );

   if ( p_nb_open <= 1 )
   {
      p_ptr_previous = NULL;
      p_ptr_next = NULL;
      p_ptr_first = NULL;
      p_ptr_last = NULL;
   }
   else
   {
      if ( p_ptr_previous != NULL )
         p_ptr_previous->p_ptr_next = p_ptr_next;
      if ( p_ptr_next != NULL )
         p_ptr_next->p_ptr_previous = p_ptr_previous;
      if ( p_ptr_first == this )
         p_ptr_first = p_ptr_next;
      if ( p_ptr_last == this )
         p_ptr_last = p_ptr_previous;
      p_ptr_previous = NULL;
      p_ptr_next = NULL;

   }

   if ( p_casc_level >= 0 )
      p_casc_next_level--;

   p_nb_open--;
}

/*-------------------------------------------------------------------------*/
/*-                      Property Methods                                 -*/
/*-------------------------------------------------------------------------*/

short Window::width ( void )
{
   return ( p_width );
}

short Window::height ( void )
{
   return ( p_height );
}

short Window::x_pos ( void )
{
   return ( p_x_pos );
}

short Window::y_pos ( void )
{
   return ( p_y_pos );
}

Window* Window::next ( void )
{
   return ( p_ptr_next );
}

Window* Window::previous ( void )
{
   return ( p_ptr_previous );
}

int Window::nb_open ( void )
{
   return ( p_nb_open );
}

int Window::border_type ( void )
{
   return ( p_border_type );
}

void Window::border_type ( int type )
{
   p_border_type = type;
}

int Window::background_color ( void )
{
   return ( p_bg_color );
}
void Window::background_color ( int color )
{
   p_bg_color = color;
}
int Window::background_texture ( void )
{
   return ( p_bg_texture );
}
void Window::background_texture ( int texture )
{
   p_bg_texture = texture;
}

int Window::background_mode ( void )
{
   return ( p_bg_mode );
}

void Window::background_mode ( int mode )
{
   p_bg_mode = mode;
}

int Window::cascade_next_level ( void )
{
   return ( p_casc_next_level );
}

void Window::enable_cascade ( void )
{
   p_casc_display = true;
}

void Window::disable_cascade ( void )
{
   p_casc_display = false;
}

void Window::cascade_width ( int width )
{
   p_casc_width = width;
}

void Window::cascade_start ( void )
{
   p_casc_active = true;
   p_casc_next_level = 0;
}

void Window::cascade_pause ( void )
{
   p_casc_active = false;
}

void Window::cascade_continue ( void )
{
   p_casc_active = true;
}

void Window::cascade_stop ( void )
{
   p_casc_active = false;
   p_casc_next_level = 0;
}

int Window::cascade_level ( void )
{
   return ( p_casc_level);
}

void Window::translucency_level ( int level )
{
   p_bg_trans_level = level;
}

/*-------------------------------------------------------------------------*/
/*-                              Methods                                  -*/
/*-------------------------------------------------------------------------*/

void Window::hide ( void )
{

   p_hidden = true;
}

void Window::unhide ( void )
{
   p_hidden = false;
}


short Window::show_all ( void )
{
   int i;
   Window *tmptr;
   short retval = 0;


   draw_party_frame();

   if ( p_nb_open > 0 )
   {
      i = 1;
      tmptr = p_ptr_first;
      while ( i != p_nb_open )
      {
         if ( tmptr->p_hidden == false )
            tmptr->draw();
         else
            // is cascade enabled and the window is part of a cascade draw it anyway.
            if ( p_casc_active == true && tmptr->p_casc_level >= 0 )
               tmptr->draw();
         tmptr = tmptr->next();
         i++;


      }
      tmptr->draw_background();
      retval = tmptr->show();

   }

   return ( retval );

}

void Window::draw_all ( void )
{
   int i;
   Window *tmptr;

   draw_party_frame();

   if ( p_nb_open > 0 )
   {
      i = 1;
      tmptr = p_ptr_first;
      while ( i != p_nb_open )
      {
         if ( tmptr->p_hidden == false )
            tmptr->draw();
         tmptr = tmptr->next();
         i++;
      }
      if ( tmptr->p_hidden == false )
         tmptr->draw();
      draw_instruction();
   }
}

void Window::refresh_all ( void )
{
   int i;
   Window *tmptr;

   if ( p_nb_open > 0 )
   {
      i = 1;
      tmptr = p_ptr_first;
      while ( i != p_nb_open )
      {
         if ( tmptr->p_hidden == false )
            tmptr->refresh();
         tmptr = tmptr->next();
         i++;
      }
      tmptr->refresh();

   }

}

void Window::instruction ( short center_x, short y )
{
   p_inst_center_x = center_x;
   p_inst_y = y;
}

void Window::instruction ( short center_x, short y, int order )
{
   p_inst_center_x = center_x;
   p_inst_y = y;
   p_inst_order = order;
}

void Window::instruction ( short center_x, short y, const char *str )
{
   p_inst_center_x = center_x;
   p_inst_y = y;
   p_inst_order = -1;
   p_inst_string = str;
}

/*-------------------------------------------------------------------------*/
/*-                           Private  Methods                            -*/
/*-------------------------------------------------------------------------*/

/*void Window::draw_border ( void )
{
   int tmpcol [ 5 ];
   int i;
   int j;
   int k;
   int diff;
   short basex [ 4 ];
   short basey [ 4 ];

   tmpcol [ 0 ] = Window_COLOR_BORDER - ( 5285 * 2 );
   tmpcol [ 1 ] = Window_COLOR_BORDER - 5285;
   tmpcol [ 2 ] = Window_COLOR_BORDER;
   tmpcol [ 3 ] = Window_COLOR_BORDER - 5285;
   tmpcol [ 4 ] = Window_COLOR_BORDER - ( 5285 * 2 );

   basex [ 0 ] = p_x_pos;
   basey [ 0 ] = p_y_pos;
   basex [ 1 ] = ( p_x_pos + p_width ) - 4;
   basey [ 1 ] = p_y_pos;
   basex [ 2 ] = p_x_pos;
   basey [ 2 ] = ( p_y_pos + p_height ) - 4;
   basex [ 3 ] = ( p_x_pos + p_width ) - 4;
   basey [ 3 ] = ( p_y_pos + p_height ) - 4;

   for ( k = 0 ; k < 4 ; k++ )
      for ( j = 0 ; j < 5 ; j++ )
         for ( i = 0 ; i < 5 ; i++ )
         {
            if ( BORDER_CORNER [ k ] [ j ] [ i ] != 5 )
               putpixel ( buffer, basex [ k ] + i , basey [ k ] + j,
                  tmpcol [ BORDER_CORNER [ k ] [ j ] [ i ] ] );
         }

   putpixel ( buffer, basex [ 0 ] + 5 , basey [ 0 ] + 5, tmpcol [ 4 ] );
   putpixel ( buffer, basex [ 1 ] - 1, basey [ 1 ] + 5, tmpcol [ 4 ] );
   putpixel ( buffer, basex [ 2 ] + 5 , basey [ 2 ] - 1, tmpcol [ 4 ] );
   putpixel ( buffer, basex [ 3 ] - 1, basey [ 3 ] - 1, tmpcol [ 4 ] );

   diff = 5;
   for ( i = 0 ; i < 5 ; i++ )
   {
//      rect( buffer, x1, y1, x2, y2, tmpcol [ i ] );
      vline ( buffer, x1 , y1 + diff, y2 - diff, tmpcol [ i ] );
      vline ( buffer, x2 , y1 + diff, y2 - diff, tmpcol [ i ] );
      hline ( buffer, x1 + diff, y1, x2 - diff, tmpcol [ i ] );
      hline ( buffer, x1 + diff, y2, x2 - diff, tmpcol [ i ] );
      x1++;
      y1++;
      x2--;
      y2--;
      diff--;
   }


} */

void Window::draw_instruction ( void )
{
//   string str;
   char str [ 121 ] = "";
//   char str2 [ 81 ] = "";

   if ( config.get ( Config_KEY_DISPLAY ) == Config_KEY_YES )
   {
//      text_mode_old ( -1 );

      if ( p_inst_order == -1 )
         textout_centre_old ( subbuffer, General_FONT_INSTRUCTION, str,
            p_inst_center_x, p_inst_y, General_COLOR_INSTRUCTION );
      else
      {
         if ( ( p_inst_order & Window_INSTRUCTION_SELECT ) > 0 )
         {
            strcat ( str, "[ " );
            switch ( SELECT_KEY )
            {
               case KEY_Z :
                  strcat ( str, "Z" );
               break;
               case KEY_W :
                  strcat ( str, "W" );
               break;
               case KEY_X :
                  strcat ( str, "X" );
               break;
            }
            strcat ( str, " : Select ]" );
         }
         if ( ( p_inst_order & Window_INSTRUCTION_CANCEL ) > 0 )
         {
            strcat ( str, "[ " );
            switch ( CANCEL_KEY )
            {
               case KEY_A :
                  strcat ( str, "A" );
               break;
               case KEY_Q :
                  strcat ( str, "Q" );
               break;
               case KEY_Z :
                  strcat ( str, "Z" );
               break;
            }
            strcat ( str, " : Cancel ]" );
         }
         if ( ( p_inst_order & Window_INSTRUCTION_DISPLAY ) > 0 )
            strcat ( str, "[ Enter : Display ]" );
         if ( ( p_inst_order & Window_INSTRUCTION_MENU ) > 0 )
            strcat ( str, "[ Up/Dn : Cursor ]" );
         if ( ( p_inst_order & Window_INSTRUCTION_SWITCH ) > 0 )
            strcat ( str, "[ Lft/Rgt : Switch ]" );
         if ( ( p_inst_order & Window_INSTRUCTION_MOVE ) > 0 )
            strcat ( str, "[ Up/Lft/Rgt : Move ]" );
         if ( ( p_inst_order & Window_INSTRUCTION_INPUT ) > 0 )
            strcat ( str, "[ Enter : Accept ]" );
         if ( ( p_inst_order & Window_INSTRUCTION_CAMP ) > 0 )
         {
            strcat ( str, "[ " );
            switch ( CANCEL_KEY )
            {
               case KEY_A :
                  strcat ( str, "A" );
               break;
               case KEY_Q :
                  strcat ( str, "Q" );
               break;
               case KEY_Z :
                  strcat ( str, "Z" );
               break;
            }
            strcat ( str, " : Camp ]" );
         }
         if ( ( p_inst_order & Window_INSTRUCTION_SCROLL ) > 0 )
            strcat ( str, "[ PgUp/Dn : Scroll ]");
         if ( ( p_inst_order & Window_INSTRUCTION_VALUE ) > 0 )
            strcat ( str, "[ Left/Right : Lower/Raise ]");
         if ( ( p_inst_order & Window_INSTRUCTION_EXIT ) > 0 )
            strcat ( str, "[ Esc : Exit Maze ]");
         if ( ( p_inst_order & Window_INSTRUCTION_ACCEPT ) > 0 )
         {
            strcat ( str, "[ " );
            switch ( SELECT_KEY )
            {
               case KEY_Z :
                  strcat ( str, "Z" );
               break;
               case KEY_W :
                  strcat ( str, "W" );
               break;
               case KEY_X :
                  strcat ( str, "X" );
               break;
            }
            strcat ( str, " : Accept ]" );
         }
          if ( ( p_inst_order & Window_INSTRUCTION_EDITOR ) > 0 )
            strcat ( str, "[ Arrows ] move [ PgDnUp ] Change Level [ Del ] Delete");



         textout_centre_old ( subbuffer, General_FONT_INSTRUCTION, str,
                      p_inst_center_x, p_inst_y, General_COLOR_INSTRUCTION );
      }

//      text_mode_old ( 0 );
   }

}

void Window::border_fill ( void )
{
   int tmpcol [ 5 ];
   int i;
   int j;
   int k;
   int diff;
   short basex [ 4 ];
   short basey [ 4 ];
   short x1 = p_x_pos;
   short y1 = p_y_pos;
   short x2 = ( p_x_pos + p_width ) - 1;
   short y2 = ( p_y_pos + p_height ) - 1;
   int z;
   //int color;
   //int casc = 0;

   //if ( p_casc_level > 0 )
   //   casc = p_casc_width * p_casc_level;

   /*x1 += casc;
   y1 += casc;
   x2 += casc;
   y2 += casc;*/


   for ( z = 0 ; z < 5 ; z++ )
   {
      if ( BORDER_COLOR [ p_border_type ] [ z ] != 250 )
         tmpcol [ z ] = makecol ( 250 - BORDER_COLOR [ p_border_type ] [ z ],
                        250 - BORDER_COLOR [ p_border_type ] [ z ],
                        250 - BORDER_COLOR [ p_border_type ] [ z ] );
      else
         //tmpcol [ z ] = 0;
         tmpcol [ z ] = -1; //makecol ( 255, 0, 255 );
   }

   basex [ 0 ] = x1;
   basey [ 0 ] = y1;
   basex [ 1 ] = x2 - 4;
   basey [ 1 ] = y1;
   basex [ 2 ] = x1;
   basey [ 2 ] = y2 - 4;
   basex [ 3 ] = x2 - 4;
   basey [ 3 ] = y2 - 4;


   /*color = makecol ( Window_BACKGROUND [ p_bg_color ] . red,
                     Window_BACKGROUND [ p_bg_color ] . green,
                     Window_BACKGROUND [ p_bg_color ] . blue );

  if ( p_bg_mode == Window_BACKGROUND_TEXTURE )
   {
      drawing_mode ( DRAW_MODE_COPY_PATTERN,
         (BITMAP*) datfimage [ Window_BACKGROUND [ p_bg_texture ] . texturedatID  ]
         . dat , 0, 0 );
      //printf ("bgID=%d, TExID=%d\r\n", p_bg_texture, Window_BACKGROUND [ p_bg_texture ] . texturedatID );
   }*/

    //wrong place to do this, do it in the final blitting. BUt everything will be transpararent.


   /*int width = x2 - x1;
   int height = y2 - y1;
   BITMAP *tmpbmp = create_bitmap ( width, height );*/


   /*draw_trans_sprite(subbuffer, tmpbmp, x1, y1);*/

   //translucency test
   /*color = makeacol ( Window_BACKGROUND [ p_bg_color ] . red,
                     Window_BACKGROUND [ p_bg_color ] . green,
                     Window_BACKGROUND [ p_bg_color ] . blue,
                     125   );
*/
   //drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );

   //------------------------------------
   /*set_trans_blender(0, 0, 0, 128);
   drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );
   rectfill ( subbuffer, x1 + 5 , y1 + 5 , x2 - 5 , y2 - 5, color );
   set_trans_blender(0, 0, 0, 255);*/
   //-------------------------------------

   //rectfill ( subbuffer, x1 + 5, y1 + 5 , x2 - 5, y2 - 5, color );

   //clear_bitmap (subbuffer);


   //drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );

   for ( k = 0 ; k < 4 ; k++ )
      for ( j = 0 ; j < 5 ; j++ )
         for ( i = 0 ; i < 5 ; i++ )
         {
            if ( BORDER_CORNER [ k ] [ j ] [ i ] != 5  && tmpcol [ BORDER_CORNER [ k ] [ j ] [ i ] ] != -1 )
               putpixel ( subbuffer, basex [ k ] + i , basey [ k ] + j,
                  tmpcol [ BORDER_CORNER [ k ] [ j ] [ i ] ] );
         }

   if ( tmpcol [ 4 ] != -1 )
   {
      putpixel ( subbuffer, basex [ 0 ] + 5 , basey [ 0 ] + 5, tmpcol [ 4 ] );
      putpixel ( subbuffer, basex [ 1 ] - 1, basey [ 1 ] + 5, tmpcol [ 4 ] );
      putpixel ( subbuffer, basex [ 2 ] + 5 , basey [ 2 ] - 1, tmpcol [ 4 ] );
      putpixel ( subbuffer, basex [ 3 ] - 1, basey [ 3 ] - 1, tmpcol [ 4 ] );
   }

   diff = 5;
   for ( i = 0 ; i < 5 ; i++ )
   {
      if ( tmpcol [ i ] != -1)
      {
         vline ( subbuffer, x1 , y1 + diff, y2 - diff, tmpcol [ i ] );
         vline ( subbuffer, x2 , y1 + diff, y2 - diff, tmpcol [ i ] );
         hline ( subbuffer, x1 + diff, y1, x2 - diff, tmpcol [ i ] );
         hline ( subbuffer, x1 + diff, y2, x2 - diff, tmpcol [ i ] );
      }
      x1++;
      y1++;
      x2--;
      y2--;
      diff--;
   }

}

void Window::draw_background ( void )
{
   int color = makecol ( Window_BACKGROUND [ p_bg_color ] . red,
                     Window_BACKGROUND [ p_bg_color ] . green,
                     Window_BACKGROUND [ p_bg_color ] . blue );

   drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );

   switch ( p_bg_mode )
   {
      case Window_BACKGROUND_COLOR:
         /*color = makecol ( Window_BACKGROUND [ p_bg_color ] . red / 2,
                     Window_BACKGROUND [ p_bg_color ] . green / 2,
                     Window_BACKGROUND [ p_bg_color ] . blue / 2);*/
      break;
      case Window_BACKGROUND_TRANSLUCENT:
         //to do : condition on certain window
         if ( p_translucent == true )
         {
            drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );
            set_trans_blender(0, 0, 0, p_bg_trans_level);
         }
      break;
      case Window_BACKGROUND_TEXTURE:
         drawing_mode ( DRAW_MODE_COPY_PATTERN,
            datref_image [ Window_BACKGROUND [ p_bg_texture ] . texturedatID ],

         //(BITMAP*) datfimage [ Window_BACKGROUND [ p_bg_texture ] . texturedatID  ]
         //. dat ,
          0, 0 );
      break;
   }



   //to do need to switch if mode is translucent

   /*if ( p_bg_mode == Window_BACKGROUND_TEXTURE )
   {

      //printf ("bgID=%d, TExID=%d\r\n", p_bg_texture, Window_BACKGROUND [ p_bg_texture ] . texturedatID );
   }*/



   rectfill ( subbuffer, p_x_pos + 3, p_y_pos + 3 , p_x_pos + p_width - 3, p_y_pos + p_height - 3, color );
      //printf ("pass here\n");
   set_trans_blender(0, 0, 0, 255);
   drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
}


void Window::draw ( void )
{
   // translucency test
   //set_trans_blender(0, 0, 0, 128);

   /*if ( p_backup != NULL )
      draw_trans_sprite(subbuffer, p_backup, p_x_pos, p_y_pos);*/

   //set_trans_blender(0, 0, 0, 128);
   /*int width = x2 - x1;
   int height = y2 - y1;
   BITMAP *tmpbmp = create_bitmap ( width, height );
   rectfill ( tmpbmp, 0, 0 , width , height, color );*/

   // For translucency, need to split background from content.

   if ( p_backup != NULL )
   {
      draw_background();

      //draw_sprite(subbuffer, p_backup, p_x_pos, p_y_pos);
      masked_blit ( p_backup, subbuffer, 0, 0, p_x_pos, p_y_pos, p_width, p_height );

      /*copy_buffer();
      while (mainloop_readkeyboard() != SELECT_KEY );*/
   }



}

void Window::show_party_frame ( void )
{
   p_show_party_frame = true;
}

void Window::hide_party_frame ( void )
{
   p_show_party_frame = false;
}

void Window::draw_party_frame ( void )
{
   //int maxHPbars;
   //int maxMPbars;
   //debug_printf( __func__, "--- Begin ---");

   if ( config.get ( Config_DISPLAY_PARTY_FRAME) == Config_PAR_YES
       && SCREEN_W >= 1024 )
   {
      if ( SQLdb != NULL)
      {
      if ( p_show_party_frame == true )
      {


         Character tmpcharacter;
         int errorsql;
         int charindex = 0;
         int x=0;
         int y=0;
         int HPbars;
         int MPbars;
         int EXPbar;
         int EXPforlevel;
         int EXPcurrent;
         int EXPpreviouslevel;
         int i;
         int yoff;
         int xoff;
         Race tmprace;
         CClass tmpclass;
//         debug_printf( __func__, "Just before SQLpreparef");
         errorsql = tmpcharacter.SQLpreparef ( "WHERE location=%d ORDER BY position",
                                           Character_LOCATION_PARTY );

         if ( errorsql == SQLITE_OK)
         {
            errorsql = tmpcharacter.SQLstep();

            while ( errorsql == SQLITE_ROW && charindex < 6 )
            {
               //display character

               if ( ( charindex % 2 ) == 0 )
                  x=0;
               else
                  x=832;

               switch ( charindex )
               {
                  case 0:
                  case 1:
                     y=0;
                  break;
                  case 2:
                  case 3:
                     y=192;
                  break;
                  case 4:
                  case 5:
                     y=384;
                  break;
               }

               //----- Character infor and portrait -----

               tmprace.SQLselect ( tmpcharacter.FKrace() );
               tmpclass.SQLselect ( tmpcharacter.FKcclass() );

//printf ("Draw Party Frame: Pass 1\n");
               rect (buffer, x, y, x+191, y+191, makecol (200, 200, 200));

               textprintf_old ( buffer, FNT_bookantiquabold24, x+2, y+128,
                           General_COLOR_TEXT, "%s", tmpcharacter.name());

               textprintf_old ( buffer, FNT_bookantiquabold24, x+2, y+151, General_COLOR_TEXT,
                  "Lv%2d %s %s", tmpcharacter.level(), tmprace.initial(), tmpclass.initial() );

               //textprintf_old ( buffer, FNT_bookantiquabold24, x+2, y+128,
               //            General_COLOR_TEXT, tmpcharacter.name());

//printf("Portraid ID = %d\n", tmpcharacter.portraitid());

               draw_sprite (buffer, datref_portrait [tmpcharacter.portraitid()], x+32, y+1 );

//printf ("Draw Party Frame: Pass 2\n");

               //----- HP and MP bars -----
               textprintf_old ( buffer, FNT_small, x+3, y+1, General_COLOR_TEXT,
                           "HP MP");

               textprintf_old ( buffer, FNT_small, x+1, y+115, General_COLOR_TEXT,
                           "%d:%d", tmpcharacter.current_HP(), tmpcharacter.current_MP());

//printf ("Draw Party Frame: Pass 3\n");
               if ( tmpcharacter.max_HP() == 0)
                  HPbars = 0;
               else
                  HPbars = ( tmpcharacter.current_HP() * 20 ) / tmpcharacter.max_HP();

               if ( HPbars == 0 && tmpcharacter.current_HP() > 0)
                  HPbars = 1;

               if ( tmpcharacter.max_MP() == 0)
                  MPbars = 0;
               else
                  MPbars = ( tmpcharacter.current_MP() * 20 ) / tmpcharacter.max_MP();

               if ( MPbars == 0 && tmpcharacter.current_MP() > 0)
                  MPbars = 1;

               //testingcode
               //HPbars /=2;
               //MPbars /=2;
//printf ("Draw Party Frame: Pass 4\n");
               for ( i = 0 ; i < 20 ; i++ )
               {
                  yoff = y + 14 + (i*5);

                  if (HPbars >= 20 - i )
                     rectfill ( buffer, x + 2, yoff, x+15, yoff + 3,
                             makecol (200, 0, 0) );
                  else
                     hline ( buffer, x+2, yoff , x+15, makecol (100,0,0));
                     //rect ( buffer, x + 2, yoff, x+15, yoff + 3,
                     //        makecol (100, 0, 0) );

                  if (MPbars >= 20 - i )
                     rectfill ( buffer, x + 17, yoff, x+30, yoff + 3,
                             makecol (0, 0, 200) );
                  else
                     hline ( buffer, x+17, yoff , x+30, makecol (0,0,100));
                     //rect ( buffer, x + 17, yoff, x+30, yoff + 3,
                     //        makecol (0, 0, 100) );
               }

//printf ("Draw Party Frame: Pass 5\n");
               // ----- EXP bar -----

               if ( tmpcharacter.level() > 1)
                  EXPpreviouslevel = EXP_TABLE [ tmpcharacter.level() -2];
               else
                  EXPpreviouslevel = 0;

               EXPforlevel = tmpcharacter.next_level_exp() - EXPpreviouslevel;

               if ( tmpcharacter.exp() >= tmpcharacter.next_level_exp())
                  EXPcurrent = tmpcharacter.next_level_exp() - EXPpreviouslevel;
               else
                  EXPcurrent = tmpcharacter.exp() - EXPpreviouslevel;

               //testing code
               //EXPcurrent += 3000;

               EXPbar = ( EXPcurrent * 190) / EXPforlevel;

               rectfill (buffer, x + 1, y+175, x + 1 + EXPbar, y+190,
                         makecol (0, 150, 0));

               for ( i = 0; i < 5 ; i++ )
               {
//printf ( "i=%d", i);
                  xoff = x + 1 + (i*38);

                  rect ( buffer, xoff, y+175, xoff + 37, y+190,
                        makecol (0, 75, 0));

               }
               textprintf_old ( buffer, FNT_small, x+2, y+176, General_COLOR_TEXT,
                           "%d", tmpcharacter.exp() );
               textprintf_right_old ( buffer, FNT_small, x+191, y+176, General_COLOR_TEXT,
                           "%d", tmpcharacter.next_level_exp() );
               textprintf_centre_old ( buffer, FNT_print, x+96, y+176,
                                  makecol ( 0, 75, 0 ), "EXP");


               charindex++;
               errorsql = tmpcharacter.SQLstep();
            }

   //debug_printf( __func__, "--- Before SQLfinalize ---");
            tmpcharacter.SQLfinalize();
         }

      }
      }
   }

   //debug_printf( __func__, "--- End ---");

}

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

int BORDER_CORNER [ 4 ] [ 5 ] [ 5 ] =
{
   { // Draw_CORNER_TLEFT
      { 5, 5, 5, 5, 5 },
      { 5, 5, 5, 0, 0 },
      { 5, 5, 0, 1, 1 },
      { 5, 0, 1, 2, 2 },
      { 5, 0, 1, 2, 3 }
   },

   { // Draw_CORNER_TRIGHT
      { 5, 5, 5, 5, 5 },
      { 0, 0, 5, 5, 5 },
      { 1, 1, 0, 5, 5 },
      { 2, 2, 1, 0, 5 },
      { 3, 2, 1, 0, 5 }
   },

   { // Draw_CORNER_BLEFT
      { 5, 0, 1, 2, 3 },
      { 5, 0, 1, 2, 2 },
      { 5, 5, 0, 1, 1 },
      { 5, 5, 5, 0, 0 },
      { 5, 5, 5, 5, 5 }
   },

   { // Draw_CORNER_BRIGHT
      { 3, 2, 1, 0, 5 },
      { 2, 2, 1, 0, 5 },
      { 1, 1, 0, 5, 5 },
      { 0, 0, 5, 5, 5 },
      { 5, 5, 5, 5, 5 }
   }

};


int BORDER_COLOR [ 5 ] [ 5 ] =
{   // note put the color to black while 0 place it to Window_BORDER_COLOR
   { // Thick
      120,
      60,
      0,
      60,
      120
   },
   { // Thin
      120,
      0,
      120,
      250,
      250,
   },
   { // Double
      0,
      180,
      240,
      180,
      0,
   },
   { // Flat
      60,
      60,
      60,
      60,
      60,
   },
   { // None
      250,
      250,
      250,
      250,
      250,
   }
};

s_Window_background Window_BACKGROUND [ 5 ] =
{
   { 1024,   0,   0,   0 },
   { 1025,  20,  50,   0 },
   { 1026,  20,   0,  50 },
   { 1027,  50,   0,   0 },
   { 1028,   0,  30,  40 },
};


