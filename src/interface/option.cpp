/***************************************************************************/
/*                                                                         */
/*                         O P T I O N . C P P                             */
/*                                                                         */
/*     Content : Class Option                                              */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : May 12nd, 2002                                      */
/*                                                                         */
/***************************************************************************/
// Include Groups
#include <grpsys.h>
#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>


/*
//#include <time.h>
#include <allegro.h>


#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
*/

/*-------------------------------------------------------------------------*/
/*-                  Constructor and Destructor                           -*/
/*-------------------------------------------------------------------------*/

Option::Option ( void )
{
   int i;

   strncpy ( p_title, "", Option_TITLE_SIZE );
//   p_font = font;
   p_nb_item = 0;
   ptr_first = NULL;
   ptr_last = NULL;

   for ( i = 0 ; i < Option_NB_HEADER; i++)
      strncpy ( p_header [ i ], "" , Option_HEADER_STR_SIZE );

   p_showheader = false;
}

Option::Option ( const char* str )
{
//   string *tmp = new string;

//   *tmp = dat;
   int i;

   strncpy ( p_title, str, Option_TITLE_SIZE );
//   p_font = fnt;
   p_nb_item = 0;
   ptr_first = NULL;
   ptr_last = NULL;


   for ( i = 0 ; i < Option_NB_HEADER; i++)
      strncpy ( p_header [ i ], "" , Option_HEADER_STR_SIZE );

   p_showheader = false;

//   delete tmp;

}
/*Option::Option ( string &str )
{
   p_title = str;
//   p_font = fnt;
   p_nb_item = 0;
   ptr_first = NULL;
   ptr_last = NULL;
} */

Option::~Option ( void )
{
   s_Option_item *tmp_ptr;
   s_Option_item *del_ptr;

   tmp_ptr = ptr_first;

   while ( tmp_ptr != NULL )
   {
      del_ptr = tmp_ptr;
      tmp_ptr = tmp_ptr -> ptr_next;
      free ( del_ptr );
   }

}

/*-------------------------------------------------------------------------*/
/*-                         Property Methods                              -*/
/*-------------------------------------------------------------------------*/

/*void Option::title ( string &str )
{
   p_title = str;
} */

void Option::title ( const char* str )
{
//   string *tmp = new string;

//   *tmp= dat ;

   strncpy ( p_title, str, Option_TITLE_SIZE );

//   delete tmp;
}

/*void Option::quit ( string &str )
{
   p_quit = str;
} */

void Option::quit ( const char* str )
{
//   string *tmp = new string;

//   *tmp= dat ;

   strncpy ( p_quit, str, Option_TEXT_SIZE );

//   delete tmp;
}

int Option::nb_item ()
{
   return ( p_nb_item );
}

int Option::nb_element ( int itemID )
{
   s_Option_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != itemID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

    if ( tmp_ptr != NULL )
         return ( tmp_ptr->nb_elem );
    else
       return ( 0 );

}


int Option::selected ( int itemID )
{
   s_Option_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != itemID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

   if ( tmp_ptr != NULL )
      return ( tmp_ptr->selected );
   else
      return ( 0 );

}

void Option::selected ( int itemID, int value )
{
   s_Option_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != itemID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

   if ( tmp_ptr != NULL )
      tmp_ptr->selected = value;
}

int Option::key ( int itemID )
{
   s_Option_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != itemID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

   if ( tmp_ptr != NULL )
      return ( tmp_ptr->key );
   else
      return ( 0 );

}

void Option::key ( int itemID, int value )
{
   s_Option_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != itemID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

   if ( tmp_ptr != NULL )
      tmp_ptr->key = value;
}

s_Option_result Option::result( int itemID )
{
   s_Option_result tmp_result;
   s_Option_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != itemID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

   if ( tmp_ptr != NULL )
   {
      tmp_result.selected = tmp_ptr->selected;
      tmp_result.key =  tmp_ptr->key;

      return (tmp_result);
   }
   else
   {
      tmp_result.selected = 0;
      tmp_result.key = 0;

      return ( tmp_result );
   }

}


void Option::header ( const char *text1,  const char *text2, const char *text3,
                        const char *text4, const char *text5, const char *text6 )

{
   p_showheader = true;
   strncpy ( p_header [ 0 ], text1, Option_HEADER_STR_SIZE);
   strncpy ( p_header [ 1 ], text2, Option_HEADER_STR_SIZE);
   strncpy ( p_header [ 2 ], text3, Option_HEADER_STR_SIZE);
   strncpy ( p_header [ 3 ], text4, Option_HEADER_STR_SIZE);
   strncpy ( p_header [ 4 ], text5, Option_HEADER_STR_SIZE);
   strncpy ( p_header [ 5 ], text6, Option_HEADER_STR_SIZE);
}

bool Option::showheader ( void )
{
    return ( p_showheader );
}


/*-------------------------------------------------------------------------*/
/*-                              Methods                                  -*/
/*-------------------------------------------------------------------------*/

   // Methods

/*void Option::add_item ( string &str, string &elem1, string &elem2,
           string &elem3, string &elem4, string &elem5, int selected = 0 )*/
void Option::add_item ( int key, const char* str, const char* elem1, const char* elem2,
   const char* elem3, const char* elem4, const char* elem5, int selected )

{

   s_Option_item *ptr_new_item;
   int nb_elem;

   ptr_new_item = new s_Option_item;

   nb_elem = 2;
   strncpy ( ptr_new_item -> text, str, Option_TEXT_SIZE );
   strncpy ( ptr_new_item -> elem [ 0 ], elem1, Option_ELEM_SIZE );
   strncpy ( ptr_new_item -> elem [ 1 ], elem2, Option_ELEM_SIZE );
   strncpy ( ptr_new_item -> elem [ 2 ], elem3, Option_ELEM_SIZE );
   strncpy ( ptr_new_item -> elem [ 3 ], elem4, Option_ELEM_SIZE );
   strncpy ( ptr_new_item -> elem [ 4 ], elem5, Option_ELEM_SIZE );
   ptr_new_item -> selected = selected;
   ptr_new_item -> key = key;

   if ( strcmp ( elem3, "" ) != 0 )
   {
      nb_elem++;

      if ( strcmp ( elem4, "" ) != 0 )
      {
         nb_elem++;

         if ( strcmp ( elem5, "" ) != 0 )
            nb_elem++;
      }
   }

   ptr_new_item -> nb_elem = nb_elem;

   if ( p_nb_item == 0 )
   {
      // insert the first item
      ptr_first = ptr_new_item;
      ptr_last = ptr_new_item;
      ptr_new_item -> ptr_next = NULL;
   }
   else
   {
      // add item at the end of the list
      ptr_last -> ptr_next = ptr_new_item;
      ptr_last = ptr_new_item;
      ptr_new_item -> ptr_next = NULL;
   }

   p_nb_item++;

}

/*
void Option::add_item ( const char* dat, const char* elem1, const char* elem2,
   const char* elem3, const char* elem4, const char* elem5, int selected )
{
   string* tmpdat = new string;
   string* tmpelem1 = new string;
   string* tmpelem2 = new string;
   string* tmpelem3 = new string;
   string* tmpelem4 = new string;
   string* tmpelem5 = new string;

   *tmpdat = dat;
   *tmpelem1 = elem1;
   *tmpelem2 = elem2;
   *tmpelem3 = elem3;
   *tmpelem4 = elem4;
   *tmpelem5 = elem5;

   add_item ( *tmpdat, *tmpelem1, *tmpelem2, *tmpelem3, *tmpelem4
      , *tmpelem5, selected );

   delete tmpdat;
   delete tmpelem1;
   delete tmpelem2;
   delete tmpelem3;
   delete tmpelem4;
   delete tmpelem5;

} */

void Option::show (  short x, short y, bool readonly )
{

   int font_height = text_height ( Option_FONT );
   int space = font_height / 2;
   int header_font_height = text_height ( Option_HEADER_FONT );
   int arrow_space = text_length ( Option_FONT, "-> ");
   int font_width = text_length ( Option_FONT, "a");
   s_Option_item *tmp_ptr;
   int cursor = 0;
//   string tmp_str;
   char kbvalue;
   short base_y = y;
   short yinc;
   bool rereadkb = false;
   int i;
   char format [ Option_DISPLAY_SIZE ];
   bool exit_option = false;
   int j;
   bool option_changed = true;
   int k;
   int tmpx;
   //BITMAP* backup = create_bitmap ( 640, 480 );
   bool title = false;
   int color;

   //x += System_X_OFFSET;
   //y += System_Y_OFFSET;

   while ( exit_option == false )
   {


      if ( option_changed == true )
      {
         blit ( buffer, backup, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
         option_changed = false;
         if ( strcmp ( p_title, "" ) != 0 )
         {
            textprintf_old ( subbackup , Option_FONT, x, y, Option_COLOR_TEXT,
               "   %s", p_title );
            base_y = y + font_height;
            title = true;
         }
         else
            base_y = y;

         if ( p_showheader == true)
         {
            tmpx = x + arrow_space;
            for ( k = 0; k < Option_NB_HEADER; k++ )
            {
               if ( k == 0)
               {
                  textout_old ( subbackup, Option_HEADER_FONT, p_header [ k ], tmpx, base_y, Option_COLOR_TEXT );
                  tmpx += font_width * 26;
               }
               else
               {
                  textout_centre_old ( subbackup, Option_HEADER_FONT, p_header [ k ], tmpx, base_y, Option_COLOR_TEXT );
                  tmpx += font_width * 10;
               }
            }
            base_y += header_font_height + Option_HEADER_SPACE;
         }

         if ( title == true)
            base_y += space;

         tmp_ptr = ptr_first;
         yinc = base_y;

         for ( j = 0 ; j < p_nb_item ; j++ )
         {
            strcpy ( format, "   %-20s " );

            for ( i = 0 ; i < 5 ; i++ )
            {
               if ( i == tmp_ptr -> selected )
                  strcat ( format, "[%8s]" );
               else
                  strcat ( format, " %8s " );
            }

            if ( readonly == true )
               color = Option_COLOR_DISABLED;
            else
               color = Option_COLOR_TEXT;

            textprintf_old ( subbackup, Option_FONT, x, yinc, color,
               format, tmp_ptr -> text,
               tmp_ptr -> elem [ 0 ],
               tmp_ptr -> elem [ 1 ],
               tmp_ptr -> elem [ 2 ],
               tmp_ptr -> elem [ 3 ],
               tmp_ptr -> elem [ 4 ] );

            yinc = yinc + font_height;
            tmp_ptr = tmp_ptr -> ptr_next;
         }

         textprintf_old ( subbackup, Option_FONT, x, yinc, Option_COLOR_TEXT,
            "   %-20s", p_quit );

      }

      tmp_ptr = ptr_first;
      yinc = base_y;

//      vsync ();
  //    blit ( backup, screen , 0 , 0 , 0 , 0 , 640 , 480 );
      copy_backup_buffer_keep();

      for ( j = 0 ; j < p_nb_item ; j++ )
      {
         if ( j == cursor )
            textprintf_old ( subscreen, Option_FONT, x, yinc, Option_COLOR_TEXT,
               "->" );

         yinc = yinc + font_height;
         tmp_ptr = tmp_ptr -> ptr_next;
      }

      if ( cursor == p_nb_item )
         textprintf_old ( subscreen, Option_FONT, x, yinc, Option_COLOR_TEXT,
            "->" );


      // Keyboard reading

      rereadkb = true;

      while ( rereadkb == true )
      {
         kbvalue = mainloop_readkeyboard();

         if ( kbvalue == -1)
            rereadkb = false;
         else
            rereadkb = true;

         if ( kbvalue == SELECT_KEY )
         {
            if ( cursor == p_nb_item )
            {
               rereadkb = false;
               exit_option = true;
            }
         }

         if ( kbvalue == CANCEL_KEY )
         {
            rereadkb = false;
            exit_option = true;
         }


         if ( kbvalue == KEY_UP )
         {
            if ( cursor > 0 )
            {
               cursor--;
               rereadkb = false;
            }
         }

         if ( kbvalue == KEY_DOWN )
         {
            if ( cursor < p_nb_item )
            {
               cursor++;
               rereadkb = false;
            }
         }

         if ( kbvalue == KEY_LEFT )
         {
            if ( readonly != true)
            {
               select_left ( cursor );
               option_changed = true;
               rereadkb = false;
            }
         }

         if ( kbvalue == KEY_RIGHT )
         {
            if ( readonly != true)
            {
               select_right ( cursor );
               option_changed = true;
               rereadkb = false;
            }
         }
      }
   }

   //destroy_bitmap ( backbuf );
   clear ( buffer );
}

/*-------------------------------------------------------------------------*/
/*-                         Private Methods                               -*/
/*-------------------------------------------------------------------------*/

void Option::select_right ( int itemID )
{
   s_Option_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != itemID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

   if ( tmp_ptr != NULL )
      if ( tmp_ptr -> selected < ( tmp_ptr -> nb_elem -1 ) )
        tmp_ptr -> selected++;

}


void Option::select_left ( int itemID )
{
   s_Option_item *tmp_ptr;
   int count = 0;

   tmp_ptr = ptr_first;

   while ( count != itemID && tmp_ptr != NULL )
   {
      tmp_ptr = tmp_ptr -> ptr_next;
      count++;
   }

   if ( tmp_ptr != NULL )
      if ( tmp_ptr -> selected > 0 )
        tmp_ptr -> selected--;

}

