/***************************************************************************/
/*                                                                         */
/*                         W I N Q U E S T . C P P                         */
/*                            Class source code                            */
/*                                                                         */
/*     Content : Class WinQuestion                                         */
/*     Programmer : Eric PIetrocupo                                        */
/*     Starting Date : November 23rd, 2002                                 */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>



/*
//#include <time.h>
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
//#include <winempty.h>
//#include <wintitle.h>
//#include <winmessa.h>
#include <winquest.h>
*/

/*-------------------------------------------------------------------------*/
/*-                  Constructors and Destructors                         -*/
/*-------------------------------------------------------------------------*/

/*WinQuestion::WinQuestion ( string &question, short center_x, short y_pos )
{
   construct ( question, center_x, y_pos );
} */

WinQuestion::WinQuestion ( const char *question, short center_x, short y_pos, bool translucent )
{
//   string *tmpstr = new string;
//   *tmpstr = question;
   p_translucent = translucent;
   construct ( question, center_x, y_pos );

//   delete tmpstr;
}

/*
WinQuestion::~WinQuestion ( void )
{

} */

/*-------------------------------------------------------------------------*/
/*-                          Virtual Methods                              -*/
/*-------------------------------------------------------------------------*/

void WinQuestion::preshow ( void )
{
   // this function does nothing
}

void WinQuestion::refresh ( void )
{
   // this function does nothing
}

short WinQuestion::show ( void )
{
   short xchar = p_x_pos + 10;
   short ychar = p_y_pos + 6;
   short length;
   short i;
   short font_width = text_length ( WinQuestion_FONT, "a" );
   short font_height = text_height ( WinQuestion_FONT );
//   string tmpstr;
   Menu mnu_question;
   short answer;
   int inst_x_backup;
   int inst_y_backup;
   int inst_order_backup;

   clear_keybuf();
   mnu_question.add_item ( WinQuestion_ANSWER_YES, "Yes" );
   mnu_question.add_item ( WinQuestion_ANSWER_NO, "No" );

   border_fill ();

   length = strlen ( p_question );

   i = 0;
   while ( i < length && p_question [ i ] != '$' )
   {
      if ( p_question [ i ] == '\n' || p_question [ i ] == '^' )
      {
         xchar = p_x_pos + 10;
         ychar = ychar + font_height;
      }
      else
      {
  //       tmpstr = p_question [ i ]; // change a character in a string
         textprintf_old ( subbuffer, WinQuestion_FONT, xchar, ychar,
            General_COLOR_TEXT, "%c", p_question [ i ] );
         xchar = xchar + font_width;
      }
      i++;
   }

// remove temporarily until instruction fusioned with window
//   draw_instruction ( Draw_INSTRUCTION_SELECT, centerx, ychar + font_height );

   inst_x_backup = p_inst_center_x;
   inst_y_backup = p_inst_y;
   inst_order_backup = p_inst_order;

   ychar = ychar + ( font_height * 2 );
   p_inst_order = Window_INSTRUCTION_SELECT + Window_INSTRUCTION_MENU;
   p_inst_center_x = p_center_x;
   p_inst_y = ychar + ( font_height * 2 );

   draw_instruction();
   answer = mnu_question.show ( p_center_x - 32, ychar, 0, true );
//   copy_buffer ();

//   while ( ( readkey() >> 8 ) != SELECT_KEY );

   blit ( subbuffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );

   p_inst_center_x = inst_x_backup;
   p_inst_y = inst_y_backup;
   p_inst_order = inst_order_backup;

   return ( answer );
}

int WinQuestion::type ( void )
{
   return ( Window_TYPE_MESSAGE );
}

/*-------------------------------------------------------------------------*/
/*-                        Private Methods                                -*/
/*-------------------------------------------------------------------------*/


void WinQuestion::construct ( const char *question, short center_x, short y_pos )
{
   int nb_line = 0;
   int nb_letter = 0;
   short font_height = text_height ( WinQuestion_FONT );
   short font_width = text_length ( WinQuestion_FONT, "a" );

   strncpy ( p_question, question, 241 );
   p_center_x = center_x;

   nb_line = line_count();
   nb_letter = max_line_size();

   p_x_pos = p_center_x - ( ( ( font_width * nb_letter ) / 2 ) + 4) ;
   p_y_pos = y_pos;
   p_width = 6 + ( font_width * nb_letter ) + 6 + 1 + 8;
   p_height = 6 + ( font_height * ( nb_line + 4 ) ) + 6 + 1;
   p_backup = create_bitmap ( p_width, p_height );
}

int WinQuestion::line_count ( void )
{
   short length = strlen ( p_question );
   short i;
   int nb_line = 1;

   i = 0;
   while ( i < length && p_question [ i ] != '$' )
   {
      if ( p_question [ i ] == '\n' || p_question [ i ] == '^' )
         nb_line++;
      i++;
   }
   return ( nb_line );
}

int WinQuestion::max_line_size ( void )
{
   short length = strlen ( p_question );
   short i;
   int nb_letter = 0;
   int tmp_nb_letter = 0;

   i = 0;
   while ( i < length && p_question [ i ] != '$' )
   {
      tmp_nb_letter++;
      if ( p_question [ i ] == '\n' || p_question [ i ] == '^' )
      {
         if ( tmp_nb_letter > nb_letter )
            nb_letter = tmp_nb_letter;
         tmp_nb_letter = 0;
      }
      i++;
   }
   if ( tmp_nb_letter > nb_letter )
      nb_letter = tmp_nb_letter;

   return ( nb_letter );
}
