/***************************************************************************/
/*                                                                         */
/*                        W I N T I T L E . C P P                          */
/*                           Class Source Code                             */
/*                                                                         */
/*     Content : Class WinTitle                                            */
/*     Programer : Eric Pietrocupo                                         */
/*     Starting Date : NOvember 10th, 2002                                 */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>



/*
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
//#include <winempty.h>
#include <wintitle.h>
*/

/*-------------------------------------------------------------------------*/
/*-                    Constructor & Destructor                           -*/
/*-------------------------------------------------------------------------*/

/*WinTitle::WinTitle ( string &title, short center_x , short y_pos )
{
   short font_height = text_height ( WinTitle_FONT );
   short text_width = text_length ( WinTitle_FONT, title.data() ) + 8;

   p_title = title;
   p_center_x = center_x;

   p_x_pos = center_x - ( ( text_width / 2 ) + 6 );
   p_y_pos = y_pos;
   p_width = 6 + text_width + 6 + 1;
   p_height = 6 + font_height + 6 + 1;
   p_backup = create_bitmap ( p_width, p_height );
   preshow();
} */

WinTitle::WinTitle ( const char *title, short center_x, short y_pos, bool translucent )
{
   short font_height = text_height ( WinTitle_FONT );
   short text_width = text_length ( WinTitle_FONT, title ) + 8;


   strncpy ( p_title, title, 81 );
   p_center_x = center_x;

   p_x_pos = center_x - ( ( text_width / 2 ) + 6 );
   p_y_pos = y_pos;
   p_width = 6 + text_width + 6 + 1;
   p_height = 6 + font_height + 6 + 1;
   p_backup = create_bitmap ( p_width, p_height );
   p_translucent = translucent;
   preshow();
}

/*
WinTitle::~WinTitle ( void )
{

}
*/

/*-------------------------------------------------------------------------*/
/*-                           Virtual Method                              -*/
/*-------------------------------------------------------------------------*/

void WinTitle::preshow ( void )
{
//   WinTitle::show();
   clear_to_color (subbuffer, makecol ( 255, 0, 255) );
   border_fill ();
   textout_centre_old ( subbuffer, WinTitle_FONT, p_title, p_center_x, p_y_pos + 6,
      WinTitle_COLOR );
   blit ( subbuffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );
   //draw_instruction();

   clear ( buffer );
}

void WinTitle::refresh ( void )
{
   // this function does nothing
}

short WinTitle::show ( void )
{
   border_fill ();
   textout_centre_old ( subbuffer, WinTitle_FONT, p_title, p_center_x, p_y_pos + 6,
      WinTitle_COLOR );
   blit ( subbuffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );
   //draw_instruction();
   copy_buffer();
   //printf ("Win Title: pass 1\r\n");
   return ( 0 );
}

int WinTitle::type ( void )
{
   return ( Window_TYPE_TITLE );
}
