/***************************************************************************/
/*                                                                         */
/*                         L I S T . C P P                                 */
/*                                                                         */
/*     Content : Class List                                                */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : August 25th, 2002                                   */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>

/*
//#include <time.h>
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <dbobject.h>
//#include <database.h>
//#include <dbdef.h>
//
//
//
//
//
//
#include <listwiz.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
*/

/*-------------------------------------------------------------------------*/
/*-                  Constructor and Destructor                           -*/
/*-------------------------------------------------------------------------*/

List::List ( void )
{
   int i;

   strcpy ( p_title,"" );
   strcpy ( p_header, "");
   p_nb_item = 0;
   p_page_size = 3;
   p_page_pos = 0;
   p_cursor = 0;
   p_sensible = false;
   p_selected = false;
   p_showheader = false;

   for (i = 0; i < List_MAX_NB_ITEM; i++)
   {
      strcpy ( p_item [ i ] . text, "");
      p_item [ i ] . mask = false;
      p_item [ i ] . answer = -1;
   }

}

List::List ( const char* str, int psize, bool sensible )
{
//   string *tmp = new string;
//   *tmp = dat;
   int i;

   strncpy  ( p_title, str, List_STR_SIZE );
   strcpy ( p_header, "");
   p_nb_item = 0;
   p_page_size = psize;
   p_page_pos = 0;
   p_cursor = 0;
   p_sensible = sensible;
   p_showheader = false;

   if ( sensible == true)
      p_selected = false;

   for (i = 0; i < List_MAX_NB_ITEM; i++)
   {
      strcpy ( p_item [ i ] . text, "");
      p_item [ i ] . mask = false;
      p_item [ i ] . answer = -1;
   }

//   delete tmp;
}
/*List::List ( string &str, int psize, bool sensible )
{
   p_title = str;
   p_nb_item = 0;
   p_page_size = psize;
   p_page_pos = 0;
   p_sensible = sensible;
} */

List::~List ( void )
{

   int i;

   strcpy ( p_title,"" );
   p_nb_item = 0;
   p_page_size = 3;
   p_page_pos = 0;
   p_cursor = 0;
   p_sensible = false;

   for (i = 0; i < List_MAX_NB_ITEM; i++)
   {
      strcpy ( p_item [ i ] . text, "");
      p_item [ i ] . mask = false;
      p_item [ i ] . answer = -1;
   }

}

/*-------------------------------------------------------------------------*/
/*-                         Property Methods                              -*/
/*-------------------------------------------------------------------------*/


/*void List::title ( string &str )
{
   p_title = str;
} */

void List::title ( const char* str )
{
//   string *tmp = new string;

//   *tmp= dat ;

   strncpy ( p_title, str, List_STR_SIZE );

//   delete tmp;
}

const char* List::title ( void )
{
   return ( p_title );
}

void List::page_size ( int value )
{
   p_page_size = value;
}

int List::page_size ( void )
{
   return ( p_page_size );
}

void List::page_pos ( int value )
{
   p_page_pos = value;
}

int List::page_pos ( void )
{
   return ( p_page_pos );
}

int List::nb_item ()
{
   return ( p_nb_item );
}

int List::char_width ( void )
{
   int max_width = strlen ( p_title );
   int tmp_width;
   int i;

   for ( i = 0 ; i < p_nb_item ; i++ )
   {
      tmp_width = strlen ( p_item [ i ].text );
      if ( tmp_width > max_width )
         max_width = tmp_width;
   }

   max_width = max_width + 3;

   return ( max_width );
}

void List::sensible ( bool value )
{
   p_sensible = value;
}

bool List::sensible ( void )
{
   return ( p_sensible );
}

int List::cursor ( void )
{
   return (p_cursor);
}

void List::cursor ( int value )
{
   p_cursor = value;
}

bool List::selected ( void )
{
   return (p_selected);
}

void List::selected ( bool value )
{
   p_selected = value;
}

int List::answer ( int index )
{
   return ( p_item [ index ] . answer);
}

int List::answer ( void )
{
   if ( p_nb_item > 0)
      return ( p_item [ p_cursor ] . answer );
   else
      return -1;
}

void List::header ( const char *text )
{
   p_showheader = true;
   strncpy ( p_header, text, List_HEADER_STR_SIZE);
}

bool List::showheader ( void )
{
   return ( p_showheader );
}

/*-------------------------------------------------------------------------*/
/*-                              Methods                                  -*/
/*-------------------------------------------------------------------------*/

/*void List::add_item ( string &str, bool mask )
{
   p_item [ p_nb_item ] . text = str;
   p_item [ p_nb_item ] . mask = mask;
   p_nb_item++;
} */

void List::add_item ( int answer, const char* str, bool mask )
{
//   string* tmp = new string;

//   *tmp = dat;
   if ( p_nb_item <= List_MAX_NB_ITEM )
   {


      strncpy ( p_item [ p_nb_item ] . text, str, List_STR_SIZE );
      p_item [ p_nb_item ] . mask = mask;
      p_item [ p_nb_item ] . answer = answer;


   //printf ("AddList test: %s, %d, %d", p_item [ p_nb_item ] . text,
   //        p_item [ p_nb_item ] . mask,
   //        p_item [ p_nb_item ] . answer );
      p_nb_item++;
   }
//   add_item ( *tmp, mask );
//   delete tmp;
}

 void List::add_itemf ( int answer, const char* format, ... )
 {
   va_list arguments;
   //int i;
   char tmpstr[List_STR_SIZE];


   va_start (arguments, format);
   vsprintf (tmpstr,format, arguments);
   va_end (arguments);

   add_item(answer, tmpstr);


 }

void List::add_query ( const char* field, const char* table, const char* condition )
{

   //sqlite3_stmt *sqlstatement;
   //const char *sqltail;
   int error;
   char tmpstr[81];
   char querystr[SQL_QUERYSTR_LEN];
   //unsigned char *tmpstr2;

   //printf("debug: 1\r\n");
   //error = sqlite3_prepare_v2 (SQLobject::p_sqldb, querystr, -1, &sqlstatement, &sqltail);
   sprintf (querystr, "SELECT %s,%s FROM %s %s", SQL_PRIMARY_KEY, field, table, condition);
   error = SQLprepare (querystr);

   //printf("debug ListWiz: querystr=%s \n", querystr);

   if (error == SQLITE_OK )
   {
      //printf ("debug ListWiz: pass1\n");
      //error = sqlite3_step (sqlstatement);
      error = SQLstep ();

      //printf ("debug ListWiz: pass2\n");
      if (error != SQLITE_ROW)
         SQLerrormsg ();
         //printf("\r\nError: Menu::add_query: SQLsteps: %s\r\n", error, sqlite3_errmsg(SQLobject::p_sqldb));

      while (error == SQLITE_ROW)
      {

         //printf ("debug ListWiz: pass3\n");
         strncpy ( tmpstr, SQLcolumn_text (1), Menu_STR_SIZE);
         add_item (SQLcolumn_int (0), tmpstr);
         error = SQLstep();

         //printf ("debug ListWiz: %s = %s\n", SQLcolumn_text (1), tmpstr );
      }

      error = SQLfinalize();
      //printf ("debug: 4");
      if ( error != SQLITE_OK)
         SQLerrormsg();
        // printf("Error: Menu::add_query: sqlite: %s\r\n", error, sqlite3_errmsg(SQLobject::p_sqldb));
   }
   else
   {

      //printf ("debug: 2");
      SQLerrormsg();
   }
   //else
     // printf("Error: Menu::add_query: sqlite: %s\r\n", error, sqlite3_errmsg(SQLobject::p_sqldb));

   //SQLfinalize();

}

void List::add_query_multi ( const char* field, const char*table, const char* condition, const char* format, int nb_field)
{
   // to be able to use format string, there is a restriction of up to 4 fields. Only use strings in format
   int error;
   char tmpstr[161] = "";
   char querystr[SQL_QUERYSTR_LEN];

   sprintf (querystr, "SELECT %s,%s FROM %s %s", SQL_PRIMARY_KEY, field, table, condition);
   error = SQLprepare (querystr);

   if (error == SQLITE_OK )
   {
      error = SQLstep ();

      if (error != SQLITE_ROW)
         SQLerrormsg ();

      while (error == SQLITE_ROW)
      {

         //printf ("debug ListWiz: pass3\n");
         switch ( nb_field )
         {
            case 1: sprintf ( tmpstr, format, SQLcolumn_text (1));
            break;
            case 2: sprintf ( tmpstr, format, SQLcolumn_text (1), SQLcolumn_text (2));
            break;
            case 3: sprintf ( tmpstr, format, SQLcolumn_text (1), SQLcolumn_text (2), SQLcolumn_text (3));
            break;
            case 4: sprintf ( tmpstr, format, SQLcolumn_text (1),SQLcolumn_text (2), SQLcolumn_text (3), SQLcolumn_text (4));
            break;
            case 5: sprintf ( tmpstr, format, SQLcolumn_text (1),SQLcolumn_text (2), SQLcolumn_text (3), SQLcolumn_text (4), SQLcolumn_text (5));
            break;
            case 6: sprintf ( tmpstr, format, SQLcolumn_text (1),SQLcolumn_text (2), SQLcolumn_text (3), SQLcolumn_text (4), SQLcolumn_text (5), SQLcolumn_text (6));
            break;
         }

         add_item (SQLcolumn_int (0), tmpstr);
         error = SQLstep();

         //printf ("debug ListWiz: %s \n", tmpstr );
      }

      error = SQLfinalize();

      if ( error != SQLITE_OK)
         SQLerrormsg();

   }
   else
   {
      SQLerrormsg();
   }
}


/*void List::update_item ( string &str, int index, bool mask )
{
   p_item [ index ] . text = str;
   p_item [ index ] . mask = mask;
} */

void List::update_selected_item ( int answer, const char *str, bool mask )
{
//   string* tmp = new string;

//   *tmp = dat;
   strncpy ( p_item [ p_cursor ] . text, str, List_STR_SIZE );
   p_item [ p_cursor ] . mask = mask;
   p_item [ p_cursor ] . answer = answer;


//   update_item ( *tmp, index, mask );
//   delete tmp;
}

void List::update_selected_itemf ( int answer, const char *format, ... )
{
   va_list arguments;
   //int i;
   char tmpstr[List_STR_SIZE];

   va_start (arguments, format);
   vsprintf (tmpstr, format, arguments);
   va_end (arguments);

   update_selected_item(answer, tmpstr);
}

void List::remove_selected_item ( void )
{
   int i;

   for  (i = p_cursor; i < p_nb_item; i++)
   {
      if ( i == p_nb_item - 1 || i == List_MAX_NB_ITEM - 1)
      {
         p_item [ i ] . answer = 0;
         p_item [ i ] . mask = false;
         strcpy (p_item [ i ] . text, "");
      }
      else
         p_item [ i ] = p_item [ i + 1 ];
   }

   p_nb_item--;
   if ( p_cursor >= p_nb_item )
      p_cursor = p_nb_item - 1;

}

/*
void List::add_item ( Weapon &wdat, int style = List_ADD_WEAPON_NAME,
                                                         bool mask = false )
{
   char tmpstr [ 51 ];

   switch ( style )
   {
      case List_ADD_WEAPON_NAME :
         p_item [ p_nb_item ] . text = wdat.name ();
         p_item [ p_nb_item ] . mask = mask;
      break;

      case List_ADD_WEAPON_BUY :
         sprintf ( tmpstr, "%s      %5d Gp  %2.2f Kg",
            wdat.cname (), wdat.price(), (float) wdat.weight () );
         p_item [ p_nb_item ] . text = tmpstr;
         p_item [ p_nb_item ] . mask = mask;
      break;
   }
   p_nb_item++;
}

void List::add_item ( Armor &adat, int style = List_ADD_ARMOR_NAME,
                                                         bool mask = false )
{
   char tmpstr [ 51 ];

   switch ( style )
   {
      case List_ADD_ARMOR_NAME :
         p_item [ p_nb_item ] . text = adat.name();
         p_item [ p_nb_item ] . mask = mask;
      break;

      case List_ADD_ARMOR_BUY :
         sprintf ( tmpstr, "%s      %5d Gp  %2.2f Kg",
            adat.cname (), adat.price(), (float) adat.weight ());
         p_item [ p_nb_item ] . text = tmpstr;
         p_item [ p_nb_item ] . mask = mask;
      break;

   }
   p_nb_item++;
}
  */
void List::mask_item ( int  ID )
{
   p_item [ ID ] . mask = true;
}

void List::unmask_all_item ( void )
{
   int i;

   for ( i = 0 ; i < List_MAX_NB_ITEM ; i++ )
      p_item [ i ] . mask = false;
}

void List::clear_all_item ( void )
{
   int i;

   for ( i = 0 ; i < List_MAX_NB_ITEM ; i++ )
   {
      p_item [ i ] . mask = false;
      strcpy ( p_item [ i ] . text,"" );
   }
   p_nb_item = 0;
}

int List::show ( int x, int y, bool reset_cursor, bool nocancel )

{
   int font_height = text_height ( List_FONT );
   int space = font_height / 2;
   int header_font_height = text_height ( List_HEADER_FONT );
   int arrow_space = text_length ( List_FONT, "-> ");
//   s_Menu_item *tmp_ptr;
   int i;
   //int cursor = cursor_pos;
   //int page = p_page_pos;
   int item_count = 0;
//   String tmp_str;
   char kbvalue;
   int base_y = 0;
   int yinc;
   int tmpanswer = -1;// = cursor_pos;
   bool answer_found = false;
   bool rereadkb = false;
   bool selected_masked = false;
   int clr = List_COLOR_TEXT;
   bool scroll = true;
   bool title = false;
   bool scrollup = false;
   bool scrolldown = false;
   int page_backup;

   //BITMAP* backbuf = create_bitmap ( SCREEN_W, SCREEN_H );

   p_selected = false;

   char shotstr [ 18 ];/*= { "lstshot1.bmp", "lstshot2.bmp", "lstshot3.bmp",
      "lstshot4.bmp", "lstshot5.bmp", "lstshot6.bmp", "lstshot7.bmp", "lstshot8.bmp" };*/
   static int shotID = 0;


   if ( reset_cursor == true )
   {
      p_page_pos = 0;
      p_cursor = 0;
   }


   if ( /*cursor < 0 || */p_page_pos >= p_nb_item )
      p_cursor = 0;

   if ( /*page < 0 || */p_page_pos >= p_nb_item )
      p_page_pos = 0;

   if ( p_cursor < p_page_pos )
      p_cursor = p_page_pos;

   if ( p_cursor > p_page_pos + p_page_size )
      p_page_pos = p_cursor;

   clear_keybuf();

   clr = List_COLOR_TEXT;
   // main loop
   while ( answer_found == false )
   {
      if ( scroll == true )
      {
         scroll = false;
         blit ( buffer, backup, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
         // draw or skip title
         if ( strcmp ( p_title, "" ) != 0 )
         {
            textprintf_old ( subbackup, List_FONT, x, y, List_COLOR_TEXT,
               "   %s", p_title );
            base_y = y + font_height;
            title = true;
         }
         else
            base_y = y;

         if ( p_showheader == true)
         {
            textout_old ( subbackup, List_HEADER_FONT, p_header, x + arrow_space, base_y, List_COLOR_TEXT );
            base_y += header_font_height + List_HEADER_SPACE;
         }
         if ( title == true)
            base_y += space;

         yinc = base_y;
         item_count = 0;
         selected_masked = false;
         scrollup = false;
         scrolldown = false;

         i = p_page_pos;
         while ( i < p_page_pos + p_page_size && i < p_nb_item )
         {
            if ( p_item [ i ] . mask == true )
               { clr = List_COLOR_MASK; }
            else
               { clr = List_COLOR_TEXT; }

               if ( item_count == 0 && p_page_pos > 0 )
               {
                  textprintf_old ( subbackup, List_FONT, x, yinc, clr, "  -%s",
                     p_item [ i ] . text );
                  scrollup = true;
               }
               else
                  if ( item_count == ( p_page_size - 1 ) &&
                     ( item_count + p_page_pos ) != ( p_nb_item -1 ) )
                  {
                     textprintf_old ( subbackup, List_FONT, x, yinc, clr, "  +%s",
                        p_item [ i ] . text );
                     scrolldown = true;
                  }
                  else
                     textprintf_old ( subbackup, List_FONT, x, yinc, clr, "   %s",
                        p_item [ i ] . text );

            //printf ("List test: %s", p_item [ i ] . text );
            yinc = yinc + font_height;
            item_count++;
            i++;
         }

      }

      copy_backup_buffer_keep();

      yinc = base_y;
      item_count = 0;
      selected_masked = false;

      i = p_page_pos;
      clr = List_COLOR_TEXT;
      while ( i < p_page_pos + p_page_size && i < p_nb_item )
      {
         if ( item_count + p_page_pos == p_cursor )
         {
//            if ( item_count == 0 && page > 0 )
               textprintf_old ( subscreen, List_FONT, x, yinc, clr, "->" );
               selected_masked = p_item [ i ] . mask;
               tmpanswer = p_item [ i ] . answer;


//            else
//               if ( item_count == ( p_page_size - 1 ) &&
//                  ( item_count + page ) != ( p_nb_item -1 ) )
//                  textprintf_old ( subscreen, List_FONT, x, yinc, clr, "->+%s",
//                     p_item [ i ] . text.data() );
//               else
//               {
//                  textprintf_old ( subscreen, List_FONT, x, yinc, clr, "-> %s",
//                     p_item [ i ] .  text.data() );
//                  selected_masked = p_item [ i ] . mask;
//               }
         }

         yinc = yinc + font_height;
         item_count++;
         i++;
      }

      // Keyboard reading

      rereadkb = true;

      while ( rereadkb == true )
      {
         kbvalue = mainloop_readkeyboard();
         clear_keybuf();

         if ( kbvalue == -1)
            rereadkb = false;
         else
            rereadkb = true;


         if ( kbvalue == SELECT_KEY )
         {
            if ( selected_masked == false )
            {
               //answer = cursor;
               rereadkb = false;
               answer_found = true;
               if (p_sensible == true)
                  p_selected = true;
            }
         }

         if ( kbvalue == CANCEL_KEY )
         {
            if ( nocancel == true )
               rereadkb = true;
            else
            {
               tmpanswer = -1;
               rereadkb = false;
               answer_found = true;
               if (p_sensible == true)
                  p_selected = true;
            }
         }

         if ( kbvalue == KEY_UP )
         {
            if ( p_cursor > 0 )
            {
               p_cursor--;
               rereadkb = false;
               if ( p_cursor < p_page_pos )
               {
                  p_page_pos--;
                  scroll = true;
               }
               if ( p_sensible == true )
               {
                  tmpanswer = p_item [ p_cursor ] . answer;
                  rereadkb = false;
                  answer_found = true;
               }
            }
         }

         if ( kbvalue == KEY_DOWN )
         {
            if ( p_cursor < ( p_nb_item -1 ) )
            {
               p_cursor++;
               rereadkb = false;
               if ( p_cursor > ( p_page_pos + p_page_size - 1 ) )
               {
                  p_page_pos++;
                  scroll = true;
               }

               if ( p_sensible == true )
               {
                  tmpanswer = p_item [ p_cursor ] . answer;
                  rereadkb = false;
                  answer_found = true;
               }
            }
         }

         if ( kbvalue == KEY_PGDN )
         {
            if ( scrolldown == true)
            {
               rereadkb = false;

               if ( p_page_pos < p_nb_item - p_page_size)
               {
                  scroll = true;
                  page_backup = p_page_pos;
                  p_page_pos += p_page_size - 1;
                  //p_cursor += p_page_size -1;

                  if ( p_page_pos > p_nb_item - p_page_size)
                     p_page_pos = p_nb_item - p_page_size;

                  p_cursor += p_page_pos - page_backup;

                  if ( p_cursor >= p_nb_item )
                     p_cursor = p_nb_item -1;
               }
            }
         }

         if ( kbvalue == KEY_PGUP )
         {
            if ( scrollup == true)
            {
               rereadkb = false;

               if ( p_page_pos > 0)
               {
                  scroll = true;
                  page_backup = p_page_pos;
                  p_page_pos -= ( p_page_size - 1);
                  //p_cursor -= ( p_page_size - 1);

                  if ( p_page_pos < 0)
                     p_page_pos = 0;

                  p_cursor += p_page_pos - page_backup;

                  if ( p_cursor < 0 )
                     p_cursor = 0;
               }
            }
         }

         if ( kbvalue == KEY_SPACE )
         {
            sprintf ( shotstr, "listshot%03d.bmp", shotID);
            make_screen_shot ( shotstr  );
            shotID++;
         }

      }
   }

   //destroy_bitmap ( backbuf );
   //p_page_pos = page;
   clear ( buffer );
   return ( tmpanswer );
}


