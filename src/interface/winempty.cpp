/***************************************************************************/
/*                                                                         */
/*                        W I N E M P T Y . C P P                          */
/*                          Class Source Code                              */
/*                                                                         */
/*     Engine : Window                                                     */
/*     Content : Class WinEmpty                                            */
/*     Programmer ; Eric PIetrocupo                                        */
/*     Starting Date : November 7th, 2002                                  */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>




/*
//#include <time.h>
#include <allegro.h>

//#include <datafile.h>
//#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
#include <winempty.h>
*/

/*-------------------------------------------------------------------------*/
/*-                     Constructor & Destructor                          -*/
/*-------------------------------------------------------------------------*/

WinEmpty::WinEmpty ( short x1, short y1, short x2, short y2, bool translucent )
{
   p_x_pos = x1;
   p_y_pos = y1;
   p_width = ( x2 - x1 ) + 1;
   p_height = ( y2 - y1 ) + 1;
   p_backup = create_bitmap ( p_width, p_height );
   p_translucent = translucent;
   preshow();
}
/*
WinEmpty::~WinEmpty ( void )
{

} */

/*-------------------------------------------------------------------------*/
/*-                           Virtual Methods                             -*/
/*-------------------------------------------------------------------------*/

void WinEmpty::preshow ( void )
{
   clear_to_color (subbuffer, makecol ( 255, 0, 255) );
   WinEmpty::show ();
   clear ( buffer );
}

void WinEmpty::refresh ( void )
{
   clear_to_color (subbuffer, makecol ( 255, 0, 255) );
   WinEmpty::show();
   clear( buffer );
}

short WinEmpty::show ( void )
{
   border_fill ();
   blit ( subbuffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );
   draw_instruction();
   return ( 0 );
}

int WinEmpty::type ( void )
{
   return ( Window_TYPE_EMPTY );
}


