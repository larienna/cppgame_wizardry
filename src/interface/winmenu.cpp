/***************************************************************************/
/*                                                                         */
/*                       W I N M E N U . C P P                             */
/*                        Class Source Code                                */
/*                                                                         */
/*     Content : Class WinMenu                                             */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : Novemberth, 11th, 2002                              */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>



/*
//#include <time.h>
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
//#include <winempty.h>
//#include <wintitle.h>
#include <winmenu.h>
*/

/*-------------------------------------------------------------------------*/
/*-                      Constructor & Destructor                         -*/
/*-------------------------------------------------------------------------*/

WinMenu::WinMenu ( Menu &mnu, short x_pos, short y_pos,
                        bool no_cancel, bool reset_cursor, bool translucent )
{
   short chr_width;
   int font_width = text_length ( Menu_FONT, "a" );
   int font_height = text_height ( Menu_FONT );
   int header_font_height = text_height ( Menu_HEADER_FONT );

   p_menu = &mnu;
   p_x_pos = x_pos;
   p_y_pos = y_pos;
   p_no_cancel = no_cancel;
   p_reset_cursor = reset_cursor;
   chr_width = p_menu->char_width();
   p_width = 6 + ( font_width * ( chr_width + 3 ) ) + 6 + 1;
   p_height = 6 + ( font_height * p_menu->nb_item() ) + 6 + 1;
   p_cursor = 0;
   p_translucent = translucent;

   if ( p_menu->showheader() == true)
   {
      p_height += header_font_height + Menu_HEADER_SPACE;
   }

   if ( strcmp ( p_menu->title(), "" ) != 0 )
      p_height = p_height + font_height + ( font_height / 2 );

   p_backup = create_bitmap ( p_width, p_height );

   if ( p_casc_active == true && p_casc_display == true)
   {
      p_casc_level = p_casc_next_level;
      p_casc_next_level++;
      p_x_pos += p_casc_width * p_casc_level;
      p_y_pos += p_casc_width * p_casc_level;

   }


}

/*WinMenu::~WinMenu ( void )
{

} */

/*-------------------------------------------------------------------------*/
/*-                         Virtual Methods                               -*/
/*-------------------------------------------------------------------------*/

void WinMenu::preshow ( void )
{
   border_fill ();
   p_menu->draw( p_x_pos + 6, p_y_pos + 6 );
   blit ( subbuffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );
   clear( buffer );
}

void WinMenu::refresh ( void )
{
   // this function does nothing
}

short WinMenu::show ( void )
{
   short tmpx;
   short tmpy;
   int answer = 0;

   tmpx = p_x_pos + 6;
   tmpy = p_y_pos + 6;

   p_inst_order = Window_INSTRUCTION_SELECT +
      Window_INSTRUCTION_MENU;
   if ( p_no_cancel == false)
      p_inst_order += Window_INSTRUCTION_CANCEL;

   border_fill ();
   //if ( p_reset_cursor == true )
   //   p_cursor = 0;

//   p_menu->draw ( tmpx, tmpy );

   draw_instruction();
   answer = p_menu->show( tmpx, tmpy, p_no_cancel, p_reset_cursor );
   blit ( subscreen, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );
   draw_instruction();
   return ( answer );
}

int WinMenu::type ( void )
{
   return ( Window_TYPE_MENU );
}



