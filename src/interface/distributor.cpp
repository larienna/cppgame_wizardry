/***************************************************************************/
/*                                                                         */
/*                        D I S T R I B U T O R . C P P                    */
/*                            Class definition                             */
/*                                                                         */
/*     Content : Class Distributor                                         */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : May 17th, 2012                                      */
/*                                                                         */
/*          This is an distributor menu that is primarily used to assign   */
/*     attributes. The idea that that is allows to split a value into      */
/*     different values.                                                   */
/*                                                                         */
/***************************************************************************/

#include <grpsys.h>
#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>

/*-------------------------------------------------------------------------*/
/*-                  Constructor and Destructor                           -*/
/*-------------------------------------------------------------------------*/

Distributor::Distributor ( void )
{
   s_Distributor_item tmpitem;
   int i;

   strcpy ( p_title, "");
   strcpy ( p_total, "");
   strcpy ( p_header, "");
   p_nb_item = 0;
   p_total_value = 0;

   strcpy ( tmpitem.text, "" );
   tmpitem.min = 0;
   tmpitem.max = 0;
   tmpitem.current = 0;

   for  ( i = 0; i < Distributor_NB_ITEM; i++)
   {
      p_item [ i ] = tmpitem;
   }

   p_nb_digit = 2;
}

Distributor::Distributor ( const char* title, const char *header, const char *total, int nb_digit )
{
   //this.Distributor();
   Distributor();

   strncpy ( p_title, title, Distributor_TITLE_SIZE);
   strncpy ( p_total, total, Distributor_TEXT_SIZE);
   strncpy ( p_header, header,  Distributor_HEADER_STR_SIZE);
   p_nb_digit = nb_digit;

}

Distributor::~Distributor ( void )
{

}

/*-------------------------------------------------------------------------*/
/*-                         Property Methods                              -*/
/*-------------------------------------------------------------------------*/



void Distributor::title ( const char* str )
{
   strncpy ( p_title, str, Distributor_TITLE_SIZE);

}

void Distributor::header ( const char* str )
{

   strncpy ( p_header, str,  Distributor_HEADER_STR_SIZE);
}

void Distributor::total ( const char* str )
{

   strncpy ( p_total, str, Distributor_TEXT_SIZE);

}

void Distributor::total_value ( int value )
{
   p_total_value = value;
}

int Distributor::total_value ( void )
{
   return ( p_total_value );
}

int Distributor::nb_item ()
{
   return ( p_nb_item );
}

int Distributor::item_value ( int index )
{
   return ( p_item [ index ] . current );
}

int Distributor::nb_digit ( void )
{
   return ( p_nb_digit );
}

void Distributor::nb_digit ( int value )
{
   p_nb_digit = value;
}


/*-------------------------------------------------------------------------*/
/*-                              Methods                                  -*/
/*-------------------------------------------------------------------------*/


void Distributor::add_item ( const char *text, int min, int max )
{
   s_Distributor_item tmpitem;

   strncpy ( tmpitem.text, text, Distributor_TEXT_SIZE );
   tmpitem.min = min;
   tmpitem.max = max;
   tmpitem.current = min;

   p_item [ p_nb_item ] = tmpitem;

   p_nb_item++;

}

int Distributor::show ( int x, int y, bool distribute_all, bool nocancel )
{

   int font_height = text_height ( Distributor_FONT );
   //int space = font_height / 2;
   int header_font_height = text_height ( Distributor_HEADER_FONT );
   int font_width = text_length ( Distributor_FONT, "a");
   int cursor = 0;
   char kbvalue;
   short base_y = y;
   bool rereadkb = false;
   int yinc;
   int tmpx;
   int i;
   int j;
   //int k;
   bool exit_distributor = false;
   bool value_changed = true;
   //BITMAP* backbuf = create_bitmap ( 640, 480 );
   char format [ 81 ];
   int retval = Distributor_ACCEPT;
   int pass;


   while ( exit_distributor == false )
   {

      if ( value_changed == true )
      {

         blit ( buffer, backup, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
         value_changed = false;

         // building up the header

         textprintf_old ( subbackup , Distributor_FONT, x, y, Distributor_COLOR_TEXT,
               "%s", p_title );
         base_y = y + font_height;

         textout_old ( subbackup, Distributor_HEADER_FONT, p_header, x, base_y, Distributor_COLOR_TEXT );

         tmpx = x + ( font_width * Distributor_TEXT_SIZE )
            + ( font_width * ( ( p_nb_digit + 2 ) / 2 )) ;

         textout_centre_old ( subbackup, Distributor_HEADER_FONT, "min", tmpx, base_y, Distributor_COLOR_TEXT );

         tmpx += font_width * ( 3 + p_nb_digit + 1
                      + p_nb_digit + 2 );

         textout_centre_old ( subbackup, Distributor_HEADER_FONT, "max", tmpx, base_y, Distributor_COLOR_TEXT );

         base_y += header_font_height + Distributor_HEADER_SPACE + ( font_height / 2 );

         // build up the core menu

         yinc = base_y;

         for ( i = 0; i < p_nb_item; i++)
         {
            strcpy (format, "%-20s [%-*d]    %-*d [%-*d]" );
            //strcat (format, Distributor_MINMAX_FORMAT );

            //strcat ( format, "   ");

            //strcat ( format, Distributor_VALUE_FORMAT );
            //strcat ( format, " ");
            //strcat ( format, Distributor_MINMAX_FORMAT );

            textprintf_old ( subbackup, Distributor_FONT, x, yinc, Distributor_COLOR_TEXT,
               format, p_item [ i ] . text, p_nb_digit,  p_item [ i ] . min,
               p_nb_digit, p_item [ i ] . current,
               p_nb_digit, p_item [ i ] . max );

            yinc = yinc + font_height;
         }


         strcpy (format, "%-20s ");

         for ( j = 0; j < p_nb_digit + 6; j++)
            strcat ( format, " ");
         strcat ( format, "%-*d" );

         textprintf_old ( subbackup, Distributor_FONT, x, yinc, Distributor_COLOR_TEXT,
            format, p_total, p_nb_digit, p_total_value );

      }

      // draw only arrow if no value has been changed
      yinc = base_y;

//      vsync ();
  //    blit ( backup, screen , 0 , 0 , 0 , 0 , 640 , 480 );
      copy_backup_buffer_keep();


      //tmpx = tmpx = x + ( font_width * ( Distributor_TEXT_SIZE + p_nb_digit + 2));

      for ( j = 0 ; j < p_nb_item ; j++ )
      {
         if ( j == cursor )
            textprintf_old ( subscreen, Distributor_FONT, tmpx, yinc, Option_COLOR_TEXT,
               " ->" );
         yinc = yinc + font_height;
      }

      // Keyboard reading

      rereadkb = true;

      while ( rereadkb == true )
      {
         kbvalue = mainloop_readkeyboard();

         if ( kbvalue == -1)
            rereadkb = false;
         else
            rereadkb = true;

         if ( kbvalue == SELECT_KEY )
         {
            pass = false;
            if ( distribute_all == true )
            {
               if ( p_total_value == 0 )
                  pass = true;
            }
            else
               pass = true;


            if ( pass == true)
            {
               rereadkb = false;
               exit_distributor = true;
               retval = Distributor_ACCEPT;
            }

         }

         if ( kbvalue == CANCEL_KEY )
         {
            if ( nocancel == false )
            {
               rereadkb = false;
               exit_distributor = true;
               retval = Distributor_CANCEL;
            }
         }


         if ( kbvalue == KEY_UP )
         {
            if ( cursor > 0 )
            {
               cursor--;
               rereadkb = false;
            }
         }

         if ( kbvalue == KEY_DOWN )
         {
            if ( cursor < p_nb_item - 1 )
            {
               cursor++;
               rereadkb = false;
            }
         }

         if ( kbvalue == KEY_LEFT )
         {
            if ( p_item [ cursor ] . current > p_item [ cursor ] . min )
            {
               p_item [ cursor ] . current--;
               p_total_value++;

               value_changed = true;
               rereadkb = false;
            }
         }

         if ( kbvalue == KEY_RIGHT )
         {
            if ( p_item [ cursor ] . current < p_item [ cursor ] . max
                && p_total_value > 0 )
            {
               p_item [ cursor ] . current++;
               p_total_value--;

               value_changed = true;
               rereadkb = false;
            }
         }
      }
   }

   //destroy_bitmap ( backbuf );
   clear ( buffer );

   return (retval);
}

/*-------------------------------------------------------------------------*/
/*-                         Private Methods                               -*/
/*-------------------------------------------------------------------------*/
