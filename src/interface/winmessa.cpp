/***************************************************************************/
/*                                                                         */
/*                         W I N M E S S A . C P P                         */
/*                            Class source code                            */
/*                                                                         */
/*     Content : Class WinMessage                                          */
/*     Programmer : Eric PIetrocupo                                        */
/*     Starting Date : November 23rd, 2002                                 */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
#include <grpinterface.h>
//#include <grpengine.h>



/*
//#include <time.h>
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//
//
//
//
//
//
//#include <list.h>
//#include <opponent.h>
//#include <charactr.h>
//#include <monster.h>
//#include <party.h>
//
//#include <game.h>
//#include <city.h>
//#include <maze.h>
//
//#include <camp.h>
//#include <config.h>
//#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
//#include <winempty.h>
//#include <wintitle.h>
#include <winmessa.h>
*/

/*-------------------------------------------------------------------------*/
/*-                  Constructors and Destructors                         -*/
/*-------------------------------------------------------------------------*/

/*WinMessage::WinMessage ( string &message, short center_x, short y_pos )
{
   construct ( message, center_x, y_pos );
} */

WinMessage::WinMessage ( const char *message, short center_x, short y_pos, bool translucent )
{
//   string *tmpstr = new string;
//   *tmpstr = message;

   p_translucent = translucent;
   construct ( message, center_x, y_pos );

//   delete tmpstr;
}

/*
WinMessage::~WinMessage ( void )
{

} */

/*-------------------------------------------------------------------------*/
/*-                          Virtual Methods                              -*/
/*-------------------------------------------------------------------------*/

void WinMessage::preshow ( void )
{
   clear_to_color (subbuffer, makecol ( 255, 0, 255) );

   short xchar = p_x_pos + 10;
   short ychar = p_y_pos + 6;
   short length;
   short i;
   short font_width = text_length ( WinMessage_FONT, "a" );
   short font_height = text_height ( WinMessage_FONT );
//   string tmpstr;
//   char tmpchr;

   border_fill ();

   length = strlen ( p_message );
   i = 0;

   while ( i < length && p_message [ i ] != '$' )
   {
      if ( p_message [ i ] == '\n' || p_message [ i ] == '^' )
      {
         xchar = p_x_pos + 10;
         ychar = ychar + font_height;
      }
      else
      {
//         tmpchr = p_message [ i ]; // change a character in a string
         textprintf_old ( subbuffer, WinMessage_FONT, xchar, ychar,
            General_COLOR_TEXT, "%c", p_message [ i ] );
         xchar = xchar + font_width;
      }
      i++;
   }

   blit ( subbuffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );
   clear( buffer );
}

void WinMessage::refresh ( void )
{
   // this function does nothing
}

short WinMessage::show ( void )
{
   short xchar = p_x_pos + 10;
   short ychar = p_y_pos + 6;
   short length;
   short i;
   short font_width = text_length ( WinMessage_FONT, "a" );
   short font_height = text_height ( WinMessage_FONT );
   int inst_x_backup;
   int inst_y_backup;
   int inst_order_backup;
//   string tmpstr;
   //char tmpchr;

   clear_keybuf();
   border_fill ();

   length = strlen ( p_message );

   i = 0;
   while ( i < length && p_message [ i ] != '$' )
   {
      if ( p_message [ i ] == '\n' || p_message [ i ] == '^' )
      {
         xchar = p_x_pos + 10;
         ychar = ychar + font_height;
      }
      else
      {
//         tmpchr = p_message [ i ]; // change a character in a string
         textprintf_old ( subbuffer, WinMessage_FONT, xchar, ychar,
            General_COLOR_TEXT, "%c", p_message [ i ] );
         xchar = xchar + font_width;
      }
      i++;
   }

// remove temporarily until instruction fusioned with window
//   draw_instruction ( Draw_INSTRUCTION_SELECT, centerx, ychar + font_height );

   inst_x_backup = p_inst_center_x;
   inst_y_backup = p_inst_y;
   inst_order_backup = p_inst_order;

   p_inst_order = Window_INSTRUCTION_SELECT;
   p_inst_center_x = p_center_x;
   p_inst_y = ychar + font_height;
   draw_instruction();
   copy_buffer ();

   while ( mainloop_readkeyboard() != SELECT_KEY );

   blit ( subbuffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );

   p_inst_center_x = inst_x_backup;
   p_inst_y = inst_y_backup;
   p_inst_order = inst_order_backup;


   return ( 0 );
}

int WinMessage::type ( void )
{
   return ( Window_TYPE_MESSAGE );
}

/*-------------------------------------------------------------------------*/
/*-                        Private Methods                                -*/
/*-------------------------------------------------------------------------*/


void WinMessage::construct ( const char *message, short center_x, short y_pos )
{
   int nb_line = 0;
   int nb_letter = 0;
   short font_height = text_height ( WinMessage_FONT );
   short font_width = text_length ( WinMessage_FONT, "a" );

   strncpy (p_message, message, 241 );
   p_center_x = center_x;

   nb_line = line_count();
   nb_letter = max_line_size();

   p_x_pos = p_center_x - ( ( ( font_width * nb_letter ) / 2 ) + 4);
   p_y_pos = y_pos;
   p_width = 6 + ( font_width * nb_letter ) + 6 + 1 + 8;
   p_height = 6 + ( font_height * ( nb_line + 1 ) ) + 6 + 1;
   p_backup = create_bitmap ( p_width, p_height );
}

int WinMessage::line_count ( void )
{
   short length = strlen ( p_message );
   short i;
   int nb_line = 1;

   i = 0;
   while ( i < length && p_message [ i ] != '$' )
   {
      if ( p_message [ i ] == '\n' || p_message [ i ] == '^' )
         nb_line++;
      i++;
   }
   return ( nb_line );
}

int WinMessage::max_line_size ( void )
{
   short length = strlen ( p_message );
   short i;
   int nb_letter = 0;
   int tmp_nb_letter = 0;

   i = 0;
   while ( i < length && p_message [ i ] != '$' )
   {
      tmp_nb_letter++;
      if ( p_message [ i ] == '\n' || p_message [ i ] == '^' )
      {
         if ( tmp_nb_letter > nb_letter )
            nb_letter = tmp_nb_letter;
         tmp_nb_letter = 0;
      }
      i++;
   }
   if ( tmp_nb_letter > nb_letter )
      nb_letter = tmp_nb_letter;

   return ( nb_letter );
}
