/***************************************************************************/
/*                                                                         */
/*                        W D A T P R O C . C P P                          */
/*                                                                         */
/*     Content : Module WinData Procedures                                 */
/*     Programmer : Eirc Pietrocupo                                        */
/*     Starting Date : November 15th, 2002                                 */
/*                                                                         */
/***************************************************************************/

// Include Groups
#include <grpsys.h>
#include <grpstd.h>
#include <grpsql.h>
#include <grpdbobj.h>
#include <grpinterface.h>
#include <grpengine.h>



/*
#include <allegro.h>

#include <datafile.h>
#include <advdatf.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
//#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>






//#include <list.h>
#include <opponent.h>
#include <charactr.h>
#include <ennemy.h>

#include <cclass.h>
#include <party.h>


#include <game.h>
#include <city.h>
//#include <maze.h> // ?? to be removed
//
#include <camp.h>
//#include <config.h>
#include <draw.h>
//#include <dialog.h>
//#include <combat.h>
#include <window.h>
//#include <winempty.h>
//#include <wintitle.h>
//#include <winmenu.h>
#include <windata.h>
#include <wdatproc.h>
*/

/*-------------------------------------------------------------------------*/
/*-                      Procedures                                       -*/
/*-------------------------------------------------------------------------*/

void WDatProc_party_bar ( int key, int x, int y )
{
//   Party &player_party = party;
//   short height = 25;
   int font_height = text_height ( General_FONT_PARTY );
   //int font_width = text_length ( General_FONT_PARTY, "a" );
   short ystart;
//   Character tmpcharacter;
   //int i;
   //int nb_character = 0;
   //int character_id = 0;
//   string tmpstr;
   char tmpstr [ 25 ];
   int color;
   Character tmpcharacter;
   CClass tmpclass;
   Race tmprace;
   Action tmpaction;
   //char tmpchar;
   int error;
   char position [6];
   //char querystr [100];
   //char commandstr [100];
   //int action_error;
   //char position2 [6]; // debugging
   //int exppercent;

   Character::compile_ranks();

//   nb_character = party.nb_character();
//   height = height + ( nb_character * font_height );
//   draw_border_fill ( 0, 480 - height , 639 , 479,
//      General_COLOR_BORDER, General_COLOR_FILL );

//printf ( "wdat: pass 1\r\n");

   ystart = y + 14 + 6;/*480 - height + 6 + 14*/;

   textout_old ( subbuffer, General_FONT_INSTRUCTION,
      "Positn. Name               Level  Exp%  Race Class     HP        MP      Status",
      x + 7, ystart - 14, General_COLOR_TEXT );

   //debug_printf( __func__, "Before");
   //nb_character = SQLcount ("name", "character", "WHERE location=2 ORDER BY position") - 1;
   //debug_printf( __func__, "After");


   //printf ( "\nwdat: nb_character=%d\r\n", nb_character );

   //debug_printf( __func__, "Before Draw party bar");
   error = tmpcharacter.SQLprepare ("WHERE location=2 ORDER BY position");
   //debug_printf( __func__, "After Draw party bar");

   //printf ("IS IT HERE!\n");

//printf ( "\nwdat: error=%d, SQLITEOK=%d\r\n", error, SQLITE_OK );

   if ( error == SQLITE_OK )
   {

//printf ( "wdat: pass 3\r\n");
      error = tmpcharacter.SQLstep();

      while ( error == SQLITE_ROW )
      {
//printf ( "wdat: pass 4\r\n");
         //strcpy ( tmpstr, tmpcharacter.statusS() );
         color = General_COLOR_DISABLE;
         strcpy ( position,"---");

         strcpy ( position, Party_STR_RANK [ tmpcharacter.rank() ] );
         strcpy ( tmpstr, "" );

         if ( tmpcharacter.body() == Character_BODY_ALIVE )
         {

            color = General_COLOR_TEXT;



            //strcpy ( position, Party_STR_RANK [ Party_FORMATION_POSITION [ party.formation() ] [nb_character] [ character_id]  ] );

            /*switch ( Party_FORMATION_POSITION [ party.formation() ] [nb_character] [ character_id])
            {
               case Party_FRONT: strcpy ( position,"Front"); break;
               case Party_BACK: strcpy ( position,"Back"); break;
               case Party_NONE: strcpy ( position,"???"); break;
            }*/
         }
         else
         {
            sprintf ( tmpstr, "%s", STR_CHR_BODY [ tmpcharacter.body() ] );
         }

         /*if ( tmpcharacter.check_levelup() == true )
            tmpchar = '@';
         else
            tmpchar = ' ';*/

/*// 3D table: Formation type, nb characters, position
extern int Party_FORMATION_POSITION [ 3 ] [ 6 ] [ 6 ];
*/
         // select command to display
         // this is never displayed becaue the commands are input into the DB after all actions are done.
         //sprintf ( querystr, "WHERE actor_type=%d and actorID=%d", Action_ACTOR_TYPE_CHARACTER, tmpcharacter.primary_key() );
         /*strcpy ( commandstr, "" );
         action_error = tmpaction.SQLpreparef ( "WHERE actor_type=%d and actorID=%d", Action_ACTOR_TYPE_CHARACTER, tmpcharacter.primary_key() );

         if ( action_error == SQLITE_OK)
         {
            action_error = tmpaction.SQLstep();

            if ( action_error == SQLITE_ROW )
               tmpaction.command_str ( commandstr );
         }
         tmpaction.SQLfinalize();*/

         int EXPpreviouslevel;
         int EXPforlevel;
         int EXPcurrent;
         int EXPperc;

         if ( tmpcharacter.level() > 1)
            EXPpreviouslevel = EXP_TABLE [ tmpcharacter.level() -2];
         else
            EXPpreviouslevel = 0;

         EXPforlevel = tmpcharacter.next_level_exp() - EXPpreviouslevel;

         if ( tmpcharacter.exp() >= tmpcharacter.next_level_exp())
            EXPcurrent = tmpcharacter.next_level_exp() - EXPpreviouslevel;
         else
            EXPcurrent = tmpcharacter.exp() - EXPpreviouslevel;

               //testing code
               //EXPcurrent += 3000;

         EXPperc = ( EXPcurrent * 100) / EXPforlevel;


         tmpclass.SQLselect ( tmpcharacter.FKcclass() );
         tmprace.SQLselect ( tmpcharacter.FKrace() );
         //int tmpcondition = tmpcharacter.compile_condition ();

         //printf ( "debug: wdatproc: condition %d", tmpcondition );
         /*for ( i = 0 ; i < EnhancedSQLobject_HASH_SIZE; i++)
            if ( ( indextobit ( i ) & tmpcondition ) > 0 )
               strcat ( tmpstr, STRFLD_CONDITION [ i ] . code );*/

         ActiveEffect tmpeffect;
         int error2;
         error2 = tmpeffect.SQLpreparef ( "WHERE target_type=%d AND target_id=%d AND kaomoji <> ''", Opponent_TYPE_CHARACTER, tmpcharacter.primary_key() );
         strcpy ( tmpstr, "|");
         int countface = 0;

         if ( error2 == SQLITE_OK)
         {
            error2 = tmpeffect.SQLstep ();

            while ( error2 == SQLITE_ROW )
            {
        // printf ( "Debug: Character: Compile Condition: effect condition=%d conditionstr=",
          //       tmpeffect.condition(), tmpeffect.condition_str() );

               if ( countface < 6 ) // only display 5 kaomoji
               {
                  strcat ( tmpstr, tmpeffect.kaomoji() );
                  strcat ( tmpstr, "|");
                  countface++;
               }
               error2 = tmpeffect.SQLstep ();
            }

            tmpeffect.SQLfinalize ();
         }


         textprintf_old ( subbuffer, General_FONT_PARTY, x + 7, ystart
         , color, "%-5s %-15s %2d %3d%% %3s %3s %3d/%3d %3d/%3d %-19s",
          position, tmpcharacter.name(), /*tmpcharacter.sexC(),
          tmpcharacter.aligmentC(),*/ tmpcharacter.level(), //tmpchar,
          EXPperc,
          tmprace.initial(), tmpclass.initial (),
          tmpcharacter.current_HP (), tmpcharacter.max_HP(),
          tmpcharacter.current_MP(), tmpcharacter.max_MP(),
          /*( const char* )*/ tmpstr /*, commandstr*/ );

/*2nd party bar call
textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy
         , General_COLOR_TEXT, "    %-15s %2d%c %3s %3s %3d/%3d %3d/%3d %-16s",
          tmpchar.name(), tmpchar.level(), lvlchar, tmprace.initial(), tmpclass.initial (),
          tmpchar.current_HP (), tmpchar.max_HP(), tmpchar.current_MP(), tmpchar.max_MP(),
          tmpstr );*/

  /*    if ( tmpcharacter.status() == Opponent_STATUS_ALIVE )
      {
         draw_health_status ( tmpcharacter.healthS()
                                     , x + ( font_width * 46 ) + 6, ystart );
      }
*/
         ystart = ystart + font_height;


//      tmpcharacter.combatstr ("Allo");

/*      int tmptag = party.character ( i );
      textprintf_old ( subbuffer, font,  0, 32, General_COLOR_TEXT,
         "Char.Party Tag : T:%d S:%d K:%d", tmptag.table(),
         tmptag.source(), tmptag.key() );

      copy_buffer();
      while ( ( readkey() >> 8 ) != KEY_ENTER );*/

         // if character dead, does not count in party formation.
         //if ( tmpcharacter.body() == Character_BODY_ALIVE )
            //character_id++;


         error = tmpcharacter.SQLstep();

      }






   }
   tmpcharacter.SQLfinalize();
//   debug_printf( __func__, "After Finalize");

}

void WDatProc_ennemy_bar ( int key , int x, int y )
{
 // todo: new bar
 // query monster list, show name, rank, health level, combat status (paralysed, etc);
 // max party bar size is now 8 monsters.

   int font_height = text_height ( General_FONT_PARTY );
   //int font_width = text_length ( General_FONT_PARTY, "a" );
   int ystart = y + 14 + 6;
   int i;
   //int nb_monster = 0;
   Monster tmpmonster;
   MonsterCategory tmpcategory;
   int error;
   int color; // hold color to gray out killed monsters
   int monster_id = 0; //used to identify monster position in party.
   char position [6];
   char tmpstr[25] = ""; // used to hold the status to the creature
   int HPbar; // temporary variable to hold the number of dots in the HP bar like [|||--]
   char HPbarstr [8]; // hold a copy of the string bar like [|||--]
   //char tmpname [Monster_NAME_STRLEN];


   Monster::compile_ranks();

   textout_old ( subbuffer, General_FONT_INSTRUCTION,
      "Positn. Name                                  HP           Status",
      x + 7, ystart - 14, General_COLOR_TEXT );

   //nb_monster = SQLcount ("name", "monster", "ORDER BY position") - 1;

   error = tmpmonster.SQLprepare ("ORDER BY position");

   if ( error == SQLITE_OK )
   {

//printf ( "wdat: pass 3\r\n");
      error = tmpmonster.SQLstep();

      while ( error == SQLITE_ROW )
      {
         color = General_COLOR_TEXT;

         if ( tmpmonster.body() != Character_BODY_ALIVE )
         {


            color = General_COLOR_DISABLE;
            sprintf ( tmpstr, "%s", STR_CHR_BODY [ tmpmonster.body() ] );
         }
         else
         {
            ActiveEffect tmpeffect;
            int error2;
            error2 = tmpeffect.SQLpreparef ( "WHERE target_type=%d AND target_id=%d AND kaomoji <> ''", Opponent_TYPE_ENNEMY
                                            , tmpmonster.primary_key() );
            strcpy ( tmpstr, "|");
            int countface = 0;

            if ( error2 == SQLITE_OK)
            {
               error2 = tmpeffect.SQLstep ();

               while ( error2 == SQLITE_ROW )
               {
        // printf ( "Debug: Character: Compile Condition: effect condition=%d conditionstr=",
          //       tmpeffect.condition(), tmpeffect.condition_str() );

                  if ( countface < 6 ) // only display 5 kaomoji
                  {
                     strcat ( tmpstr, tmpeffect.kaomoji() );
                     strcat ( tmpstr, "|");
                     countface++;
                  }
                  error2 = tmpeffect.SQLstep ();
               }

               tmpeffect.SQLfinalize ();
            }
         }

        // color = General_COLOR_DISABLE;

         /*strcpy ( position,"---");

         if ( tmpmonster.body() == Character_BODY_ALIVE )
         {
            strcpy ( tmpstr, "" );
            color = General_COLOR_TEXT;

            switch ( Monster_POSITION [nb_monster] [ monster_id])
            {
               case Party_FRONT: strcpy ( position,"Front"); break;
               case Party_BACK: strcpy ( position,"Back"); break;
               case Party_NONE: strcpy ( position,"???"); break;
            }
         }*/

         strcpy ( position, Party_STR_RANK [ tmpmonster.rank() ] );

         // building up the HP bar
         strcpy ( HPbarstr, "[?????]" );

         HPbar = ( tmpmonster.current_HP() * 5 ) / tmpmonster.max_HP();
         if ( (( tmpmonster.current_HP() * 5 ) % tmpmonster.max_HP() ) > 0 )
            HPbar++;

         HPbarstr[0] = '[';

         for ( i = 1 ; i <= 5 ; i++ )
            if ( i <= HPbar )
               HPbarstr [ i ] = '|';
            else
               HPbarstr [ i ] = '-';

         HPbarstr[6] = ']';

         if ( tmpmonster.identified () >= 2 )
            sprintf ( HPbarstr, "%3d/%3d", tmpmonster.current_HP(), tmpmonster.max_HP() );

         //tmpmonster.displayname ( tmpname );

         textprintf_old ( subbuffer, General_FONT_PARTY, x + 7, ystart
         , color, "%-5s %-20s      %7s     %-24s",
          position, tmpmonster.displayname(), HPbarstr, tmpstr );

         ystart = ystart + font_height;

         if ( tmpmonster.body() == Character_BODY_ALIVE )
            monster_id++;

         error = tmpmonster.SQLstep();


      }
   }

   tmpmonster.SQLfinalize();

  //----------------------------- old code -----------------------------------------


 /*  int font_height = text_height ( General_FONT_PARTY );
   int font_width = text_length ( General_FONT_PARTY, "a" );
   short ystart;
   int i;
   int nb_character;
//   string tmpstr;
   char tmpstr [ 16 ];
   char tmpname [16];
   int color;

   Ennemy tmpe;
//   Opponent &tmpennemy = tmpe;

   nb_character = party.nb_character();

   ystart = y + 6;

//   textout_old ( subbuffer, General_FONT_INSTRUCTION,
//      "      Name               Status",
//      x + 7, ystart - 14, General_COLOR_TEXT );

   for ( i = 0 ; i < nb_character ; i++ )
   {
*/
/*      tmpe.SQLselect ( party.FKcharacter ( i ) );


      strcpy ( tmpstr, tmpe.statusS() );
      color = General_COLOR_DISABLE;

      if ( tmpe.status() == Opponent_STATUS_ALIVE )
      {
         strcpy ( tmpstr, "" );
         color = General_COLOR_TEXT;
      }

//      strncpy ( tmpname, tmpe.cname(), 15 );
//      tmpname [ 15 ] = '\0';
      textprintf_old ( subbuffer, General_FONT_PARTY, x + 7, ystart
         , color, "    %s  %s", tmpe.name(), tmpstr );

      if ( tmpe.status() == Opponent_STATUS_ALIVE )
      {
         draw_health_status ( tmpe.healthS()
                                     , x + ( font_width * 20 ) + 6, ystart );
      }

      ystart = ystart + font_height;*/
 /*  }*/

}


void WDatProc_gallery_bitmap ( BITMAP &bmp, short x, short y )
{
   stretch_sprite( subbuffer, &bmp, x + 6, y + 6, 320, 320 );
}

void WDatProc_texture_bitmap ( BITMAP &bmp, short x, short y )
{
   stretch_sprite( subbuffer, &bmp, x + 6, y + 6, 256, 256 );
}

void WDatProc_gallery_font ( BITMAP &bmp, short x, short y )
{
   draw_sprite( subbuffer, &bmp, x + 6, y + 6 );
}


void WDatProc_character ( Character &character, short x, short y )
{
   int font_height = text_height ( General_FONT_PARTY );
   int i;
   //int j;
   int tmpy;
   int tmpx;
//   Weapon tmpw;
//   Shield tmps;
//   Armor tmpm;
//   Accessory tmpa;
   Item tmpitem;
   Race tmprace;
   CClass tmpclass;
   //int point [ 12 ];
   char tmpstr [ 31 ];
   char itemstr [100];
   //unsigned short mask;
   int error;
   int nb_attack = 0;

   //character.eval_stat_all();


   tmpy = y + 10;
   tmpx = x + 20;

   tmprace.SQLselect ( character.FKrace() );
   tmpclass.SQLselect ( character.FKcclass() );

   //printf ("Before: %u sec \n", clock() / ( CLOCKS_PER_SEC * 1000 ));

   //clock_t before = clock();

   character.compile_stat();

   //clock_t after = clock();

   /*printf("Before: %.0f\n",
        (double)((double)before / CLOCKS_PER_SEC) * 1000);
   printf("After: %.0f\n",
        (double)((double)after / CLOCKS_PER_SEC) * 1000);
*/

   //printf ("After: %u sec \n", clock() / ( CLOCKS_PER_SEC * 1000) );

//--------------------------  Column 1    ---------------------------------

   textprintf_old ( subbuffer, System_FONT_NORMAL, tmpx, tmpy, General_COLOR_TEXT,
      "%s", character.name() );
   tmpy += font_height;

   textprintf_old ( subbuffer, System_FONT_NORMAL, tmpx, tmpy, General_COLOR_TEXT,
      "Level %d %s %s", character.level(), tmprace.name(), tmpclass.name() );
   tmpy += font_height ;

   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "Exp Point   %d", character.exp() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "Next Level  %d", character.next_level_exp() );
   tmpy += font_height *2;

   textout_old ( subbuffer, Camp_FONT_INSTRUCTION, "Attribute               Modifier", tmpx, tmpy, General_COLOR_TEXT );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "STRength     %2d  (STR %+2d)", character.attribute ( 0 ), character.STRmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "DEXterity    %2d  (DEX %+2d)", character.attribute ( 1 ), character.DEXmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "ENDurance    %2d  (END %+2d)", character.attribute ( 2 ), character.ENDmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "INTelligence %2d  (INT %+2d)", character.attribute ( 3 ), character.INTmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "CUNning      %2d  (CUN %+2d)", character.attribute ( 4 ), character.CUNmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "WILlpower    %2d  (WIL %+2d)", character.attribute ( 5 ), character.WILmodifier() );
   tmpy += font_height * 2;

   textout_old ( subbuffer, Camp_FONT_INSTRUCTION, "Health", tmpx, tmpy, General_COLOR_TEXT );
   tmpy += font_height;

   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "Hit Point    %3d/%3d  (d%2d)", character.current_HP(), character.max_HP(), tmpclass.HPdice() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "Magick Point %3d/%3d  (d%2d)", character.current_MP(), character.max_MP(), tmpclass.MPdice() );
   tmpy += font_height;

   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "Body        %s", STR_CHR_BODY [ character.body() ] );
   tmpy += font_height;

   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "Soul        %d%%", character.soul() );
   tmpy += font_height * 2;

    textout_old ( subbuffer, General_FONT_INSTRUCTION,
      "Equipment", tmpx, tmpy, General_COLOR_TEXT );
    tmpy += font_height;

    error = tmpitem.SQLpreparef ("WHERE loctype=3 AND lockey=%d ORDER BY type", character.primary_key());
    //tmpitem.SQLerrormsg();

    //printf ("CharacterPK = %d", character.primary_key());

    if ( error == SQLITE_OK)
    {
        //printf ("pass 1\r\n");
        error = tmpitem.SQLstep();

        while ( error == SQLITE_ROW)
        {
            strcpy ( itemstr, STR_ITM_TYPE_CODE[tmpitem.type()]);

            if ( tmpitem.hand() > 0 )
               sprintf ( tmpstr, " %dH ", tmpitem.hand());
            else
               sprintf ( tmpstr, " -- ");

            strcat ( itemstr, tmpstr);
            strcat ( itemstr, tmpitem.vname() );

            if ( tmpitem.cursed() == true)
               strcat ( itemstr, " (CURSED)");


            textout_old ( subbuffer, Camp_FONT_CHARACTER, itemstr, tmpx, tmpy, General_COLOR_TEXT );
            tmpy += font_height;

            error = tmpitem.SQLstep();
        }
    }

    tmpitem.SQLfinalize();

    // ----- Column 2 & 3 top section -----

    tmpx = 240;
    tmpy = y + 6;

    //?? add character stats here

    textprintf_old ( subbuffer, FNT_small, tmpx, tmpy, General_COLOR_TEXT,
               "D20 Statistics               Base+Attrib.+Equip.+Effect = Total" );

    tmpy += font_height;

    for ( i = 0 ; i < D20_NB_STAT; i++)
    {
       textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "%20s %3d  %3d  %3d  %3d   %3d",
                   STR_SYS_D20STAT [ i ],
                   character.d20stat ( i, D20MOD_BASE ),
                   character.d20stat ( i, D20MOD_ATTRIBUTE ),
                   character.d20stat ( i, D20MOD_EQUIP ),
                   character.d20stat ( i, D20MOD_AEFFECT ),
                   character.d20stat ( i, D20_NB_MODIFIER ) );
       tmpy += font_height;
    }

tmpy += font_height;

    textprintf_old ( subbuffer, FNT_small, tmpx, tmpy, General_COLOR_TEXT,
               "XYZ Statistics               dY  Attrib.+Equip.+Effect  =  Total " );

    tmpy += font_height;

    // weapon damage: Has nb of attack and dice Y
       textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "%20s d%2d %3d  %3d  %3d  %1d(d%2d%+3d)",
                   STR_SYS_XYZSTAT [ 0 ],
                   character.xyzstat ( 0, XYZMOD_Y ),
                   character.xyzstat ( 0, XYZMOD_ZATTRIBUTE ),
                   character.xyzstat ( 0, XYZMOD_ZEQUIP ),
                   character.xyzstat ( 0, XYZMOD_ZAEFFECT ),
                   character.xyzstat ( 0, XYZMOD_X ),
                   character.xyzstat ( 0, XYZMOD_Y ),
                   character.xyzstat ( 0, XYZ_NB_MODIFIER ) );
       tmpy += font_height;

    // spell damage: has nb of power but not spell dice Y
       textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "%20s d \? %3d  %3d  %3d  %1d(d \?%+3d)",
                   STR_SYS_XYZSTAT [ 1 ],
                   character.xyzstat ( 1, XYZMOD_ZATTRIBUTE ),
                   character.xyzstat ( 1, XYZMOD_ZEQUIP ),
                   character.xyzstat ( 1, XYZMOD_ZAEFFECT ),
                   character.xyzstat ( 1, XYZMOD_X ),
                   character.xyzstat ( 1, XYZ_NB_MODIFIER ) );
       tmpy += font_height;

    for ( i = 2 ; i < 4; i++)
    {
       textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "%20s d%2d %3d  %3d  %3d    1d%2d%+3d",
                   STR_SYS_XYZSTAT [ i ],
                   character.xyzstat ( i, XYZMOD_Y ),
                   character.xyzstat ( i, XYZMOD_ZATTRIBUTE ),
                   character.xyzstat ( i, XYZMOD_ZEQUIP ),
                   character.xyzstat ( i, XYZMOD_ZAEFFECT ),
                   character.xyzstat ( i, XYZMOD_Y ),
                   character.xyzstat ( i, XYZ_NB_MODIFIER ) );
       tmpy += font_height;
    }

    /* for reference
    #define D20_NB_STAT        8
#define D20_NB_MODIFIER    4
#define XYZ_NB_STAT        2
#define XYZ_NB_MODIFIER    5


#define D20MOD_BASE        0
#define D20MOD_ATTRIBUTE   1
#define D20MOD_EQUIP       2
#define D20MOD_AEFFECT     3

#define XYZSTAT_DMG     0
#define XYZSTAT_MDMG    1

#define XYZMOD_X           0
#define XYZMOD_Y           1
#define XYZMOD_ZATTRIBUTE  2
#define XYZMOD_ZEQUIP      3
#define XYZMOD_ZAEFFECT    4
*/

    tmpy += font_height;

    textprintf_old ( subbuffer, FNT_small, tmpx, tmpy, General_COLOR_TEXT,
               "Flat Stats" );

    tmpy += font_height;

    nb_attack = character.level() / character.flatstat( FLAT_ATTDIV ) + 1;
    int cs = character.flatstat ( FLAT_CS );

    strcpy ( tmpstr, "");
    /*for ( i = 0 ; i < nb_attack; i++ )
    {
       sprintf ( tmpstr, "%s%d/", tmpstr, cs );
       cs -= character.flatstat ( FLAT_MULTIHIT );
    }*/

    textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
                "%d %s range attack at %d CS + %d per missed att."
                ,nb_attack, STR_ITM_RANGE [character.flatstat ( FLAT_RANGE)]
                , cs , character.flatstat ( FLAT_MULTIHIT ) );
/*


    if ( character.flatstat( FLAT_CLUMSINESS ) != 0 )
         nb_attack = character.flatstat( FLAT_WS ) / character.flatstat( FLAT_CLUMSINESS );

    nb_attack++;

    textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
                "( WS:%3d / Clums.:%2d ) + 1 = %d %s Atk",
                character.flatstat( FLAT_WS ),
                character.flatstat( FLAT_CLUMSINESS ),
                nb_attack,
                STR_ITM_RANGE [character.flatstat ( FLAT_RANGE)] );*/


    /*tmpy += font_height;

    textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
                "%s range atk, CS = %d ( -%d / extra atk )",
                STR_ITM_RANGE [character.flatstat ( FLAT_RANGE)],
                character.flatstat ( FLAT_CS),
                character.flatstat ( FLAT_MULTIHIT) );*/

//    tmpy += font_height;

    tmpy += font_height;
    tmpy += font_height;

    textprintf_old ( subbuffer, FNT_small, tmpx, tmpy, General_COLOR_TEXT,
               "Resistance" );
    tmpy += font_height;

    for ( i = 0; i < 32 ; i++)
    {
       if ( ( indextobit ( i ) & character.resistance() ) > 0 )
       {
          textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "%s", STR_SYS_PROPERTY [ i ] );
          tmpy += font_height;
       }
    }



    // ----- Column 3 -----

    tmpx = 440;
    tmpy = y + 6;

}

void WDatProc_character_gold ( int key, int x, int y )
{
   int font_height = text_height ( System_FONT_NORMAL );

   textprintf_old ( subbuffer, System_FONT_NORMAL, x +6, y + 6, General_COLOR_TEXT,
               "Gold: %d", party.gold() );
   textprintf_old ( subbuffer, System_FONT_NORMAL, x +6, y + 6 + font_height, General_COLOR_TEXT,
               "Time:%s", game.clock.complete_str() );


}

void WDatProc_new_character ( Character &character, short x, short y )
{

   Race tmprace;
   CClass tmpclass;
   x = x + 6;
   y += 6;
   //char tmpstr [ 21 ];
   int font_height = text_height ( System_FONT_NORMAL );


   tmprace.SQLselect ( character.FKrace() );
   tmpclass.SQLselect ( character.FKcclass() );


   textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
      "Name :       %s", character.name() );
   y += font_height;
   textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
      "Race :       %s", tmprace.name() );
   y += font_height;
   textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
      "Class :      %s", tmpclass.name() );
   y += font_height * 2;

      textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
         "STRength      %d", character.strength() );
   y += font_height;
      textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
         "DEXterity     %d", character.dexterity() );
   y += font_height;
      textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
         "ENDurance     %d", character.endurance() );
   y += font_height;
      textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
         "INTelligence  %d", character.intelligence() );
   y += font_height;
      textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
         "CUNning       %d", character.cunning() );
   y += font_height;
      textprintf_old ( subbuffer, System_FONT_NORMAL, x, y , General_COLOR_TEXT,
         "WILlpower     %d", character.willpower() );



}


void WDatProc_inspect_item ( int key, int x, int y )
{
   Item tmpitem;
   int tmpy = y + 6;
   int tmpx = x + 6;
   int font_height = text_height ( FNT_print );
   int font_width = text_length ( FNT_print, "a" );
   char tmpstr [21];
   char longstr [201];
   int i;
   int tmpx2;

   tmpitem.SQLselect ( key );

   /*textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Name: %s", tmpitem.vname() );
   tmpy += font_height;*/

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Type: %s", STR_ITM_TYPE [ tmpitem.type()]);
   tmpy += font_height;

   switch ( tmpitem.hand() )
   {
      case 0:
      break;
      case 1:
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "One handed" );
      break;
      case 2:
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Two handed" );
      break;
      default:
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Wrong NB of hands" );
      break;

   }
   tmpy += font_height;


   if ( tmpitem.identified() == true)
   {
      textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Value:   %d", tmpitem.price());
      tmpy += font_height;

      textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Rarity:  %s", STR_ITM_RARITY [tmpitem.rarity()]);
      tmpy += font_height;

      strcpy (tmpstr, "");

      if ( tmpitem.cursed() == true)
         strcat ( tmpstr, "Cursed ");

      if ( tmpitem.key() > 0)
         strcat ( tmpstr, "Key ");

      if ( strcmp ( tmpstr, "") != 0 )
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "%s Item", tmpstr );
         tmpy += font_height;
      }
      else
         tmpy += font_height;

      if ( tmpitem.charges_max() > 0 )
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Charges: %d/%d", tmpitem.charges_current(), tmpitem.charges_max());
         tmpy += font_height;
      }
      else
         tmpy += font_height;

      textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Defense: %s", tmpitem.defense_str() );
         tmpy += font_height;


      switch ( tmpitem.type())
      {
         case Item_TYPE_WEAPON:
            textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Element Inflicted:");
         break;
         case Item_TYPE_ACCESSORY:
         case Item_TYPE_ARMOR:
         case Item_TYPE_SHIELD:
            textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Element Resisted :");
         break;
         default:
            textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Element Unknown  :");
         break;
      }



      strcpy ( longstr, "");
      for ( i = 0; i < EnhancedSQLobject_HASH_SIZE ; i++)
      {
         if ( ( indextobit ( i ) & tmpitem.element() ) > 0 )
         {
            sprintf ( tmpstr, " %s,", STRFLD_ELEMENTAL_PROPERTY [ i ] .display );
            strcat ( longstr, tmpstr );
         }
      }
      tmpx2= tmpx + font_width * 19;
      textprintf_old ( subbuffer, FNT_small, tmpx2, tmpy, General_COLOR_TEXT, "%s", longstr );
      tmpy += font_height;

   }
   else
   {
      tmpy += font_height * 3;
      textprintf_old ( subbuffer, FNT_print, tmpx + 32, tmpy, General_COLOR_TEXT, "I t e m    U n i d e n t i f i e d");
   }


   //------ 2nd column ------

   tmpy = y + 6 /*+ font_height*/;
   tmpx = tmpx + ( font_width * 18);

   strcpy ( tmpstr, STRFLD_PROFIENCY [ bitoindex ( tmpitem.profiency() ) ] . display );
   //strcpy ( tmpstr, "Undefined");
   /*
   switch ( tmpitem.type() )
   {
      case Item_TYPE_WEAPON:

      break;
      case Item_TYPE_SHIELD:
         strcpy ( tmpstr, STR_ITM_CAT_SHIELD[ bitoindex ( tmpitem.category() ) ] );
      break;
      case Item_TYPE_ARMOR:
         strcpy ( tmpstr, STR_ITM_CAT_ARMOR[ bitoindex ( tmpitem.category() ) ] );
      break;
   }*/

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Category: %s", tmpstr );
   tmpy += font_height;

   if ( tmpitem.identified() == true )
   {
      if ( tmpitem.mh() > 0 )
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Multihit Bonus: %d", tmpitem.mh() );
         tmpy += font_height;
      }

      if ( tmpitem.attribute_attack() > 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Attack Attribute: %s", STR_SYS_ATTRIBUTE_CODE [tmpitem.attribute_attack() - 1 ]);
         tmpy += font_height;
      }

      if ( tmpitem.attribute_damage() > 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "damage Attribute: %s", STR_SYS_ATTRIBUTE_CODE [tmpitem.attribute_damage() - 1 ]);
         tmpy += font_height;
      }

      if ( tmpitem.range() > 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Range:      %s", STR_ITM_RANGE[tmpitem.range()] );
         tmpy += font_height;
      }

      if ( tmpitem.dmgy() > 0)
      {

         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "DMG Y:      D%d", tmpitem.dmgy() );
         tmpy += font_height;
      }

      if ( tmpitem.dmgz() != 0)
      {

         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "DMG Z:      %+d", tmpitem.dmgz() );
         tmpy += font_height;
      }

      if ( tmpitem.mdmgz() != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Magic DMG Z:%+d", tmpitem.mdmgz() );
         tmpy += font_height;
      }

      if ( tmpitem.attdiv() != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Attack Divider:%+d", tmpitem.attdiv() );
         tmpy += font_height;
      }

      if ( tmpitem.powdiv() != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Attack Divider:%+d", tmpitem.powdiv() );
         tmpy += font_height;
      }
   }

   //----- 3rd column -----

   tmpy = y + 6 /*+ font_height*/;
   tmpx = tmpx + ( font_width * 34);

   if ( tmpitem.identified() == true)
   {
      if ( tmpitem.d20stat( D20STAT_AD ) != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "AD:      %d", tmpitem.d20stat( D20STAT_AD ) );
         tmpy += font_height;
      }

      if ( tmpitem.d20stat( D20STAT_MD ) != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "MD:      %d", tmpitem.d20stat( D20STAT_MD ) );
         tmpy += font_height;
      }

      if ( tmpitem.d20stat( D20STAT_PS ) != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "PS:      %+d", tmpitem.d20stat( D20STAT_PS ) );
         tmpy += font_height;
      }

      if ( tmpitem.d20stat( D20STAT_MS ) != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "MS:      %+d", tmpitem.d20stat( D20STAT_MS ) );
         tmpy += font_height;
      }

      if ( tmpitem.d20stat( D20STAT_MELEECS ) != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Melee CS:%+d", tmpitem.d20stat( D20STAT_MELEECS ) );
         tmpy += font_height;
      }

      if ( tmpitem.d20stat( D20STAT_RANGECS ) != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "Range CS:%+d", tmpitem.d20stat( D20STAT_RANGECS ) );
         tmpy += font_height;
      }


      if ( tmpitem.d20stat( D20STAT_INIT ) != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "IN:      %+d", tmpitem.d20stat( D20STAT_INIT ) );
         tmpy += font_height;
      }

      if ( tmpitem.d20stat( D20STAT_DR ) != 0)
      {
         textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT, "DR:      %d", tmpitem.d20stat( D20STAT_DR ) );
         tmpy += font_height;
      }

   }


}




void WDatProc_character_levelup ( Character &character, short x, short y )
{
   int font_height = text_height ( General_FONT_PARTY );
   int tmpx = x + 10;
   int tmpy = y + 10;
   //s_Opponent_stat stat;

   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "%s", character.name() );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
      "Level :       %d", character.level () );
   tmpy += font_height * 2;


   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "STRength     %2d  (STR %+2d)", character.attribute ( 0 ), character.STRmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "DEXterity    %2d  (DEX %+2d)", character.attribute ( 1 ), character.DEXmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "ENDurance    %2d  (END %+2d)", character.attribute ( 2 ), character.ENDmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "INTelligence %2d  (INT %+2d)", character.attribute ( 3 ), character.INTmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "CUNning      %2d  (CUN %+2d)", character.attribute ( 4 ), character.CUNmodifier() );
   tmpy += font_height;
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, tmpx, tmpy, General_COLOR_TEXT,
      "WILlpower    %2d  (WIL %+2d)", character.attribute ( 5 ), character.WILmodifier() );
   tmpy += font_height * 2;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
      "Hit Point :    %3d", character.max_HP () );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
      "Magick Point : %3d", character.max_MP () );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
      "Soul :         %3d", character.soul () );
   tmpy += font_height *2;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
      "Exp :        %d", character.exp () );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
      "Next Level : %d", character.next_level_exp () );
   tmpy += font_height * 2;



   char conditionstr [201] = "";
   char schoolstr [121];
   char levelstr [21] = "level";
   bool highlevel = false;
   int schoolcursor = 1;
   bool firstschool = true;
   int charschool = 0;
   //int answer;

   CClass tmpclass;
   tmpclass.SQLselect ( character.FKcclass() );


   charschool = tmpclass.magic_school ();

   for ( schoolcursor = 1; schoolcursor < CClass_MAGICSCHOOL_HIGHALL ; schoolcursor = schoolcursor<< 1)
   {
         //printf ( "debug: cmdproc: readspell %d, charschool=%d\n", schoolcursor, charschool );

         if ( (charschool & schoolcursor) > 0 )
         {
            if ( firstschool == true )
            {
               sprintf ( schoolstr, "school=%d ", schoolcursor );
               firstschool = false;
            }
            else
               sprintf ( schoolstr, "%s OR school=%d ", schoolstr, schoolcursor );

            if ( schoolcursor > CClass_MAGICSCHOOL_ONLYLOW)
               highlevel = true;
         }
   }

   if ( highlevel == true )
      { sprintf ( levelstr, "highlevel"); }


      //SQLactivate_errormsg();
      sprintf ( conditionstr, "WHERE ( %s) AND %s <= %d ORDER BY effect_id, pk",
               schoolstr, levelstr, character.level() );

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
      "Nb of Known Spells : %d",  SQLcount ( "word", "spell", conditionstr ) );
   tmpy += font_height * 2;

   /*textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 96, General_COLOR_TEXT,
      "Phisical DMG Dice : %d", character.dmg_y() );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 112, General_COLOR_TEXT,
      "Magik DMG Dice :    %d", character.mdmg_y() );*/
   /*textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 128, General_COLOR_TEXT,
      "Multi Attack Mod. : %d", stat.multihitmod );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 144, General_COLOR_TEXT,
      "Skill Bonus :       %d", ( character.level() / 5 ) );*/

   //to do: add learned spells, or the last spells in the list (label Latest spells)
   //could use weapon skill progress witha appriximate nb of attack , but weapon variable.

}



void WDatProc_cclass ( int key, int x, int y )
{
   int font_height = text_height ( General_FONT_PARTY );
   int font_width = text_length ( General_FONT_PARTY, "a" );
   int tmpy;
   int tmpx;
   int tmpx2;
   CClass tmpclass;
   char tmpstr [21];
   char longstr [201];
   int i;

   tmpy = y + 7;
   tmpx = x + 7;

   tmpclass.SQLselect ( key );


   /*textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Name:               %s [%s", tmpclass.name(), tmpclass.initial() );
   tmpy += font_height;*/

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "HP dice:                  D%d", tmpclass.HPdice() );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Active Defense(AD):       %d", tmpclass.AD() );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Physical Saves(PS):       %d", tmpclass.PS() );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Mental Saves(PS):         %d", tmpclass.MS() );
   tmpy += font_height;

   /*textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Initiative(IN):           %d", tmpclass.INIT() );
   tmpy += font_height;*/

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Melee Combat Skill(MCS):  %d", tmpclass.melee_CS() );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Range Combat Skill(RCS):  %d", tmpclass.range_CS() );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Extra Attack Divider:     %d", tmpclass.attdiv() );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Profiency:" );

   strcpy ( longstr, "");
   for ( i = 0; i < 32 ; i++)
   {
      if ( ( indextobit ( i ) & tmpclass.profiency() ) > 0 )
      {
         sprintf ( tmpstr, " %s,", STRFLD_PROFIENCY [ i ] .display );
         strcat ( longstr, tmpstr );
      }
   }
   tmpx2= tmpx + font_width * 9;
   textprintf_old ( subbuffer, FNT_small, tmpx2, tmpy, General_COLOR_TEXT, "%s", longstr );
   tmpy += font_height;


   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Resistance:" );
   strcpy ( longstr, "");
   for ( i = 0; i < 32 ; i++)
   {
      if ( ( indextobit ( i ) & tmpclass.resistance() ) > 0 )
      {
         sprintf ( tmpstr, " %s,", STR_SYS_PROPERTY [ i ] );
         strcat ( longstr, tmpstr );
      }
   }
   tmpx2= tmpx + font_width * 10;
   textprintf_old ( subbuffer, FNT_small, tmpx2, tmpy, General_COLOR_TEXT, "%s", longstr );
   tmpy += font_height;

   //----- Column 2 -----

   tmpy = y + 7;
   tmpx = x + 248;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Passive Skill:             " );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Active Skill:              " );
   tmpy += font_height;


   if ( tmpclass.magic_school() != CClass_MAGICSCHOOL_NONE )
   {
     // int bitcounter = 1;
      //i = 0;

      strcpy ( longstr, "");

      for ( i = 0; i < EnhancedSQLobject_HASH_SIZE; i++)
      {
         if ( ( indextobit (i) & tmpclass.magic_school() ) > 0)
         {
            strcat ( longstr, STRFLD_MAGIC_SCHOOL [ i ] . display );
            strcat ( longstr, ", ");

         }



      }



     /* switch ( tmpclass.magic_school())
      {
         case CClass_MAGICSCHOOL_ARCANE:
            strncpy ( tmpstr, STRFLD_MAGIC_SCHOOL [ 1 ], 20);
         break;
         case CClass_MAGICSCHOOL_DIVINE:
            strncpy ( tmpstr, STRFLD_MAGIC_SCHOOL [ 2 ], 20);
         break;
         case CClass_MAGICSCHOOL_ALCHEMY:
            strncpy ( tmpstr, STRFLD_MAGIC_SCHOOL [ 3 ], 20);
         break;
         case CClass_MAGICSCHOOL_PSIONIC:
            strncpy ( tmpstr, STRFLD_MAGIC_SCHOOL [ 4 ], 20);
         break;
         case CClass_MAGICSCHOOL_OTHER:
            strncpy ( tmpstr, ", 20 );
         break;
      }*/

      textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Magic School:" );
      tmpx2= tmpx + font_width * 13;
      textprintf_old ( subbuffer, FNT_small, tmpx2, tmpy, General_COLOR_TEXT,
               "%s", longstr );

      tmpy += font_height;

      textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "MP dice:                  D%d", tmpclass.MPdice() );
      tmpy += font_height;

      textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Magic Defense(MD):        %d", tmpclass.MD() );
      tmpy += font_height;

      textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Extra Power Divider:      %d", tmpclass.powdiv() );
      tmpy += font_height;

      /*textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Magic Damage Dice(MDMGY): D%d", tmpclass.MDMGY() );
      tmpy += font_height;*/

   }

   stretch_sprite ( subbuffer, datref_character [ 1024 + tmpclass.pictureID() ],
                490, 306, 144, 168);

   //Wait for new class pictures.
   //draw_sprite ( subbuffer, datref_character [ 1024 + tmpclass.pictureID() ],
   //        x + 640 - 6 - 240, y + 6 );



}

void WDatProc_race ( int key, int x, int y )
{
   int font_height = text_height ( General_FONT_PARTY );
   int font_width = text_length ( General_FONT_PARTY, "a" );
   int tmpy;
   int tmpx;
   Race tmprace;
   char longstr [201];
   char tmpstr [21];
   int tmpx2;
   int i;

   tmpy = y + 7;
   tmpx = x + 7;

   tmprace.SQLselect ( key );

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Minimum Attributes" );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "STRength:     %d", tmprace.attribute( Character_STR ) );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "DEXterity:    %d", tmprace.attribute( Character_DEX ) );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "ENDurance:    %d", tmprace.attribute( Character_END ) );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "INTelligence: %d", tmprace.attribute( Character_INT ) );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "CUNning:      %d", tmprace.attribute( Character_CUN ) );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "WILpower:     %d", tmprace.attribute( Character_WIL ) );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "Resistance:" );
   strcpy ( longstr, "");
   for ( i = 0; i < 32 ; i++)
   {
      if ( ( indextobit ( i ) & tmprace.resistance() ) > 0 )
      {
         sprintf ( tmpstr, " %s,", STR_SYS_PROPERTY [ i ] );
         strcat ( longstr, tmpstr );
      }
   }
   tmpx2= tmpx + font_width * 11;
   textprintf_old ( subbuffer, FNT_small, tmpx2, tmpy, General_COLOR_TEXT, "%s", longstr );
   tmpy += font_height;



   /*stretch_sprite ( subbuffer, ( BITMAP* ) datfcharacter [ BMP_RACEM000  + tmprace.pictureID() ] .dat,
                561, 317, 72, 72);
   stretch_sprite ( subbuffer, ( BITMAP* ) datfcharacter [ BMP_RACEF000  + tmprace.pictureID() ] .dat,
                561, 389, 72, 72);*/


   draw_sprite ( subbuffer, datref_portrait [ tmprace.pictureID() ],
           x + 640 - 6 - 256, y + 6  );
   draw_sprite ( subbuffer, datref_portrait [ tmprace.pictureID() + 2 ],
           x + 640 - 6 - 128, y + 6  );

}

void WDatProc_attribute_info ( int key, int x, int y )
{
   int font_height = text_height ( General_FONT_PARTY );
   //int font_width = text_length ( General_FONT_PARTY, "a" );
   int tmpy;
   int tmpx;

   tmpy = y + 7;
   tmpx = x + 7;


   textprintf_old ( subbuffer, FNT_small, tmpx, tmpy, General_COLOR_TEXT,
               "Modifiers" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "4-5:   -3" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "6-7:   -2" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "8-9:   -1" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "10-11:  0" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "12-13: +1" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "14-15: +2" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "16-17: +3" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "18-19: +4" );
   tmpy += font_height;
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "20:    +5" );
   tmpy += font_height;


   tmpy = y + 7;
   tmpx = x + 100;

   textprintf_old ( subbuffer, FNT_small, tmpx, tmpy, General_COLOR_TEXT,
               "Attribute Modifier Application" );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "STRength:     Damage(DMGZ),       Damage Resistance (DR)" );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "DEXterity:    Active Defense(AD), Physical Combat Skill(PCS)" );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "ENDurance:    Hit Points(HP),     Physical Saves(PS)" );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "INTelligence: Magic DMG(MDMGZ),   Initiative(IN)" );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "CUNning:      Magic Defense (MD), Mental Combat Skill(MCS)" );
   tmpy += font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
               "WILpower:     Mana Points(MP),    Mental Saves(MS)" );
   tmpy += font_height;


}

/*void WDatProc_party_bar2 ( int key, int x, int y )
{
   int font_height = text_height ( General_FONT_PARTY );
   int font_width = text_length ( General_FONT_PARTY, "a" );
   int tmpy;
   int tmpx;
   char tmpstr [ 16 ];
   int color;
   Character tmpchar;
   CClass tmpclass;
   Race tmprace;
   char lvlchar;
   int error;

//printf ( "wdat2: pass 1\r\n");
   tmpy = y + 14 + 6;
   tmpx = x + 7;

   textout_old ( subbuffer, General_FONT_INSTRUCTION,
      "      Name                Level Race Class     HP         MP       Status                  Action",
      tmpx, tmpy - 14, General_COLOR_TEXT );
//printf ( "wdat2: pass 1b\r\n");
   error = tmpchar.SQLprepare ("WHERE location=2 ORDER BY position");
//printf ( "wdat2: pass 2\r\n");
   if ( error == SQLITE_OK )
   {
      error = tmpchar.SQLstep();

//printf ( "wdat2: pass 3\r\n");
      while ( error == SQLITE_ROW )
      {
         //strcpy ( tmpstr, tmpcharacter.statusS() );
         //color = General_COLOR_DISABLE;

         if ( tmpchar.body() == Opponent_STATUS_ALIVE )
         {
            strcpy ( tmpstr, "" );
            //color = General_COLOR_TEXT;
         }
         else
            strcpy ( tmpstr, STR_CHR_BODY [ tmpchar.body()]);

         if ( tmpchar.check_levelup() == true )
            lvlchar = '@';
         else
            lvlchar = ' ';

//printf ( "wdat2: pass 4\r\n");
         tmpclass.SQLselect ( tmpchar.FKcclass() );
         tmprace.SQLselect ( tmpchar.FKrace() );
//printf ( "wdat2: pass 5\r\n");
         textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy
         , General_COLOR_TEXT, "    %-15s %2d%c %3s %3s %3d/%3d %3d/%3d %-16s",
          tmpchar.name(), tmpchar.level(), lvlchar, tmprace.initial(), tmpclass.initial (),
          tmpchar.current_HP (), tmpchar.max_HP(), tmpchar.current_MP(), tmpchar.max_MP(),
          tmpstr );

         tmpy += font_height;

         error = tmpchar.SQLstep();
//printf ( "wdat2: pass 6\r\n");
      }
   }

   tmpchar.SQLfinalize();
}*/



void WDatProc_system_info ( int key, int x, int y )
{
   int font_height = text_height ( General_FONT_PARTY );
   //int font_width = text_length ( General_FONT_PARTY, "a" );
   int tmpy = y + 6;
   int tmpx = x + 6;
   char tmpstr [51];

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "Allegro Version:     %s", ALLEGRO_VERSION_STR );
   tmpy += font_height;

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "Allegro Release Date:%s", ALLEGRO_DATE_STR );
   tmpy += font_height;

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "Allegro Platform:    %s", ALLEGRO_PLATFORM_STR );
   tmpy += font_height;

   switch ( os_type )
   {
      case OSTYPE_UNKNOWN: strcpy ( tmpstr, "MS-DOS");break;
      case OSTYPE_WIN3: strcpy ( tmpstr, "Windows 3.1");break;
      case OSTYPE_WIN95: strcpy ( tmpstr, "Windows 95");break;
      case OSTYPE_WIN98: strcpy ( tmpstr, "Windows 98");break;
      case OSTYPE_WINME: strcpy ( tmpstr, "Windows ME");break;
      case OSTYPE_WINNT: strcpy ( tmpstr, "Windows NT");break;
      case OSTYPE_WIN2000: strcpy ( tmpstr, "Windows 2000");break;
      case OSTYPE_WINXP: strcpy ( tmpstr, "Windows XP");break;
      case OSTYPE_WIN2003: strcpy ( tmpstr, "Windows 2003");break;
      case OSTYPE_WINVISTA: strcpy ( tmpstr, "Windows Vista");break;
      case OSTYPE_OS2: strcpy ( tmpstr, "OS/2");break;
      case OSTYPE_WARP: strcpy ( tmpstr, "OS/2 Warp 3");break;
      case OSTYPE_DOSEMU: strcpy ( tmpstr, "Linux DOSEMU");break;
      case OSTYPE_OPENDOS: strcpy ( tmpstr, "Caldera OpenDOS");break;
      case OSTYPE_LINUX: strcpy ( tmpstr, "Linux");break;
      case OSTYPE_SUNOS: strcpy ( tmpstr, "SunOS/Solaris");break;
      case OSTYPE_FREEBSD: strcpy ( tmpstr, "FreeBSD");break;
      case OSTYPE_NETBSD: strcpy ( tmpstr, "NetBSD");break;
      case OSTYPE_IRIX: strcpy ( tmpstr, "IRIX");break;
      case OSTYPE_DARWIN: strcpy ( tmpstr, "Darwin");break;
      case OSTYPE_QNX: strcpy ( tmpstr, "QNX");break;
      case OSTYPE_UNIX: strcpy ( tmpstr, "Unknown Unix variant");break;
      case OSTYPE_BEOS: strcpy ( tmpstr, "BeOS");break;
      case OSTYPE_MACOS: strcpy ( tmpstr, "MacOS");break;
      case OSTYPE_MACOSX: strcpy ( tmpstr, "MacOS X");break;
      default: strcpy ( tmpstr, "Unidentified");break;
   }

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "Operating System:    %s", tmpstr );
   tmpy += font_height;

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "CPU Vendor:          %s", cpu_vendor );
   tmpy += font_height;


   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "Graphic Resolution:  %d x %d", SCREEN_W, SCREEN_H );
   tmpy += font_height;

/*
   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "Allegro Version:    %d", allegro_id );
   tmpy += font_height;

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "Allegro Version:    %d", allegro_id );
   tmpy += font_height;

   textprintf_old ( subbuffer, FNT_print, tmpx, tmpy, General_COLOR_TEXT,
               "Allegro Version:    %d", allegro_id );
   tmpy += font_height;
*/

// OS type: need table with list of code values, else need to use a big switch.
}

void WDatProc_character_equip ( int key, int x, int y)
{
   // key is equal to item
   int tmpy = y + 6;
   int tmpx = x + 6;
   int font_height = text_height ( General_FONT_PARTY );
   //int font_width = text_length ( General_FONT_PARTY, "a" );
   //Item tmpitem;
   Character tmpchar;
   int error;
   //bool equipable = false;

   //tmpitem.SQLselect ( key );
   //printf ("debug: wdatproc: character_equip\n");
   textprintf_old ( subbuffer, FNT_small, tmpx, tmpy, General_COLOR_TEXT, "Equipable by", tmpchar.name());
   tmpy += font_height;

//   debug_printf( __func__, "Before");
   error = tmpchar.SQLprepare ( "WHERE location=2 ORDER by position" );
 //  debug_printf( __func__, "After");

   if ( error == SQLITE_OK)
   {
      error = tmpchar.SQLstep();

      while ( error == SQLITE_ROW)
      {
         if ( tmpchar.is_equipable( key ) == false )
            textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT, "#%s", tmpchar.name());
         else
            textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT, " %s", tmpchar.name());

         tmpy += font_height;

         error = tmpchar.SQLstep();
      }
   }

   tmpchar.SQLfinalize();




}

void WDatProc_combat_log ( int key, int x, int y )
{

   int i;
   int tmpoffset = system_log.marker();
   int tmpx = x + 6;
   int tmpy = y + 6;
   int font_height = text_height ( General_FONT_PARTY );

   if ( tmpoffset > 7)
      tmpoffset=7;

   for ( i = tmpoffset; i >= 0 ; i--)
   {
      textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT, "%s", system_log.read( i ) );
      tmpy += font_height;
   }

}

void WDatProc_character_actions ( int key, int x, int y )
{
   //code not working
   // Main problem is that actions are not inserted in the database yet.
   // so need to set a copy in character.
   Action tmpaction;
   int errorsql;
   Character tmpcharacter;
   int tmpx = x + 6;
   int tmpy = y + 6;
   int font_height = text_height ( General_FONT_PARTY );

   errorsql = tmpaction.SQLpreparef ("WHERE actor_type=%d", Action_ACTOR_TYPE_CHARACTER);

   if ( errorsql == SQLITE_OK )
   {
      errorsql = tmpaction.SQLstep();

      while ( errorsql == SQLITE_ROW )
      {
         tmpcharacter.SQLselect ( tmpaction.actorID() );

         textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
                     "%s:%s", tmpcharacter.displayname(),
                     CmdProc_COMMANDLIST [ tmpaction.commandID () ] . name  );

         tmpy += font_height;
         errorsql = tmpaction.SQLstep();
      }

      tmpaction.SQLfinalize();
   }
}

void WDatProc_spell_info ( int key, int x, int y )
{
   int tmpx = x + 6;
   int tmpy = y + 6;
   int font_height = text_height ( General_FONT_PARTY );

   Spell tmpspell;

   tmpspell.SQLselect ( key );

   for ( int i = 0 ; i < Spell::NB_FIELD; i++)
   {
      if ( tmpspell.get_display (i) == true)
      {


         switch ( tmpspell.get_type (i) )
         {
            case INTEGER:
               textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
                     "%s: %d", tmpspell.get_display_name (i), tmpspell.get_int (i) );
            break;
            case STRFIELD:
            case TEXT:
               textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
                     "%s: %s", tmpspell.get_display_name (i), tmpspell.get_str (i) );
            break;
         }
         tmpy += font_height;
      }

   }
   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT, "Description:");

   tmpy +=font_height;

   textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT, "%s", tmpspell.description());


}

void WDatProc_active_effect_detail ( int key, int x, int y)
{
   int tmpx = x + 6;
   int tmpy = y + 6;
   int font_height = text_height ( General_FONT_PARTY );
   int count = 0;
   bool column2= false;

   ActiveEffect tmpeffect;

   tmpeffect.SQLselect ( key );

   for ( int i = 0 ; i < ActiveEffect::NB_FIELD; i++)
   {
      if ( tmpeffect.get_display (i) == true)
      {
         switch ( tmpeffect.get_type (i) )
         {
            case INTEGER:
               textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
                     "%s: %d", tmpeffect.get_display_name (i), tmpeffect.get_int (i) );
            break;
            case STRFIELD:
            case TEXT:
               textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
                     "%s: %s", tmpeffect.get_display_name (i), tmpeffect.get_str (i) );
            break;
         }
         tmpy += font_height;
         count ++;
      }

      if ( count >= 9 && column2 == false)
      {
         tmpx = x + 320;
         tmpy = y + 6;
         column2 = true;
      }

   }


}

void WDatProc_combat_active_effect ( int key, int x, int y)
{
   int tmpx = x + 6;
   int tmpy = y + 6;
   int font_height = text_height ( General_FONT_PARTY );
   int count = 0;
   int error;

   ActiveEffect tmpeffect;

   error = tmpeffect.SQLpreparef ( "WHERE target_type=%d AND target_id=%d", Opponent_TYPE_CHARACTER, key );

   if ( error == SQLITE_OK )
   {
      error = tmpeffect.SQLstep();

      while ( error == SQLITE_ROW )
      {
         if ( count < 8 )
         {
            textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
                     "%s", tmpeffect.name () );

            tmpy += font_height;
         }
         error = tmpeffect.SQLstep();
         count++;
      }

      if ( count > 8 )
         textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy, General_COLOR_TEXT,
                     "et al.", tmpeffect.name () );

      tmpeffect.SQLfinalize();
   }

}


/*--------------------------- old code ---------------------------------------*/

/*void WDatProc_character_list ( Player &player, short x, short y )
{
   draw_character_list ( player, x , y , false );
}

void WDatProc_character_list_masked ( Player &player, short x, short y )
{
   draw_character_list ( player, x , y , true );
}*/

/*void WDatProc_party_list ( Player &player, short x, short y )
{
   int nb_party = player.nb_party ();
   int nb_character;
   int i;
   int j;
   short xstart;
   short ystart;
//   Party tmpparty;
   short height = text_height ( General_FONT_PARTY );
   Party tmparty;
   Character tmpcharacter;
   City tmpcity;
   CClass tmpclass;
   char tmpstr [ 21 ];
//   Race tmprace;

//   tmprace.SQLselect ( character.race() );



//   Character tmpcharacter;

//   draw_border ( 0 , 0 , 639, 479, General_COLOR_BORDER );
   xstart = x + 6;

   for ( j = 0 ; j < nb_party ; j++ )
   {
      tmparty.SQLselect ( player.party ( j ) );
      tmpcity.SQLselect ( tmparty.location() );

      ystart = y + ( ( ( height * 7 ) + 16 ) * j ) + 6;

      textprintf_old ( subbuffer, General_FONT_PARTY, xstart, ystart, General_COLOR_TEXT,
          "%-30s %<<%-30s >> %s", tmparty.name(), tmpcity.name(),
          STR_PAR_STATUS [ tmparty.status() ] );

      textout_old ( subbuffer, General_FONT_INSTRUCTION,
         "Name                Family               Sex Alig Class Level  Status    ",
         xstart + 16, ystart + height, General_COLOR_TEXT );

      ystart = ystart + 14;

      nb_character = tmparty.nb_character ();
      for ( i = 0 ; i < nb_character ; i++ )
      {
         tmpcharacter.SQLselect ( tmparty.character ( i ) );

         ystart = ystart + height;
         tmpclass.SQLselect ( tmpcharacter.cclass () );
         textprintf_old ( subbuffer, General_FONT_PARTY, xstart + 16, ystart,
            General_COLOR_TEXT, "%-15s %-15s %1s %2s  %-3s Lv%d %-7s",
            tmpcharacter.name(), tmpcharacter.family(),
            tmpcharacter.sexC(), tmpcharacter.aligmentC(),
            tmpclass.initial(), tmpcharacter.level(),
            tmpcharacter.statusS() );
      }
   }

   Window::instruction ( 480, 40, Window_INSTRUCTION_SELECT
      + Window_INSTRUCTION_CANCEL + Window_INSTRUCTION_MENU );
//   p_inst_center_x = 320;
//   p_inst_y = 460;
//   draw_instruction (Draw_INSTRUCTION_MENU + Draw_INSTRUCTION_SELECT +
//      Draw_INSTRUCTION_CANCEL, 320, 460 );

}

void WDatProc_new_account ( Account &account , short x, short y )
{
   x = x + 6;

//   account.SQLselect

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 6, General_COLOR_TEXT,
      "Loginame : %s", account.loginame() );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 22, General_COLOR_TEXT,
      "Name     : %s", account.name() );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 38, General_COLOR_TEXT,
      "Family   : %s", account.family() );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 54, General_COLOR_TEXT,
      "Nickname : %s", account.nickname() );

}*/


/*void WDatProc_worldmap ( BITMAP &bmp, short x, short y )
{
   draw_sprite ( subbuffer, &bmp, x + 6, y + 6 );
}*/

/*void InterfaceBuild_Party_Bar ( List &tmplist )
{

   int font_height = text_height ( General_FONT_PARTY );
   int font_width = text_length ( General_FONT_PARTY, "a" );

   char tmpstr [ 16 ];

   Character tmpchar;
   CClass tmpclass;
   Race tmprace;
   char lvlchar;
   int error;

   // problem with this function is that compared to data, the information in the party is
   // not updated, maybe only use for menu selection, or recreate all the time.

   tmplist.header("      Name               Level Race Class     HP       MP       Status                  Action" );

   error = tmpchar.SQLprepare ("WHERE location=2");

   if ( error == SQLITE_OK )
   {

      error = tmpchar.SQLstep();

      while ( error == SQLITE_ROW )
      {
         tmpclass.SQLselect ( tmpchar.FKcclass() );
         tmprace.SQLselect ( tmpchar.FKrace() );
         //strcpy ( tmpstr, tmpcharacter.statusS() );
         //color = General_COLOR_DISABLE;

         if ( tmpchar.body() == Opponent_STATUS_ALIVE )
            strcpy ( tmpstr, "" );
         else
            strcpy ( tmpstr, STR_CHR_BODY [ tmpchar.body()]);

         if ( tmpchar.check_levelup() == true )
            lvlchar = '@';
         else
            lvlchar = ' ';

         tmplist.add_itemf ( tmpchar.primary_key(), "%-15s %2d%c %3s %3s %3d/%3d %3d/%3d %-16s",
                           tmpchar.name(), tmpchar.level(), lvlchar, tmprace.initial(), tmpclass.initial(),
                           tmpchar.current_HP (), tmpchar.max_HP(), tmpchar.current_MP(),
                           tmpchar.max_MP(), tmpstr);

         //need to add combat string

         error = tmpchar.SQLstep();
      }
   }

}*/

/*void WDatProc_new_race ( Race &race, short x, short y )
{
   x = x + 6;
   y = y + 6;
   unsigned char tmpvar;
   unsigned char mask;
   unsigned short wmask;
   unsigned short wtmpvar;
   int i;

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y, General_COLOR_TEXT,
      "Name :        %s", race.name() );
*/
   /*textprintf_old ( subbuffer, General_FONT_PARTY, x, y+16, General_COLOR_TEXT,
      "Plural Name : %s", race.plural() );*/
/*
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 32, General_COLOR_TEXT,
      "STRength      %d", race.attribute ( Character_STRENGTH ) );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 48, General_COLOR_TEXT,
      "DEXterity     %d", race.attribute ( Character_DEXTERITY ) );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 64, General_COLOR_TEXT,
      "ENDurance     %d", race.attribute ( Character_ENDURANCE ) );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 80, General_COLOR_TEXT,
      "INTelligence  %d", race.attribute ( Character_INTELLIGENCE ) );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 96, General_COLOR_TEXT,
      "CUNning       %d", race.attribute ( Character_CUNNING ) );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 112, General_COLOR_TEXT,
      "WILpower      %d", race.attribute ( Character_WILLPOWER ) );

   textout_old ( subbuffer, General_FONT_PARTY, "Aligment Restriction",
      x, y + 128, General_COLOR_TEXT );
*/
   /*tmpvar = race.aligment_restriction();
   mask = 1;

   for ( i = 0 ; i < Opponent_NB_ALIGMENT ; i++ )
   {
      if ( ( tmpvar & mask ) > 0 )
         textprintf_old ( subbuffer, General_FONT_PARTY, x + ( i * 24), y + 144,
         General_COLOR_TEXT, "%2s ", ALIGMENT_INFO [ i ] . initial );
      mask = mask << 1;
   }*/

  /* textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 160, General_COLOR_TEXT,
      "Life Span    %d Years", race.lifespan () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 176, General_COLOR_TEXT,
      "Size         %s", STR_OPP_SIZE [ race.size () ] );
*/
/*   textout_old ( subbuffer, General_FONT_PARTY, "Stat Modifier",
      x, y + 192, General_COLOR_TEXT );

   tmpvar = race.stat_bonus();
   mask = 1;

   for ( i = 0 ; i < 6 ; i++ )
   {
      if ( ( tmpvar & mask ) > 0 )
         textprintf_old ( subbuffer, General_FONT_PARTY, x + ( i * 24 ), y + 208,
         General_COLOR_TEXT, "%3s ", STAT_MODIFIER [ i ].sname );
      mask = mask << 1;
   }*/
/*
   textout_old ( subbuffer, General_FONT_PARTY, "Health Resistance",
      x, y + 224, General_COLOR_TEXT );

   wtmpvar = race.resistance();
   wmask = 1;
   short tmpx = x;
   short tmpy = y + 240;

   for ( i = 0 ; i < Opponent_NB_HEALTH ; i++ )
   {
      if ( ( wtmpvar & wmask ) > 0 )
         textprintf_old ( subbuffer, General_FONT_PARTY, tmpx, tmpy ,
         General_COLOR_TEXT, "%s", HEALTH_INFO [ i ].name );
      wmask = wmask << 1;
      tmpy += 16;
      if ( i == 7 )
      {
         tmpy = y + 240;
         tmpx = x + 112;
      }
   }

   //    private: unsigned short p_health_resist; // 240
   // 2 column, 1st physical, 2nd Psychic

   // set resistance with letters

   //?? add HP, MP, DMG, MDMG dices.

   textout_old ( subbuffer, General_FONT_PARTY, "Abilities",
      x, y + 368, General_COLOR_TEXT );


}*/

/*
void WDatProc_opponent_stat ( Opponent &opponent, short x, short y )
{
   x = x + 6;
   y = y + 6;
   s_Opponent_stat stat;
   Ennemy tmpennemy;

   opponent.eval_stat_all();
   stat = opponent.stat();

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y, General_COLOR_TEXT,
      "Name :    %s", opponent.name () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 16, General_COLOR_TEXT,
      "STRength :     %d", opponent.strength () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 32, General_COLOR_TEXT,
      "DEXterity :    %d", opponent.dexterity () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 48, General_COLOR_TEXT,
      "ENDurance :    %d", opponent.endurance () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 64, General_COLOR_TEXT,
      "INTelligence : %d", opponent.intelligence () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 80, General_COLOR_TEXT,
      "CUNning :      %d", opponent.cunning () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 96, General_COLOR_TEXT,
      "WILlpower :    %d", opponent.willpower () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 112, General_COLOR_TEXT,
      "LUCk :    %d", opponent.luck () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 128, General_COLOR_TEXT,
      "Level :   %d", opponent.level () );

*/
/*   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 142, General_COLOR_TEXT,
      "Size :    %s", STR_OPP_SIZE [ opponent.size () ] );*/
/*   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 158, General_COLOR_TEXT,
      "HP :      %d", opponent.max_HP () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 174, General_COLOR_TEXT,
      "MP :      %d", opponent.max_MP () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 206, General_COLOR_TEXT,
      "Soul :     %d", opponent.soul () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 222, General_COLOR_TEXT,
      "EXP :      %d", opponent.reward_exp () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 238, General_COLOR_TEXT,
      "Gold :     %d", opponent.gold () );

*/
/*   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 252, General_COLOR_TEXT,
      "Aligment : %s", opponent.aligmentS () );
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 268, General_COLOR_TEXT,
      "Status :   %s", opponent.statusS () );*/

/*
   if ( opponent.type() == Opponent_TYPE_ENNEMY )
   {
      tmpennemy.SQLselect ( opponent.pri );
         textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 284, General_COLOR_TEXT,
         "Encounter Probability : %d", tmpennemy.eprob () );
         textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 300, General_COLOR_TEXT,
         "Rank Priority :         %d", tmpennemy.rankpriority () );

   }

   // add 2nd column combat stat

   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y, General_COLOR_TEXT,
      "Multi hit Modifier : %d", stat.multihitmod );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 16, General_COLOR_TEXT,
      "hitbonus : %d", stat.hitbonus );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 32, General_COLOR_TEXT,
      "encmod : %d", stat.encmod );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 48, General_COLOR_TEXT,
      "dmg_x : %d", stat.dmg_x );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 64, General_COLOR_TEXT,
      "dmg_y : %d", stat.dmg_y );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 80, General_COLOR_TEXT,
      "dmg_z : %d", stat.dmg_z );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 96, General_COLOR_TEXT,
      "dmg_type : %d", stat.dmg_type );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 112, General_COLOR_TEXT,
      "mdmg_x : %d", stat.mdmg_x );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 128, General_COLOR_TEXT,
      "mdmg_y : %d", stat.mdmg_y );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 144, General_COLOR_TEXT,
      "mdmg_z : %d", stat.mdmg_z );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 160, General_COLOR_TEXT,
      "range : %d", stat.range );
//   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 176, General_COLOR_TEXT,
//      "hitroll : %d", stat.hitroll );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 192, General_COLOR_TEXT,
      "nb_max_attack : %d", stat.nb_max_attack );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 208, General_COLOR_TEXT,
      "PD : %d", stat.PD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 224, General_COLOR_TEXT,
      "AD : %d", stat.AD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 240, General_COLOR_TEXT,
      "DR : %d", stat.DR );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 256, General_COLOR_TEXT,
      "MDR : %d", stat.MDR );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 272, General_COLOR_TEXT,
      "MPD : %d", stat.MPD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 288, General_COLOR_TEXT,
      "MAD : %d", stat.MAD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 304, General_COLOR_TEXT,
      "PSAVE : %d", stat.PSAVE );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 320, General_COLOR_TEXT,
      "MSAVE : %d", stat.MSAVE );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 336, General_COLOR_TEXT,
      "init : %d", stat.init );
      */
/*
   unsigned short elm_resist  ;
   unsigned short hlt_resist  ;
   unsigned short elm_effect  ;
   unsigned short hlt_effect  ;
   unsigned short magikproperty  ;


}*/
/*void WDatProc_ennemy_picture ( Party &party, short x, short y )
{
}
*/
/*-------------------------------------------------------------------------*/
/*-                     Private module usage procedures                   -*/
/*-------------------------------------------------------------------------*/

/*void draw_character_list ( Player &player, short x, short y, bool mask )
{
//   short height = 24;
   int font_height = text_height ( General_FONT_PARTY );
   short ystart;
   int nb_character;
   int i;
   Character tmpcharacter;
   CClass tmpclass;
   char tmpstr [ 21 ];

//   Character tmpcharacter;
   int clr;

   nb_character = player.nb_character();
//   height = height + ( ( nb_character + 1 ) * font_height )
//   + ( font_height / 2 );
//   draw_border_fill ( 0, 0 , 639 , height,
//      General_COLOR_BORDER, General_COLOR_FILL );

   Window::instruction ( 320, 340, Window_INSTRUCTION_SELECT +
      Window_INSTRUCTION_CANCEL + Window_INSTRUCTION_MENU );
//   p_inst_center_x = 320;
//   p_inst_y = 355;

//   draw_instruction (Draw_INSTRUCTION_MENU + Draw_INSTRUCTION_SELECT +
//      Draw_INSTRUCTION_CANCEL, 320, 355 );

   textout_old ( subbuffer, General_FONT_INSTRUCTION,
      "     Name                 Family              Sex Alig Class Level  Status    Location",
      x + 10, y + 6 + font_height, General_COLOR_TEXT );

   ystart = y + 6 + 12 + font_height + ( font_height / 2 );

   for ( i = 0 ; i < nb_character ; i++ )
   {
      tmpcharacter.SQLselect ( player.character ( i ) );
//      int tmptag = player.character ( 0 );

//      textprintf_old ( subbuffer, font, 100, 100, General_COLOR_TEXT,
//         "[%3d|%3d|%5d]", tmptag.table(), tmptag.source(), tmptag.key() );

      clr = General_COLOR_TEXT;
      if ( mask == true &&
         tmpcharacter.available() != Character_AVAILABLE )
         clr = General_COLOR_DISABLE;

      tmpclass.SQLselect ( tmpcharacter.cclass() );
      textprintf_old ( subbuffer, General_FONT_PARTY, x + 7, ystart
         , clr, "    %-15s %-15s %1s %2s  %3s Lv%2d %-7s %-13s",
         tmpcharacter.name(), tmpcharacter.family(),
         tmpcharacter.sexC(), tmpcharacter.aligmentC(),
            tmpclass.initial(), tmpcharacter.level(),
         tmpcharacter.statusS(), tmpcharacter.availableS() );

      ystart = ystart + font_height;
   }


}

*/


// --- INspect item ---

   /*dbs_Item_ability tmpability;
   char tmpstr [ 81 ];
   char tmpstr2 [ 81 ];
   char tmpstr3 [ 81 ];
   char tmpstr4 [ 81 ];
   //Weapon tmpw;
   //Armor tmpm;
   //Shield tmps;
   //Accessory tmpa;
   //Expandable tmpe;
   int i;
   int j;
   int tmpval;
   short tmpy;
   Item tmpitem;

   x = x + 6;
   y = y + 6;
   tmpability = tmpitem.ability();


//#define Item_IDENTIFICATION_EFFECT     8 // Hlt & Elm effect + Mo + MD effect
//#define Item_IDENTIFICATION_ABILITY    16 // Ability, autoability, charges
//#define Item_IDENTIFICATION_DRAWBACK   32 // Exhaust, Drawback

// weigth
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 48, General_COLOR_TEXT,
      "Weight :  %4.2f Kg", 1 );


   //--------------- IDENTIFICATION_NAME --------------------------

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y, General_COLOR_TEXT,
      "%s", tmpitem.vname() );

   if ( tmpitem.identified ( Item_IDENTIFICATION_NAME ) == true )
   {
      if ( tmpitem.identified ( Item_IDENTIFICATION_STATUS ) == true )
         tmpitem.statusS1( tmpstr);
      else
         strcpy ( tmpstr ,"Unknown" );
      strcpy ( tmpstr2 ,tmpitem.typeS() );
   }
   else
   {
      strcpy ( tmpstr ,"Unknown" );
      strcpy ( tmpstr2 , "Item" );
   }
   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 16, General_COLOR_TEXT,
      "%s %s", tmpstr, tmpstr2 );

   //--------------- IDENTIFICATION_STAT --------------------------

   if ( tmpitem.identified ( Item_IDENTIFICATION_STAT ) == true )
   {
      sprintf ( tmpstr, "%d", tmpitem.price() );*/
/*
      switch ( tmpitem.type() )
      {
         case Item_TYPE_WEAPON :
            tmpw.SQLselect ( tmpitem.tag() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y, General_COLOR_TEXT,
               "%d handed %s weapon", tmpw.nb_hand(), STR_WPN_CATEGORY [ tmpw.category() ] );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+16, General_COLOR_TEXT,
               "%s range W. using %s", STR_WPN_RANGE [ tmpw.range() ],
               STR_WPN_ATTRIBUTE [ tmpw.attribute() ]);

            tmpval = tmpw.static_dmg();
            if ( tmpval == 0 )
            {
               textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+32, General_COLOR_TEXT,
               "Damage : %d dY + %d", tmpw.dmg_x(), tmpw.dmg_z() );
            }
            else
            {
               textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+32, General_COLOR_TEXT,
               "Damage : %d d%d + %d", tmpw.dmg_x(), tmpw.static_dmg(), tmpw.dmg_z() );
            }

            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+48, General_COLOR_TEXT,
            "Damage Type :     %s",  STR_WPN_DMGTYPE [ tmpw.dmg_type() ] );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+64, General_COLOR_TEXT,
            "#max of Attacks : %d",  tmpw.nb_max_attack());
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+80, General_COLOR_TEXT,
            "Active Defense :  %d",  tmpw.AD());
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+96, General_COLOR_TEXT,
            "Hit Bonus :       %d",  tmpw.hitbonus());
         break;

         case Item_TYPE_ARMOR :
            tmpm.SQLselect ( tmpitem.tag() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y, General_COLOR_TEXT,
               "%s Armor", STR_ARM_CATEGORY [ tmpm.category() ] );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+16, General_COLOR_TEXT,
               "Passive Defense  %d", tmpm.PD() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+32, General_COLOR_TEXT,
               "Active Defense   %d", tmpm.AD() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+48, General_COLOR_TEXT,
               "Damage Resist    %d", tmpm.DR() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+64, General_COLOR_TEXT,
               "Magik Pasv Def.  %d", tmpm.MPD() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+80, General_COLOR_TEXT,
               "Magik Dmg Resist %d", tmpm.MDR() );
         break;
         case Item_TYPE_SHIELD :
            tmps.SQLselect ( tmpitem.tag() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y, General_COLOR_TEXT,
               "%s Shield", STR_SHL_CATEGORY [ tmps.category() ] );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+16, General_COLOR_TEXT,
               "Passive Defense  %d", tmps.PD() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+32, General_COLOR_TEXT,
               "Active Defense   %d", tmps.AD() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+48, General_COLOR_TEXT,
               "Magic Pasv def.  %d", tmps.MPD() );
            textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y+64, General_COLOR_TEXT,
               "Magik Actv Def.  %d", tmps.MAD() );

         break;
         case Item_TYPE_ACCESSORY :
            tmpa.SQLselect ( tmpitem.tag() );
            if ( tmpitem.identified ( Item_IDENTIFICATION_STATUS ) == true )
            {
               if ( tmpa.bonus() != 0 )
               {
                  textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y, General_COLOR_TEXT,
                  "%d bonus applied to", tmpa.bonus() );

                  tmpy = y + 16;

                  for ( i = 0 ; i < 3 ; i++ )
                  {
                     if ( tmpa.statID ( i ) != Accessory_STAT_NONE )
                     {
                        textprintf_old ( subbuffer, General_FONT_PARTY, x+240, tmpy, General_COLOR_TEXT,
                           "%s", STR_ACC_STAT [ tmpa.statID ( i ) ]  );
                        tmpy += 16;
                     }
                  }
               }
               textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y, General_COLOR_TEXT,
                  "No Modifiers" );

            }
         break;
      }*/
  /* }
   else
   {
      textprintf_old ( subbuffer, General_FONT_PARTY, x+240, y + 80, General_COLOR_TEXT,
        "< Unknown Stats >" );
   }


   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 64, General_COLOR_TEXT,
      "Value :   %s Gp", tmpstr );


   //--------------- IDENTIFICATION_STATUS --------------------------
   if ( tmpitem.identified ( Item_IDENTIFICATION_STATUS ) == true )
      tmpitem.statusS2( tmpstr );
   else
      strcpy ( tmpstr ,"" );

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 32, General_COLOR_TEXT,
      "%s", tmpstr );

   //--------------- IDENTIFICATION_EFFECT --------------------------
   if ( tmpitem.type () == Item_TYPE_WEAPON )
      textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 192, General_COLOR_TEXT,
         "Effect : " );
   else
      textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 192, General_COLOR_TEXT,
         "Resist : " );

   if ( tmpitem.identified ( Item_IDENTIFICATION_EFFECT ) == true )
   {
   //?? note : need to add MD and MO properties
      unsigned short mask = 1;
      unsigned char nb_shift = 0;

      for ( i = 0 ; i < 2 ; i++ )
         for ( j = 0 ; j < 8 ; j++ )
         {
            mask = mask << 1;
            nb_shift++;
            if ( ( tmpitem.hlteffect() & mask ) > 0 )
               textprintf_old ( subbuffer, General_FONT_PARTY, x + ( j * 56 ), y + ( i * 16 ),
                  General_COLOR_TEXT, "%7s",  STR_ITM_HLTEFFECT [ nb_shift ] );
         }

      mask = 1;
      nb_shift = 0;

      for ( i = 0 ; i < 2 ; i++ )
         for ( j = 0 ; j < 8 ; j++ )
         {
            mask = mask << 1;
            nb_shift++;
            if ( ( tmpitem.elmeffect() & mask ) > 0 )
               textprintf_old ( subbuffer, General_FONT_PARTY, x + ( j * 56 ), y + ( i * 16 ) + 64,
                  General_COLOR_TEXT, "%7s",  STR_ITM_ELMEFFECT [ nb_shift ] );
         }


   }
   //--------------- IDENTIFICATION_ABILITY --------------------------
   if ( tmpitem.identified ( Item_IDENTIFICATION_ABILITY ) == true )
   {
      tmpitem.abilityS( tmpstr );
      strcpy ( tmpstr2, STR_ITM_AUTOABILITY [ tmpitem.autoability() ] );
      if ( tmpability.type != Item_ABILITY_NONE )
      {
         sprintf ( tmpstr3, "%d", tmpitem.nb_charge() );
         sprintf ( tmpstr4, "%d", tmpability.max_charge );
      }
      else
      {
         strcpy ( tmpstr3, "-" );
         strcpy ( tmpstr4, "-" );
      }
   }
   else
   {
      strcpy ( tmpstr, "?" );
      strcpy ( tmpstr2, "?" );
      strcpy ( tmpstr3, "?" );
      strcpy ( tmpstr4, "?" );
   }

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 80, General_COLOR_TEXT,
      "Autoablty %s", tmpstr2 );

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 96, General_COLOR_TEXT,
      "Ability   %s", tmpstr );

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 112, General_COLOR_TEXT,
      "Charges   %s / %s", tmpstr3, tmpstr4 );

   //--------------- IDENTIFICATION_DRAWBACK --------------------------
   if ( tmpitem.identified ( Item_IDENTIFICATION_DRAWBACK ) == true )
   {
      strcpy ( tmpstr, tmpitem.exhaustedS() );
      strcpy ( tmpstr2, tmpitem.drawbackS() );
   }
   else
   {
      strcpy ( tmpstr, "?" );
      strcpy ( tmpstr2, "?" );
   }

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 128, General_COLOR_TEXT,
      "Exhausted %s", tmpstr );

   textprintf_old ( subbuffer, General_FONT_PARTY, x, y + 144, General_COLOR_TEXT,
      "Drawback  %s", tmpstr2 );
*/

/*void WDatProc_character_stat ( Character &character, short x, short y )
{
   s_Opponent_stat stat;

   //character.eval_stat_all();
   //stat= character.stat();

   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y, General_COLOR_TEXT,
      "Multi hit Modifier : %d", stat.multihitmod );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 16, General_COLOR_TEXT,
      "hitbonus : %d", stat.hitbonus );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 32, General_COLOR_TEXT,
      "encmod : %d", stat.encmod );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 48, General_COLOR_TEXT,
      "dmg_x : %d", stat.dmg_x );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 64, General_COLOR_TEXT,
      "dmg_y : %d", stat.dmg_y );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 80, General_COLOR_TEXT,
      "dmg_z : %d", stat.dmg_z );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 96, General_COLOR_TEXT,
      "dmg_type : %d", stat.dmg_type );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 112, General_COLOR_TEXT,
      "mdmg_x : %d", stat.mdmg_x );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 128, General_COLOR_TEXT,
      "mdmg_y : %d", stat.mdmg_y );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 144, General_COLOR_TEXT,
      "mdmg_z : %d", stat.mdmg_z );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 160, General_COLOR_TEXT,
      "range : %d", stat.range );
//   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 176, General_COLOR_TEXT,
//      "hitroll : %d", stat.hitroll );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 192, General_COLOR_TEXT,
      "nb_max_attack : %d", stat.nb_max_attack );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 208, General_COLOR_TEXT,
      "PD : %d", stat.PD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 224, General_COLOR_TEXT,
      "AD : %d", stat.AD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 240, General_COLOR_TEXT,
      "DR : %d", stat.DR );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 256, General_COLOR_TEXT,
      "MDR : %d", stat.MDR );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 272, General_COLOR_TEXT,
      "MPD : %d", stat.MPD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 288, General_COLOR_TEXT,
      "MAD : %d", stat.MAD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 304, General_COLOR_TEXT,
      "PSAVE : %d", stat.PSAVE );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 320, General_COLOR_TEXT,
      "MSAVE : %d", stat.MSAVE );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+320, y + 336, General_COLOR_TEXT,
      "init : %d", stat.init );
*/
/*   int i;
   char tmpstr [ 51 ];
   int tmpval;
   s_Opponent_stat tmpstat;

   character.eval_stat_all();
   tmpstat = character.allstat();

   for ( i = 0 ; i < 8 ; i++ )
   {
      tmpval = tmpstat.roll [ i ].max + tmpstat.roll [ i ].modifier;
      if ( tmpval < tmpstat.roll [ i ].min )
         tmpval = tmpstat.roll [ i ].max;

      sprintf ( tmpstr, "%2d to %2d(%2d) + %2d", tmpstat.roll [ i ].min,
      tmpstat.roll [ i ].max,
      tmpval,
      tmpstat.roll [ i ].bonus );
      textout_old ( subbuffer, General_FONT_PARTY, tmpstr, 134, 6 + ( 16 * i ), General_COLOR_TEXT );
   }

   textout_old ( subbuffer, General_FONT_PARTY, "Evade     ", 6, 6, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Absorb    ", 6, 24, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Initiative", 6, 40, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Block     ", 6, 56, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Parry     ", 6, 72, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Hit       ", 6, 88, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Shock     ", 6, 104, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Passive Defense", 6, 120, General_COLOR_TEXT );

   textprintf_old ( subbuffer, General_FONT_PARTY, 6, 152, General_COLOR_TEXT,
      "Protection  %d", tmpstat.protection );
   textprintf_old ( subbuffer, General_FONT_PARTY, 6, 168, General_COLOR_TEXT,
      "Penetration DMG  %d", tmpstat.penetration );
   textprintf_old ( subbuffer, General_FONT_PARTY, 6, 184, General_COLOR_TEXT,
      "Multi-hit        %d", tmpstat.multihit );
   textprintf_old ( subbuffer, General_FONT_PARTY, 6, 200, General_COLOR_TEXT,
      "# of Parry       %d", tmpstat.nb_parry );
   textprintf_old ( subbuffer, General_FONT_PARTY, 6, 216, General_COLOR_TEXT,
      "# of Block       %d", tmpstat.nb_block );
   textprintf_old ( subbuffer, General_FONT_PARTY, 6, 232, General_COLOR_TEXT,
      "Bulk             %d", tmpstat.bulk );
   textprintf_old ( subbuffer, General_FONT_PARTY, 6, 248, General_COLOR_TEXT,
      "Encumbrance      %d", tmpstat.encumbrance );
  */



/*
//   unsigned short elm_resist;
//   unsigned short hlt_resist;
//   unsigned short elm_effect;
//   unsigned short hlt_effect;
//   unsigned short moproperty;
//   unsigned short mdproperty;
//   unsigned char dmg_type;
//   unsigned char category;
//   unsigned short wproperty;
*/






/*   short xpos [ Opponent_NB_STAT ];
   short ypos [ Opponent_NB_STAT ];
   int i;
   int j;
   short tmpvar;
   short highestx;
   char tmpcstr [ 4 ];*/
/*

   character.eval_stat_all();

   for ( i = 0 ; i < Opponent_NB_STAT ; i++ )
   {
      xpos [ i ] = 0;
      ypos [ i ] = 0;
   }

   x = x + 6;
   y = y + 6;
   character.eval_stat_all();

   // building general stat
   textout_old ( subbuffer, General_FONT_PARTY, "Evade", x, y, General_COLOR_TEXT );
   xpos [ Opponent_STAT_EVADE ] = x + 88;
   ypos [ Opponent_STAT_EVADE ] = y + 0;
   textout_old ( subbuffer, General_FONT_PARTY, "Bulk", x, y + 16, General_COLOR_TEXT );
   xpos [ Opponent_STAT_BULK ] = x + 88;
   ypos [ Opponent_STAT_BULK ] = y + 16;
   textout_old ( subbuffer, General_FONT_PARTY, "Cover", x, y + 32, General_COLOR_TEXT );
   xpos [ Opponent_STAT_COVER ] = x + 88;
   ypos [ Opponent_STAT_COVER ] = y + 32;
   textout_old ( subbuffer, General_FONT_PARTY, "Resist", x, y + 48, General_COLOR_TEXT );
   xpos [ Opponent_STAT_RESIST ] = x + 88;
   ypos [ Opponent_STAT_RESIST ] = y + 48;
   textout_old ( subbuffer, General_FONT_PARTY, "Dissipate", x, y + 64, General_COLOR_TEXT );
   xpos [ Opponent_STAT_DISSIPATE ] = x + 88;
   ypos [ Opponent_STAT_DISSIPATE ] = y + 64;


   // building armor protection stat
   textout_old ( subbuffer, General_FONT_PARTY, "Armor     Durability     Absorb", x + 314, y , General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Torso    ", x + 314, y + 16, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Left Arm ", x + 314, y + 32, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Right Arm", x + 314, y + 48, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Left Leg ", x + 314, y + 64, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Right Leg", x + 314, y + 80, General_COLOR_TEXT );
   textout_old ( subbuffer, General_FONT_PARTY, "Head     ", x + 314, y + 96, General_COLOR_TEXT );

   for ( i = 0 ; i < 6 ; i++ )
   {
      xpos [ Opponent_STAT_DURABILITY00 + i ] = x + 394;
      ypos [ Opponent_STAT_DURABILITY00 + i ] = y + 16 + ( i * 16 );
      xpos [ Opponent_STAT_ABSORB00 + i ] = x + 514;
      ypos [ Opponent_STAT_ABSORB00 + i ] = y + 16 + ( i * 16 );
   }
  */
   // building weapon stat
/*
   textout_old ( subbuffer, General_FONT_INSTRUCTION, "Fighting Style A", x, y + 150, General_COLOR_TEXT );

   if ( character.fstyle ( 0 ) != Character_FSTYLE_NONE
      || character.fstyle ( 0 ) != Character_FSTYLE_SHIELD )
   {
      textout_old ( subbuffer, General_FONT_PARTY, "Hit", x, y+162, General_COLOR_TEXT );
      xpos [ Opponent_STAT_HIT00 ] = x + 100;
      ypos [ Opponent_STAT_HIT00 ] = y + 162;

      textout_old ( subbuffer, General_FONT_PARTY, "Parry", x, y+178, General_COLOR_TEXT );
      xpos [ Opponent_STAT_PARRY00 ] = x + 100;
      ypos [ Opponent_STAT_PARRY00 ] = y + 178;

      textout_old ( subbuffer, General_FONT_PARTY, "Unparry", x, y+194, General_COLOR_TEXT );
      xpos [ Opponent_STAT_UNPARRY00 ] = x + 100;
      ypos [ Opponent_STAT_UNPARRY00 ] = y + 194;

      textout_old ( subbuffer, General_FONT_PARTY, "Combo", x, y+210, General_COLOR_TEXT );
      xpos [ Opponent_STAT_COMBO00 ] = x + 100;
      ypos [ Opponent_STAT_COMBO00 ] = y + 210;

      textout_old ( subbuffer, General_FONT_PARTY, "Multi-hit", x,y+226, General_COLOR_TEXT );
      xpos [ Opponent_STAT_MULTI00 ] = x + 100;
      ypos [ Opponent_STAT_MULTI00 ] = y + 226;

      textout_old ( subbuffer, General_FONT_PARTY, "Penetrate", x, y+242, General_COLOR_TEXT );
      xpos [ Opponent_STAT_PENETRATION00 ] = x + 100;
      ypos [ Opponent_STAT_PENETRATION00 ] = y + 242;

      textout_old ( subbuffer, General_FONT_PARTY, "Damage", x, y+258, General_COLOR_TEXT );
      xpos [ Opponent_STAT_DAMAGE00 ] = x + 100;
      ypos [ Opponent_STAT_DAMAGE00 ] = y + 258;

//      textout_old ( subbuffer, General_FONT_PARTY, "Shock", x, y+166, General_COLOR_TEXT );
//      xpos [ Opponent_STAT_SHOCK00 ] = x + 100;
//      ypos [ Opponent_STAT_SHOCK00 ] = y + 166;

      textout_old ( subbuffer, General_FONT_PARTY, "Cripple", x, y+274, General_COLOR_TEXT );
      xpos [ Opponent_STAT_CRIPPLE00 ] = x + 100;
      ypos [ Opponent_STAT_CRIPPLE00 ] = y + 274;

      textout_old ( subbuffer, General_FONT_PARTY, "Nb Parry", x, y+290, General_COLOR_TEXT );
      xpos [ Opponent_STAT_NB_PARRY00 ] = x + 100;
      ypos [ Opponent_STAT_NB_PARRY00 ] = y + 290;
   }

   if ( character.fstyle ( 0 ) != Character_FSTYLE_NONE
      || character.fstyle ( 0 ) != Character_FSTYLE_SHIELD )
   {
      textout_old ( subbuffer, General_FONT_PARTY, "Hit", x, y+306, General_COLOR_TEXT );
      xpos [ Opponent_STAT_HIT01 ] = x + 100;
      ypos [ Opponent_STAT_HIT01 ] = y + 306;

      textout_old ( subbuffer, General_FONT_PARTY, "Parry", x, y+322, General_COLOR_TEXT );
      xpos [ Opponent_STAT_PARRY01 ] = x + 100;
      ypos [ Opponent_STAT_PARRY01 ] = y + 322;

      textout_old ( subbuffer, General_FONT_PARTY, "Unparry", x, y+338, General_COLOR_TEXT );
      xpos [ Opponent_STAT_UNPARRY01 ] = x + 100;
      ypos [ Opponent_STAT_UNPARRY01 ] = y + 338;

      textout_old ( subbuffer, General_FONT_PARTY, "Combo", x, y+354, General_COLOR_TEXT );
      xpos [ Opponent_STAT_COMBO01 ] = x + 100;
      ypos [ Opponent_STAT_COMBO01 ] = y + 354;

      textout_old ( subbuffer, General_FONT_PARTY, "Multi-hit", x, y+370, General_COLOR_TEXT );
      xpos [ Opponent_STAT_MULTI01 ] = x + 100;
      ypos [ Opponent_STAT_MULTI01 ] = y + 370;

      textout_old ( subbuffer, General_FONT_PARTY, "Penetrate", x,386166, General_COLOR_TEXT );
      xpos [ Opponent_STAT_PENETRATION01 ] = x + 100;
      ypos [ Opponent_STAT_PENETRATION01 ] = y + 386;

      textout_old ( subbuffer, General_FONT_PARTY, "Damage", x, y+402, General_COLOR_TEXT );
      xpos [ Opponent_STAT_DAMAGE01 ] = x + 100;
      ypos [ Opponent_STAT_DAMAGE01 ] = y + 402;

      textout_old ( subbuffer, General_FONT_PARTY, "Shock", x, y+418, General_COLOR_TEXT );
      xpos [ Opponent_STAT_SHOCK01 ] = x + 100;
      ypos [ Opponent_STAT_SHOCK01 ] = y + 418;

      textout_old ( subbuffer, General_FONT_PARTY, "Cripple", x, y+434, General_COLOR_TEXT );
      xpos [ Opponent_STAT_CRIPPLE01 ] = x + 100;
      ypos [ Opponent_STAT_CRIPPLE01 ] = y + 434;

      textout_old ( subbuffer, General_FONT_PARTY, "Nb Parry", x, y+450, General_COLOR_TEXT );
      xpos [ Opponent_STAT_NB_PARRY01 ] = x + 100;
      ypos [ Opponent_STAT_NB_PARRY01 ] = y + 450;
   }
*/

   /*   string name; // name of the manuver used for display in combat
   int probability; // value used for % roll to determine manuver selected
   int hit; // propability to hit ennemy target
   int unparry; // Determine how hard is the weapon to parry
   int multi; // probablility to make additional attacks
   int type; // type of damage : Piercing or cutting
   int penetration; // Penetration %bonus
   int damage; // piercing or cutting damage
   int shock; // shock damage
}s_Weapon_manuver;

   dbs_Item item_data  ;
   int parry  ;
   int combo  ;
   int templateID  ;
   int hit_bonus  ;
   int unparry_bonus  ;
   int multi_bonus  ;
   int penetration_bonus  ;
   int damage_bonus  ;
   int shock_bonus  ;
   int cripple_bonus  ;
*/

/*
   for ( i = 0 ; i < Opponent_NB_STATYPE ; i++ )
   {
      for ( j = 0 ; j < 25 ; j++ )
      {
         tmpvar = character.stat ( i, j );
         if ( tmpvar != 0 || i == 0 )
         {
            if ( i > 0 && tmpvar > 0 )
            {
               textprintf_old ( subbuffer, General_FONT_PARTY, xpos [ j ], ypos [ j ],
                  WDatProc_stat_color ( i ), "+" );
               xpos [ j ] = xpos [ j ] + 8;
            }
            if ( xpos [ j ] != 0 || ypos [ j ] != 0 )
            {
               sprintf ( tmpcstr, "%d", tmpvar );
               textprintf_old ( subbuffer, General_FONT_PARTY, xpos [ j ], ypos [ j ],
                  WDatProc_stat_color ( i ), "%d", tmpvar );
               xpos [ j ] = xpos [ j ] + ( strlen ( tmpcstr ) * 8 );
            }
         }
      }
   }

   highestx = 0;
   for ( i = 0 ; i < 5 ; i++ )
      if ( highestx < xpos [ i ] )
         highestx = xpos [ i ];
   for ( i = 0 ; i < 5 ; i++ )
      xpos [ i ] = highestx;

   highestx = 0;
   for ( i = Opponent_STAT_DURABILITY00 ; i < Opponent_STAT_DURABILITY05 ; i++ )
      if ( highestx < xpos [ i ] )
         highestx = xpos [ i ];
   for ( i = Opponent_STAT_DURABILITY00 ; i < Opponent_STAT_DURABILITY05 ; i++ )
      xpos [ i ] = highestx;

   highestx = 0;
   for ( i = Opponent_STAT_ABSORB00 ; i < Opponent_STAT_ABSORB05 ; i++ )
      if ( highestx < xpos [ i ] )
         highestx = xpos [ i ];
   for ( i = Opponent_STAT_ABSORB00 ; i < Opponent_STAT_ABSORB05 ; i++ )
      xpos [ i ] = highestx;


   for ( j = 0 ; j < 25 ; j++ )
   {
      if ( xpos [ j ] != 0 || ypos [ j ] != 0 )
      {

      tmpvar = character.sum_stat ( j );
      textprintf_old ( subbuffer, General_FONT_PARTY, xpos [ j ], ypos [ j ],
         General_COLOR_TEXT, "=%d", tmpvar );
      }
   }

   // color legend

   textout_old ( subbuffer, FNT_print, "Attribute", x + 22, y + 440, WDatProc_COLOR_BASE );
   textout_old ( subbuffer, FNT_print, "Health", x + 102, y + 440, WDatProc_COLOR_HEALTH );
   textout_old ( subbuffer, FNT_print, "Wound", x + 158, y + 440, WDatProc_COLOR_WOUND );
   textout_old ( subbuffer, FNT_print, "Maze", x + 206, y + 440, WDatProc_COLOR_MAZE );
   textout_old ( subbuffer, FNT_print, "Spell", x + 246, y + 440, WDatProc_COLOR_SPELL );
   textout_old ( subbuffer, FNT_print, "Race", x + 294, y + 440, WDatProc_COLOR_RACE );
   textout_old ( subbuffer, FNT_print, "Skill", x + 334, y + 440, WDatProc_COLOR_SKILL );
   textout_old ( subbuffer, FNT_print, "Equipment", x + 382, y + 440, WDatProc_COLOR_EQUIPMENT );
   textout_old ( subbuffer, FNT_print, "Other", x + 462, y + 440, WDatProc_COLOR_OTHER );
   textout_old ( subbuffer, FNT_print, "Bulk", x + 510, y + 440, WDatProc_COLOR_BULK );

}*/

// --- character inspection ---

//   ritem = NULL;

   /*int nb_item = character.nb_inventory();
   char tmpc;

   for ( i = 0 ; i < nb_item ; i++ )
   {
      tmpc = ' ';
      tmpitem.SQLselect ( character.inventory ( i ) );
      if ( character.is_equipable ( tmpitem.tag() ) == false &&
         tmpitem.type() != Item_TYPE_EXPANDABLE )
         tmpc = '#';
      textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 236, 58 + ( i * 16 )
            , General_COLOR_TEXT, "%c%-18s %4.2f", tmpc, tmpitem.vname(),
            1 );
   }*/
/*
   textout_old ( subbuffer, General_FONT_INSTRUCTION, "Equipment",
      242, 234, General_COLOR_TEXT );

   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 250, General_COLOR_TEXT,
      "Weapon " );
//   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 314, General_COLOR_TEXT,
//      "Weapon B" );
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 266, General_COLOR_TEXT,
      "Shield" );
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 282, General_COLOR_TEXT,
      "Armor" );
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 298, General_COLOR_TEXT,
      "Head" );
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 314, General_COLOR_TEXT,
      "Hands" );
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 330, General_COLOR_TEXT,
      "Feets" );
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 346, General_COLOR_TEXT,
      "Other" );
*/
/*   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 378, General_COLOR_TEXT,
      "Weight %5.2f / %3d Kg", character.weight(), character.maxweight() );
   textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 242, 394, General_COLOR_TEXT,
      "Encumbrance %d\%%", character.encumbrance() );*/
/*
   short tmpy = 346; // used for column 3

   for ( i = 0 ; i < Character_EQUIP_SIZE ; i++ )
   {
      if ( character.FKequiped ( i ) != 0 )
      {
         //tmpitem.SQLselect ( character.equiped ( i ) );
         textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 314, 250 + ( i * 16 ),
            General_COLOR_TEXT, "%-15s", tmpitem.vname() );
         if ( tmpitem.autoability() != Item_AUTOABILITY_NONE )
         {
            textprintf_old ( subbuffer, Camp_FONT_CHARACTER, 466, tmpy, General_COLOR_TEXT,
               "%s", STR_ITM_AUTOABILITY [ tmpitem.autoability() ] );
            tmpy += 16;
         }
      }
   }
*/


//-------------------------------- column 3 -------------------------------

   /*s_Opponent_stat stat;

   //character.eval_stat_all();
   //stat= character.stat();

   textout_old ( subbuffer, Camp_FONT_INSTRUCTION, "Combat Stat", 466, 42, General_COLOR_TEXT );

   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 58, General_COLOR_TEXT,
      "Attack Bonus      %2d", stat.hitbonus );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 74, General_COLOR_TEXT,
      "Initiative        %2d", stat.init );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 90, General_COLOR_TEXT,
      "PD/AD          %2d/%2d", stat.PD, stat.AD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 106, General_COLOR_TEXT,
      "Magik PD/AD    %2d/%2d", stat.MPD, stat.MAD );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 122, General_COLOR_TEXT,
      "Dmg Reduce        %2d", stat.DR );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 138, General_COLOR_TEXT,
      "Magik Dmg reduce  %2d", stat.MDR );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 154, General_COLOR_TEXT,
      "Phys. Dmg  %2d d%2d+%2d", stat.dmg_x, stat.dmg_y, stat.dmg_z );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 170, General_COLOR_TEXT,
      "Magik Dmg  %2d d%2d+%2d", stat.mdmg_x, stat.mdmg_y, stat.mdmg_z );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 186, General_COLOR_TEXT,
      "Phys. Saves       %2d", stat.PSAVE );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 202, General_COLOR_TEXT,
      "Magik Saves       %2d", stat.MSAVE );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 218, General_COLOR_TEXT,
      "Encmbrance Pnlty  %2d", stat.encmod );
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 234, General_COLOR_TEXT,
      "Extra Att Penlty  %2d", stat.multihitmod );

   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 250, General_COLOR_TEXT,
      "HtEf");
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 266, General_COLOR_TEXT,
      "HtRs");
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 282, General_COLOR_TEXT,
      "ElEf");
   textprintf_old ( subbuffer, General_FONT_PARTY, x+466, y + 298, General_COLOR_TEXT,
      "ElRs");

   mask = 1;
   for ( i = 0 ; i < 16 ; i++ )
   {
      //--------------------
      if ( ( stat.hlt_effect & mask ) > 0 )
         textprintf_old ( subbuffer, General_FONT_PARTY, x+506 +( i * 8 ), y + 250,
            General_COLOR_TEXT, "%c", HEALTH_INFO [ i ]. name [ 0 ]);
      else
         textprintf_old ( subbuffer, General_FONT_PARTY, x+506 +( i * 8 ), y + 250,
            General_COLOR_TEXT, "." );
      //--------------------
      if ( ( stat.hlt_resist & mask ) > 0 )
         textprintf_old ( subbuffer, General_FONT_PARTY, x+506 +( i * 8 ), y + 266,
            General_COLOR_TEXT, "%c", HEALTH_INFO [ i ]. name [ 0 ]);
      else
         textprintf_old ( subbuffer, General_FONT_PARTY, x+506 +( i * 8 ), y + 266,
            General_COLOR_TEXT, "." );
      //-------------------
      if ( ( stat.elm_effect & mask ) > 0 )
         textprintf_old ( subbuffer, General_FONT_PARTY, x+506 +( i * 8 ), y + 282,
            General_COLOR_TEXT, "%c", STR_ITM_ELMEFFECT [ i ][ 0 ]);
      else
         textprintf_old ( subbuffer, General_FONT_PARTY, x+506 +( i * 8 ), y + 282,
            General_COLOR_TEXT, "." );
      //------------------
      if ( ( stat.elm_resist & mask ) > 0 )
         textprintf_old ( subbuffer, General_FONT_PARTY, x+506 +( i * 8 ), y + 298,
            General_COLOR_TEXT, "%c", STR_ITM_ELMEFFECT [ i ][ 0 ]);
      else
         textprintf_old ( subbuffer, General_FONT_PARTY, x+506 +( i * 8 ), y + 298,
            General_COLOR_TEXT, "." );
      mask = mask << 1;
   }


   textout_old ( subbuffer, Camp_FONT_INSTRUCTION, "Effect", 466, 330, General_COLOR_TEXT );

// NOTE: Item autoability Are drawn from column 2 code ( Equipment )

//   draw_instruction ( Draw_INSTRUCTION_SELECT, 550, 460 );
*/


