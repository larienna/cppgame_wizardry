@echo off

echo --- Building Sub Datafiles
del img_story.dat
dat img_story.dat -c1 -f -n1 -s0 -bpp 16 -t BMP -a img_story\*.bmp
echo --- Building %~dp0.wla ---
del ..\adventure.wla
dat ..\adventure.wla -c1 -f -n1 -s0 -a maze.bin database.sqlite img_story.dat credits.text

echo -----------------------------------
echo FINISHED: Rename adventure.wla to whatever you like.
pause