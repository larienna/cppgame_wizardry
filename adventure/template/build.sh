#!/bin/bash
dir=${PWD##*/}
echo --- Building Sub Datafiles
rm img_story.dat
dat img_story.dat -c1 -f -n1 -s0 -bpp 16 -t BMP -a img_story/*.bmp
echo --- Building $dir.wla ---
rm ../$dir.wla
dat ../$dir.wla -c1 -f -n1 -s0 -a maze.bin database.sqlite img_story.dat credits.text
