/* Allegro datafile object indexes, produced by grabber v4.4.2, MinGW32 */
/* Datafile: d:\cpp\Wizardry\DATAFILE\character.dat */
/* Date: Sat Jun 09 14:51:36 2012 */
/* Do not hand edit! */

#define BMP_CLASS000                     0        /* BMP  */
#define BMP_CLASS001                     1        /* BMP  */
#define BMP_CLASS002                     2        /* BMP  */
#define BMP_CLASS003                     3        /* BMP  */
#define BMP_CLASS004                     4        /* BMP  */
#define BMP_CLASS005                     5        /* BMP  */
#define BMP_CLASS006                     6        /* BMP  */
#define BMP_CLASS007                     7        /* BMP  */
#define BMP_CLASS008                     8        /* BMP  */
#define BMP_CLASS009                     9        /* BMP  */
#define BMP_CLASS010                     10       /* BMP  */
#define BMP_CLASS011                     11       /* BMP  */
#define BMP_CLASS012                     12       /* BMP  */
#define BMP_CLASS013                     13       /* BMP  */
#define BMP_CLASS014                     14       /* BMP  */
#define BMP_CLASS015                     15       /* BMP  */
#define BMP_RACEF000                     16       /* BMP  */
#define BMP_RACEF001                     17       /* BMP  */
#define BMP_RACEF002                     18       /* BMP  */
#define BMP_RACEF003                     19       /* BMP  */
#define BMP_RACEF004                     20       /* BMP  */
#define BMP_RACEF005                     21       /* BMP  */
#define BMP_RACEF006                     22       /* BMP  */
#define BMP_RACEF007                     23       /* BMP  */
#define BMP_RACEF008                     24       /* BMP  */
#define BMP_RACEF009                     25       /* BMP  */
#define BMP_RACEF010                     26       /* BMP  */
#define BMP_RACEF011                     27       /* BMP  */
#define BMP_RACEF012                     28       /* BMP  */
#define BMP_RACEF013                     29       /* BMP  */
#define BMP_RACEF014                     30       /* BMP  */
#define BMP_RACEF015                     31       /* BMP  */
#define BMP_RACEM000                     32       /* BMP  */
#define BMP_RACEM001                     33       /* BMP  */
#define BMP_RACEM002                     34       /* BMP  */
#define BMP_RACEM003                     35       /* BMP  */
#define BMP_RACEM004                     36       /* BMP  */
#define BMP_RACEM005                     37       /* BMP  */
#define BMP_RACEM006                     38       /* BMP  */
#define BMP_RACEM007                     39       /* BMP  */
#define BMP_RACEM008                     40       /* BMP  */
#define BMP_RACEM009                     41       /* BMP  */
#define BMP_RACEM010                     42       /* BMP  */
#define BMP_RACEM011                     43       /* BMP  */
#define BMP_RACEM012                     44       /* BMP  */
#define BMP_RACEM013                     45       /* BMP  */
#define BMP_RACEM014                     46       /* BMP  */
#define BMP_RACEM015                     47       /* BMP  */
#define ZZZ_END_OF_CHARACTER             48       /* TAG  */

