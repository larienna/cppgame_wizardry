/* Allegro datafile object indexes, produced by grabber v4.0.1, djgpp */
/* Datafile: E:\CPP\WIZARDRY\EDITOR.DAT */
/* Date: Wed Mar  3 10:11:33 2004 */
/* Do not hand edit! */

#define BMP_CURARROW                     0        /* BMP  */
#define BMP_CURDIG                       1        /* BMP  */
#define BMP_CURSELECT                    2        /* BMP  */
#define BMP_CURTILE                      3        /* BMP  */
#define BMP_PATGRID                      4        /* BMP  */
#define BMP_PATSELECT                    5        /* BMP  */
#define BMP_TILEDOOR                     6        /* BMP  */
#define BMP_TILEGRID                     7        /* BMP  */
#define BMP_TILESOLID                    8        /* BMP  */
#define BMP_TILESTART                    9        /* BMP  */
#define ZZZ_END_OF_EDITOR                10       /* TAG  */

