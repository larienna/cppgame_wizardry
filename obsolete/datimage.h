/* Allegro datafile object indexes, produced by grabber v4.2.2, Unix */
/* Datafile: /home/ericp/DataDrive/cpp/Wizardry/DATAFILE/IMAGE.DAT */
/* Date: Wed Jun 27 21:23:40 2012 */
/* Do not hand edit! */

#define BMP_ALLEGRO                      0        /* BMP  */
#define BMP_COMPANY000                   1        /* BMP  */
#define BMP_COMPANY001                   2        /* BMP  */
#define BMP_COMPANY002                   3        /* BMP  */
#define BMP_COMPANY003                   4        /* BMP  */
#define BMP_COMPANY004                   5        /* BMP  */
#define BMP_COMPANY005                   6        /* BMP  */
#define BMP_DRAGON01                     7        /* BMP  */
#define BMP_DRAGON03                     8        /* BMP  */
#define BMP_DRAGON05                     9        /* BMP  */
#define BMP_ICN_CASTLE                   10       /* BMP  */
#define BMP_ICN_CAVE                     11       /* BMP  */
#define BMP_ICN_CITY                     12       /* BMP  */
#define BMP_ICN_DARKNESS                 13       /* BMP  */
#define BMP_ICN_FIZZLE                   14       /* BMP  */
#define BMP_ICN_FOG                      15       /* BMP  */
#define BMP_ICN_IDENTIFY                 16       /* BMP  */
#define BMP_ICN_KEEP                     17       /* BMP  */
#define BMP_ICN_LEVITATE                 18       /* BMP  */
#define BMP_ICN_LIGHT                    19       /* BMP  */
#define BMP_ICN_POISON_GAS               20       /* BMP  */
#define BMP_ICN_PROTECT                  21       /* BMP  */
#define BMP_ICN_RUIN                     22       /* BMP  */
#define BMP_ICN_SDOWN                    23       /* BMP  */
#define BMP_ICN_SFRONT                   24       /* BMP  */
#define BMP_ICN_SLEFT                    25       /* BMP  */
#define BMP_ICN_SOBJ                     26       /* BMP  */
#define BMP_ICN_SRIGHT                   27       /* BMP  */
#define BMP_ICN_SUP                      28       /* BMP  */
#define BMP_ICN_SUPDOWN                  29       /* BMP  */
#define BMP_ICN_TEMPLE                   30       /* BMP  */
#define BMP_ICN_TOWER                    31       /* BMP  */
#define BMP_ICN_VILLAGE                  32       /* BMP  */
#define BMP_ICN_WATER                    33       /* BMP  */
#define BMP_ICN_WATER_BREATH             34       /* BMP  */
#define BMP_TITLE                        35       /* BMP  */
#define BMP_TOMB                         36       /* BMP  */
#define BMP_WINTEX000                    37       /* BMP  */
#define BMP_WINTEX001                    38       /* BMP  */
#define BMP_WINTEX002                    39       /* BMP  */
#define BMP_WINTEX003                    40       /* BMP  */
#define BMP_WINTEX004                    41       /* BMP  */
#define ZZZ_END_OF_IMAGE                 42       /* END  */

