/***************************************************************************/
/*                                                                         */
/*                           M A Z E P R O C . H                           */
/*                            Module Definition                            */
/*                                                                         */
/*     Content : Module MazeProc                                           */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : February 1st, 2003                                  */
/*     License : GNU General Public License                                */
/*                                                                         */
/*          Contains a list of procedures to be called externaly by the    */
/*     maze through function pointer.                                      */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                        Module Parameter                               -*/
/*-------------------------------------------------------------------------*/

#define MazeProc_NB_EVENT    256

/*-------------------------------------------------------------------------*/
/*-                           Constants                                   -*/
/*-------------------------------------------------------------------------*/

// event teleporter parameter
#define Maze_EVTPORT_XPOS    0
#define Maze_EVTPORT_YPOS    1
#define Maze_EVTPORT_ZPOS    2
#define Maze_EVTPORT_FACE    3
//?? maybe change the names

/*-------------------------------------------------------------------------*/
/*-                           Type definition                             -*/
/*-------------------------------------------------------------------------*/

typedef struct s_MazeProc_event
{
   bool (*proc)(s_Maze_event&);
}s_MazeProc_event;

/*-------------------------------------------------------------------------*/
/*-                         Global Variables                              -*/
/*-------------------------------------------------------------------------*/

extern const s_MazeProc_event MazeProc_EVENT [ MazeProc_NB_EVENT ];

/*-------------------------------------------------------------------------*/
/*-                            Prototypes                                 -*/
/*-------------------------------------------------------------------------*/

bool MazeProc_event_exit ( s_Maze_event &event );
bool MazeProc_event_moveup ( s_Maze_event &event );
bool MazeProc_event_movedown ( s_Maze_event &event );
bool MazeProc_event_teleport ( s_Maze_event &event );
bool MazeProc_event_rotator ( s_Maze_event &event );
bool MazeProc_event_message ( s_Maze_event &event );
bool MazeProc_event_special_combat ( s_Maze_event &event );

