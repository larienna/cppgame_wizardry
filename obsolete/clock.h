/****************************************************************************/
/*                                                                          */
/*                       G A M E C L O C K . H                              */
/*                                                                          */
/*   Content: Class GameClock                                               */
/*   Programmer: Eric Pietrocupo                                            */
/*   Starting Date: June 24th, 2012                                         */
/*                                                                          */
/*   This is some sort of time class that allows to keep track of the game  */
/*   clock. Only minute, hours and days are recorded. There is no calendar. */
/*                                                                          */
/****************************************************************************/

#ifndef CLOCK_H_INCLUDED
#define CLOCK_H_INCLUDED

// --- Stamp ---
// The stamp is stored as un unsigned int
// 16 bit for the days, 8 bit for the hours, and 8 bit for the minutes

#define GameClock_STAMP_DAY    0xFF00
#define GameClock_STAMP_HOUR   0x00F0
#define GameClock_STAMP_MINUTE 0x000F

/*--------------------------------------------------------------------------*/
/*-                             Class Definition                           -*/
/*--------------------------------------------------------------------------*/

class GameClock
{
   // properties
   private: unsigned short p_day; // Number of days passed in the game
   private: unsigned char p_hour; // Number of hours passed in the day
   private: unsigned char p_minute; // Number of minutes passed in the hour

   // Constructor and Destructor

   GameClock ( void );
   ~GameClock ( void );

   // Property Methods

   public: unsigned short day ( void );
   public: void day ( unsigned short value );
   public: unsigned char hour ( void );
   public: void hour ( unsigned char value );
   public: unsigned char minute ( void );
   public: void minute ( unsigned char value );
   public: unsigned int stamp ( void );
   public: void stamp ( unsigned int value );


   // Methods

   public: void add_day ( int value );
   public: void add_hour ( int value );
   public: void add_minute ( int value );
   public: const char* complete_str ( void );
   public: const char* time_str ( void );

}



#endif // CLOCK_H_INCLUDED
