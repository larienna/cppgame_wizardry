﻿
W I Z A R D R Y
L E G A C Y

Version : 0.1.6
Last Update : July, 2012
Copyright (C) 2002-2004,2006, 2012 Eric Pietrocupo
Original Copyright (C) 1981 Sir-Tech Software

Contains Copyrighted (C) material from these companies
- Sir-Tech Software	- Capcom		- Ascii
- Game Studio		- Media Factory		- Raven Software

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, go to www.gnu.org and download a copy.

Content
1. Introduction
2. Install
3. Setup
4. Files
5. Problems
6. Known Bugs
7. Options
8. Demo Overview
9. Compiling the Game
10. Stuff and Features Missing
11. Contact

>>> 1. Introduction >>>>>>>>>>

	Welcome to Wizardry legacy. This program is an open source game
based on the original Wizardry made by Sir-Tech Software.  It is also fun 
to state that wizardry is made by a canadian company is this remake is 
being made by a canadian (^_^). THe project was abandonned in 2004 and 
patched in 2006 but now it's back. The objective if to complete the 
project by making it a playable game. I intend to hope increasing the
development speed by using new technology and by reorganizing the structure
of the game.   


	The objective is to make a wizardry game that looks very close to
the original wizardry games with additional functionalities. Some changes
will be made to the game design and it will be shipped with an adventure editor to allow
players to make their quest. It might also be possible to eventualy
implement an Online version for simultaneous multiplayer game on the 
internet but don't expect me to do it.

	The game is currently in demo stage and it is not yet completly
operational. It also only comes with a test adventure only used for
programming testing and demo visualisation purposes. Real adventures will
come from other people who will use the Adventure editor.  I work on the
project when I have time and when I am motivated to do some coding. This
is why the project does not advance so fast, but still advances. I
hope some day the project will be finished ( well at least the basic
features ) but I don't guarantee anything. The project is now abandoned,
I am not sure if one day I am going to get back on it. I simply don't have
enough free time to put on this.

	If you want to help the development of the project, there is many
ways you can do it. Take a look at my web site or send me an e-mail for
more information. ( Contact information at the end of this document ).

>>> 2. Installation >>>>>>>>>>

	Installing the game is different according to the os distribution
you are using :

DOS / Windows : Simply unzip the wldos???.zip or wlwin???.zip file in a
directory. Then unzip the wldat???.zip in the same directory. Your now
ready for the setup instruction ( next section ). Warning, the zip now
contains path for subdirectories, unzip with -d option for command line
users.

Linux : Installing on linux is a bit more complex and might not
neccesarily work. First, like DOS/Window, unzip the file wllin???.tgz and
wldat???.zip in a directory. Unzip thise files with the -L option ( For
Unzip, Tar is OK ) to makesure they get lower cased ( Programm use lower
case file names. DOS ignores it but not Linux ).

	Then you must install the Allegro Library. To do so :

- Copy the file liballeg.so to the "/usr/lib" or "/usr/local/lib" directory.

- Make sure the path where you copied the file is in "/etc/ld.so.conf"

- Run "ldconfig"

	Try setup and starting the game. If it does not work, which can be
possible, type "ldd wizadry". It will give you a list of the libraries
required by the game. Make sure that you have these files in your lib path
( look in /etc/ld.so.conf ). 

	Finaly, it is still possible that it does not work, this can be
due to the fact that your libs are not the same version as mine or that we
have different kernel version. In this case, you will need to compile the
game yourself which mean that you will also need to compille allegro
library on your system ( Look on my web site to find the allegros Home
Page, and look below in the section "Compiling the game" for details how
to compile my game ).

>>> 3. Setup >>>>>>>>>>

	This programm use the Allegro Video game programming library which
contains can support many kind of computer hardware and is portable on
many operating system. 

	You should be able to download a version for each
OS available ( Dos, Window, etc. ). If not, you can download the
source code and recompile the game with the allegro library. I recommand
djgpp for DOS, Mingw32 for Window and gcc for Linux/Unix.

	There is a setup utility fusioned with the game which allows to
select your Digital and Music card. To activate this utility, use
setup.bat or type "wizardry.exe -setup". Set the options and make the
sound test to see if it works. 

	I have given with the game, and IBK file that contains MIDI
instrument definition. It might make the music sound better if you are
using a MIDI or OPLX driver. To load it, execute the setup program, select
your music device and double click on it. There will be 2 check square on
the next screen. Click on the left one and select the FMTIMBRE.IBK file
supplied with the demo. Midi Performance varies directly from the hardware
you have.

	The game run in 640x480x16bit colors. Basicly, the vesa driver
will be used but I think it is possible for the Allegro library to use the
windows driver ( if you are running the windows version ). There is a
large graphic driver selection, The allegro library will generaly take the
best available. Color depth is now 16 bit to allow the windows version to
run in full screen ( not possible with the 15 bit version ).

	The joystick and the mouse are simply not used in the game, so
there is no need to set them. (^_^)

	For Linux user, Since my linux installation does not have sound
support, it is possible that the binaries won't run music/sound and even
the mouse ( for the setup )

>>> 4. Files >>>>>>>>>>

	Here is the list of files that should have been supplied in the
packages :

- Root Folder :

Wizardry.exe : This is the programm itself. You must run this file to
	       start the game.

Setup.bat : A batch file that calls the setup utility from the game.

Readme.txt : Well in case you have not noticed, this is the file you are
	     currently reading.

Allegro.cfg : This file will be generated by the setup utility programm. 
              It contains the configuration selected during the setup. You
	      can edit this file by hand if you know how it works.

Wizardry.cfg : This file will be created after the first time you run the
	       game ( except in the windows version : see Problems" . It
               contains the configuration of the games which can
	       be changed through the option menu. Deleting it will reset
	       game options to default ( except the windows version
               which will make the game crash : see Known bugs ).

System.dba : This is a database file which contains all the information
             about the current games available and the account created.

Gnugpl.txt : This is the definition of the GNU General Public Licence
	     protecting this programm. In a few word, it give you the
	     right to copy and modify the programm but you cannot make
	     profit of any kind with the game. ( The licence is a bit old,
	     I should download a new one ).

Liballeg.so : If you are using the linux distribution, this file will be
	included. It is the allegro library. It will prevent you from
	compiling it. Look at installation instruction for more details.

Alleg40.dll : If you are using the windows distributionm this file should
	be included. It is the allegro library for windows. It will be
	used when running the game.	

- Datafile folder : These files are included from the Datafile package.
This package is downloaded seperatly from the binary distribution.

Setup.dat : This is the data file used by the setup programm.

Keyboard.dat : This is a database of information for the management of the
               keyboard, used by the setup programm.

Language.dat : This is a database of information for the language
               management, used by the setup programm.

Fmtimbre.ibk : Instrument definition file supplied with the game that can
	       be selected in the setup programm to get better instruments.

Wizardry.dba : A database file ( dba = Database Archive ) which contains
               the game basic data for Equipment stats, Monsters stats,
               etc. This data is common to all adventure.

Audio.dat    : Wizardry Datafile : Contains sound and musics

Editor.dat   : Wizardry Datafile : Contains Editor related images.

Ennemy.dat   : Wizardry Datafile : Contains ennemy pictures.

Image.dat    : Wizardry Datafile : Unclassified various pictures.

Maztex.dat   : Wizardry Datafile : Maze related images and textures.

- Savegame folder : Created during game

sav???.dba : These are savegames created when playing the game.

- Adventure Folder :

demo.wza : This is a demo adventure for testing and demonstration purpose.


>>> 5. Problems >>>>>>>>>>

	The DOS version does not perfectly work on windows. The
probability to work is influenced by the temperature of the room and the
positions of the stars in the sky. I tried it on Windows XP and it
partialy works ( with a color bug ). I still suggest you use the windows
version. I tried to run the Allegro's palette test program on Windows 2000
and it did not even support the 8bit graphic mode. Which mean that if you want 
to run the DOS on windows, cross your fingers and run the programm, if it
works, then your are a lucky winner. Else download the windows version.

	The windows version is still not perfect. There is some
protability problems from a windows version to another. It seems that
windows XP is not really backward compatible with it's previous OS. The
windows version should have been compiled under XP or 98. Cross your
finger and run the game. If the color bug in the maze stay remove
remove the POLYGON SHADING option in the option menu for the
right texture colors. 

	If you have a 3D video card, there is a bug in the maze polygon
drawing. There is a white bar to the left and a white stain to the right
that sometimes appear when walking in the maze. Just ignore it, it's
normal. I have not taken time to correct this bug if it is correctable.

	It is also possible, for the windows version, that an error
message appear when you start the programm saying that this programm
requires COM and it is not installed on your computer. This should
escpecialy be true if you don't have any microsoft compiler on your
computer. You can simply click OK and ignore this message since I don't
use COM in my programm and I don't think allegro does it too. 

	If you have a video card that can display high resolution true
color mode, and if at the same time you only have a VESA BIOS 1.x on the
card, you might experience a problem. What will happen is that the video
mode will be switched but nothing or strange white bars will appear on the
screen. In this case, you need Sci-tech display doctor software to patch
the VESA BIOS. You can download an evaluation copy of the software from
Sci-tech's web site which is available for many OS. 

	If you are running the programm in DOS and you receive a "Cannot
load DPMI" as an error message when you start the game, this mean you need
a DPMI ( DOS Protected Mode Interface ) driver. The most popular and free
DPMI driver available is CWSDPMI. You can find it on the internet or on my
web site in the Download -> Other Section. 

	This program require this DPMI driver because it has been compiled
in 32 bit protected mode while DOS runs in 16Bit real mode. Most modern OS
like Windows, Linux already run in 32bit protected mode so they don't need
a driver to switch in protected mode. Unfortunatly, Free DOS 32 is not yet
available yet so we need a DPMI driver to do the job.

	If you have other problems, refer to the known bug section below
or send me a e-mail.

>>> 6. Known Bugs >>>>>>>>>>

- The Color Bug : All the textures in the maze get weird collors when
using POLYGON SHADING. Since shading creates more derived colors, maybe it
does not support that much colors. Still I don't know why this bug happens
because in 16bit color modes, there is not color palette so you should
technicly never run out of colors. If this bug occur, turn the POLYGON
SHADING OFF. This bug generaly occurs in windows.

- Sound frequency bug : For unknown reasons, sometime the sound is played
at twice the normal frequency which makes the sound shorter.This bug
does not necessarily manifest itself on all platform.

- Music Noise bug : Sometime, the music is played like if it was too loud
which create noise and flickering in the music sound.  This bug
does not necessarily manifest itself on all platform.

- Transparent wall : Sometimes, when setting no textured walls, the walls
become simply transparent and you don't see them at all. but you can still
see all the masked textures.

	Found a new bug, notice me by e-mail. Note that not all user
interface are perfect, it happen sometimes, for example, that the
instruction are not at the right place. I don't consider these an
important bug.

>>> 7. Options >>>>>>>>>>

	There is a few options you can set in the game from the option
menu that can be accessed from the startup screen and the camp menu. There
is a few thing you can set :

POLYGON TEXTURE : You can select if the texture will be displayed on the
walls of the maze. It will only affect wall textures, doors will still be
drawn with textures.

POLYGON RENDERING : There is 2 different kind of polygon drawing, the
Perspective-correct method and the Affine method. The affine method is
faster but the textures are wrongly drawn, because they do not consider
the perspective of the polygon, which makes them look like if they were
curved. POLYGON RENDERING is unaffected if POLYGON TEXTURE is set to NO. 

POLYGON SHADING : You can set or remove the light shading on the wall by
using this option. This will dramaticly increase the drawing speed. 
Shading will also affect textureless walls. Removing shading will also
remove the color bug in windows.

TRANSPARENT TINT : There are additional transparent walls to draw and
screen tinting to do when traveling in hazardous environment like : Fizzle
Fields, Poison Gas, Fog, Water, etc. This option allows you to remove the
drawing of these trasparent walls to gain an extreme gain in speed. There
is still an icon on the top right of the screen which indicates if you are
in an hazardous environment. Removing transparency will also dramaticly
increase the drawing speed.

FLOOR & CEILING : To gain additional speed, you can set this option to
"No" to remove the Floor and Ceiling drawing. The Ceiling and the Floor
will stay black and only the walls will be drawn. Special floors and
ceiling will still be drawn.

DISPLAY KEY USAGE : This option allows to set or remove text indicating
the functions of each keyboard keys in the current screen.

DISPLAY INSTANT HELP : This option allows to display instant help text
shown in various screen of the game. Usefull for beginner. ( Sorry, not
implemented yet )

MUSIC VOLUME : Allows to change the volume of the music. This option
currently does not work on my computer for a reason I don't know. ( I
can't even adjust the sound from setup program ).

SOUND VOLUME : Allow you to change the volume of the sound effect in the
game. Compared to the MUSIC VOLUME option, this option works perfectly.

CANCEL/SELECT KEY CONFIG : This option allows you to change which key is
used as the Cancel and Accept button. The default is A and Z but here you
have a chance to change it. 

DISPLAY PARTY BAR : When this option is set, you will see the list of your
characters in your party at the bottom of the screen when traveling in the
maze.

DISPLAY SPELL ICONS : This option will draw icons on the top left of the
screen representing the current party spell active when traveling in the
maze.

DISPLAY MAZE ICONS : This option will draw icons on the top right of the
screen indicating various things related to the maze.

BORDER TYPE : You can change the type of borders the windows will use
when drawn on the screen. Set it to your taste from the choices available:
Thick, Thin, Double, Flat or None.

ANIMATIONS : There will a be few extra animations in the game and this
option allows you to activate them. There is currently only the teleport
effect in the game. You should turn this off if your computer is not at
least a pentium. It will save you time.

WINDOW BACKGROUND : This option determines the type of background you want
in your drawn in your windows. Two modes are available UNICOLOR, which
makes the windows drawn with the same color, or TEXTURED which draws a
texture in the background of the windows. The color or the texture used
can be set with the 2 following configuration option.

WINDOW TEXTURE : This option set which texture is going to be used to draw
the background of the window if the WINDOW BACKGROUND drawing mode is set
to TEXTURED else it is ignored. The available options are : Earth, Scroll,
Forest, Slate, Rock. 

WINDOW COLOR : This option set the color which is going to be used to draw
the background of the window if the WINDOW BACKGROUND drawing mode is set
to UNICOLOR else it is ignored. The available colors are : Black, Green,
Blue, Purple, Ocean.

>>> 8. Demo Overview >>>>>>>>>>

	Here is a few instruction on how to view everything there is to
view in the demo.

	Note that the keys used by the demo are these. These key are
always displayed in all the screens of the game unless you set the
"Displayer key usage" option off.

A : Cancel
Z : Accept
Enter : Accept text
Up/Down : Navigate in menu
Left/Right : Change option
Arrows : Move in the maze

	You can also change the accept and cancel key in the option menu.

	Starting a game is now a bit more complex than before since now
the account system works. But don't worry, a feature not yet implemented
called "Continue Game" will allow to skip these steps and go directly to
the game.

1. Once the game is setup, start the wizardry.exe programm. There will be
a loading screen, a copyright screen and then the title screen will
apprear. If you wish, you can visit the art gallery to hear a few music or
look at a few graphics of the game by selecting "Art Gallery". Else Select 
"Start Game" form the main menu.

2. You now enter the Wizardry's management system. Here you can create and
manage accounts and game. First, you need to register the adventure file.
Select "Register Adventure", and then "demo.wza".  Second you need to
create a game. Select "Create game", select the adventure you have just
created, Select difficulty "easy", levelup "fast" and select "no" when
they ask you if you want to protect your game. Then select "Create
account", enter the information asked : Loginame, fist name, family name,
nickname and then password. Remember your login name and password.
Remember that authentification is case and space sensitive. When you are
done, select "login", enter the loginame of your account and your
password. 

3. You now enter the account's menu. From here you can join an old/new
game or retire from a game you have already played. Select "Join a game",
the list of the games created will appear. Select one, then a message will
appear. Select "Yes" to create a player in the game. You will see the
intruduction story of the adventure ( there is no way to skip it, but you
only see it once ).From now on you are in the game and all data will be
saved in this game.

4. You now should be in the "Player's Domain" . Select "Create
character" and folow the questions. You must first enter the names of your
character. Then select your sex, race and aligment with the menus. You
will also have to set the attributes of your character and select 3
skills.  When your character is created, repeat the process a few times to
have more than 1 character. You can create up to 18 characters for a
player.

5. If you wish, you can use the "Inspect Character" command in the
"Player's option" menu to look in detail at the statistics of your
character. And you can also use the "Delete Character" to remove a
character from the list.

4. Before you can start a game, you need to create a party. You can create
up to 3 parties of 1 to 6 characters. When you have a few characters
available, you select the command "Create Party" in the player's option,
then you select the characters in the list and they will be added to your
party. You can stop adding characters by canceling ( A Key ) or add
all your 6 characters. If you do the "Create party" command again, you
will create a 2nd party. When your selection is done, you will be prompt
for a party name, enter the name you wish. You cannot add together
characters with opposite aligment.

5. If you made an error, you can use the "Disband party" command to
disassemble a party of characters. Else select the "Select party" command.
A list of all the parties whith their character's will be shown. Select
any party you wish from the with the selection menu at the top of the
screen.

6. When you select your party, the game start and you appear in a city. 
The city is not completly functional. You can rest at the INN by selecting
"Adventurer's Inn" -> "Rest" -> the type of room you wish to used -> and
finaly the number of days. You can also visit the "Central Market" and
"Buy"  different kind of "Weapons" and "Armors" for your character. You
will be able to equip them later in the camp menu. Then, to enter the
Maze, select "Edge of town" and then "Explore Dark Cave". Else If you want
to go back to the player's Domain screen select "Game Master's Den"  then
"Quit Game". 

7. Select "Edge of Town"->"Explore Dark Cave" to enter the maze. When you
are in the maze, you can move with the up/left/right arrows. You can test
the frame rate speed by holding the left or right key and counting the
number of frames you can see in a seconds.If does not give precise values
but it give you a good idea. If it is less than 4 frame seconds, you can
always go to option screen from the camp menu and set the Polygon Shading,
Polygon Texture transparency, floor and ceiling drawing to NO. 

8. If you press the A key, the camp menu will appear. There is currently
only 1 feature implemented and it is the character inspection. You can
inspect any character in your party. When inspecting characters, a menu
will allow you to "equip", "Unequip" or "Drop" an Item in your possession.
To add items in your character inventory, go buy some stuff in the city. (
nothing is free you know )

9. The maze is a tutorial maze to understands the features of the maze.
Walk foward and follow the instructions messages. The demo maze will loop
back at the beginning of the maze when it is finished so that you can
exit. You will encounter ennemies in the maze, when engaging combat, the
combat will only last 1 round whatever you do.

10. When back at the city, select "Game Master's den"-> "quit game", then
"Exit" the player's menu, logout the account, and exit the manager's menu
to return to the title screen. If you wish, you can now quit the game from
the maze camp command "quit party". It will send you back to the players's
domain and selecting this party again will make you start back where you
were in the maze.

	So this is it, I hope you have enjoyed your tour of the game. If
you have any comments or suggestions, contact me. 

>>> 9. Compiling the game >>>>>>>>>>>

	Here are a few helpfull instruction before attempting to compile
the game. 

1. Get a full functional C++ working compiler. The best is to use the Gcc
Compiler or one of it's port ( Djgpp, Mingw32, gcc ).

2. Get the allegro library and compile it. To see if
it works, run the examples programms. Supplied with the library.

3. The database library is now supplied as an external library, you need
to download and compile it. The DDT
library, which stand for Dynamic database table, can be downloaded on my
allegro website at:

ariel.bdeb.qc.ca/~ericp/allegro
look in : Software-> Dynamic Database Table

4. Modify the makefile of the game. At the top of the make file, I have
placed a information which can be changed. Technicaly it should works, but
maybe you could need to change the path of the header if they don't find
them or you can add or remove compiler options.

	For example, djgpp name standarc c++ librairy as "stdcxx" while
Mingw32 name it as "stdc++". So there might be a few changes to be done
according to your compiler. I made the makefile by myself so it is less
cryptic than the generator I used. 

	If you are compiling on linux, look for the comments "LINUX
User" in the make file and follow indications.

5. Unzip the data file where wizardry.exe is then you can now start the
setup program by starting "setup" or "wizardry -setup" and then starting
to play by typing "wizardry". 

	Some problem you might encounter when compiling :

- Files can't be found : Sometime, the make file cannot found the files to
compile. Make sure the path are set correctly at the beginning of the make
file, and make sure your filenames are in lower case if you are compiling
on a linux/unix system.

- Seting the optimisation parameter ( O1, O2, O3 ... ) could make the
programm crash or not making it work properly. There is somekind of bug
somewhere.

- Error in the code : Some compiler does not suport having default
parameter value in the source code. So you must remove them  :

example : "void proc ( int variable = 0 )" in source .cpp file
change to : "void proc ( int variable )"

	It is also possible it setup.c that it gives an eror about "\". To
solve this problem, there is a multi-line #define in the code. You should
remove the backslash and place all the commands on the same line.

Example:
#define code1	\
{		\	
code2;		\
code3;		\
code...;	\
}

Change to:
#define code1	{ code2; code3; code...; }


   Another weird problem, is the support of some standard library
functions.

example :
windows dows not support : rawclock(), random(), srandom()
linux does not support : itoa(), strlwr(), strupr()

	Fortunatly they have been replaced. The windows version have
proprocessor command that use rand() instead random() while the other
platform continue to use random(). When compiling on other platforms, if
you have an random() or srandom() undeclared, you replace them with
rand() and srand(). Random is declared as a macro in general.h and srandom
is used in the init.c file. The reason I keep using random() is because
rand() on low values gives numbers which are not so random. This is why I
must use random() instead. Linux and dos should generaly support random().

>>> 10. Stuff and Features Missing >>>>>>>>>>

	Officialy, the game is playable even if it is not complete. Even
if playable, it won't be fun to do some long term play since there is too
much features missing to be considered a Beta Game. Here are the feature
currently Missing to the game :

Spells and Magik : The spell casting system and effects on the rules are
not yet implemented. The Character classes contains informations on who
can cast spell, but they can simply not cast them.

Race Abilities and Character Class : Special Features given by race and
classes like Natural Magic or Dispel, Prayer, Hide commands in combat are
not implemented yet. You cannot pick locks or remove traps, heal
characters, etc.

City, Camp, Maze, item features : Some services in the cities are not
implemented (like cure and revival ), Command in camp like reorder,
search, are mot implemented and most event in maze like traps, chest,
secret doors, are not implemented either. The various abilities a magikal
item can have ( Ability, Drawbacks, Autoability ) are not implemented yet.

No Map Editor : There is no map editor available to draw maze maps. It is
essential to be able to make an adventure even if there is no editor for
the rest of the adventure's information. Which mean that there is no
adventure available to play.

Level Humanoid Ennemies : This is no humanoid levelupable ennemies ( ex:
Lv 3 Fighters, Lv5 Mages ) you will only encounter monsters.


>>> 11. Contact >>>>>>>>>>

	If you need more information or if you want to help in the
development of the game you can always contact us by visiting our website
or by writing to me directly and e-mail. 

- Web Site
Contains many information about the design of the game, the
updates made so far and the possibility to download wizardry related
stuff.

ariel.bdeb.qc.ca/~ericp/wizardry

   If you want to access my website dedicated to allegro video game
programming in general, go at the address below. This is also the place
where you can download my dynamic database table library.

ariel.bdeb.qc.ca/~ericp/allegro

- E-mail : You can contact me directly if you have any question, remarks,
suggestion or feedback to give.

ericp@ariel.bdeb.qc.ca


				Please enjoy the Wizardry Legacy Demo.

							Sayonara (^_^)

							Eric Pietrocupo	
							The Shadow







