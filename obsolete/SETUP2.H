/* Allegro datafile object indexes, produced by grabber v3.11 */
/* Datafile: e:\allegro\setup\setup.dat */
/* Date: Sun Apr 11 11:27:06 1999 */
/* Do not hand edit! */

#define BACKGROUND                       0        /* BMP  */
#define SETUP_FONT                       1        /* FONT */
#define SETUP_PAL                        2        /* PAL  */
#define TEST_MIDI                        3        /* MIDI */
#define TEST_SAMPLE                      4        /* SAMP */

#define SETUP_EMBEDDED

extern "C" {

#ifdef SETUP_EMBEDDED
int setup_main(void);
#else
int main(void);
#endif

}

