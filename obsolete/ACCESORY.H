/***************************************************************************/
/*                                                                         */
/*                        A C C E S O R Y . H                              */
/*                        Class Definition                                 */
/*                                                                         */
/*     Content : Class Accessory                                           */
/*     Programmer : Eric Pietrocupo                                        */
/*     Stating Date : June 9th , 2002                                      */
/*                                                                         */
/*          This is a derived class from Iitem which contains the          */
/*    information of accessory equipement which can be equiped on a        */
/*    character.                                                           */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                         Class Parameter                               -*/
/*-------------------------------------------------------------------------*/

#define Accessory_NB_STAT     3

/*-------------------------------------------------------------------------*/
/*-                           Constants                                   -*/
/*-------------------------------------------------------------------------*/

// stat to modify by bonus

#define Accessory_STAT_NONE      0
#define Accessory_STAT_PD        1
#define Accessory_STAT_AD        2
#define Accessory_STAT_MPD       3
#define Accessory_STAT_MAD       4
#define Accessory_STAT_DR        5
#define Accessory_STAT_MDR       6
#define Accessory_STAT_PSAVE     7
#define Accessory_STAT_MSAVE     8
#define Accessory_STAT_HIT       9
#define Accessory_STAT_INIT      10
#define Accessory_STAT_DMG_Z     11
#define Accessory_STAT_MDMG_Z    12


/*-------------------------------------------------------------------------*/
/*-                         Type Definition                               -*/
/*-------------------------------------------------------------------------*/

typedef struct dbs_Accessory
{
   dbs_Item item_data;// a_packed;
   tiny statID [ Accessory_NB_STAT ];// a_packed;
   tiny bonus;// a_packed;

}a_packed dbs_Accessory;

/*-------------------------------------------------------------------------*/
/*-                        Class Definition                               -*/
/*-------------------------------------------------------------------------*/

class Accessory : public Item
{

   /*--- Properties ---*/

   private: tiny p_statID [ Accessory_NB_STAT ]; // identification
   private: tiny p_bonus; // value to apply to the 3 statID
/*   private: tiny p_PD;
   private: tiny p_AD;
   private: tiny p_MPD;
   private: tiny p_MAD;
   private: tiny p_DR;
   private: tiny p_MDR;
   private: tiny p_Psave;
   private: tiny p_Msave;
   private: tiny p_hit;
   private: tiny p_init;
   private: tiny p_dmg_z;
   private: tiny p_mdmg_z;*/

   /*--- Constructors & Destructors ---*/

   public: Accessory ( void );
   public: Accessory ( DBTag Acctag );
   public: ~Accessory ( void );

   /*--- Property Methods -- -*/

   public: tiny statID ( tiny index );
   public: void statID ( tiny index, tiny value );
   public: tiny bonus ( void );
   public: void bonus ( tiny value );

/*   public: tiny location ( void );
   public: void location ( tiny value );*/
/*   public: tiny PD ( void );
   public: void PD ( tiny value );
   public: tiny AD ( void );
   public: void AD ( tiny value );
   public: tiny MPD ( void );
   public: void MPD ( tiny value );
   public: tiny MAD ( void );
   public: void MAD ( tiny value );
   public: tiny DR ( void );
   public: void DR ( tiny value );
   public: tiny MDR ( void );
   public: void MDR ( tiny value );
   public: tiny Psave ( void );
   public: void Psave ( tiny value );
   public: tiny Msave ( void );
   public: void Msave ( tiny value );
   public: tiny init ( void );
   public: void init ( tiny value );
   public: tiny hit ( void );
   public: void hit ( tiny value );
   public: tiny dmg_z ( void );
   public: void dmg_z ( tiny value );
   public: tiny mdmg_z ( void );
   public: void mdmg_z ( tiny value );*/



//   public: const char* locationS ( void );

   /*--- Methods ---*/

   public: virtual void objdat_to_strdat ( void *dataptr );
   public: virtual void strdat_to_objdat ( void *dataptr );
   public: virtual void child_DBremove ( void );

   /*--- Private Methods ---*/

   /*--- Virtual Methods ---*/
};

/*------------------------------------------------------------------------*/
/*-                          Global Variables                            -*/
/*------------------------------------------------------------------------*/

extern const char STR_ACC_LOCATION [] [6];
extern const char STR_ACC_STAT [] [21];



