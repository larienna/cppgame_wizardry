/**************************************************************************/
/*                                                                        */
/*                            T E X T U R E . H                           */
/*                                                                        */
/*     Content: Texture Class                                             */
/*     Programmer: Eric Pietrocupo                                        */
/*     Creation Date: May 11th, 2013                                      */
/*                                                                        */
/*     This class encapsulate the referencing of textures to make sure    */
/*     they each have a unique ID what ever the expansion of the tex set. */
/*                                                                        */
/**************************************************************************/

#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

// ----------------------- Constant Parameters ----------------------------

#define Texture_SIZE             256   // ex: 128x128
#define Texture_CODE_SIZE        6 // nb of characters of codes   1+1+3+\0

class Texture : public SQLobject
{

   // --------------- Properties ---------------------

   private: char p_code [ Texture_CODE_SIZE ]; // unique code of the texture including the set name
   private: int p_setid; // determine which texture set (datafile) to use.
   private: int p_picid; // id of the refered picture in the datafile
   //private: char p_category; // copy of the category character for sorting (if cannot use SQL)
   // use SUBSTRING(colName, 2, 1)

   // --------------- Constructor & Destructor ----------------

   public: Texture ( void );
   public: virtual ~Texture ( void );


   // -------------- Property Methods ----------------------

   public: char *code ( void ) { return ( p_code ); }
   public: void code ( char* tmpcode ) { strncpy ( p_code, tmpcode, Texture_CODE_SIZE ); }
   public: int setid ( void ) { return ( p_setid ); }
   public: void setid ( int value ) { p_setid = value; }
   public: int picid ( void ) { return ( p_picid ); }
   public: void picid ( int value ) { p_picid = value; }

   // --------------- methods ------------------------------

   // --------------- Virtual Methods ----------------------

   public: void virtual sql_to_obj (void);
   public: void virtual template_sql_to_obj ( void);
   public: void virtual obj_to_sqlupdate (void);
   public: void virtual obj_to_sqlinsert (void);

}
/*	edit	delete	0	pk	INTEGER	yes		yes
	edit	delete	1	code	TEXT	yes		no
	edit	delete	2	setid	INTEGER	no	1	no
	edit	delete	3	picid	INTEGER	no	0	no*/


#endif // TEXTURE_H_INCLUDED
