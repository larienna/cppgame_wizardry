/**
* S Q L R E C O R D S E T . H
*
* Programmer: Eric Pietrocupo
* Starting Date: October 15th, 2017
*
* Structure that handle data recieved from an SQL SELECT query
* with methods to manipulate this information. There is no need to hold the whole
* recordset into memory for the need of this game since there is no back and forth
* navigation of records. So the recordset will hold the structure and the
* current record that was read.
*
* NOTE: By putting more thoughts into it, there might be no need for record set as
* regular step and prepare methods can do virtually the same job. There is also no need
* to find real field names or support back and forth navigation. I might only modify
* the old code to support variable lenghth query as they could be important now.
*
* This seems obsolete from the start. Old codecan be used perfectly.
*/

#ifndef SQLRECORDSET_H_INCLUDED
#define SQLRECORDSET_H_INCLUDED

// Ask to put the code as regular C
#ifdef __cplusplus
extern "C" {
#endif

//----- Structure definitions -----

/**
* Contains the information on each field.
*/
typedef struct
{
   char *name;
   char *tablename;
   int type;
}s_SQLrs_field;
//TODO not sure how you can set aliases on a field and how to query them for display

/**
* Contains an array of data stored as string for the flexibility. Using struct prevent
* the need of using a two dimension array.
*/
typedef struct
{
   //char *data[];
}s_SQLrs_record;

/**
* Use for making the correspondance from field name to index. It will keep the field list
* sorted so that binary search can be used. When populated, the content never changes.
*/
typedef struct
{
   char *fieldname;
   int index;
}s_SQLrs_field_to_index;


/**
* Structure that holds the whole recordset information using regular C struct instead
* of classes. Dynamic allocation will be required since the number of fields and records
* are variable.
*/
typedef struct
{
   int nb_records;
   int nb_fields;
   //s_SQLrs_field field[];
   //char **data; // Pointer on an array of pointers to strings
   //s_SQLrs_field_to_index fieldmap[];
}s_SQLrecordset;

//----- Prototype Definition -----

/** Creates a recordset from a query */
void  SQLrs_create ( s_SQLrecordset *rs, sqlite3 *db, const char *query );
/** Creates a recordset from a query and allow passing formated parameters*/
void  SQLrs_createf ( s_SQLrecordset *rs, sqlite3 *db, const char *query, ... );
/** Free the memory allocated by a recordset */
void  SQLrs_free ( s_SQLrecordset rs );
/** Get the index key of a field where you only have the name */
void  SQLrs_indexof ( s_SQLrecordset rs, const char *name );
/** Convenience method to read data of a specific field and record */
char *SQLrs_data ( s_SQLrecordset rs, int recordid, int fieldid );
/** Read data of a field in a record and convert it to integer */
int   SQLrs_data_int ( s_SQLrecordset rs, int recordid, int fieldid );
/** Read data of a field in a record and convert it to float */
float SQLrs_data_float ( s_SQLrecordset rs, int recordid, int fieldid );
/** Print all the information about the structure of the recordset */
void SQLrs_print_structure ( s_SQLrecordset rs );
/** Display all the fields of the recordset */
void SQLrs_print ( s_SQLrecordset rs );

#ifdef __cplusplus
} // end of extern C
#endif

/* methods ideas from Pixel Board Recordset
Navigation: next, forward, etc. Because methods return currrent position information
Data: Read data into various format: string, int, float, etc. at current record position.
      Those methods accessible with field index or field name.
to_string: Print structure information or data information
structure: Get information about structure. Can use field direct access since eveything is public
Other SQL operation: Insert, update delete. Not very good. Use separately, RS is for read only.
*/

#endif // SQLRECORDSET_H_INCLUDED
