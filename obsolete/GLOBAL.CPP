// include group
//#include <grpstd.h>
#include <grpsql.h>

#include <general.h>
#include <allegro.h>


#include <datafile.h>
#include <datmacro.h>
#include <system.h>
//#include <init.h>
//#include <menu.h>
#include <option.h>
//#include <screen.h>
//#include <dbdata.h>
//#include <dbtag.h>
//#include <database.h>
//#include <dbobject.h>
#include <ddt.h>
#include <dbdef.h>



//

//

//
//#include <list.h>

#include <opponent.h>
#include <charactr.h>
#include <ennemy.h>
#include <party.h>

#include <game.h>

#include <city.h>
#include <encountr.h>
#include <maze.h>
//
//#include <camp.h>
#include <config.h>
//#include <draw.h>
//#include <dialog.h>
#include <combat.h>
#include <window.h>
#include <mazeproc.h>
//#include <wdatproc.h>
//#include <sinterfc.h>
//#include <siproc.h>
//#include <sqlite3.h>

/*-------------------------------------------------------------------------*/
/*-                      Global Variables                                 -*/
/*-------------------------------------------------------------------------*/

//string dummystr ("" );
//s_Item_ability dummyability;
//s_Item_effect dummyeffect;


//char filelist [ General_FILELIST_SIZE ] [ 13 ];
//short flistindex = 0;

//Random rndvalue;

// keyboard key config





//Player player; // player logged in the client
//Party player_party; // party of the client player








// maze wall polygon vertex definition :

//   V3D vertex [ 4 ]
/*   bool flipok; // indicates if this wall can be flipped
   bool flipisodd; // true if flip on odd, else flip on even
   tiny thickwallID; // thickwall id to use with wall, -1 = not thick wall
*/


/*
word SKILL_LEVEL [ Character_NB_SKILL_LEVEL ] =
{ 100, 250, 450, 700, 65000 };
  */


//Weapon DATABASE_WEAPON [ Weapon_DATABASE_SIZE ];

//Armor DATABASE_ARMOR [ Armor_DATABASE_SIZE ];





// seems to be old system

/*s_Opponent_limit_stat_info LIMIT_STAT [ Opponent_NB_STAT ] =
{

   {  0,  95 }, // Opponent_STAT_EVADE
   {  0,  90 }, // Opponent_STAT_RESIST
   {  0,  50 }, // Opponent_STAT_DISSIPATE
   {  0, 100 }, // Opponent_STAT_BULK
   {  0,  90 }, // Opponent_STAT_COVER
   {  0,  95 }, // Opponent_STAT_DURABILITY00
   {  0,  95 }, // Opponent_STAT_DURABILITY01
   {  0,  95 }, // Opponent_STAT_DURABILITY02
   {  0,  95 }, // Opponent_STAT_DURABILITY03
   {  0,  95 }, // Opponent_STAT_DURABILITY04
   {  0,  95 }, // Opponent_STAT_DURABILITY05
   {  0,  95 }, // Opponent_STAT_DURABILITY06
   {  0,  95 }, // Opponent_STAT_DURABILITY07
   {  0,  95 }, // Opponent_STAT_DURABILITY08
   {  0,  95 }, // Opponent_STAT_DURABILITY09
   {  0,  50 }, // Opponent_STAT_ABSORB00
   {  0,  50 }, // Opponent_STAT_ABSORB01
   {  0,  50 }, // Opponent_STAT_ABSORB02
   {  0,  50 }, // Opponent_STAT_ABSORB03
   {  0,  50 }, // Opponent_STAT_ABSORB04
   {  0,  50 }, // Opponent_STAT_ABSORB05
   {  0,  50 }, // Opponent_STAT_ABSORB06
   {  0,  50 }, // Opponent_STAT_ABSORB07
   {  0,  50 }, // Opponent_STAT_ABSORB08
   {  0,  50 }, // Opponent_STAT_ABSORB09
   {  0, 100 }, // Opponent_STAT_HIT00
   {  0, 100 }, // Opponent_STAT_HIT01
   {  0, 100 }, // Opponent_STAT_HIT02
   {  0, 100 }, // Opponent_STAT_HIT03
   {  0, 100 }, // Opponent_STAT_HIT04
   {  0, 100 }, // Opponent_STAT_PARRY00
   {  0, 100 }, // Opponent_STAT_PARRY01
   {  0, 100 }, // Opponent_STAT_PARRY02
   {  0, 100 }, // Opponent_STAT_PARRY03
   {  0, 100 }, // Opponent_STAT_PARRY04
   {  0, 100 }, // Opponent_STAT_UNPARRY00
   {  0, 100 }, // Opponent_STAT_UNPARRY01
   {  0, 100 }, // Opponent_STAT_UNPARRY02
   {  0, 100 }, // Opponent_STAT_UNPARRY03
   {  0, 100 }, // Opponent_STAT_UNPARRY04
   {  0,  95 }, // Opponent_STAT_COMBO00
   {  0,  95 }, // Opponent_STAT_COMBO01
   {  0,  95 }, // Opponent_STAT_COMBO02
   {  0,  95 }, // Opponent_STAT_COMBO03
   {  0,  95 }, // Opponent_STAT_COMBO04
   {  0,  95 }, // Opponent_STAT_MULTI00
   {  0,  95 }, // Opponent_STAT_MULTI01
   {  0,  95 }, // Opponent_STAT_MULTI02
   {  0,  95 }, // Opponent_STAT_MULTI03
   {  0,  95 }, // Opponent_STAT_MULTI04
   {  0, 100 }, // Opponent_STAT_PENETRATION00
   {  0, 100 }, // Opponent_STAT_PENETRATION01
   {  0, 100 }, // Opponent_STAT_PENETRATION02
   {  0, 100 }, // Opponent_STAT_PENETRATION03
   {  0, 100 }, // Opponent_STAT_PENETRATION04
   {  0,  50 }, // Opponent_STAT_DAMAGE00
   {  0,  50 }, // Opponent_STAT_DAMAGE01
   {  0,  50 }, // Opponent_STAT_DAMAGE02
   {  0,  50 }, // Opponent_STAT_DAMAGE03
   {  0,  50 }, // Opponent_STAT_DAMAGE04
   {  0,  50 }, // Opponent_STAT_SHOCK00
   {  0,  50 }, // Opponent_STAT_SHOCK01
   {  0,  50 }, // Opponent_STAT_SHOCK02
   {  0,  50 }, // Opponent_STAT_SHOCK03
   {  0,  50 }, // Opponent_STAT_SHOCK04
   {  0,  95 }, // Opponent_STAT_CRIPPLE00
   {  0,  95 }, // Opponent_STAT_CRIPPLE01
   {  0,  95 }, // Opponent_STAT_CRIPPLE02
   {  0,  95 }, // Opponent_STAT_CRIPPLE03
   {  0,  95 }, // Opponent_STAT_CRIPPLE04
   {  0,  5  }, // Opponent_STAT_NB_PARRY00
   {  0,  5  }, // Opponent_STAT_NB_PARRY01
   {  0,  5  }, // Opponent_STAT_NB_PARRY02
   {  0,  5  }, // Opponent_STAT_NB_PARRY03
   {  0,  5  }, // Opponent_STAT_NB_PARRY04
//   {  0,  10 }, // Opponent_STAT_NB_PARRYSHIELD
};
  */       /*
s_WDatProc_stat_color STAT_COLOR [ Opponent_NB_STATYPE ] =
{
//#define Opponent_STATYPE_BASE      0
   { 200, 200, 255 },
//#define Opponent_STATYPE_HEALTH    1
   { 255, 200, 50 },
//#define Opponent_STATYPE_WOUND     2
   { 200, 50, 50 },
//#define Opponent_STATYPE_MAZE      3
   { 255, 50, 255 },
//#define Opponent_STATYPE_SPELL     4
   { 255, 255, 50 },
//#define Opponent_STATYPE_RACE      5  // character
   {  0, 255, 255 },
//#define Opponent_STATYPE_SKILL     6  // character
   { 50, 255, 50 },
//#define Opponent_STATYPE_EQUIPMENT 7  // Character
   { 200, 200, 200 },
//#define Opponent_STATYPE_OTHER     8  // Fighting style or other
   { 100, 100, 255 },
//#define Opponent_STATYPE_BULK      9  // various
   { 150, 150, 100 },
};           */



/*s_Ennemy_generic_weapon ENNEMY_WEAPON [ Ennemy_NB_WEAPON ] =
{
   {"Long Sword        ",       6,      46,     0,        0},
   {"Blade             ",       6,     110,     0,    18432},
   {"Bastard Sword     ",       6,     175,     0,        0},
   {"Short Bow         ",       2,     250,     0,    12873},
   {"Dagger            ",       2,      44,     0,       35},
   {"Spear             ",       7,     175,     0,        4},
   {"Mace              ",      65,      46,     0,        0},
   {"Flail             ",      65,      46,     0,       73},
   {"Rod               ",       1,      45,     0,        0},
   {"Battle Axe        ",       4,     175,     0,    32768},
};

s_Ennemy_generic_armor ENNEMY_ARMOR [ Ennemy_NB_ARMOR ] =
{
   {"None              ",   0,   0,   0 },
   {"Leather Armor     ",   1,   3,   1 },
   {"Chain Mail        ",   2,   4,   3 },
   {"Plate Mail        ",   3,   5,   5 },
   {"Light Scales      ",   0,   2,   2 },
   {"Heavy Scales      ",   1,   4,   4 },
   {"Hard Shell        ",   2,   6,   5 },
};

s_Ennemy_generic_shield ENNEMY_SHIELD [ Ennemy_NB_SHIELD ] =
{
   {"None              ",   0,   0,   0 },
   {"Buckler           ",   0,   1,   1 },
   {"Small Shield      ",   1,   2,   2 },
   {"Large Shield      ",   2,   3,   2 },
   {"Tower Shield      ",   3,   4,   3 },
};
  */

/*short LIFE_SPAN [ 5 ] =
{ 50, 80, 120, 200, 320 };*/

/*s_Race_stat_modifier STAT_MODIFIER [ 6 ] =
{
   { "+1 HP / Level","HP+" },
   { "-1 HP / Level","HP-" },
   { "+1 MP / Level","SP+" },
   { "-1 MP / Level","SP-" },
//   { "+8 LP        ","LP+" },
//   { "-8 LP        ","LP-" }
};
*/

/*-------------------------------------------------------------------------*/
/*-                       Constant String Tables                          -*/
/*-------------------------------------------------------------------------*/



/*
s_SICmd_item SICMDITEM_TEST [] =
{
   {"Allo", SICmdProc_test_allo },
   {"Bonjour", SICmdProc_test_bonjour },
   {"Comment Ca Va", SICmdProc_test_comment_ca_va },
   {"Ca va bien", SICmdProc_test_ca_va_bien },
   {"Exit", NULL },
};*/


/*
s_Character_race_info RACE_INFO [ Character_NB_RACE ] =
{
   { "High Men ", "High Mens ", "Humans    ",
      {  8,  8,  8,  8,  8,  8 }, 0, -1, 80, 0, 0,
      0 Opponent_TEMPLATE_HUMANOID },
   { "Jinrui   ", "Jinrui    ", "Humans    ",
      {  7,  9,  7,  7,  9,  9 }, 0, -1, 80, 0, 0,
      0 Opponent_TEMPLATE_HUMANOID },
   { "High Elf ", "High Elves", "Elves     ",
      {  7,  9,  5, 10,  7,  8 },
      Character_RESTRICT_CHAOTIC + Character_RESTRICT_EVIL,
      Opponent_ELEMENT_LIFE, 320,
      Opponent_HEALTH_SEALED, 0 Opponent_SABILITY_NATURALMANA,
      0 Opponent_TEMPLATE_HUMANOI},
   { "Dark Elf ", "Dark Elves", "Elves     ",
      {  7,  8,  5,  9,  8,  9 },
      Character_RESTRICT_CHAOTIC + Character_RESTRICT_GOOD,
      Opponent_ELEMENT_DEATH, 280,
      Opponent_HEALTH_DOOMED, 0 Opponent_SABILITY_DARKVISION,
      0 Opponent_TEMPLATE_HUMANOI},
   { "Halfling ", "Halflings ", "Short Mens",
      {  6,  9,  6,  8, 10,  8 },
      Character_RESTRICT_EVIL, -1, 120,
      Opponent_HEALTH_CURSED, 0,
      0 Opponent_TEMPLATE_HUMANOI},
   { "Dwarf    ", "Dwarves   ", "Short Mens",
      { 10,  6,  9,  6,  7,  9 },
      Character_RESTRICT_EVIL,
      Opponent_ELEMENT_EARTH, 240,
      Opponent_HEALTH_DISEASE, 0,
      0 Opponent_TEMPLATE_HUMANOI},
   { "Draconian", "Draconians", "Reptiles  ",
      {  9,  6,  9,  5,  7, 10 },
      Character_RESTRICT_GOOD + Character_RESTRICT_EVIL,
      Opponent_ELEMENT_AIR, 60,
      0, 0 Opponent_SABILITY_LEVITATE + Opponent_SABILITY_NATURAL_ARMOR ,
      0 Opponent_TEMPLATE_WING_HUMANOI},
   { "Lizardman", "Lizardmens", "Reptiles  ",
      {  9,  8, 10,  5,  7,  8 },
      Character_RESTRICT_GOOD + Character_RESTRICT_EVIL,
      Opponent_ELEMENT_WATER, 60,
      0, 0 Opponent_SABILITY_AMPHIBIOUS,
      0 Opponent_TEMPLATE_HUMANOI},
   { "Orc      ", "Orcs      ", "Goblinoids",
      {  9,  7,  9,  7,  7,  8 },
      Character_RESTRICT_LAWFULL + Character_RESTRICT_BALANCED,
      -1, 80, Opponent_HEALTH_AFFRAID, 0,
      0 Opponent_TEMPLATE_HUMANOI},
   { "Goblin   ", "Goblins   ", "Goblinoids",
      {  6, 10,  7,  9,  8,  7 },
      Character_RESTRICT_LAWFULL + Character_RESTRICT_BALANCED,
      Opponent_ELEMENT_FIRE, 60,
      Opponent_HEALTH_POISONED, 0,
      0 Opponent_TEMPLATE_HUMANO}
};*/
/*
s_Weapon_template WEAPON_TEMPLATE [ Weapon_NB_TEMPLATE ] =
// weapon template types
// 0 to 23 = chracter weapons
// 24 a 47 = Monster Weapons
{// name, Prob%, hit%, unpary%, multi%, type, penetration,damage, shock
   {  "Short Sword",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END,
    { {"Thrust    ", 30, 15, 50, 60, Weapon_DAMAGE_PIERCE,     60,  8,  5 },
      {"Slice     ",100, 25, 50, 80, Weapon_DAMAGE_CUT,        35,  6, 10 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Long Sword",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END,
    { {"Thrust    ", 20, 15, 50, 60, Weapon_DAMAGE_PIERCE,     65, 10,  6 },
      {"Slice     ",100, 25, 50, 80, Weapon_DAMAGE_CUT,        40, 8, 12 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Blade",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Thrust    ", 20, 20, 50, 80, Weapon_DAMAGE_PIERCE,     55, 10,  4 },
      {"Slice     ",100, 30, 50,100, Weapon_DAMAGE_CUT,        35, 10,  8 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Rapier",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_DEX_CUN,
    { {"Thrust    ", 60, 20, 40, 80, Weapon_DAMAGE_PIERCE,     45, 10,  3 },
      {"Cut       ", 90, 30, 40,100, Weapon_DAMAGE_CUT,        30,  6,  6 },
      {"Punch     ",100, 15,100, 80, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 10 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Bastard Sword",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Slice     ", 80, 10, 60, 60, Weapon_DAMAGE_CUT,        45,  6, 14 },
      {"Thrust    ",100,  5, 60, 40, Weapon_DAMAGE_PIERCE,     70,  8,  7 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Long Bow",
      Weapon_CATEGORY_MISSILE + Weapon_RANGE_LONG + Weapon_ATTRIB_DEX_CUN
         + Weapon_TWOHANDED,
    { {"Quick Shot", 20,  5,100, 30, Weapon_DAMAGE_PIERCE,     70, 13,  2 },
      {"Aim Shot  ",100, 20,100,  0, Weapon_DAMAGE_PIERCE,     85, 14,  2 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Short Bow",
      Weapon_CATEGORY_MISSILE + Weapon_RANGE_LONG + Weapon_ATTRIB_DEX_CUN
         + Weapon_TWOHANDED,
    { {"Quick Shot", 40, 10,100, 40, Weapon_DAMAGE_PIERCE,     70, 11,  2 },
      {"Aim Shot  ",100, 20,100,  0, Weapon_DAMAGE_PIERCE,     80, 12,  2 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Cross Bow",
      Weapon_CATEGORY_MISSILE + Weapon_RANGE_LONG + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Shot      ",100, 15,100,  0, Weapon_DAMAGE_PIERCE,     80, 13,  2 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Pistol",
      Weapon_CATEGORY_MISSILE + Weapon_RANGE_LONG + Weapon_ATTRIB_STR_END,
    { {"Fire      ",100,  5,100,  0, Weapon_DAMAGE_PIERCE,     90, 12,  3 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Riffle",
      Weapon_CATEGORY_MISSILE + Weapon_RANGE_LONG + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Fire      ",100,  5,100,  0, Weapon_DAMAGE_PIERCE,    100, 12,  3 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Dagger",
      Weapon_CATEGORY_OFFHAND + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END,
    { {"Stab      ", 70, 20,100,100, Weapon_DAMAGE_PIERCE,     60,  9,  6 },
      {"Hit       ", 85, 10,100,100, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  8 },
      {"Cut       ",100,  5, 80,100, Weapon_DAMAGE_CUT,        30,  6,  3 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Main Gauche",
      Weapon_CATEGORY_OFFHAND + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END,
    { {"Thrust    ", 60, 15, 60, 80, Weapon_DAMAGE_PIERCE,     45, 10,  3 },
      {"Cut       ", 90, 25, 60,100, Weapon_DAMAGE_CUT,        30,  6,  6 },
      {"Punch     ",100, 15,100, 80, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 10 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Staff",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Strike    ", 60, 30, 60,100, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 11 },
      {"Stab      ", 80, 15, 60, 70, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 10 },
      {"Push      ",100, 20,100, 80, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  9 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Spear",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Thrust    ", 50, 20, 60, 60, Weapon_DAMAGE_PIERCE,     75, 13,  7 },
      {"Stab      ", 60, 15, 50, 60, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  9 },
      {"Strike    ", 90, 25, 50,100, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 10 },
      {"Push      ",100, 20,100, 80, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  8 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Pole Arm",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_SHORT + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Thrust    ", 30, 15, 60, 30, Weapon_DAMAGE_PIERCE,     85, 15,  7 },
      {"Cut       ", 60, 15, 50, 50, Weapon_DAMAGE_CUT,        35, 10,  9 },
      {"Strike    ", 80, 20, 50, 70, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 10 },
      {"Stab      ", 90, 10, 50, 30, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  9 },
      {"Push      ",100, 15,100, 50, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  8 } }
   },
   {  "Mace",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END,
    { {"Strike    ", 80, 25, 60, 50, Weapon_DAMAGE_PIERCE,      55,  6, 13 },
      {"Stab      ",100, 10, 60, 30, Weapon_DAMAGE_PIERCE,      55,  6,  9 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "War Hammer",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Strike    ", 70, 20, 70, 50, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 15 },
      {"Stab      ",100, 10, 60, 30, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 11 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Flail",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END,
    { {"Strike    ",100, 25, 80, 50, Weapon_DAMAGE_PIERCE,      55,  6, 12 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Rod",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END,
    { {"Strike    ", 80, 25, 50, 50, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 10 },
      {"Stab      ",100, 15, 50, 30, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  7 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Axe",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END,
    { {"Cut       ", 70, 15, 70, 60, Weapon_DAMAGE_CUT,        60 , 11, 11 },
      {"Stab      ", 85,  5, 60, 40, Weapon_DAMAGE_PIERCE,     55,  7, 9 },
      {"Strike    ",100, 15, 60, 60, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 13 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Battle Axe",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Cut       ", 70, 15, 70, 50, Weapon_DAMAGE_CUT,        70, 13, 13 },
      {"Stab      ", 85,  5, 60, 30, Weapon_DAMAGE_PIERCE,     55,  7, 11 },
      {"Strike    ",100, 15, 60, 50, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 15 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Scythe",
      Weapon_CATEGORY_MELEE + Weapon_RANGE_MELEE + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Slice     ", 45, 20, 60, 40, Weapon_DAMAGE_CUT,        35, 11, 12 },
      {"Thrust    ", 65,  5, 70, 30, Weapon_DAMAGE_PIERCE,     65, 13,  8 },
      {"Stab      ", 75, 10, 50, 40, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  9 },
      {"Strike    ", 90, 15, 50, 60, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 10 },
      {"Push      ",100, 15,100, 80, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  8 } }
   },
   {  "Chain Sickle",
      Weapon_CATEGORY_GRAPPLE + Weapon_RANGE_SHORT + Weapon_ATTRIB_DEX_CUN
         + Weapon_TWOHANDED,
    { {"Cut       ", 30, 10, 80, 20, Weapon_DAMAGE_CUT,        30,  6,  8 },
      {"Strike    ", 70, 25, 80, 20, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 10 },
      {"Thrust    ", 80,  5, 80, 20, Weapon_DAMAGE_PIERCE,     50,  8,  4 },
      {"Etangle   ",100, 20, 90, 30, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 14 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Mornig Star",
      Weapon_CATEGORY_GRAPPLE + Weapon_RANGE_SHORT + Weapon_ATTRIB_STR_END
         + Weapon_TWOHANDED,
    { {"Strike    ", 80, 20, 80, 20, Weapon_DAMAGE_PIERCE,      55,  6, 13 },
      {"Etangle   ",100, 20, 90, 30, Weapon_DAMAGE_SHOCK_ONLY,  0,  0, 14 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 } }
   },
   {  "Wand",
      Weapon_CATEGORY_MISSILE + Weapon_RANGE_LONG + Weapon_ATTRIB_INT_WIL,
    { {"Blast     ", 20, 30,100, 10, Weapon_DAMAGE_SHOCK_ONLY,  0,  6, 12 },
      {"Beam      ", 40, 20,100,  0, Weapon_DAMAGE_PIERCE,      40, 6,  9 },
      {"Spray     ", 60, 10,100, 20, Weapon_DAMAGE_SPLASH,      0,  0,  5 },
      {"Ball      ", 80, 10,100, 50, Weapon_DAMAGE_SHOCK_ONLY,  0,  0,  8 },
      {"Shard     ",100, 10,100, 20, Weapon_DAMAGE_PIERCE,      10, 2,  4 } }
   },
   {  "Throwing Knifes",
      Weapon_CATEGORY_THROWN + Weapon_RANGE_SHORT + Weapon_ATTRIB_DEX_CUN,
    { {"Throw     ",100, 20,100, 50, Weapon_DAMAGE_PIERCE,     60,  6,  5 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 }             }
   },
   {  "Throwing Disk",
      Weapon_CATEGORY_THROWN + Weapon_RANGE_SHORT + Weapon_ATTRIB_DEX_CUN,
    { {"Throw     ",100, 25,100, 30, Weapon_DAMAGE_CUT,        40,  6,  8 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 }             }
   },
   {  "Throwing Axe",
      Weapon_CATEGORY_THROWN + Weapon_RANGE_SHORT + Weapon_ATTRIB_DEX_CUN,
    { {"Cut       ",100, 15,100, 10, Weapon_DAMAGE_CUT,        80, 10, 10 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 }             }
   },
   {  "Thrown Mini",
      Weapon_CATEGORY_THROWN + Weapon_RANGE_SHORT + Weapon_ATTRIB_DEX_CUN,
    { {"Throw     ",100, 30,100, 80, Weapon_DAMAGE_PIERCE,     20,  1,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 },
      {"          ",  0,  0,  0,  0, Weapon_DAMAGE_PIERCE,      0,  0,  0 }             }
   }
};*/

/*
s_Character_skill_info SKILL_INFO [ Character_SKILL_LIST_SIZE ] =
{
   {"Weapon Mastery     ", Character_STRENGTH ,     Character_DEXTERITY },
   {"Weapon Art         ", Character_DEXTERITY ,    Character_CUNNING },
   {"Weapon Magic       ", Character_DEXTERITY ,    Character_INTELLIGENCE },
   {"Forge Item         ", Character_STRENGTH ,     Character_ENDURANCE },
   {"Mechanics          ", Character_DEXTERITY ,    Character_INTELLIGENCE },
   {"Stealth            ", Character_DEXTERITY ,    Character_CUNNING },
   {"Assassin           ", Character_DEXTERITY ,    Character_INTELLIGENCE },
   {"Acrobatics         ", Character_DEXTERITY ,    Character_ENDURANCE },
   {"Magic Weilding     ", Character_CUNNING ,      Character_WILLPOWER },
   {"Spell Casting Fire ", Character_INTELLIGENCE , Character_CUNNING },
   {"Spell Casting Water", Character_INTELLIGENCE , Character_CUNNING },
   {"Spell Casting Air  ", Character_INTELLIGENCE , Character_CUNNING },
   {"Spell Casting Earth", Character_INTELLIGENCE , Character_CUNNING },
   {"Spell Casting Life ", Character_INTELLIGENCE , Character_CUNNING },
   {"Spell Casting Death", Character_INTELLIGENCE , Character_CUNNING },
   {"Summoning          ", Character_CUNNING , Character_WILLPOWER },
   {"Enchant Item       ", Character_DEXTERITY ,    Character_INTELLIGENCE },
   {"Identify           ", Character_INTELLIGENCE , Character_CUNNING },
   {"Dispel             ", Character_CUNNING ,      Character_WILLPOWER },
   {"Healing            ", Character_INTELLIGENCE , Character_CUNNING },
};*/

/*
V3D ELEMENT_DIAGRAM [ Opponent_NB_ELEMENT ] [ Opponent_NB_ELEM_LEVEL ] =
{
//Opponent_ELEMENT_LIFE      0
   {
      { 198<<16,  70<<16, 1,0,0, makecol24 ( 200, 255, 255 ) },
      { 198<<16,  80<<16, 1,0,0, makecol24 ( 200, 255, 255 ) },
      { 198<<16,  90<<16, 1,0,0, makecol24 ( 200, 255, 255 ) },
      { 198<<16, 100<<16, 1,0,0, makecol24 ( 200, 255, 255 ) },
      { 198<<16, 110<<16, 1,0,0, makecol24 ( 200, 255, 255 ) },
   },
//Opponent_ELEMENT_WATER     1
   {
      { 238<<16,  90<<16, 1,0,0, makecol24 ( 250, 150, 100 ) },
      { 228<<16,  95<<16, 1,0,0, makecol24 ( 250, 150, 100 ) },
      { 218<<16, 100<<16, 1,0,0, makecol24 ( 250, 150, 100 ) },
      { 208<<16, 105<<16, 1,0,0, makecol24 ( 250, 150, 100 ) },
      { 198<<16, 110<<16, 1,0,0, makecol24 ( 250, 150, 100 ) },
   },
//Opponent_ELEMENT_AIR       2
   {
      { 238<<16, 130<<16, 1,0,0, makecol24 ( 175, 175, 175 ) },
      { 228<<16, 125<<16, 1,0,0, makecol24 ( 175, 175, 175 ) },
      { 218<<16, 120<<16, 1,0,0, makecol24 ( 175, 175, 175 ) },
      { 208<<16, 115<<16, 1,0,0, makecol24 ( 175, 175, 175 ) },
      { 198<<16, 110<<16, 1,0,0, makecol24 ( 175, 175, 175 ) },
   },
//Opponent_ELEMENT_DEATH     3
   {
      { 198<<16, 150<<16, 1,0,0, makecol24 ( 100, 75, 125 ) },
      { 198<<16, 140<<16, 1,0,0, makecol24 ( 100, 75, 125 ) },
      { 198<<16, 130<<16, 1,0,0, makecol24 ( 100, 75, 125 ) },
      { 198<<16, 120<<16, 1,0,0, makecol24 ( 100, 75, 125 ) },
      { 198<<16, 110<<16, 1,0,0, makecol24 ( 100, 75, 125 ) },
   },
//Opponent_ELEMENT_FIRE      4
   {
      { 158<<16, 130<<16, 1,0,0, makecol24 ( 100, 100, 225 ) },
      { 168<<16, 125<<16, 1,0,0, makecol24 ( 100, 100, 225 ) },
      { 178<<16, 120<<16, 1,0,0, makecol24 ( 100, 100, 225 ) },
      { 188<<16, 115<<16, 1,0,0, makecol24 ( 100, 100, 225 ) },
      { 198<<16, 110<<16, 1,0,0, makecol24 ( 100, 100, 225 ) },
   },
//Opponent_ELEMENT_EARTH     5
   {
      { 158<<16,  90<<16, 1,0,0, makecol24 ( 50, 150, 150 ) },
      { 168<<16,  95<<16, 1,0,0, makecol24 ( 50, 150, 150 ) },
      { 178<<16, 100<<16, 1,0,0, makecol24 ( 50, 150, 150 ) },
      { 188<<16, 105<<16, 1,0,0, makecol24 ( 50, 150, 150 ) },
      { 198<<16, 110<<16, 1,0,0, makecol24 ( 50, 150, 150 ) },
   }
};*/
