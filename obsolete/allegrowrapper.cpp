///////////////////////////////////////////////////////////////////////////
//                                                                       //
//                 A l l e g r o w r a p p e r . c p p                   //
//                                                                       //
//   Content: Wrappoer Functions for textout family                      //
//   Author: Eric Pietrocupo                                             //
//   Starting date: april 15th, 2012                                     //
//                                                                       //
//   Allegro seems to have changed his textout function family by adding //
//   the background as a parameter. Since I use textout intensively, I   //
//   decided to wrap the new function by using the old names             //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

#include <allegro.h>
#include <allegrowrapper.h>

/*----------------------------------------------------------------------*/
/*                              Procedures                              */
/*----------------------------------------------------------------------*/
/*
void textout(BITMAP *bmp, const FONT *f, const char *s, int x, int y,
int color, int bg)
{
   textout_ex ( bmp, f, s, x, y, color, -1 );
}

void textout_centre(BITMAP *bmp, const FONT *f, const char *s, int x, y,
int color, int bg)
{
   textout_centre_ex ( bmp, f, s, x, y, color, -1 );
}

void textout_right(BITMAP *bmp, const FONT *f, const char *s, int x, int
y, int color, int bg)
{
   textout_right_ex ( bmp, f, s, x, y, color, -1 );
}


void textout_justify(BITMAP *bmp, const FONT *f, const char *s, int x1,
int x2, int y, int diff, int color, int bg)
{
   textout_justify_ex ( bmp, f, s, x, y, diff, color, -1 );
}


void textprintf(BITMAP *bmp, const FONT *f, int x, int y, int color, int
bg, const char *fmt, ...)
{
   va_list listPointer;
   va_start( listPointer, fmt );

   
   textprintf_ex ( bmp, f, s, x, y, color, -1, fmt, ... );
}


void textprintf_centre(BITMAP *bmp, const FONT *f, int x, int y, int
color, int bg, const char *fmt, ...)
{
   textprintf_centre_ex ( bmp, f, s, x, y, color, -1, fmt, ... );
}


void textprintf_right(BITMAP *bmp, const FONT *f, int x, y, color, bg,
const char *fmt, ...)
{
   textprintf_right_ex ( bmp, f, s, x, y, color, -1, fmt, ... );
}

void textprintf_justify(BITMAP *bmp, const FONT *f, int x1, x2, y, diff,
color, bg, const char *fmt, ...)
{
   textprintf_justify_ex ( bmp, f, s, x, y, diff, color, -1, fmt, ... );
}
*/
