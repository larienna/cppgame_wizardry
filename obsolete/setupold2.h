/* Allegro datafile object indexes, produced by grabber v3.11 */
/* Datafile: e:\allegro\setup\setup.dat */
/* Date: Sun Apr 11 11:27:06 1999 */
/* Do not hand edit! */

#define SETUP_EMBEDDED

extern "C" {

#ifdef SETUP_EMBEDDED
int setup_main(void);
#else
int main(void);
#endif

}


