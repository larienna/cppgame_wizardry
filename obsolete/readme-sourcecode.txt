Wizardry Legacy
Source Code

Copyright Eric Pietrocupo

  This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

This is the source code for wizardry Legacy. There is no make files 
but I included the project files for Code Blocks which is the IDE
I have used.

The game requires allegro 4.2 or 4.4 and Sqlite 3 to work.

