/* Allegro datafile object indexes, produced by grabber v4.0.1, djgpp */
/* Datafile: E:\CPP\WIZARDRY\DATAFILE\FONT.DAT */
/* Date: Wed Mar 10 08:55:45 2004 */
/* Do not hand edit! */

#define FNT_AMBROSIA24                   0        /* FONT */
#define FNT_BLACKCHANCERY16              1        /* FONT */
#define FNT_BLACKCHANCERY24              2        /* FONT */
#define FNT_ELGAR24                      3        /* FONT */
#define FNT_ELGAR32                      4        /* FONT */
#define FNT_FREEHAND16                   5        /* FONT */
#define FNT_FREEHAND24                   6        /* FONT */
#define FNT_HELENA24                     7        /* FONT */
#define FNT_HELVETICA22                  8        /* FONT */
#define FNT_KAUFMAN16                    9        /* FONT */
#define FNT_KAUFMAN24                    10       /* FONT */
#define FNT_PRINT                        11       /* FONT */
#define FNT_SCRIPT                       12       /* FONT */
#define FNT_SMALL                        13       /* FONT */
#define ZZZ_END_OF_FONT                  14       /* END  */

