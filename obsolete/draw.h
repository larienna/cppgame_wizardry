/***************************************************************************/
/*                                                                         */
/*                             D R A W . H                                 */
/*                           Module Definition                             */
/*                                                                         */
/*     Content : common drawing procedures                                 */
/*     Programmer : Eric Pietrocupo                                        */
/*     Starting Date : May 21st, 2002                                      */
/*                                                                         */
/*          This module contains various drawing procedures used by many   */
/*     classes in the programm.                                            */
/*          THe new vocation of these modules is to draw all the stuff     */
/*     located outside the view poort of the game or added over the top    */
/*     of everything drawn. For example: party bars                        */
/*                                                                         */
/***************************************************************************/

/*-------------------------------------------------------------------------*/
/*-                            Constants                                  -*/
/*-------------------------------------------------------------------------*/

/*#define Draw_INSTRUCTION_SELECT  1
#define Draw_INSTRUCTION_CANCEL  2
#define Draw_INSTRUCTION_DISPLAY 4
#define Draw_INSTRUCTION_MENU    8
#define Draw_INSTRUCTION_SWITCH  16
#define Draw_INSTRUCTION_MOVE    32
#define Draw_INSTRUCTION_INPUT   64
#define Draw_INSTRUCTION_CAMP    128*/
/*
// bprder corner ID
#define Draw_CORNER_TLEFT     1
#define Draw_CORNER_TRIGHT    1
#define Draw_CORNER_BLEFT     1
#define Draw_CORNER_BRIGHT    1*/

/*-------------------------------------------------------------------------*/
/*-                       Global Variable Definition                      -*/
/*-------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------*/
/*-                          Prototypes                                   -*/
/*-------------------------------------------------------------------------*/

// normally included by Window class, but used by external window for no offset drawing.
void draw_border ( short x1, short y1, short x2, short y2, int color );
void draw_border_fill ( short x1, short y1, short x2, short y2, int color, int fill );
//void draw_instruction ( const char *str, short x, short y );
//void draw_instruction ( unsigned char instID, short x, short y );

//void draw_party_bar ( Party &party );
//void draw_party_spell ( Party &party );
//void draw_party_list ( void );

//void draw_health_status ( s_Opponent_hstr hstr, short x, short y );
//void draw_character ( Character &character );
//void draw_character_list ( bool mask_unavailable = false );
//void draw_new_character ( Character &character );



