/**
* S Q L R E C O R D S E T . C P P
*
* Programmer: Eric Pietrocupo
* Starting Date: October 15th, 2017
*
* Structure that handle data recieved from an SQL SELECT query
* with methods to manipulate this information.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <sqlrecordset.h>




/** Creates a recordset from a query */
void  SQLrs_create ( s_SQLrecordset *rs, sqlite3 *db, const char *query )
{
   sqlite3_stmt *stmt = NULL;
   int sqlcode;

   sqlcode = sqlite3_prepare_v2 ( db, query, -1, &stmt, NULL );

   if ( sqlcode == SQLITE_OK )
   {

   }
   else
   {
      printf("\r\nError: SQLrs_create: %s\r\n", sqlite3_errmsg(db));
      printf("\r\n   Query: %s", query);
   }

}
/** Creates a recordset from a query and allow passing formated parameters*/
void  SQLrs_createf ( s_SQLrecordset *rs, sqlite3 *db, const char *query, ... )
{
   va_list arguments;

   char *tmpstr;
   int length;

   //get the length by supplying an empty destination to allocate a buffer.
   va_start (arguments, query );
   length = vsnprintf ( NULL, 0, query, arguments);
   length++;
   tmpstr =  malloc (length );
   va_end (arguments);

   //redo the same process for real
   va_start (arguments, query);
   vsnprintf (tmpstr, length, query, arguments);
   va_end (arguments);


   printf ("The initial query is :\n%s\n", query );
   printf ("The final query is :\n%s", tmpstr );

   //SQLrs_create ( rs, db, tmpstr );
}


/** Free the memory allocated by a recordset */
void  SQLrs_free ( s_SQLrecordset rs )
{
}

/** Get the index key of a field where you only have the name */
void  SQLrs_indexof ( s_SQLrecordset rs, const char *name )
{
}

/** Convenience method to read data of a specific field and record */
char *SQLrs_data ( s_SQLrecordset rs, int recordid, int fieldid )
{
}

/** Read data of a field in a record and convert it to integer */
int   SQLrs_data_int ( s_SQLrecordset rs, int recordid, int fieldid )
{
}

/** Read data of a field in a record and convert it to float */
float SQLrs_data_float ( s_SQLrecordset rs, int recordid, int fieldid )
{
}

/** Print all the information about the structure of the recordset */
void SQLrs_print_structure ( s_SQLrecordset rs )
{
}

/** Display all the fields of the recordset */
void SQLrs_print ( s_SQLrecordset rs )
{
}


