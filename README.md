Wizardry Legacy
===============

Copyright (C) 2002-2004, 2006, 2012- Eric Pietrocupo  
Original Copyright (C) 1981 Sir-Tech Software  
License: GNU General Public License  

Website: [Wizardry Legacy Website](http://wl.lariennalibrary.com/index.php)  
email: ericp[at]lariennalibrary.com

# Introduction

Welcome to Wizardry legacy. This program is an open source game based on the original Wizardry made by Sir-Tech Software. The project was abandoned in 2004 and patched in 2006 and was brought back from the haunted archives in 2012, set on hold around 2015 and now it's available on Gitlab. The next step would be able to make it available on linux repositories once adventures are available.

The objective is to make a wizardry game that looks very close to the original wizardry games with additional functionalities. Some changes will be made to the game design and it will be shipped with an adventure editor to allow players to make their adventures. The game is almost playable and an adventure can possibly be made with what I have so far since the demo supplied with the game is finishable. I have an adventure in mind, but I did not build anything yet.

Near 2015, I started to code in Java, and I intend to do more Java video game programming since it allow deployment on android. So I have less time to work on this project, but like getting back to it once a while. Now that I have more background in object oriented programming and C language programming, I almost want to refactor half of the code and replace the C++ code to make it a C only game. Still, it's not worth the time just to make things better coded. If you want to contribute, you can do so with artwork, sounds, music and programming. If I could use only authorised and open assets, it would make it's distribution easier. As for the programming, I only want to keep the control of the game rule design. The rest is open for modification, but ask me first if you want to add or change something.

For more information about the game play, check out the [website](http://wl.lariennalibrary.com/index.php?n=GameBook.GameBook)

# Project re-orientation and new strategy

There has been a lot of thought about if I should finish this project. Since many years of works are still required. I am flirting with the idea of making a commercial project, but that implies even more work and I am not sure I'll be able to deliver the merchandise. An RPG is also more demanding in matter of assets(espcially adventures) than strategy games.

So I decided to make a survey to gather feed back. If you are interested, you can fill up the survey below so that I can know if and how I should make a new video game.

[Wizardry Legacy Survey](https://docs.google.com/forms/d/e/1FAIpQLSfUcQY9N-k1N2wsimAjBB4H8aODUelCSVqHOy4Xoe0AytKErQ/viewform)

For now, there new strategy is to divide and conquer. I decided to extract and refactor the portion of the game that could be reusable any other projects, even non-gaming projects, and make them standalone project. I could work on those modules once a while when I have some time. Then later, I could decide if I go commercial and create a new game by sticking the new modules together. Or if I just make a remake of the old Wizardry by sticking the new modules to the old projects. So be on the look-out for side projects in my gitlab repositories:

* [Mapper](https://gitlab.com/larienna/mapper): This is the 3D maze engine, recreated and improved. It's now using real 3D projection.
* Glymmer: This is the graphic text menu system with the connection to the database.
* F(x)Launcher: This module would allow launching business logic functions from the command line or from inside the program.

All those subprojects would be apache licensed allowing their use in any other commercial projects. 

# Download

You can download the windows binaries from my [website](http://wl.lariennalibrary.com/index.php?n=Main.Download). If you are on linux or other platform, you should download the sources and compile the software yourself because the binaries are hardly portable. The other advantages of building the sources yourself is that you get the most up to date version of the game. While the binaries have release schedules. I intend eventually add the game to official repositories when I'll have a playable adventure.

# Setup

This program use the Allegro Video game programming library which contains can support many kind of computer hardware and is portable on many operating systems. This is why I removed the allegro setup utility.

If the game fails to run, you can always use the allegro setup utility that will allow you to create the “allegro.cfg” file that you simply need to copy in the root directory of the game.

The game run in 640x480x16bit colors in full screen. All video card should support this video resolution since it's vesa standard. Even my netbook, which has an awkward screen resolution, support it. Larger resolutions allow surrounding party frames and windows, but the maze display remains the same size, it's hardcoded this way.

The game use only a keyboard interface, if you want to play with a joystick, setup a joystick emulator like “JoytoKey”(Windows) that convert joystick input to keyboard input. There are not that many keys to set.

# Known Bugs

* Sound in Linux: sound and music might not work correctly.
* In the editor, when debugging a lock encounter, it crashes. During the game it works fine
* In the intro/ending, if there is only 1 paragraph of text, it crashes. Adventure define a minimum of 2 to
bypass the bug.
* Some features are no yet completely implemented. So things will be lacking to the game for sure.

Found a new bug, notice me by e-mail or raise an issue on gitlat with a detailed explanation and a list of steps to reproduce the bug.

# Game files

This is the files required for the game to run that should be available after compilation or installation.

### Files Required

* __wizardry (or wizardry.exe)__ : This is the binary executable. If you don't have that file, you have no game.
* __adventure__ : This is the adventures available to play. If you download new adventures, place them there.
* __datafile__ : This is the assets of the game, contains pictures, music, sounds, etc.

### Files generated after execution

* __savegame__ : This is the folder where all your savegames are located. Yes you can backup this to avoid unpleasant surprises in the game (like getting anihilated in the dungeon)
* __wizardry.cfg__ : A binary file that contains default configuration settings to be used for new games.
* __allegro.cfg__ : Configuration file for allegro. Currently setup the sound driver. _IMPORTANT_ if you move the files into a new directory, edit this file and change the path to digimid or delete the file and start wizardry again.

### Documentation

* __license.txt__ : a copy of the GNU General Public License
* __REAMDE.md__ : this file

### Libraries

On windows, the following files should also be available in the game directory. The files are available in the binary installation of windows.

* __alleg44.dll__ : The allegro library
* __sqlite3.dll__ : the sqlite library

You might be able to find those .dll on the internet too.

On linux, the libraries should be installed somewhere on your system as 

* __liballeg.so__ : The allegro library
* __libsqlite3.so__ : The sqlite library

If those files are missing from your system, use _apt-get_ to install the missing dependencies. Check the dependency section of this page to know the name of the packages.


# Material Sources

Contains copyrighted material from the following sources:

| Type                           | Source                                   | Copyright                 |
| ------------------------------ | ---------------------------------------- | ------------------------- |
| Music, Artwork, textures, etc. | Wizardry 1,2,3,5                         | Sir tech and many others  |
| Texture                        | Hexen and Heretic                        | Raven Software            |
| Texture                        | [www.davegh.com](www.davegh.com)         | David Gurrea              |
| Artwork                        | [Thalzon's Battlers and faces](http://forums.rpgmakerweb.com/index.php?/topic/49-thalzons-battlers-and-faces/) | Thalzon                   | 

# Dependencies

* [Allegro 4.4](http://liballeg.org/old.html)   ( debian packages: __liballegro4-dev__ __liballegro4.4__ )
* [Sqlite 3](https://sqlite.org/download.html) ( debian package: __libsqlite3-0__ __libsqlite3-dev__ )
* AASTR (Anti Alliased Stretch Blitting) by Michael Bukin (included in the project)

# Building from the sources

The project now comes with a cmake builder which makes it easier to compile and distribute on many platform. You will need for the procedure below to have installed the following software:

* git ( Debian package: __git__ )
* Allegro 4 and sqlite3 libraries 
* cmake (Debian package: __cmake__ )
* a c++ compiler (ex: gcc, Visual c++, Mingw) (Debian package: __build-essential__)

I recommend building the project yourself if you run on linux, because all the tools are easily accessible to build the program yourself. But I know that it can sometimes be pretty complicated, I built many application from sources with various problems and errors. I cannot guaranty that the procedure below is bulletproof.

I successfully compiled the code on Windows and linux, but I never tried on MacOS. In theory it should work, but I never tested it. I do not recommend building the software on linux, I lost 2 points of sanity when I built the windows version. You will also need an internet connection to build the sources, or at least download the source and install the necessary packages. Oonce that is done, you can compile offline

### installing the software

If you are on a new machine without anything to build software, you could enter the following command to install all the packages listed above if you are on a distribution that use debian packages ( like ubuntu and mint ). Open a terminal and type:

~~~sh
sudo apt-get update
sudo apt-get install git cmake build-essential liballegro4-dev liballegro4.4 libsqlite3-0 libsqlite3-dev
~~~

You will be prompted with your user password to get administrator privillege. Then let the computer do it's job and do something else.

### Getting the sources

When you open a terminal, you should start in your home folder. Either you download the file structure manually from the gitlab website, or you use the following git command if you have git installed.

~~~sh
git clone https://gitlab.com/larienna/cppgame_wizardry.git
~~~

you enter the directory with command

~~~sh
cd cppgame_wizardry
~~~

### Build the Make File

I am using cmake to build the Makefile. so create a folder called build and enter the folder. 

~~~sh
mkdir build
cd build
~~~

Then run cmake by typing the following command which use the script CMakeLists.txt in the parent directory to build the program in the build directory

~~~sh
cmake ..
~~~

cmake is multiplatform, therefore it can create the makefiles accoring to different platform configuration. The default is the unix makefiles, but if for example you want to use the MinGW platform, you will call the command like this:

~~~sh
cmake -G "MinGW Makefiles" ..
~~~

Refer to the cmake documentation to know the generators available. Look for cmake generators.

### Build the program from the sources.

At that point, the real Makefile should be available to start building the program specifically for your platform by typing:

~~~sh
make
~~~

### Install the program on the system

The install script is currently not working correctly since wizardry is designed to run everything in the same folder. Once installed, it might require admin access to just allow creation of save games. Instead you can run the program directly from the build folder by typing:

~~~sh
./wizardry
~~~

You can move the files anywhere you want in your system and create a shortcut to the binary, or add it to your path. 

### In case of trouble

If you somehow screw up the process above. You can restart compilation after a make command using

~~~sh
make clean
make
~~~

This will clear all progress so far, and recompile the game. I you want to rebuild what cmake made, you can simply delete the whole content of the build directory and call cmake again with those commands:

~~~sh
rm -r *
cmake ..
~~~

# License

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>.
